//
//  ColorCode.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit

class ColorCode: NSObject {
    
    static let lightBlue  = "#05B9EDFF"
    static let darkBlue  = "#1264B0FF"
    static let red  = "#D94D50FF"
    static let placeHolderColor  = "#9B9B9BFF"
    static let textColor  = "#323643FF"
    static let successBannerBGColor = "#05B9EDFF"
    static let infoBannerBGColor = "#1264B0FF"

    //MARK: TLB
    //MAARK:-
    static let appColor = "#305FC5FF"
    static let themeColor = "#305FC5FF"
    static let primaryColor = "#2E305FC5"
    static let errorBannerBGColor = "#FC0B04FF"
    static let textDescriptionColor = "#515C5DFF"
    static let buttontextColor = "#545B5EFF"
    static let whiteColor = "#FFFFFFFF"
    static let shadowLightGreyColor = "AAAAAAFF"
    
    static let filledColor = "#484848FF"
    static let appThemeColor = "#2E7C86FF"
    static let appSearchBgAndTitleColor = "#45b6b0FF"
    static let appTabbarButtonColor = "#1bc4bfFF"
    static let appBackgroundColor = "#f6f6f6FF"
    static let appTextBlackColor = "#43494bFF"
    static let appLightGreyShadow = "#C8CCD5FF"
    static let appGreyBluishShadowColor = "#a6afbeFF"
    static let appShadowColorOptional = "#e2e2e2FF"
    
    
    static let driverOfflineColor   = "#FFFFFFFF"
    static let driverOnlineColor    = "#FFFFFFFF"
    static let driverOfflineBGColor = "#FC0B04FF"
    static let driverOnlineBGColor  = "#15AF02FF"
    
    static let viewCartCellPriceColor = "#D52514FF"
    static let viewCartCellCurrencyColor = "#535353FF"
    
    //--Blue color--
    static let buttonGroupDetailBlueColor = "#5677FCFF"
    static let buttonEventRecurrenceDaySelectedColor = "#738FFEFF"
    
    static let popupBgColor = "#FC444DFF"
    
    static let cardColor    =     "#112F55FF"
    
    static let cursorColor = "#DD0687FF"
    static let tabInActiveColor = "#485059FF"
    static let spinnerOuterColor = "#FDD200FF"
    static let spinnerOuterLightColor = "#facd00FF"
    static let spinnerInnerColor = "#C8057AFF"
    static let spinnerInnerLightColor = "#C8057AFF"
    
    static let appYellowColor = "#FDD400FF"
    static let appPinkColor = "#C8057AFF"
    
}

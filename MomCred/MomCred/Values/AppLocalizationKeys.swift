//
//  AppLocalizationKeys.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

enum LocalizationKeys : String  {
    
    case app_name
    case title_app_update
    case msg_new_version
    case title_session_expire
    case title_error
    
    case video
    case photo
    
    case success
    case info
    case error
    case notifications
    case no_notificaition_to_clear
    case clear_all
    
    //MARK:Button Titles
    /////////////////////Button Titles//////////////////////
    case btn_ok
    case btn_cancel
    case btn_done
    case btn_yes
    case btn_no
    
    // Wrong Patient
    case validPhoneNumber
    
    // Common Error
    case allFieldRequire
    case alreadyRegisterMobileNumber
    case aleradyRegisterMobileNumerError
    case invalidMobileNumber
    
    // Sign up
    case sign_up
    case REGISTER
    case verified
    case enter_mobile_number
    case forgot_password
    case forgot_password_title
    case enter_register_number
    case full_name
    case email
    case password
    case retype_password
    case registration_desc
    case otp_verify_desc
    case enter_one_time_password
    case old_password
    case new_password
    case confirm_password
    
    //Login Screen
    case emailEmpty
    case validEmail
    case passwordLowerLimit
    case oldpasswordLowerLimit
    case newpasswordLowerLimit
    case confirmpasswordLowerLimit
    case invalidEmail
    case empty_email
    case emptyPassword
    case confirmPassworEmpty
    case passwordNotMatch
    case emptyemail
    
    //Camera
    case settings_dialog_unable_to_pick_file
    case settings_dialog_gallery_text
    case settings_dialog_camera_text
    
    // Complete Register Screen
    case FirstnameEmpty
    case lastNameEmpty
    case dobEmpty
    case ZipCodeEmpty
    case ValidZipCode
    case MobileEmpty
    case MobilevalidEmpty
    
    // Profile
    case profile
    case submit
    case ok
    case email_optional
    case mobile
    case first_name
    case last_name
    case onboard_emptyFirstName
    case onboard_emptyLastName
    case onboard_emptyUsageType
    case onboard_emptyUserName
    case onboard_emptyFullName
//    case invalidEmail
    case emptyMobile
    case invalidMobile
    case emptyAddress
    case address
    case save
    case delete
    case call_us
    case email_us
    case website
    case hamburger_contact_us
    case terms_of_service
    case privacy_policy
    case logout
    case login
    case confirm_logout
    case cancel
//    case settings_dialog_unable_to_pick_file
//    case settings_dialog_gallery_text
//    case settings_dialog_camera_text
    case passwordEmpty
    case passwordOldEmpty
    case passwordNewEmpty
//    case passwordLowerLimit
//    case confirmPassworEmpty
//    case passwordNotMatch
    
    case city
    case state
    case zipcode
    case gender
    case validZipCode
    
    case titleColor
    case titleCategories
    case addToWishList
    case removeToWishList
    
    case coupon_code
    case valid_until
    case copy_coupon
    case copied_success
    case copied
    case terms_of_service_error
    
    // Service provider
    case business_information
    case service
    case credit_services
    case calender_listing
    case personnel
    case facilities
    case inside_look_content
    case instructional_content
    case testimonials
    case payment_setting
    case change_your_password
    case sign_out
    
    // Enthusiast
    case my_purchase_history
    case my_refund
    case my_credit
    case my_voucher
    case premium_member
    
    // More option
    case additional_information
    case additional_service
    case contact_us
    case help_support
    case privacy_policy_option
    case terms_condition
    case article
    case feedback
    case share_app
    case settings
    case help
    
    // Menu Option
    case add_new
    case complete_pending
    case edit
    case termscondition
    
    //Advance Search Option
    case service_provider
    case individual_service
    case non_instructional_local_business
//    case calender_listing
    case facility
//    case personnel
//    case inside_look_content
//    case instructional_content
    
    // Service Detail
    case edit_service
    case view_purchase
    case purchase_now
    case build_package
    
    case error_invalid_url
    case error_website_invalid_url_desc
    case error_select_social_media_platform
    case error_enter_social_media_link
    case error_enter_social_media_content

    case error_document_image_not_found
    
    //Instructional or Front Office
    case lesson_single
    case lesson_group
    case camp_day
    case camp_night
    
    case error_plese_enter_cost_name
    case error_please_enter_cost_description
    case error_please_enter_price
    case error_please_enter_instructions
    
    // Business Information - OverView
    case error_select_business_image
    case error_select_business_name
    case error_enter_business_desc
    case error_enter_mission_statement
    case txt_custom_entry
    case error_select_service_provider_type
    case txt_service_provider_sub_type
    case error_select_service_provider_sub_cat_type
    case error_select_business_type
    case error_enter_business_type
    case txt_image
    case error_enter_busi_custom_entry
    
    // Business Information - Email Information
    case error_enter_primary_email
    case error_email_primary_valid
    case error_enter_primaryEmailDesc
    case error_enter_secondary_email
    case error_email_secondary_valid
    case error_enter_secondaryEmailDesc
    case error_primary_number
    case error_secondary_number
    case error_primary_phone_desc
    case error_secondary_phone_desc
    case error_add_website
    case error_add_social_webiste
    
    // Exceptional Service
    case error_txt_service_provider_classification
    case error_offer_exception_service
    case error_select_exceptional_service_offered
    case error_select_primary_exceptional_service
    
    // Service Type
    case error_select_primary_service_type
    case error_select_secondary_service_type
    case error_select_service_type

    // Additional links
    case txt_additional_links
    case txt_add_more_additional_links
    case error_please_enter_link_name
    case txt_added_additional_links
    case txt_add_more_accreditation
    case txt_qualifications
    case txt_accreditation
    case txt_added_accreditation
    case error_please_enter_link
    case error_please_enter_name
    case error_please_enter_description
    case error_please_enter_year_exp
    
    // Affiliation Partner ship
    case txt_affiliation_partnership
    case txt_add_more_affiliation_partnership
    case txt_added_affiliation_partnership
    
    // Award
    case txt_award
    case txt_add_more_award
    case txt_added_award
    
    case txt_name
    case txt_title
    case txt_link
    case txt_email
    case txt_phone
    case txt_description
    
    case error_add_accreditation
    case txt_service_types

    case txt_purchasability_text
    case error_select_purchasebility
    case error_purchasability_limit
    
    // Add Service - OverView
    case error_select_service_image
    case error_select_service_name
    case error_select_purchasebility_count
    case error_select_target_goal
    case error_select_date_time
    case error_select_repeat_time
    
    case error_enter_location_classification
    case error_enter_location_speciality
    
    case error_certificate_name
    case error_free_service_name
    case error_trial_service_name
    case error_travel_service_type
    case error_travel_service_req
    case error_travel_service_location
    
    case error_therapy_type
    case error_therapy_field_cover
    case error_therapy_payment_type
    case error_therapy_insurance_desc
    
    case error_instructional_selection
    case error_instructional_count
    case error_front_office_facility_rental_selection
    case error_listing_additional_cost
    
    case error_event_type
    case error_event_type_manual
    case error_event_charitable
    case error_event_charitable_donate
    
    case error_specific_desc
    
    case error_skill_level
    case error_skill_min_age
    case error_skill_max_age
    case error_skill_age_diff
    case error_skill_duration_type
    case error_skill_duration_time
    
    case error_content_associated_field_listing
    case error_content_associated_personal_listing
    
    case error_other_good_service_listing
    
    case error_additional_information_listing
    
    case private_str
    case public_str
    case error_city
    
    case standard
    case exceptional
    case std_exp
    case charitable
    
    case error_you_have_reached_max_limit
    
    case error_enter_social_platform_name
    case error_enter_field_category_name
    
    case txtLocationType
    case error_invalid_end_time
    case error_select_start_time
    case error_select_primary_exclusive_travel
    case error_select_secondary_exclusive_travel
    case error_enter_primary_address_name
    case error_enter_secondary_address_name
    case error_select_primary_address
    case error_select_secondary_address
    case error_primary_location_type
    case error_secondary_location_type
    case error_primary_location_ownership
    case error_secondary_location_ownership
    case error_select_business_error_cityhours
    case error_select_secondary_business_hours
    case txt_add_new
    case error_enter_new
    case error_add_field
    case error_select_service_types
    case error_add_affiliation
    case error_add_awards
    case error_select_image
    case error_video_limit
    case error_video_not_found
    case error_video_length
    case txt_year_of_delivery
    case error_provide_delivery_ser_exp
    case error_file_size
    case location_select
    case zipcode_enter

    
    case error_remove_media
    case error_this_app_needs_access_to_your_camera_and_gallery
    
    case error_busi_minimum_media
    
    case every_day
    case every_week
    case every_month
    
    case error_add_service_type
    
    case error_service_price
    case error_additional_cost
    
    case error_document_name
    case error_document_description
    
    case error_description
    case error_detail_emtpy
    
    // Personnel Service
    case error_select_personnel_service_image
    case error_select_personnel_service_name
    case error_select_personnel_type
    case error_select_personnel_job_title
    case error_select_personnel_desc
    
    case error_provide_personnel_yr_exp
    
    case error_add_training_history
    
    case error_add_service_listing
    case error_add_facility_listing
    case error_add_personnel_listing
    case error_add_amenity_listing
    
    case error_add_external_link
    
    case error_phone_number
    case error_phone_desc
    case error_certification_name
    case error_affiliation_name
    case error_training_name
    case error_accomplishment_name
    case error_add_contacts
    case error_add_emails

    // Facility
    case error_select_facility_service_image
    case error_select_facility_service_name
    case error_facility_classification
    case error_select_facility_type
    case error_select_facility_facility_title
    case error_select_facility_desc
    case error_select_facility_service_delivery_title
    case error_add_facility_personnel_listing
    
    case error_select_associated_facility_title
    case error_equipment_name
    case error_amenity_name
    
    // Inside-Look Service
    case error_select_ILC_service_image
    case error_select_ILC_service_name
    case error_select_ILC_desc
    case error_select_ILC_type
    case error_select_ILC_service_type

    case error_select_ILC_facility_title
    case error_select_ILC_associated_facility_title
    
    // Instructional Content Service
    case error_select_IC_service_image
    case error_select_IC_service_name
    case error_select_IC_type
    case error_select_IC_instructional_type
    case error_select_IC_desc
    case error_add_IC_traget_listing
    
    // Circular service selection
    case error_service_service_selection
    case error_facility_service_selection
    case error_personnel_service_selection
    case error_inside_look_service_selection
    case error_instructional_service_selection
    case error_service_added_selection
    
    // Home Scr/ Dashboard
    case video_section
    case video_section_sub_title
    case service_section
    case listing
    
    // Service and Profiel exceed alert
    case service_selection_exceed
    case profile_selection_exceed
    
    case service_selection_remaining
    case service_selection_complete
    
    case package_selection_continue
    case package_selection_complete

    // Card missing
    case card_no_missing
    case card_expiry_missing
    case card_cvv_missing
    
    case text_service_amount
    case text_purchase_complete
    
    case error_price_missing
    
    case error_no_subscription
    case error_no_subscription_service
    
    case error_in_progress
    case package_bought_text
    case error_something_wrong
    
    // Rating & Review
    case error_rating_value
    case error_rating_comment
    
    case title_redeem_code
    case error_redeem_code

    // Payment Setting
    case error_payment_country
    case error_payment_account_type
    case error_payment_account_fullname
    case error_payment_rounting_number
    case error_payment_account_number
    
    case withdraw
    case withdraw_alert_desc
    case refund_alert_desc
}

extension LocalizationKeys {
    func getLocalized() -> String{
        return self.rawValue.localized()
    }
}

extension String {
    func localized() -> String{
        return Helper.sharedInstance.getMessageForCode(self)!
    }
}

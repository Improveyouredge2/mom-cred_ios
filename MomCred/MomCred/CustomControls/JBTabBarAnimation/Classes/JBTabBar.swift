//
//  JBTabBar.swift
//  JBTabBarAnimation
//
//  Created by Jithin Balan on 2/5/19.
//

import UIKit

public class JBTabBar: UITabBar {

    private let tabBarCurveShapeLayer = CAShapeLayer()
    private struct Constants {
        static let itemWidth = 60
    }
    
    var selectedIndex: Int = 0
    
    var startPoint: CGPoint {
        let itemWidth = self.bounds.width / CGFloat(items?.count ?? 0)
        let startXPoint = (itemWidth * CGFloat(selectedIndex)) + (itemWidth / 2)
        return CGPoint(x: startXPoint - 41, y: 0)
    }
    
    private var diameter: CGFloat = 72
//    private var diameter: CGFloat = 100
    
    override public func draw(_ rect: CGRect) {
        super.draw(rect)
        self.setupTabBar()
    }
    
    func setupTabBar() {
        self.isTranslucent = true
        self.backgroundColor = UIColor.clear
        self.backgroundImage = UIImage()
        self.shadowImage = UIImage()
        self.clipsToBounds = false
        
        tabBarCurveShapeLayer.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        //tabBarCurveShapeLayer.fillColor = UIColor.white.cgColor
//        tabBarCurveShapeLayer.fillColor = UIColor.red.cgColor
        tabBarCurveShapeLayer.fillColor = UIColor(hexString: ColorCode.tabInActiveColor)?.cgColor
//        tabBarCurveShapeLayer.strokeColor = UIColor.white.cgColor
        tabBarCurveShapeLayer.strokeColor = UIColor(hexString: ColorCode.tabInActiveColor)?.cgColor
        tabBarCurveShapeLayer.path = createPathForTabBar().cgPath
        self.layer.insertSublayer(tabBarCurveShapeLayer, at: 0)
        
    }
    
//    private func createPathForTabBar() -> UIBezierPath {
//        let path = UIBezierPath()
//        path.move(to: CGPoint(x: 0, y: 0))
//        path.addLine(to: startPoint)
//
//        let leftCurveControlPoint1 = CGPoint(x: startPoint.x + 6, y: 0)
//        let leftCurveControlPoint2 = CGPoint(x: startPoint.x + 10, y: 5)
//        path.addCurve(to: CGPoint(x: startPoint.x + 10, y: 10), controlPoint1: leftCurveControlPoint1, controlPoint2: leftCurveControlPoint2)
//
//        //let middleCurveControlPoint1 = CGPoint(x: startPoint.x + 10, y: 60)
//        //        let middleCurveControlPoint2 = CGPoint(x: startPoint.x + diameter, y: 60)
//
//        let middleCurveControlPoint1 = CGPoint(x: startPoint.x + 10, y: 80)
//        let middleCurveControlPoint2 = CGPoint(x: startPoint.x + diameter, y: 80)
//        path.addCurve(to: CGPoint(x: startPoint.x + diameter, y: 10), controlPoint1: middleCurveControlPoint1, controlPoint2: middleCurveControlPoint2)
//
//        let rightCurveControlPoint1 = CGPoint(x: startPoint.x + diameter, y: 5)
//        let rightCurveControlPoint2 = CGPoint(x: startPoint.x + diameter + 5, y: 0)
//        path.addCurve(to: CGPoint(x: startPoint.x + diameter + 10, y: 0), controlPoint1: rightCurveControlPoint1, controlPoint2: rightCurveControlPoint2)
//
//        path.addLine(to: CGPoint(x: self.bounds.width, y: 0))
//        path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
//        path.addLine(to: CGPoint(x: 0, y: self.bounds.height))
//        path.addLine(to: CGPoint(x: 0, y: 0))
//
//        return path
//
//    }
    
    private func createPathForTabBar() -> UIBezierPath {
//        let path = UIBezierPath()
//        path.move(to: CGPoint(x: 0, y: 0))
//        path.addLine(to: startPoint)
//
//        let leftCurveControlPoint1 = CGPoint(x: startPoint.x + 6, y: 0)
//        let leftCurveControlPoint2 = CGPoint(x: startPoint.x + 10, y: 0)
//        path.addCurve(to: CGPoint(x: startPoint.x + 5, y: 10), controlPoint1: leftCurveControlPoint1, controlPoint2: leftCurveControlPoint2)
//
//        let middleCurveControlPoint1 = CGPoint(x: startPoint.x-30, y: 75)
//        let middleCurveControlPoint2 = CGPoint(x: startPoint.x + diameter + 40, y: 75)
//        path.addCurve(to: CGPoint(x: startPoint.x + diameter, y: 10), controlPoint1: middleCurveControlPoint1, controlPoint2: middleCurveControlPoint2)
//
//
//        let rightCurveControlPoint1 = CGPoint(x: startPoint.x + diameter - 3, y: 0)
//        let rightCurveControlPoint2 = CGPoint(x: startPoint.x + diameter - 8, y: 0)
//        path.addCurve(to: CGPoint(x: startPoint.x + diameter + 10, y: 0), controlPoint1: rightCurveControlPoint1, controlPoint2: rightCurveControlPoint2)
//
//        path.addLine(to: CGPoint(x: self.bounds.width, y: 0))
//        path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
//        path.addLine(to: CGPoint(x: 0, y: self.bounds.height))
//        path.addLine(to: CGPoint(x: 0, y: 0))
        
        // return path
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: startPoint.x, y: startPoint.y))
        path.addCurve(to: CGPoint(x: startPoint.x + 10.43, y: startPoint.y + 18.95), controlPoint1: CGPoint(x: startPoint.x + 6.48, y: startPoint.y + 1.27), controlPoint2: CGPoint(x: startPoint.x + 9.96, y: startPoint.y + 7.59))
        path.addCurve(to: CGPoint(x: startPoint.x + 25.42, y: startPoint.y + 51.92), controlPoint1: CGPoint(x: startPoint.x + 11.78, y: startPoint.y + 36), controlPoint2: CGPoint(x: startPoint.x + 13.43, y: startPoint.y + 51.92))
        path.addCurve(to: CGPoint(x: startPoint.x + 59.17, y: startPoint.y + 51.92), controlPoint1: CGPoint(x: startPoint.x + 26.1, y: startPoint.y + 51.92), controlPoint2: CGPoint(x: startPoint.x + 37.35, y: startPoint.y + 51.92))
        path.addCurve(to: CGPoint(x: startPoint.x + 72.68, y: startPoint.y + 33), controlPoint1: CGPoint(x: startPoint.x + 66.73, y: startPoint.y + 52.64), controlPoint2: CGPoint(x: startPoint.x + 71.23, y: startPoint.y + 46.33))
        path.addCurve(to: CGPoint(x: startPoint.x + 84, y: startPoint.y + 0), controlPoint1: CGPoint(x: startPoint.x + 74.85, y: startPoint.y + 13), controlPoint2: CGPoint(x: startPoint.x + 75.82, y: startPoint.y + 3.44))
        
        path.addLine(to: CGPoint(x: self.bounds.width, y: 0))
        path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
        path.addLine(to: CGPoint(x: 0, y: self.bounds.height))
        path.addLine(to: CGPoint(x: 0, y: 0))
        
        return path
        
    }
    
    func curveAnimation(for index: Int) {
        self.selectedIndex = index
        curveAnimation()
    }
    
    private func curveAnimation() {
        let pathAnimation = CASpringAnimation(keyPath: "path")
        pathAnimation.damping = 100
        pathAnimation.toValue = createPathForTabBar().cgPath
        pathAnimation.duration = 0.9
        pathAnimation.fillMode = .forwards
        pathAnimation.isRemovedOnCompletion = false
        pathAnimation.autoreverses = false
        pathAnimation.repeatCount = 0
        tabBarCurveShapeLayer.add(pathAnimation, forKey: "pathAnimation")
    }
    
    func finishAnimation() {
        tabBarCurveShapeLayer.path = createPathForTabBar().cgPath
    }

}

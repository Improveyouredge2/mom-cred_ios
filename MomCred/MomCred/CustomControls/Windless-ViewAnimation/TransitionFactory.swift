//
//  TransitionFactory.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import UIKit

class TransitionFactory {
    
    static func fade() -> CATransition {
        let transition = CATransition()
        transition.type = CATransitionType.fade
        return transition
    }
}

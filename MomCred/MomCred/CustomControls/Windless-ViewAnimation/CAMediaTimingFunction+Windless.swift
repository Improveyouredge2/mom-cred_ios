//
//  CAMediaTimingFunction+Windless.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import UIKit

public extension CAMediaTimingFunction {
    
    static let linear = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
    
    static let easeIn = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
    
    static let easeOut = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
    
    static let easeInOut = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
}

//
//  ViewLayerSetup.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit

class ViewLayerSetup: UIView{
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        didSet {
            layer.shadowColor = shadowColor?.cgColor
        }
    }
    
    @IBInspectable var masksToBounds: Bool = false{
        didSet {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable var shadowOffset: CGSize = CGSize.zero{
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    
    // Add new variable
    @IBInspectable var isAllowCircular: Bool = false
    
    override func layoutSubviews() {
        if isAllowCircular {
            super.layoutSubviews()
            let radius: CGFloat = self.bounds.size.width / 2.0
            self.layer.cornerRadius = radius
            self.clipsToBounds = true
        }
    }
}

class GradientView: UIView {
    
    @IBInspectable var roundedCorner: Bool = false {
        didSet {
            if roundedCorner == true {
                layer.cornerRadius = (self.bounds.height/2.0)
                self.clipsToBounds = true
            }
        }
    }
    
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //1264B0, 05B9ED
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [ #colorLiteral(red: 0.01960784314, green: 0.7254901961, blue: 0.9294117647, alpha: 1).cgColor , #colorLiteral(red: 0.07058823529, green: 0.3921568627, blue: 0.6901960784, alpha: 1).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5);
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5);
    }
}

class GradientBackgroundView: UIView {

    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //1264B0, 05B9ED
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [ #colorLiteral(red: 0.09411764706, green: 0.6117647059, blue: 0.8901960784, alpha: 1).cgColor , #colorLiteral(red: 0.09411764706, green: 0.4392156863, blue: 0.8901960784, alpha: 1).cgColor]
//        gradientLayer.startPoint = CGPoint(x: 0.2, y: 0.0);
//        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0);
        
        gradientLayer.locations = [0.0, 1.0]
    }
}

class ButtonLayerSetup: UIButton{
    
    override var intrinsicContentSize: CGSize {
        let labelSize = titleLabel?.sizeThatFits(CGSize(width: frame.size.width, height: CGFloat.greatestFiniteMagnitude)) ?? .zero
        let desiredButtonSize = CGSize(width: labelSize.width + titleEdgeInsets.left + titleEdgeInsets.right, height: labelSize.height + titleEdgeInsets.top + titleEdgeInsets.bottom)
        
        return desiredButtonSize
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        didSet {
            layer.shadowColor = shadowColor?.cgColor
        }
    }
    
    @IBInspectable var masksToBounds: Bool = false{
        didSet {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable var shadowOffset: CGSize = CGSize.zero{
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    @IBInspectable var multiLineText: Bool = false{
        didSet {
            if(multiLineText){
                titleLabel?.numberOfLines = 0
            } else {
                titleLabel?.numberOfLines = 1
            }
        }
    }
    
    @IBInspectable var alignmentCenter: Bool = false{
        didSet {
            if(alignmentCenter){
                titleLabel?.textAlignment = .center
            } else {
                titleLabel?.textAlignment = .left
            }
        }
    }
    
        @IBInspectable var imageHighlightColor: UIColor? {
            didSet {
                if(imageHighlightColor != nil){
                    self.setImageColorForState(image: UIImage(color: imageHighlightColor!)!, color: imageHighlightColor!, forState: UIControl.State.highlighted)
                }
            }
        }
}

class ButtonConfiguration: UIButton{
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let radius: CGFloat = self.bounds.size.height*0.50
        self.layer.cornerRadius = radius
        layer.masksToBounds = radius > 0
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.lightGray.cgColor
        self.clipsToBounds = true
      //  self.titleLabel?.textColor  = UIColor(hexString: ColorCode.buttontextColor)
        self.setTitleColor(.white, for: .highlighted)
        self.setTitleColor(.white, for: .selected)
        self.setTitleColor(.gray, for: .normal)
    }
    
//    @IBInspectable var borderColor : UIColor = UIColor.lightGray {
//        didSet {
//            self.setNeedsDisplay()
//        }
//    }
    
    override var isHighlighted: Bool{
        didSet{
            print("isHighlighted with status: \(isHighlighted)")
            if isHighlighted{
                layer.borderWidth = 0
                // self.borderColor = UIColor(hexString: ColorCode.themeColor)!
                //  layer.borderColor = UIColor(hexString: ColorCode.themeColor)?.cgColor
                layer.borderColor = UIColor.clear.cgColor
                layer.backgroundColor = UIColor(hexString: ColorCode.themeColor)?.cgColor
                self.titleLabel?.textColor = .white
    
            }else{
                
                layer.borderWidth = 1.0
                // self.borderColor = UIColor.lightGray
                layer.borderColor = UIColor.lightGray.cgColor
                layer.backgroundColor = UIColor.white.cgColor
                self.titleLabel?.textColor  = UIColor(hexString: ColorCode.buttontextColor)
            }
        }
    }
    
    //
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        layer.borderColor = UIColor.clear.cgColor
    //        layer.backgroundColor = UIColor(hexString: ColorCode.themeColor)?.cgColor
    //        self.titleLabel?.textColor = .white
    //
    //    }
    //
    //    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        layer.borderColor = borderColor.cgColor
    //        layer.backgroundColor = UIColor.white.cgColor
    //        self.titleLabel?.textColor = .gray
    //    }
    
    
    //    @IBInspectable var imageHighlightColor: UIColor? {
    //        didSet {
    //            self.setImageColorForState(image: (self.imageView?.image!)!, color: imageHighlightColor!, forState: UIControlState.highlighted)
    //        }
    //    }
}

class LabelLayerSetup: UILabel{
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var masksToBounds: Bool = false{
        didSet {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
//        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
    override var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }
}


class ImageLayerSetup: UIImageView{
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    //    @IBInspectable var shouldRasterize: Bool = false{
    //        didSet{
    //            layer.shouldRasterize = shouldRasterize
    //        }
    //    }
    
    @IBInspectable var shadowRadius: CGFloat = 0{
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0{
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        didSet {
            layer.shadowColor = shadowColor?.cgColor
        }
    }
    
    @IBInspectable var masksToBounds: Bool = false{
        didSet {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable var shadowOffset: CGSize = CGSize.zero{
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    // Add new variable
    @IBInspectable var isAllowCircular: Bool = false
    
    override func layoutSubviews() {
        if isAllowCircular {
            super.layoutSubviews()
            let radius: CGFloat = self.bounds.size.width / 2.0
            self.layer.cornerRadius = radius
            self.clipsToBounds = true
        }
    }
}

@IBDesignable class IQTextView: UITextView {
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    func setup() {
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
    }
}


// its will use where we need rounded UIButton
class RoundedButton: UIButton {
    
    // for rounded
    @IBInspectable var cornerRadius: CGFloat = 0.0
    @IBInspectable var circular: Bool = true
    
    // for Shadow
    @IBInspectable var isShadow: Bool = false
    @IBInspectable var shadowOffsetWidth: Float = 0.0
    @IBInspectable var shadowOffsetHeight: Float = 0.0
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    // for Border
    @IBInspectable var isBorder: Bool = false
    @IBInspectable var borderWidth: Float = 0.5
    @IBInspectable var borderColor: UIColor? = UIColor.black
    
    @IBInspectable var isLeftBorder: Bool = false
    @IBInspectable var isRightBorder: Bool = false
    @IBInspectable var isTopBorder: Bool = false
    @IBInspectable var isBottomBorder: Bool = false
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        cornerRadius = (circular == true ? self.bounds.size.height / 2.0 : cornerRadius)
        
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        
        if isShadow {
            let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
            layer.shadowColor = shadowColor?.cgColor
            layer.shadowOffset = CGSize(width: CGFloat(shadowOffsetWidth), height: CGFloat(shadowOffsetHeight));
            layer.shadowOpacity = shadowOpacity
            layer.shadowPath = shadowPath.cgPath
        }
        
        if isBorder {
            self.layer.borderWidth = CGFloat(borderWidth)
            self.layer.borderColor = borderColor?.cgColor
        }
        
        layer.masksToBounds = false
        
    }
}

class UIViewWithDashedLineBorder: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 15)
        
        UIColor.clear.setFill()
        path.fill()
        
        UIColor.lightGray.setStroke()
        path.lineWidth = 1
        
        let dashPattern : [CGFloat] = [8, 12]
        path.setLineDash(dashPattern, count: 1, phase: 0)
        path.stroke()
    }
}

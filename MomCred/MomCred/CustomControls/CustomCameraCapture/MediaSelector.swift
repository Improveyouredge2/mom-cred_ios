//
//  MediaSelector.swift
//  MomCred
//
//  Created by MD on 19/08/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import Photos
import MobileCoreServices
import TOCropViewController

protocol MediaSelectorDelegate: class {
    func mediaSelected(name: String, type: String, data: Data, thumb: UIImage, url: URL?)
    func cancelMediaSelector()
}

class MediaSelector: NSObject {
    private struct Constants {
        static let thumbSize: CGSize = .init(width: 500, height: 500)
        static let defaultImageSize: Int = 1048576 // 1 MB
        static let cacheDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first ?? NSHomeDirectory().appending("/Documents/Library/Caches/")
    }
    
    static let shared: MediaSelector = .init()
    
    private weak var delegate: MediaSelectorDelegate?
    private var requiredImageSize: CGSize = .zero
    private var cropType: TOCropViewCroppingStyle = .default
    private var shouldCrop: Bool = false
    private var presentingViewController: UIViewController?
    
    func showSelector(on viewController: UIViewController, delegate: MediaSelectorDelegate, mediaTypes: [String], sourceType: UIImagePickerController.SourceType, shouldCrop: Bool, requiredImageSize: CGSize = .zero, cropType: TOCropViewCroppingStyle = .default, videoDuration: TimeInterval = 60) {

        self.presentingViewController = viewController
        self.delegate = delegate
        self.requiredImageSize = requiredImageSize
        self.shouldCrop = shouldCrop
        self.cropType = cropType
        
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            let imagePicker: UIImagePickerController = .init()
            imagePicker.delegate = self
            imagePicker.mediaTypes = mediaTypes
            imagePicker.sourceType = sourceType
            
            if mediaTypes.contains(kUTTypeMovie as String) {
                imagePicker.allowsEditing = true
                imagePicker.videoMaximumDuration = videoDuration
            }
            
            viewController.present(imagePicker, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "Error", message: "Selected source is unavailable. Please try again later.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            viewController.present(alertController, animated: true, completion: nil)
        }
    }
    
    func thumbnailForVideo(at url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let assetImgGenerate: AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let totalDuration: Double = Double(asset.duration.value) / Double(asset.duration.timescale)
        let percentageDuration: Double = totalDuration * 0.1
        let thumbAtTime: Double
        // generate thumbnail at 5 sec
        if percentageDuration < 5 {
            thumbAtTime = percentageDuration
        } else {
            thumbAtTime = 5
        }

        let time: CMTime = CMTime(seconds: thumbAtTime, preferredTimescale: 100)
        do {
            let img: CGImage = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail: UIImage = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print("unable to create thumbnail for video at: \(url)")
        }
        return nil
    }
    
    func scaleImage(_ image: UIImage, toWidth: CGFloat) -> UIImage {
        if toWidth <= image.size.width { return image }
        let scaleFactor: CGFloat = toWidth / image.size.width
        let updatedSize = CGSize(width: toWidth, height: image.size.height * scaleFactor)
        return scaleImage(image, toSize: updatedSize)
    }
    
    func scaleImage(_ image: UIImage, toHeight: CGFloat) -> UIImage {
        if toHeight <= image.size.height { return image }
        let scaleFactor: CGFloat = toHeight / image.size.height
        let updatedSize = CGSize(width: image.size.width * scaleFactor, height: toHeight)
        return scaleImage(image, toSize: updatedSize)
    }
    
    private func scaleImage(_ image: UIImage, toSize size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        image.draw(in: CGRect(origin: .zero, size: size))
        
        let updatedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return updatedImage ?? image
    }
    
    private func uniqueFilePath(_ prefix: String, extension extn: String) -> String {
        let dir = Constants.cacheDirectory
        var fileCount: Int = 0
        var filePath: String = "\(dir)/\(prefix)_\(fileCount).\(extn)"
        let fileManager =  FileManager.default
        while fileManager.fileExists(atPath: filePath) {
            fileCount += 1
            filePath = "\(dir)/\(prefix)_\(fileCount).\(extn)"
        }
        return filePath
    }
    
    private func resizedImage(_ image: UIImage) -> UIImage {
        var updatedImage = image
        if image.size.width > image.size.height {
            let width = requiredImageSize.width > 0 ? requiredImageSize.width : Constants.thumbSize.width
            updatedImage = scaleImage(image, toWidth: width)
        } else {
            let height = requiredImageSize.height > 0 ? requiredImageSize.height : Constants.thumbSize.height
            updatedImage = scaleImage(image, toHeight: height)
        }
        
        var idata: Data? = updatedImage.jpegData(compressionQuality: 1.0)
        var resized: UIImage = updatedImage
        var currentLength: Int = idata?.count ?? 0
        while currentLength > Constants.defaultImageSize {
            idata = resized.jpegData(compressionQuality: 0.9)
            let afterLength: Int = idata?.count ?? 0
            if let data: Data = idata, let img: UIImage = UIImage(data: data) {
                resized = img
            }
            if afterLength == currentLength { break }
            currentLength = afterLength
        }

        return resized
    }
    
    private func resizeImage(_ image: UIImage) {
        let selectedImage = resizedImage(image)
        let idata: Data? = selectedImage.jpegData(compressionQuality: 1.0)
        if let imageData = idata {
            delegate?.mediaSelected(name: "temp_image.jpeg", type: "1", data: imageData, thumb: selectedImage, url: nil)
        }
    }
}

extension MediaSelector: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        presentingViewController?.dismiss(animated: true) { [weak self] in
            self?.delegate?.cancelMediaSelector()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var cropViewImage: UIImage? = nil
        if let mediaType = info[.mediaType] as? String {
            if mediaType == (kUTTypeMovie as String) {
                guard let mediaURL: URL = info[UIImagePickerController.InfoKey.mediaURL] as? URL else { return }
                
                if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(mediaURL.path) {
                    let avAsset: AVURLAsset = AVURLAsset(url: mediaURL, options: nil)
                    if let exportSession: AVAssetExportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetMediumQuality) {
                        if let cachesDirectoryURL: URL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first {
                            var fileCount: Int = 0
                            let fileName: String = "converted-video_\(fileCount).mp4"
                            var fileURL: URL = cachesDirectoryURL.appendingPathComponent(fileName)
                            
                            while FileManager.default.fileExists(atPath: fileURL.path) {
                                fileCount += 1
                                fileURL = cachesDirectoryURL.appendingPathComponent("converted-video_\(fileCount).mp4")
                            }
                            
                            exportSession.outputURL = fileURL
                            exportSession.outputFileType = .mp4
                            exportSession.shouldOptimizeForNetworkUse = true
                            let start: CMTime = CMTime(seconds: 0.0, preferredTimescale: 0)
                            let range: CMTimeRange = CMTimeRange(start: start, duration: avAsset.duration)
                            exportSession.timeRange = range
                            
                            Spinner.show("Resizing video ...")
                            exportSession.exportAsynchronously { [weak self, weak exportSession] in
                                guard let self = self, let outputURL = exportSession?.outputURL else { return }
                                guard let thumbnail = self.thumbnailForVideo(at: outputURL) else { return }
                                let videoThumbnail: UIImage = self.resizedImage(thumbnail)
                                guard let videoThumbnailData: Data = videoThumbnail.jpegData(compressionQuality: 1.0) else { return }

                                let thumbnailURL: URL = URL(fileURLWithPath: self.uniqueFilePath("temp_movie_thumb", extension: "jpg"))
                                try? videoThumbnailData.write(to: thumbnailURL)
                                
                                guard let videoData: Data = try? Data(contentsOf: outputURL) else { return }
                                DispatchQueue.main.async { [weak self] in
                                    self?.delegate?.mediaSelected(name: outputURL.lastPathComponent, type: "2", data: videoData, thumb: videoThumbnail, url: nil)
                                    // delete the converted file data
                                    try? FileManager.default.removeItem(at: outputURL)
                                    Spinner.hide()
                                }
                            }
                        }
                    }
                }
            } else if mediaType == (kUTTypeImage as String) {
                var image: UIImage? = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
                if image == nil {
                    image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                }

                guard let sImage: UIImage = image else { return }
                if shouldCrop {
                    cropViewImage = sImage
                } else {
                    resizeImage(sImage)
                }
            }
        }
        
        presentingViewController?.dismiss(animated: true, completion: { [weak self] in
            guard let self = self else { return }
            if let sImage = cropViewImage {
                let cropController = TOCropViewController(croppingStyle: self.cropType, image: sImage)
                cropController.delegate = self
                cropController.rotateButtonsHidden = true
                cropController.rotateClockwiseButtonHidden = true
                self.presentingViewController?.present(cropController, animated: true, completion: nil)
            }
        })
    }
}

extension MediaSelector: TOCropViewControllerDelegate {
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
        presentingViewController?.dismiss(animated: true, completion: nil)
        resizeImage(image)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropToCircularImage image: UIImage, with cropRect: CGRect, angle: Int) {
        presentingViewController?.dismiss(animated: true, completion: nil)
        resizeImage(image)
    }
}

import UIKit

extension UITextField {
    func updateInputType(_ type: FormTextFieldInputType) {
        switch type {
        case .name:
            autocapitalizationType = .words
            autocorrectionType = .no
            break

        case .username:
            autocapitalizationType = .none
            autocorrectionType = .no
            keyboardType = .namePhonePad
            break

        case .phoneNumber:
            autocapitalizationType = .none
            autocorrectionType = .no
            keyboardType = .phonePad
            break

        case .integer:
            autocapitalizationType = .none
            autocorrectionType = .no
            keyboardType = .phonePad
            break

        case .decimal:
            autocapitalizationType = .none
            autocorrectionType = .no
            keyboardType = .numberPad
            break

        case .address:
            autocapitalizationType = .words
            keyboardType = .asciiCapable
            break

        case .email:
            autocapitalizationType = .none
            autocorrectionType = .no
            keyboardType = .emailAddress
            break

        case .password:
            autocapitalizationType = .none
            autocorrectionType = .no
            keyboardType = .asciiCapable
            isSecureTextEntry = true
            break
        default: break
        }
    }
    
    //MARK: Validate as numeric allowing decimal
    func validatePriceField(string: String, text: String, maxLength:Int, range: NSRange) -> Bool {
        let currentCharacterCount = text.count
        let newLength = currentCharacterCount + string.count - range.length
        let decimalSeparator = NSLocale.current.decimalSeparator
        
        switch string {
        case "0","1","2","3","4","5","6","7","8","9":
            if text.contains("\(decimalSeparator ?? ".")"){
                var token = text.components(separatedBy: "\(decimalSeparator ?? ".")")
                return token[1].length <= 1
            }else{
                return newLength <= maxLength
            }
            
        case "०","१","२","३","४","५","६","७","८","९":
            return false
            
        case decimalSeparator:
            let countdots = text.components(separatedBy: "\(decimalSeparator ?? ".")").count - 1
            if countdots == 0 {
                let updatedLength = maxLength + 3
                return newLength <= updatedLength
            }else{
                if countdots > 0 && string == "\(decimalSeparator ?? ".")" {
                    return false
                } else {
                    let updatedLength = maxLength + 3
                    return newLength <= updatedLength
                }
            }
            
        default:
            let array = Array(string)
            if array.count == 0 {
                if newLength >= 6{
                    return true
                }else{
                    return newLength <= maxLength
                }
            }
            return false
        }
    }
}


extension UITextField {
    
    func shouldShowClearButton(show : Bool = true){
        if show {
            if let clearButton = self.value(forKey: "_clearButton") as? UIButton {
                // Create a template copy of the original button image
                clearButton.setImage(UIImage(named: "error-1"), for: .normal)
                clearButton.setImage(#imageLiteral(resourceName: "error-1"), for: .highlighted)
                self.clearButtonMode = .whileEditing
            }
        }

    }}

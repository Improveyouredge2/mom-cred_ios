//
//  APIManager.swift
//  SearchPublicProfile
//
//  Created by Mahesh Dhakad on 16/01/19.
//  Copyright © 2019 CONSAGOUS. All rights reserved.
//

import Foundation
import MobileCoreServices
import Alamofire

class APIManager  {
    
    
    //POST
    //let req = APIManager().sendPostRequest(urlString:String?,header:[String:String]?, postData:Data?)
    //GET
    //let req = APIManager().sendGetRequest(urlString:String?,header:[String:String]?, postData:Data?)
    
    //PostMultiForm
    //let req = APIManager().sendPostMultiFormRequest(urlString:String,header:[String:String]?, postData:Data?,formDataParameter:[String:Any]?,uploadImageList:[UploadImageData]?)
    
    static func request(urlRequest:URLRequest?, isDisplayLoader:Bool = false,callback:@escaping (Bool, NSDictionary?, String?) -> Void) {
        
        if(Helper.sharedInstance.isNetworkConnected){
            if urlRequest != nil {
                
                let session = URLSession.shared
                
                
                //TODO: update session default timeout of API
                let sessionConfig = URLSessionConfiguration.default
                sessionConfig.timeoutIntervalForRequest = 100.0
                sessionConfig.timeoutIntervalForResource = 190.0
                
//                let session = URLSession(configuration: sessionConfig, delegate: Request.sharedInstance, delegateQueue: nil)
                
                if isDisplayLoader {
                    DispatchQueue.main.async {
                        Spinner.show()
                    }
                }
                
                session.dataTask(with: urlRequest!) {data, response, error in
                    
                    if error != nil {
                        print(error!.localizedDescription)
                        // Parse error
                        let responseError = error! as NSError
                        var errorMessage:String?;
                        if(APIManager().NetworkErrors(responseError.code)) {
                            errorMessage = "\(HelperConstant.DEFAULT_REST_ERROR_MESSAGE)"
                        }else{
                            errorMessage = responseError.localizedDescription
                        }
                        
                        callback(false, nil, errorMessage!) // callback(failed, data, errorMessage)
                    }
                    
                    if(data == nil){
                        callback(false, nil, "error in parsing") // callback(failed,
                        return
                    }
                    
                    do {
                        let jsonResult: NSDictionary? = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        print("API Response - \(jsonResult!)")
                        guard (jsonResult?.value(forKey: "status") as! NSNumber).intValue < 0  else {
                            
                            let reqResponse = APIResponse().getModelObjectFromServerResponse(response: jsonResult! as AnyObject)
                            
                            if(reqResponse != nil && reqResponse?.status?.intValue == APIResponseStatus.kResponseStatusOK){
                                
                                callback(true, jsonResult, "") // callback(failed, data, errorMessage)
                                
                            } else {
                                
                                if(reqResponse?.message != nil){
                                    if(reqResponse?.error_code != nil && reqResponse?.error_code?.intValue == APIStatusCode.VERSIONUPDATE.rawValue){
                                        Helper.sharedInstance.showVersionUpdateAvailableAlertController()
                                    } else if(reqResponse?.error_code != nil && reqResponse?.error_code?.intValue == APIStatusCode.TOKEN_EXPIRE.rawValue){
                                        Helper.sharedInstance.showTokenExpireAlertController(reqResponse?.message ?? "")
                                    } else if(reqResponse?.error_code != nil && reqResponse?.error_code?.intValue == APIStatusCode.VERSION_NO_CHANGE.rawValue){
                                        Helper.sharedInstance.versionNoChange()
                                    }  else{
                                        callback(false, nil, (reqResponse?.message)!) // callback(success, data, errorMessage)
                                    }
                                    
                                }else{
                                    callback(false, nil, response.debugDescription) // callback(success, data, errorMessage)
                                }
                            }
                            
                            return
                        }
                        
                    } catch {
                        print(error.localizedDescription)
                        
                        let errorStr = String(data: data!, encoding: String.Encoding.utf8)
                        callback(false, nil, errorStr!) // callback(failed, data, errorMessage)
                    }
                    }.resume()
            }
        } else {
            Helper.sharedInstance.showNetworkNotAvailableAlertController()
        }
    }
}

// POST/GET request with URLRequest
extension APIManager {
    
    func sendPostRequest(urlString:String?,header:[String:String]?, strJSON:String?) -> URLRequest? {
        
        var request = URLRequest(url: URL(string: BASEURLs.baseURL + urlString!)!)
        
        request.allHTTPHeaderFields = header
        
        request.httpMethod = "POST"
        
        let postData = strJSON?.data(using: String.Encoding.utf8)!
        request.httpBody = postData
       
        print("\nREQUEST URLs :- \n\(request.urlRequest?.url?.absoluteString ?? "")")
         print("REQUEST Parameters:- \n\(strJSON ?? "")")
         print("\nREQUEST Header:- \n\(header ?? [:])\n")
        
        return request
    }
    
    func sendGetRequest(urlString:String?,header:[String:String]?) -> URLRequest?{
        
        var request = URLRequest(url: URL(string: BASEURLs.baseURL + urlString!)!)
        
        request.allHTTPHeaderFields = header
        
        request.httpMethod = "GET"
        print("\nRequest with url :- \n\(request.urlRequest?.url?.absoluteString ?? "")\n")
        print("\nRequest with Header :- \n\(header ?? [:])\n")
        
        return request
    }
    
    func sendPostMultiFormRequest(urlString:String,header:[String:String]?,formDataParameter:[String:Any]?,uploadImageList:[UploadImageData]?) -> URLRequest?{
        
        let request = URLRequest(url: URL(string: BASEURLs.baseURL + urlString)!)
         print("\nRequest with url :- \n\(request.urlRequest?.url?.absoluteString ?? "")")
        print("formDataParameter---------------\n\(formDataParameter ?? [:])")
        print("uploadImageList---------------\n\(uploadImageList ?? [])")
        
        do{
            let request = try createRequest(urlString: urlString,header:header, formDataParameter:formDataParameter, uploadImageList: uploadImageList)
            return request
        } catch {
            print(error.localizedDescription)
        }
   
        return nil
    }
}

// Upload multi-form data
extension APIManager {
    /// Create request
    ///
    /// - parameter userid:   The userid to be passed to web service
    /// - parameter password: The password to be passed to web service
    /// - parameter email:    The email address to be passed to web service
    ///
    /// - returns:            The NSURLRequest that was created
    
    func createRequest(urlString:String?,header:[String:String]?, formDataParameter:[String:Any]?, uploadImageList:[UploadImageData]?) throws -> URLRequest {
        let boundary = generateBoundaryString()
        
        let url = URL(string: BASEURLs.baseURL + urlString!)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        for (key, value) in header! {
            request.setValue(value, forHTTPHeaderField:key)
        }
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpBody = try createBody(with: formDataParameter , imageUploadList: uploadImageList, boundary: boundary)
        
        return request
    }
    
    /// Create body of the multipart/form-data request
    ///
    /// - parameter parameters:   The optional dictionary containing keys and values to be passed to web service
    /// - parameter filePathKey:  The optional field name to be used when uploading files. If you supply paths, you must supply filePathKey, too.
    /// - parameter paths:        The optional array of file paths of the files to be uploaded
    /// - parameter boundary:     The multipart/form-data boundary
    ///
    /// - returns:                The NSData of the body of the request
    
//    func createBody(with parameters: [String: Any]?, imageUploadList: [UploadImageData]?, boundary: String) throws -> Data {
//        let body = NSMutableData()
//        if parameters != nil {
//            for (key, value) in parameters! {
//                if value is [String] {
//                    let arrVal = value as! [String]
//                    for (val) in arrVal {
//                        body.appendString("--\(boundary)\r\n")
//                        body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//                        body.appendString("\(val)\r\n")
//                    }
//                } else {
//                    body.appendString("--\(boundary)\r\n")
//                    body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//                    body.appendString("\(value)\r\n")
//                }
//
//            }
//        }
//
//        if(imageUploadList != nil){
//            let mimetype = "image/png"
//            for uploadImageData in imageUploadList! {
//                body.appendString("--\(boundary)\r\n")
//                if uploadImageData.imageData !=  nil{
//                    body.appendString("Content-Disposition: form-data;name=\"\(uploadImageData.imageKeyName!)\";\r\nfilename=\"\(uploadImageData.imageName!)\"\r\n")
//                    body.appendString("Content-Type: \(mimetype)\r\n\r\n")
//                    body.append(uploadImageData.imageData!)
//                    body.appendString("\r\n")
//
//                }else{
//                    body.appendString("Content-Disposition: form-data;name=\"\(uploadImageData.imageKeyName!)\";\r\nfilename=\"\(uploadImageData.imageName!)\"\r\n")
//                    body.appendString("Content-Type: \(mimetype)\r\n\r\n")
//                    body.append("1".data(using: String.Encoding.utf8)!)
//                    body.appendString("\r\n")
//
//                }
//            }
//        }
//
//        body.appendString("--\(boundary)--\r\n")
//
//        return body as Data
//    }
    
    func convertIntoJSONString(arrayObject: [Any]) -> String? {
        
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
            
        } catch let error as NSError {
            print("Array convertIntoJSON - \(error.description)")
        }
        return nil
    }
    
    func convertDicIntoJSONString(object: [String:Any]) -> String? {
        
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: object, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
            
        } catch let error as NSError {
            print("Array convertIntoJSON - \(error.description)")
        }
        return nil
    }
    
    func convertArrJsonDicIntoJSONString(arrayObject: [[String:Any]]) -> String? {
        
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
            
        } catch let error as NSError {
            print("Array convertIntoJSON - \(error.description)")
        }
        return nil
    }

    
    func createBody(with parameters: [String: Any]?, imageUploadList: [UploadImageData]?, boundary: String) throws -> Data {
        let body = NSMutableData()
        
        
        
        if parameters != nil {
            for (key, value) in parameters! {
//                if value is [String] {
//                    let arrVal = value as! [String]
//                    for (val) in arrVal {
//                        body.appendString("--\(boundary)\r\n")
//                        body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
//                        body.appendString("\(val)\r\n")
//                    }
//                }else
                    if value is [[String:Any]] { // Json object array
//                    let arrVal = value as! [[String:Any]]
//                    var testStr:String = ""
//                    for (val) in arrVal {
//                        if(testStr.length > 0){
//                            testStr.append(",\(val)")
//                        } else {
//                            testStr.append("\(val)")
//                        }
//                    }
//                    testStr = testStr.replacingOccurrences(of: "[", with: "{")
//                    testStr = testStr.replacingOccurrences(of: "]", with: "}")
//                    testStr = testStr.replacingOccurrences(of: "\\", with: "")
//                    testStr = "[\(testStr)]"
                    
                    body.appendString("--\(boundary)\r\n")
                    body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                        let testVal = self.convertArrJsonDicIntoJSONString(arrayObject: value as! [[String : Any]])
                        body.appendString(testVal ?? "")
                        
                        body.appendString("\r\n")
                    } else { // 
                        body.appendString("--\(boundary)\r\n")
                        body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                        
                        if(value is [String:Any]){
                            let testVal = self.convertDicIntoJSONString(object: value as! [String : Any])
                            body.appendString(testVal ?? "")
                            
                            body.appendString("\r\n")
                        } else{
                            
                            var testStr = "\(value)"
                            testStr = testStr.replacingOccurrences(of: "\\", with: "")
                            body.appendString("\(testStr)\r\n")
                        }
                }
                
            }
        }

        if(imageUploadList != nil){
            let mimetype = "image/png"
            for uploadImageData in imageUploadList! {
                body.appendString("--\(boundary)\r\n")
                if uploadImageData.imageData !=  nil{
                    body.appendString("Content-Disposition: form-data;name=\"\(uploadImageData.imageKeyName!)\";\r\nfilename=\"\(uploadImageData.imageName!)\"\r\n")
                    
                    body.appendString("Content-Type: \(mimetype)\r\n\r\n")
                    body.append(uploadImageData.imageData!)
                    body.appendString("\r\n")
                    
                }else if(uploadImageData.imageUrl == nil) {
                    body.appendString("Content-Disposition: form-data;name=\"\(uploadImageData.imageKeyName!)\";\r\nfilename=\"\(uploadImageData.imageName!)\"\r\n")
                    body.appendString("Content-Type: \(mimetype)\r\n\r\n")
                    body.append("1".data(using: String.Encoding.utf8)!)
                    body.appendString("\r\n")
                    
                }
            }
        }

        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
    }
    
    /// Create boundary string for multipart/form-data request
    ///
    /// - returns:            The boundary string that consists of "Boundary-" followed by a UUID string.
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}

//MARK:- check network releated error

extension APIManager {
    
    func NetworkErrors(_ code:Int)-> Bool{
        var isFound = false
        switch code {
        case Int(CFNetworkErrors.cfurlErrorNotConnectedToInternet.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfurlErrorTimedOut.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfurlErrorCancelled.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfurlErrorNetworkConnectionLost.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfurlErrorBadServerResponse.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfurlErrorUserAuthenticationRequired.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfurlErrorUserCancelledAuthentication.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfurlErrorDataNotAllowed.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfErrorHTTPAuthenticationTypeUnsupported.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfErrorHTTPBadCredentials.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfErrorHTTPConnectionLost.rawValue):
            isFound = true
            break
        case Int(CFNetworkErrors.cfErrorHTTPBadURL.rawValue):
            isFound = true
            break
        case Int(EPERM):        //       1      / Operation not permitted
            isFound = true
            break
        case Int(ENOENT):       //       2      / No such file or directory
            isFound = true
            break
        case Int(ESRCH):        //       3      / No such process
            isFound = true
            break
        case Int(EINTR):        //       4      / Interrupted system call
            isFound = true
            break
        case Int(EIO):          //       5      / Input/output error
            isFound = true
            break
        case Int(ENXIO):        //       6      / Device not configured /
            isFound = true
            break
        case Int(E2BIG):        //       7      / Argument list too long /
            isFound = true
            break
        case Int(ENOEXEC):      //      8       / Exec format error /
            isFound = true
            break
        case Int(EBADF):        //      9       /Bad file descriptor
            isFound = true
            break
        case Int(ECHILD):       //      10      / No child processes /
            isFound = true
            break
        case Int(EDEADLK):      //      11      / Resource deadlock avoided // 11 was EAGAIN /
            isFound = true
            break
        case Int(ENOMEM):       //      12      / Cannot allocate memory /
            isFound = true
            break
        case Int(EACCES):       //      13      / Permission denied /
            isFound = true
            break
        case Int(EFAULT):       //      14      / Bad address #H
            isFound = true
            break
        default:
            isFound = false
            break
        }
        return isFound
    }
}

/*************************/
//MARK: - SSL Certificate
///*************************/
class Request: NSObject, URLSessionDelegate {
    static let sharedInstance = Request()
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        //func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
        
        // Adapted from OWASP https://www.owasp.org/index.php/Certificate_and_Public_Key_Pinning#iOS
        
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                var secresult = SecTrustResultType.invalid
                let status = SecTrustEvaluate(serverTrust, &secresult)
                
                if(errSecSuccess == status) {
                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
                        let data = CFDataGetBytePtr(serverCertificateData);
                        let size = CFDataGetLength(serverCertificateData);
                        let cert1 = NSData(bytes: data, length: size)
//                        let file_der = Bundle.main.path(forResource: certFile, ofType: "cer")
                        
                        
                        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))
                        return
                        
                    }
                }
            }
        }
        
        // Pinning failed
        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
    }
    
}

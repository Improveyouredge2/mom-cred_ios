//
//  APIResponse.swift
//  SearchPublicProfile
//
//  Created by Mahesh Dhakad on 16/01/19.
//  Copyright © 2019 CONSAGOUS. All rights reserved.
//

import Foundation
import ObjectMapper

class APIResponse:Mappable {
    
        var status:NSNumber?
        var message:String? // String
        var token:String?
        var error_code:NSNumber?
        var error_line:NSNumber?
    
        
        required init(){
            
        }
        
    // Convert Json response to class object
    func getModelObjectFromServerResponse(response: AnyObject)->APIResponse?{
        var baseResponse:APIResponse?
        if response is NSDictionary{
            let resDict:NSDictionary? = (response as! NSDictionary)
            if resDict != nil{
                let mapper = Mapper<APIResponse>()
                if let modelObject = mapper.map(JSON: resDict as! [String : Any]){
                    baseResponse = modelObject
                }
            }
        }else if response is String{
            let resJSONString = response as? String
            let data = resJSONString?.data(using: String.Encoding.utf8)!
            
            if (resJSONString?.isEmpty)! { // On Success response is empty
                baseResponse = APIResponse()
            }
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<APIResponse>()
                    if let modelObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        baseResponse = modelObject
                    }
                }
            } catch let error as NSError {
                print(APIConstants().kAppLogString(error.localizedDescription))
            }
        }
        return baseResponse ?? nil
    }
        
        required init?(map: Map){
        }
        
        func mapping(map: Map){
            status      <- map["status"]
            message     <- map["message"]
            token       <- map["token"]
            error_code  <- map["error_code"]
            error_line  <- map["error_line"]
        }
    
}


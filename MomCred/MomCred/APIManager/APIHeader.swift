//
//  APIHeader.swift
//  SearchPublicProfile
//
//  Created by Mahesh Dhakad on 16/01/19.
//  Copyright © 2019 CONSAGOUS. All rights reserved.
//

import Foundation


class APIHeaders: NSObject {
    
    func toString(_ object: AnyObject?) -> String?{
        return String(format: "%@", object as! NSString)
    }
    
    func urlEncode(_ object: AnyObject?) -> String?{
        let string: String = toString(object)!
        return string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
    
    func urlEncodedString(_ dict: NSDictionary?, restUrl: String?, baseUrl:String? = nil) -> String{
        
        let parts: NSMutableArray = []
        if(dict != nil)
        {
            for (key,value) in dict!{
                let part = String(format: "%@=%@", urlEncode(key as AnyObject?)!,urlEncode(value as AnyObject?)!)
                parts.add(part)
            }
        }
        
        let queryString: NSString = (parts.componentsJoined(by: "&") as NSString)
        var urlString: NSString = ""
        
        if(baseUrl != nil){
            if(queryString.length > 0){
                urlString = NSString(format: "%@%@?%@",baseUrl!,restUrl!,queryString )
            }
            else{
                urlString = NSString(format: "%@%@",baseUrl!,restUrl! )
            }
        }else{
            if(queryString.length > 0){
                urlString = NSString(format: "%@%@?%@",baseUrl!,restUrl!,queryString )
            }
            else{
                urlString = NSString(format: "%@%@", baseUrl!, restUrl! )
            }
        }
        return  urlString as String
    }
    
    func getDefaultHeaders() -> [String : String]{
        
        let lang = HelperConstant.DefaultLang
        let token = PMUserDefault.getAppToken() ?? ""
        
        let headers : [String : String] = ["Content-Type":"application/json","Accept":"application/json","x-request-os":"ios"  , "timeZone" : "IST" , HeaderKeys.kLANGUAGE:lang , HeaderKeys.kAUTHORIZATION : token , HeaderKeys.kVERSION : APIConstants.API_VERSION, HeaderKeys.kFCM_TOKEN : "1234567890998873428343285623572365", HeaderKeys.kDEVICE_TYPE : "iOS", HeaderKeys.kDEVICE_ID: "111"]
        
        return headers;
    }
    
    func getDefaultImageHeaders() -> [String : String]{
        
        let lang = HelperConstant.DefaultLang
        let token = PMUserDefault.getAppToken() ?? ""
        
        let headers : [String : String] = ["content-type": "multipart/form-data",HeaderKeys.kLANGUAGE:lang , HeaderKeys.kAUTHORIZATION : token , HeaderKeys.kVERSION : APIConstants.API_VERSION, HeaderKeys.kDEVICE_TYPE : "iOS", HeaderKeys.kDEVICE_ID: "111" , "current_time_zone" : "IST" ]
        
        return headers;
    }
    
    //    func getDefaultHeadersWithoutContentType() -> NSDictionary{
    //        let headers: NSMutableDictionary = [:]
    //        headers.setValue(APIConstants.API_VERSION, forKey: HeaderKeys.kVERSION)
    //        return headers;
    //    }
    
    func getCustomHeaders(extraParameter:[String:String]) -> NSDictionary {
        let defaultHeaderValue = NSMutableDictionary(dictionary: getDefaultHeaders())
        
        for (key,value) in extraParameter{
            defaultHeaderValue.setValue(value, forKey: key)
        }
        
        return defaultHeaderValue
    }
    
    //MARK:- Get application http environment url dict
    class func getSelectedHttpEnvironment(_ env:String) -> NSDictionary {
        let dict = ["SERVER_END_POINT":env]
        return dict as NSDictionary
    }
    
}

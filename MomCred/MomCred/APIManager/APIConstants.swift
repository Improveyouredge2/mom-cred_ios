//
//  APIConstants.swift
//  SearchPublicProfile
//
//  Created by Mahesh Dhakad on 16/01/19.
//  Copyright © 2019 CONSAGOUS. All rights reserved.
//

import Foundation


struct APIConstants {
    
    internal static let API_VERSION = "1.0.0"
    internal static let APP_STORE_APP_ID = "1234567"
    
    internal static let APP_API_REQUEST_DATE_FORMAT =   "yyyy-MM-dd hh:mm a"
    internal static let APP_DATE_FORMAT =   "MMM dd, yyyy"
    
    func kAppLogString(_ message: String,
                          function: String = #function,
                          file: String = #file,
                          line: Int = #line) {
        print("Message \"\(message)\" (File: \(file), Function: \(function), Line: \(line))")
    }
}

struct BASEURLs {
    
//    static let production = "https://libraries.consagous.co.in/shoppingcart"
//    https://momcred.consagous.co.in/webservices/Users/register
    static let development = "https://momcred.consagous.co.in/webservices/Users/"
    static let qa = "https://momcredtest.consagous.co.in/webservices/Users/"
//    static let staging = "http://localhost:4003"
    static let baseURL = development
 //   static let baseURL = qa
//    static let baseURL = production
}

struct APIKeys {
    
    // Login
    internal static let API_MOBILE_VERIFICATION  = "mobile_verification"
    internal static let API_LOGIN  = "login"
    internal static let API_SIGNUP = "register"
    internal static let API_FORGOTPASS = "forgot_password"
    
    // Profile
    internal static let API_PROFILE_DETAIL  = "get_profile"
    internal static let API_PROFILE_UPDATE  = "profile_update"
    internal static let API_CHANGE_PASSWORD  = "changepassword"
    internal static let API_CONTACT_US  = "get_contact_detail"
    
    internal static let API_HELP_SUPPORT = "help-support"
    
    internal static let API_ADDITIONAL_INFO = "get_additional_info"
    internal static let API_ADDITIONAL_SERVICE = "get_additional_services"

    internal static let API_PRIVACY_POLICY = "get_privacy_policy"
    internal static let API_TERMS_CONDITION = "get_terms_condition"
    internal static let API_ABOUT_US = "get_about_us"
    internal static let API_CONTACT_US_WEB = "get_contact_us"
    
    internal static let API_INSTRUCTIONAL_SIGNUP_TERMS = "get_terms_of_use"
    internal static let API_INSTRUCTIONAL_SIGNUP_AGREEMENT = "get_service_provider_agreement"
    internal static let API_GET_BACKGROUND_CHECK_URL = "get_account_invitation"

    internal static let API_GET_NEWS_FEED = "get_newsfeed"
    internal static let API_ADD_NEWS_FEED = "add_newsfeed"

    // POS Dasbhoad
    internal static let API_PRODUCT_BANNER_LIST  = "/api/productbannerlist"
    internal static let API_PRODUCT_HOME_LIST = "/api/productHomeList"
    internal static let API_PRODUCT_LIST = "/api/productList"
    internal static let API_SEARCH_LIST = "/api/searchProductList"
    internal static let API_PRODUCT_DETAIL = "/api/productDetail"
    
    internal static let API_NOTIFICATION_LIST = ""
    internal static let API_NOTIFICATION_CLEAR = ""
    
    // Business Information
    // Listing
    internal static let API_PARENT_LIST = "getlistingparent"
    internal static let API_CHILD_LIST = "getlistingchild"
    
    internal static let API_ADD_CHILD_LIST = "add_listing"
    internal static let API_GET_CATEGORY_LIST = "getlistingcategory"
    
    internal static let API_UPLOAD_IMAGE = "uploadimage"
    internal static let API_DELETE_MEDIA = "delete_media"
    internal static let API_UPLOAD_MULTIPLE_IMAGE = "multiuploadimage"
    internal static let API_DELETE_MEDIA_INDIVIDUAL = "delete_media_individual"
    
    internal static let API_GET_BUSINESS_DETAIL = "getbusiness"
    internal static let API_ADD_BUSINESS_ALL = "add_business_all"
    
    internal static let API_ADD_SERVICE_ALL = "add_service_all"
    internal static let API_GET_SERVICE_ALL = "getservice"
    
    internal static let API_GET_SERVICE_CATEGORY = "servicecategory"
    internal static let API_GET_SERVICE_BY_CATEGORY = "getservicebycat"
        
    internal static let API_ADD_PERSONNEL_SERVICE_ALL = "add_personal"
    internal static let API_GET_PERSONAL = "getpersonal"
    
    internal static let API_GET_LOCATION = "getlocation"
    internal static let API_GET_CITIES = "get_cities"

    internal static let API_ADD_FACILITY_SERVICE_ALL = "add_facility"
    internal static let API_GET_FACILITY = "getfacility"
    
    internal static let API_ADD_INSIDE_LOOK_SERVICE_ALL = "add_insidelook"
    internal static let API_GET_INSIDE_LOOK = "getinsidelook"
    
    internal static let API_ADD_INSTRUCTION_CONTENT_SERVICE_ALL = "add_instructionalcontent"
    internal static let API_GET_INSTRUCTION_CONTENT_LOOK = "getinstructionalcontent"
    
    
    internal static let API_DASHBOARD_SERVICE_PROVIDER = "dashboardserviceprovider"
    internal static let API_DASHBOARD_SERVICE_PROVIDER_ALL_SERVICE = "getallservicepage"
    internal static let API_DASHBOARD_USER = "dashboarduser"
    internal static let API_DASHBOARD_USER_ALL_SERVICE = "getallservicepageuser"
    
    internal static let API_KEYBOARD_SEARCH = "search"
    
    internal static let API_SERVICE_CATEGORY_BY_USER = "servicecategorybyuser"
    internal static let API_SERVICES_BY_BUSINESS_USER = "getservicebybusiuser"
    
    internal static let API_PERSONNEL_CATEGORY_BY_USER = "getpersonalbyuser"
    internal static let API_FACILITY_CATEGORY_BY_USER = "getfacilitybyuser"
    internal static let API_INSIDE_LOOK_CATEGORY_BY_USER = "getinsidelookbyuser"
    internal static let API_INSTRUCTIONAL_CONTENT_CATEGORY_BY_USER = "getinstructionalcontentbyuser"
    
    internal static let API_QUICK_SEARCH = "quicksearch"
    internal static let API_QUICK_SEARCH_SEARCH = "quicksearchservice"
    
    internal static let API_GET_PACKAGE = "getpackage"
    
    internal static let API_PAYMENT = "payment"
    internal static let API_PAYMENT_CASH = "payment_cash"

    internal static let API_PAYMENT_HISTORY = "paymenthistory"
    internal static let API_PAYMENT_HISTORY_PROVIDER = "paymenthistoryprovider"
    internal static let API_PAYMENT_HISTORY_DETAIL = "paymenthistorydetail"
    internal static let API_PAYMENT_HISTORY_DETAIL_USER = "paymenthistorydetailuser"
    internal static let API_PAYMENT_HISTORY_FILTER = "paymenthistoryfilter"
    
    internal static let API_VOUCHER = "myvoucher"
    internal static let API_VOUCHER_DETAIL = "voucherdetail"
    
    internal static let API_ADVANCE_SEARCH_SERVICE_PROVIDER = "advancesearchbusiness"
    internal static let API_ADVANCE_SEARCH_SERVICE = "advancesearchservice"
    internal static let API_ADVANCE_SEARCH_PERSONAL = "advancesearchpersonal"
    internal static let API_ADVANCE_SEARCH_FACILITY = "advancesearchfacility"
    internal static let API_ADVANCE_SEARCH_INSIDE_LOOK = "advancesearchinsidelook"
    internal static let API_ADVANCE_SEARCH_INSTRUCTIONAL = "advancesearchinstructionalcontent"
    
    internal static let API_ADD_RATING = "add_rating"
    internal static let API_GET_RATING = "getrating"
    internal static let API_GET_TESTIMONIALS = "testimonials"

    internal static let API_GET_ARTICLE = "getarticle"
    
    internal static let API_REDEEM_VOUCHER = "redeemvoucher"
    
    internal static let API_PAYMENT_SETTING = "paymentsetting"
    
    internal static let API_WITHDRAW_STATUS = "withdrawstatus"
    
    internal static let API_WITHDRAWL_LIST = "withdrawList"

    
    internal static let API_WITHDRAW_AMOUNT = "withdrawamount"
    
    internal static let API_REFUND_PROCESS = "refundprocess"
    
    internal static let API_REFUND_LIST = "refundlist"
    
    internal static let API_SERVICE_BY_ID = "getservicebyid"
    
    internal static let API_FACILITY_BY_ID = "getfacilitybyid"
    
    internal static let API_PERSONAL_BY_ID = "getpersonalbyid"
    
    internal static let API_INSIDE_LOOK_BY_ID = "getinsidelookbyid"
    
    internal static let API_INSTRUCTIONAL_BY_ID = "getinstructionalcontentbyid"
    
    internal static let API_ADD_SERVICE_TO_CART = "add_service_to_cart"

    internal static let API_DELETE_SERVICE_FROM_CART = "delete_cart_service"

    internal static let API_GET_SERVICE_FROM_CART = "get_cart_service"
    
    internal static let API_REMOVE_CART_SERVICES = "remove_cart_service"

    internal static let API_GET_CART_SERVICES = "cart_service"
    
    internal static let API_REDEEM_CODE = "redeem_code"

}

struct HeaderKeys {
    internal static let kLANGUAGE     = "lang"
    internal static let kVERSION     = "version"
    internal static let kTOKEN       = "token"
    internal static let kUSER_ID     = "user_id"
    internal static let kFCM_TOKEN   = "fcm_token"
    internal static let kDEVICE_TYPE = "device_type"
    internal static let kAUTHORIZATION = "Authorization"
    internal static let kDEVICE_ID = "device_id"
    
}

struct MIME {
    internal static let IMAGE_JPEG = "image/jpeg"
    internal static let IMAGE_PNG = "image/png"
    
    internal static let APP_PDF = "application/pdf"
    internal static let AUDIO_MP4 = "audio/mp4"
    internal static let VIDEO_MP4 = "video/mp4"
    
    internal static let VIDEO_MOV = "video/quicktime"
    
}


struct APIResponseStatus {
    internal static let kResponseStatus: String = "status"
    internal static let kResponseStatusOK:Int = 1
}

enum APIStatusCode:Int{
    
    case SUCCESS  = 200
    
    case VERSIONUPDATE = 460
    //case TOKEN_EXPIRE = 461
    case TOKEN_EXPIRE = 401
    
    case USER_DEACTIVATE = 462
    
    case VERSION_NO_CHANGE = 465
    case VERSION_MINIOR_CHANGE = 466
    case VERSION_MAJOR_CHANGE = 467
    
}

enum PaymentMode{
    case CREDIT
    case DEBIT
    case COD
}

enum PaymentMethodName:String{
    case checkmo
}

//
//  FontsConfig.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit

class FontsConfig: NSObject {
 
    static let FONT_SIZE_SMALL:CGFloat = 10.0
    static let FONT_SIZE_NORMAL:CGFloat = 14.0
    static let FONT_SIZE_MEDIUM:CGFloat = 18.0
    static let FONT_SIZE_MEDIUM20:CGFloat = 20.0
    static let FONT_SIZE_LARGE:CGFloat = 22.0
    
    static let FONT_SIZE_16:CGFloat = 16.0
    static let FONT_SIZE_14:CGFloat = 14.0
  
    static var FONT_TYPE_Regular:String = "Nunito-Regular"
    static let FONT_TYPE_SEMI_Bold:String = "Nunito-SemiBold"
    static let FONT_TYPE_Bold:String = "Nunito-Bold"
    static let FONT_TYPE_Light:String = "Nunito-Light"

    struct FontHelper {
        static func defaultRegularFontWithSize(_ size: CGFloat) -> UIFont {
            return UIFont(name: FONT_TYPE_Regular as String, size: size)!
        }
        
        static func defaultLightFontWithSize(_ size: CGFloat) -> UIFont {
            return UIFont(name: FONT_TYPE_Light, size: size)!
        }
        
        static func defaultBoldFontWithSize(_ size: CGFloat) -> UIFont {
            return UIFont(name: FONT_TYPE_Bold, size: size)!
        }
    }
}

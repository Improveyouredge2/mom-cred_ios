//
//  AppDelegate.swift
//  MomCred
//
//  Created by Apple_iOS on 21/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var navigationController:UINavigationController?
    var fcmManager:NMFCMManager!
    
    var isAllowSkip = false
    
    fileprivate var dashboardVC:DashboardView?
    fileprivate var signUpViewController: UserSelectionViewController?
    fileprivate var loginViewController: LoginViewController?
    private(set) var activeTabbar:JBTabBarController?

    //MARK:- Resign screen shot
    var imageview : UIImageView?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 13.0, *) {
            window!.overrideUserInterfaceStyle = .light
        }
        
        Analytics.setAnalyticsCollectionEnabled(false)
        FirebaseApp.app()?.isDataCollectionDefaultEnabled = false
        
//        UIAccessibility.requestGuidedAccessSession(enabled: true){
//            success in
//            print("Request guided access success \(success)")
//        }
        
        print(" \(UIAccessibility.isGuidedAccessEnabled)")
        
        //MARK:- CRASHLATICS
//        Fabric.sharedSDK().debug = true
//        Fabric.with([Crashlytics.self])
        
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        UIView.appearance().isExclusiveTouch = true
        
        if(HelperConstant.GOOGLE_API_KEY.length > 0){
//            GMSServices.provideAPIKey(HelperConstant.GOOGLE_API_KEY)
            GMSPlacesClient.provideAPIKey(HelperConstant.GOOGLE_API_KEY)
            GMSServices.provideAPIKey(HelperConstant.GOOGLE_API_KEY)
        }
        
        if(Helper.sharedInstance.isUserAlreadyLoggedInToApplication()){
            self.setupCustomTabBar()
        } else {
            self.openLoginScreen(isLoadOnNavigation: true)
        }
        
        return true
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        //MARK:- Resign screen shot
        let imgV = UIImageView(frame: self.window?.bounds ?? UIScreen.main.bounds)
        imgV.image = UIImage.init(named: "00_splash")
        imageview = imgV//UIImageView.init(image: UIImage.init(named: "00_splash"))
        self.window?.addSubview(imageview!)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        Helper.sharedInstance.stopMonitoring()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        Helper.sharedInstance.startMonitoring()
        
        //MARK:- Resign screen shot
        if (imageview != nil){
            
            imageview?.removeFromSuperview()
            imageview = nil
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- Resign screen shot
//    func applicationWillResignActive(_ application: UIApplication) {
//
//        imageview = UIImageView.init(image: UIImage.init(named: "bg_splash"))
//        self.window?.addSubview(imageview!)
//        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
//        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
//    }
//
//    func applicationDidBecomeActive(_ application: UIApplication) {
//        if (imageview != nil){
//
//            imageview?.removeFromSuperview()
//            imageview = nil
//        }
//        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    }
//
}

//MARK:- Applicaiton initial setup
extension AppDelegate{
    
    func customTabbar() {
        if let token = PMUserDefault.getAppToken(), !token.trim().isEmpty {
            let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
            switch  role_id {
            case AppUser.ServiceProvider.rawValue:
                let serviceProviderTabbar : ServiceProviderTabbar = LMBaseApp.sharedInstance.getViewController(storyboardName: HelperConstant.TabbarStoryboard, viewControllerName: ServiceProviderTabbar.nameOfClass)
                self.activeTabbar = serviceProviderTabbar
                self.navigationController  = UINavigationController(rootViewController: serviceProviderTabbar)
                break
            case AppUser.LocalBusiness.rawValue:
                let localBussinessTabbar : LocalBussinessTabbar = LMBaseApp.sharedInstance.getViewController(storyboardName: HelperConstant.TabbarStoryboard, viewControllerName: LocalBussinessTabbar.nameOfClass)
                self.activeTabbar = localBussinessTabbar
                self.navigationController  = UINavigationController(rootViewController: localBussinessTabbar)
                break
            case AppUser.Enthusiast.rawValue:
                let enthusiastTabbar : EnthusiastTabbar = LMBaseApp.sharedInstance.getViewController(storyboardName: HelperConstant.TabbarStoryboard, viewControllerName: EnthusiastTabbar.nameOfClass)
                self.activeTabbar = enthusiastTabbar
                self.navigationController  = UINavigationController(rootViewController: enthusiastTabbar)
                break
            default:
                break
            }
        } else {
            let guestUserTabbar: GuestUserTabBar = LMBaseApp.sharedInstance.getViewController(storyboardName: HelperConstant.TabbarStoryboard, viewControllerName: GuestUserTabBar.nameOfClass)
            activeTabbar = guestUserTabbar
            navigationController  = UINavigationController(rootViewController: guestUserTabbar)
        }

        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.rootViewController = self.navigationController
        self.navigationController?.isNavigationBarHidden = true
        self.window?.makeKeyAndVisible()
    }
    
    func setupCustomTabBar(){
        
        self.loginViewController = nil
        self.signUpViewController = nil
        self.navigationController = nil
        if(navigationController == nil){
            customTabbar()
        } else {
            self.window?.rootViewController = self.navigationController
            self.navigationController?.isNavigationBarHidden = true
        }
    }
    
    func openLoginScreen(isLoadOnNavigation:Bool = false){
        if(loginViewController == nil){
            loginViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.onBoardStoryBoard, viewControllerName: LoginViewController.nameOfClass)
            Helper.sharedInstance.loginCallingScr = LoginCallingScr.Home
            
            if(isLoadOnNavigation){
                self.navigationController  = UINavigationController(rootViewController: self.loginViewController!)
                self.window?.rootViewController = self.navigationController
                self.navigationController?.isNavigationBarHidden = true
                self.window?.makeKeyAndVisible()
            } else {
                self.window?.rootViewController = loginViewController
            }
        }
    }
    
    func openSignUpScreen(isLoadOnNavigation:Bool = false){
        signUpViewController = nil
        if(signUpViewController == nil){
            signUpViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.onBoardStoryBoard, viewControllerName: UserSelectionViewController.nameOfClass)
            self.navigationController?.pushViewController(signUpViewController!, animated: true)
        }
    }
    
    func openScreenWithTabIndex(index:Int){
        navigationController?.popToRootViewController(animated: false)

        self.activeTabbar?.selectedIndex = index
        self.activeTabbar?.viewDidAppear(true)
    }
    
    func openPremiumMembershipView() {
        openScreenWithTabIndex(index: 2)
        
        if let viewControllers = activeTabbar?.viewControllers, viewControllers.count > 2, let viewC = viewControllers[2] as? PMProfileView {
            let membershipItem = viewC.menuList.first { $0.controller is PMPremiumMemberShipPlanView }
            if let membershipVC = membershipItem?.controller {
                navigationController?.pushViewController(membershipVC, animated: true)
            }
        }
    }
}


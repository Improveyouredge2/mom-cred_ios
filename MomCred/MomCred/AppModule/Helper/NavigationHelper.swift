//
//  NavigationHelper.swift
//  MomCred
//
//  Created by MD on 07/08/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import SafariServices
import MessageUI

extension UIViewController {
    func openLink(for link: String) {
        let url: URL?
        if !link.hasPrefix("http://"), !link.hasPrefix("https://") {
            url = URL(string: "https://\(link)")
        } else {
            url = URL(string: link)
        }
        if let url = url {
            let safari = SFSafariViewController(url: url)
            present(safari, animated: true, completion: nil)
        }
    }
    
    func openMail(for email: String) {
        if MFMailComposeViewController.canSendMail() {
            let controller = MFMailComposeViewController()
            controller.setToRecipients([email])
            present(controller, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "Error", message: "Mail account is not configured.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }

    func showServiceDetail(for serviceId: String) {
        ServiceService.getServiceDetailFor(serviceId: serviceId) { [weak self] (details, _) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if let details = details {
                    let detailView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceDetailView.nameOfClass) as ServiceDetailView
                    detailView.serviceAddRequest = details
                    detailView.isHideEditOption = true
                    self?.navigationController?.pushViewController(detailView, animated: true)
                }
            }
        }
    }
    
    func showFacilityDetail(for facilityId: String) {
        MyFacilitiesService.getFacilityDetailFor(facilityId: facilityId) { [weak self] (details, _) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if let details = details {
                    let facilityDetail =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesDetailViewController.nameOfClass) as MyFacilitiesDetailViewController
                    facilityDetail.myFacilityAddRequest = details
                    facilityDetail.isHideEditOption = true
                    self?.navigationController?.pushViewController(facilityDetail, animated: true)
                }
            }
        }
    }
    
    func showPersonalDetail(for personalId: String) {
        PersonnelService.getPersonalDetailFor(personalId: personalId) { [weak self] (details, _) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if let details = details {
                    let personalDetailVC =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: MyPersonnelDetailViewController.nameOfClass) as MyPersonnelDetailViewController
                    personalDetailVC.personnelAddRequest = details
                    personalDetailVC.isHideEditOption = true
                    self?.navigationController?.pushViewController(personalDetailVC, animated: true)
                }
            }
        }
    }
    
    func showInsideLookDetail(for insideLookId: String) {
        ILCService.getInsideLookDetailFor(insideLookId: insideLookId) { [weak self] (details, _) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if let details = details {
                    let ilcDetailVC =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InsideLookContentStoryboard, viewControllerName: ILCDetailViewController.nameOfClass) as ILCDetailViewController
                    ilcDetailVC.ilcAddRequest = details
                    ilcDetailVC.isHideEditOption = true
                    self?.navigationController?.pushViewController(ilcDetailVC, animated: true)
                }
            }
        }
    }
    
    func showInstructionalDetail(for instructionalId: String) {
        ICService.getInstructionalContentDetailFor(instructionalId: instructionalId) { [weak self] (details, _) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if let details = details {
                    let icDetailVC =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: ICDetailViewController.nameOfClass) as ICDetailViewController
                    icDetailVC.icAddRequest = details
                    icDetailVC.isHideEditOption = true
                    self?.navigationController?.pushViewController(icDetailVC, animated: true)
                }
            }
        }
    }
}

//
//  NMHelper.swift
//
//  Created by Apple_iOS on 18/01/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit
import NotificationBannerSwift
import MBProgressHUD
import Reachability
import SwiftEventBus
import SideMenu
import AVKit

import AFMInfoBanner


/**
 *  Struct for MenuItem on Sidebar with below feilds.
 *  @title: menu option title.
 *  @imageIcon: menu option icon image data.
 *  @controller: Object BaseViewController for manage screen
 *  @tabIndex: Specify index of menu option in list
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
struct HamburgerMenuItem{
    var title:String?
    var imageIcon:UIImage?
    var controller:LMBaseViewController?
    var tabIndex:Int = -1
}


/**
 *  Enum for Sidebar menu index.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
enum HamburgerMenuType:Int{
    case Home
    case ShopByCategory
    case DealOfTheDay
    case YourOrder
    case WishList
    case CouponCode
    case Offers
    case ReferAndWin
    case Report
    case ContactUs
    case AboutUs
    case Faq
}

/**
 *  Enum for previous screen of login screen.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
enum LoginCallingScr:Int{
    case Menu
    case MyAccount
    case Cart
    case ProductDetail
    case Home
}

///**
// *  Enum for previous screen of login screen.
// *
// *  @Developed By: Team Consagous [CNSGSIN054]
// */
//enum LoginCallingScr:Int{
//    case Menu
//    case MyAccount
//    case Cart
//    case ProductDetail
//}

/**
 *  Module Helper class for specifying utility method/functionality.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class Helper: NSObject {
    
    static let sharedInstance = Helper()
    
    static var notificationCount :   String    = ""
    
    var notificationUnReadCount = ""
    var isLoaderDisplay = false
    var isNetworkConnected = false
    var isInfoLoaded = false
    var networkAlert:UIAlertController?
    
    var loginCallingScr: LoginCallingScr = LoginCallingScr.Menu
    
    // Reachability instance for Network status monitoring
    let reachability = Reachability()!
    
    var notificationView: NMNotificationView!
    var advanceSearchOptionListViewController:AdvanceSearchOptionListViewController!
    var filterViewController:FilterViewController!
    var quickSearchViewController:QuickSearchViewController!
    
    fileprivate var dictButtonHolderForMultipleTap:NSMutableDictionary = [:]
    
    //MARK:- Check Network status
    /**
     *  Starts monitoring the network availability status
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleNetworkChangeEvent),
                                               name: Notification.Name.reachabilityChanged,
                                               object: reachability)
        do{
            try reachability.startNotifier()
            
            if(reachability.connection == .wifi || reachability.connection == .cellular){
                isNetworkConnected = true
            }
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }
    
    /**
     *  Stops monitoring the network availability status
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: Notification.Name.reachabilityChanged,
                                                  object: reachability)
    }
    
    /**
     *  Called whenever there is a change in NetworkReachibility Status
     *
     *  @param notification: Notification with the Reachability instance.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @objc func handleNetworkChangeEvent(_ notification: Foundation.Notification!) -> Void {
        let reachability = notification.object as! Reachability
        switch reachability.connection {
        case .none:
            debugPrint("Network became unreachable")
            isNetworkConnected = false
            showNetworkNotAvailableAlertController()
        case .wifi:
            debugPrint("Network reachable through WiFi")
            isNetworkConnected = true
            showScreenInfo()
            HelperConstant.appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
        case .cellular:
            debugPrint("Network reachable through Cellular Data")
            isNetworkConnected = true
            showScreenInfo()
            HelperConstant.appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    /**
     *  Send internal notification for network connect done by application
     *
     *  @param empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showScreenInfo(){
        if(!isInfoLoaded){
            isInfoLoaded = true
        }
        
        SwiftEventBus.post(HelperConstant.NetworkConnect)
        
        // Upload image in background if anything pending
        Helper.sharedInstance.uploadBIPendingImages()
        
    }
    
    /**
     *  Load value from string file to show text in diffrent language
     *
     *  @param key language constant name.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getMessageForCode(_ constantName:String) -> String? {
        let fileName = HelperConstant.DefaultLang
        return NSLocalizedString(constantName, tableName: fileName, bundle: Bundle.main, value: "", comment: "")
    }
    
    /**
     *  Show Notification Banner
     *
     *  @param key title on banner, message on banner, button title on banner and controller in which banner display.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showNotificationBanner(title:String?, message:String, buttonTitle:String?, controller:UIViewController?){
        
        AFMInfoBanner.hideAll()
        
        if (title?.contains(LocalizationKeys.success.getLocalized()))! || (title?.lowercased().contains("success"))!{
            AFMInfoBanner.showAndHide(withText: message, style: AFMInfoBannerStyle.info)
        } else  if (title?.contains(LocalizationKeys.error.getLocalized()))! {
            AFMInfoBanner.showAndHide(withText: message, style: AFMInfoBannerStyle.error)
        }
        
        // remove all banner queue
//        NotificationBannerQueue.default.removeAll()
//
//        let banner = NotificationBanner(title: title!, subtitle: message, style: .success)
//        banner.backgroundColor = UIColor(hexString: ColorCode.appColor)
//        banner.titleLabel?.textColor = UIColor(hexString: ColorCode.whiteColor)
//        banner.subtitleLabel?.textColor = UIColor(hexString: ColorCode.whiteColor)
//        banner.show()
    }
    
    
    /**
     *  Load value from string file to show text in diffrent language
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showNetworkNotAvailableAlertController(){
        
        AFMInfoBanner.hideAll()
        
        let errorMessage = getMessageForCode("no_internet_message")!
        AFMInfoBanner.showAndHide(withText: errorMessage, style: AFMInfoBannerStyle.error)
        
//        let errorTitle = getMessageForCode("no_internet_title")!
//        let errorMessage = getMessageForCode("no_internet_message")!
//
//        let banner = NotificationBanner(title: errorTitle, subtitle: errorMessage, style: .success)
//        banner.duration =   HelperConstant.NOTIFICATION_BANNER_MIN_DURATION
//        banner.backgroundColor = UIColor(hexString: ColorCode.primaryColor)
//        banner.titleLabel?.textColor = UIColor(hexString: ColorCode.whiteColor)
//        banner.subtitleLabel?.textColor = UIColor(hexString: ColorCode.whiteColor)
//        banner.show()
    }
    
    
    /**
     *  Show common alert view controller
     *
     *  @param key title on banner, message on banner, button title on banner and controller in which banner display..
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showAlertViewControllerWith(title:String?, message:String, buttonTitle:String?, controller:UIViewController?){
        
        AFMInfoBanner.hideAll()
        
        if (title?.contains(LocalizationKeys.success.getLocalized()))! || (title?.lowercased().contains("success"))!{
            AFMInfoBanner.showAndHide(withText: message, style: AFMInfoBannerStyle.info)
        } else  if (title?.contains(LocalizationKeys.error.getLocalized()))! {
            AFMInfoBanner.showAndHide(withText: message, style: AFMInfoBannerStyle.error)
        }
        
//        let banner = NotificationBanner(title: title!, subtitle: message, style: .success)
//
//        banner.duration =   HelperConstant.NOTIFICATION_BANNER_MIN_DURATION
//        banner.backgroundColor = UIColor(hexString: ColorCode.successBannerBGColor)
//
//        if (title?.contains(LocalizationKeys.info.getLocalized()))! || (title?.lowercased().contains("oops"))!{
//            banner.backgroundColor  =   UIColor(hexString: ColorCode.infoBannerBGColor)
//        } else  if (title?.contains(LocalizationKeys.error.getLocalized()))! {
//            banner.backgroundColor = UIColor(hexString: ColorCode.errorBannerBGColor)
//        }
//
//        banner.titleLabel?.textColor = UIColor(hexString: ColorCode.whiteColor)
//        banner.subtitleLabel?.textColor = UIColor(hexString: ColorCode.whiteColor)
//        banner.show()
    }
    
    /**
     *  For showing error dialog if result from an event is false
     *
     *  @param key title Dialog Title and error Error message
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showRestErrorNeutralDialog(title:String, error:NMErrorModel?) {
        let errorTitle = title
        let errorMessage = error?.errorMsg
        let okTitle = Helper.sharedInstance.getMessageForCode("okBtnTitle")!
        
        networkAlert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        let defaultAction = UIAlertAction(title: okTitle, style: .default, handler: nil)
        networkAlert?.addAction(defaultAction)
        HelperConstant.appDelegate.window?.rootViewController?.present(networkAlert!, animated: true, completion: nil)
    }
    
    /**
     *  Show/Hide HudView(loader)
     *
     *  @param key view is UIView object
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
//    func showProgressHudViewWithTitle(title:String?){
//        if(isLoaderDisplay){
//            hideProgressHudView()
//        }
//        isLoaderDisplay = true
//        if( reachability.isReachable){
////            let loadingNotification = MBProgressHUD.showAdded(to: NMHelperConstant.appDelegate.window!, animated: true)
////            loadingNotification.mode = MBProgressHUDMode.indeterminate
////            loadingNotification.label.text =  title ?? ""
////            loadingNotification.label.textColor = UIColor.black
////            loadingNotification.alpha = 1.0
////            loadingNotification.label.font = FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_NORMAL)
//            
//            Spinner.show()
//        }
//        HelperConstant.appDelegate.window?.isUserInteractionEnabled = false
//    }
//    
//    /**
//     *  Show HudView(loader) On a Particular View
//     *
//     *  @param key view is UIView object
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN054]
//     */
//    func showProgressHudInView(view:UIView?){
//        if(isLoaderDisplay){
//            hideProgressHudView()
//        }
//        isLoaderDisplay = true
//        if( reachability.isReachable){
////            var loadingNotification:MBProgressHUD?
////
////            if(view != nil){
////                loadingNotification = MBProgressHUD.showAdded(to: view!, animated: true)
////            } else {
////                loadingNotification = MBProgressHUD.showAdded(to: NMHelperConstant.appDelegate.window!, animated: true)
////            }
////
////            loadingNotification?.mode = MBProgressHUDMode.indeterminate
////            loadingNotification?.label.text = ""
////            loadingNotification?.label.textColor = UIColor.black
////            loadingNotification?.alpha = 1.0
////            loadingNotification?.label.font = FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_NORMAL)
//            
//            Spinner.show()
//        }
//        HelperConstant.appDelegate.window?.isUserInteractionEnabled = false
//    }
//    
//    /**
//     *  Hide HudView(loader) On a Particular View
//     *
//     *  @param key empty.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN054]
//     */
//    func hideProgressHudView(){
////        MBProgressHUD.hide(for: NMHelperConstant.appDelegate.window!, animated: true)
//        
//        Spinner.hide()
//        HelperConstant.appDelegate.window?.isUserInteractionEnabled = true
//    }
//    
//    /**
//     *  Hide HudView(loader) On a Particular View
//     *
//     *  @param key empty.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN054]
//     */
//    func hideProgressHudfromView(view:UIView){
////        MBProgressHUD.hide(for: view, animated: true)
//        Spinner.hide()
//        HelperConstant.appDelegate.window?.isUserInteractionEnabled = true
//    }
    
    /**
     *  Load viewController from storyboard (used in multi-storyboard)
     *
     *  @param key storyboard name and view controller name.
     *
     *  @return generic type object as per sended type.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getViewController<T>(storyboardName:String, viewControllerName:String) -> T {
        let storyBoard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerName) as! T
        return viewController
    }
    
    /*
     *  set number of view border by @setViewCornerRadius
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     *
     */
    class func setViewCornerRadius(views: [UIView],borderColor: UIColor,borderWidth: Float,cornerRadius: Float){
        for view in views {
            view.layer.borderColor  = borderColor.cgColor
            view.layer.borderWidth  = CGFloat(borderWidth)
            view.layer.cornerRadius = CGFloat(cornerRadius)
        }
    }
    
    
    func showAlertNativeDialog(title:String, message:String,completion:@escaping ((String)->Void)) {
        
        let font: UIFont = UIFont(name: FontsConfig.FONT_TYPE_Regular, size: 18) ?? UIFont.systemFont(ofSize: 18)

        let TitleString = NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor : UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black])
                
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.setValue(TitleString, forKey: "attributedTitle")
        alert.view.tintColor = UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black

        let okAction = UIAlertAction(title: LocalizationKeys.btn_ok.getLocalized().capitalized, style: .default){ (action) in
            completion(LocalizationKeys.btn_ok.getLocalized())
        }
        alert.addAction(okAction)
        
        if let vc = Helper.getTopViewController() {
            vc.present(alert, animated: true, completion: nil)
        } else {
            HelperConstant.appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
}

extension Helper {
    
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
            
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}


//MARK:- Hamburger menu option
//MARK:-
extension Helper{
    
    /**
     *  Check if user if already logged in to application
     *
     *  @param empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func isUserAlreadyLoggedInToApplication() -> Bool{
        let loginInfo = PMUserDefault.getLoginInfo()
        if(loginInfo != nil){
            return true
        }
        return false
    }
    
    /**
     *  Prepare side bar menu option list with ViewController object of screen.
     *
     *  @param empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getHamburgerMenuList() -> [HamburgerMenuItem]{
        
        var menuList:[HamburgerMenuItem] = []
        
        var menuItem = HamburgerMenuItem()
        menuItem.title = "Home"
        menuItem.imageIcon = UIImage(named: "home-inactive")
        menuItem.tabIndex = HamburgerMenuType.Home.rawValue
        menuList.append(menuItem)
        
        menuItem = HamburgerMenuItem()
        menuItem.title = "Offers"
        menuItem.imageIcon = UIImage(named: "deals_tag_white")
        menuItem.tabIndex = HamburgerMenuType.Offers.rawValue
        menuList.append(menuItem)
        
        return menuList
    }
}

//MARK:- Implement Attributed Strings
//MARK:-
extension Helper{
    
    /**
     *  Prepare Attributed text with text, strike text,font, color and style.
     *
     *  @param normalText, strikeText, textFont, textColor, currency, currencyTextFont and currencyColor
     *
     *  @return NSMutableAttributedString text object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func convertSimpleTextToAttributedTextTextWithStrikeText(normalText:String, strikeText:String, textFont:UIFont, textColor:UIColor, currency:String, currencyTextFont:UIFont, currencyColor:UIColor) -> NSMutableAttributedString?{
        let attributeNormalText = NSAttributedString(string: normalText, attributes: [NSAttributedString.Key.font:textFont, NSAttributedString.Key.foregroundColor:textColor])
        let attributeCurrencyText = NSAttributedString(string: currency, attributes: [NSAttributedString.Key.font:currencyTextFont, NSAttributedString.Key.foregroundColor: currencyColor])
        
        // Strike text
        let attributeStrikeText = NSMutableAttributedString(string: strikeText, attributes: [NSAttributedString.Key.font:textFont, NSAttributedString.Key.foregroundColor:textColor, NSAttributedString.Key.strikethroughColor:currencyColor])
        
        let attributeStrikeCurrencyText = NSMutableAttributedString(string: currency, attributes: [NSAttributedString.Key.font:currencyTextFont, NSAttributedString.Key.foregroundColor:currencyColor])
        
        let attachmentStrikeString:NSMutableAttributedString = NSMutableAttributedString(attributedString: attributeStrikeText)
        attachmentStrikeString.append(NSMutableAttributedString(string: " "))
        attachmentStrikeString.append(attributeStrikeCurrencyText)
        attachmentStrikeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attachmentStrikeString.length))
        
        var attachmentString:NSMutableAttributedString? = nil
        
        attachmentString = NSMutableAttributedString(attributedString: attributeNormalText)
        attachmentString!.append(NSMutableAttributedString(string: " "))
        attachmentString!.append(attributeCurrencyText)
        attachmentString!.append(NSMutableAttributedString(string: "  "))
        
        attachmentString?.append(attachmentStrikeString)
        
        return attachmentString ?? nil
    }
    
    /**
     *  Prepare Attributed text with text,font, color and style.
     *
     *  @param normalText, textFont, textColor, currency, currencyTextFont and currencyColor
     *
     *  @return NSMutableAttributedString text object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func convertSimpleTextToAttributedTextTextViewCartCell(normalText:String, textFont:UIFont, textColor:UIColor, currency:String, currencyTextFont:UIFont, currencyColor:UIColor) -> NSMutableAttributedString?{
        let attributeNormalText = NSAttributedString(string: normalText, attributes: [NSAttributedString.Key.font:textFont, NSAttributedString.Key.foregroundColor:textColor])
        let attributeCurrencyText = NSAttributedString(string: currency, attributes: [NSAttributedString.Key.font:currencyTextFont, NSAttributedString.Key.foregroundColor: currencyColor])
        
        var attachmentString:NSMutableAttributedString? = nil
        attachmentString = NSMutableAttributedString(attributedString: attributeNormalText)
        attachmentString!.append(NSMutableAttributedString(string: " "))
        attachmentString!.append(attributeCurrencyText)
        
        return attachmentString ?? nil
    }
    
    /**
     *  Prepare Attributed for customize label to show icon on it with text, instead of using uiimageview control.
     *
     *  @param textStr, isIconLeftSide, isIconAdd, iconName, iconOffset and iconFixedSize
     *
     *  @return NSMutableAttributedString text object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func convertSimpleTextToAttributedTextIcon(_ textStr:String, isIconLeftSide:Bool, isIconAdd:Bool, iconName:String? = nil, iconOffset:CGPoint? = CGPoint.zero, iconFixedSize:CGSize? = CGSize.zero) -> NSMutableAttributedString?{
        let attachment = NSTextAttachment()
        let attributeStr = NSMutableAttributedString(string:textStr)
        var attachmentString:NSMutableAttributedString? = nil
        var myImage:UIImage? = nil
        var myImageSize:CGSize = CGSize.zero
        
        // check image name
        if(iconName != nil){
            myImage = UIImage(named: iconName!)
        }
        
        // check image object and prepare in require size.
        if(myImage != nil){
            attachment.image = myImage
            
            if(iconFixedSize != nil && (iconFixedSize?.width)! > CGFloat(0) || (iconFixedSize?.height)! > CGFloat(0)){
                myImageSize = iconFixedSize!
            }else{
                myImageSize = myImage!.size
            }
            attachment.bounds = CGRect(origin: iconOffset!, size: myImageSize)
        }
        
        // Check icon is required and the position on icon on text right side or left side of text in Attributted text
        if(isIconAdd){
            if(isIconLeftSide){
                attachmentString = NSMutableAttributedString(attributedString: NSMutableAttributedString(string: " "))
                attachmentString?.append(NSAttributedString(attachment: attachment))
                attachmentString!.append(attributeStr)
            }else{
                attachmentString = NSMutableAttributedString(attributedString: attributeStr)
                attachmentString!.append(NSMutableAttributedString(string: "  "))
                attachmentString?.append(NSAttributedString(attachment: attachment))
            }
            
        }else{
            attachmentString = NSMutableAttributedString(attributedString: attributeStr)
        }
        
        return attachmentString ?? nil
    }
    
    /**
     *  Method to prepare AttributedString for manage header information in hidden.
     *
     *  @param empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func convertSimpleTextToAttributedTextHiddenHeader(_ placeholder:String, _ pincode:String) -> NSMutableAttributedString?{
        
        let placeholderStr = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.font: FontsConfig.FontHelper.defaultRegularFontWithSize(14)])
        let pincodeStr = NSAttributedString(string: pincode, attributes: [NSAttributedString.Key.font: FontsConfig.FontHelper.defaultRegularFontWithSize(14), NSAttributedString.Key.foregroundColor: UIColor(hexString: ColorCode.primaryColor) ?? UIColor.white])
        
        var attachmentString:NSMutableAttributedString? = nil
        
        attachmentString = NSMutableAttributedString(attributedString: placeholderStr)
        attachmentString!.append(pincodeStr)
        
        
        return attachmentString ?? nil
    }
}

//MARK:- Setup Side Menu (Hamburger)
extension Helper{
    
    /**
     *  Prepare Side Menu bar controller to display on screen with help of SideMenuManager class object.
     *
     *  @param navigationViewController object of UINavigationController
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setupSideMenu(navigationViewController:UINavigationController){
        
//        hamburgerViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.HamburgerStoryboard, viewControllerName: HamburgerViewController.nameOfClass) as HamburgerViewController
//
//        // Define the menus
//        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: hamburgerViewController)
//        menuLeftNavigationController.leftSide = true
//
//        SideMenuManager.defaultManager.menuLeftNavigationController = menuLeftNavigationController
//        SideMenuManager.defaultManager.menuAddPanGestureToPresent(toView: navigationViewController.navigationBar)
//        SideMenuManager.defaultManager.menuAddScreenEdgePanGesturesToPresent(toView: navigationViewController.view)
//
//        SideMenuManager.defaultManager.menuPresentMode = .menuSlideIn
    }
    
    /**
     *  Prepare Customize Navigation bar custom view.
     *
     *  @param
        - buttonImage: Object of UIButton for left side option on Navigation bar.
        - iconImage: Icon image name for add image on left side button option.
        - title: Display text on navigation bar in center.
        - description: Display description on navigation bar below title.
        - navigationItem: Object of UINavigationItem
        - navigationBarLeftButtonAction: Add target method for left side button
        - navigationBarDetailButtonAction: Add gesture on navigation bar button
        - navigationBarRightButtonTitle: Title for right side UIButton
        - navigationBarRightButtonAction: Add target for right side UIButton
        - target: Object of UIViewController
        - navigationHide: Display or hide custom navigation bar
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setUpNavigationBarCustomViews(buttonImage:String,iconImage:String?, title:String, description:String?,navigationItem:UINavigationItem,navigationBarLeftButtonAction:Selector, navigationBarDetailButtonAction:Selector?, navigationBarRightButtonTitle:String?, navigationBarRightButtonAction:Selector?, target:UIViewController, navigationHide: Bool) {
        
        //Left view setup
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.appTheamWhiteColor()
        }
        UIApplication.shared.statusBarStyle = .lightContent
        
        
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: (HelperConstant.appDelegate.window?.frame.size.width)!, height: 44))
        customView.backgroundColor = UIColor.appTheamWhiteColor()
        target.navigationController?.isNavigationBarHidden = navigationHide
        
        //button-back
        let buttonBack = UIButton(type: UIButton.ButtonType.custom)
        buttonBack.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        buttonBack.setImage(UIImage(named:buttonImage), for: .normal)
        buttonBack.addTarget(target, action:Selector(NSStringFromSelector(navigationBarLeftButtonAction)), for: UIControl.Event.touchUpInside)
        buttonBack.backgroundColor = UIColor.clear
        customView.addSubview(buttonBack)
        
        // button- detail
        if(navigationBarDetailButtonAction != nil){
            let recognizer = UITapGestureRecognizer(target: target, action: Selector(NSStringFromSelector(navigationBarDetailButtonAction!)))
            customView.isUserInteractionEnabled = true
            customView.addGestureRecognizer(recognizer)
        }
        
        //icon image
        var marginX:CGFloat?
        if(iconImage != nil){
            let imageViewIcon = UIImageView(frame: CGRect(x: buttonBack.frame.size.width + 3, y: 4, width: 36, height: 36))
            imageViewIcon.backgroundColor = UIColor.gray
            imageViewIcon.layer.cornerRadius = imageViewIcon.frame.size.width/2
            imageViewIcon.clipsToBounds = true
            customView.addSubview(imageViewIcon)
            marginX = CGFloat(imageViewIcon.frame.origin.x + imageViewIcon.frame.size.width + 8)
        }else{
            marginX = CGFloat(buttonBack.frame.origin.x + buttonBack.frame.size.width + 3)
        }
        
        // add right navigation text button
        var rightButtonWidth:CGFloat = 0
        if(navigationBarRightButtonTitle != nil && navigationBarRightButtonAction != nil){
            let buttonRight = UIButton(type: UIButton.ButtonType.custom)
            let stringRect = navigationBarRightButtonTitle?.boundingRect(with: CGSize(width: CGFloat(CGFloat.greatestFiniteMagnitude), height:44), options: ([.usesLineFragmentOrigin, .usesFontLeading]), attributes: [NSAttributedString.Key.font: FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_14)], context: nil)
            rightButtonWidth = ceil(stringRect?.width ?? 0) + 10
            let rectFrame = customView.frame
            buttonRight.frame = CGRect(x: rectFrame.size.width - (rightButtonWidth + 10), y: 0, width: rightButtonWidth, height: 44)
            buttonRight.addTarget(target, action:Selector(NSStringFromSelector(navigationBarRightButtonAction!)), for: UIControl.Event.touchUpInside)
            buttonRight.titleLabel?.font =  FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_14)
            buttonRight.setTitle(navigationBarRightButtonTitle, for: UIControl.State.normal)
            buttonRight.setTitleColor(UIColor(hexString: ColorCode.buttonGroupDetailBlueColor), for: UIControl.State.normal)
            customView.addSubview(buttonRight)
        }
        
        //lable for title and description
        if(description != nil){
            let labelTitle = UILabel(frame: CGRect(x: marginX!, y: 8, width: (customView.frame.size.width - marginX! - 25) - rightButtonWidth, height: 20))
            labelTitle.text = title
            labelTitle.font = FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_LARGE)
            labelTitle.textColor = UIColor(red: (33.0/255.0), green: (33.0/255.0), blue: (33.0/255.0), alpha: 1.0)
            labelTitle.textAlignment = NSTextAlignment.center
            labelTitle.backgroundColor = UIColor.clear
            customView.addSubview(labelTitle)
            
            let labelDescription = UILabel(frame: CGRect(x: labelTitle.frame.origin.x, y: 24, width: labelTitle.frame.size.width, height: 15))
            labelDescription.text = description
            labelDescription.font = FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_LARGE)
            labelDescription.textColor = UIColor(red: (33.0/255.0), green: (33.0/255.0), blue: (33.0/255.0), alpha: 1.0)
            labelDescription.textAlignment = NSTextAlignment.left
            labelDescription.backgroundColor = UIColor.clear
            customView.addSubview(labelDescription)
        }else{
            
            // for center-algin
            let labelTitle = UILabel(frame: CGRect(x: marginX!, y: 13, width: (customView.frame.size.width - (marginX!*2) - rightButtonWidth), height: 20))
            labelTitle.text = title
            labelTitle.font = FontsConfig.FontHelper.defaultRegularFontWithSize(FontsConfig.FONT_SIZE_14)
            labelTitle.textColor = UIColor(red: (33.0/255.0), green: (33.0/255.0), blue: (33.0/255.0), alpha: 1.0)
            labelTitle.textAlignment = NSTextAlignment.center
            labelTitle.backgroundColor = UIColor.clear
            customView.addSubview(labelTitle)
        }
        
        let leftButton = UIBarButtonItem(customView: customView)
        
        //left side blank spave filler
        let leftSidePaddingFiller = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        leftSidePaddingFiller.width = -16
        navigationItem.leftBarButtonItems = [leftSidePaddingFiller,leftButton]
    }
    
    func convertArrayToJson(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return ""
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }

}

extension Helper{
    
    /**
     *  Open login view controller.
     *
     *  @param empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func openLoginViewController(){
        PMUserDefault.clearAllData()
        NMUserDefault.clearAllData()
        
        HelperConstant.appDelegate.isAllowSkip = false
        HelperConstant.appDelegate.openLoginScreen(isLoadOnNavigation: true)
    }
    
    func openSignUpViewController(){
        PMUserDefault.clearAllData()
        NMUserDefault.clearAllData()
        HelperConstant.appDelegate.isAllowSkip = false
        HelperConstant.appDelegate.openSignUpScreen(isLoadOnNavigation: true)
    }
}

//MARK:- Multiple click check on button
//MARK:-
extension Helper{
    /**
     *  Preventing button multiple clicks
     *
     *  @param fields.
        - senderButton: UIButton object on which delay is required
        - delayMillis: Minimum time of delay
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func isTooEarlyMultipleClicks(_ senderButton:UIButton,delayMillis:String) -> Bool{
        let key = String(format:"%d",senderButton.tag)
        if(dictButtonHolderForMultipleTap.value(forKey: key) as? String != nil){
            
            //button is already tapped...just check the time stamp
            let lastClickTime = dictButtonHolderForMultipleTap.value(forKey: key) as? String
            let timeStamp = Date().timeIntervalSince1970
            
            
            if(lastClickTime != nil){
//                (Double(lastClickTime!) + Double(delayMillis)) > Double(removeOptionalString(String(describing: timeStamp)) ?? "0.00")!
                let lastClickValue:Double = (Double(lastClickTime!) ?? 0.00)*1000
                let delayValue:Double = Double(delayMillis) ?? 0.00
                let timeStampValue:Double = (Double(removeOptionalString(String(describing: timeStamp))) ?? 0.00)*1000
                
                if((lastClickValue + delayValue) > timeStampValue){
                    return true;
                }
            }
        }
        
        //button tapped first time...just save the time stamp with tag
        let currentTimeMiliSeconds = Date().timeIntervalSince1970
        dictButtonHolderForMultipleTap.setValue(removeOptionalString(String(describing: currentTimeMiliSeconds)), forKey: key)
        
        return false
    }
    
    /**
     *  Checking UIButton object early touch event.
     *
     *  @param fields.
        - senderButton: UIButton object on which delay implemented or not.
     *
     *  @return Status of early touch event on same UIButton.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func isTooEarlyMultipleClicks(_ senderButton:UIButton) -> Bool {
        print("isTooEarlyMultipleClicks")
        return isTooEarlyMultipleClicks(senderButton, delayMillis: "1000")//default 1 sec
    }
    
    /**
     *  Method to remove 'optional' keyword from given text string.
     *
     *  @param fields.
        - convertText: Text in which 'optional' keyword attach.
     *
     *  @return String with removing 'optional' keyword from given string.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func removeOptionalString(_ convertText:String) -> String{
        var convertedString = convertText
        convertedString = convertedString.replacingOccurrences(of: "Optional(", with: "")
        convertedString = convertedString.replacingOccurrences(of: ")", with: "")
        return convertedString
    }
    
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            if(urlString.validateUrl()){
                // create NSURL instance
                if let url = NSURL(string: "http://\(urlString)") {
                    // check if your application can open the NSURL instance
                    return UIApplication.shared.canOpenURL(url as URL)
                }
            }
        }
        return false
    }
    
    func checkVideoSize(reqData:Data) -> Double{
        if let data = reqData as Data? {
            print("There were \(data.count) bytes")
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
            bcf.countStyle = .file
            var stringTxt = bcf.string(fromByteCount: Int64(data.count))
            print("formatted result: \(stringTxt)")
            stringTxt = stringTxt.replacingOccurrences(of: " MB", with: "")
            return Double(stringTxt) ?? 0.00
        }
        
        return 0.00
    }
}


// MARK:- Manage screen flow functionality
extension Helper{
    
    /**
     *  Method to manage manage screen flow functionality as per skip flow.
     *
     *  @param empty.
     *
     *  @return status of navigation happen.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func navigateToScreen() -> Bool{
        
        var isFound = false
        
        switch Helper.sharedInstance.loginCallingScr {
        case .Menu:
            isFound = true
            HelperConstant.appDelegate.navigationController?.popToRootViewController(animated: true)
            break
        case .MyAccount:
            isFound = true
            HelperConstant.appDelegate.navigationController?.popToRootViewController(animated: true)
            break
        case .Cart:
            isFound = true
            HelperConstant.appDelegate.navigationController?.popToRootViewController(animated: true)
            break
        case .Home:
            isFound = true
            HelperConstant.appDelegate.setupCustomTabBar()
            break
        default:
            break
        }
        
        return isFound
    }

    
    /**
     *  Method to open Notification list screen.
     *
     *  @param empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func openNotificaiton(){
        
        
        if(notificationView == nil){
            notificationView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.NotificationStoryboard, viewControllerName: NMNotificationView.nameOfClass)
        }
        
        if(notificationView != nil){
            HelperConstant.appDelegate.navigationController?.pushViewController(notificationView, animated: true)
        }
    }
    
    func userLogout(){
        openLoginViewController()
    }
    
    func openAdvanceSearchOptionList(callbackListing:((_ serviceList:[ServiceAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceSearchRequest:ServiceSearchRequest?) -> Void?)?, callbackServiceProvider:((_ busiList:[BIAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceProviderSearchRequest:ServiceProviderSearchRequest?) -> Void?)?){
        if(self.advanceSearchOptionListViewController == nil){
            self.advanceSearchOptionListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: AdvanceSearchOptionListViewController.nameOfClass)
        }
        
        if(advanceSearchOptionListViewController != nil){
            self.advanceSearchOptionListViewController.callbackListing = callbackListing
            self.advanceSearchOptionListViewController.callbackServiceProvider = callbackServiceProvider
            HelperConstant.appDelegate.navigationController?.present(advanceSearchOptionListViewController, animated: true, completion: nil)
        }
    }
    
    func openFilterOption(purchaseFilterDataList:[PurchaseListFilterResponse.PurchaseFilterDataResponse]?, callbackListing:((_ dateList:[String]?, _ tagIndexList:[String]?) -> Void?)?){
        if(filterViewController == nil){
            filterViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: FilterViewController.nameOfClass)
        }
        
        if(filterViewController != nil){
            
            if(purchaseFilterDataList != nil){
                filterViewController.purchaseFilterDataList = purchaseFilterDataList
            }
            
            filterViewController.callbackListing = callbackListing
            
            HelperConstant.appDelegate.navigationController?.present(filterViewController, animated: true, completion: nil)
        }
    }
}

//MARK:- Server API special checks for version and session
//MARK:-

extension Helper{
    
    func showVersionUpdateAvailableAlertController(){
        
        self.networkAlert = nil
        OperationQueue.main.addOperation() {

            Spinner.hide()
            
            if(self.networkAlert == nil){
                let errorTitle = LocalizationKeys.title_app_update.getLocalized()
                let errorMessage = LocalizationKeys.msg_new_version.getLocalized()
                self.networkAlert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertController.Style.alert)
                HelperConstant.appDelegate.window?.rootViewController?.present(self.networkAlert!, animated: true, completion: nil)
            }
        }
    }
    
    func showTokenExpireAlertController(_ message:String?){
        OperationQueue.main.addOperation() {
            Spinner.hide()
            self.networkAlert = nil
            if(self.networkAlert == nil){
                let title = LocalizationKeys.title_session_expire.getLocalized()
                self.networkAlert = UIAlertController(title: title, message: message!, preferredStyle: UIAlertController.Style.alert)
                let defaultAction = (UIAlertAction(title:LocalizationKeys.btn_ok.getLocalized(), style: UIAlertAction.Style.default, handler:{(action:UIAlertAction) in
                    self.openLoginViewController()
                }))
                self.networkAlert?.addAction(defaultAction)
                HelperConstant.appDelegate.window?.rootViewController?.present(self.networkAlert!, animated: true, completion: nil)
            }
        }
    }
    
    
    func versionNoChange(){
        // Remove version preference info
    }
    
    func versionMinorChange(message:String){
        // Display alert with Ok and Cancel
        //  self.openVersionUpdateWithCancelOption(message: message)
    }
    
    func versionMajorChange(message:String){
        // Display alert without OK and Cancel button
        //   self.openVersionUpdateWithoutCancelOption(message: message)
    }
}

//MARK:- Server API for upload bulk images
//MARK:-

extension Helper{
    func uploadBIPendingImages(){
//        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0){
//            let busi_id = UserDefault.getBIID() ?? ""
            
            let path = PMFileUtils.getLocalMediaFileFolderPath(folderName:"")!
            
            var listing = try! FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            
            if listing.count > 0
            {
                print("\n----------------------------")
                print("LISTING: \(path)")
                print("")
                
                listing = listing.sorted(by: { $0.absoluteString < $1.absoluteString })
                
                for file in listing
                {
                    print("File: \(file.debugDescription)")
                    print("Folder: \(file.lastPathComponent)")
                    
                    let folderName = file.lastPathComponent
                    var folderId = folderName
                    var isVideoStatus = false
                    let splitList = folderId.split(separator: "_")
                    var folderNameType = ""
                    if(splitList.count > 1){
                        folderNameType = String(splitList[0])
                        folderId = String(folderId.split(separator: "_")[1] )
                    }
                    
                    print(folderNameType)
                    var fileUrls:[URL]? = PMFileUtils.getFolderAllFileUrls(folderName: folderName)
                    var uploadImage:[UploadImageData] = []
                    
                    if(fileUrls != nil && (fileUrls?.count)! > 0){
                        
                        fileUrls = fileUrls?.sorted(by: { $0.absoluteString < $1.absoluteString })
                        
                        print("Urls: \(fileUrls ?? [])")
                        
                        var imageDataTemp = UploadImageData()
                        
                        do {
                            for fileUrl in (fileUrls)! {
                                
                                let imageData = try Data(contentsOf: fileUrl)
                                
                                let theFileName = (fileUrl.absoluteString as NSString).lastPathComponent
                                let theExt = (fileUrl.absoluteString as NSString).pathExtension
                                
                                imageDataTemp.id = folderId
                                imageDataTemp.imageData = imageData
                                imageDataTemp.imageName = theFileName
                                imageDataTemp.imageKeyName = "busi_img"
                                imageDataTemp.folderName = folderName
                                
                                
                                imageDataTemp.mediaCategory = "1" // 1 for businessimage 2 for documents
                                
                                if(folderNameType == "busi"){
                                    imageDataTemp.media_service = "0"
                                } else if(folderNameType == "service"){
                                    imageDataTemp.media_service = "1"
                                } else if(folderNameType == "personnel"){
                                    imageDataTemp.media_service = "2"
                                } else if(folderNameType == "facility"){
                                    imageDataTemp.media_service = "3"
                                } else if(folderNameType == "ILC"){
                                    imageDataTemp.media_service = "4"
                                } else if(folderNameType == "IC"){
                                    imageDataTemp.media_service = "5"
                                }
                                
                                if(theExt.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpeg") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame){
                                    imageDataTemp.imageType = "1" // 1 for image  2 for video
                                    imageDataTemp.media_extension = "1"
                                } else {
                                    imageDataTemp.imageType = "2" // 1 for image  2 for video
                                    imageDataTemp.media_extension = "2"
                                    isVideoStatus = true
                                }
                                
                                uploadImage.append(imageDataTemp)
                                
                                if(isVideoStatus){
                                    imageDataTemp = UploadImageData()
                                    
                                    let imageName = (fileUrl.absoluteString as NSString).lastPathComponent
                                    let theFileName = "thumb_\(imageName.split(separator: ".")[0]).png"

                                    let filePath = "\(file)\(theFileName)"
                                    let imageData = try Data(contentsOf: URL(string: filePath)!)
                                    
                                    imageDataTemp.id = folderId
                                    imageDataTemp.imageData = imageData
                                    imageDataTemp.imageName = theFileName
                                    imageDataTemp.imageKeyName = "snapshot"
                                    imageDataTemp.folderName = folderName
                                    
                                    imageDataTemp.mediaCategory = "1" // 1 for businessimage 2 for documents
                                    
                                    imageDataTemp.imageType = "1" // 1 for image  2 for video
                                    
                                    uploadImage.append(imageDataTemp)
                                }
                                
                                break
                            }
                        } catch {
                            //                    print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
                            print("Error while enumerating files")
                        }
                            // Upload on server
                            self.uploadImageOnServer(imageDataTemp: uploadImage)
                        
                        break
                    } else {
                        // Remove folder
                        _ = PMFileUtils.removeFolderAtLocalPath(folderName: folderName)
                        
                        // Check another Folder
                        self.uploadBIPendingImages()
                        
                    }
                    print("")
                    print("----------------------------\n")
                }
            }
    }
    
    func uploadImageOnServer(imageDataTemp: [UploadImageData]){
        BIServiceStep1.updateData(imageList: imageDataTemp, callback: { (status, imageResponse, message) in
            // Check image response and remove file from path
            if(status && imageResponse != nil && imageResponse?.data != nil && imageResponse?.data?.name != nil){
                if(PMFileUtils.removeImageAtLocalPath(fileName: imageDataTemp[0].imageName ?? "", folderName: imageDataTemp[0].folderName ?? "")){
                    
                    if(imageDataTemp[0].imageType == "2"){
                        // Remove thumb
                        if(PMFileUtils.removeImageAtLocalPath(fileName: "\(imageDataTemp[1].imageName ?? "")", folderName: imageDataTemp[1].folderName ?? "")) {
                            // Check another Image
                            self.uploadBIPendingImages()
                        }
                    } else {
                        // Check another Image
                        self.uploadBIPendingImages()
                    }
                }
            }
        })
    }
}

extension Helper {
    // Save video from photo library or camera to local path
    func export(_ assetURL: URL, folderName:String, fileName:String, fileData:NSData, completionHandler: @escaping (_ fileURL: URL?, _ error: Error?) -> ()) {
        
        let asset = AVURLAsset(url: assetURL)
        
        guard let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality) else {
            completionHandler(nil, nil)
            return
        }
        
        let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let outputURL = documentURL.appendingPathComponent("\(fileName).MP4")
        
        do{
            try FileManager.default.removeItem(at: outputURL)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        
        debugPrint(outputURL)
        exporter.shouldOptimizeForNetworkUse = true
        exporter.outputURL = outputURL
        exporter.outputFileType = AVFileType.mp4
        exporter.exportAsynchronously {
            if exporter.status == .completed {
                
                do {
                    
                    let fileData:NSData = try NSData(contentsOfFile: (exporter.outputURL?.absoluteString ?? "").replacingOccurrences(of: "file://", with: ""))
                    let savedFilePath = PMFileUtils.saveFileAtLocalPath(fileData: fileData, fileName: "\(fileName).MP4", folderName: folderName)
                    
                    //
                    try FileManager.default.removeItem(at: exporter.outputURL!)
                    
                    completionHandler(URL(string: savedFilePath ?? ""), nil)
                }
                catch let error as NSError {
                    print("Ooops! Something went wrong: \(error)")
                }
                
            } else {
                
                let savedFilePath = PMFileUtils.saveFileAtLocalPath(fileData: fileData, fileName: "\(fileName).MP4", folderName: folderName)
                
//                let status = fileData.write(to: savedFilePath, atomically: true)
                completionHandler(URL(string: savedFilePath ?? ""), nil)
                
//                completionHandler(nil, exporter.error)
            }
        }
    }
    
    func openTabScreenWithIndex(index:Int){
        HelperConstant.appDelegate.openScreenWithTabIndex(index: index)
    }
    
    func openPremiumMembershipView() {
        HelperConstant.appDelegate.openPremiumMembershipView()
    }
}

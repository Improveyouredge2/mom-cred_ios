//
//  UserDefault.swift
//  Mom-Cred
//
//  Copyright © 2019 Consagous. All rights reserved.

import Foundation

/**
 *  PMUserDefault class is save, fetch and delete data in device preference (UserDefaults)
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class UserDefault{
    
    // common object of UserDefaults, used in class method.
    fileprivate static let defaults = UserDefaults.standard
    
    //////////////// Remove all info //////
    class func clearAllData(){
        defaults.removeObject(forKey: HelperConstant.userDefaultBIID)
        

        defaults.synchronize()
    }
    
    ///////////////// GET & SAVE Id /////////////////
    // Business Id
    class func saveBIID(id: String){
        defaults.set(id, forKey: HelperConstant.userDefaultBIID)
        defaults.synchronize()
    }
    
    class func getBIID()->String?{
        let id:String?
        id = defaults.object(forKey: HelperConstant.userDefaultBIID) as? String
        return id ?? nil
    }
    
    class func removeBIID(){
        defaults.removeObject(forKey: HelperConstant.userDefaultBIID)
    }
    
    // Service ID
    class func saveSID(id: String){
        defaults.set(id, forKey: HelperConstant.userDefaultSID)
        defaults.synchronize()
    }
    
    class func getSID()->String?{
        let id:String?
        id = defaults.object(forKey: HelperConstant.userDefaultSID) as? String
        return id ?? nil
    }
    
    class func removeSID(){
        defaults.removeObject(forKey: HelperConstant.userDefaultSID)
    }
    
    // Personnel
    class func savePID(id: String){
        defaults.set(id, forKey: HelperConstant.userDefaultPID)
        defaults.synchronize()
    }
    
    class func getPID()->String?{
        let id:String?
        id = defaults.object(forKey: HelperConstant.userDefaultPID) as? String
        return id ?? nil
    }
    
    class func removePID(){
        defaults.removeObject(forKey: HelperConstant.userDefaultPID)
    }
    
    // Facility
    class func saveFID(id: String){
        defaults.set(id, forKey: HelperConstant.userDefaultFID)
        defaults.synchronize()
    }
    
    class func getFID()->String?{
        let id:String?
        id = defaults.object(forKey: HelperConstant.userDefaultFID) as? String
        return id ?? nil
    }
    
    class func removeFID(){
        defaults.removeObject(forKey: HelperConstant.userDefaultFID)
    }
    
    // Inside-Look Content ILC
    class func saveILC(id: String){
        defaults.set(id, forKey: HelperConstant.userDefaultILC)
        defaults.synchronize()
    }
    
    class func getILC()->String?{
        let id:String?
        id = defaults.object(forKey: HelperConstant.userDefaultILC) as? String
        return id ?? nil
    }
    
    class func removeILC(){
        defaults.removeObject(forKey: HelperConstant.userDefaultILC)
    }
    
    // Instructional Content IC
    class func saveIC(id: String){
        defaults.set(id, forKey: HelperConstant.userDefaultIC)
        defaults.synchronize()
    }
    
    class func getIC()->String?{
        let id:String?
        id = defaults.object(forKey: HelperConstant.userDefaultIC) as? String
        return id ?? nil
    }
    
    class func removeIC(){
        defaults.removeObject(forKey: HelperConstant.userDefaultIC)
    }
    
    
    ///////////////// GET & SAVE Additional info /////////////////
    class func saveAdditionalInfoDocument(itemList:[String])
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: itemList)
        defaults.set(data, forKey: HelperConstant.userDefaultAdditionalDocument)
    }
    
    class func getAdditionalInfoDocument() -> [String]?
    {
        if(defaults.object(forKey: HelperConstant.userDefaultAdditionalDocument) != nil){
            let userInfo:[String]?
            let outData:AnyObject = defaults.object(forKey: HelperConstant.userDefaultAdditionalDocument)! as AnyObject
            userInfo = NSKeyedUnarchiver.unarchiveObject(with: outData as! Data) as! [String]?
            return userInfo ?? nil
        }else{
            return nil
        }
    }
    
    ///////////////// GET & SAVE Business info /////////////////
    class func saveBusinessInfo(userInfo: NSDictionary){
        // Storing the model dictionary
        do{
            let data = try NSKeyedArchiver.archivedData(withRootObject: userInfo, requiringSecureCoding: false)
            defaults.set(data, forKey: HelperConstant.userDefaultPendingBusiness)
            UserDefaults.standard.synchronize()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    class func getBusinessInfo()-> NSDictionary?{
        if(defaults.object(forKey: HelperConstant.userDefaultPendingBusiness) != nil){
            var userInfo:NSDictionary?
            let outData:AnyObject = defaults.object(forKey: HelperConstant.userDefaultPendingBusiness)! as AnyObject
            do {
                userInfo = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(outData as! Data) as! NSDictionary?
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            return userInfo ?? nil
        }else{
            return nil
        }
    }
    
    class func removeBusinessInfo(){
        defaults.removeObject(forKey: HelperConstant.userDefaultPendingBusiness)
    }
    
    ///////////////// GET & SAVE Service info /////////////////
    class func saveServiceInfo(userInfo: NSDictionary){
        // Storing the model dictionary
        do{
            let data = try NSKeyedArchiver.archivedData(withRootObject: userInfo, requiringSecureCoding: false)
            defaults.set(data, forKey: HelperConstant.userDefaultPendingService)
            UserDefaults.standard.synchronize()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    class func getServiceInfo()-> NSDictionary?{
        if(defaults.object(forKey: HelperConstant.userDefaultPendingService) != nil){
            var userInfo:NSDictionary?
            let outData:AnyObject = defaults.object(forKey: HelperConstant.userDefaultPendingService)! as AnyObject
            do {
                userInfo = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(outData as! Data) as! NSDictionary?
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            return userInfo ?? nil
        }else{
            return nil
        }
    }
    
    class func removeServiceInfo(){
        defaults.removeObject(forKey: HelperConstant.userDefaultPendingService)
    }
}

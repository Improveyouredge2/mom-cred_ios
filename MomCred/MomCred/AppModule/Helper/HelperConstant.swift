//
//  NMHelperConstant.swift
//  NotificationLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit


/**
 *  Module Helper constant class for specifying constant used in module
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class HelperConstant: NSObject {
    internal static let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    internal static let IPHONE_X_DEFAULT_NAVIGATION_BAR_HEIGHT:CGFloat = 88
    internal static let IPHONE_X_DEFAULT_NAVIGATION_HEADER_BAR_HEIGHT:CGFloat = 44
    
    /**
     *  Enum specify device interface types like iPhone, iPad
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    enum UIUserInterfaceIdiom : Int
    {
        case Unspecified
        case Phone
        case Pad
    }
    
    /**
     *  Enum specify language used in module with string constant
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    enum LanguageType: String {
        case English = "English"
        case Spanish = "Spanish"
        case Chinese = "Chinese"
    }
    
    /**
     *  Enum specify static page constant used in module
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    enum StaticPage:String{
        case AboutUs
        case ContactUs
        case TermsAndCondition
        case PrivacyPolicy
        
        var StaticPageText: String{
            switch self {
            case .AboutUs:
                return "about-us.txt"
            case .ContactUs:
                return "contact-us.txt"
            case .TermsAndCondition:
                return "termsandcondition.txt"
            case .PrivacyPolicy:
                return "privacypolicy.txt"
            default:
                return "test.txt"
            }
        }
    }
    
    enum MediaExtension:String{
        case Video
        case Image
        
        var MediaExtensionText: String{
            switch self{
            case .Video:
                return "2"
            case .Image:
                return "1"
            default:
                return "1"
            }
        }
    }
    
    /**
     *  Enum specify different screen size of devices like iPhone 5s, iPhone 6 etc.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.nativeBounds.width
        static let SCREEN_HEIGHT        = UIScreen.main.nativeBounds.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    
    /**
     *  Enum specify device type on the basis of app run on device
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 1136.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 1136.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 1334.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 2208.0
        static let IS_IPHONE_X_AND_IPHONE_XS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 2436.0
        static let IS_IPHONE_XR = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 1792.0
        static let IS_IPHONE_XS_MAX  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 2688.0
        
    }
    
    /**
     *  Enum specify platform like arm6 or armm7
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if arch(i386) || arch(x86_64)
            isSim = true
            #endif
            return isSim
        }()
    }
    
//    internal static let GOOGLE_API_KEY = "AIzaSyBhXnDjZJcDPbVOZdfoSQKJ86ziI9DgbaU"
    internal static let GOOGLE_API_KEY = "AIzaSyCZgyoXdhRf2FNyVyf7C7z-GEXgwZc9yhY"

    //TODO: Open when Google doc key enable
//    static let GOOGLE_API_KEY:String = {
//        var nsDictionary: NSDictionary? = NSDictionary()
//        if let path = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist") {
//            nsDictionary = NSDictionary(contentsOfFile: path)
//        }
//
//        return nsDictionary != nil ? (nsDictionary?.value(forKey:  "API_KEY") != nil ? (nsDictionary?.value(forKey:  "API_KEY") as! String) : "") : ""
//    }()
    
    internal static let NOTIFICATION_BANNER_MIN_DURATION = 5.0 // specify banner display minimun time in secods
    
    internal static let APP_API_REQUEST_DATE_FORMAT =   "yyyy-MM-dd hh:mm a"
    internal static let APP_DATE_FORMAT =   "MMM dd, yyyy"
    
    internal static let LIMIT_LOWER_PASSWORD = 6
    internal static let LIMIT_UPPER_PASSWORD = 20
    internal static let LIMIT_FULLNAME = 30
    internal static let LIMIT_EMAIL = 50
    internal static let LIMIT_LOWER_PHONE = 10
    internal static let LIMIT_UPPER_PHONE = 12
    internal static let LIMIT_ADDRESS = 180
    internal static let LIMIT_ZIPCODE_NUMBER  = 6
    internal static let LIMIT_PURCHASEBILITY = 5
    internal static let LIMIT_REPEAT_TIME = 3
    internal static let LIMIT_AMOUNT = 10
    internal static let LIMIT_VIDEO_SIZE = 50
    internal static let LIMIT_AGE = 2
    internal static let LIMIT_DURATION = 8
    
    internal static let AppNotificationButtonTag = 1234321 // specify tag index used on controls
    
    internal static var DefaultLang = AppLanguage.English.rawValue // specify default language of app
    
    //Default REST Error Code and Message
    internal static let DEFAULT_REST_ERROR_CODE     = "default_rest_error_code"
    internal static let DEFAULT_REST_ERROR_MESSAGE  = "Unable to process your request.\nPlease try again later."
    internal static let Default_No_Data_Found_MSG   = "No records found!"
    
    internal static let userDefaultAPNSTokenKey = "APNS_TOKEN"
    internal static let userDefaultFCMTokenKey = "FCM_TOKEN"
    internal static let kFCMMessageIDs = "FCMMessageIDs"
    
    internal static let LOCAL_NOTIFICATION_IDENTIFIER = "localNotification"
    
    //USER DEFAULT KEY'S
    internal static let NetworkConnect                  = "NetworkConnect"
    internal static let LanguageChanged                 = "LanguageChanged"
    internal static let userDefaultUserLoginInfoKey     = "userLoginInfo"
    internal static let userDefaultBIID                 = "userDefaultBIID"
    internal static let userDefaultSID                  = "userDefaultSID"
    internal static let userDefaultPID                  = "userDefaultPID"
    internal static let userDefaultFID                  = "userDefaultFID"
    internal static let userDefaultILC                  = "userDefaultILC"
    internal static let userDefaultIC                  = "userDefaultIC"
    
    internal static let userDefaultAdditionalDocument = "userDefaultAdditionalDocument"
    
    internal static let userDefaultPendingService = "userDefaultPendingService"
    internal static let userDefaultPendingBusiness = "userDefaultPendingBusiness"
    
    internal static let kRefreshViewCart = "RefreshViewCart"
    
    internal static let kRefreshProfileImage = "ProfileImage"
    internal static let kRefreshTableView = "RefreshTableView"
    
    // Storyboard Name
    // Login And Signup Module StoryBoard
    internal static let customAlertStoryBoard = "CustomAlert"
    internal static let onBoardStoryBoard = "OnBoardStoryBoard"
    internal static let PickerViewsStoryboard   =   "PickerViews"
    internal static let TabbarStoryboard   =   "Tabbar"

    // Notification Storyboard Name
    internal static let NotificationStoryboard = "NMNotification"
    
    // App main module
    internal static let HamburgerStoryboard = "HamburgerViewController"
    internal static let DashboardStoryboard = "Dashboard"
    internal static let PackageStoryboard = "Package"
    internal static let ProductDetailStoryboard = "ProductDetail"
    internal static let PurchaseStoryboard = "Purchase"
    internal static let EarningsStoryboard = "Earnings"
    internal static let SearchFilterStoryboard = "SearchFilter"
    internal static let InstructionalServiceProviderStoryboard = "InstructionalServiceProvider"
    internal static let PersonnelStoryboard = "Personnel"
    internal static let FacilityStoryboard = "Facility"
    internal static let InstructionalContentStoryboard = "InstructionalContent"
    internal static let BusinessInformationStoryboard = "BusinessInformation"
    internal static let LocalBusinessStoryboard = "LocalBusiness"
    internal static let ServiceStoryboard = "Service"
    internal static let InsideLookContentStoryboard = "InsideLook"
    
    internal static let FilterVCStoryboard = "FilterVC"
    internal static let ProductVCStoryboard = "POSProductVC"
    internal static let WishListStoryboard = "WishList"
    internal static let CartStoryboard = "Cart"
    internal static let BuyStoryboard = "Buy"
    internal static let MyAccountStoryboard = "MyAccount"
    internal static let OrdersStoryboard = "Orders"
    internal static let OfferStoryboard = "Offer"
    internal static let ReferralStoryboard = "Referral"
    internal static let MoreOptionStoryboard = "More"
    
    internal static let DealOfTheDayStoryboard = "DealOfTheDay"
    internal static let ProductDetailsVCStoryboard = "ProductDetailsVC"
    internal static let PurchaseProductDetailsVCStoryboard = "PurchaseProductDetailsVC"
    internal static let CreditSystemVCStoryboard = "CreditSystem"
    
    internal static let BookingStoryboard = "Booking"
    internal static let MoreStoryboard = "More"
    
    // Extra API constant
    
    // Progress bar color constant
    
    //internal static let CURRENCY_SYMBOL = "INR"
    internal static let CURRENCY_SYMBOL = "$"
    
    internal static let quantityArray = ["1", "2", "3", "4", "5"]
    
    internal static let WidthWithoutButton:CGFloat = 0
    internal static let WidthWithSearch:CGFloat = 44
    
    enum AppLanguage:String{
    case English
    }
    
    
    class func kLogString(_ items: Any...){
        print(items)
    }
    
    internal static let minimumBlocks = 3
    internal static let minimumMediaBlocks = 2
    internal static let maximumProfileMedia = 5
    internal static let maximumSearchLocation = 4
    
    internal static let weekDayName:[String] = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"
    ]
    
    internal static let durationType:[String] = ["Minutes", "Hours","Days", "Weeks", "Months"]
    
}

struct ErrorMessage {
    static let unknownAPIError = "Service is currently unavailable."
    static let unavailableData = "Data is currently unavailable"
    static let unauthenticatedError = "Your session has expired. Please re-login."
}

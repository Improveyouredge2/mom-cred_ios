//
//  PMUserDefault.swift
//  Mom-Cred
//
//  Copyright © 2019 Consagous. All rights reserved.

import Foundation

/**
 *  PMUserDefault class is save, fetch and delete data in device preference (UserDefaults)
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PMUserDefault{
    
    // common object of UserDefaults, used in class method.
    fileprivate static let defaults = UserDefaults.standard
    
    //////////////// Remove all info //////
    class func clearAllData(){
        defaults.removeObject(forKey: PMHelperConstant.userDefaultUserProfilePicKey)
        defaults.removeObject(forKey: PMHelperConstant.userDefaultUserFirstNameKey)
        defaults.removeObject(forKey: PMHelperConstant.userDefaultUserLastNameKey)
        defaults.removeObject(forKey: PMHelperConstant.userDefaultIsEmail)
        defaults.removeObject(forKey: PMHelperConstant.userDefaultUserPhoneKey)
        defaults.removeObject(forKey: PMHelperConstant.userDefaultUserNameKey)
        //defaults.removeObject(forKey: PMHelperConstant.userDefaultUserIdKey)
        defaults.removeObject(forKey: PMHelperConstant.userDefaultUserAddressKey)
        defaults.removeObject(forKey: PMHelperConstant.userDefaultUserLoginInfoKey)
        //defaults.removeObject(forKey: PMHelperConstant.userDefaultUserRoleKey)

        defaults.synchronize()
    }
    
    ///////////////// GET & SAVE UserName /////////////////
    class func saveUserName(userName: String){
        defaults.set(userName, forKey: PMHelperConstant.userDefaultUserNameKey)
        defaults.synchronize()
    }
    
    class func getUserName()->String?{
        let userName:String?
        userName = defaults.object(forKey: PMHelperConstant.userDefaultUserNameKey) as? String
        return userName ?? nil
    }
  
    ///////////////// GET & SAVE First Name /////////////////
    class func saveUserFirstName(userFirstName: String){
        defaults.set(userFirstName, forKey: PMHelperConstant.userDefaultUserFirstNameKey)
        defaults.synchronize()
    }
    class func saveUserUserBank(isAdded: Bool){
        defaults.set(isAdded, forKey: PMHelperConstant.userDefaultBankDetailsKey)
        defaults.synchronize()
    }
    
    
    class func getUserFirstName()->String?{
        let firstName:String?
        firstName = defaults.object(forKey: PMHelperConstant.userDefaultUserFirstNameKey) as? String
        return firstName ?? nil
    }
    
    ///////////////// GET & SAVE Last Name /////////////////
    class func saveUserLastName(userLastName: String){
        defaults.set(userLastName, forKey: PMHelperConstant.userDefaultUserLastNameKey)
        defaults.synchronize()
    }
    
    class func getUserLastName()->String?{
        let lastName:String?
        lastName = defaults.object(forKey: PMHelperConstant.userDefaultUserLastNameKey) as? String
        return lastName ?? nil
    }
    
    ///////////////// GET & SAVE Profile Pic /////////////////
    class func saveProfilePic(profilePic: String){
        defaults.set(profilePic, forKey: PMHelperConstant.userDefaultUserProfilePicKey)
        defaults.synchronize()
    }
    
    class func getProfilePic()->String?{
        let profilePic:String?
        profilePic = defaults.object(forKey: PMHelperConstant.userDefaultUserProfilePicKey) as? String
        return profilePic ?? nil
    }
    
    ///////////////// GET & SAVE User Role /////////////////
    class func saveUserRole(role: String){
        defaults.set(role, forKey: PMHelperConstant.userDefaultUserRoleKey)
        defaults.synchronize()
    }
    
    class func getUserRole()->String?{
        let role:String?
        role = defaults.object(forKey: PMHelperConstant.userDefaultUserRoleKey) as? String
        return role ?? nil
    }
    
    class func getBankDetailsAdded()->Bool{
        var isAdded:Bool = false
        isAdded = defaults.object(forKey: PMHelperConstant.userDefaultBankDetailsKey) as? Bool ?? false
        return isAdded
    }
    
    ///////////////// GET & SAVE User Phone number /////////////////
    class func saveUserPhone(userPhone: String){
        defaults.set(userPhone, forKey: PMHelperConstant.userDefaultUserPhoneKey)
        defaults.synchronize()
    }
    
    class func getUserPhone()->String?{
        let userPhone:String?
        userPhone = defaults.object(forKey: PMHelperConstant.userDefaultUserPhoneKey) as? String
        return userPhone ?? nil
    }
  
    ///////////////// GET & SAVE Email /////////////////
    class func setUserEmail( email : String){
        defaults.set(email, forKey: PMHelperConstant.userDefaultIsEmail)
        defaults.synchronize()
    }
    
    class func getUserEmail() -> String? {
        let email:String?
        email = defaults.object(forKey: PMHelperConstant.userDefaultIsEmail) as? String
        return email ?? nil
    }

    ///////////////// GET & SAVE UserId /////////////////
    class func saveUserId(userId: String){
        defaults.set(userId, forKey: PMHelperConstant.userDefaultUserIdKey)
        defaults.synchronize()
    }
    
    class func getUserId()->String?{
        let userId:String?
        userId = defaults.object(forKey: PMHelperConstant.userDefaultUserIdKey) as? String
        return userId ?? nil
    }
    
    //////////////// GET & SAVE APP TOKEN /////////////////
    class func saveAppToken(appToken: String){
        defaults.set(appToken, forKey: PMHelperConstant.userDefaultAppTokenKey)
        defaults.set(appToken, forKey: PMHelperConstant.qr_scan_token)
        defaults.synchronize()
    }
    
    class func getAppToken()->String?{
        let appToken:String?
        appToken = defaults.object(forKey: PMHelperConstant.userDefaultAppTokenKey) as? String
        return appToken ?? nil
    }
    
    class func getAuthToken()->String?{
        let authToken:String?
        authToken = defaults.object(forKey: PMHelperConstant.qr_scan_token) as? String
        return authToken ?? nil
    }
    
    //////////////// GET & SAVE USER ADDRESS /////////////////
    class func saveAddress(address: String){
        defaults.set(address, forKey: PMHelperConstant.userDefaultUserAddressKey)
        defaults.synchronize()
    }
    
    class func getAddress()->String?{
        let address:String?
        address = defaults.object(forKey: PMHelperConstant.userDefaultUserAddressKey) as? String
        return address ?? nil
    }
    
    ///////////////// GET & SAVE Login Info //////
    class func saveLoginInfo(loginInfo: NSDictionary){
        defaults.set(loginInfo, forKey: PMHelperConstant.userDefaultUserLoginInfoKey)
        defaults.synchronize()
    }
    
    class func getLoginInfo()->NSDictionary?{
        let loginInfo:NSDictionary?
        loginInfo = defaults.object(forKey: PMHelperConstant.userDefaultUserLoginInfoKey) as? NSDictionary
        return loginInfo ?? nil
    }
    
    class func removeLoginInfo(){
        defaults.removeObject(forKey: PMHelperConstant.userDefaultUserLoginInfoKey)
        defaults.synchronize()
    }
    
    ///////////////// GET & SAVE Subscription status /////////////////
    class func saveSubsriptionInfo(subsriptionInfo: String){
        defaults.set(subsriptionInfo, forKey: PMHelperConstant.userDefaultSubscriptionInfo)
        defaults.synchronize()
    }
    
    class func getSubsriptionInfo()->String?{
        let subsriptionInfo:String?
        subsriptionInfo = defaults.object(forKey: PMHelperConstant.userDefaultSubscriptionInfo) as? String
        return subsriptionInfo ?? nil
    }
    
    ///////////////// GET & SAVE Subscription price /////////////////
    class func saveSubsriptionPrice(subsriptionPrice: String){
        defaults.set(subsriptionPrice, forKey: PMHelperConstant.userDefaultSubscriptionPrice)
        defaults.synchronize()
    }
    
    class func getSubsriptionPrice()->String?{
        let subsriptionPrice:String?
        subsriptionPrice = defaults.object(forKey: PMHelperConstant.userDefaultSubscriptionPrice) as? String
        return subsriptionPrice ?? nil
    }
    
    ///////////////// GET & SAVE Stripe fees /////////////////
    class func saveStripeCharges(subsriptionPrice: String){
        defaults.set(subsriptionPrice, forKey: PMHelperConstant.userDefaultStripePrice)
        defaults.synchronize()
    }
    
    class func getStripeCharges()->String?{
        let subsriptionPrice:String?
        subsriptionPrice = defaults.object(forKey: PMHelperConstant.userDefaultStripePrice) as? String
        return subsriptionPrice ?? nil
    }
    
    ///////////////// GET & SAVE Stripe fees /////////////////
       class func savePlanId(subsriptionPrice: String){
           defaults.set(subsriptionPrice, forKey: PMHelperConstant.userDefaultPlanId)
           defaults.synchronize()
       }
       
       class func getPlanId()->String?{
           let subsriptionPrice:String?
           subsriptionPrice = defaults.object(forKey: PMHelperConstant.userDefaultPlanId) as? String
           return subsriptionPrice ?? nil
       }
}

//
//  PMMyRefundModel.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  RefundListRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class RefundListRequest : Mappable {
    
    var page_id: String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        
        page_id <- map["pageid"]
    }
}

/**
 *  RefundListResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class RefundListResponse : APIResponse {
    
    var data:[RefundListDataResponse]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> RefundListResponse?{
        var refundListResponse:RefundListResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<RefundListResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    refundListResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<RefundListResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        refundListResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return refundListResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        super.mapping(map: map)
        data         <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class RefundListDataResponse: Mappable{
        
        var purchase_id:String?
        var serviceusername:String?
        var transactionid:String?
        var service_name:String?
        var date:String?
        var purchase_package:String?
        var purchase_data:PurchaseDataResponse?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->RefundListDataResponse?{
            var refundListDataResponse:RefundListDataResponse?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<RefundListDataResponse>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        refundListDataResponse = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<RefundListDataResponse>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            refundListDataResponse = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return refundListDataResponse ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            purchase_id         <- map["purchase_id"]
            serviceusername     <- map["serviceusername"]
            transactionid       <- map["transactionid"]
            service_name        <- map["service_name"]
            date                <- map["date"]
            purchase_package    <- map["purchase_package"]
            purchase_data       <- map["purchase_data"]
        }
    }
    
    /**
     *  Sub-class PurchaseDataResponse is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class PurchaseDataResponse: Mappable{
        var service_id:String?
        var service_user:String?
        var stripefees:NSNumber?
        var serviceamount:NSNumber?
        var withstripefees:NSNumber?
        var creditloyalty:NSNumber?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PurchaseDataResponse?{
            var purchaseDataResponse:PurchaseDataResponse?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<PurchaseDataResponse>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        purchaseDataResponse = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<PurchaseDataResponse>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            purchaseDataResponse = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return purchaseDataResponse ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            service_id      <- map["service_id"]
            service_user    <- map["service_user"]
            stripefees      <- map["stripefees"]
            serviceamount   <- map["serviceamount"]
            withstripefees  <- map["withstripefees"]
            creditloyalty   <- map["creditloyalty"]
            
        }
    }
}

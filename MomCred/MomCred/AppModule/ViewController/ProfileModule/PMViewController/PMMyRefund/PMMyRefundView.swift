//
//  PMMyRefundView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMMyRefundView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblReviewCount : UILabel!
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    
    //MARK:- Var(s)
    var presenter       =       PMMyRefundPresenter()
    
    var currentPageIndex = 1
    
//    fileprivate var reviewListViewController:ReviewListViewController?
    fileprivate var isDisplayLoader = true
    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    
    var purchaseList:[RefundListResponse.RefundListDataResponse]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add refresh loader for pulling and refresh
        refreshView.delegate = self
        tableView.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        
        self.presenter.connectView(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.reviewListViewController = nil
        
        if(purchaseList == nil || (purchaseList != nil && (purchaseList?.count)! == 0)){
            Spinner.show()
            self.serverRequest()
        }
    }
    
}


extension PMMyRefundView: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .pulling(offset, threshould):
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                let refundListRequest = self.getRefundListRequest()
                self.presenter.serverRefundList(refundListRequest: refundListRequest, callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            let refundListRequest = self.getRefundListRequest()
            self.presenter.serverRefundList(refundListRequest: refundListRequest, callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}

extension PMMyRefundView{
    
    fileprivate func serverRequest(){
        let refundListRequest = self.getRefundListRequest()
        self.presenter.serverRefundList(refundListRequest: refundListRequest,callback: {
            (status, response, message) in
        })
    }
    
    fileprivate func getRefundListRequest() -> RefundListRequest{
        let refundListRequest = RefundListRequest()
        refundListRequest.page_id = "\(currentPageIndex)"
        return refundListRequest
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PMMyRefundView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return purchaseList != nil ? (purchaseList?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let data       =    self.arrNoteList[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: PurchaseListCell.nameOfClass, for: indexPath) as! PurchaseListCell
        
        let purchase = purchaseList?[indexPath.row]
        
        
        cell.lblTitle.text = purchase?.serviceusername ?? ""
        cell.lblSubTitle.text = purchase?.service_name ?? ""
        
        cell.lblTranscationNum.text = purchase?.transactionid ?? ""
//        cell.lblTranscationDate.text = purchase?.date ?? ""
        
        cell.lblTranscationDate.text = " \(DateTimeUtils.sharedInstance.toLocalDate(purchase?.date?.trim() ?? "", with: "yyyy-MM-dd hh:mm:ss")?.appSpecificStringFromFormat() ?? "")"
        
        let amount = String(format: "$ %.2f", purchase?.purchase_data?.serviceamount?.doubleValue ?? 0.0)
        cell.lblAmount.text = "$ \(amount)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
//        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if(self.reviewListViewController == nil){
//            //TODO: For testing open Review screen
//            self.reviewListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ProductDetailStoryboard, viewControllerName: ReviewListViewController.nameOfClass) as ReviewListViewController
//            self.navigationController?.pushViewController(self.reviewListViewController!, animated: true)
//        }
    }
}


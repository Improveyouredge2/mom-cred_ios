//
//  PMChangePasswordPresenter.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMChangePasswordPresenter {
    
    var view:PMChangePasswordView! // Object of change password view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key change password view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PMChangePasswordView) {
        self.view = view
    }
    
    /**
     *  update user password information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(){
        
        let userId   = PMUserDefault.getUserId()
        let token = PMUserDefault.getAppToken()
        
        let changePasswordRequestParam = PMChangePassword(user_id: PMUserDefault.getUserId(), token: PMUserDefault.getAppToken(), new_password: self.view.inputNewPassword.text() ?? "", old_password: self.view.inputOldPassword.text() ?? "")
        
        PMChangePasswordService.updateData(changePassword: changePasswordRequestParam, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.prepareScreenInfo()
                    
                    self.view.navigationController?.popViewController(animated: true)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

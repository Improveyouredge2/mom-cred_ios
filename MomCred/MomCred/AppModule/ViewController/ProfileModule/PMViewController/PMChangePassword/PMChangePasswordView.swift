//
//  PMChangePasswordView.swift
//  ProfileLib
//
//  Copyright © 2019 consagous. All rights reserved.
//

import Foundation
import UIKit

class PMChangePasswordView: LMBaseViewController {
    
    @IBOutlet weak var inputOldPassword: RYFloatingInput!
    @IBOutlet weak var inputNewPassword: RYFloatingInput!
    @IBOutlet weak var inputConfirmPassword: RYFloatingInput!
    
    @IBOutlet weak var buttonSave:UIButton!
    
    // Class object of presenter class
    fileprivate let presenter = PMChangePasswordPresenter()
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        inputOldPassword.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .warningColor(.white)
                .placeholer(LocalizationKeys.old_password.getLocalized())
                .maxLength(HelperConstant.LIMIT_UPPER_PASSWORD, onViolated: (message: "", callback: nil))
                .secure(true)
                .build()
        )
        
        inputNewPassword.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .warningColor(.white)
                .placeholer(LocalizationKeys.new_password.getLocalized())
                .maxLength(HelperConstant.LIMIT_UPPER_PASSWORD, onViolated: (message: "", callback: nil))
                .secure(true)
                .build()
        )
        
        inputConfirmPassword.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .warningColor(.white)
                .placeholer(LocalizationKeys.confirm_password.getLocalized())
                .maxLength(HelperConstant.LIMIT_UPPER_PASSWORD, onViolated: (message: "", callback: nil))
                .secure(true)
                .build()
        )
        
        presenter.connectView(view: self)
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
    }
}

// MARK:- Action method implementation
extension PMChangePasswordView{
    
    /**
     *  Back button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodBackAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     *  Submit button click event action method. Method check all validation of app requirements and submit action call.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodSubmitAction(_ sender: UIButton) {
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            var isFound = true
            
            //        textFieldOldPassword.resignFirstResponder()
            //        textFieldNewPassword.resignFirstResponder()
            //        textFieldConfirmPassword.resignFirstResponder()
            inputOldPassword.resignFirstResponder()
            inputNewPassword.resignFirstResponder()
            inputConfirmPassword.resignFirstResponder()
            
            // checking condition
            var message = ""
            if (inputOldPassword.text()?.isEmpty)! {
                message = LocalizationKeys.passwordOldEmpty.getLocalized()
                isFound = false
            } else if((inputOldPassword.text()?.length)! < HelperConstant.LIMIT_LOWER_PASSWORD){
                message = LocalizationKeys.oldpasswordLowerLimit.getLocalized()
                isFound = false
            } else if (inputNewPassword.text()?.isEmpty)! {
                message = LocalizationKeys.passwordNewEmpty.getLocalized()
                isFound = false
            } else if((inputNewPassword.text()?.length)! < HelperConstant.LIMIT_LOWER_PASSWORD){
                message = LocalizationKeys.newpasswordLowerLimit.getLocalized()
                isFound = false
            } else if (inputConfirmPassword.text()?.isEmpty)! {
                message = LocalizationKeys.confirmPassworEmpty.getLocalized()
                isFound = false
            }else if((inputConfirmPassword.text()?.length)! < HelperConstant.LIMIT_LOWER_PASSWORD){
                message = LocalizationKeys.confirmPassworEmpty.getLocalized()
                isFound = false
            } else if (inputNewPassword.text()?.compare(inputConfirmPassword.text()!) != ComparisonResult.orderedSame) {
                message = LocalizationKeys.passwordNotMatch.getLocalized()
                isFound = false
            }
            
            if !isFound {
                Helper.sharedInstance.showAlertViewControllerWith(title: "Error", message: message, buttonTitle: "OK", controller: self)
            }
            
            if isFound {
                self.presenter.submitData()
            }
        }
    }
}


extension PMChangePasswordView{
    /**
     *  Method update empty information on screen.
     *
     *  @param key empty.
     *
     *  @return empty
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func prepareScreenInfo(){
//        textFieldOldPassword.text = ""
//        textFieldNewPassword.text = ""
//        textFieldConfirmPassword.text = ""
        
        self.inputOldPassword.input.text = ""
        self.inputNewPassword.input.text = ""
        self.inputConfirmPassword.input.text = ""
    }
}

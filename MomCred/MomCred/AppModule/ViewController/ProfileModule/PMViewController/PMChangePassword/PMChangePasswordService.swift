//
//  PMChangePasswordService.swift
//  ProfileLib
//
//  Created by Apple_iOS on 20/01/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMChangePasswordService{
    
    /**
     *  Method update new password on server.
     *
     *  @param key new password. Callback method to update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func updateData(changePassword: PMChangePassword?, callback:@escaping (_ status:Bool, _ response: ProfileResponse?, _ message: String?) -> Void) {
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = changePassword?.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_CHANGE_PASSWORD, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:ProfileResponse? = ProfileResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
            
        }
        
    }
}

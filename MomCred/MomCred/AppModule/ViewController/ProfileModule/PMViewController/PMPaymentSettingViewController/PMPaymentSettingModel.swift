//
//  PMPaymentSettingModel.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/02/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  RedeemCodeRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PaymentSettingRequest : Mappable {
    
    var name: String?
    var email: String?
    var routing: String?
    var accountnu: String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        
        name        <- map["name"]
        email       <- map["email"]
        routing     <- map["routing"]
        accountnu   <- map["accountnu"]
    }
}

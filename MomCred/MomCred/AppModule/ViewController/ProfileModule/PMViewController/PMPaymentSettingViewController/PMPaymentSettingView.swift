//
//  PMPaymentSettingView.swift
//  MomCred
//
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftEventBus
import SDWebImage

class PMPaymentSettingView: LMBaseViewController {
    
    
    //MARK:- IBOutlet
    //MARK:-
    @IBOutlet weak var inputCountry: RYFloatingInput!
    @IBOutlet weak var inputAccountType: RYFloatingInput!
    @IBOutlet weak var inputAccountHolderName: RYFloatingInput!
    @IBOutlet weak var inputEmail: RYFloatingInput!
    @IBOutlet weak var inputRoutingNumber: RYFloatingInput!
    @IBOutlet weak var inputAccountNumber: RYFloatingInput!
    
    @IBOutlet weak var saveContainer : UIView!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    // Class object of presenter class
    fileprivate let presenter = PMPaymentSettingPresenter()
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        // Do any additional setup after loading the view.
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setScreenData()
    }
    
    // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.imgBackGround.isHidden = true
        self.view.endEditing(true)
        SwiftEventBus.unregister(self)
    }
}

//MARK:- Fileprivate method implementation
//MARK:-
extension PMPaymentSettingView {
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        inputCountry.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("US")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Country")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputCountry.isUserInteractionEnabled = false
        
        inputAccountType.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("Individual")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Account Type")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputAccountType.isUserInteractionEnabled = false
        
        
        let loginInfo:NSDictionary? = PMUserDefault.getLoginInfo()
        let  loginData:LoginRespone? = LoginRespone().getModelObjectFromServerResponse(jsonResponse: loginInfo!)
        
        if(loginData != nil && loginData?.loginResponseData != nil && loginData?.loginResponseData?.account_detail != nil){
            
            let accountDetail = loginData?.loginResponseData?.account_detail
            
            inputAccountHolderName.setup(setting:
                RYFloatingInputSetting.Builder.instance()
                    .backgroundColor(.clear)
                    .text(accountDetail?.name ?? "")
                    .accentColor(.white)
                    .warningColor(.white)
                    .placeholer("Account Holder FullName")
                    .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                    .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                    .build()
            )
            
            inputEmail.setup(setting:
                RYFloatingInputSetting.Builder.instance()
                    .backgroundColor(.clear)
                    .text(accountDetail?.email ?? "")
                    .accentColor(.white)
                    .warningColor(.white)
                    .placeholer(LocalizationKeys.email.getLocalized())
                    .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                    .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                    .build()
            )
            inputEmail.input.keyboardType = .emailAddress
            
            
            inputRoutingNumber.setup(setting:
                RYFloatingInputSetting.Builder.instance()
                    .backgroundColor(.clear)
                    .text(accountDetail?.routing ?? "")
                    .accentColor(.white)
                    .warningColor(.white)
                    .placeholer("Routing Number")
                    .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                    .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                    .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                    .build()
            )
            inputRoutingNumber.input.keyboardType = .numberPad
            
            inputAccountNumber.setup(setting:
                RYFloatingInputSetting.Builder.instance()
                    .backgroundColor(.clear)
                    .text(accountDetail?.accountnu ?? "")
                    .accentColor(.white)
                    .warningColor(.white)
                    .placeholer("Account Number")
                    .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                    .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                    .build()
            )
            inputAccountNumber.input.keyboardType = .numberPad
            
            self.updateSaveBtnStatus(isAllowSave: false)
            
        } else {
            inputAccountHolderName.setup(setting:
                RYFloatingInputSetting.Builder.instance()
                    .backgroundColor(.clear)
                    .accentColor(.white)
                    .warningColor(.white)
                    .placeholer("Account Holder FullName")
                    .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                    .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                    .build()
            )
            
            inputEmail.setup(setting:
                RYFloatingInputSetting.Builder.instance()
                    .backgroundColor(.clear)
                    //                .text(data?.email ?? "")
                    .accentColor(.white)
                    .warningColor(.white)
                    .placeholer(LocalizationKeys.email.getLocalized())
                    .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                    .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                    .build()
            )
            inputEmail.input.keyboardType = .emailAddress
            
            
            inputRoutingNumber.setup(setting:
                RYFloatingInputSetting.Builder.instance()
                    .backgroundColor(.clear)
                    .accentColor(.white)
                    .warningColor(.white)
                    .placeholer("Routing Number")
                    .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                    .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                    .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                    .build()
            )
            inputRoutingNumber.input.keyboardType = .numberPad
            
            inputAccountNumber.setup(setting:
                RYFloatingInputSetting.Builder.instance()
                    .backgroundColor(.clear)
                    .accentColor(.white)
                    .warningColor(.white)
                    .placeholer("Account Number")
                    .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                    .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                    .build()
            )
            inputAccountNumber.input.keyboardType = .numberPad
            
            self.updateSaveBtnStatus(isAllowSave: true)
        }
    }
    
    func updateSaveBtnStatus(isAllowSave:Bool){
        if(isAllowSave){
            self.btnDelete.isHidden = true
            
            self.inputAccountHolderName.input.text = ""
            self.inputEmail.input.text = ""
            self.inputRoutingNumber.input.text = ""
            self.inputAccountNumber.input.text = ""
            
        } else {
            self.btnDelete.isHidden = false
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func performValidation() -> Bool{
        
        if (self.inputCountry.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.error_payment_country.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputAccountType.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.error_payment_account_type.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputAccountHolderName.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.error_payment_account_fullname.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputEmail.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.empty_email.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if !(inputEmail.text()?.trim().isEmail())! && (inputEmail.text()?.trim().count)! > 0{
            let message = LocalizationKeys.invalidEmail.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputRoutingNumber.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.error_payment_rounting_number.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputAccountNumber.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.error_payment_account_number.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        return true
    }
}

//MARK:- Actions method implementation
//MARK:-
extension PMPaymentSettingView {
    
    /**
     *  Edit button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnEditAction(_ sender : UIButton){
        
    }
    
    /**
     *  Save button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnSaveAction(_ sender : UIButton){
        
        //        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
        
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            if performValidation() {
                presenter.submitData()
            }
        }
    }
}

//MARK: PMProfileViewInterface Method implementation
//MARK:-
extension PMPaymentSettingView: PMEditProfileViewInterface {
    /**
     *  Start windless animation on controls.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showLoading(){
        //        self.view.windless.start()

        Spinner.show()
    }
    
    /**
     *  End windless animation on controls.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func hideLoading(){
        //        self.view.windless.end()
        Spinner.hide()
    }
    
    /**
     *  Implement screen controls with empty data.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showEmptyView(){
        
    }
}

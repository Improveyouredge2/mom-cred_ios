//
//  PMPaymentSettingService.swift
//  ProfileLib
//
//  Created by Apple_iOS on 15/01/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMPaymentSettingService  {
    
    
    /**
     *  Method upload screen information on server with profile data and user profiel image.
     *
     *  @param key profile information and selected image for user profile. Callback method to update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func updateData(paymentSettingRequest: PaymentSettingRequest?, callback:@escaping (_ status:Bool, _ response: APIResponse?, _ message: String?) -> Void) {
        
        // Post
        let strJSON = paymentSettingRequest?.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_PAYMENT_SETTING, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        Spinner.show()
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: false) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:APIResponse? = APIResponse().getModelObjectFromServerResponse(response: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

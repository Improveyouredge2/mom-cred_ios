//
//  PMPaymentSettingPresenter.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMPaymentSettingPresenter {
    
    var view:PMPaymentSettingView! // Object of profile view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key profile view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PMPaymentSettingView) {
        self.view = view
    }
        
   
    /**
     *  update user profile information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(){
        
        let userId   = PMUserDefault.getUserId()
        let token = PMUserDefault.getAppToken()
        
        let paymentSettingRequest = PaymentSettingRequest()
        
        paymentSettingRequest.name = self.view.inputAccountHolderName.text() ?? ""
        paymentSettingRequest.email = self.view.inputEmail.text() ?? ""
        paymentSettingRequest.routing = self.view.inputRoutingNumber.text() ?? ""
        paymentSettingRequest.accountnu = self.view.inputAccountNumber.text() ?? ""
        
        PMPaymentSettingService.updateData(paymentSettingRequest: paymentSettingRequest, callback: {
            (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    
                    PMUserDefault.saveUserUserBank(isAdded: true)

                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.updateSaveBtnStatus(isAllowSave: !self.view.btnDelete.isHidden)
                    

//                    self.view.hideLoading()
                    Spinner.hide()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
      
    }
}

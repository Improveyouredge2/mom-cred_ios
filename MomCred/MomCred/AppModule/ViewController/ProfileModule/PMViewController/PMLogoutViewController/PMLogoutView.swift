//
//  PMLogoutView.swift
//  ProfileLib
//
//  Copyright © 2019 consagous. All rights reserved.
//

import Foundation
import UIKit

protocol PMLogoutViewDelegate {

    func cancelLogoutViewScr()
    func acceptLogoutViewScr()
}

class PMLogoutView: LMBaseViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var titleHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var buttonError: UIButton!
    
    var scrTitle:String?
    var scrDesc:String?
    
    var delegate:PMLogoutViewDelegate?
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        super.viewDidLoad()
        
        if(self.scrTitle != nil){
            self.lblTitle.text = self.scrTitle
        }
        
        if(self.scrDesc != nil){
            self.lblDesc.text = self.scrDesc
        }
    }
    
}

extension PMLogoutView {
    
    /**
     *  Close button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if(self.delegate != nil){
                self.delegate?.cancelLogoutViewScr()
            }
        })
    }
    
    /**
     *  Cancel button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if(self.delegate != nil){
                self.delegate?.cancelLogoutViewScr()
            }
        })
    }
    
    /**
     *  Ok button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodOkAction(_ sender: UIButton) {
//        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            self.dismiss(animated: true, completion: {
                if(self.delegate != nil){
                    self.delegate?.acceptLogoutViewScr()
                }
            })
//        }
    }
}

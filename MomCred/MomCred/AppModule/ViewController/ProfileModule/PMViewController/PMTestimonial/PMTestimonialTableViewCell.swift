//
//  PMTestimonialTableViewCell.swift
//  MomCred
//
//  Created by consagous on 05/06/19.
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit
import Foundation

class PMTestimonialTableViewCell: UITableViewCell {
    @IBOutlet weak var cardView : ViewLayerSetup!
    @IBOutlet weak var imgProfilePic : ImageLayerSetup!
    @IBOutlet weak var lblDesciption : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var starRatingView : HCSStarRatingView!
    
    var testimonial: TestimonialItem? {
        didSet {
            imgProfilePic.image = UIImage(named: "appLogo")
            lblTitle.text = testimonial?.fullName
            lblDesciption.text = testimonial?.comment
            lblDate.text = testimonial?.date?.stringFromFormat("dd MMM yyyy")
            if let rating = testimonial?.rating, let ratingValue = Float(rating) {
                starRatingView.value = CGFloat(ratingValue)
            } else {
                starRatingView.value = 0
            }
        }
    }
}

//
//  PMTestimonialService.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  PMTestimonialService is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PMTestimonialService{
    
    /**
     *  Method to get banner list for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getRatingData(paginationRequest:PaginationRequest?, callback:@escaping (_ status:Bool, _ response: RatingDetailResponse?, _ message: String?) -> Void) {
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = paginationRequest?.toJSONString()
        
        // POST
        // Convert Model request object into JSONString
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_RATING, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:RatingDetailResponse? = RatingDetailResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    static func getTestimonials(callback:@escaping (_ status:Bool, _ response: TestimonialResponse?, _ message: String?) -> Void) {
        let reqPost = APIManager().sendGetRequest(urlString: APIKeys.API_GET_TESTIMONIALS, header: APIHeaders().getDefaultHeaders())
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            if status {
                if let responseDict = response {
                    let res: TestimonialResponse? = TestimonialResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

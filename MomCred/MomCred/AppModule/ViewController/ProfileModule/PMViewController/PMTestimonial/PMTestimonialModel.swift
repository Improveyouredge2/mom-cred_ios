//
//  PMTestimonialModel.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  RatingDetailResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class RatingDetailResponse : APIResponse {
    
    var ratingDetailDataList:[RatingDetailData]?
    var totalrating:String?
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> RatingDetailResponse?{
        var ratingDetailResponse:RatingDetailResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<RatingDetailResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    ratingDetailResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<RatingDetailResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        ratingDetailResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return ratingDetailResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        ratingDetailDataList    <- map["data"]
        totalrating             <- map["totalrating"]
    }
    
    /**
     *  ProductDetailData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class RatingDetailData: Mappable{
        
        var rate_id:String?
        var rate_star:String?
        var rate_comment:String?
        var rate_user:String?
        var rate_user_name:String?
        var rate_service_provider:String?
        var rate_status:String?
        var rate_date:String?
        var user_image:String?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        required init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->RatingDetailData?{
            var ratingDetailData:RatingDetailData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<RatingDetailData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        ratingDetailData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<RatingDetailData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            ratingDetailData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return ratingDetailData ?? nil
        }
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        func mapping(map: Map){
            
            rate_id                 <- map["rate_id"]
            rate_star               <- map["rate_star"]
            rate_comment            <- map["rate_comment"]
            rate_user               <- map["rate_user"]
            rate_user_name          <- map["username"]
            rate_service_provider   <- map["rate_service_provider"]
            rate_status             <- map["rate_status"]
            rate_date               <- map["rate_date"]
            user_image              <- map["user_image"]
        }
    }
}

class TestimonialItem: Mappable {
    var fullName: String?
    var comment: String?
    var rating: String?
    var date: Date?

    required init?(map: Map) { }
    
    func mapping(map: Map) {
        fullName <- map["full_name"]
        comment <- map["rate_comment"]
        rating <- map["rate_star"]
        if let dateStr: String = map["rate_date"].value() {
            date = dateStr.dateFromFormat("yyyy-MM-dd HH:mm:ss") //"2021-02-02 07:53:05"
        }
    }
}

class TestimonialResponse: APIResponse {
    var testimonials: [TestimonialItem]?
    var rating: Int?
    var ratingCount: Int?
    
    required init?(map: Map) { super.init(map: map) }
    
    required init() { super.init() }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        testimonials <- map["data.user_comment"]
        rating <- map["data.getrating"]
        ratingCount <- map["data.getratingcount"]
    }
}

//
//  PMTestimonialPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  PMTestimonialPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PMTestimonialPresenter {
    
    weak var view: PMTestimonialView! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PMTestimonialView) {
        self.view = view
    }
}

extension PMTestimonialPresenter{
    func getTestimonials(callback:@escaping (_ status:Bool, _ response: TestimonialResponse?, _ message: String?) -> Void) {
        PMTestimonialService.getTestimonials { (status, response, message) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if status {
                    self?.view.testimonialResponse = response
                }
                callback(status, response, message)
            }
        }
    }
}

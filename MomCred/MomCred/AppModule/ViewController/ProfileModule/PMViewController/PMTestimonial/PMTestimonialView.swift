//
//  PMTestimonialView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMTestimonialView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tblTestimonialList : UITableView!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblReviewCount : UILabel!
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var starRatingView : HCSStarRatingView!
    
    var testimonialResponse: TestimonialResponse? {
        didSet {
            if let ratingCount: Int = testimonialResponse?.ratingCount {
                if ratingCount == 1 {
                    lblReviewCount.text = "\(ratingCount) Review"
                } else {
                    lblReviewCount.text = "\(ratingCount) Reviews"
                }
            } else {
                lblReviewCount.text = "0 Reviews"
            }
            if let rating: Int = testimonialResponse?.rating {
                starRatingView.value = CGFloat(rating)
            } else {
                starRatingView.value = 0
            }
            tblTestimonialList.reloadData()
        }
    }
    
    var currentPageIndex = 0
    var isAdvanceSearchResultShow = false
    
    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var isDisplayLoader = true
    
    //MARK:- Var(s)
    var presenter       =       PMTestimonialPresenter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshView.delegate = self
        tblTestimonialList.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tblTestimonialList.addPullLoadableView(loadMoreView, type: .loadMore)
        
        self.presenter.connectView(view: self)
        tblTestimonialList.estimatedRowHeight = 100
        tblTestimonialList.rowHeight = UITableView.automaticDimension
        
        let userName = PMUserDefault.getUserFirstName()
        if(userName != nil){
            let profileImageUrl = PMUserDefault.getProfilePic()
            self.lblTitle.text = userName
            self.lblSubTitle.text = PMUserDefault.getUserName()
            
            if(profileImageUrl != nil){
                self.imgProfile.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
            }
        }
        
        self.lblReviewCount.text = "0 Reviews"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Spinner.show()
        presenter.getTestimonials { (status, response, message) in
            DispatchQueue.main.async {
                Spinner.hide()
            }
        }
    }
}

extension PMTestimonialView: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .pulling(offset, threshould):
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                presenter.getTestimonials { (status, response, message) in
                    completionHandler()
                }
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            presenter.getTestimonials { (status, response, message) in
                completionHandler()
            }
            break
        }
    }
}


//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PMTestimonialView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testimonialResponse?.testimonials?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PMTestimonialTableViewCell.nameOfClass, for: indexPath) as! PMTestimonialTableViewCell
        cell.testimonial = testimonialResponse?.testimonials?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }    
}

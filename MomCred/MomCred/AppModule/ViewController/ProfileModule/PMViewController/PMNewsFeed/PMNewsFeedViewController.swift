//
//  PMNewsFeedViewController.swift
//  MomCred
//
//  Created by MD on 18/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class PMNewsFeedViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tblNewsFeed : UITableView!

    //MARK:- fileprivate
    private let refreshView = KRPullLoadView()
    private let loadMoreView = KRPullLoadView()
    private var isDownloadWorking = false
    private var isDisplayLoader = true
    
    //MARK:- Var(s)
    private var presenter = PMNewsFeedPresenter()

    var newsFeedItems: [PMNewsFeedItem]?
    var currentPageIndex = 1
    var shouldRefreshContent: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshView.delegate = self
        loadMoreView.delegate = self
        tblNewsFeed.addPullLoadableView(refreshView, type: .refresh)
        tblNewsFeed.addPullLoadableView(loadMoreView, type: .loadMore)
        
        presenter.connectView(view: self)
        tblNewsFeed.estimatedRowHeight = 100
        tblNewsFeed.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if shouldRefreshContent {
            shouldRefreshContent = false
            currentPageIndex = 1
            Spinner.show()
            presenter.getNewsFeed()
        }
    }
    
    @IBAction private func btnCreateNewsAction(sender: UIButton) {
        showCreateNewsView()
    }
    
    private func showCreateNewsView(for item: PMNewsFeedItem? = nil) {
        let controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMNewsFeedCreateViewController.nameOfClass) as PMNewsFeedCreateViewController
        controller.newsItem = item
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension PMNewsFeedViewController: KRPullLoadViewDelegate{
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case .pulling(_, _):
                print("Pulling")
            case .loading:
                isDisplayLoader = false
                currentPageIndex += 1
                presenter.getNewsFeed()
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                currentPageIndex = 1
            }
            isDisplayLoader = false
        case .loading:
            pullLoadView.messageLabel.text = "Updating..."
            presenter.getNewsFeed()
        }
    }
}


//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PMNewsFeedViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsFeedItems?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PMNewsFeedTableViewCell.nameOfClass, for: indexPath) as! PMNewsFeedTableViewCell
        cell.feedItem = newsFeedItems?[indexPath.row]
        return cell
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let feedItem = newsFeedItems?[indexPath.row]
        showCreateNewsView(for: feedItem)
    }
}

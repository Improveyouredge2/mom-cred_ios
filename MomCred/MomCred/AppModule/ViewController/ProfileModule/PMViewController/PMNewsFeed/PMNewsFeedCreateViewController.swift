//
//  PMNewsFeedCreateViewController.swift
//  MomCred
//
//  Created by MD on 19/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SDWebImage

class PMNewsFeedCreateViewController: LMBaseViewController {
    private struct Constants {
        static let dateFormat: String = "dd MMM yyyy"
        static let timeFormat: String = "hh:mm a"
        static let dateTimeFormat: String = "dd MMM yyyy hh:mm a"
    }
    
    @IBOutlet private weak var imgThumb: UIImageView!
    @IBOutlet private weak var imgVideoThumb: UIImageView!
    @IBOutlet private weak var txtNews: RYFloatingInput!
    @IBOutlet private weak var txtDate: RYFloatingInput!
    @IBOutlet private weak var txtTime: RYFloatingInput!
    @IBOutlet private weak var btnSave: UIButton!

    private var newsDate: Date?
    private var newsVideo: Data?
    private let disposeBag = DisposeBag()

    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        return formatter
    }()

    var newsItem: PMNewsFeedItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupField(field: txtNews, placeholder: "News")
        setupField(field: txtDate, placeholder: "Date", mode: .date)
        setupField(field: txtTime, placeholder: "Time", mode: .time)
        
        setupImageView(imgThumb)
        setupImageView(imgVideoThumb)
        
        updateUI()
    }
    
    private func updateUI() {
        if let newsItem = newsItem {
            lbl_NavigationTitle.text = "Edit News"
            txtNews.input.text = newsItem.feed_text
            if let secondsStr = newsItem.news_feed_date, let seconds = Double(secondsStr) {
                let date = Date(timeIntervalSince1970: seconds)
                newsDate = date
                txtDate.input.text = date.stringFromFormat(Constants.dateFormat)
                txtTime.input.text = date.stringFromFormat(Constants.timeFormat)
            }
            
            if let imgURL = newsItem.news_feed_picture, let url = URL(string: imgURL) {
                imgThumb.sd_setImage(with: url, completed: nil)
            }
            if let videoURL = newsItem.news_feed_video, let url = URL(string: videoURL) {
                if let image = SDImageCache.shared.imageFromCache(forKey: videoURL) {
                    imgVideoThumb.image = image
                } else {
                    url.getVideoThumbnail { [weak self] image in
                        if let image = image {
                            SDImageCache.shared.store(image, forKey: videoURL, toDisk: true, completion: nil)
                            self?.imgVideoThumb.image = image
                        }
                    }
                }
            }
        } else {
            lbl_NavigationTitle.text = "Add News"
        }
    }
    
    private func setupField(field: RYFloatingInput, placeholder: String) {
        field.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer(placeholder)
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        field.input.rx.controlEvent([.editingDidEnd, .editingDidBegin, .editingChanged]).subscribe(onNext: { [weak self] _ in
            if let datePicker = field.input.inputView as? UIDatePicker {
                self?.dateChanged(datePicker)
            }
        }).disposed(by: disposeBag)
    }
    
    private func setupField(field: RYFloatingInput, placeholder: String, mode: UIDatePicker.Mode) {
        setupField(field: field, placeholder: placeholder)
        
        let picker: UIDatePicker = .init()
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }
        picker.datePickerMode = mode
        picker.timeZone = NSTimeZone.local
        picker.backgroundColor = UIColor.white
        field.input.inputView = picker
    }
    
    @objc private func dateChanged(_ datePicker: UIDatePicker) {
        if datePicker == txtDate.input.inputView {
            txtDate.input.text = datePicker.date.stringFromFormat(Constants.dateFormat)
        } else if datePicker == txtTime.input.inputView {
            txtTime.input.text = datePicker.date.stringFromFormat(Constants.timeFormat)
        }
        
        if let date = txtDate.input.text, let time = txtTime.input.text, !date.trim().isEmpty, !time.trim().isEmpty {
            let dateTimeStr = "\(date) \(time)"
            newsDate = dateTimeStr.dateFromFormat(Constants.dateTimeFormat)
        }
    }
    
    private func setupImageView(_ imgView: UIImageView) {
        imgView.image = nil
        imgView.isUserInteractionEnabled = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showImagePicker(gesture:)))
        imgView.addGestureRecognizer(gesture)
    }
    
    @objc private func showImagePicker(gesture: UITapGestureRecognizer) {
        displayPhotoSelectionOption(withCircularAllow: false, isSelectVideo: gesture.view == imgVideoThumb, onlyVideos: gesture.view == imgVideoThumb)
    }
    
    override func pickedImage(fromImagePicker: UIImage, imageData: Data, mediaType: String, imageName: String, fileUrl: String?) {
        if mediaType == "1" {
            imgThumb.image = fromImagePicker
        } else if mediaType == "2" {
            newsVideo = imageData
            imgVideoThumb.image = fromImagePicker
        }
    }
    
    @IBAction private func btnSaveAction(sender: UIButton) {
        if let image = imgThumb.image, let imageData = image.pngData() {
            if let newsText = txtNews.input.text, !newsText.trim().isEmpty {
                if let newsDate = newsDate {
                    PMNewsFeedCreateService.createNews(id: newsItem?.id, text: newsText, newsDate: newsDate, image: imageData, video: newsVideo) { status in
                        DispatchQueue.main.async { [weak self] in
                            let viewC = self?.navigationController?.viewControllers.first { $0 is PMNewsFeedViewController }
                            if let viewC = viewC as? PMNewsFeedViewController {
                                viewC.shouldRefreshContent = true
                            }
                            Spinner.hide()
                            self?.navigationController?.popViewController(animated: true)
                        }
                    }
                } else {
                    showBannerAlertWith(message: "Please select news date and time.", alert: .error)
                }
            } else {
                showBannerAlertWith(message: "Please enter news text.", alert: .error)
            }
        } else {
            showBannerAlertWith(message: "Please select a news image.", alert: .error)
        }
    }
}

//
//  PMNewsFeedCreateService.swift
//  MomCred
//
//  Created by MD on 20/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation

class PMNewsFeedCreateService {
    class func createNews(id: String?, text: String, newsDate: Date, image: Data, video: Data?, completion: @escaping (Bool) -> Void) {
        var params: [String: Any] = ["feed_text": text, "news_feed_date": newsDate.timeIntervalSince1970, ]
        if let id = id, !id.trim().isEmpty {
            params["id"] = id
        }

        var uploadList = [UploadImageData]()
        let imageData = UploadImageData()
        imageData.imageName = "temp_\(Date().timeIntervalSince1970).png"
        imageData.imageKeyName = "news_feed_picture"
        imageData.imageData = image
        uploadList.append(imageData)

        if let video = video {
            let videoData = UploadImageData()
            videoData.imageName = "temp_\(Date().timeIntervalSince1970).mp4"
            videoData.imageKeyName = "news_feed_video"
            videoData.imageData = video
            uploadList.append(videoData)
        }

        let reqMultiForm = APIManager().sendPostMultiFormRequest(urlString: APIKeys.API_ADD_NEWS_FEED, header: APIHeaders().getDefaultHeaders(), formDataParameter: params, uploadImageList: uploadList)
        APIManager.request(urlRequest: reqMultiForm, isDisplayLoader: true) { (status, response, error) in
            completion(status)
        }
    }
}

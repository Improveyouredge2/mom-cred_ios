//
//  PMNewsFeedTableViewCell.swift
//  MomCred
//
//  Created by MD on 18/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit
import AVKit
import SDWebImage
import Lightbox

class PMNewsFeedTableViewCell: UITableViewCell {
    @IBOutlet private weak var imgThumb: UIImageView! { didSet { setupImageView(imgThumb) } }
    @IBOutlet private weak var imgVideoThumb: UIImageView! { didSet { setupImageView(imgVideoThumb) } }
    @IBOutlet private weak var imgPlayBtn: UIImageView! { didSet { imgPlayBtn.isHidden = true } }
    @IBOutlet private weak var lblTitle: UILabel! { didSet { lblTitle.text = "" } }
    @IBOutlet private weak var lblDate: UILabel! { didSet { lblDate.text = "" } }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lblTitle.text = ""
        lblDate.text = ""
        imgThumb.image = nil
        imgPlayBtn.isHidden = true
        imgVideoThumb.image = nil
    }
    
    var feedItem: PMNewsFeedItem? {
        didSet {
            lblTitle.text = feedItem?.feed_text?.trim()
            if let secondsStr = feedItem?.news_feed_date, let seconds = Double(secondsStr) {
                lblDate.text = Date(timeIntervalSince1970: seconds).appSpecificStringFromFormatWithTime()
            }
            if let imgURL = feedItem?.news_feed_picture, let url = URL(string: imgURL) {
                imgThumb.sd_setImage(with: url, completed: nil)
            }
            if let videoURL = feedItem?.news_feed_video, let url = URL(string: videoURL) {
                imgPlayBtn.isHidden = false
                if let image = SDImageCache.shared.imageFromCache(forKey: videoURL) {
                    imgVideoThumb.image = image
                } else {
                    url.getVideoThumbnail { [weak self] image in
                        if let image = image {
                            SDImageCache.shared.store(image, forKey: videoURL, toDisk: true, completion: nil)
                            if self?.feedItem?.news_feed_video == videoURL {
                                self?.imgVideoThumb.image = image
                            }
                        }
                    }
                }
            }
        }
    }
        
    private func setupImageView(_ imgView: UIImageView) {
        imgView.image = nil
        imgView.isUserInteractionEnabled = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showImageCarousal(gesture:)))
        imgView.addGestureRecognizer(gesture)
    }
    
    @objc private func showImageCarousal(gesture: UITapGestureRecognizer) {
        // Create an array of images.
        var images: [LightboxImage] = []
        
        if let imgURL = feedItem?.news_feed_picture, let url = URL(string: imgURL) {
            if let image = SDImageCache.shared.imageFromCache(forKey: imgURL) {
                images.append(LightboxImage(image: image))
            } else {
                images.append(LightboxImage(imageURL: url))
            }
        }
        if let videoURL = feedItem?.news_feed_video, let url = URL(string: videoURL) {
            if let image = SDImageCache.shared.imageFromCache(forKey: videoURL) {
                images.append(LightboxImage(image: image, text: "", videoURL: url))
            } else {
                let lightImage = LightboxImage(imageClosure: { [weak self] in
                    return self?.imgVideoThumb.image ?? UIImage(named: "appLogo")!
                }, text: "", videoURL: url)
                images.append(lightImage)
            }
        }

        var initialPage: Int = 0
        if gesture.view == imgVideoThumb, images.count > 1 {
            initialPage = 1
        }

        let controller = LightboxController(images: images, startIndex: initialPage)
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        controller.dynamicBackground = true

        HelperConstant.appDelegate.window?.rootViewController?.present(controller, animated: true, completion: nil)
        LightboxConfig.handleVideo = { from, videoURL in
            let videoController = AVPlayerViewController()
            videoController.player = AVPlayer(url: videoURL)
            
            from.present(videoController, animated: true) {
                videoController.player?.play()
            }
        }
    }
}

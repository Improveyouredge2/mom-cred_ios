//
//  PMNewsFeedService.swift
//  MomCred
//
//  Created by MD on 18/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation

class PMNewsFeedService {
    class func getNewsFeed(pageIndex: Int, callback:@escaping (_ status: Bool, _ response: PMNewsFeedModel?, _ message: String?) -> Void) {
        let strJSON = "{\"current_page\": \"\(pageIndex)\"}"
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_NEWS_FEED, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            if status {
                if let responseDict = response {
                    let res: PMNewsFeedModel? = PMNewsFeedModel().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

//
//  PMNewsFeedModel.swift
//  MomCred
//
//  Created by MD on 19/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import ObjectMapper

class PMNewsFeedItem: Mappable {
    var id: String?
    var user_id: String?
    var feed_text: String?
    var news_feed_picture: String?
    var news_feed_video: String?
    var news_feed_date: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        id <- map["id"]
        user_id <- map["user_id"]
        feed_text <- map["feed_text"]
        news_feed_picture <- map["news_feed_picture"]
        news_feed_video <- map["news_feed_video"]
        news_feed_date <- map["news_feed_date"]
    }
}

class PMNewsFeedModel: APIResponse {
    var newsItems: [PMNewsFeedItem]?

    override func mapping(map: Map) {
        super.mapping(map: map)
        newsItems <- map["data"]
    }
}

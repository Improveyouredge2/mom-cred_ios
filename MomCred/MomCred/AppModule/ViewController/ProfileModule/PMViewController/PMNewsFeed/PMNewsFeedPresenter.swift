//
//  PMNewsFeedPresenter.swift
//  MomCred
//
//  Created by MD on 18/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class PMNewsFeedPresenter {
    weak var view: PMNewsFeedViewController!
    
    func connectView(view: PMNewsFeedViewController) {
        self.view = view
    }
}

extension PMNewsFeedPresenter {
    func getNewsFeed() {
        PMNewsFeedService.getNewsFeed(pageIndex: view.currentPageIndex) { (status, response, message) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if status {
                    if let dataList = response?.newsItems {
                        if self?.view.currentPageIndex == 1 {
                            self?.view.newsFeedItems = dataList
                        } else {
                            self?.view.newsFeedItems?.append(contentsOf: dataList)
                        }
                    }
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
                self?.view?.tblNewsFeed.reloadData()
            }
        }
    }
}

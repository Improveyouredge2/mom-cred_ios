//
//  PMVoucherListService.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  PMVoucherListService is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PMVoucherListService{

    /**
     *  Method connect to get Purchase list data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverVoucherListing(voucherListRequest:VoucherListRequest?, callback:@escaping (_ status:Bool, _ response: VoucherListResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = voucherListRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_VOUCHER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:VoucherListResponse? = VoucherListResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

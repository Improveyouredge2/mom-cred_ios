//
//  PMVoucherSearchListPresenter.swift
//  MomCred
//
//  Created by MD on 28/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation

class PMVoucherSearchListPresenter {
    
    weak var view: PMVoucherSearchListViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PMVoucherSearchListViewController) {
        self.view = view
    }
}

extension PMVoucherSearchListPresenter{
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverVoucherListing(voucherListRequest:VoucherSearchListRequest?, callback:@escaping (_ status:Bool, _ response: VoucherListResponse?, _ message: String?) -> Void){
        
        PMVoucherListService.serverVoucherListing(voucherListRequest: voucherListRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    if(response != nil && response?.data != nil && (response?.data?.count)! > 0){
                        if(self.view.currentPageIndex == 0){
                            self.view.voucherList = []
                            self.view.voucherList = response?.data
                        } else {
                            self.view.voucherList?.append(contentsOf: response?.data ?? [])
                        }
                    }
                    
                    self.view.tableView.reloadData()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, nil, message)
                }
            }
        })
    }
}

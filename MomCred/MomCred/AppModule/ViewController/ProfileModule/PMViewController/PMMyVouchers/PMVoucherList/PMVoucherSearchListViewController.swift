//
//  PMVoucherSearchListViewController.swift
//  MomCred
//
//  Created by MD on 28/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation

class PMVoucherSearchListViewController: LMBaseViewController {
    
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.barTintColor = UIColor.clear
            searchBar.backgroundColor = UIColor.clear
            searchBar.isTranslucent = true
            searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
            searchBar.delegate = self
            
            if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField {
                searchTextField.textColor = UIColor.white
                searchTextField.attributedPlaceholder =  NSAttributedString(string: "Verification Number", attributes: [.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
            }
        }
    }

    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView : UITableView!
    
    var currentPageIndex = 0
    var voucherListRequest: VoucherSearchListRequest?
    
    //MARK:- Var(s)
    fileprivate var presenter = PMVoucherSearchListPresenter()
    fileprivate var isDisplayLoader = true
    
    //MARK:- fileprivate
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var voucherDetailsView:PMVoucherDetailsView?
    
    var voucherList:[PurchaseServiceVoucherDataResponse]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add refresh loader for pulling and refresh
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        
        presenter.connectView(view: self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
    }
}

extension PMVoucherSearchListViewController{
    
    fileprivate func getVoucherListRequest() -> VoucherSearchListRequest{
        if voucherListRequest == nil {
            let voucherListRequest = VoucherSearchListRequest()
            voucherListRequest.page_id = "\(currentPageIndex)"
            voucherListRequest.search = searchBar.text
            self.voucherListRequest = voucherListRequest
        } else {
            voucherListRequest?.page_id = "\(currentPageIndex)"
        }
        return voucherListRequest ?? VoucherSearchListRequest()
    }
    
    fileprivate func serverRequest(){
        let voucherListRequest = getVoucherListRequest()
        presenter.serverVoucherListing(voucherListRequest: voucherListRequest,callback: {
            (status, response, message) in
        })
    }
}


extension PMVoucherSearchListViewController: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .pulling(offset, threshould):
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                isDisplayLoader = false
                currentPageIndex += 1
                
                let voucherListRequest = getVoucherListRequest()
                presenter.serverVoucherListing(voucherListRequest: voucherListRequest, callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 currentPageIndex = 0
            }
            isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            let voucherListRequest = getVoucherListRequest()
            presenter.serverVoucherListing(voucherListRequest: voucherListRequest, callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}


//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PMVoucherSearchListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return voucherList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PMMyVoucherTableViewCell.nameOfClass, for: indexPath) as! PMMyVoucherTableViewCell
        cell.voucherItem = voucherList?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(voucherDetailsView == nil){
            voucherDetailsView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMVoucherDetailsView.nameOfClass) as PMVoucherDetailsView
            
            let obj = voucherList?[indexPath.row]
            voucherDetailsView?.voucher_id = obj?.voucher_id ?? ""
            navigationController?.pushViewController(voucherDetailsView!, animated: true)
        }
    }
}

extension PMVoucherSearchListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload(_:)), object: searchBar)
        perform(#selector(self.reload(_:)), with: searchBar, afterDelay: 0.75)
    }

    @objc func reload(_ searchBar: UISearchBar) {
        voucherListRequest = nil
        voucherList = nil
        currentPageIndex = 0

        guard let query = searchBar.text, query.trimmingCharacters(in: .whitespaces) != "" else {
            print("nothing to search")
            tableView.reloadData()
            return
        }

        print(query)
        let request = getVoucherListRequest()
        presenter.serverVoucherListing(voucherListRequest: request) { (status, response, message) in
        }
    }
}

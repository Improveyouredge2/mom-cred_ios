//
//  PMMyVoucherTableViewCell.swift
//  MomCred
//
//  Created by MD on 23/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit
import Lightbox

class InfoStackView: UIStackView {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
}

class PMMyVoucherTableViewCell: UITableViewCell {
    @IBOutlet private weak var lblPrice: UILabel!
    @IBOutlet private weak var lblDate: UILabel!
    @IBOutlet private weak var imgVoucher: UIImageView! { didSet { setupImageView(imgVoucher) } }
    @IBOutlet private weak var imgService: UIImageView! { didSet { setupImageView(imgService) } }
    @IBOutlet private weak var infoStackView: UIStackView!
    
    var voucherItem: PurchaseServiceVoucherDataResponse? {
        didSet {
            if let obj = voucherItem {
                var views: [UIView] = []
                if let number = obj.voucher_no {
                    let infoV = infoView(title: "Verification Number", value: number)
                    views.append(infoV)
                }
                if let number = obj.voucher_redeem_no {
                    let infoV = infoView(title: "Redeeming Number", value: number)
                    views.append(infoV)
                }
                if let name = obj.overview {
                    let infoV = infoView(title: "Service Name", value: name)
                    views.append(infoV)
                }
                if let name = obj.service_user_name {
                    let infoV = infoView(title: "Service Provider", value: name)
                    views.append(infoV)
                }
                
                
                if let serviceCategories = obj.service_field, !serviceCategories.isEmpty {
                    for fieldInfo in serviceCategories {
                        if let fieldCategory = fieldInfo.catFieldName, !fieldCategory.isEmpty, let specificField = fieldInfo.specificFieldName, !specificField.isEmpty {
                            let infoV1 = infoView(title: "Field Category", value: fieldCategory)
                            views.append(infoV1)

                            let infoV2 = infoView(title: "Specific Field", value: specificField)
                            views.append(infoV2)
                        }
                    }
                }
                if let name = obj.service_type {
                    let infoV = infoView(title: "Service Type", value: name)
                    views.append(infoV)
                }
                
                if let calendar_date = obj.calendar_date, calendar_date != ""  {
                    let infoV = infoView(title: "Calendar listing", value: calendar_date)
                    views.append(infoV)
                }

                if let voucherStatus: String = obj.voucher_status {
                    var status: String = ""
                    if voucherStatus == "1" {
                        status = "Redeemed"
                    } else if voucherStatus == "2" {
                        status = "Money back"
                    } else if voucherStatus == "3" {
                        status = "Refunded"
                    } else if voucherStatus == "4" {
                        status = "Money back guarantee pending"
                    } else if voucherStatus == "5" {
                        status = "Money back guarantee granted"
                    } else if voucherStatus == "6" {
                        status = "Money back guarantee denied"
                    } else {
                        status = "Refundable"
                    }

                    let magentaColor: UIColor = UIColor(hex: "C8057A")
                    let font: UIFont = UIFont(name: FontsConfig.FONT_TYPE_Regular, size: 20) ?? UIFont.systemFont(ofSize: 20)

                    let voucherStatusLabel: UILabel = UILabel()
                    voucherStatusLabel.text = status
                    voucherStatusLabel.numberOfLines = 2
                    voucherStatusLabel.font = font
                    voucherStatusLabel.textColor = magentaColor
                    views.append(voucherStatusLabel)
                }

                if let voucherDate = obj.voucher_date,let dateVal =  DateTimeUtils.sharedInstance.toLocalDate(voucherDate, with: "yyyy-MM-dd HH:mm:ss") {
                    lblDate.text = "Date Purchase \(dateVal.appSpecificStringFromFormatWithTime())"
                } else {
                    lblDate.text = ""
                }

                if let qrImage = obj.voucher_qr, let url = URL(string: qrImage) {
                    imgVoucher.setImage(url: url)
                } else if let qrImage = obj.qr_code, let url = URL(string: qrImage) {
                    imgVoucher.setImage(url: url)
                } else {
                    imgVoucher.image = nil
                }
                if let serviceImage = obj.service_pic, let url = URL(string: serviceImage) {
                    imgService.setImage(url: url)
                } else {
                    imgService.image = nil
                }
                if let price = obj.voucher_amount, let priceValue = Double(price) {
                    lblPrice.text = priceValue.formattedPrice
                } else {
                    lblPrice.text = 0.0.formattedPrice
                }
                
                infoStackView.removeAllArrangedSubviews()
                for view in views {
                    infoStackView.addArrangedSubview(view)
                }
            }
        }
    }
    
    private func infoView(title: String, value: String) -> InfoStackView {
        let info = Bundle.main.loadNibNamed("InfoStackView", owner: self, options: nil)?.first as! InfoStackView
        info.lblTitle.text = title
        info.lblValue.text = value
        return info
    }
    
    private func setupImageView(_ imgView: UIImageView) {
        imgView.image = nil
        imgView.isUserInteractionEnabled = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showImageCarousal(gesture:)))
        imgView.addGestureRecognizer(gesture)
    }
    
    @objc private func showImageCarousal(gesture: UITapGestureRecognizer) {
        // Create an array of images.
        var images: [LightboxImage] = []
        
        if let image = imgService.image {
            images.append(LightboxImage(image: image))
        } else if let pic = voucherItem?.service_pic, let url = URL(string: pic) {
            images.append(LightboxImage(imageURL: url))
        }
        if let image = imgVoucher.image {
            images.append(LightboxImage(image: image))
        } else if let pic = voucherItem?.qr_code, let url = URL(string: pic) {
            images.append(LightboxImage(imageURL: url))
        }  else if let pic = voucherItem?.voucher_qr, let url = URL(string: pic) {
            images.append(LightboxImage(imageURL: url))
        }
        
        var initialPage: Int = 0
        if gesture.view == imgVoucher, images.count > 1 {
            initialPage = 1
        }

        let controller = LightboxController(images: images, startIndex: initialPage)
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        controller.dynamicBackground = true
        HelperConstant.appDelegate.window?.rootViewController?.present(controller, animated: true, completion: nil)
    }
}

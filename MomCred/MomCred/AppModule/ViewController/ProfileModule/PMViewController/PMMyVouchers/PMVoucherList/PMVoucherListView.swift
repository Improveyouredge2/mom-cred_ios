//
//  PMVoucherListView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMVoucherListView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView : UITableView!
    
    var currentPageIndex = 0
    
    //MARK:- Var(s)
    fileprivate var presenter = PMVoucherListPresenter()
    fileprivate var isDisplayLoader = true
    
    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var voucherDetailsView:PMVoucherDetailsView?
    
    var voucherList:[PurchaseServiceVoucherDataResponse]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add refresh loader for pulling and refresh
        refreshView.delegate = self
        tableView.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        
        presenter.connectView(view: self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        voucherDetailsView = nil
        if(voucherList == nil || (voucherList != nil && (voucherList?.count)! == 0)){
            Spinner.show()
            serverRequest()
        }
    }
    
    @IBAction private func btnSearchAction(sender: UIButton) {
        let searchVC: PMVoucherSearchListViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMVoucherSearchListViewController.nameOfClass)
        navigationController?.pushViewController(searchVC, animated: true)
    }
}

extension PMVoucherListView{
    
    fileprivate func getVoucherListRequest() -> VoucherListRequest{
        let voucherListRequest = VoucherListRequest()
        voucherListRequest.page_id = "\(currentPageIndex)"
        
        return voucherListRequest
    }
    
    fileprivate func serverRequest(){
        let voucherListRequest = getVoucherListRequest()
        presenter.serverVoucherListing(voucherListRequest: voucherListRequest,callback: {
            (status, response, message) in
        })
    }
}


extension PMVoucherListView: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .pulling(offset, threshould):
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                isDisplayLoader = false
                currentPageIndex += 1
                
                let voucherListRequest = getVoucherListRequest()
                presenter.serverVoucherListing(voucherListRequest: voucherListRequest, callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 currentPageIndex = 0
            }
            isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            let voucherListRequest = getVoucherListRequest()
            presenter.serverVoucherListing(voucherListRequest: voucherListRequest, callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}


//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PMVoucherListView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return voucherList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PMMyVoucherTableViewCell.nameOfClass, for: indexPath) as! PMMyVoucherTableViewCell
        cell.voucherItem = voucherList?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(voucherDetailsView == nil){
            voucherDetailsView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMVoucherDetailsView.nameOfClass) as PMVoucherDetailsView
            
            let obj = voucherList?[indexPath.row]
            voucherDetailsView?.voucher_id = obj?.voucher_id ?? ""
            navigationController?.pushViewController(voucherDetailsView!, animated: true)
        }
    }
}

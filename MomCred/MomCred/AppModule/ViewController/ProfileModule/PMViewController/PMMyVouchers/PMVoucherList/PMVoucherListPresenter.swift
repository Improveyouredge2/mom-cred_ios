//
//  PMVoucherListPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  PMVoucherListPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PMVoucherListPresenter {
    
    var view:PMVoucherListView! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PMVoucherListView) {
        self.view = view
    }
}

extension PMVoucherListPresenter{
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverVoucherListing(voucherListRequest:VoucherListRequest?, callback:@escaping (_ status:Bool, _ response: VoucherListResponse?, _ message: String?) -> Void){
        
        PMVoucherListService.serverVoucherListing(voucherListRequest: voucherListRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    if(response != nil && response?.data != nil && (response?.data?.count)! > 0){
                        if(self.view.currentPageIndex == 0){
                            self.view.voucherList = []
                            self.view.voucherList = response?.data
                        } else {
                            self.view.voucherList?.append(contentsOf: response?.data ?? [])
                        }
                    }
                    
                    self.view.tableView.reloadData()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, nil, message)
                }
            }
        })
    }
}

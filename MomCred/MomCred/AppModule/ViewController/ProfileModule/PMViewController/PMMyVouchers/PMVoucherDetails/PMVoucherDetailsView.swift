//
//  PMVoucherDetailsView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMVoucherDetailsView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView : UITableView!
    
    @IBOutlet weak var constraintTableViewHeightConstant:NSLayoutConstraint!
    
    @IBOutlet weak var lblVerificationNum : UILabel!
    @IBOutlet weak var lblRedeemNum : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var imgVoucherQR : UIImageView!

    //MARK:- Var(s)
    var presenter = PMVoucherDetailsPresenter()
    var voucherDetailResponse:VoucherListDetailDataResponse?
    var voucher_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)
        
        let voucherListDetailRequest = VoucherListDetailRequest()
        voucherListDetailRequest.voucher_id = self.voucher_id
        
        Spinner.show()
        self.presenter.serverVoucherDetail(voucherListDetailRequest: voucherListDetailRequest)

    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.tableView.contentSize.height > 0){
            tableView.scrollToBottom()
            self.constraintTableViewHeightConstant?.constant =
                self.tableView.contentSize.height
            
            self.updateTableViewHeight(tableView:self.tableView)
        }
        
    }
    
    fileprivate func updateTableViewHeight(tableView:UITableView) {
    
        // Location
        if(tableView == self.tableView){
            UIView.animate(withDuration: 0, animations: {
                self.tableView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewHeightConstant.constant = heightOfTableView
            }
        }
    }
}

extension PMVoucherDetailsView{
    func updateScrInfo() {
        if let voucherDetail = voucherDetailResponse {
            lblVerificationNum.text = voucherDetail.voucher_id ?? ""
            lblRedeemNum.text = voucherDetail.voucher_redeem_no ?? ""
            lblPrice.text = "$ \(voucherDetail.voucher_amount ?? "")"
            if let qrImage = voucherDetail.voucher_qr, let url = URL(string: qrImage) {
                imgVoucherQR.setImage(url: url)
            } else if let qrImage = voucherDetail.qr_code, let url = URL(string: qrImage) {
                imgVoucherQR.setImage(url: url)
            } else {
                imgVoucherQR.image = nil
            }
            
            tableView.reloadData()
        }
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PMVoucherDetailsView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.arrNoteList.count
        if(self.voucherDetailResponse != nil){
            return self.voucherDetailResponse?.serviceList != nil ? (self.voucherDetailResponse?.serviceList?.count)! : 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj       =    self.voucherDetailResponse?.serviceList![indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: PurchaseDetailListCell.nameOfClass, for: indexPath) as! PurchaseDetailListCell
        
        //TODO: For testing
        //            let profileImageUrl = PMUserDefault.getProfilePic()
        //            cell.imgProfilePic.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
        
        //TODO: For testing
        if(obj?.service_img != nil){
            cell.imgProfilePic.sd_setImage(with: URL(string: (obj?.service_img ?? "")))
        } else {
            cell.imgProfilePic.image = nil
        }
        
        cell.lblTitle.text = obj?.service_name ?? ""
        cell.lblBusiName.text = "By: \(obj?.serviceusername ?? "")"
        
        cell.lblCalendarDate.text = obj?.calendar_date ?? ""
        
        if obj?.calendar_date == "" {
            cell.lblCalendarListing.isHidden = true
            cell.lblCalendarDate.isHidden = true
        } else {
            cell.lblCalendarListing.isHidden = false
            cell.lblCalendarDate.isHidden = false
        }

        cell.lblDesciption.text = ""
        
        
        if let price = obj?.price, let priceValue = Double(price) {
            cell.lblPrice.text = priceValue.formattedPrice
        } else {
            cell.lblPrice.text = 0.0.formattedPrice
        }
        
        if let credit = obj?.creditamount, let creditValue = Double(credit) {
            cell.lblCreditTitle.isHidden = false
            cell.lblCredit.isHidden = false
            cell.lblCredit.text = creditValue.formattedPrice
        } else {
//            cell.lblCredit.text = 0.0.formattedPrice
            cell.lblCreditTitle.isHidden = true
            cell.lblCredit.isHidden = true
        }
        
        if(obj?.purchase_donation_amount != nil && (obj?.purchase_donation_amount?.length)! > 0){
            cell.lblCharitableAmt.text = obj?.purchase_donation_amount ?? ""
            cell.stackViewCharitable.isHidden = false
            cell.lblCharitable.isHidden = false
        } else {
            cell.stackViewCharitable.isHidden = true
            cell.lblCharitable.isHidden = true
            cell.lblCharitableAmt.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let serviceId = voucherDetailResponse?.serviceList?[indexPath.row].service_id {
            showServiceDetail(for: serviceId)
        }
    }
}

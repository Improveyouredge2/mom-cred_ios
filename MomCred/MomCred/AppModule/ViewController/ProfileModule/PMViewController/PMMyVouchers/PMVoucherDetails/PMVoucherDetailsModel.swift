//
//  PMVoucherDetailsModel.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  PurchaseListRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class VoucherListDetailRequest : Mappable {
    
    var voucher_id: String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        
        voucher_id     <- map["voucher_id"]
    }
}

/**
 *  PurchaseListResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class VoucherListDetailResponse : APIResponse {
    
    var data:VoucherListDetailDataResponse?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> VoucherListDetailResponse?{
        var voucherListDetailResponse:VoucherListDetailResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<VoucherListDetailResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    voucherListDetailResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<VoucherListDetailResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        voucherListDetailResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return voucherListDetailResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        data         <- map["data"]
    }
}


/**
 *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class VoucherListDetailDataResponse: PurchaseServiceVoucherDataResponse{
    
    
    var service:PurchaseServiceDetailDataResponse?
    
    var serviceList:[PurchaseServiceDetailDataResponse]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    override func getModelObjectFromServerResponse(jsonResponse: AnyObject)->VoucherListDetailDataResponse?{
        var voucherListDetailDataResponse:VoucherListDetailDataResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<VoucherListDetailDataResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    voucherListDetailDataResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<VoucherListDetailDataResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        voucherListDetailDataResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return voucherListDetailDataResponse ?? nil
    }
    
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map){
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        super.mapping(map: map)
        
        voucher_user <- map["serviceusername"]
        voucher_date <- map["date"]
        
        service          <- map["service"]
        
        // TODO: manage list, remove when list is coming
        if(service != nil){
            serviceList = [service!]
        }
        
    }
}

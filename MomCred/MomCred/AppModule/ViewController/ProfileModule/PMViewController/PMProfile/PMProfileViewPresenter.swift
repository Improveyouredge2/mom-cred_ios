//
//  PMProfileViewPresenter.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMProfileViewPresenter {
    
    var view:PMProfileView! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PMProfileView) {
        self.view = view
    }
    
    /**
     *  Get user profile information from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func fetchData(){
        
//        if(PMUserDefault.getUserName() != nil){
//            // update information on screen
//            self.view.updateProfile()
//        } else {
            
            self.view.showLoading()
            PMEditProfileService.getData(callback: { (status, response, message) in
                
                if(status == true){
                    OperationQueue.main.addOperation() {
                        Spinner.hide()
                        
                        // update information on screen
                        self.view.updateProfile()
                        self.view.hideLoading()
                    }
                } else {
                    OperationQueue.main.addOperation() {
                        Spinner.hide()
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    }
                }
            })
//        }
    }
}

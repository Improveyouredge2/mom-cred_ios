//
//  MyAccountCell.swift
//  ProfileLib
//
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit

class PMProfileViewCell: UITableViewCell {

    @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var btnTextNImage: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // animate between regular and selected state
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

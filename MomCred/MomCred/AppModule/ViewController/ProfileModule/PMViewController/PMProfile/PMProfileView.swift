//
//  PMAccountView.swift
//  ProfileLib
//
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit

enum ProfileMenuIndex{
    case None
    case Service
}

protocol PMProfileViewInterface {
    func showLoading()
    func hideLoading()
    func showEmptyView()
}

class PMProfileView: LMBaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userImageView: UIButton!
    @IBOutlet weak var lblForName: UILabel!
    @IBOutlet weak var lblForSubTitle: UILabel!
    
//    @IBOutlet weak var progressiveView: GradientProgressBar!
//    @IBOutlet weak var progressiveBgView: UIView!
    
    private(set) var menuList:[MyProfileMenuItem] = []
    fileprivate var menuArray: [HSMenu] = []
    fileprivate var logoutViewController:PMLogoutView?
    
    // Class object of presenter class
    fileprivate let presenter = PMProfileViewPresenter()
    
    fileprivate var editProfileView:PMEditProfileView?
    fileprivate var serviceCatListViewController:MyServiceCatListViewController?
    
    struct MyProfileMenuItem{
        var title:String?
        var imageIcon:UIImage?
        var isArrowHidden = false
        var isSwitchHidden = true
        var profileMenuIndex:ProfileMenuIndex = .None
        var controller:LMBaseViewController?
    }
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset = UIEdgeInsets.zero
        
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        userImageView.layer.borderWidth = 2.0
        userImageView.layer.borderColor =  UIColor.white.cgColor
        userImageView.clipsToBounds = true
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.sign_out.getLocalized())
        menuArray = [menu1]
        
        self.presenter.connectView(view: self)
        
        self.accountMenuList()
        
        //
        
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.logoutViewController = nil
        self.editProfileView = nil
        self.serviceCatListViewController = nil
//        updateProfile()
        self.presenter.fetchData()
        
        let menuItem: MyProfileMenuItem? = menuList.first { $0.controller is LocalBusinessDetailViewController }
        if let controller = menuItem?.controller as? LocalBusinessDetailViewController {
            BIServiceStep1.getBIDetail { (status, response, message) in
                controller.biAddRequest = response?.data?.first
            }
        }
    }
}

// MARK:- Action method implementation
extension PMProfileView{
    
    /**
     *  Back button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodProfileAction(_ sender: AnyObject) {
//        self.navigationController?.popViewController(animated: true)
        
        if(self.editProfileView == nil){
            self.editProfileView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMEditProfileView.nameOfClass) as PMEditProfileView
            
            self.navigationController?.pushViewController(self.editProfileView!, animated: true)
        }
    }
}

//MARK:- Fileprivate method implementation
extension PMProfileView{
    
    /**
     *  Update information on screen control for user profile image and user profile name. Show profile percentage on screen also.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func updateProfile(){
        
        let userName = PMUserDefault.getUserFirstName()
        
        if(userName != nil){
            self.lblForSubTitle.text = userName
            self.lblForName.text = PMUserDefault.getUserName()
            
            let profileImageUrl = PMUserDefault.getProfilePic()
            if(profileImageUrl != nil){
                self.userImageView.sd_setImage(with: URL(string: (profileImageUrl!)), for: .normal, completed: nil)
            }
        } else {
            self.lblForName.text = "Guest User"
            self.userImageView.setImage(nil, for: UIControl.State.normal)
        }
    }
    
    /**
     *  Prepare user account setting menu option for user to select and update information.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func accountMenuList(){
        
        if(menuList.count > 0){
            menuList.removeAll()
        }
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            self.loadServiceProviderMenu()
            break
        case AppUser.LocalBusiness.rawValue:
            self.loadLocalBusinessMenu()
            break
        case AppUser.Enthusiast.rawValue:
            self.loadEnthusiastMenu()
            break
        default:
            break
        }
        
        tableView.reloadData()
    }
    
    fileprivate func loadServiceProviderMenu(){
        var menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.business_information.getLocalized()
        
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIDetailViewController.nameOfClass) as BIDetailViewController
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = "Newsfeed"
        
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMNewsFeedViewController.nameOfClass) as PMNewsFeedViewController
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.service.getLocalized()
        // Service view control add in selection
//        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: MyServiceViewController.nameOfClass) as MyServiceViewController
        menuItem.profileMenuIndex = .Service
        menuList.append(menuItem)
        
        //TODO: [12-09-19]: After discussion with PM and client, it is commented
//        menuItem = MyProfileMenuItem()
//        menuItem.title = LocalizationKeys.credit_services.getLocalized()
////        menuItem.imageIcon = UIImage(named: "key_gray")
//        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMChangePasswordView.nameOfClass) as PMChangePasswordView
//        menuList.append(menuItem)
//
//        menuItem = MyProfileMenuItem()
//        menuItem.title = LocalizationKeys.calender_listing.getLocalized()
////        menuItem.imageIcon = UIImage(named: "key_gray")
//        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMMyCalendarView.nameOfClass) as PMMyCalendarView
//        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.personnel.getLocalized()
//        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: MyPersonalListViewController.nameOfClass) as MyPersonalListViewController
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.facilities.getLocalized()
//        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesListViewController.nameOfClass) as MyFacilitiesListViewController
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.inside_look_content.getLocalized()
//        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InsideLookContentStoryboard, viewControllerName: ILCListViewController.nameOfClass) as ILCListViewController
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.instructional_content.getLocalized()
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: ICListViewController.nameOfClass) as ICListViewController
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.payment_setting.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMPaymentSettingView.nameOfClass) as PMPaymentSettingView
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.testimonials.getLocalized()
//        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMTestimonialView.nameOfClass) as PMTestimonialView
//        let controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: ArticleListViewController.nameOfClass) as ArticleListViewController
//        controller.navTitle = "Testimonials"
//        menuItem.controller = controller
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.change_your_password.getLocalized()
//        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMChangePasswordView.nameOfClass) as PMChangePasswordView
        menuList.append(menuItem)
    }
    
    fileprivate func loadLocalBusinessMenu() {
        var menuItem1 = MyProfileMenuItem()
        menuItem1.title = LocalizationKeys.business_information.getLocalized()
        let controller: LocalBusinessDetailViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.LocalBusinessStoryboard, viewControllerName: LocalBusinessDetailViewController.nameOfClass)
        menuItem1.controller = controller
        BIServiceStep1.getBIDetail { (status, response, message) in
            controller.biAddRequest = response?.data?.first
        }
        menuList.append(menuItem1)
        
        var menuItem2 = MyProfileMenuItem()
        menuItem2.title = "Newsfeed"
        
        menuItem2.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMNewsFeedViewController.nameOfClass) as PMNewsFeedViewController
        menuList.append(menuItem2)

        var menuItem3 = MyProfileMenuItem()
        menuItem3.title = LocalizationKeys.change_your_password.getLocalized()
        menuItem3.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMChangePasswordView.nameOfClass) as PMChangePasswordView
        menuList.append(menuItem3)
        
        var menuItem4 = MyProfileMenuItem()
        menuItem4.title = LocalizationKeys.testimonials.getLocalized()
//        let articleVC: ArticleListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: ArticleListViewController.nameOfClass) as ArticleListViewController
//        articleVC.navTitle = "Testimonials"
//        menuItem4.controller = articleVC
        menuItem4.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMTestimonialView.nameOfClass) as PMTestimonialView
        menuList.append(menuItem4)
    }
    
    fileprivate func loadEnthusiastMenu() {
        var menuItem = MyProfileMenuItem()
        
//        menuItem.title = LocalizationKeys.my_purchase_history.getLocalized()
//        //        menuItem.imageIcon = UIImage(named: "key_gray")
//        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMPurchaseView.nameOfClass) as PMPurchaseView
//        menuList.append(menuItem)
        
//        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.my_refund.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMMyRefundView.nameOfClass) as PMMyRefundView
        menuList.append(menuItem)
        
        /*
        menuItem.title = LocalizationKeys.my_credit.getLocalized()
//        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.CreditSystemVCStoryboard, viewControllerName: CreditSystemViewController.nameOfClass) as CreditSystemViewController
        menuList.append(menuItem)
        */
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.my_voucher.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMVoucherListView.nameOfClass) as PMVoucherListView
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.premium_member.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMPremiumMemberShipPlanView.nameOfClass) as PMPremiumMemberShipPlanView
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.change_your_password.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMChangePasswordView.nameOfClass) as PMChangePasswordView
        menuList.append(menuItem)
    }
}

// MARK:- Action method implementation
extension PMProfileView {
    func backButtonAction(_ sender:Any){
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}

extension PMProfileView: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if(index == 0){
            
            logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
            logoutViewController?.delegate = self
            self.logoutViewController?.modalPresentationStyle = .overFullScreen
            self.present(logoutViewController!, animated: true, completion: nil)
        }
    }
}

extension PMProfileView: UITableViewDelegate,UITableViewDataSource {

    /**
     *  Specify number of section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PMProfileViewCell.nameOfClass, for: indexPath) as! PMProfileViewCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.btnTextNImage.setTitle(menuList[indexPath.row].title, for: .normal)
        cell.btnTextNImage.setImage(menuList[indexPath.row].imageIcon ?? nil, for: UIControl.State.normal)
        cell.arrow.isHidden = menuList[indexPath.row].isArrowHidden
        return cell
    }
    
    /**
     *  Implement action on tapping tableview cell with UITableViewDataDelegate method implementation.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myAccountMenuItem = menuList[indexPath.row]
        
        if(myAccountMenuItem.controller != nil){
            self.navigationController?.pushViewController(myAccountMenuItem.controller!, animated: true)
        } else if indexPath.row == menuList.count - 1{
            logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
            logoutViewController?.delegate = self
            self.logoutViewController?.modalPresentationStyle = .overFullScreen
            self.present(logoutViewController!, animated: true, completion: nil)
        } else if(myAccountMenuItem.profileMenuIndex == .Service){
            
            if(self.serviceCatListViewController == nil){
                self.serviceCatListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: MyServiceCatListViewController.nameOfClass) as MyServiceCatListViewController
                
                self.navigationController?.pushViewController(self.serviceCatListViewController!, animated: true)
            }
        }
    }
}

extension PMProfileView: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
        
        // remove login screen
        PMUserDefault.clearAllData()
        Helper.sharedInstance.userLogout()
        
        //TODO: Add display login screen or shareInstance logout method
    }
}

extension PMProfileView: PMProfileViewInterface {
    
    /**
     *  Start windless animation on controls.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showLoading(){
//        self.view.windless.start()
        Spinner.show()
    }
    
    /**
     *  End windless animation on controls.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func hideLoading(){
//        self.view.windless.end()
        Spinner.hide()
    }
    
    /**
     *  Implement screen controls with empty data.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showEmptyView(){
        
    }
}

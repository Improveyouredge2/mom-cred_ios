//
//  PMMembershipPlanPaymentConfirmationView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMMembershipPlanPaymentConfirmationView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var lblMemberShipPlan : UILabel!
    @IBOutlet weak var lblMemberShipExpDate : UILabel!
    @IBOutlet weak var lblTotalMemberShipPlanAmount : UILabel!
    
    //MARK:- Var(s)
    var membershipPlan:String = ""
    var expDate:String = ""
    var totalAmount:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        lblMemberShipPlan.text = membershipPlan
        lblTotalMemberShipPlanAmount.text = totalAmount

    }
}

extension PMMembershipPlanPaymentConfirmationView{
    
    /**
     *  Buy Now action button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func actionHome(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

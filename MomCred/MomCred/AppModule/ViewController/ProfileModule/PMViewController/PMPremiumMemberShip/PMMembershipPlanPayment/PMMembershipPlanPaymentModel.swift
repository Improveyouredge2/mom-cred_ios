//
//  PMMembershipPlanPaymentModel.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  PaymentInfo is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PaymentInfo : Mappable {
    
    private var cardNo          : String?
    private var expiry          : String?
    private var cvv             : String?
    var amount          : String?
    private var type            : String?
    private var package         : String?
    private var data            : PurchaseData?
    private var redeem          : [RedeemData]?
    private var chargebreakup   : String?
    
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key userid, cat_id, token and language parameters class variable intialization.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(cardNo : String?, expiry : String?, cvv : String?, amount : String?, type:String?, monthCount : String?, serviceList : [ServiceData]?, package:String?, planId:String?, redeemList: [RedeemData]?) {
        
        self.cardNo = cardNo
        self.expiry = expiry
        self.cvv = cvv
        self.amount = amount
        self.type = type
        self.package = package
        self.redeem = redeemList
        
        let data = PurchaseData()
        data.month = monthCount
        data.service = serviceList
        data.plan_id = planId
        self.data = data
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        cardNo  <- map["cardNumber"]
        expiry  <- map["expirationDate"]
        cvv     <- map["cardCode"]
        amount  <- map["amount"]
        type    <- map["type"]
        data    <- map["data"]
        package <- map["package"]
        redeem  <- map["redeem"]
    }
}

class RedeemData: Mappable {
    var amount: Double?
    var purchaseId: String?

    init(amount: Double?, purchaseId: String?) {
        self.amount = amount
        self.purchaseId = purchaseId
    }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        amount  <- map["amount"]
        purchaseId  <- map["purchaseId"]
    }
}

/**
 *  ServiceAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class PurchaseData: Mappable {
    
    var month:String?
    var plan_id:String?
    var service:[ServiceData]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init() {
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        month       <- map["month"]
        plan_id     <- map["plan_id"]
        service     <- map["service"]
    }
}

/**
 *  ServiceAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class ServiceData: Mappable {
    
    var serviceId:String?
    var amount:String?
    var service_user:String?
    var donationPer:String?
    var donationAmt:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init() {
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        serviceId       <- map["serviceId"]
        amount          <- map["amount"]
        service_user    <- map["service_user"]
        
        donationPer     <- map["donationPer"]
        donationAmt     <- map["donationAmt"]
    }
}

/**
 *  PaymentRequestInfo is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PaymentRequestInfo : Mappable {
    
    private var data      : String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key userid, cat_id, token and language parameters class variable intialization.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(data : String?) {
        
        self.data = data
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        data  <- map["data"]
    }
}

/**
 *  ServiceAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class PaymentResponseInfo: APIResponse {
    
    var data:PaymentResponseDetail?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init() {
        super.init()
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    override func getModelObjectFromServerResponse(response jsonResponse: AnyObject)->PaymentResponseInfo?{
        
        var paymentResponseInfo:PaymentResponseInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PaymentResponseInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    paymentResponseInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PaymentResponseInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        paymentResponseInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return paymentResponseInfo ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map) {
        
        super.mapping(map: map)
        
        data        <- map["data"]
        
    }
    

    /**
     *  ServiceAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class PaymentResponseDetail: Mappable {
        
        var transId:String?
        var response:String?
        var pay_id:NSNumber?
        var purchase_id: NSNumber?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key user id, first name, last name, email, address and token.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init() {
        }
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map) {
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(response jsonResponse: AnyObject)->PaymentResponseDetail?{
            
            var paymentResponseDetail:PaymentResponseDetail?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<PaymentResponseDetail>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        paymentResponseDetail = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<PaymentResponseDetail>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            paymentResponseDetail = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return paymentResponseDetail ?? nil
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map) {
            
            transId        <- map["transId"]
            response       <- map["response"]
            pay_id         <- map["pay_id"]
            purchase_id     <- map["purchase_id"]
        }
    }
}


//
//  PMMembershipPlanPaymentView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMMembershipPlanPaymentView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var lblMemberShipPlan : UILabel!
    @IBOutlet weak var lblSelectedMonthMemberShipPlan : UILabel!
    @IBOutlet weak var lblTotalMemberShipPlanAmount : UILabel!
    @IBOutlet weak var lblProcessFees : UILabel!
    
    @IBOutlet weak var txtFeildCardNum:FormTextField!
    @IBOutlet weak var txtFeildCardExp:FormTextField!
    @IBOutlet weak var txtFeildCardCVV:FormTextField!
    
    fileprivate let presenter = PMMembershipPlanPaymentPresenter()
    fileprivate var membershipPlanPaymentConfirmationView:PMMembershipPlanPaymentConfirmationView?
    
    //MARK:- Var(s)
    var membershipPlan:String = ""
    var selectedMonth:String = ""
    var totalAmount:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        let expiryDatePicker = MonthYearPickerView()
        self.txtFeildCardExp.inputView = expiryDatePicker
        expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
            let string = String(format: "%02d/%d", month, year)
            NSLog(string) // should show something like 05/2015
            
            self.txtFeildCardExp.text = string
        }
        
        // Card number
        txtFeildCardNum.formatter = CardNumberFormatter()
        txtFeildCardNum.placeholder = "Card Number"
        txtFeildCardNum.inputType = .integer
        var validation = Validation()
        validation.maximumLength = "1234 5678 1234 5678".count
        validation.minimumLength = "1234 5678 1234 5678".count
        let characterSet = NSMutableCharacterSet.decimalDigit()
        characterSet.addCharacters(in: " ")
        validation.characterSet = characterSet as CharacterSet
        let inputValidator = InputValidator(validation: validation)
        txtFeildCardNum.inputValidator = inputValidator
        
        // Card exp.
        txtFeildCardExp.inputType = .integer
        txtFeildCardExp.formatter = CardExpirationDateFormatter()
        txtFeildCardExp.placeholder = "Expiration Date (MM/YY)"
        
        var validationExp = Validation()
        validationExp.minimumLength = 1
        let characterSetExp = NSMutableCharacterSet.decimalDigit()
        validationExp.characterSet = characterSetExp as CharacterSet
        let inputValidatorExp = CardExpirationDateInputValidator(validation: validationExp)
        txtFeildCardExp.inputValidator = inputValidatorExp
        
        //CVV
        txtFeildCardCVV.inputType = .integer
        txtFeildCardCVV.placeholder = "CVC"
        
        var validationCVV = Validation()
        validationCVV.maximumLength = "CVC".count
        validationCVV.minimumLength = "CVC".count
        validationCVV.characterSet = NSCharacterSet.decimalDigits
        let inputValidatorCVV = InputValidator(validation: validationCVV)
        txtFeildCardCVV.inputValidator = inputValidatorCVV
        
        lblMemberShipPlan.text = "\(membershipPlan ?? "")/1 month"
        
        if(Int(selectedMonth)! > 1){
            lblSelectedMonthMemberShipPlan.text = "\(selectedMonth ?? "") months"
        } else {
            lblSelectedMonthMemberShipPlan.text = "\(selectedMonth ?? "") month"
        }
        
        let processCharges = PMUserDefault.getStripeCharges() ?? ""
        
//        self.lblProcessFees.text = processCharges
       
        if let monthCount = NumberFormatter().number(from:selectedMonth) {
            let month = CGFloat(truncating: monthCount)
            if let planAmount = NumberFormatter().number(from: membershipPlan) {
                
                if let processFeesCharge = NumberFormatter().number(from: processCharges){
                    let membershipPlan = CGFloat(truncating: planAmount)
                    
                    let paymentGateCharge = CGFloat(truncating: processFeesCharge)
                    let tempTotalAmount = (month * membershipPlan) + paymentGateCharge
                    
                    var totalProcessCharges = (paymentGateCharge * membershipPlan)/100
                    
                    self.lblProcessFees.text = String(format: "$ %.2f", totalProcessCharges)
                    self.lblTotalMemberShipPlanAmount.text = String(format: "$ %.2f", tempTotalAmount)
                    
                    self.totalAmount = "\(tempTotalAmount)"
                }
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.membershipPlanPaymentConfirmationView = nil
    }
}

extension PMMembershipPlanPaymentView{
    fileprivate func openNextScr(){
        if(self.membershipPlanPaymentConfirmationView == nil){
            self.membershipPlanPaymentConfirmationView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMMembershipPlanPaymentConfirmationView.nameOfClass) as PMMembershipPlanPaymentConfirmationView
            
            self.membershipPlanPaymentConfirmationView?.membershipPlan = lblSelectedMonthMemberShipPlan.text ?? ""
            self.membershipPlanPaymentConfirmationView?.totalAmount = lblTotalMemberShipPlanAmount.text ?? ""
            
            self.navigationController?.pushViewController(self.membershipPlanPaymentConfirmationView!, animated: true)
        }
    }
}

extension PMMembershipPlanPaymentView{
    
    @IBAction func actionExpiryMethod(){
//        let expiryDatePicker = MonthYearPickerView()
//        self.txtFeildCardExp.inputView = expiryDatePicker
//        expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
//            let string = String(format: "%02d/%d", month, year)
//            NSLog(string) // should show something like 05/2015
//
//            self.txtFeildCardExp.text = string
//        }
    }
    
    /**
     *  Buy Now action button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func actionConfirmPay(_ sender: UIButton) {
        
//        case card_no_missing
//        case card_expiry_missing
//        case card_cvv_missing
        
        if((txtFeildCardNum.text?.length)! == 0) {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.card_no_missing.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((txtFeildCardExp.text?.length)! == 0) {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.card_expiry_missing.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((txtFeildCardCVV.text?.length)! == 0){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.card_cvv_missing.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((txtFeildCardNum.text?.length)! > 0 && (txtFeildCardExp.text?.length)! > 0 && (txtFeildCardCVV.text?.length)! > 0){

            let expirationDates = (txtFeildCardExp.text ?? "").split(separator: "/")
            var cardExpirationMonth = ""
            var cardExpirationYear = ""
            if(expirationDates != nil && expirationDates.count > 1){
                cardExpirationMonth = String(expirationDates[0])
                cardExpirationYear = String(expirationDates[1])
            }

            let paymentInfo = PaymentInfo(cardNo: txtFeildCardNum.text?.replacingOccurrences(of: " ", with: "") ?? "", expiry: "\(cardExpirationYear)-\(cardExpirationMonth)", cvv: txtFeildCardCVV.text ?? "", amount: totalAmount, type: "1", monthCount: selectedMonth, serviceList: nil, package: nil, planId: PMUserDefault.getPlanId() ?? "", redeemList: nil)
            let encodeString = paymentInfo.toJSONString()?.base64Encoded()

            print(encodeString)

            //API integration
            self.presenter.submitPaymentData(paymentInfo: paymentInfo, callback: {
                (status, response) in

                if(status){
                    self.openNextScr()
                }
            })
        }
        
//        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_in_progress.getLocalized(), buttonTitle: nil, controller: nil)
        
    }
}

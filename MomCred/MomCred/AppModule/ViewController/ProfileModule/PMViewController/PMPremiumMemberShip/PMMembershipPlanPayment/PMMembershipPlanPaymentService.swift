//
//  PMMembershipPlanPaymentService.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  AddReviewService is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PMMembershipPlanPaymentService{
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func submitPaymentData(paymentInfo:PaymentInfo?, callback:@escaping (_ status:Bool, _ response: PaymentResponseInfo?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        let encodeString = paymentInfo?.toJSONString()?.base64Encoded()
        
        let requestData = PaymentRequestInfo(data: encodeString)
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData.toJSONString()
        
        var urlString = APIKeys.API_PAYMENT
        if Double(paymentInfo!.amount!)! <= 0.0 {
            urlString = APIKeys.API_PAYMENT_CASH
            paymentInfo!.amount! = "0.0"
        }
        
        reqPost = APIManager().sendPostRequest(urlString: urlString, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:PaymentResponseInfo? = PaymentResponseInfo().getModelObjectFromServerResponse(response: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}


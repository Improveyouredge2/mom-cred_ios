//
//  PMPremiumMemberShipPlanView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class PMPremiumMemberShipPlanView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var dropDown : DropDown!
    
    @IBOutlet weak var btnTermsCondition: UIButton!
    @IBOutlet weak var lblMemberShipPlan : UILabel!
    
    @IBOutlet weak var viewSubscribeBg : UIView!
    @IBOutlet weak var viewSubscribe : UIView!
    @IBOutlet weak var lblExpiryDate : UILabel!
    
    fileprivate var membershipPlan:String = "49.99"
    fileprivate var selectedMonth:String = ""
    fileprivate var membershipPlanPaymentView:PMMembershipPlanPaymentView?
    
    //MARK:- Var(s)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewSubscribe.isHidden = true

        // The list of array to display. Can be changed dynamically
        dropDown.optionArray = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
        //Its Id Values and its optional
        dropDown.optionIds = [1,2,3,4,5,6,7,8,9,10,11,12]
        
        dropDown.selectedIndex = 0
        selectedMonth = dropDown.optionArray[0]
        dropDown.text = selectedMonth
        
        // The the Closure returns Selected Index and String
        dropDown.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.selectedMonth = selectedText
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.membershipPlanPaymentView = nil
        
        let subscriptionInfoData = PMUserDefault.getSubsriptionInfo()
        
        if(subscriptionInfoData != nil){
            let subscriptionInfo:SubscriptionDetail? = SubscriptionDetail().getModelObjectFromServerResponse(jsonResponse: (subscriptionInfoData ?? "") as AnyObject)
            
            if(subscriptionInfo != nil && subscriptionInfo?.plan_status != nil && subscriptionInfo?.plan_status == "1" && subscriptionInfo?.sub_end != nil){
                self.viewSubscribe.isHidden = false
                self.viewSubscribeBg.isHidden = false
                lblExpiryDate.text = subscriptionInfo?.sub_end ?? ""
            } else {
                self.viewSubscribeBg.isHidden = true
            }
        } else {
            self.viewSubscribeBg.isHidden = true
        }
        
        let membershipPlanInfo = PMUserDefault.getSubsriptionPrice()
        
        if(membershipPlanInfo != nil && (membershipPlanInfo?.length)! > 0){
            self.membershipPlan = membershipPlanInfo ?? ""
        }
        
        lblMemberShipPlan.text = "$\(membershipPlan ?? "")"
    }
}

extension PMPremiumMemberShipPlanView{
    
    /**
     *  Buy Now action button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func actionBuyNow(_ sender: UIButton) {
        if(self.btnTermsCondition.isSelected){
            if(self.membershipPlanPaymentView == nil){
                self.membershipPlanPaymentView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMMembershipPlanPaymentView.nameOfClass) as PMMembershipPlanPaymentView
                
                self.membershipPlanPaymentView?.membershipPlan = membershipPlan
                self.membershipPlanPaymentView?.selectedMonth = selectedMonth
                
                self.navigationController?.pushViewController(self.membershipPlanPaymentView!, animated: true)
            }
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.terms_of_service_error.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
    /**
     *  Terms and Condition view action button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func actionTermsCondition(_ sender:UIButton){
        
    }
    
    /**
     *  Terms and Condition accept action button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func actionAcceptTermsCondition(_ sender: UIButton) {
        self.btnTermsCondition.isSelected = !self.btnTermsCondition.isSelected
    }
}

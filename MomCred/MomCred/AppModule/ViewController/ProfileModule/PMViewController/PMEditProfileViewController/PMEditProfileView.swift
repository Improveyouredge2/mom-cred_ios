//
//  PMEditProfileView.swift
//  ProfileLib
//
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftEventBus
import SDWebImage

protocol PMEditProfileViewInterface {
    func showLoading()
    func hideLoading()
    func showEmptyView()
}

class PMEditProfileView: LMBaseViewController {

    
    //MARK:- IBOutlet
    //MARK:-
    @IBOutlet weak var imgBackGround : UIImageView!
    @IBOutlet weak var imgProfilePic : UIImageView!
    @IBOutlet weak var btnCamera : UIButton!
    
    @IBOutlet weak var inputFullName: RYFloatingInput!
    @IBOutlet weak var inputUserName: RYFloatingInput!
    @IBOutlet weak var inputEmail: RYFloatingInput!
    @IBOutlet weak var inputMobile: RYFloatingInput!
    @IBOutlet weak var inputDob: RYFloatingInput!
    @IBOutlet weak var inputAddress: RYFloatingInput!
    
    @IBOutlet weak var saveContainer : UIView!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    // Persist image object for sending on server
    var selectedProfileImage        :   UIImage!
    var imgDataProfPic:Data? = nil
    
    // Class object of presenter class
    fileprivate let presenter = PMEditProfilePresenter()

    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        presenter.fetchData()
        
        openDatePicker()
        
        // Do any additional setup after loading the view.
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.imgBackGround.isHidden = false
        
        SwiftEventBus.onMainThread(self, name: HelperConstant.LanguageChanged, handler: self.setupLanguage)
        
        self.setupLanguage(notification: nil)

    }
    
    // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.imgBackGround.isHidden = true
        self.view.endEditing(true)
        SwiftEventBus.unregister(self)
    }
}

//MARK:- Fileprivate method implementation
//MARK:-
extension PMEditProfileView {
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(data : LoginRespone.LoginResponseData?){
        
        inputFullName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(data?.full_name ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Full Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputUserName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(data?.username ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("User Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputUserName.isUserInteractionEnabled = false
        
        inputEmail.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(data?.email ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer(LocalizationKeys.email.getLocalized())
                .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputEmail.input.keyboardType = .emailAddress
        inputEmail.isUserInteractionEnabled = false
        
        inputDob.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
//                .text(data?.dob ?? "")
                .text(DateTimeUtils.sharedInstance.toLocalDate(data?.dob ?? "", with: "yyyy-MM-dd")?.appSpecificStringFromFormat() ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Date of birth ")
                .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputMobile.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(data?.mobile_no ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer(LocalizationKeys.mobile.getLocalized())
                .maxLength(HelperConstant.LIMIT_UPPER_PHONE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        
        inputMobile.isUserInteractionEnabled = false
        
        inputAddress.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(data?.address ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer(LocalizationKeys.address.getLocalized())
                .maxLength(HelperConstant.LIMIT_ADDRESS, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        
        self.imgProfilePic.sd_setImage(with: URL(string: data?.profile_pic ?? ""), placeholderImage: #imageLiteral(resourceName: "profile"))
        
    }
    
    /**
     *  Update language text on controls.
     *
     *  @param key notification object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setupLanguage(notification:Notification?) -> Void{
        self.lbl_NavigationTitle.text = LocalizationKeys.profile.getLocalized()
        self.btnSave.setTitle(LocalizationKeys.save.getLocalized(), for: UIControl.State.normal)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func performValidation() -> Bool{
        
        if (self.inputFullName.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.onboard_emptyFirstName.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputUserName.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.onboard_emptyLastName.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }  else if (self.inputMobile.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.emptyMobile.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if ((inputMobile.text()?.trim().length)! < HelperConstant.LIMIT_LOWER_PHONE || (inputMobile.text()?.trim().length)! > HelperConstant.LIMIT_UPPER_PHONE ) {
            let message = LocalizationKeys.invalidMobile.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if !(inputEmail.text()?.trim().isEmail())! && (inputEmail.text()?.trim().count)! > 0{
            let message = LocalizationKeys.invalidEmail.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        return true
    }

    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func pickedImage(fromImagePicker: UIImage, imageData: Data, mediaType:String, imageName:String, fileUrl: String?) {
        self.selectedProfileImage = fromImagePicker
        self.imgProfilePic.image    =   fromImagePicker
        
        self.imgDataProfPic = imageData
    }
}

// MARK:- Date Picker
extension PMEditProfileView{
    fileprivate func openDatePicker(){
        let picker: UIDatePicker = UIDatePicker.init()
        
        // Set some of UIDatePicker properties
        picker.datePickerMode = .date
        picker.maximumDate = Date()
        picker.timeZone = NSTimeZone.local
        picker.backgroundColor = UIColor.white
        
        // Add an event to call onDidChangeDate function when value is changed.
        picker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        
//        textFieldDate.inputView = picker
        self.inputDob.input.inputView = picker
    }
    
    @objc fileprivate func dateValueChanged(_ sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "dd MMM yyyy"
        
        // Apply date format
        var selectedDate: String = dateFormatter.string(from: sender.date)
//        textFieldDate.text = selectedDate
        self.inputDob.input.text = selectedDate
        
//        let serverFormat = sender.date.apiSpecificStringFromFormat()
    }
}

//MARK:- Actions method implementation
//MARK:-
extension PMEditProfileView {
    
    /**
     *  Edit button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnEditAction(_ sender : UIButton){
        
    }
    
    /**
     *  Save button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnSaveAction(_ sender : UIButton){
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            if performValidation() {
                presenter.submitData()
            }
        }
    }
    
    /**
     *  Image picker button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnImagePickerAction(_ sender : UIButton){
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    debugPrint("access allowed")
                    
                    OperationQueue.main.addOperation() {
                        //                            if(self.arrDocumentImages.count == 0){
                        self.displayPhotoSelectionOption()
                    }
                    
                } else {
                    //access denied
                    debugPrint("Access denied")
                    OperationQueue.main.addOperation() {
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_this_app_needs_access_to_your_camera_and_gallery.getLocalized(), buttonTitle: nil, controller: nil)
                    }
                }
            })

//            self.displayPhotoSelectionOption()
        }
    }
    
}

//MARK: PMProfileViewInterface Method implementation
//MARK:-
extension PMEditProfileView: PMEditProfileViewInterface {
    /**
     *  Start windless animation on controls.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showLoading(){
//        self.view.windless.start()
       
        Spinner.show()
    }
    
    /**
     *  End windless animation on controls.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func hideLoading(){
//        self.view.windless.end()
        Spinner.hide()
    }
    
    /**
     *  Implement screen controls with empty data.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showEmptyView(){
        
    }
}

//MARK:-
//MARK:- @end

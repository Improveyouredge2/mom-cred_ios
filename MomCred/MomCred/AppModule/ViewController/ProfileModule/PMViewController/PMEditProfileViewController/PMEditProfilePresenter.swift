//
//  PMEditProfilePresenter.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMEditProfilePresenter {
    
    var view:PMEditProfileView! // Object of profile view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key profile view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PMEditProfileView) {
        self.view = view
    }
        
    /**
     *  Get user profile information from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func fetchData(){
        self.view.showLoading()
        PMEditProfileService.getData(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {

                    self.view.setScreenData(data: response?.loginResponseData)
                    self.view.hideLoading()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }

    /**
     *  update user profile information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(){
        
        let userId   = PMUserDefault.getUserId()
        let token = PMUserDefault.getAppToken()
        
        let newDob = DateTimeUtils.sharedInstance.toLocalDate(self.view.inputDob.text() ?? "", with: "dd MMM yyyy")?.apiSpecificStringFromFormat() ?? ""
        
        let request = ProfileUpdate(name: self.view.inputFullName.text() ?? "", dob: newDob ?? "", mobile: self.view.inputMobile.text() ?? "", address: self.view.inputAddress.text() ?? "")
        
        var uploadImageList:[UploadImageData] = []
        var uploadImageData = UploadImageData()
        
        if self.view.selectedProfileImage != nil{
            uploadImageData.imageName = PMHelperConstant.kIMG_NAME_PROFPIC
            uploadImageData.imageKeyName = PMHelperConstant.kProfilePic
            uploadImageData.imageData = (self.view.imgDataProfPic! as Data)
            uploadImageList.append(uploadImageData)
        }
        
        PMEditProfileService.updateData(profileRequest: request, imageList: uploadImageList, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    self.view.setScreenData(data: response?.loginResponseData)
                    self.view.hideLoading()
                    
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    self.view.navigationController?.popViewController(animated: true)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    self.view.btnEdit.isHidden = false
                }
            }
        })
    }
}

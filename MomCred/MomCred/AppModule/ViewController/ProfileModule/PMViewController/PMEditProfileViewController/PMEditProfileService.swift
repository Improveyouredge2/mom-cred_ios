//
//  PMEditProfileService.swift
//  ProfileLib
//
//  Created by Apple_iOS on 15/01/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMEditProfileService  {
    
    /**
     *  Method connect with api call to server and provide information to screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getData(callback:@escaping (_ status:Bool, _ response: LoginRespone?, _ message: String?) -> Void) {
     
        let profileRequest = ProfileRequest(userId: "349")
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = profileRequest.toJSONString()
        
        let reqPost = APIManager().sendGetRequest(urlString: APIKeys.API_PROFILE_DETAIL, header: APIHeaders().getDefaultHeaders())
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:LoginRespone? = LoginRespone().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    if(res?.loginResponseData != nil){
                        PMUserDefault.saveUserFirstName(userFirstName: res?.loginResponseData?.full_name ?? "")
                        PMUserDefault.saveUserName(userName: res?.loginResponseData?.username ?? "")
                        PMUserDefault.setUserEmail(email: res?.loginResponseData?.email ?? "")
                        PMUserDefault.saveAppToken(appToken: res?.loginResponseData?.authorization ?? "")
                        PMUserDefault.saveUserPhone(userPhone: res?.loginResponseData?.mobile_no ?? "")
                        PMUserDefault.saveProfilePic(profilePic: res?.loginResponseData?.profile_pic ?? "")
//                        PMUserDefault.saveUserRole(role: res?.loginResponseData?.role_id ?? "")
                        PMUserDefault.saveSubsriptionInfo(subsriptionInfo: res?.loginResponseData?.subscription?.toJSONString() ?? "")
                        PMUserDefault.saveSubsriptionPrice(subsriptionPrice: res?.loginResponseData?.membershipprice ?? "")
                        
                        PMUserDefault.saveStripeCharges(subsriptionPrice: res?.loginResponseData?.stripefees ?? "")
                        
                        PMUserDefault.savePlanId(subsriptionPrice: res?.loginResponseData?.plan_id ?? "")
                        
                        PMUserDefault.saveLoginInfo(loginInfo: res?.toJSON() as! NSDictionary)
                    }
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
            
        }
        
    }
    
    /**
     *  Method upload screen information on server with profile data and user profiel image.
     *
     *  @param key profile information and selected image for user profile. Callback method to update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func updateData(profileRequest: ProfileUpdate?, imageList:[UploadImageData]?, callback:@escaping (_ status:Bool, _ response: LoginRespone?, _ message: String?) -> Void) {
        
        // MultiForm
        let jsonArray = profileRequest?.toJSON()
        let reqMultiForm = APIManager().sendPostMultiFormRequest(urlString: APIKeys.API_PROFILE_UPDATE, header: APIHeaders().getDefaultHeaders(), formDataParameter: jsonArray, uploadImageList: imageList)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqMultiForm, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:LoginRespone? = LoginRespone().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
//                    if(res?.loginResponseData != nil){
//                        PMUserDefault.saveUserFirstName(userFirstName: res?.loginResponseData?.full_name ?? "")
//                        PMUserDefault.saveUserName(userName: res?.loginResponseData?.username ?? "")
//                        PMUserDefault.setUserEmail(email: res?.loginResponseData?.email ?? "")
//                        PMUserDefault.saveAppToken(appToken: res?.loginResponseData?.authorization ?? "")
//                        PMUserDefault.saveUserPhone(userPhone: res?.loginResponseData?.mobile_no ?? "")
//                        PMUserDefault.saveProfilePic(profilePic: res?.loginResponseData?.profile_pic ?? "")
////                        PMUserDefault.saveUserRole(role: res?.loginResponseData?.role_id ?? "")
//                        
//                        PMUserDefault.saveLoginInfo(loginInfo: res?.toJSON() as! NSDictionary)
//                    }
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

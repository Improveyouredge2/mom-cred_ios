//
//  PMCalendarListingView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMCalendarListingView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
//    @IBOutlet weak var tblTestimonialList : UITableView!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    
    fileprivate var menuArray: [HSMenu] = []
    
    //MARK:- Var(s)
//    var presenter       =       MyNotesPresenter()
//    var arrNoteList :   [GetMyNotesResponse.GetMyNotesResponseData.GetMyNotesInfoData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.edit.getLocalized())
        menuArray = [menu1]
        
        let userName = PMUserDefault.getUserFirstName()
        if(userName != nil){
            let profileImageUrl = PMUserDefault.getProfilePic()
            self.lblTitle.text = userName
            
            if(profileImageUrl != nil){
                self.imgProfile.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
            }
        }
    }
    
}

extension PMCalendarListingView{
    /**
     *  Common Back button click event method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous
     */
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}


extension PMCalendarListingView: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if(index == 0){
        }
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PMCalendarListingView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.arrNoteList.count
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let data       =    self.arrNoteList[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: PMTestimonialTableViewCell.nameOfClass, for: indexPath) as! PMTestimonialTableViewCell
        
        //TODO: For testing
        let profileImageUrl = PMUserDefault.getProfilePic()
        cell.imgProfilePic.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
        
        cell.lblDesciption.text = "sfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd fl"
//
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

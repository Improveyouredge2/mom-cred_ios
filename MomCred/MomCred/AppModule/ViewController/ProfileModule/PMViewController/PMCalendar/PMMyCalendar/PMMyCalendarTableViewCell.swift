//
//  PMMyCalendarTableViewCell.swift
//  MomCred
//
//  Created by consagous on 05/06/19.
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit

class PMMyCalendarTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView : ViewLayerSetup!
    @IBOutlet weak var imgProfilePic : ImageLayerSetup!
//    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var lblDesciption : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.cardView.backgroundColor   =   UIColor(hexString: ColorCode.cardColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

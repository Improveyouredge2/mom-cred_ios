//
//  PMMyCalendarView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMMyCalendarView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tbCalendarList : UITableView!
    
    fileprivate var menuArray: [HSMenu] = []
    
    fileprivate var createCalendarView:PMCreateCalendarView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.presenter.connectView(view: self)
        tbCalendarList.estimatedRowHeight = 100
        tbCalendarList.rowHeight = UITableView.automaticDimension
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.add_new.getLocalized())
        menuArray = [menu1]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.createCalendarView = nil
    }
}


extension PMMyCalendarView{
    /**
     *  Common Back button click event method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous
     */
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}


extension PMMyCalendarView: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if(index == 0 && self.createCalendarView == nil){
            
            self.createCalendarView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMCreateCalendarView.nameOfClass)
            self.navigationController?.pushViewController(self.createCalendarView!, animated: true)
        }
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PMMyCalendarView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.arrNoteList.count
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let data       =    self.arrNoteList[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: PMMyCalendarTableViewCell.nameOfClass, for: indexPath) as! PMMyCalendarTableViewCell
        
//        cell.lblUserName.text                       =   data.user_name ?? ""
//        cell.lblNoteDesciption.text               =    data.note_content ?? ""
//        cell.lblDate.text                               =   data.created_on ?? ""
//        cell.imgProfilePic.sd_setImage(with: (data.user_image?.count)! > 2 ? (data.user_image ?? "").toURL() : nil, placeholderImage: #imageLiteral(resourceName: "minion"))
        
        //TODO: For testing
        let profileImageUrl = PMUserDefault.getProfilePic()
        cell.imgProfilePic.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
        
//        cell.lblDesciption.text = "sfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd fl"
        
        cell.lblDesciption.text = "3367 Gusikowski Plaza Suite 792"
//
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let calendarListing:PMCalendarListingView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMCalendarListingView.nameOfClass) as PMCalendarListingView
        self.navigationController?.pushViewController(calendarListing, animated: true)
    }
    
    
}

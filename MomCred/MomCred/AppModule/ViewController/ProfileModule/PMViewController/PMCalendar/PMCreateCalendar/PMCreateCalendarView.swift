//
//  PMCreateCalendarView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMCreateCalendarView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
//    @IBOutlet weak var tblTestimonialList : UITableView!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblReviewCount : UILabel!
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    
    //MARK:- Var(s)
//    var presenter       =       MyNotesPresenter()
//    var arrNoteList :   [GetMyNotesResponse.GetMyNotesResponseData.GetMyNotesInfoData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.presenter.connectView(view: self)
//        tblTestimonialList.estimatedRowHeight = 100
//        tblTestimonialList.rowHeight = UITableView.automaticDimension
        
//
//        let userName = PMUserDefault.getUserFirstName()
//        if(userName != nil){
//            let profileImageUrl = PMUserDefault.getProfilePic()
//            self.lblTitle.text = userName
//            self.lblSubTitle.text = PMUserDefault.getUserName()
//
//            if(profileImageUrl != nil){
////                self.imgProfile?.sd_setImage(with: URL(string: (profileImageUrl!)), for: .normal, completed: nil)
//                self.imgProfile.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
//            }
//        }
    }
    
}

//MARK:- MyNotesDeleage
//MARK:-
//extension PMTestimonialView : MyNotesDeleage {
////    func noteDeletedWith(_ note_Id: String?) {
////        if let firstIndex  = self.arrNoteList.firstIndex(where: {$0.id == note_Id}) {
//////            self.arrNoteList.remove(at: firstIndex)
////            self.tblTestimonialList.reloadData()
////        }
////    }
//
////    func arrDataWith(_ arrData: [GetMyNotesResponse.GetMyNotesResponseData.GetMyNotesInfoData]?) {
//////        self.arrNoteList    =   arrData ?? []
////        self.tblTestimonialList.reloadData()
////    }
//}


//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PMCreateCalendarView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.arrNoteList.count
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let data       =    self.arrNoteList[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: PMTestimonialTableViewCell.nameOfClass, for: indexPath) as! PMTestimonialTableViewCell
        
//        cell.lblUserName.text                       =   data.user_name ?? ""
//        cell.lblNoteDesciption.text               =    data.note_content ?? ""
//        cell.lblDate.text                               =   data.created_on ?? ""
//        cell.imgProfilePic.sd_setImage(with: (data.user_image?.count)! > 2 ? (data.user_image ?? "").toURL() : nil, placeholderImage: #imageLiteral(resourceName: "minion"))
        
        //TODO: For testing
        let profileImageUrl = PMUserDefault.getProfilePic()
        cell.imgProfilePic.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
        
        cell.lblDesciption.text = "sfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd fl"
//
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
}

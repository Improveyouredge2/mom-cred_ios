//
//  ErrorModel.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  PMErrorModel is generic object, move from one screen to another or api error in app for display error with information.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PMErrorModel{
    static var genericErrorModel:PMErrorModel?
    
    var errorCode:String?
    var errorMsg:String?
    var mobileStatus : Int?
    var emailStatus  : Int?
    var mobileMsg : String?
    var emailMsg  : String?
    var flag       :    String?
    
    var data:JSONSerialization? //Optional
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key error code and message.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(errorCode:String, errorMsg:String) {
        self.errorCode = errorCode
        self.errorMsg = errorMsg
    }
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key error code, message and serialization object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(errorCode:String, errorMsg:String, data:JSONSerialization) {
        self.errorCode = errorCode
        self.errorMsg = errorMsg
        self.data = data
    }
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key error code, message, mobile status, mobile message, email status, email message and flag.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(errorCode:String, errorMsg:String , mobileStatus :Int , mobileMsg :String , emailStatus :Int , emailMsg :String , flag : String?) {
        self.errorCode      = errorCode
        self.errorMsg       = errorMsg
        self.mobileMsg      = mobileMsg
        self.emailMsg       = emailMsg
        self.mobileStatus   = mobileStatus
        self.emailStatus    = emailStatus
        self.flag           = flag
    }
    
    /**
     *  Prepare string with combining error code and message.
     *
     *  @param key empty.
     *
     *  @return String containing error code and message.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func toString() -> String{
        return "Error {code='\(String(describing: errorCode))', msg='\(String(describing: errorMsg))'}";
    }
    
    /**
     *  Provide PMErrorModel object with error code and description.
     *
     *  @param key empty.
     *
     *  @return PMErrorModel object with error code and description.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class func defaultUnknownError() -> PMErrorModel{
        if(PMErrorModel.genericErrorModel == nil) {
            PMErrorModel.genericErrorModel = PMErrorModel(errorCode: HelperConstant.DEFAULT_REST_ERROR_CODE, errorMsg: HelperConstant.DEFAULT_REST_ERROR_MESSAGE)
        }
        return PMErrorModel.genericErrorModel!
    }
}


//
//  UserProfile.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import ObjectMapper

/**
 *  ProfileRequest is serialize object user profile in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ProfileRequest: Mappable {
    private var userId:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(userId:String?) {
        self.userId = userId
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        userId <- map["userid"]
    }
}

/**
 *  ProfileResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ProfileResponse : APIResponse {
    
    var profileResponseData:ProfileResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ProfileResponse?{
        var profileResponse:ProfileResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ProfileResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    profileResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ProfileResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        profileResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return profileResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        profileResponseData        <- map["data"]
    }
    
    /**
     *  Sub-class ProfileResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class ProfileResponseData: Mappable{
        
        var user_id : String?
        var role_id : String?
        var token : String?
        var driver_status : String?
        var first_name : String?
        var last_name : String?
        var mobile : String?
        var email : String?
        var profile_img : String?
        var unique_no : String?
        var dob : String?
        var gender : String?
        var gst_number : String?
        var address : String?
        var driving_license : String?
        var rc_number : String?
        var lat : String?
        var lang : String?
        var device_type : String?
        var referral_code : String?
        var activation_code : String?
        var login_count : String?
        var status : String?
        var create_dt : String?
        var update_dt : String?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ProfileResponseData?{
            var profileResponseData:ProfileResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<ProfileResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        profileResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ProfileResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            profileResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return profileResponseData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        func mapping(map: Map){
            user_id             <- map["user_id"]
            role_id             <- map["role_id"]
            token               <- map["token"]
            driver_status       <- map["driver_status"]
            first_name          <- map["first_name"]
            last_name           <- map["last_name"]
            mobile              <- map["mobile"]
            email               <- map["email"]
            profile_img         <- map["profile_img"]
            unique_no           <- map["unique_no"]
            dob                 <- map["dob"]
            gender              <- map["gender"]
            gst_number          <- map["gst_number"]
            address             <- map["address"]
            driving_license     <- map["driving_license"]
            rc_number           <- map["rc_number"]
            lat                 <- map["lat"]
            lang                <- map["lang"]
            device_type         <- map["device_type"]
            referral_code       <- map["referral_code"]
            activation_code     <- map["activation_code"]
            login_count         <- map["login_count"]
            status              <- map["status"]
            create_dt           <- map["create_dt"]
            update_dt           <- map["update_dt"]
            
        }
    }
}

/**
 *  ProfileUpdate is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ProfileUpdate : Mappable {
    private var name : String?
    private var dob : String?
    private var mobile : String?
    private var address : String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(name : String?, dob : String?, mobile : String?, address : String?) {
        self.name       = name
        self.dob        = dob
        self.mobile     = mobile
        self.address    = address
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        name        <- map["name"]
        dob         <- map["dob"]
        mobile      <- map["mobile"]
        address     <- map["address"]
    }
    
}

class UploadProfile: Mappable {
    var user_id:String?
    var token:String?
    var name:String?
    var phone:String?
    
    init(user_id:String?, token:String?, name:String?, phone:String?) {
        self.user_id = user_id
        self.token = token
        self.name = name
        self.phone = phone
    }
    
    required init?(map: Map) {
        
    }
    
    //Map Auth Request Parameters
    func mapping(map: Map) {
        user_id         <- map["user_id"]
        token           <- map["token"]
        name            <- map["name"]
        phone           <- map["mobile_number"]
    }
}

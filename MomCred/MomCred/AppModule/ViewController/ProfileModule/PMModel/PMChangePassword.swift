//
//  PMChangePassword.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  PMChangePassword is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PMChangePassword: Mappable{
    
    private var user_id:String? // User logged in user id
    private var token:String? // app token
    private var new_password:String? // new selected password
    private var old_password:String? // user old password
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, app token, new password and old password.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(user_id:String?, token:String?, new_password:String?, old_password:String?) {
        self.user_id = user_id
        self.token = token
        self.new_password = new_password
        self.old_password = old_password
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        user_id         <- map["user_id"]
        token           <- map["token"]
        new_password    <- map["new_password"]
        old_password    <- map["old_password"]
    }
}

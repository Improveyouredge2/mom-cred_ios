//
//  PMHelperConstant.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit
//
///**
// *  Enum for app language constant
// *
// *  @Developed By: Team Consagous [CNSGSIN054]
// */
//enum AppLanguage : String {
//    case English
//    case Hindi
//}
//
///**
// *  Extension for app language code
// *
// *  @Developed By: Team Consagous [CNSGSIN054]
// */
//extension AppLanguage {
//    var LanguageCode:String{
//        switch self {
//        case .English:
//            return "en"
//        case .Hindi:
//            return "hi"
//        }
//    }
//}

/**
 *  Module Helper constant class for specifying constant used in module
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PMHelperConstant: NSObject {

    //USER DEFAULT KEY'S
    internal static let userDefaultIsEmail              = "email"
    internal static let userDefaultUserNameKey          = "username"
    internal static let userDefaultUserFirstNameKey     = "userfirstname"
    internal static let userDefaultBankDetailsKey     = "userbankdetailsadded"

    internal static let userDefaultUserLastNameKey      = "userlastname"
    internal static let userDefaultUserProfilePicKey    = "userProfilePic"
    internal static let userDefaultUserPhoneKey         = "userphone"
    internal static let userDefaultUserIdKey            = "userid"
    internal static let userDefaultAppTokenKey          = "user_token"
    internal static let userDefaultUserAddressKey       = "user_address"
    internal static let userDefaultUserLoginInfoKey = "userLoginInfo"
    internal static let userDefaultUserRoleKey = "UserRoleKey"
    internal static let userDefaultSubscriptionInfo = "SubscriptionInfo"
    internal static let userDefaultSubscriptionPrice = "SubscriptionPrice"
    
    internal static let userDefaultStripePrice = "StripePrice"
    internal static let userDefaultPlanId = "PlanId"
    
    
    internal static let userDefaultUserAuthorizationInfoKey = "userAuthorization"
    
    // Storyboard Name
    internal static let ProfileStoryboard = "PMProfile"
    
    // Extra API constant
    internal static let kIMG_NAME_PROFPIC = "profile_image.png"
    internal static let kProfilePic:String = "profile_image"
    
    internal static let qr_scan_token  = "QR_SCAN_TOKEN"

    
}

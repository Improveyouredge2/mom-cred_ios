//
//  FileUtils.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  FileUtils is specify folder path, file path, save, delete & fetch functionality to file
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PMFileUtils {
    private let MAX_PREVIEW_IMAGE_QUALITY = 25
    private let MAX_IMAGE_QUALITY = 90
    static let SENT_IMAGE_MAX_WIDTH = 1000
    static let SENT_IMAGE_MAX_HEIGHT = 800
    
    /**
     *  This private method. This method is for get local file saving path in device document directory
     *
     *  @param key empty.
     *
     *  @return document directory path in URL object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class func getLocalMediaFileFolderPath(folderName:String) -> URL?{
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let path = URL(string: documentsDirectory)?.appendingPathComponent("LocalFile/\(folderName)")
        if(path != nil){
            do {
                try FileManager.default.createDirectory(atPath: (path?.absoluteString)!, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        
        return path ?? nil
    }
    
    class func getFolderAllFileUrls(folderName:String, skipsHiddenFiles: Bool = true ) -> [URL]? {
        
        var fileURLs:[URL]?
        let fileManager = FileManager.default
//        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
       let documentsURL = PMFileUtils.getLocalMediaFileFolderPath(folderName:folderName)!
        do {
            fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil, options: skipsHiddenFiles ? .skipsHiddenFiles : [] )
            // process files
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
        
        return fileURLs
    }
    
    class func directoryExistsAtPath(_ folderName: String) -> Bool {
//        var fileURLs:[URL]?
//        let fileManager = FileManager.default
//        let documentsURL = PMFileUtils.getLocalMediaFileFolderPath(folderName:folderName)!
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let path = URL(string: documentsDirectory)?.appendingPathComponent("LocalFile/\(folderName)")
        
        var isDirectory = ObjCBool(true)
        let exists = FileManager.default.fileExists(atPath: path?.absoluteString ?? "", isDirectory: &isDirectory)
        return exists && isDirectory.boolValue
    }
    
    /**
     *  Check expected file in given folder directory. File name is auto generated as per files saved in given path folder
     *
     *  @param key folder directory path, expected file name, counter for add in name as suffix.
     *
     *  @return New file folder path in URL object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class func getExpectedFileInCopyFolder(folderDir:URL, expectedFileName:String, counter:Int) -> URL?{
        var newName:String = expectedFileName;
        if(counter > 0) { //Recursion case, create file name with counter
            let lastDotIndex:Range<String.Index>? = expectedFileName.range(of: ".", options: String.CompareOptions.backwards, range: nil, locale: nil)!
            if(lastDotIndex == nil) {
                newName = "\(expectedFileName)(\(counter))"
            } else {
                newName = "\(expectedFileName.substring(to: (lastDotIndex?.lowerBound)!))(\(counter)).\(expectedFileName.substring(from: (lastDotIndex?.upperBound)!))"
            }
        }

        let pathUrl:URL = folderDir.appendingPathComponent(newName)
        if(FileManager.default.fileExists(atPath: pathUrl.absoluteString)){
            return getExpectedFileInCopyFolder(folderDir: folderDir, expectedFileName: expectedFileName, counter: counter + 1)
        }
        
        return pathUrl
    }
    
    /**
     *  Check and prepare file path as per given folder directory.
     *
     *  @param key folder directory path, expected file name, counter for add in name as suffix.
     *
     *  @return New file folder path in URL object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class func getFileInFolder(folderDir:URL, expectedFileName:String, counter:Int) -> URL?{
        var newName:String = expectedFileName;
        
        let pathUrl:URL = folderDir.appendingPathComponent(newName)
        if(FileManager.default.fileExists(atPath: pathUrl.absoluteString)){
            return pathUrl
        }
        
        return pathUrl
    }
    
    /**
     *  Save image file at give folder path .
     *
     *  @param key image date and file name.
     *
     *  @return New file folder path in String object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class func saveImageAtLocalPath(imageData:NSData, fileName:String, folderName:String) -> String?{ // save file and return local path
        let folderPath:URL? = getExpectedFileInCopyFolder(folderDir: PMFileUtils.getLocalMediaFileFolderPath(folderName:folderName)!, expectedFileName: fileName, counter: 0)
        var filePath:String?
        if(folderPath != nil){
            do{
                try imageData.write(toFile: (folderPath?.absoluteString)!, options: NSData.WritingOptions.atomic)
                
                filePath = URL(fileURLWithPath: (folderPath?.absoluteString)!).absoluteString
            } catch let error as NSError{
                print("Error---->\(error)")
            }
        }
        return filePath
    }
    
    /**
     *  Save document file at give folder path .
     *
     *  @param key image date and file name.
     *
     *  @return New file folder path in String object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class func saveFileAtLocalPath(fileData:NSData, fileName:String, folderName:String) -> String?{ // save file and return local path

        let folderPath:URL? = getFileInFolder(folderDir: PMFileUtils.getLocalMediaFileFolderPath(folderName:folderName)!, expectedFileName: fileName, counter: 0)
        var filePath:String?
        if(folderPath != nil && !FileManager.default.fileExists(atPath: (folderPath?.absoluteString)!)){
            do{
                try fileData.write(toFile: (folderPath?.absoluteString)!, options: NSData.WritingOptions.atomic)
                
                filePath = URL(fileURLWithPath: (folderPath?.absoluteString)!).absoluteString
            } catch let error as NSError{
                print("Error---->\(error)")
            }
        }
        return filePath
    }
    
    /**
     *  Remove image file at give folder path .
     *
     *  @param key image date.
     *
     *  @return deletion status of file.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class func removeImageAtLocalPath(fileName:String, folderName:String) -> Bool{ // save file and return local path
        var isFileSaveSuccessfully = false
        let fileManager = FileManager.default
        
        var documentsURL = PMFileUtils.getLocalMediaFileFolderPath(folderName:folderName)!
        
        //documentsURL = documentsURL.appendingPathExtension(fileName)
        documentsURL = documentsURL.appendingPathComponent(fileName)
        
        do {
            try fileManager.removeItem(atPath: documentsURL.absoluteString)
            isFileSaveSuccessfully = true
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        // TODO: for check file delete
//        self.clearTempFolder()
        
        return isFileSaveSuccessfully
    }
    
    class func clearTempFolder() {
        let fileManager = FileManager.default
        let tempFolderPath = NSTemporaryDirectory()
        do {
            let filePaths = try fileManager.contentsOfDirectory(atPath: tempFolderPath)
            for filePath in filePaths {
                try fileManager.removeItem(atPath: tempFolderPath + filePath)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
    /**
     *  Remove folder at give folder path .
     *
     *  @param key image date.
     *
     *  @return deletion status of file.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class func removeFolderAtLocalPath(folderName:String) -> Bool{ // save file and return local path
        var isFileSaveSuccessfully = false
        let fileManager = FileManager.default
        
//        var documentsURL = PMFileUtils.getLocalMediaFileFolderPath(folderName:folderName)!
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let path = URL(string: documentsDirectory)?.appendingPathComponent("LocalFile/\(folderName)")
        
        do {
            try fileManager.removeItem(atPath: path?.absoluteString ?? "")
            isFileSaveSuccessfully = true
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        return isFileSaveSuccessfully
    }
    
    /**
     *  Remove document file at give folder path .
     *
     *  @param key image date.
     *
     *  @return deletion status of file.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class func removeRemoveAtLocalPath(filePath:String) -> Bool{ // save file and return local path
        var isFileSaveSuccessfully = false
        let fileManager = FileManager.default
        
        do {
            try fileManager.removeItem(atPath: filePath)
            isFileSaveSuccessfully = true
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
        return isFileSaveSuccessfully
    }
    
    /**
     *  Fetch document file data from given file name .
     *
     *  @param key file name.
     *
     *  @return document file data.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class func getFileDataAtLocalPath(fileName:String, folderName:String) -> NSData? {
        let folderPath:URL? = getFileInFolder(folderDir: PMFileUtils.getLocalMediaFileFolderPath(folderName:folderName)!, expectedFileName: fileName, counter: 0)
        
        var filePath:URL?
        var fileData:NSData?
        if(folderPath != nil){
            do{
                //try fileData.write(toFile: (folderPath?.absoluteString)!, options: NSData.WritingOptions.atomic)
                
                
                filePath = URL(fileURLWithPath: (folderPath?.absoluteString)!)
                if(filePath != nil){
                    fileData = NSData(contentsOf: filePath!)
                }
            } catch let error as NSError{
                print("Error---->\(error)")
            }
        }
        return fileData
    }
    
    
    /**
     * Upload file to given url having content type and additional headers.
     * @param url Url to upload file
     * @param path File Path
     * @param contentType Content Type
     * @param headers Additional headers, if there.
     * @return True for successful upload, false otherwise.
     * @throws IOException
     */
    class func uploadFile(url:String, path:String, contentType:String, headers:[String:String]?, _ onSuccess:@escaping ((AnyObject?) -> Void), onError:@escaping ((AnyObject?) -> Void)) {
        if(headers != nil){
            let image_data:NSData? = NSData(contentsOf: URL(string: path)!)
            if(image_data == nil){
                return
            }
            
            //TODO: Remove image/file after upload sucessfully
            
            // sending file length on server in header
//            let contentLength = NSNumber(integerLiteral: (image_data?.length)!).stringValue
//            let url = URL(string: url)
//            let request = NSMutableURLRequest(url: url!)
//
//            request.httpMethod = "PUT"
//            request.setValue(contentType, forHTTPHeaderField: "Content-Type")
//            request.setValue(contentLength, forHTTPHeaderField: "Content-Length")
//            if(headers != nil && (headers?.count)! > 0){
//                for (key,value) in headers!{
//                    request.setValue(value, forHTTPHeaderField: key)
//                }
//            }
//
//            request.httpBody = image_data as Data?
//
//            var response:URLResponse?
//            do{
//                let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
//
//                var convertData:Any?
//
//                if let httpResponse:HTTPURLResponse? = response as? HTTPURLResponse {
//                    if(httpResponse != nil && httpResponse?.statusCode == NetworkConfigConstants.NetworkAPIStatusCode.SUCCESS){
//                        onSuccess(nil)// image upload successfully
//                    }else{
//                        do{
//                            convertData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as Any?
//                            onError(convertData as AnyObject?? ?? nil)
//                        }catch let error as NSError{
//                            AppConstant.klogString(error.localizedDescription)
//                            onError(error)
//                        }
//                    }
//                }
//            }catch let error as NSError{
//                AppConstant.klogString(error.localizedDescription)
//                onError(error)
//            }
        }
        
        return
    }
}

//
//  DateTimeUtils.swift
//  Ikan
//


import Foundation

class DateTimeUtils {
    
    static let sharedInstance = DateTimeUtils()

    private var DF_TIME_hh_mm_aa:DateFormatter?
    private var DF_TIME_yyyy_MM_dd:DateFormatter?
    private var DF_TIME_dd_MMM_yy:DateFormatter?
    private var DF_TIME_dd_MMMM_yyyy:DateFormatter?
    private var DF_TIME_dd_MMM_yyyy:DateFormatter?
    private var DF_TIME_dd_MMM:DateFormatter?
    private var DF_dd_MM_yyyy:DateFormatter?
    private var DF_DAY_MMM_dd_yyyy:DateFormatter?
    private var DF_MMMM:DateFormatter?
    private var DF_FULL_DAY_DD_MM_YYYY:DateFormatter?
    private var DF_dd_MM_yyyy_TIME:DateFormatter?
    private var DF_dd_MM_yyyy_TIME_12_HOURS:DateFormatter?
    private var DF_dd_MM_TIME_12_HOURS:DateFormatter?
    
    private var DF_MMM_dd_yyyy:DateFormatter?
    
    internal static let APP_API_REQUEST_DATE_FORMAT =   "yyyy-MM-dd"
    internal static let APP_API_REQUEST_DATE_WITH_TIME_FORMAT =   "yyyy-MM-dd HH:mm:ss"
    internal static let APP_DATE_FORMAT =   "dd MMM yyyy"
    internal static let APP_DATE_FANCY_FORMAT = "dd-MMM-yyyy, hh:mm a"
    
    init(){
        loadFormatter()
    }
    
    func loadFormatter() {
        
        DF_TIME_hh_mm_aa = DateFormatter()
        if(DF_TIME_hh_mm_aa != nil){
            DF_TIME_hh_mm_aa?.dateFormat = "hh:mm a"
            DF_TIME_hh_mm_aa?.timeZone = TimeZone.current
            DF_TIME_hh_mm_aa?.locale = Locale.current
        }
        
        
        DF_TIME_dd_MMMM_yyyy = DateFormatter()
        if(DF_TIME_dd_MMMM_yyyy != nil){
            DF_TIME_dd_MMMM_yyyy?.dateFormat = "dd-MMMM-yyyy"
            DF_TIME_dd_MMMM_yyyy?.timeZone = TimeZone.current
            DF_TIME_dd_MMMM_yyyy?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM_yyyy = DateFormatter()
        if(DF_TIME_dd_MMM_yyyy != nil){
            DF_TIME_dd_MMM_yyyy?.dateFormat = "dd-MMM-yyyy"
            DF_TIME_dd_MMM_yyyy?.timeZone = TimeZone.current
            DF_TIME_dd_MMM_yyyy?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM = DateFormatter()
        if(DF_TIME_dd_MMM != nil){
            DF_TIME_dd_MMM?.dateFormat = "dd-MMMM"
            DF_TIME_dd_MMM?.timeZone = TimeZone.current
            DF_TIME_dd_MMM?.locale = Locale.current
        }
        
        DF_TIME_yyyy_MM_dd = DateFormatter()
        if(DF_TIME_yyyy_MM_dd != nil){
            DF_TIME_yyyy_MM_dd?.dateFormat = "yyyy/MM/dd"
            DF_TIME_yyyy_MM_dd?.timeZone = TimeZone.current
            DF_TIME_yyyy_MM_dd?.locale = Locale.current
        }
        
        DF_TIME_dd_MMM_yy = DateFormatter()
        if(DF_TIME_dd_MMM_yy != nil){
            DF_TIME_dd_MMM_yy?.dateFormat = "dd/MMM/yy"
            DF_TIME_dd_MMM_yy?.timeZone = TimeZone.current
            DF_TIME_dd_MMM_yy?.locale = Locale.current
        }

        DF_DAY_MMM_dd_yyyy = DateFormatter()
        if(DF_DAY_MMM_dd_yyyy != nil){
            DF_DAY_MMM_dd_yyyy?.dateFormat = "EEE, dd MMM, yyyy"
            DF_DAY_MMM_dd_yyyy?.timeZone = TimeZone.current
            DF_DAY_MMM_dd_yyyy?.locale = Locale.current
        }
        
        DF_MMMM = DateFormatter()
        if(DF_MMMM != nil){
            DF_MMMM?.dateFormat = "MMMM yyyy"
            DF_MMMM?.timeZone = TimeZone.current
            DF_MMMM?.locale = Locale.current
        }
        
        DF_FULL_DAY_DD_MM_YYYY = DateFormatter()
        if(DF_FULL_DAY_DD_MM_YYYY != nil){
            DF_FULL_DAY_DD_MM_YYYY?.dateFormat = "EEEE, dd MMMM, yyyy"
            DF_FULL_DAY_DD_MM_YYYY?.timeZone = TimeZone.current
            DF_FULL_DAY_DD_MM_YYYY?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy = DateFormatter()
        if(DF_dd_MM_yyyy != nil){
            DF_dd_MM_yyyy?.dateFormat = "dd/MM/yyyy"
            DF_dd_MM_yyyy?.timeZone = TimeZone.current
            DF_dd_MM_yyyy?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy_TIME  = DateFormatter()
        if(DF_dd_MM_yyyy_TIME != nil){
            DF_dd_MM_yyyy_TIME?.dateFormat = "dd/MM/yyyy HH:mm:ss z"
            DF_dd_MM_yyyy_TIME?.timeZone = TimeZone.current
            DF_dd_MM_yyyy_TIME?.locale = Locale.current
        }
        
        DF_dd_MM_yyyy_TIME_12_HOURS = DateFormatter()
        if(DF_dd_MM_yyyy_TIME_12_HOURS != nil){
            //DF_dd_MM_yyyy_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm a"
            DF_dd_MM_yyyy_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm"
            DF_dd_MM_yyyy_TIME_12_HOURS?.timeZone = TimeZone.current
            DF_dd_MM_yyyy_TIME_12_HOURS?.locale = Locale.current
        }
        
        DF_dd_MM_TIME_12_HOURS = DateFormatter()
        if(DF_dd_MM_TIME_12_HOURS != nil){
            //DF_dd_MM_TIME_12_HOURS?.dateFormat = "dd/MM/yyyy HH:mm a"
            DF_dd_MM_TIME_12_HOURS?.dateFormat = "dd-MMM HH:mm"
            DF_dd_MM_TIME_12_HOURS?.timeZone = TimeZone.current
            DF_dd_MM_TIME_12_HOURS?.locale = Locale.current
        }
        
        DF_MMM_dd_yyyy = DateFormatter()
        if(DF_MMM_dd_yyyy != nil){
            DF_MMM_dd_yyyy?.dateFormat = "MMM dd, yyyy"
            DF_MMM_dd_yyyy?.timeZone = TimeZone.current
            DF_MMM_dd_yyyy?.locale = Locale.current
        }
    }

    func currentDateForConfirRX(date:Date) ->String? {
        return (DF_MMM_dd_yyyy?.string(from: date))!
    }
    
    func formatSimpleTime(date:Date) ->String? {
        return (DF_TIME_hh_mm_aa?.string(from: date))!
    }

    func formatFancyDate(date:Date) ->String? {
        return (DF_TIME_dd_MMMM_yyyy?.string(from: date))!
    }
    
    func formatFancyShortDate(date:Date) ->String? {
        return (DF_TIME_dd_MMM_yyyy?.string(from: date))!
    }
    
    func formatFancyDateForHistory(date:Date) ->String? {
        return (DF_TIME_dd_MMM?.string(from: date))!
    }
    func  formatSimpleDate(date:Date) -> String?{
        return (DF_TIME_dd_MMM_yy?.string(from: date))!
    }
    
    func formatDateToString(date:Date) -> String{
        return (DF_TIME_yyyy_MM_dd?.string(from: date))!
    }
    
    func formateDateToMonthString(date:Date) -> String{
        return (DF_MMMM?.string(from: date))!
    }
    
    func formateFullDateStr(date:Date) -> String{
        return (DF_FULL_DAY_DD_MM_YYYY?.string(from:date))!
    }
    
    func formateDateTimeToString(date:Date) -> String{
        return (DF_dd_MM_yyyy_TIME?.string(from:date))!
    }
    
    func formateDateTimeTo12HourTimeString(date:Date) -> String{
        return (DF_dd_MM_yyyy_TIME_12_HOURS?.string(from:date))!
    }
    
    func formateShortDateTimeTo12HourTimeString(date:Date) -> String{
        return (DF_dd_MM_TIME_12_HOURS?.string(from: date))!
    }

    func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.string(from: dt!)
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: "\(date) 12:00")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = DateTimeUtils.APP_DATE_FORMAT
        
        return dateFormatter.string(from: dt!)
    }
    
    func getHMSDateFromUTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dt = dateFormatter.date(from: date)
        
        let newDateFormatter = DateFormatter()
        newDateFormatter.timeZone   =   TimeZone.current
        newDateFormatter.dateFormat =   DateTimeUtils.APP_DATE_FANCY_FORMAT
        
        return newDateFormatter.string(from: dt!)
        
//        dateFormatter.timeZone = TimeZone.current
//        dateFormatter.dateFormat = AppConstant.APP_DATE_FORMAT
//        return dateFormatter.string(from: dt!)
    }
    
    func getHMDateFromUTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dt = dateFormatter.date(from: date)
        
        let newDateFormatter = DateFormatter()
        newDateFormatter.timeZone   =   TimeZone.current
        newDateFormatter.dateFormat =   "dd MMMM, yyyy"
        
        return newDateFormatter.string(from: dt!)

    }
    
    func getTimeFromUTCToLocal(strUTCDate:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: strUTCDate)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
        
        return dateFormatter.string(from: dt!)
    }
    
    func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: dt!)
    }
    
    
    func dateFromServerStr(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
      //  dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd"
       // let convertedStr = dateStr + " 00:00:00 z"
        let convertedDate = dateFormatter.date(from:dateStr)
        return convertedDate
    }
    
    func dateFromServerHMS_Date(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let convertedDate = dateFormatter.date(from:dateStr)
        return convertedDate
    }
    
    func dateFromServerWithNormalDate(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let convertedDate = dateFormatter.date(from:dateStr)
        return convertedDate
    }
    
    func dateFromServerWithTime(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
//        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "HH:mm:ss"
        let convertedDate = dateFormatter.date(from:dateStr)
        return convertedDate
    }
    
    func dateForCustomLog(dateStr:String, timeStr:String) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        let convertedStr = dateStr + " " + timeStr
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    
    func formatDayDateFormat(date:Date) -> String?{
        return (DF_DAY_MMM_dd_yyyy?.string(from: date))!
    }

    func getDeviceTimeZoneDateStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
//    func getHrsMinFromDateToCurrentDate(_ dateUnixFormate:Double) -> (Int, Int){
//        let date = Date(timeIntervalSince1970: dateUnixFormate)
//        let serverTimeSeconds = Int(PMHelper.getServerTime()!)!
//
//        let startTimeStamp = Int(date.timeIntervalSince1970)*1000
//        let endTimeStamp = Int(((Date().timeIntervalSince1970)*1000)) + (serverTimeSeconds)
//        let totalSeconds = (endTimeStamp - startTimeStamp)/1000
//
//        let hrs = (totalSeconds/60)/60
//        let mins: Int = (Int(round(Double(totalSeconds))) / 60) % 60
//
//        return (Int(hrs), Int(mins))
//    }
    
    func getDeviceTimeZoneTimeStrFromUnixFormat(_ dateUnixFormate:Double) -> String?{
        let date = Date(timeIntervalSince1970: dateUnixFormate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "hh:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func stringToDate(_ str: String)->Date{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: str)!
    }
    
    func dateToString(_ str: Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle=DateFormatter.Style.short
        return dateFormatter.string(from: str)
    }
    
    
    func getCurrentMonthName() -> String{
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let nameOfMonth = dateFormatter.string(from: now)
        return nameOfMonth
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60)
    }
    
    func dateFromStr(dateStr:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss z"
        //let convertedStr = dateStr + " 00:00:00 z"
        let convertedStr = dateStr
        let convertedDate = dateFormatter.date(from:convertedStr)
        return convertedDate
    }
    func fancyDateFromStr(dateStr:String) -> Date? {
        let convertedStr = dateStr
        let convertedDate = DF_TIME_dd_MMMM_yyyy?.date(from:convertedStr)
        return convertedDate
    }
    
    func fancyShortDateFromStr(dateStr:String) -> Date? {
        let convertedStr = dateStr
        let convertedDate = DF_TIME_dd_MMM_yyyy?.date(from:convertedStr)
        return convertedDate
    }
    
    func todayDateWithStartTime() -> Date?{
        let todayDate = Date()
        let todayDateStr = DF_dd_MM_yyyy?.string(from: todayDate)
        let todayDateWithTimeStr = "\(todayDateStr!) 00:00:00 z"
        
        return dateFromStr(dateStr: todayDateWithTimeStr)
    }
    
    func todayDateWithEndTime() -> Date?{
        let todayDate = Date()
        let todayDateStr = DF_dd_MM_yyyy?.string(from: todayDate)
        let todayDateWithTimeStr = "\(todayDateStr!) 23:59:59 z"
        
        return dateFromStr(dateStr: todayDateWithTimeStr)
    }
    
    func getDateFromTimeInterval(timeInterval:TimeInterval) -> Date{
//        return Date(timeIntervalSince1970: timeInterval / 1000.0)
        return Date(timeIntervalSince1970: timeInterval )
    }
    
    func getTimeIntervalFromDate(date:Date) -> TimeInterval{
        return (date.timeIntervalSince1970)*1000
    }
    
    func getimeIntervalFromDate(timeInterval:Date) -> String{
        return String(describing: Date.timeIntervalSince(timeInterval))
    }
    
//    func getDateFromStringTimeInterval(timeInterval:String) -> Date?{
//        let dateTimeMilliSecondString:String? = BaseApp.sharedInstance.removeOptionalWordFromString(timeInterval)
//
//        if(dateTimeMilliSecondString != nil){
//            let messageDate:Date? = self.getDateFromTimeInterval(timeInterval: Double(dateTimeMilliSecondString!)!)
//            return messageDate
//        }
//        return nil
//    }
    
    func toLocalDate(_ withDateString : String, with format : String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  format
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        return dateFormatter.date(from: withDateString)
    }

}

extension Date {
    
    func getCurrentMonth() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    
    func getCurrentYear() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    
    /// Returns a Date with the specified days added to the one it is called with
    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        var targetDay: Date
        targetDay = Calendar.current.date(byAdding: .year, value: years, to: self)!
        targetDay = Calendar.current.date(byAdding: .month, value: months, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .day, value: days, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .hour, value: hours, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .minute, value: minutes, to: targetDay)!
        targetDay = Calendar.current.date(byAdding: .second, value: seconds, to: targetDay)!
        return targetDay
    }
    
    /// Returns a Date with the specified days added to the one it is called with
    func add(years: Int = 0) -> Date {
        var targetDay: Date
        targetDay = Calendar.current.date(byAdding: .year, value: years, to: self)!
        return targetDay
    }
    
    /// Returns a Date with the specified days subtracted from the one it is called with
    func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date {
        let inverseYears = -1 * years
        let inverseMonths = -1 * months
        let inverseDays = -1 * days
        let inverseHours = -1 * hours
        let inverseMinutes = -1 * minutes
        let inverseSeconds = -1 * seconds
        return add(years: inverseYears, months: inverseMonths, days: inverseDays, hours: inverseHours, minutes: inverseMinutes, seconds: inverseSeconds)
    }
    
    
    // MARK: - Format dates
    
    func stringFromFormat(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    // MARK: - Format dates
    
    
    func appSpecificStringFromFormat() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = DateTimeUtils.APP_DATE_FORMAT
        formatter.timeZone = TimeZone.current
        return formatter.string(from: self)
    }
    
    func appSpecificStringFromFormatWithTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = DateTimeUtils.APP_DATE_FANCY_FORMAT
        formatter.timeZone = TimeZone.current
        return formatter.string(from: self)
    }
    
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
//    func appSpecificFancyStringFromFormat() -> String {
//        let formatter = DateFormatter()
//        //        formatter.timeZone = TimeZone.current
//        let currentDate = Date().toLocalTime()
//        let dateToCheck = self.toLocalTime()
//        let dayDiffrence = Calendar.current.dateComponents( [.day], from: currentDate, to: dateToCheck ).day ?? 0
//        if dayDiffrence == 0 {
//            formatter.dateFormat = ", hh:mm a"
//            return "\(LocalizationKeys.today.getLocalized())\(formatter.string(from: self))"
//        } else {
//            formatter.dateFormat = DateTimeUtils.APP_DATE_FANCY_FORMAT
//            return formatter.string(from: self)
//        }
//    }
    
    func apiSpecificStringFromFormat() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = DateTimeUtils.APP_API_REQUEST_DATE_FORMAT
        //formatter.timeZone = TimeZone(abbreviation: "UTC")
        return formatter.string(from: self)
    }
    
    // MARK: - Differences
    
//    func differenceWith(_ date: Date, inUnit unit: NSCalendar.Unit) -> Int {
//        return (calendar.components(unit, from: self, to: date, options: []) as NSDateComponents).value(forComponent: unit)
//    }
    

}

extension Date{

    func isSameDate(_ comparisonDate: Date) -> Bool {
      let order = Calendar.current.compare(self, to: comparisonDate, toGranularity: .day)
      return order == .orderedSame
    }
    
    func isBeforeDate(_ comparisonDate: Date) -> Bool {
      let order = Calendar.current.compare(self, to: comparisonDate, toGranularity: .day)
      
      return order == .orderedAscending
    }
    
    func isAfterDate(_ comparisonDate: Date) -> Bool {
      let order = Calendar.current.compare(self, to: comparisonDate, toGranularity: .day)
      return order == .orderedDescending
    }
    
    var startOfDay: Date {
      return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date? {
      var components = DateComponents()
      components.day = 1
      components.second = -1
      return Calendar.current.date(byAdding: components, to: startOfDay)
    }
}

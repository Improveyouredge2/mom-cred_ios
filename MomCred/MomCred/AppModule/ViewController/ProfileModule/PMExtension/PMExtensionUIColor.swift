//
//  ExtensionUIColor.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.characters.index(hexString.startIndex, offsetBy: 1)
            let hexColor = hexString.substring(from: start)
            
            if hexColor.characters.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
    
    /**
     *  @return A color object containing HSB values similar to the iOS 7 messages app green bubble color.
     */
    class func msg_messageBubbleGreenColor() -> UIColor {
        return UIColor(hue: CGFloat(130.0 / 360.0), saturation: CGFloat(0.68), brightness: CGFloat(0.84), alpha: CGFloat(1.0))
    }
    
    /**
     *  @return A color object containing HSB values similar to the iOS 7 messages app green bubble color.
     */
    class func appTheamRedColor() -> UIColor {
        return UIColor.init(red: 206/255.0, green: 12/255.0, blue: 93/255.0, alpha: 1.0)
    }
    
    /**
     *  @return A color object containing HSB values similar to the iOS 7 messages app green bubble color.
     */
    class func appTheamWhiteColor() -> UIColor {
        return UIColor.init(red: 249/255.0, green: 249/255.0, blue: 249/255.0, alpha: 1.0)
    }
    
    /**
     *  @return A color object containing HSB values similar to the iOS 7 messages app blue bubble color.
     */
    class func msg_messageBubbleBlueColor() -> UIColor {
        return UIColor(hue: CGFloat(210.0 / 360.0), saturation: CGFloat(0.94), brightness: CGFloat(1.0), alpha: CGFloat(1.0))
    }
    
    /**
     *  @return A color object containing HSB values similar to the iOS 7 red color.
     */
    class func msg_messageBubbleRedColor() -> UIColor {
        return UIColor(hue: CGFloat(0.0 / 360.0), saturation: CGFloat(0.79), brightness: CGFloat(1.0), alpha: CGFloat(1.0))
    }
    
    /**
     *  @return A color object containing HSB values similar to the iOS 7 messages app light gray bubble color.
     */
    class func msg_messageBubbleLightGrayColor() -> UIColor {
        return UIColor(hue: CGFloat(240.0 / 360.0), saturation: CGFloat(0.02), brightness: CGFloat(0.92), alpha: CGFloat(1.0))
    }
    
    //MARK: - Utilities
    /**
     *  Creates and returns a new color object whose brightness component is decreased by the given value, using the initial color values of the receiver.
     *
     *  @param value A floating point value describing the amount by which to decrease the brightness of the receiver.
     */
    func msg_colorByDarkeningColor(withValue value: CGFloat) -> UIColor {
        let totalComponents = self.cgColor.numberOfComponents
        let isGreyscale = (totalComponents == 2) ? true : false
        var oldComponents: [CGFloat] = self.cgColor.components!
        var newComponents = [CGFloat](repeating: 0.0, count: 4)
        if isGreyscale {
            newComponents[0] = oldComponents[0] - value < 0.0 ? 0.0 : oldComponents[0] - value
            newComponents[1] = oldComponents[0] - value < 0.0 ? 0.0 : oldComponents[0] - value
            newComponents[2] = oldComponents[0] - value < 0.0 ? 0.0 : oldComponents[0] - value
            newComponents[3] = oldComponents[1]
        }else {
            newComponents[0] = oldComponents[0] - value < 0.0 ? 0.0 : oldComponents[0] - value
            newComponents[1] = oldComponents[1] - value < 0.0 ? 0.0 : oldComponents[1] - value
            newComponents[2] = oldComponents[2] - value < 0.0 ? 0.0 : oldComponents[2] - value
            newComponents[3] = oldComponents[3]
        }
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let newColor = CGColor(colorSpace: colorSpace, components: newComponents)
        let retColor = UIColor(cgColor: newColor!)
        return retColor
    }
}

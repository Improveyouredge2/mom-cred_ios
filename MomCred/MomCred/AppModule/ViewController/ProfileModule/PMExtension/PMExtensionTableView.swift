//
//  ExtensionTableView.swift
//
//

import Foundation
import UIKit

public extension UITableView {
    
    func reloadData(_ completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() }, completion: { _ in completion() })
    }
    
    var indexesOfVisibleSections: [Int] {
        // Note: We can't just use indexPathsForVisibleRows, since it won't return index paths for empty sections.
        var visibleSectionIndexes = [Int]()
        
        for i in 0..<numberOfSections {
            var headerRect: CGRect?
            // In plain style, the section headers are floating on the top, so the section header is visible if any part of the section's rect is still visible.
            // In grouped style, the section headers are not floating, so the section header is only visible if it's actualy rect is visible.
            if (self.style == .plain) {
                headerRect = rect(forSection: i)
            } else {
                headerRect = rectForHeader(inSection: i)
            }
            if headerRect != nil {
                // The "visible part" of the tableView is based on the content offset and the tableView's size.
                let visiblePartOfTableView: CGRect = CGRect(x: contentOffset.x, y: contentOffset.y, width: bounds.size.width, height: bounds.size.height)
                if (visiblePartOfTableView.intersects(headerRect!)) {
                    visibleSectionIndexes.append(i)
                }
            }
        }
        return visibleSectionIndexes
    }
    
    func scrollToBottom() {
        DispatchQueue.main.async {
            if(self.numberOfSections > 0){
                let indexPath = IndexPath(
                    row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                    section: self.numberOfSections - 1)
                if indexPath.row >= 0 {
                    self.scrollToRow(at: indexPath, at: .bottom, animated: false)
                }
            }
        }
    }
    
    func scrollToTop() {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            if indexPath.row >= 0 {
               self.scrollToRow(at: indexPath, at: .top, animated: false)
            }
        }
    }
    
    
//    internal var visibleSectionHeaders: [SectionHeaderView] {
//        var visibleSects = [SectionHeaderView]()
//        for sectionIndex in indexesOfVisibleSections {
//            if let sectionHeader = headerView(forSection: sectionIndex) {
//                visibleSects.append(sectionHeader as! SectionHeaderView)
//            }
//        }
//        
//        return visibleSects
//    }
}

extension UIStackView {
    
    func removeAllArrangedSubviews() {
        
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        
        // Deactivate all constraints
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}

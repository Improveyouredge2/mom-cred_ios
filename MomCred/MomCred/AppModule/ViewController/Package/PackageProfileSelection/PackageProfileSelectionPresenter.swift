//
//  PackageProfileSelectionPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  PurchasePresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PackageProfileSelectionPresenter {
    
    var view:PackageProfileSelectionViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PackageProfileSelectionViewController) {
        self.view = view
    }
}

extension PackageProfileSelectionPresenter{
    /**
     *  Get Home screen detail informatoin from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func serverQuickSearchListing(quickSearchRequest:QuickSearchRequest?){
        quickSearchRequest?.page_index = "\(view.currentPage)"
        QuickSearchService.serverQuickSearchListing(quickSearchRequest: quickSearchRequest, callback: { (status, response, message) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if status {
                    self?.view.currentPage += 1
                    self?.view.callbackListing(response?.service, quickSearchRequest, nil)
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    self?.view.callbackListing(nil, nil, nil)
                }
            }
        })
    }
}

//
//  PackageProfileSelectionViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * PurchaseViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PackageProfileSelectionViewController : LMBaseViewController {
    
    @IBOutlet private weak var searchOptionView: UIView! {
        didSet {
            searchOptionView.isHidden = true
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnOption1: UIButton!
    @IBOutlet weak var btnOption2: UIButton!
    
    @IBOutlet weak var textFieldSearch: UITextField!
    
    var selectedPackage:PackageListResponse.PackageDataResponse?
    var currentPage: Int = 1
    
    fileprivate var selectedOption = 1
    fileprivate var presenter = PackageProfileSelectionPresenter()
    fileprivate var searchText = ""
    fileprivate var quickSearchViewController:QuickSearchViewController!
    fileprivate var advanceSearchOptionListViewController:AdvanceSearchOptionListViewController!
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textFieldSearch.delegate = self
        
        presenter.connectView(view: self)
        btnOption1.setBackgroundColor(color: UIColor(hexString: ColorCode.spinnerOuterColor) ?? UIColor.yellow, forState: UIControl.State.normal)
        
        if(self.selectedPackage != nil){
            btnOption1.setTitle("\(self.selectedPackage?.pack_profile_service ?? "") from \(self.selectedPackage?.pack_profile ?? "") profile each", for: UIControl.State.normal)
            
            btnOption2.setTitle("\(self.selectedPackage?.pack_service ?? "") from 1 profile", for: UIControl.State.normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetSelectionStatus()
    }
}

extension PackageProfileSelectionViewController{
    
    /**
     *  Method udpate server response on screen controls.
     *
     *  @param key server response object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showSearchResult(result:DashboardAllServiceProviderResponseData?){
        
        if(result != nil && result?.busi != nil && (result?.busi?.count)! > 0){
//            let controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalServiceProviderStoryboard, viewControllerName: InstructionalServiceProviderListViewController.nameOfClass) as InstructionalServiceProviderListViewController
//            
//            controller.busiList = result?.busi
//            controller.screenTitle = self.searchText
//            
//            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

//MARK: Textfield delegates
extension PackageProfileSelectionViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        if(textField.text != nil && (textField.text?.length)! > 0){
            Spinner.show()
            
            self.searchText = textField.text ?? ""
            //presenter.getKeywordSearch(searchText: textField.text ?? "")
            
            let quickSearchRequest = QuickSearchRequest()            
            presenter.serverQuickSearchListing(quickSearchRequest: quickSearchRequest)
        }
        
        return true
    }
}

extension PackageProfileSelectionViewController {
    private func resetSelectionStatus() {
        btnOption1.setBackgroundColor(color: UIColor.clear, forState: UIControl.State.normal)
        btnOption2.setBackgroundColor(color: UIColor.clear, forState: UIControl.State.normal)
        btnOption1.isSelected = false
        btnOption2.isSelected = false
    }
    
    @IBAction func actionOptionSelection(_ sender: UIButton) {
        resetSelectionStatus()
        if btnOption1 == sender {
            btnOption1.isSelected = !btnOption1.isSelected
            selectedOption = 1
            btnOption1.setBackgroundColor(color: UIColor(hexString: ColorCode.spinnerOuterColor) ?? UIColor.yellow, forState: UIControl.State.normal)
        } else if btnOption2 == sender {
            btnOption2.isSelected = !btnOption2.isSelected
            selectedOption = 2
            btnOption2.setBackgroundColor(color: UIColor(hexString: ColorCode.spinnerOuterColor) ?? UIColor.yellow, forState: UIControl.State.normal)
        }
        
        showPackageBuilder()
    }
    
    private func showPackageBuilder() {
        let storyboard = UIStoryboard(name: HelperConstant.PackageStoryboard, bundle: nil)
        let pbVC = storyboard.instantiateViewController(withIdentifier: PackageBuilderViewController.nameOfClass) as! PackageBuilderViewController
        pbVC.packageBuilder.selectedPackage = selectedPackage
        if selectedOption == 1 {
            pbVC.packageBuilder.totalServiceSelection = Int(selectedPackage?.pack_profile_service ?? "") ?? 0
            pbVC.packageBuilder.totalProfileSelection = Int(self.selectedPackage?.pack_profile ?? "") ?? 0
        } else {
            pbVC.packageBuilder.totalServiceSelection = Int(self.selectedPackage?.pack_service ?? "") ?? 0
            pbVC.packageBuilder.totalProfileSelection = 1
        }
        navigationController?.pushViewController(pbVC, animated: true)
    }
    
    private func showAdvanceSearch() {
        if(self.advanceSearchOptionListViewController == nil){
            self.advanceSearchOptionListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: AdvanceSearchOptionListViewController.nameOfClass)
        }
        
        if(advanceSearchOptionListViewController != nil){
            self.advanceSearchOptionListViewController.callbackListing = callbackListing
            self.advanceSearchOptionListViewController.isServiceOnly = true
            HelperConstant.appDelegate.navigationController?.present(advanceSearchOptionListViewController, animated: true, completion: nil)
        }
    }
        
    @IBAction func actionSearchOptionSelection(_ sender: UIButton) {
        if(sender.tag == 2){
            if(self.advanceSearchOptionListViewController == nil){
                self.advanceSearchOptionListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: AdvanceSearchOptionListViewController.nameOfClass)
            }
            
            if(advanceSearchOptionListViewController != nil){
                self.advanceSearchOptionListViewController.callbackListing = callbackListing
                self.advanceSearchOptionListViewController.isServiceOnly = true
                HelperConstant.appDelegate.navigationController?.present(advanceSearchOptionListViewController, animated: true, completion: nil)
            }
        } else {
            if(self.quickSearchViewController == nil){
                self.quickSearchViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: QuickSearchViewController.nameOfClass)
            }
            
            if(self.quickSearchViewController != nil){
                self.quickSearchViewController.callbackListing = callbackListing
                self.quickSearchViewController.callbackServiceProvider = nil
                HelperConstant.appDelegate.navigationController?.present(self.quickSearchViewController, animated: true, completion: nil)
            }
        }
    }
}

extension PackageProfileSelectionViewController{
    
    func callbackListing(_ serviceList:[ServiceAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceSearchRequest:ServiceSearchRequest?) -> Void?{
        
        let packageBuildViewController:PackageBuildViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackageBuildViewController.nameOfClass) as PackageBuildViewController
        
        if(selectedOption == 1){
            packageBuildViewController.totalServiceSelection = Int(self.selectedPackage?.pack_profile_service ?? "") ?? 0
            packageBuildViewController.totalProfileSelection = Int(self.selectedPackage?.pack_profile ?? "") ?? 0
        } else {
            packageBuildViewController.totalServiceSelection = Int(self.selectedPackage?.pack_service ?? "") ?? 0
            packageBuildViewController.totalProfileSelection = 1
        }
        
        packageBuildViewController.serviceList = serviceList
        
        self.navigationController?.pushViewController(packageBuildViewController, animated: true)
     
        return nil
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate

extension PackageProfileSelectionViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 15
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PackageListCell.nameOfClass) as! PackageListCell
        
        //        let indexData = self.helpArr[indexPath.row]
        //
        //        cell.lblTitle.text = indexData.question?.trim()
        //
        //        cell.btnExpand.tag = indexPath.row
        //        cell.btnExpand.addTarget(self, action: #selector(expandButtonAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        return UITableView.automaticDimension
        return 130
    }
}
//
//class PackageListCell: UITableViewCell {
//    @IBOutlet weak var lblTitle : UILabel!
//    @IBOutlet weak var lblSubTitle : UILabel!
//}

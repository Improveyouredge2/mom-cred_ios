//
//  PackageBuildTableViewCell.swift
//  MomCred
//
//  Created by consagous on 05/06/19.
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit

protocol PackageDetailTableViewCellDelegate {
    func removeCell(cellIndex: IndexPath)
    func serviceIncrease(cellIndex: IndexPath)
    func serviceDecrease(cellIndex: IndexPath)

}

class PackageDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProfilePic : ImageLayerSetup!
//    @IBOutlet weak var lblUserName : UILabel!
    
    @IBOutlet weak var lblTitle         : UILabel!
    @IBOutlet weak var lblBusiName    : UILabel!
    @IBOutlet weak var lblDesciption    : UILabel!
    @IBOutlet weak var lblPrice         : UILabel!
    @IBOutlet weak var lblAddress       : UILabel!
    @IBOutlet weak var lblCredit      : UILabel!
    @IBOutlet weak var lblServiceQty      : UILabel!

    @IBOutlet weak var lblCharitable      : UILabel!
    @IBOutlet weak var lblCharitableAmt      : UILabel!
    
    @IBOutlet weak var stackViewCharitable: UIStackView!
    
    var cellIndex:IndexPath?
    var delegate:PackageDetailTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.cardView.backgroundColor   =   UIColor(hexString: ColorCode.cardColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func actionCloseSelection(_ sender: UIButton) {
        
        if(delegate != nil){
            delegate?.removeCell(cellIndex: cellIndex!)
        }
    }
    @IBAction func actionPlushSelection(_ sender: UIButton) {
        
        if(delegate != nil){
            delegate?.serviceIncrease(cellIndex: cellIndex!)
        }
    }
    @IBAction func actionMinusSelection(_ sender: UIButton) {
        
        if(delegate != nil){
            delegate?.serviceDecrease(cellIndex: cellIndex!)
        }
    }

}

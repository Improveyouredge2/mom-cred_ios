//
//  PackageDetailViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * PackageDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PackageDetailViewController : LMBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var constraintPackageListHeightConstant:NSLayoutConstraint!
    
    @IBOutlet weak var lblSelectionCount:UILabel!
    @IBOutlet weak var lblTotalPrice:UILabel!
    
    var totalServiceSelection  = 0
    var totalProfileSelection  = 0
    var totalServiceCount = 0
    
    fileprivate var packageAmount: Double = 0
    fileprivate var creditAmount: Double = 0
    fileprivate var totalAmount: Double = 0
    fileprivate var packagePaymentViewController:PackagePaymentViewController?
    
    var serviceList:[ServiceAddRequest]?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //presenter.connectView(view: self)
        
//        btnOption1.setTitle("\(self.selectedPackage?.pack_profile_service ?? "") from \(self.selectedPackage?.pack_profile ?? "") profile each", for: UIControl.State.normal)
//
//        btnOption2.setTitle("\(self.selectedPackage?.pack_service ?? "") from 1 profile", for: UIControl.State.normal)
        
        self.lblSelectionCount.text = "\(self.totalServiceSelection) from \(self.totalProfileSelection) profile each"
        
//        "\(self.totalProfileSelection) from 1 profile"
        self.lblTotalPrice.text = ""
        
        self.calculateTotalPrice()
        
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.packagePaymentViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.tableView.contentSize.height > 0){
             tableView.scrollToBottom()
            self.constraintPackageListHeightConstant?.constant =
                self.tableView.contentSize.height + self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
}

extension PackageDetailViewController{
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintPackageListHeightConstant.constant = heightOfTableView
        }
    }
    
    fileprivate func calculateTotalPrice(){
        if(self.serviceList != nil && (self.serviceList?.count)! > 0){
            
            var price: Double = 0
            var creditTotal: Double = 0
            
            for service in (serviceList)! {
                
                if(service.service_price != nil && service.service_price?.service_price != nil){
                    
                    var servicePrice = Double(service.service_price?.service_price ?? "0") ?? 0
                    
                    if(service.donationAmt != nil){
                        servicePrice += Double(service.donationAmt ?? 0)
                    }
                    
                    price += servicePrice
                    
                    if(service.credit_system != nil && (service.credit_system?.length)! > 0 && (service.credit_system == "1")){ // 15% credit
                        let creditPercent = service.creditpercent ?? ""
                        
                        let servicePrice = service.service_price?.service_price ?? ""
                        
                        if let serivePr = NumberFormatter().number(from: servicePrice), let creditPer = NumberFormatter().number(from: creditPercent) {
                            let totalCreditCharges = (creditPer.doubleValue * serivePr.doubleValue)/100
                            creditTotal = creditTotal + totalCreditCharges
                        }
                    }
                }
            }
            
            let totalPrice = String(format: "%.2f", price)
            self.packageAmount = price
            self.lblTotalPrice.text = "$\(totalPrice)"
            self.creditAmount = creditTotal
            
//            self.totalAmount = self.packageAmount - self.creditAmount
            
            self.totalAmount = self.packageAmount
            
        } else {
            self.lblTotalPrice.text = "$"
        }
    }
    
    fileprivate func updateSelectionMsg() -> String {
        
        if(self.serviceList != nil && (self.serviceList?.count)! > 0){
            let remainingService = self.totalServiceCount - (self.serviceList?.count)!
            
            let remainingString = String(format: LocalizationKeys.service_selection_remaining.getLocalized(), arguments: ["\(remainingService)"])
            
            return remainingString
        }
        
        return ""
    }
}

extension PackageDetailViewController{
    
    @IBAction func actionResetSelection(_ sender: UIButton) {
        
    }
    
    @IBAction func actionProceedToPaySelection(_ sender: UIButton) {
        
        if(serviceList != nil && self.totalServiceCount == (serviceList?.count)!){
            
            if(self.packagePaymentViewController == nil){
                self.packagePaymentViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackagePaymentViewController.nameOfClass) as PackagePaymentViewController
                
                self.packagePaymentViewController?.serviceList = self.serviceList
                self.packagePaymentViewController?.packageAmount = self.packageAmount
                self.packagePaymentViewController?.creditAmount = self.creditAmount
                self.packagePaymentViewController?.totalAmount = self.totalAmount
                
                self.packagePaymentViewController?.packageInfo = "\(self.totalServiceSelection),\(self.totalProfileSelection)"
                
                self.navigationController?.pushViewController(self.packagePaymentViewController!, animated: true)
            }
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: self.updateSelectionMsg(), buttonTitle: nil, controller: nil)
        }
    }
}


// MARK:- UITableViewDataSource & UITableViewDelegate

extension PackageDetailViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.serviceList != nil) ? (self.serviceList?.count)! : 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        let cell = tableView.dequeueReusableCell(withIdentifier: PackageDetailTableViewCell.nameOfClass) as! PackageDetailTableViewCell
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        let obj = self.serviceList![indexPath.row]
        
        //TODO: For testing
        if(obj.mediabusiness != nil && (obj.mediabusiness?.count)! > 0){
            cell.imgProfilePic.sd_setImage(with: URL(string: (obj.mediabusiness![0].thumb ?? "")))
        } else {
            cell.imgProfilePic.image = nil
        }
        
        cell.lblTitle.text = obj.service_name ?? ""
        cell.lblBusiName.text = "By: \(obj.busi_title ?? "")"
        
        if(obj.service_field != nil && (obj.service_field?.count)! > 0){
            cell.lblDesciption.text = obj.service_field![0].specificFieldName
        } else {
            cell.lblDesciption.text = ""
        }
        
        var address = ""
        if(obj.service_location_info?.location_name != nil && (obj.service_location_info?.location_name?.length)! > 0){
            address = obj.service_location_info?.location_address ?? ""
            
        } else if(obj.service_location_info?.location_name_sec != nil && (obj.service_location_info?.location_name_sec?.length)! > 0){
            address = obj.service_location_info?.location_address_sec ?? ""
        }
        
        cell.lblAddress.text = address
        
        if(obj.service_price != nil && obj.service_price?.service_price != nil){
            cell.lblPrice.text = "$ \(obj.service_price?.service_price ?? "")"
        } else {
            cell.lblPrice.text = "$"
        }
        
        if(obj.credit_system != nil && (obj.credit_system?.length)! > 0 && (obj.credit_system == "1")){ // 15% credit
            let creditPercent = obj.creditpercent ?? ""
            
            let servicePrice = obj.service_price?.service_price ?? ""
            
            cell.lblCredit.text = ""
            if let serivePr = NumberFormatter().number(from: servicePrice){
                
                if let creditPer = NumberFormatter().number(from: creditPercent){
                    
                    //                let paymentGateCharge = Float(truncating: creditPer)
                    let totalCreditCharges = (creditPer.floatValue * serivePr.floatValue)/100
                    
                    cell.lblCredit.text = String(format: "$ %.2f", totalCreditCharges)
                }
            }
        } else {
            cell.lblCredit.text = ""
        }
        
        
        let totalAmount = ""
        if(obj.service_event != nil && obj.service_event?.causeCharitablePercentage != nil){
            if(obj.donationAmt == nil && obj.donationPer == nil){
                let serviceAmount = Float(obj.service_price?.service_price ?? "0.00") ?? 0.00
                let donationPer = Float(obj.service_event?.causeCharitablePercentage ?? "0.00") ?? 0.00
                let donationAmt = (serviceAmount * donationPer) / 100
                //totalAmount = "\(serviceAmount + donationAmt)"
                
                obj.donationAmt = donationAmt
                obj.donationPer = donationPer
            }
            
//            cell.lblCharitableAmt.text = "$\(obj.donationAmt ?? "")"
            cell.lblCharitableAmt.text = String(format: "$ %.2f", obj.donationAmt ?? "0.00")
            
            cell.stackViewCharitable.isHidden = false
            cell.lblCharitable.isHidden = false
        } else {
            cell.lblCharitable.isHidden = true
            cell.stackViewCharitable.isHidden = true
        }
        
        if(totalAmount.length > 0){
            cell.lblPrice.text = "$ \(totalAmount)"
        }
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        //        return UITableView.automaticDimension
//        return 180
//    }
}

extension PackageDetailViewController: PackageDetailTableViewCellDelegate{
    func removeCell(cellIndex: IndexPath){
        
        let obj = self.serviceList![cellIndex.row]
        self.serviceList?.removeAll{$0.service_id == obj.service_id}
        self.tableView.reloadData()
    }
    func serviceIncrease(cellIndex: IndexPath) {
        
    }
    func serviceDecrease(cellIndex: IndexPath){
        
        
    }
}

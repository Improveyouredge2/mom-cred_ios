//
//  PackageBuilderViewController.swift
//  MomCred
//
//  Created by MD on 03/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let packageServicesUpdated = Notification.Name("PackageServicesUpdatedNotification")

}

class PackageBuilderData: NSObject {
    var selectedServices: [ServiceAddRequest] = [] {
        didSet {
            calculateTotalPrice()
            NotificationCenter.default.post(name: .packageServicesUpdated, object: selectedServices)
        }
    }
    
    var selectedPackage: PackageListResponse.PackageDataResponse?

    var totalProfileSelection: Int = 0
    var totalServiceSelection: Int = 0
    
    var totalServices: Int { totalProfileSelection * totalServiceSelection }
    var isSelectionComplete: Bool { selectedServices.count == totalServiceSelection * totalProfileSelection }
    
    private(set) var packageAmount: Double = 0
    private(set) var creditAmount: Double = 0

    fileprivate func calculateTotalPrice() {
        var price: Double = 0.00
        var creditTotal: Double = 0.00
        
        for service in selectedServices {
            if let servicePriceStr = service.service_price?.service_price, let servicePrice = Double(servicePriceStr) {
                price += servicePrice
                if let donationAmt = service.donationAmt {
                    price += Double(donationAmt)
                }
                
                if let selectedPackage = selectedPackage, let percent = selectedPackage.pack_percent, let percentValue = Double(percent) {
                    let totalCreditCharges = (percentValue * servicePrice) / 100
                    creditTotal += totalCreditCharges
                } else if let creditSystem = service.credit_system, !creditSystem.trim().isEmpty, creditSystem == "1" { // 15% credit
                    let creditPercent = service.creditpercent ?? ""
                    if let creditPer = NumberFormatter().number(from: creditPercent)?.doubleValue {
                        let totalCreditCharges = (creditPer * servicePrice) / 100
                        creditTotal += totalCreditCharges
                    }
                }
            }
        }
        packageAmount = price
        creditAmount = creditTotal
    }
}

class PackageBuilderViewController: LMBaseViewController {
    @IBOutlet weak var tblServices: UITableView! {
        didSet {
            tblServices.estimatedRowHeight = 100
            tblServices.rowHeight = UITableView.automaticDimension
        }
    }
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var lblPackageType: UILabel!
    @IBOutlet weak var lblPackagePrice: UILabel!
    @IBOutlet weak var lblPackageCredit: UILabel!

    private(set) var packageBuilder: PackageBuilderData = .init()
    
    var shouldShowSearch: Bool = true
    
    private let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .spellOut
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: .packageServicesUpdated, object: nil)
        
        lblPackageType.text = "\(packageBuilder.totalServiceSelection) from \(packageBuilder.totalProfileSelection) profile each"
        
        updatePriceLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if packageBuilder.isSelectionComplete {
            shouldShowSearch = false
        } else if shouldShowSearch {
            showSearchProvider()
        }
//        btnContinueAction()
    }
    
    @objc private func reloadTable() {
        tblServices.reloadData()
        updatePriceLabel()
    }
    
    private func updatePriceLabel() {
        lblPackagePrice.attributedText = amountAttributedString(title: "Total Amount\n", amount: packageBuilder.packageAmount.formattedPrice)
        lblPackageCredit.attributedText = amountAttributedString(title: "Total Credit\n", amount: packageBuilder.creditAmount.formattedPrice)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .packageServicesUpdated, object: nil)
    }
    
    @IBAction private func btnContinueAction() {
        if packageBuilder.isSelectionComplete {
            showPaymentFlow() // proceed to payment
        } else {
            showSearchProvider()
        }
    }
    
    private func numberString(for value: Int) -> String {
        return numberFormatter.string(from: NSNumber(value: value)) ?? "\(value)"
    }
    
    private func showSearchProvider() {
        shouldShowSearch = false
        let serviceProviderViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: ServiceProviderViewController.nameOfClass) as ServiceProviderViewController
        var searchIndex = 1
        if packageBuilder.selectedServices.count > 0, packageBuilder.totalServiceSelection > 0 {
            searchIndex = (packageBuilder.selectedServices.count/packageBuilder.totalServiceSelection) + 1
        }
        
        var selectedProfiles: [String] = packageBuilder.selectedServices.compactMap { $0.busi_id }
        selectedProfiles.removeDuplicates()
        
        let searchIndexStr = numberString(for: searchIndex)
//        let profileIndexStr = numberString(for: packageBuilder.totalProfileSelection)
        let profileIndexStr = numberString(for: selectedProfiles.count + 1)
        serviceProviderViewController.packageSearchInfo = "Search \(searchIndexStr) for Profile \(profileIndexStr)"
        serviceProviderViewController.callbackServiceProvider = { [weak self] provider, quickSearchRequest, providerSearchRequest in
            if let provider = provider {
                self?.showServiceProviderListing(busiList: provider, quickSearchRequest: quickSearchRequest, serviceProviderSearchRequest: providerSearchRequest)
            }
            return nil
        }

        HelperConstant.appDelegate.navigationController?.present(serviceProviderViewController, animated: true, completion: nil)
    }
    
    private func showServiceProviderListing(busiList: [BIAddRequest], quickSearchRequest: QuickSearchRequest?, serviceProviderSearchRequest: ServiceProviderSearchRequest?) {
        let isplVC = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalServiceProviderStoryboard, viewControllerName: InstructionalServiceProviderListViewController.nameOfClass) as InstructionalServiceProviderListViewController
        
        isplVC.busiList = busiList
        isplVC.isAdvanceSearchResultShow = true
        isplVC.isBuildAPackage = true
        isplVC.screenTitle = LocalizationKeys.service_provider.getLocalized()
        isplVC.quickSearchRequest = quickSearchRequest
        isplVC.serviceProviderSearchRequest = serviceProviderSearchRequest
        isplVC.totalProfileSelection = packageBuilder.totalProfileSelection
        isplVC.totalServiceSelection = packageBuilder.totalServiceSelection
        isplVC.selectedServices = packageBuilder.selectedServices
        isplVC.selectedPackage = packageBuilder.selectedPackage
        navigationController?.pushViewController(isplVC, animated: true)
    }
    
    private func showPaymentFlow() {
        let ppVC = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackagePaymentViewController.nameOfClass) as PackagePaymentViewController
        
        ppVC.serviceList = packageBuilder.selectedServices
        ppVC.packageAmount = packageBuilder.packageAmount
        ppVC.creditAmount = packageBuilder.creditAmount
        ppVC.creditPercent = Double(packageBuilder.selectedPackage?.pack_percent ?? "") ?? 0
        ppVC.totalAmount = packageBuilder.packageAmount
        ppVC.packageInfo = "\(packageBuilder.totalServiceSelection),\(packageBuilder.totalProfileSelection)"
        navigationController?.pushViewController(ppVC, animated: true)
    }
    
    private func amountAttributedString(title: String, amount: String) -> NSAttributedString {
        let magentaColor = UIColor(hex: "C8057A")
        let attributedString = NSMutableAttributedString(string: title, attributes: [.foregroundColor: UIColor.white])
        attributedString.append(NSAttributedString(string: amount, attributes: [.foregroundColor: magentaColor]))
        return attributedString
    }
}

extension PackageBuilderViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return packageBuilder.selectedServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PackageBuildTableViewCell.nameOfClass) as! PackageBuildTableViewCell
        
        cell.cellIndex = indexPath
        cell.delegate = self

        cell.btnDelete.isHidden = false
        cell.btnSelectionContainer.isHidden = true

        cell.service = packageBuilder.selectedServices[indexPath.row]

        var selectionStatus = false
        if let serviceId = cell.service?.service_id {
            selectionStatus = packageBuilder.selectedServices.compactMap({ $0.service_id }).contains(serviceId)
        }
        cell.selectionStatus = selectionStatus
        
        var creditPercent: String?
        if let selectedPackage = packageBuilder.selectedPackage, let percent = selectedPackage.pack_percent {
            creditPercent = percent
        } else if let creditSystem = cell.service?.credit_system, !creditSystem.isEmpty, creditSystem == "1", let percent = cell.service?.creditpercent {
            creditPercent = percent
        }
        cell.creditPercent = creditPercent

        return cell
    }
}

extension PackageBuilderViewController: PackageBuildTableViewCellDelegate {
    func selectionStatus(cellIndex: IndexPath, status: Bool) -> Bool { return true }
    
    func packageBuildTableViewCellDidSelectRemove(_ cell: PackageBuildTableViewCell) {
        if let indexPath = cell.cellIndex {
            packageBuilder.selectedServices.remove(at: indexPath.row)
        }
    }
}

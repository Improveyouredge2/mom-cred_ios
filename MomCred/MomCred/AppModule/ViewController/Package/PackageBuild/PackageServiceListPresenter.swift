//
//  PackageServiceListPresenter.swift
//  MomCred
//
//  Created by MD on 30/12/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit

class PackageServiceListPresenter {
    weak var view: PackageServiceListViewController!
    
    func connectView(view: PackageServiceListViewController) {
        self.view = view
    }
}

extension PackageServiceListPresenter {
    func getServices(forBusinessUser userId: String, pageIndex index: Int = 1, completion: @escaping (Bool, [ServiceAddRequest]?, String?)->Void) {
        let strJSON = "{\"busi_user\": \(userId), \"current_page\": \(index)}"
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_SERVICES_BY_BUSINESS_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { status, response, error in
            if status { // Success
                if let responseDict = response {
                    let res: DashboardAllServiceResponseData? = DashboardAllServiceResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    print(res ?? "")
                    completion(status, res?.service, res?.message)
                }
            } else { // Failed
                completion(status, nil, error)
            }
        }
    }
}

//
//  PackageBuildTableViewswift
//  MomCred
//
//  Created by consagous on 05/06/19.
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit

protocol PackageBuildTableViewCellDelegate {
    func selectionStatus(cellIndex: IndexPath, status: Bool) -> Bool
    func packageBuildTableViewCellDidSelectRemove(_ cell: PackageBuildTableViewCell)
}

extension PackageBuildTableViewCellDelegate {
    func packageBuildTableViewCellDidSelectRemove(_ cell: PackageBuildTableViewCell) { }
    func remainingServiceSelection() -> Int { return 0 }
}

class PackageBuildTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProfilePic : ImageLayerSetup!
    @IBOutlet weak var serviceInfoView: UIStackView!
    @IBOutlet weak var serviceTypesView: UIStackView!
    @IBOutlet weak var lblDiscount : UILabel! { didSet { lblDiscount.isHidden = true } }
    @IBOutlet weak var btnSelectionContainer: UIView!
    @IBOutlet weak var btnSelection : UIButton!
    @IBOutlet weak var lblServiceCount: UILabel! { didSet { lblServiceCount.text = nil } }
    @IBOutlet weak var stepper: UIStepper! {
        didSet {
            stepper.isHidden = true
            stepper.backgroundColor = UIColor(hexString: ColorCode.spinnerOuterColor)
        }
    }

    @IBOutlet weak var btnDelete: UIButton! { didSet { btnDelete.isHidden = true } }
    
    var cellIndex: IndexPath?
    var delegate: PackageBuildTableViewCellDelegate?
    var creditPercent: String? {
        didSet {
            lblDiscount.isHidden = (creditPercent == nil)
            if let creditPercent = creditPercent {
                lblDiscount.text = "\(creditPercent)%\ncredit"
            }
        }
    }
    var allowMultiServiceSelection: Bool = false
    var selectionStatus: Bool = false {
        didSet {
            updateBtnSelection(selected: selectionStatus)
        }
    }
    var selectionCount: Int = 0 {
        didSet {
            currentCount = selectionCount
            updateStepperStatus(for: selectionCount)
        }
    }
    private var currentCount: Int = 0
    
    let appLogo: UIImage? = UIImage(named: "appLogo")
    
    var service: ServiceAddRequest? {
        didSet {
            if let image = service?.mediabusiness?.first?.thumb, let url = URL(string: image) {
                imgProfilePic.sd_setImage(with: url, placeholderImage: appLogo)
            } else if let image = service?.mediabusiness?.first?.imageurl, let url = URL(string: image) {
                imgProfilePic.sd_setImage(with: url, placeholderImage: appLogo)
            }  else {
                imgProfilePic.image = appLogo
            }
            
            var infoViews: [InfoStackView] = []
            if let value = service?.provider_name {
                let infoV = infoView(title: "Service provider", value: value)
                infoViews.append(infoV)
            }
            if let value = service?.service_name {
                let infoV = infoView(title: "Service Name", value: value)
                infoViews.append(infoV)
            }

            if let value = service?.service_instructor?.inFo_office {
                let infoV = infoView(title: "Service type", value: value)
                infoViews.append(infoV)
            }
            if let serviceFields = service?.service_field {
                for field in serviceFields {
                    if let fieldTitle = field.catFieldName, let specificField = field.specificFieldName {
                        let infoV = infoView(title: "Field category", value: fieldTitle)
                        infoViews.append(infoV)

                        let infoV1 = infoView(title: "Specific field", value: specificField)
                        infoViews.append(infoV1)
                    }
                }
            }

            if let purchasability = service?.purchasablity, purchasability == "1", let numberOfPurchase = service?.number_of_purchase {
                if numberOfPurchase.trim() == "0" {
                    let infoV = infoView(title: "Spots", value: "0 / Unlimited")
                    infoViews.append(infoV)
                } else {
                    let infoV = infoView(title: "Spots", value: "0 / \(numberOfPurchase)")
                    infoViews.append(infoV)
                }
            }
            if let value = service?.donationamount?.trim(), let amount = Double(value) {
                let infoV = infoView(title: "Donation Amount", value: amount.formattedPrice)
                infoViews.append(infoV)
            } else {
                let infoV = infoView(title: "Donation Amount", value: 0.0.formattedPrice)
                infoViews.append(infoV)
            }
            if let value = service?.service_price?.service_price?.trim(), let price = Double(value) {
                let infoV = infoView(title: "Price", value: price.formattedPrice)
                infoViews.append(infoV)
            } else {
                let infoV = infoView(title: "Price", value: 0.0.formattedPrice)
                infoViews.append(infoV)
            }
            if let value = service?.service_location_info?.location_address {
                let infoV = infoView(title: "Location by town", value: value)
                infoViews.append(infoV)
            }
            if let value = service?.service_calendar {
                var dateString: String = ""
                if let date = value.date {
                    dateString = date
                }
                if let time = value.time {
                    if dateString.trim().isEmpty {
                        dateString = time
                    } else {
                        dateString = "\(dateString)/\(time)"
                    }
                }
                if !dateString.trim().isEmpty {
                    let infoV = infoView(title: "Date/Time", value: dateString)
                    infoViews.append(infoV)
                }
            }

            serviceInfoView.removeAllArrangedSubviews()
            for view in infoViews {
                serviceInfoView.addArrangedSubview(view)
            }
            
            var serviceTypes = [UIImage]()
            if let creditSystem = service?.credit_system, creditSystem == "1", let imgCredit = UIImage(named: "ic_credit") {
                serviceTypes.append(imgCredit)
            }
            if let isDateEmpty = service?.service_calendar?.date?.isEmpty, let isTimeEmpty = service?.service_calendar?.time?.isEmpty, !isDateEmpty, !isTimeEmpty, let imgCalendar = UIImage(named: "ic_calendar") {
                serviceTypes.append(imgCalendar)
            }
            if let charitableEvent = service?.service_event?.causeCharitableEvent, let charitablePercentage = service?.service_event?.causeCharitablePercentage, !charitableEvent.isEmpty, !charitablePercentage.isEmpty, let imgDonation = UIImage(named: "ic_donation") {
                serviceTypes.append(imgDonation)
            } else if let donationAmt = service?.donationamount, let donationAmtValue = Int(donationAmt), donationAmtValue > 0, let imgDonation = UIImage(named: "ic_donation") {
                serviceTypes.append(imgDonation)
            }
            if let purchasability = service?.purchasablity, purchasability == "1", let imgPurchasability = UIImage(named: "ic_purchase") {
                serviceTypes.append(imgPurchasability)
            }
            if let exceptional = service?.service_information?.exceptionalServices, !exceptional.isEmpty, let imgExceptional = UIImage(named: "ic_exceptional") {
                serviceTypes.append(imgExceptional)
            }
            
            // remove all arranged subviews in case of reusing the cell
            serviceTypesView.removeAllArrangedSubviews()
            for serviceImage in serviceTypes {
                let imageView = UIImageView(image: serviceImage)
                imageView.contentMode = .scaleAspectFit
                serviceTypesView.addArrangedSubview(imageView)
            }
        }
    }
    
    @IBAction func actionSearchOptionSelection(_ sender: UIButton) {
        if !allowMultiServiceSelection, let cellIndex = cellIndex {
            _ = delegate?.selectionStatus(cellIndex: cellIndex, status: !btnSelection.isSelected)
        } else {
            updateBtnSelection(selected: true)
        }
    }
    
    @IBAction func btnDeleteAction() {
        delegate?.packageBuildTableViewCellDidSelectRemove(self)
    }

    func updateBtnSelection(selected: Bool) {
        if allowMultiServiceSelection {
            let maximumSelectionAllowed = delegate?.remainingServiceSelection() ?? 0
            if maximumSelectionAllowed > 0 {
                updateStepperStatus(for: 1)
//                stepper.maximumValue = Double(maximumSelectionAllowed)
                btnStepperAction(sender: stepper)
            } else if let cellIndex = cellIndex {
                // called just to display the error message
                _ = delegate?.selectionStatus(cellIndex: cellIndex, status: true)
            }
        } else {
            let btnBackgroundColor: UIColor = selected ? (UIColor(hexString: ColorCode.spinnerOuterColor) ?? UIColor.yellow) : UIColor.clear
            btnSelection.isSelected = selected
            btnSelection.setBackgroundColor(color: btnBackgroundColor, forState: UIControl.State.normal)
        }
    }
        
    private func updateStepperStatus(for value: Int) {
        stepper.isHidden = value <= 0
        stepper.value = Double(value)
        btnSelection.isHidden = value > 0
        lblServiceCount.text = value > 0 ? "\(value)" : nil

        if stepper.value <= 0 {
            lblServiceCount.text = nil
            btnSelection.isHidden = false
            stepper.isHidden = true
        }
    }
    
    @IBAction private func btnStepperAction(sender: UIStepper) {
        if let cellIndex = cellIndex {
            let stepperValue = Int(stepper.value)
            lblServiceCount.text = stepperValue > 0 ? "\(stepperValue)" : nil
            let didAdd: Bool? = delegate?.selectionStatus(cellIndex: cellIndex, status: currentCount < stepperValue)
            if let didAdd = didAdd, didAdd {
                currentCount = stepperValue
                updateStepperStatus(for: stepperValue)
            } else {
                updateStepperStatus(for: currentCount)
            }
        }
    }
    
    private func infoView(title: String, value: String) -> InfoStackView {
        let info = Bundle.main.loadNibNamed("InfoStackView", owner: self, options: nil)?.first as! InfoStackView
        info.lblTitle.text = title
        info.lblTitle.textColor = UIColor(hex: "C8047A")
        info.lblTitle.font = UIFont.systemFont(ofSize: 16)
        info.lblValue.text = value
        info.lblValue.textColor = .white
        info.lblValue.font = UIFont.systemFont(ofSize: 16)
        return info
    }
}

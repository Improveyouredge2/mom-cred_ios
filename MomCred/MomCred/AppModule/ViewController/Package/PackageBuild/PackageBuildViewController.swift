//
//  PackageBuildViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * PackageBuildViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PackageBuildViewController : LMBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewBottomSelectionPending: UITableView!
    @IBOutlet weak var btnBottomSelectionPending: UIButton!
    
    @IBOutlet weak var constraintBottomSelectionPendingHeightConstant:NSLayoutConstraint!
    
    fileprivate var selectionCount = 0
    fileprivate var constantBottomSelection:CGFloat = 0
    fileprivate var packageDetailViewController:PackageDetailViewController?
    
    var totalServiceSelection  = 0
    var totalProfileSelection  = 0
    var totalServiceCount = 0
    
    var serviceList:[ServiceAddRequest]?
    
    var selectedServiceList:[ServiceAddRequest]?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.constantBottomSelection = self.constraintBottomSelectionPendingHeightConstant.constant
        
        self.constraintBottomSelectionPendingHeightConstant.constant = 0
        
        //presenter.connectView(view: self)
        
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.packageDetailViewController = nil
    }
    
}

extension PackageBuildViewController{
    fileprivate func openNextScr(){
        if(self.packageDetailViewController == nil){
            self.packageDetailViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackageDetailViewController.nameOfClass) as PackageDetailViewController
            
            self.packageDetailViewController?.serviceList = self.selectedServiceList
            self.packageDetailViewController?.totalProfileSelection = self.totalProfileSelection
            self.packageDetailViewController?.totalServiceSelection = self.totalServiceSelection
            self.packageDetailViewController?.totalServiceCount = self.totalServiceCount
            self.navigationController?.pushViewController(self.packageDetailViewController!, animated: true)
        }
    }
}

extension PackageBuildViewController{
    @IBAction func actionResetSelection(_ sender: UIButton) {
        self.selectedServiceList?.removeAll()
        self.tableView.reloadData()
    }
    
    @IBAction func actionBottomSelectionPendingMethod(){
        
        if(self.isSelectionComplete()){
            self.openNextScr()
        }
    }
}

extension PackageBuildViewController{
    fileprivate func isSelectionValid(homeServiceResponse:ServiceAddRequest) -> Bool{
        
        var isFound = false
        
        if(self.selectedServiceList != nil && (self.selectedServiceList?.count)! > 0){
            
            // Check all profile info
            var profileIdList = self.selectedServiceList?.map{($0.busi_id ?? "")}
            
            profileIdList?.removeDuplicates()
            
            var totalProfileCount = profileIdList != nil ? (profileIdList?.count)! : 0
            if(!(profileIdList?.contains(homeServiceResponse.busi_id ?? "") ?? false)){
                totalProfileCount += 1
            }
            
            print(profileIdList)
            
            if(totalProfileCount <= self.totalProfileSelection){
                
                let data = self.selectedServiceList?.filter({$0.busi_id == homeServiceResponse.busi_id})
                
                var totalServiceCount = data != nil ? (data?.count)! : 0
                
                if(!(data?.map{($0.service_id ?? "")}.contains(homeServiceResponse.service_id ?? "") ?? false)){
                    totalServiceCount += 1
                }
                
                if(totalServiceCount <= self.totalServiceSelection){
                    if(!(data?.map{($0.service_id ?? "")}.contains(homeServiceResponse.service_id ?? "") ?? false)){
                        isFound = true
                    }
                } else {
//                    print("Service selection for service provider is exceed")
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.service_selection_exceed.getLocalized(), buttonTitle: nil, controller: nil)
                }
                
            } else {
//                print("Profile selection exceed")
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.profile_selection_exceed.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else {
            isFound = true
        }
        
        return isFound
        
    }
    
    fileprivate func addSelectedServiceList(homeServiceResponse:ServiceAddRequest){
        
        if(self.selectedServiceList == nil){
            self.selectedServiceList = []
        }
        
        self.selectedServiceList?.append(homeServiceResponse)
        
//        case service_selection_remaining
//        case service_selection_complete
        
        if(self.constraintBottomSelectionPendingHeightConstant.constant != self.constantBottomSelection){
            self.constraintBottomSelectionPendingHeightConstant.constant = self.constantBottomSelection
            //        self.view.setNeedsDisplay()
            //        self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        } else if((self.selectedServiceList?.count)! == 0){
            self.constraintBottomSelectionPendingHeightConstant.constant = 0
            self.view.layoutIfNeeded()
        }
        
        self.updateSelectionMsg()
    }
    
    fileprivate func isSelectionComplete() -> Bool{
        var isDone = false
        
        if(self.selectedServiceList != nil && (self.selectedServiceList?.count)! > 0){
            
            self.totalServiceCount = self.totalServiceSelection * self.totalProfileSelection
            
            if(self.totalServiceCount == (self.selectedServiceList?.count)!){
                isDone = true
            }
        }
        
        return isDone
    }
    
    fileprivate func updateSelectionMsg(){
        
        if(self.selectedServiceList != nil && (self.selectedServiceList?.count)! > 0){
            
            let totalService = self.totalServiceSelection * self.totalProfileSelection
            
            let remainingService = totalService - (self.selectedServiceList?.count)!
            
            var remainingString = String(format: LocalizationKeys.service_selection_remaining.getLocalized(), arguments: ["\(remainingService)"])
            
            if(totalService == (self.selectedServiceList?.count)!){
                remainingString = LocalizationKeys.service_selection_complete.getLocalized()
            }
            
            self.btnBottomSelectionPending.setTitle(remainingString, for: UIControl.State.normal)
        } else {
            self.constraintBottomSelectionPendingHeightConstant.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}


// MARK:- UITableViewDataSource & UITableViewDelegate

extension PackageBuildViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        return 15
        return (self.serviceList != nil) ? (self.serviceList?.count)! : 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PackageBuildTableViewCell.nameOfClass) as! PackageBuildTableViewCell
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        cell.service = serviceList?[indexPath.row]

        cell.cellIndex = indexPath
        cell.delegate = self

        cell.btnDelete.isHidden = false
        cell.btnSelectionContainer.isHidden = true

        var selectionStatus = false
        if let selectedServiceList = selectedServiceList, let serviceId = cell.service?.service_id {
            selectionStatus = selectedServiceList.compactMap({ $0.service_id }).contains(serviceId)
        }
        cell.selectionStatus = selectionStatus

        var creditPercent: String?
        if let creditSystem = cell.service?.credit_system, !creditSystem.isEmpty, creditSystem == "1", let percent = cell.service?.creditpercent {
            creditPercent = percent
        }
        cell.creditPercent = creditPercent

        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
//        return 130
    }
}

extension PackageBuildViewController: PackageBuildTableViewCellDelegate {
    func selectionStatus(cellIndex: IndexPath, status:Bool) -> Bool {
        
        let obj = self.serviceList![cellIndex.row]
        
        if(status){
            if(self.isSelectionValid(homeServiceResponse: obj)){
                self.addSelectedServiceList(homeServiceResponse: obj)
            }
            
            self.tableView.reloadData()
            
            if(self.isSelectionComplete()){
                //TODO: Next screen
                print("Next screen calling")
                
                self.openNextScr()
            }
            return true
        } else { // Remove selection
            self.selectedServiceList?.removeAll{$0.service_id == obj.service_id}
            self.updateSelectionMsg()
            self.tableView.reloadData()
            return true
        }
    }
}

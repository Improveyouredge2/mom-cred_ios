//
//  PackageServiceListViewController.swift
//  MomCred
//
//  Created by MD on 30/12/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit

class PackageServiceListViewController: LMBaseViewController {
    @IBOutlet weak var tblServices: UITableView! {
        didSet {
            tblServices.estimatedRowHeight = 100
            tblServices.rowHeight = UITableView.automaticDimension
        }
    }

    @IBOutlet weak var btnBottomSelectionPending: UIButton!
    
    @IBOutlet weak var constraintBottomSelectionPendingHeightConstant:NSLayoutConstraint!

    private var presenter = PackageServiceListPresenter()
    private var currentPage = 1
    private var serviceList: [ServiceAddRequest]? {
        didSet {
            tblServices.reloadData()
        }
    }
    var selectedPackage: PackageListResponse.PackageDataResponse?

    //MARK:- fileprivate
    private let refreshView = KRPullLoadView()
    private let loadMoreView = KRPullLoadView()
    private var isDownloadWorking = false
    private var isDisplayLoader = true
    
    private var selectionCount = 0
    private var constantBottomSelection: CGFloat = 0
    
    var totalServiceSelection: Int  = 0
    var totalProfileSelection: Int  = 0
    
    var selectedServiceList: [ServiceAddRequest]? {
        didSet {
            if tblServices != nil {
                tblServices.reloadData()
            }
        }
    }

    var busiInfo: BIAddRequest?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter.connectView(view: self)
        
        refreshView.delegate = self
        tblServices.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tblServices.addPullLoadableView(loadMoreView, type: .loadMore)
        
        constantBottomSelection = constraintBottomSelectionPendingHeightConstant.constant
        constraintBottomSelectionPendingHeightConstant.constant = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getServices()
    }
    
    private func getServices(completion: (() -> Void)? = nil) {
        if let busiUser = busiInfo?.busi_user {
            presenter.getServices(forBusinessUser: busiUser, pageIndex: currentPage) { (_, services, _) in
                DispatchQueue.main.async { [weak self] in
                    if let services = services, !services.isEmpty {
                        self?.currentPage += 1
                        self?.updateServiceList(services)
                    }
                    completion?()
                }
            }
        }
    }
    
    private func updateServiceList(_ services: [ServiceAddRequest]) {
        if serviceList != nil {
            serviceList?.append(contentsOf: services)
        } else {
            serviceList = services
        }
    }
    
    @IBAction func actionResetSelection(_ sender: UIButton) {
        selectedServiceList?.removeAll()
        updateSelectionMsg()
    }
    
    @IBAction func actionBottomSelectionPendingMethod() {
        let controller = navigationController?.viewControllers.first { $0 is PackageBuilderViewController }
        if let controller = controller as? PackageBuilderViewController {
            let updatedServices = selectedServiceList ?? []
            controller.packageBuilder.selectedServices = updatedServices
            controller.shouldShowSearch = true
            navigationController?.popToViewController(controller, animated: true)
        } else {
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    private func profileCount() -> Int {
        var totalProfileCount = 0
        if let selectedServices = selectedServiceList {
            var profileIdList = selectedServices.compactMap { $0.busi_id }
            profileIdList.removeDuplicates()
            totalProfileCount = profileIdList.count
        }
        return totalProfileCount
    }
    
    private func servicesCount() -> Int {
        var totalServiceCount = 0
        if let selectedServices = selectedServiceList, let busiId = busiInfo?.busi_id {
            let data = selectedServices.filter { $0.busi_id == busiId }
            totalServiceCount = data.count
        }
        return totalServiceCount
    }
    
    fileprivate func isSelectionValid(homeServiceResponse:ServiceAddRequest) -> Bool {
        var isFound = false
        if let selectedServices = selectedServiceList, !selectedServices.isEmpty {
            
            // Check all profile info
            var profileIdList = selectedServices.compactMap { $0.busi_id }
            profileIdList.removeDuplicates()
            
            var totalProfileCount = profileIdList.count
            if !profileIdList.contains(homeServiceResponse.busi_id ?? "") {
                totalProfileCount += 1
            }
            if totalProfileCount <= totalProfileSelection {
                let data = selectedServices.filter { $0.busi_id == homeServiceResponse.busi_id }
                let totalServiceCount = data.count
                
//                if !data.compactMap({ $0.service_id }).contains(homeServiceResponse.service_id ?? "") {
//                    totalServiceCount += 1
//                }
                
                if totalServiceCount < totalServiceSelection {
                    if totalServiceSelection > 1 {
                        isFound = true
                    } else {
                        let alreadyAdded = data.compactMap({ $0.service_id }).contains(homeServiceResponse.service_id ?? "")
                        if !alreadyAdded {
                            isFound = true
                        }
                    }
                } else {
//                    print("Service selection for service provider is exceed")
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.service_selection_exceed.getLocalized(), buttonTitle: nil, controller: nil)
                }
            } else {
//                print("Profile selection exceed")
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.profile_selection_exceed.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else {
            isFound = true
        }
        return isFound
    }
    
    fileprivate func addSelectedServiceList(homeServiceResponse:ServiceAddRequest){
        if selectedServiceList == nil {
            selectedServiceList = []
        }
        
        selectedServiceList?.append(homeServiceResponse)
        if constraintBottomSelectionPendingHeightConstant.constant != constantBottomSelection {
            constraintBottomSelectionPendingHeightConstant.constant = constantBottomSelection
//            view.layoutIfNeeded()
        } else if let isSelectionEmpty = selectedServiceList?.isEmpty, isSelectionEmpty {
            constraintBottomSelectionPendingHeightConstant.constant = 0
//            view.layoutIfNeeded()
        }
        
        updateSelectionMsg()
    }
    
    fileprivate func isSelectionComplete() -> Bool {
        var isDone = false
        var totalServiceCount: Int = 0
        if let selectedServices = selectedServiceList, !selectedServices.isEmpty {
            totalServiceCount = totalServiceSelection * totalProfileSelection
            
            if totalServiceCount == selectedServices.count {
                isDone = true
            }
        }
        
        return isDone
    }
    
    fileprivate func updateSelectionMsg() {
        if let selectedServices = selectedServiceList, !selectedServices.isEmpty, servicesCount() >= totalServiceSelection {
            let totalService = totalServiceSelection * totalProfileSelection
//            let remainingService = totalService - selectedServices.count
//            var remainingString = String(format: LocalizationKeys.service_selection_remaining.getLocalized(), arguments: ["\(remainingService)"])
            var remainingString: String = LocalizationKeys.package_selection_continue.getLocalized()
            
            if totalService == selectedServices.count {
                remainingString = LocalizationKeys.package_selection_complete.getLocalized()
            }
            
            self.btnBottomSelectionPending.setTitle(remainingString, for: UIControl.State.normal)
        } else {
            self.constraintBottomSelectionPendingHeightConstant.constant = 0
//            self.view.layoutIfNeeded()
        }
    }
}

extension PackageServiceListViewController: KRPullLoadViewDelegate {
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        switch type {
        case .refresh:
            switch state {
            case .none:
                pullLoadView.messageLabel.text = ""
                
            case let .pulling(offset, threshould):
                if offset.y > threshould {
                    //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                }
                isDisplayLoader = false
                
            case let .loading(completionHandler):
                pullLoadView.messageLabel.text = "Updating..."
                getServices() {
                    completionHandler()
                }
            }
        case .loadMore:
            switch state {
            case .pulling(_, _):
                print("Pulling")
            case let .loading(completionHandler):
                isDisplayLoader = false
                getServices() {
                    completionHandler()
                }
            case .none:
                break
            }
        }
    }
}

//MARK:- UITableView Datasource and Delegates
extension PackageServiceListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PackageBuildTableViewCell.nameOfClass) as! PackageBuildTableViewCell
        
        cell.cellIndex = indexPath
        cell.delegate = self
        cell.service = serviceList?[indexPath.row]
        
        var selectionCount: Int = 0
        if let selectedServices = selectedServiceList, let serviceId = cell.service?.service_id {
            selectionCount = selectedServices.compactMap({ $0.service_id }).filter({ $0 == serviceId }).count
        }
        
        var creditPercent: String?
        if let selectedPackage = selectedPackage, let percent = selectedPackage.pack_percent {
            creditPercent = percent
        } else if let creditSystem = cell.service?.credit_system, !creditSystem.isEmpty, creditSystem == "1", let percent = cell.service?.creditpercent {
            creditPercent = percent
        }
        cell.creditPercent = creditPercent
        
        cell.allowMultiServiceSelection = totalServiceSelection > 1
        if cell.allowMultiServiceSelection {
            cell.selectionCount = selectionCount
        } else {
            cell.selectionStatus = selectionCount > 0
        }

        return cell
    }
}

extension PackageServiceListViewController: UITableViewDelegate { }

extension PackageServiceListViewController: PackageBuildTableViewCellDelegate {
    func selectionStatus(cellIndex: IndexPath, status: Bool) -> Bool {
        if let obj = serviceList?[cellIndex.row] {
            if status {
                if isSelectionValid(homeServiceResponse: obj) {
                    addSelectedServiceList(homeServiceResponse: obj)
                    return true
                }
            } else {
                let serviceIndex = selectedServiceList?.firstIndex { $0.service_id == obj.service_id }
                if let serviceIndex = serviceIndex {
                    selectedServiceList?.remove(at: serviceIndex)
                    updateSelectionMsg()
                    return true
                }
            }
        }
        return false
    }
    
    func remainingServiceSelection() -> Int {
        return totalServiceSelection - servicesCount()
    }
}

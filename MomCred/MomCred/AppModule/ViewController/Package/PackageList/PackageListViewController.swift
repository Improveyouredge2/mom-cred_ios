//
//  PackageListViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * PackageListViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PackageListViewController : LMBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var expandTableNumber = [Int] ()
    var packageList:[PackageListResponse.PackageDataResponse]?
    
    fileprivate var packageProfileSelectionViewController:PackageProfileSelectionViewController?
    
    fileprivate let presenter = PackageListPresenter()
    
    fileprivate var logoutViewController:PMLogoutView?
    
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.packageProfileSelectionViewController = nil
        if let token = PMUserDefault.getAppToken(), !token.trim().isEmpty {
            self.presenter.getProfile()
        }
        self.presenter.getPackageList()
    }
}

extension PackageListViewController{
    /**
     *  Method udpate server response on screen controls.
     *
     *  @param key server response object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func updateScreenInfo(){
        
        // check server repsonse
        if(self.packageList != nil){
            self.tableView.reloadData()
        } else { // No notification in user account

        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate

extension PackageListViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return packageList != nil ? (packageList?.count)! : 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PackageListCell.nameOfClass) as! PackageListCell
        if let packageDetail = packageList?[indexPath.row] {
            cell.lblTitle.text = "\(packageDetail.pack_service ?? "") \(packageDetail.pack_title?.capitalized ?? "")"
            
            if let percent = packageDetail.pack_percent, let percentValue = Int(percent), percentValue > 0 {
                cell.lblSubTitle.text = "\(percentValue)% of each service's monetary value to credit"
                cell.lblSubTitle.isHidden = false
            } else {
                cell.lblSubTitle.isHidden = true
            }
        }
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        return UITableView.automaticDimension
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let subscriptionInfoData = PMUserDefault.getSubsriptionInfo()
        
        if(subscriptionInfoData != nil){
            let subscriptionInfo:SubscriptionDetail? = SubscriptionDetail().getModelObjectFromServerResponse(jsonResponse: (subscriptionInfoData ?? "") as AnyObject)
            
            if(subscriptionInfo != nil && subscriptionInfo?.sub_end != nil){
                self.packageProfileSelectionViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackageProfileSelectionViewController.nameOfClass) as PackageProfileSelectionViewController
                
                self.packageProfileSelectionViewController?.selectedPackage = self.packageList?[indexPath.row]
                self.navigationController?.pushViewController(self.packageProfileSelectionViewController!, animated: true)
            } else {
                //TODO: Show Subscription purchase
                
                self.logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
                self.logoutViewController?.delegate = self
                self.logoutViewController?.scrTitle = ""
                self.logoutViewController?.scrDesc = LocalizationKeys.error_no_subscription.getLocalized()
//                self.selectedIndex = sender.tag
                self.logoutViewController?.modalPresentationStyle = .overFullScreen
                self.present(logoutViewController!, animated: true, completion: nil)
            }
        }
    }
}

extension PackageListViewController: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr() {
        if(logoutViewController != nil){
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
        
//        Helper.sharedInstance.openTabScreenWithIndex(index: 2)
        Helper.sharedInstance.openPremiumMembershipView()
    }
}

//

class PackageListCell: UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
}

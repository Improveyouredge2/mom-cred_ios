//
//  PackageListService.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  PurchaseService is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PackageListService{
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getPackageList(callback:@escaping (_ status:Bool, _ response: PackageListResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        
//        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_PACKAGE, header: APIHeaders().getDefaultHeaders(), strJSON: nil)
        reqPost = APIManager().sendGetRequest(urlString: APIKeys.API_GET_PACKAGE, header: APIHeaders().getDefaultHeaders())
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:PackageListResponse? = PackageListResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

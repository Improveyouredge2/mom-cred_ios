//
//  PackageListPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  PurchasePresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PackageListPresenter {
    
    var view:PackageListViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PackageListViewController) {
        self.view = view
    }
}

extension PackageListPresenter{
    /**
     *  Get Home screen detail informatoin from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getPackageList(){
        
        PackageListService.getPackageList(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    if(response != nil && (response?.data != nil && (response?.data?.count)! > 0)){
                        self.view.packageList = response?.data
                    }
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    self.view.updateScreenInfo()
                }
            }
        })
    }
    
    /**
     *  Get user profile information from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getProfile(){
        PMEditProfileService.getData(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    PMUserDefault.saveSubsriptionInfo(subsriptionInfo: response?.loginResponseData?.subscription?.toJSONString() ?? "")
                    PMUserDefault.saveSubsriptionPrice(subsriptionPrice: response?.loginResponseData?.membershipprice ?? "")
                    
                    // stripefees
                    PMUserDefault.saveStripeCharges(subsriptionPrice: response?.loginResponseData?.stripefees ?? "")
                    
                    PMUserDefault.savePlanId(subsriptionPrice: response?.loginResponseData?.plan_id ?? "")
                }
            } else {
                OperationQueue.main.addOperation() {
//                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

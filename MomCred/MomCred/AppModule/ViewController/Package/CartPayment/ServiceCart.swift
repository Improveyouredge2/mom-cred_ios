//
//  ServiceCart.swift
//  MomCred
//
//  Created by MD on 01/12/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let cartUpdated = Notification.Name("ServiceCartItemsUpdated")
}

class ServiceCart {
    static let shared = ServiceCart()
    
    private(set) var cartServices: [ServiceAddRequest]? {
        didSet {
            NotificationCenter.default.post(name: .cartUpdated, object: cartServices)
        }
    }
    
    private(set) var serviceCartResponse: ServiceCartResponse?

    init() {
        getExistingCart()
    }
    
    func getExistingCart(completion: ((_ isEmpty: Bool) -> Void)? = nil) {
        CartNetworkAPI.getServicesInCart { (response, error) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                var isEmpty: Bool = true
                if let response = response {
                    self?.serviceCartResponse = response
                    self?.updateCartServiceItems(for: response)
                    isEmpty = response.cartItems?.isEmpty ?? true
                } else if let error = error {
                    print(error)
                }
                completion?(isEmpty)
            }
        }
    }
    
    private func updateCartServiceItems(for serviceResponse: ServiceCartResponse) {
        if let cartItems = serviceResponse.cartItems {
            for item in cartItems {
                if let serviceId = item.cartService {
                    if indexOfServiceId(serviceId) == nil {
                        ServiceService.getServiceDetailFor(serviceId: serviceId) { (addRequest, error) in
                            if let addRequest = addRequest {
                                DispatchQueue.main.async { [weak self] in
                                    Spinner.hide()
                                    self?.addServiceToCartServices(addRequest)
                                }
                            }
                        }
                    }
                }
            }
            if let existingServices = cartServices, cartItems.count < existingServices.count {
                let cartServiceIds = cartItems.compactMap { $0.cartService }
                let remainingServices = existingServices.filter { item in
                    if let serviceId = item.service_id {
                        return cartServiceIds.contains(serviceId)
                    }
                    return false
                }
                self.cartServices = remainingServices
            }
        }
    }
    
    private func reOrder(array: [ServiceAddRequest], order: [ServiceCartItem]) -> [ServiceAddRequest] {
        let filteredOrderArray = array.filter { request -> Bool in
            let index = order.firstIndex { $0.cartService == request.service_id }
            return index != nil
        }
        let filteredArray = array.filter { request -> Bool in
            let index = order.firstIndex { $0.cartService == request.service_id }
            return index == nil
        }
        return filteredOrderArray + filteredArray
    }
    
    private func indexOfService(_ service: ServiceAddRequest) -> Int? {
        if let serviceId = service.service_id {
            return indexOfServiceId(serviceId)
        }
        return nil
    }
    
    private func indexOfServiceId(_ serviceId: String) -> Int? {
        return cartServices?.firstIndex { $0.service_id == serviceId }
    }
    
    func clearCartItems() {
        serviceCartResponse = nil
        cartServices?.removeAll()
    }

    func serviceExistsInCart(_ service: ServiceAddRequest) -> Bool {
        return indexOfService(service) != nil
    }
    
    private func addServiceToCartServices(_ service: ServiceAddRequest) {
        var existingServices = cartServices ?? [ServiceAddRequest]()
        existingServices.append(service)
        
        let serviceResponseCartItems = serviceCartResponse?.cartItems ?? []
        cartServices = reOrder(array: existingServices, order: serviceResponseCartItems)
    }
    
    func getServiceQuantity(_ service: ServiceAddRequest) -> String{
        
        let cartItem  = serviceCartResponse?.cartItems ?? []

        let data = cartItem.filter { $0.cartService == service.service_id}
        if data.count>0{
            return data[0].quantity ?? "0"
        }else {
             return "0"
        }
      
    }
    
    func addServiceToCart(_ service: ServiceAddRequest) {
        if !serviceExistsInCart(service) {
            if let serviceId = service.service_id, let businessId = service.busi_id, let userId = service.service_user {
                CartNetworkAPI.addServiceToCart(serviceId: serviceId, businessId: businessId, userId: userId) { (response, error) in
                    DispatchQueue.main.async { [weak self] in
                        Spinner.hide()
                        if let response = response {
                            if let serviceCartResponse = self?.serviceCartResponse {
                                serviceCartResponse.cartItems = response.cartItems
                            } else {
                                self?.getExistingCart()
                            }
                            self?.addServiceToCartServices(service)
                        } else if let error = error {
                            print(error)
                        }
                    }
                }
            }
        }
    }
    func againAddServiceToCart(_ service: ServiceAddRequest) {
            if let serviceId = service.service_id, let businessId = service.busi_id, let userId = service.service_user {
                CartNetworkAPI.addServiceToCart(serviceId: serviceId, businessId: businessId, userId: userId) { (response, error) in
                    DispatchQueue.main.async { [weak self] in
                        Spinner.hide()
                        if let response = response {
                            self?.getExistingCart()
                        } else if let error = error {
                            print(error)
                        }
                    }
                }
            }
        
    }
    
    func removeServiceFromCart(_ service: ServiceAddRequest) {
        if let index = indexOfService(service) {
            let cartItem = serviceCartResponse?.cartItems?.first { $0.cartService == service.service_id }
            if let cartId = cartItem?.cartId {
                CartNetworkAPI.deleteServiceFromCart(cartId: cartId) { (response, error) in
                    DispatchQueue.main.async { [weak self] in
                        Spinner.hide()
                        if let response = response {
                            self?.serviceCartResponse?.cartItems = response.cartItems
                            self?.cartServices?.remove(at: index)
                        } else if let error = error {
                            print(error)
                        }
                    }
                }
            }
        }
    }
    
    func removeCartService(_ service: ServiceAddRequest) {
            let cartItem = serviceCartResponse?.cartItems?.first { $0.cartService == service.service_id }
     
            if let cartId = cartItem?.cartService {
                CartNetworkAPI.removeCartService(cartId: cartId) { (response, error) in
                    DispatchQueue.main.async { [weak self] in
                        Spinner.hide()
                        if let response = response {
                            self?.getExistingCart()
                        } else if let error = error {
                            print(error)
                        }
                    }
                }
            }
        
    }
    
    
}

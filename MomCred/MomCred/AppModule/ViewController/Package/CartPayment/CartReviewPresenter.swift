//
//  CartReviewPresenter.swift
//  MomCred
//
//  Created by MD on 30/11/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit

class CartReviewPresenter {
    
    var view: CartReviewViewController! // Object of account view screen
    
    func connectView(view: CartReviewViewController) {
        self.view = view
    }
}


extension CartReviewPresenter {
    func submitPaymentData(paymentInfo:PaymentInfo?, callback:@escaping (_ status:Bool, _ response: PaymentResponseInfo?) -> Void){
        PMMembershipPlanPaymentService.submitPaymentData(paymentInfo: paymentInfo, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    callback(status, response)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, response)
                }
            }
        })
    }
}

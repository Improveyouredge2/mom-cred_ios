//
//  CartItemsViewController.swift
//  MomCred
//
//  Created by MD on 30/11/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit

class CartItemsViewController: LMBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var constraintPackageListHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var lblSelectionCount: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    var totalServiceSelection = 0
    var totalProfileSelection = 0
    var totalServiceCount = 0
    
    fileprivate var totalAmount: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ServiceCart.shared.getExistingCart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(handleCartNotification), name: .cartUpdated, object: nil)
        handleCartNotification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .cartUpdated, object: nil)
    }
    
    @objc private func handleCartNotification() {
        tableView.reloadData()
        
        lblSelectionCount.text = "\(ServiceCart.shared.cartServices?.count ?? 0) services"
        calculateTotalPrice()

        if (ServiceCart.shared.cartServices?.count ?? 0) <= 0 {
            navigationController?.popViewController(animated: true)
        }
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if tableView.contentSize.height > 0 {
             tableView.scrollToBottom()
            constraintPackageListHeightConstant?.constant = tableView.contentSize.height + tableView.contentSize.height
            
            updateTableViewHeight()
        }
    }
}

extension CartItemsViewController {
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: { [weak self] in
            self?.tableView.layoutIfNeeded()
        }, completion: { [weak self] (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            if let cells = self?.tableView.visibleCells {
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
            }
            self?.constraintPackageListHeightConstant.constant = heightOfTableView
        })
    }
    
    fileprivate func calculateTotalPrice() {
        var price: Double = 0
        if let cartServices = ServiceCart.shared.cartServices {
            for service in cartServices {
                if let srvPrice = service.service_price?.service_price {
                    let servicePrice = Double(srvPrice) ?? 0
                    price += servicePrice
                }
            }
        }
        
        totalAmount = price
        lblTotalPrice.text = totalAmount.formattedPrice
    }
}

extension CartItemsViewController {
    @IBAction func actionResetSelection(_ sender: UIButton) {
        
    }
    
    @IBAction func actionProceedToPaySelection(_ sender: UIButton) {
        if let isEmpty = ServiceCart.shared.cartServices?.isEmpty, isEmpty {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: "Cart is already empty. Please add services before checkout.", buttonTitle: nil, controller: nil)
        } else {
            let cartStoryboard = UIStoryboard(name: "CartReview", bundle: nil)
            if let cartReview = cartStoryboard.instantiateViewController(withIdentifier: "CartReviewViewController") as? CartReviewViewController {
                cartReview.serviceList = ServiceCart.shared.cartServices
                cartReview.packageAmount = totalAmount
                cartReview.cartResponse = ServiceCart.shared.serviceCartResponse
                cartReview.packageInfo = "\(totalServiceSelection),\(totalProfileSelection)"
                navigationController?.pushViewController(cartReview, animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate

extension CartItemsViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ServiceCart.shared.cartServices?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        updateViewConstraints()
        let cell = tableView.dequeueReusableCell(withIdentifier: PackageDetailTableViewCell.nameOfClass) as! PackageDetailTableViewCell
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        let obj = ServiceCart.shared.cartServices?[indexPath.row]
        
        //TODO: For testing
        if let thumb = obj?.mediabusiness?.first?.thumb {
            cell.imgProfilePic.sd_setImage(with: URL(string: thumb))
        } else {
            cell.imgProfilePic.image = nil
        }
        if let ser = obj {
            cell.lblServiceQty.text = ServiceCart.shared.getServiceQuantity(ser)
        }else{
            cell.lblServiceQty.text = "0"
        }
        cell.lblTitle.text = obj?.service_name
        cell.lblBusiName.text = "By: \(obj?.busi_title ?? "")"
        
        if let fieldName = obj?.service_field?.first?.specificFieldName {
            cell.lblDesciption.text = fieldName
        } else {
            cell.lblDesciption.text = ""
        }
        
        var address = ""
        if let locName = obj?.service_location_info?.location_name, !locName.isEmpty {
            address = obj?.service_location_info?.location_address ?? ""
        } else if let locName = obj?.service_location_info?.location_name_sec, !locName.isEmpty {
            address = obj?.service_location_info?.location_address_sec ?? ""
        }
        
        cell.lblAddress.text = address

        if let servicePrice = obj?.service_price?.service_price, let price = Float(servicePrice) {
            cell.lblPrice.text = price.formattedPrice
        } else {
            cell.lblPrice.text = 0.0.formattedPrice
        }
        
        let totalAmountValue = ""
        if let charitablePercent = obj?.service_event?.causeCharitablePercentage {
            if(obj?.donationAmt == nil && obj?.donationPer == nil){
                let serviceAmount = Float(obj?.service_price?.service_price ?? "0.00") ?? 0.00
                let donationPer = Float(charitablePercent) ?? 0.00
                let donationAmt = (serviceAmount * donationPer) / 100

                obj?.donationAmt = donationAmt
                obj?.donationPer = donationPer
            }
            
            let donationPercent: Float = obj?.donationPer ?? 0
            let percentageVal: String = (donationPercent > 0 ? "\(donationPercent.withoutTrailingZeros)%" : "")
            cell.lblCharitable.text = "Donation Amount " + percentageVal
            cell.lblCharitableAmt.text = obj?.donationAmt?.formattedPrice ?? 0.0.formattedPrice
            
            cell.stackViewCharitable.isHidden = false
            cell.lblCharitable.isHidden = false
        } else {
            cell.lblCharitable.isHidden = true
            cell.stackViewCharitable.isHidden = true
        }
        
        if totalAmountValue.length > 0 {
            cell.lblPrice.text = "$ \(totalAmountValue)"
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 330
    }
}

extension CartItemsViewController: PackageDetailTableViewCellDelegate {
    func removeCell(cellIndex: IndexPath) {
        if let service = ServiceCart.shared.cartServices?[cellIndex.row] {
            ServiceCart.shared.removeServiceFromCart(service)
        }
    }
    func serviceIncrease(cellIndex: IndexPath) {
        
        let cell = tableView.cellForRow(at: cellIndex) as!PackageDetailTableViewCell
        var qty = Int(cell.lblServiceQty.text ?? "0") ?? 0
        qty = qty + 1
        cell.lblServiceQty.text = String(qty)
        
        if let service = ServiceCart.shared.cartServices?[cellIndex.row] {
            ServiceCart.shared.againAddServiceToCart(service)
        }


         
    }
    func serviceDecrease(cellIndex: IndexPath){
        let cell = tableView.cellForRow(at: cellIndex) as!PackageDetailTableViewCell
        var qty = Int(cell.lblServiceQty.text ?? "0") ?? 0
        qty = qty - 1
        
        if qty >= 0 {
            cell.lblServiceQty.text = String(qty)
        }else {
            cell.lblServiceQty.text = "0"
        }
      
        if let service = ServiceCart.shared.cartServices?[cellIndex.row] {
            ServiceCart.shared.removeCartService(service)
        }
        
    }
}

extension BinaryFloatingPoint {
    var withoutTrailingZeros: String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .decimal
        currencyFormatter.minimumFractionDigits = 0
        currencyFormatter.roundingMode = .down
        return currencyFormatter.string(for: self) ?? ""
    }
}

//
//  CartReviewViewController.swift
//  MomCred
//
//  Created by MD on 30/11/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit

class CartReviewViewController: LMBaseViewController {

    //MARK:- IBOutlet(s)
    @IBOutlet private weak var tblRedeem: BIDetailTableView! {
        didSet {
            tblRedeem.backgroundColor = .clear
            tblRedeem.estimatedRowHeight = 80
            tblRedeem.rowHeight = UITableView.automaticDimension
            tblRedeem.allowsSelection = false
            tblRedeem.dataSource = self
            tblRedeem.sizeDelegate = self
        }
    }
    @IBOutlet private weak var tblRedeemHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblPackage: UILabel!
    @IBOutlet weak var lblPackageAmount: UILabel!
    @IBOutlet weak var lblCreditRedeem: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblProcessFees: UILabel!
    @IBOutlet weak var lblRemainingCredit: UILabel!

    @IBOutlet weak var txtFeildCardNum: FormTextField!
    @IBOutlet weak var txtFeildCardExp: FormTextField!
    @IBOutlet weak var txtFeildCardCVV: FormTextField!
    
    @IBOutlet weak var viewCardDetails: UIView!
    @IBOutlet weak var viewRemainingCredit: UIView!
    @IBOutlet weak var viewTransactionCharges: UIView!

    fileprivate let presenter = CartReviewPresenter()
    fileprivate var packagePendingBuildViewController: PackagePendingBuildViewController?
    
    //MARK:- Var(s)
    var packageAmount: Double = 0
    private var redeemAmount: Double = 0 {
        didSet {
            totalAmount = packageAmount + (cartResponse?.transactionCharge ?? 0) - redeemAmount
            /*
            let stripeCharges = PMUserDefault.getStripeCharges() ?? ""
            let totalCharges = packageAmount - redeemAmount
            if let processingPercent = NumberFormatter().number(from: stripeCharges)?.doubleValue {
                let processingFee = (processingPercent * totalCharges) / 100
                totalAmount = totalCharges + processingFee
                lblProcessFees.text = processingFee.formattedPrice
            } else {
                totalAmount = totalCharges
                lblProcessFees.text = (0.0).formattedPrice
            }
             */
        }
    }
    private var totalAmount: Double = 0 {
        didSet {
            
            if totalAmount <= 0.0 {
                lblTotalAmount.text = "$0.0"
                self.viewCardDetails.isHidden = true
                self.viewRemainingCredit.isHidden = false
                
                let absoluteTotalAmount = abs(totalAmount)

                let remainingCredit = absoluteTotalAmount + (cartResponse?.transactionCharge ?? 0)
                self.lblRemainingCredit.text = remainingCredit.formattedPrice
                self.viewTransactionCharges.isHidden = true
            } else {
                self.viewTransactionCharges.isHidden = false
                self.viewCardDetails.isHidden = false
                self.viewRemainingCredit.isHidden = true
                lblTotalAmount.text = totalAmount.formattedPrice
            }
        }
    }
    var isSingleServicePurchase: Bool = false
    var packageInfo: String?
    
    var serviceList: [ServiceAddRequest]?
    var cartResponse: ServiceCartResponse?
    
    private var redeemedOptions: [ServiceCartRedeemItem] = [] {
        didSet {
            var redeemValue: Double = 0
            for item in redeemedOptions {
                redeemValue += (item.credit ?? 0)
            }
            redeemAmount = redeemValue
            lblCreditRedeem.text = redeemValue.formattedPrice
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)

        if isSingleServicePurchase {
            lblPackage.text = LocalizationKeys.text_service_amount.getLocalized()
        }
        
        lblPackageAmount.text = packageAmount.formattedPrice
        lblCreditRedeem.text = 0.0.formattedPrice
        lblProcessFees.text = (cartResponse?.transactionCharge ?? 0).formattedPrice
        
        let expiryDatePicker = MonthYearPickerView()
        txtFeildCardExp.inputView = expiryDatePicker
        expiryDatePicker.onDateSelected = { [weak self] (month: Int, year: Int) in
            let string = String(format: "%02d/%d", month, year)
            NSLog(string) // should show something like 05/2015
            
            self?.txtFeildCardExp.text = string
        }
        
        // Card number
        txtFeildCardNum.formatter = CardNumberFormatter()
        txtFeildCardNum.placeholder = "Card Number"
        txtFeildCardNum.inputType = .integer
        var validation = Validation()
        validation.maximumLength = "1234 5678 1234 5678".count
        validation.minimumLength = "1234 5678 1234 5678".count
        let characterSet = NSMutableCharacterSet.decimalDigit()
        characterSet.addCharacters(in: " ")
        validation.characterSet = characterSet as CharacterSet
        let inputValidator = InputValidator(validation: validation)
        txtFeildCardNum.inputValidator = inputValidator
        
        // Card exp.
        txtFeildCardExp.inputType = .integer
        txtFeildCardExp.formatter = CardExpirationDateFormatter()
        txtFeildCardExp.placeholder = "Expiration Date (MM/YY)"
        
        var validationExp = Validation()
        validationExp.minimumLength = 1
        let characterSetExp = NSMutableCharacterSet.decimalDigit()
        validationExp.characterSet = characterSetExp as CharacterSet
        let inputValidatorExp = CardExpirationDateInputValidator(validation: validationExp)
        txtFeildCardExp.inputValidator = inputValidatorExp
        
        //CVV
        txtFeildCardCVV.inputType = .integer
        txtFeildCardCVV.placeholder = "CVC"
        
        var validationCVV = Validation()
        validationCVV.maximumLength = "CVC".count
        validationCVV.minimumLength = "CVC".count
        validationCVV.characterSet = NSCharacterSet.decimalDigits
        let inputValidatorCVV = InputValidator(validation: validationCVV)
        txtFeildCardCVV.inputValidator = inputValidatorCVV
        
        totalAmount = packageAmount + (cartResponse?.transactionCharge ?? 0)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        packagePendingBuildViewController = nil
    }
}

extension FloatingPoint {
    var formattedPrice: String {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .currency
        return formatter.string(for: self) ?? "$0.00"
    }
}

extension UITextField {
    var isEmpty: Bool {
        return text?.isEmpty ?? true
    }
}

extension CartReviewViewController {
    
    func isExpDateValid() {
//        let date = Date()
//        let calendar = NSCalendar.current
//        let components = calendar.dateComponents([.Day , .Month , .Year], from: date)
//
//        let year =  components.year
//        let month = components.month
    }
    
    fileprivate func openNextScr(purchaseId: String?) {
        if let purchaseId = purchaseId {
            let detailView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PurchaseStoryboard, viewControllerName: PurchaseDetailView.nameOfClass) as PurchaseDetailView
            detailView.serviceName = ""
            detailView.serviceType = ""
            detailView.purchaseId = purchaseId
            detailView.isFromPaymentPage = true
            navigationController?.pushViewController(detailView, animated: true)
        } else if packagePendingBuildViewController == nil {
            packagePendingBuildViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackagePendingBuildViewController.nameOfClass) as PackagePendingBuildViewController
            
            packagePendingBuildViewController?.serviceList = serviceList
            packagePendingBuildViewController?.isSingleServicePurchase = isSingleServicePurchase
            navigationController?.pushViewController(packagePendingBuildViewController!, animated: true)
        }
    }
}

extension CartReviewViewController {
    
    /**
     *  Buy Now action button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func actionConfirmPay(_ sender: UIButton) {
        
        if totalAmount <= 0.0 {

        } else {
           
            if txtFeildCardNum.isEmpty {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.card_no_missing.getLocalized(), buttonTitle: nil, controller: nil)
                return
            }
            
            if txtFeildCardExp.isEmpty {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.card_expiry_missing.getLocalized(), buttonTitle: nil, controller: nil)
                return
            }
            
            if txtFeildCardCVV.isEmpty {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.card_cvv_missing.getLocalized(), buttonTitle: nil, controller: nil)
                return
            }
            
        }        
        
        let expirationDates = (txtFeildCardExp.text ?? "").split(separator: "/")
        var cardExpirationMonth = ""
        var cardExpirationYear = ""
        if expirationDates.count > 1 {
            cardExpirationMonth = String(expirationDates[0])
            cardExpirationYear = String(expirationDates[1])
        }
        
        var serviceDataList:[ServiceData] = []
        var isFound = false
        
        if let serviceList = serviceList {
            for serviceInfo in serviceList {
                if let serviceUser = serviceInfo.service_user, !serviceUser.isEmpty, let serviceId = serviceInfo.service_id, !serviceId.isEmpty, let servicePrice = serviceInfo.service_price?.service_price, !servicePrice.isEmpty {
                    
                    let serviceData = ServiceData()
                    serviceData.serviceId = serviceId
                    serviceData.service_user = serviceUser
                    isFound = true
                    
                    if let charitablePercent = serviceInfo.service_event?.causeCharitablePercentage {
                        if serviceInfo.donationAmt == nil && serviceInfo.donationPer == nil {
                            let serviceAmount = Float(servicePrice) ?? 0.00
                            let donationPer = Float(charitablePercent) ?? 0.00
                            let donationAmt = (serviceAmount * donationPer) / 100
                            
                            serviceInfo.donationAmt = donationAmt
                            serviceInfo.donationPer = donationPer
                            
                            serviceData.donationAmt = String(format: "%.2f", donationAmt)
                            serviceData.donationPer = charitablePercent
                            
                            serviceData.amount = "\(serviceAmount + donationAmt)"
                        } else {
                            let serviceAmount = Float(servicePrice) ?? 0.00
                            
                            let donationAmt = serviceInfo.donationAmt
                            let donationPer = serviceInfo.donationPer
                            
                            serviceData.amount = "\(serviceAmount + (donationAmt ?? 0.00))"
                            
                            serviceData.donationAmt = String(format: "%.2f", donationAmt ?? "0.00")
                            serviceData.donationPer = "\(donationPer ?? 0.00)"
                        }
                    } else {
                        serviceData.amount = serviceInfo.service_price?.service_price ?? "0.00"
                    }

                    serviceDataList.append(serviceData)
                }
            }
        }
        
        if isFound {
            let redeemList = redeemedOptions.compactMap { item -> RedeemData? in
                if let credit = item.credit, let purchaseId = item.purchaseId {
                    return RedeemData(amount: credit, purchaseId: purchaseId)
                }
                return nil
            }
            let paymentInfo = PaymentInfo(cardNo: txtFeildCardNum.text?.replacingOccurrences(of: " ", with: "") ?? "", expiry: "\(cardExpirationYear)-\(cardExpirationMonth)", cvv: txtFeildCardCVV.text ?? "", amount: "\(totalAmount)", type: "2", monthCount: nil, serviceList: serviceDataList, package: packageInfo ?? "", planId: nil, redeemList: redeemList)
            let encodeString = paymentInfo.toJSONString()?.base64Encoded()
            
            print(encodeString ?? "")
            
            Spinner.show()
            presenter.submitPaymentData(paymentInfo: paymentInfo, callback: { [weak self] (status, response) in
                if status {
                    ServiceCart.shared.clearCartItems()
                    self?.openNextScr(purchaseId: response?.data?.purchase_id?.stringValue)
                }
            })
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_something_wrong.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension CartReviewViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ServiceCart.shared.serviceCartResponse?.redeemItems?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartReviewRedeemTableViewCell") as! CartReviewRedeemTableViewCell
        let redeemItem = ServiceCart.shared.serviceCartResponse?.redeemItems?[indexPath.row]
        cell.redeemItem = redeemItem
        cell.delegate = self
        if let redeemItem = redeemItem {
            let index = redeemedOptions.firstIndex { $0.purchaseId == redeemItem.purchaseId }
            cell.isRedeemed = index != nil
        }
        return cell
    }
}

extension CartReviewViewController: CartReviewRedeemTableViewCellDelegate {
    func didSelectRedeemTableViewCell(_ cell: CartReviewRedeemTableViewCell) {
        
        if let redeemItem = cell.redeemItem {
            let index = redeemedOptions.firstIndex { $0.purchaseId == redeemItem.purchaseId }
            if let index = index {
                redeemedOptions.remove(at: index)
                cell.isRedeemed = false
            } else {
                redeemedOptions.append(redeemItem)
                cell.isRedeemed = true

                /*if let creditVal: Double = redeemItem.credit, totalAmount - creditVal > 0 {
                    redeemedOptions.append(redeemItem)
                    cell.isRedeemed = true
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: "Credit amount should not exceed the total amount", buttonTitle: nil, controller: nil)
                }*/
            }
        }
    }
}

extension CartReviewViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        tblRedeemHeightConstraint.constant = size.height
    }
}

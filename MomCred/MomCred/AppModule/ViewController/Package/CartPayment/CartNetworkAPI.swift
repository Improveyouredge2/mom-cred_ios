//
//  CartNetworkAPI.swift
//  MomCred
//
//  Created by MD on 12/12/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

class ServiceCartItem: Mappable {
    var cartId: String?
    var cartUser: String?
    var cartService: String?
    var cartData: String?
    var cartType: String?
    var cartTime: String?
    var serviceUserId: String?
    var quantity: String?

    init() { }
    
    required init?(map: Map) { }

    func mapping(map: Map) {
        cartId          <- map["cart_id"]
        cartUser        <- map ["cart_user"]
        cartService     <- map["cart_service"]
        cartData        <- map["cart_data"]
        cartType        <- map["cart_type"]
        cartTime        <- map["cart_time"]
        serviceUserId   <- map["service_user_id"]
        quantity        <- map["quantity"]

        
    }
}

class ServiceCartRedeemItem: Mappable {
    var fullName: String?
    var credit: Double?
    var serviceName: String?
    var purchaseId: String?

    init() { }
    
    required init?(map: Map) { }

    func mapping(map: Map) {
        fullName <- map["full_name"]
        credit <- map["credit"]
        serviceName <- map["service_name"]
        purchaseId <- map["purchase_id"]
    }
}

class ServiceCartResponse: Mappable {
    var status: Int?
    var error_code: Int?
    var error_line: Int?
    var message: String?
    var transactionCharge: Double?
    var cartItems: [ServiceCartItem]?
    var redeemItems: [ServiceCartRedeemItem]?

    init() { }
    
    required init?(map: Map) { }

    func mapping(map: Map) {
        status      <- map["status"]
        error_code  <- map ["error_code"]
        error_line  <- map["error_line"]
        message     <- map["message"]
        transactionCharge <- map["data.transaction_charge"]
        cartItems   <- map["data.cart_data"]
        redeemItems <- map["data.redeem_data"]
    }
}

class ServiceCartUpdateResponse: Mappable {
    var status: Int?
    var error_code: Int?
    var error_line: Int?
    var message: String?
    var cartItems: [ServiceCartItem]?

    init() { }
    
    required init?(map: Map) { }

    func mapping(map: Map) {
        status      <- map["status"]
        error_code  <- map ["error_code"]
        error_line  <- map["error_line"]
        message     <- map["message"]
        cartItems   <- map["data"]
    }
}

class CartNetworkAPI {
    class func addServiceToCart(serviceId: String, businessId: String, userId: String, completion: @escaping (_ response: ServiceCartUpdateResponse?, _ error: Error?) -> Void) {
        let requestBody = "{\"service_id\": \(serviceId), \"service_userid\": \(userId), \"service_businessid\": \(businessId) }"
        let reqPost: URLRequest? = APIManager().sendPostRequest(urlString: APIKeys.API_ADD_SERVICE_TO_CART, header: APIHeaders().getDefaultHeaders(), strJSON: requestBody)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            if status, let responseDict = response {
                let res: ServiceCartUpdateResponse? = ServiceCartUpdateResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                completion(res, errorWithMessage(res?.message))
            } else { // Failed
                completion(nil, errorWithMessage(error))
            }
        }
    }
    
    class func removeCartService(cartId: String, completion: @escaping (_ response: ServiceCartUpdateResponse?, _ error: Error?) -> Void) {
        let requestBody = "{\"cart_service\": \(cartId)}"
        let reqPost: URLRequest? = APIManager().sendPostRequest(urlString: APIKeys.API_REMOVE_CART_SERVICES, header: APIHeaders().getDefaultHeaders(), strJSON: requestBody)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            if status, let responseDict = response {
                let res: ServiceCartUpdateResponse? = ServiceCartUpdateResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                completion(res, errorWithMessage(res?.message))
            } else { // Failed
                completion(nil, errorWithMessage(error))
            }
        }
    }
    
    
    class func deleteServiceFromCart(cartId: String, completion: @escaping (_ response: ServiceCartUpdateResponse?, _ error: Error?) -> Void) {
        let requestBody = "{\"cart_id\": \(cartId)}"
        let reqPost: URLRequest? = APIManager().sendPostRequest(urlString: APIKeys.API_DELETE_SERVICE_FROM_CART, header: APIHeaders().getDefaultHeaders(), strJSON: requestBody)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            if status, let responseDict = response {
                let res: ServiceCartUpdateResponse? = ServiceCartUpdateResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                completion(res, errorWithMessage(res?.message))
            } else { // Failed
                completion(nil, errorWithMessage(error))
            }
        }
    }
    /*
    class func getServicesInCart(completion: @escaping (_ response: ServiceCartResponse?, _ error: Error?) -> Void) {
        let reqPost: URLRequest? = APIManager().sendGetRequest(urlString: APIKeys.API_GET_SERVICE_FROM_CART, header: APIHeaders().getDefaultHeaders())
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            if status, let responseDict = response {
                let res: ServiceCartResponse? = ServiceCartResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                completion(res, errorWithMessage(res?.message))
            } else { // Failed
                completion(nil, errorWithMessage(error))
            }
        }
    }
     */
    class func getServicesInCart(completion: @escaping (_ response: ServiceCartResponse?, _ error: Error?) -> Void) {
        let reqPost: URLRequest? = APIManager().sendGetRequest(urlString: APIKeys.API_GET_CART_SERVICES, header: APIHeaders().getDefaultHeaders())
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            if status, let responseDict = response {
                let res: ServiceCartResponse? = ServiceCartResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                completion(res, errorWithMessage(res?.message))
            } else { // Failed
                completion(nil, errorWithMessage(error))
            }
        }
    }

    private class func errorWithMessage(_ message: String?) -> NSError {
        return NSError(domain: "MomCredErrorDomain", code: 1234567890, userInfo: [NSLocalizedDescriptionKey: message ?? "Unable to get facility details at this time. Please try again later."])
    }
}

//
//  CartReviewAmountTableViewCell.swift
//  MomCred
//
//  Created by MD on 30/11/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit

protocol CartReviewRedeemTableViewCellDelegate: class {
    func didSelectRedeemTableViewCell(_ cell: CartReviewRedeemTableViewCell)
}

class CartReviewRedeemTableViewCell: UITableViewCell {
    @IBOutlet private weak var lblFullName: UILabel!
    @IBOutlet private weak var lblServiceName: UILabel!
    @IBOutlet private weak var lblCreditValue: UILabel!
    @IBOutlet private weak var btnRedeem: UIButton!

    weak var delegate: CartReviewRedeemTableViewCellDelegate?
    
    var redeemItem: ServiceCartRedeemItem? {
        didSet {
            lblFullName.text = redeemItem?.fullName
            lblServiceName.text = redeemItem?.serviceName
            lblCreditValue.text = redeemItem?.credit?.formattedPrice
        }
    }
    
    var isRedeemed: Bool = false {
        didSet {
            btnRedeem.isSelected = isRedeemed
        }
    }
    
    override func prepareForReuse() {
        btnRedeem.isSelected = false
        lblFullName.text = nil
        lblServiceName.text = nil
        lblCreditValue.text = 0.0.formattedPrice
    }
    
    @IBAction private func btnRedeemAction(sender: UIButton) {
        delegate?.didSelectRedeemTableViewCell(self)
    }
}

class CartReviewAmountTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  NavigationBarView.swift
//  MomCred
//
//  Created by MD on 05/12/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit

class NavigationBarView: UIView {
    @IBOutlet private weak var btnBack: UIButton!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var rightStackView: UIStackView!
    
    private var shouldNavigateToCartOnUpdate: Bool = false
    
    weak var navigationController: UINavigationController?
    
    var title: String = "" {
        didSet {
            lblTitle.text = title
        }
    }
    
    private lazy var btnShoppingCart: UIBadgeButton = {
        let btn = UIBadgeButton()
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        let cartImage = UIImage(named: "shopping_cart")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(cartImage, for: .normal)
        btn.badge = nil
        btn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        btn.tintColor = UIColor.appTheamRedColor()
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        addNotificationObservers()
        btnShoppingCart.addTarget(self, action: #selector(handleCartAction), for: .touchUpInside)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateCartButton()
    }
    
    @objc private func handleCartAction() {
        if let subscriptionInfo = PMUserDefault.getSubsriptionInfo() {
            let subscriptionDetail: SubscriptionDetail? = SubscriptionDetail().getModelObjectFromServerResponse(jsonResponse: subscriptionInfo as AnyObject)
            if subscriptionDetail?.sub_end == nil {
                let popupVC: PMLogoutView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
                popupVC.delegate = self
                popupVC.scrTitle = ""
                popupVC.scrDesc = LocalizationKeys.error_no_subscription.getLocalized()
                popupVC.modalPresentationStyle = .overFullScreen
                navigationController?.present(popupVC, animated: true, completion: nil)
            } else if let isEmpty = ServiceCart.shared.cartServices?.isEmpty, !isEmpty {
                showCartView()
            } else {
                ServiceCart.shared.getExistingCart() { [weak self] (isEmpty) in
                    if isEmpty {
                        let alertController = UIAlertController(title: "Empty Cart!", message: "Nothing to checkout, please add services before proceeding.", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate, let rootController = appDelegate.window?.rootViewController {
                            rootController.present(alertController, animated: true, completion: nil)
                        }
                    } else {
                        self?.shouldNavigateToCartOnUpdate = true
                    }
                }
            }
        }
    }
    
    private func showCartView() {
        let storyboard = UIStoryboard(name: "CartReview", bundle: nil)
        if let cartItems = storyboard.instantiateViewController(withIdentifier: "CartItemsViewController") as? CartItemsViewController {
            navigationController?.pushViewController(cartItems, animated: true)
        }
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(_:)), name: .cartUpdated, object: nil)
    }
    
    @objc private func handleNotification(_ notification: Notification) {
        if notification.name == .cartUpdated {
            updateCartButton()
            
            if shouldNavigateToCartOnUpdate {
                shouldNavigateToCartOnUpdate = false
                showCartView()
            }
        }
    }
    
    private func updateCartButton() {
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        if role_id == AppUser.ServiceProvider.rawValue {
            return
        }
        if PMUserDefault.getAppToken() == nil || PMUserDefault.getAppToken()?.trim().isEmpty == true {
            return
        }
        
        if btnShoppingCart.superview == nil {
            rightStackView.insertArrangedSubview(btnShoppingCart, at: 0)
        }
        btnShoppingCart.isHidden = false
        let cartItemCount = ServiceCart.shared.cartServices?.count ?? 0
        if cartItemCount > 0 {
            btnShoppingCart.badge = "\(cartItemCount)"
        } else {
            btnShoppingCart.badge = nil
        }
    }
    
    private func removeNotificationObservers() {
        NotificationCenter.default.removeObserver(self, name: .cartUpdated, object: nil)
    }
    
    deinit {
        removeNotificationObservers()
    }
}

extension NavigationBarView: PMLogoutViewDelegate {
    func cancelLogoutViewScr() { }
    
    func acceptLogoutViewScr() {
        Helper.sharedInstance.openPremiumMembershipView()
    }
}

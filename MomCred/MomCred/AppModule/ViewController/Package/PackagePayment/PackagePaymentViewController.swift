//
//  PackagePaymentViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * PackagePaymentViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PackagePaymentViewController : LMBaseViewController {

    //MARK:- IBOutlet(s)
    @IBOutlet weak var lblPackage : UILabel!
    @IBOutlet weak var lblPackageAmount : UILabel!
    @IBOutlet weak var lblCreditAmount : UILabel!
    @IBOutlet weak var lblTotalAmount : UILabel!
    @IBOutlet weak var lblProcessFees : UILabel!
    
    @IBOutlet weak var txtFeildCardNum:FormTextField!
    @IBOutlet weak var txtFeildCardExp:FormTextField!
    @IBOutlet weak var txtFeildCardCVV:FormTextField!
    
    fileprivate let presenter = PackagePaymentPresenter()
    fileprivate var packagePendingBuildViewController:PackagePendingBuildViewController?
    
    //MARK:- Var(s)
    var packageAmount: Double = 0
    var creditAmount: Double = 0
    var totalAmount: Double = 0
    var creditPercent: Double = 0
    
    var isSingleServicePurchase = false
    var packageInfo:String?
//    var totalServiceCount:Int = 0
    
    var serviceList:[ServiceAddRequest]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        
        
        if(isSingleServicePurchase){
            //self.lblPackage.text = LocalizationKeys.text_service_amount.getLocalized()
        }
        
        lblPackageAmount.text = packageAmount.formattedPrice
        lblCreditAmount.text = creditAmount.formattedPrice
        
        let processCharges = PMUserDefault.getStripeCharges() ?? ""
        var totalCharges = self.totalAmount
        if let processFeesCharge = NumberFormatter().number(from: processCharges)?.doubleValue {
            let totalProcessCharges = (processFeesCharge * totalAmount) / 100
            totalCharges = totalAmount + totalProcessCharges
            lblProcessFees.text = totalProcessCharges.formattedPrice
        }
        
        lblTotalAmount.text = totalCharges.formattedPrice
        
        let expiryDatePicker = MonthYearPickerView()
        self.txtFeildCardExp.inputView = expiryDatePicker
        expiryDatePicker.onDateSelected = { [weak self] (month: Int, year: Int) in
            let string = String(format: "%02d/%d", month, year)
            NSLog(string) // should show something like 05/2015
            self?.txtFeildCardExp.text = string
        }
        
        // Card number
        txtFeildCardNum.formatter = CardNumberFormatter()
        txtFeildCardNum.placeholder = "Card Number"
        txtFeildCardNum.inputType = .integer
        var validation = Validation()
        validation.maximumLength = "1234 5678 1234 5678".count
        validation.minimumLength = "1234 5678 1234 5678".count
        let characterSet = NSMutableCharacterSet.decimalDigit()
        characterSet.addCharacters(in: " ")
        validation.characterSet = characterSet as CharacterSet
        let inputValidator = InputValidator(validation: validation)
        txtFeildCardNum.inputValidator = inputValidator
        
        // Card exp.
        txtFeildCardExp.inputType = .integer
        txtFeildCardExp.formatter = CardExpirationDateFormatter()
        txtFeildCardExp.placeholder = "Expiration Date (MM/YY)"
        
        var validationExp = Validation()
        validationExp.minimumLength = 1
        let characterSetExp = NSMutableCharacterSet.decimalDigit()
        validationExp.characterSet = characterSetExp as CharacterSet
        let inputValidatorExp = CardExpirationDateInputValidator(validation: validationExp)
        txtFeildCardExp.inputValidator = inputValidatorExp
        
        //CVV
        txtFeildCardCVV.inputType = .integer
        txtFeildCardCVV.placeholder = "CVC"
        
        var validationCVV = Validation()
        validationCVV.maximumLength = "CVC".count
        validationCVV.minimumLength = "CVC".count
        validationCVV.characterSet = NSCharacterSet.decimalDigits
        let inputValidatorCVV = InputValidator(validation: validationCVV)
        txtFeildCardCVV.inputValidator = inputValidatorCVV
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.packagePendingBuildViewController = nil
    }
}

extension PackagePaymentViewController{
    
    func isExpDateValid(){
//        let date = Date()
//        let calendar = NSCalendar.current
//        let components = calendar.dateComponents([.Day , .Month , .Year], from: date)
//
//        let year =  components.year
//        let month = components.month
    }
    
    fileprivate func openNextScr(){

        if(self.packagePendingBuildViewController == nil){
            self.packagePendingBuildViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackagePendingBuildViewController.nameOfClass) as PackagePendingBuildViewController
            
            self.packagePendingBuildViewController?.serviceList = self.serviceList
            self.packagePendingBuildViewController?.isSingleServicePurchase = self.isSingleServicePurchase
            packagePendingBuildViewController?.creditPercent = creditPercent
            packagePendingBuildViewController?.isFromPaymentPage = true
            self.navigationController?.pushViewController(self.packagePendingBuildViewController!, animated: true)
        }
    }
}

extension PackagePaymentViewController{
    
    /**
     *  Buy Now action button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func actionConfirmPay(_ sender: UIButton) {
        
        if((txtFeildCardNum.text?.length)! == 0) {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.card_no_missing.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((txtFeildCardExp.text?.length)! == 0) {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.card_expiry_missing.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((txtFeildCardCVV.text?.length)! == 0){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.card_cvv_missing.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((txtFeildCardNum.text?.length)! > 0 && (txtFeildCardExp.text?.length)! > 0 && (txtFeildCardCVV.text?.length)! > 0){

            let expirationDates = (txtFeildCardExp.text ?? "").split(separator: "/")
            var cardExpirationMonth = ""
            var cardExpirationYear = ""
            if expirationDates.count > 1 {
                cardExpirationMonth = String(expirationDates[0])
                cardExpirationYear = String(expirationDates[1])
            }

            // data?.map{($0.service_id ?? "")}
//            let serviceId = serviceList?.map{($0.service_id ?? "")}
//            let serviceIdText = serviceId?.joined(separator: ",")
            
//            ServiceData
            var serviceDataList:[ServiceData] = []
            var isFound = false
            
            for serviceInfo in (self.serviceList)!{
                
                if(serviceInfo.service_user != nil && (serviceInfo.service_user?.length)! > 0 && serviceInfo.service_id != nil && (serviceInfo.service_id?.length)! > 0 && serviceInfo.service_price != nil && serviceInfo.service_price?.service_price != nil && (serviceInfo.service_price?.service_price?.length)! > 0){
                    let serviceData = ServiceData()
                    serviceData.serviceId = serviceInfo.service_id ?? ""
                    //serviceData.amount = serviceInfo.service_price?.servicePrice ?? ""
                    serviceData.service_user = serviceInfo.service_user ?? ""
                    isFound = true
                    if(serviceInfo.service_event != nil && serviceInfo.service_event?.causeCharitablePercentage != nil){
                        if(serviceInfo.donationAmt == nil && serviceInfo.donationPer == nil){
                            let serviceAmount = Float(serviceInfo.service_price?.service_price ?? "0.00") ?? 0.00
                            let donationPer = Float(serviceInfo.service_event?.causeCharitablePercentage ?? "0.00") ?? 0.00
                            let donationAmt = (serviceAmount * donationPer) / 100
                            
                            serviceInfo.donationAmt = donationAmt
                            serviceInfo.donationPer = donationPer
                            
                            serviceData.donationAmt = String(format: "%.2f", donationAmt)
                            serviceData.donationPer = serviceInfo.service_event?.causeCharitablePercentage ?? ""
                            
                            serviceData.amount = "\(serviceAmount + donationAmt)"
                            
                            
                        } else {
                            
                            let serviceAmount = Float(serviceInfo.service_price?.service_price ?? "0.00") ?? 0.00
                            
                            let donationAmt = serviceInfo.donationAmt
                            let donationPer = serviceInfo.donationPer
                            //                        let donationAmt = (serviceAmount * donationPer) / 100
                            
                            serviceData.amount = "\(serviceAmount + (donationAmt ?? 0.00))"
                            
                            serviceData.donationAmt = String(format: "%.2f", donationAmt ?? "0.00")
                            serviceData.donationPer = "\(donationPer ?? 0.00)"
                        }
                    } else {
                        serviceData.amount = serviceInfo.service_price?.service_price ?? "0.00"
                    }
                    
                    
                    serviceDataList.append(serviceData)
                }
            }
            
            if(isFound){
                let paymentInfo = PaymentInfo(cardNo: txtFeildCardNum.text?.replacingOccurrences(of: " ", with: "") ?? "", expiry: "\(cardExpirationYear)-\(cardExpirationMonth)", cvv: txtFeildCardCVV.text ?? "", amount: "\(totalAmount)", type: "2", monthCount: nil, serviceList: serviceDataList, package: self.packageInfo ?? "", planId: nil, redeemList: nil)
                let encodeString = paymentInfo.toJSONString()?.base64Encoded()
                
                print(encodeString ?? "")
                
                Spinner.show()
                self.presenter.submitPaymentData(paymentInfo: paymentInfo, callback: {
                    (status, response) in
                    
                    if(status){
                        self.openNextScr()
                    }
                })
            } else {
                
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_something_wrong.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

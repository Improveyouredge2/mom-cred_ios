//
//  PackagePaymentPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  PackagePaymentPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PackagePaymentPresenter {
    
    var view:PackagePaymentViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PackagePaymentViewController) {
        self.view = view
    }
}


extension PackagePaymentPresenter{
    
    func submitPaymentData(paymentInfo:PaymentInfo?, callback:@escaping (_ status:Bool, _ response: PaymentResponseInfo?) -> Void){
        
        
        PMMembershipPlanPaymentService.submitPaymentData(paymentInfo: paymentInfo, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    callback(status, response)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, response)
                }
            }
        })
    }
}

//
//  PackagePendingBuildViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * PackageCompleteBuildViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PackagePendingBuildViewController : LMBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var constraintPackageListHeightConstant:NSLayoutConstraint!
    
    var isSingleServicePurchase = false
    var isFromPaymentPage: Bool = false
    var creditPercent: Double = 0
    
    var serviceList:[ServiceAddRequest]?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.tableView.contentSize.height > 0) {
            tableView.scrollToBottom()
            self.constraintPackageListHeightConstant?.constant =
                self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintPackageListHeightConstant.constant = heightOfTableView
        }
    }

    override func actionNavigationBack(_ sender: Any) {
        if isFromPaymentPage {
            navigationController?.popToRootViewController(animated: true)
        } else {
            super.actionNavigationBack(sender)
        }
    }
}

extension PackagePendingBuildViewController{
    
    @IBAction func actionResetStartSelection(_ sender: UIButton) {
        
        //TODO: Move to home screen of Package to restart
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func actionProceedToPaySelection(_ sender: UIButton) {
        
        //TODO: Open when pending
//        let packagePaymentViewController:PackagePaymentViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackagePaymentViewController.nameOfClass) as PackagePaymentViewController
//        self.navigationController?.pushViewController(packagePaymentViewController, animated: true)
        
    }
    
    
}


// MARK:- UITableViewDataSource & UITableViewDelegate

extension PackagePendingBuildViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.serviceList != nil) ? (self.serviceList?.count)! : 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        let cell = tableView.dequeueReusableCell(withIdentifier: PackageDetailTableViewCell.nameOfClass) as! PackageDetailTableViewCell
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        cell.imgProfilePic.image = nil
        cell.lblTitle.text = nil
        cell.lblBusiName.text = nil
        cell.lblDesciption.text = nil
        cell.lblAddress.text = nil
        cell.lblPrice.text = nil
        cell.lblCredit.text = nil
        cell.lblCharitable.isHidden = true
        cell.stackViewCharitable.isHidden = true

        if let obj = self.serviceList?[indexPath.row] {
            if let thumb = obj.mediabusiness?.first?.thumb {
                cell.imgProfilePic.sd_setImage(with: URL(string: thumb))
            }
            
            cell.lblTitle.text = obj.service_name
            cell.lblBusiName.text = "By: \(obj.busi_title ?? "")"

            if let fieldName = obj.service_field?.first?.specificFieldName {
                cell.lblDesciption.text = fieldName
            }
            
            var address = ""
            if let locationName = obj.service_location_info?.location_name, !locationName.trim().isEmpty {
                address = obj.service_location_info?.location_address ?? ""
            } else if let locationName = obj.service_location_info?.location_name_sec, !locationName.trim().isEmpty {
                address = obj.service_location_info?.location_address_sec ?? ""
            }
            cell.lblAddress.text = address

            if let servicePriceStr = obj.service_price?.service_price, let servicePrice = Double(servicePriceStr) {
                cell.lblPrice.text = servicePrice.formattedPrice
                cell.lblCredit.text = (servicePrice * creditPercent/100).formattedPrice
            } else {
                cell.lblPrice.text = 0.0.formattedPrice
                cell.lblCredit.text = 0.0.formattedPrice
            }
            
            if let donation = obj.donationAmt, obj.donationPer != nil {
                cell.lblCharitableAmt.text = donation.formattedPrice
                cell.stackViewCharitable.isHidden = false
                cell.lblCharitable.isHidden = false
            }
        }
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        //        return UITableView.automaticDimension
//        return 180
//    }
}

extension PackagePendingBuildViewController: PackageDetailTableViewCellDelegate{
    func removeCell(cellIndex: IndexPath){
        
        //TODO: Remove from index also
        //        self.tableView.deleteRows(at: [cellIndex], with: .automatic)
    }
    func serviceIncrease(cellIndex: IndexPath) {
        
    }
    func serviceDecrease(cellIndex: IndexPath){
        
        
    }
}

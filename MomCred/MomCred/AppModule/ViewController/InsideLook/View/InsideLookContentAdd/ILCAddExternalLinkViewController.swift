//
//  ILCAddExternalLinkViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

// Page 10


/**
 * ILCAddExternalLinkViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ILCAddExternalLinkViewController : LMBaseViewController{
    
    fileprivate let formNumber = 10
    
    // Event
    @IBOutlet weak var tableViewExternalLink: BIDetailTableView! {
        didSet {
            tableViewExternalLink.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintstableViewExternalLinkHeight: NSLayoutConstraint!
    
    @IBOutlet weak var inputLink: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var viewExternalLinkTableView: UIView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingInsideLookContent: Bool = false

    fileprivate var specificTargetList:[String] = []
    fileprivate var externalLinkDetailList:[ServiceExternalLinkData] = [] {
        didSet {
            viewExternalLinkTableView.isHidden = (externalLinkDetailList.count == 0)
        }
    }
    fileprivate var isUpdate = false
    fileprivate var ilcAddDocumentViewController:ILCAddDocumentViewController?
    fileprivate var presenter = ILCAddExternalLinkPresenter()
    
    var ilcAddRequest:ILCAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)

        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        if !isEditingInsideLookContent {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.ilcAddDocumentViewController = nil
    }
}


extension ILCAddExternalLinkViewController{
    fileprivate func setScreenData(){
        
        self.inputLink.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Link ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )

        if(self.ilcAddRequest?.look_ext_link != nil){
            self.externalLinkDetailList = self.ilcAddRequest?.look_ext_link?.serviceExternalLinkList ?? []
            self.tableViewExternalLink.reloadData()
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        if(UserDefault.getILC() != nil && (UserDefault.getILC()?.length)! > 0  && self.ilcAddRequest?.look_id == nil){
            self.ilcAddRequest?.look_id = UserDefault.getILC() ?? ""
        } else if(self.ilcAddRequest?.look_id == nil){
            self.ilcAddRequest?.look_id = ""
        }
        
        self.ilcAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if (self.externalLinkDetailList.count > 0){
            let serviceListing = ServiceExternalLink()
            serviceListing.serviceExternalLinkList = self.externalLinkDetailList
            
            self.ilcAddRequest?.look_ext_link = serviceListing
        } else {
            self.ilcAddRequest?.look_ext_link = nil
        }
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.ilcAddDocumentViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InsideLookContentStoryboard, viewControllerName: ILCAddDocumentViewController.nameOfClass) as ILCAddDocumentViewController
        
        self.ilcAddDocumentViewController?.ilcAddRequest = self.ilcAddRequest
        self.navigationController?.pushViewController(self.ilcAddDocumentViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension ILCAddExternalLinkViewController{
    @IBAction func methodAddExternalLinkAction(_ sender: UIButton){
        
        if(!((self.inputLink.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: self.inputLink.text() ?? ""))){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_invalid_url.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((self.inputLink.text()?.length)! > 0){
            self.isUpdate = true
            let externalLinkDetailInfo = ServiceExternalLinkData()
            externalLinkDetailInfo.link = self.inputLink.text()
            externalLinkDetailInfo.desc = self.textViewDesc.text
            
            if(self.externalLinkDetailList.count < HelperConstant.minimumBlocks){
                externalLinkDetailList.append(externalLinkDetailInfo)
                
                self.specificTargetList = []
                self.inputLink.input.text = ""
                self.textViewDesc.text = ""
                
                self.tableViewExternalLink.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let ilcDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ILCDetailViewController }
            if let ilcDetailVC = ilcDetailVC as? ILCDetailViewController {
                ilcDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(ilcDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ILCAddExternalLinkViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return externalLinkDetailList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
        
        if(externalLinkDetailList.count > indexPath.row){
            let objInfo = externalLinkDetailList[indexPath.row]
            
            cell.cellIndex = indexPath
            cell.delegate = self
            cell.lblIndex.text = "Link \(indexPath.row + 1)"
            cell.lblLink.text = objInfo.link
            cell.lblDesc.text = objInfo.desc
        }
        return cell
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ILCAddExternalLinkViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        self.isUpdate = true
        self.tableViewExternalLink.reloadData()
    }
}

extension ILCAddExternalLinkViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        self.isUpdate = true
    }
}

extension ILCAddExternalLinkViewController:ExternalLinkTableViewCellDelegate{
    func removeExternalLinkInfo(cellIndex:IndexPath?){
        self.isUpdate = true
        if(self.externalLinkDetailList.count > 0){
            self.externalLinkDetailList.remove(at: cellIndex?.row ?? 0)
            self.tableViewExternalLink.reloadData()
        }
    }
}

extension ILCAddExternalLinkViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        constraintstableViewExternalLinkHeight.constant = size.height
    }
}

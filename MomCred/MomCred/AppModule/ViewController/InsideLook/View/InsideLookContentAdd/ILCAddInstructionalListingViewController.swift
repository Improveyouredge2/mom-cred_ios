//
//  ILCAddInstructionalListingViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

// Page 6

/**
 * ILCAddInstructionalListingViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ILCAddInstructionalListingViewController : LMBaseViewController{
    
    //    var expandTableNumber = [Int] ()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    
    @IBOutlet weak var businessInformationQualification:BusinessInformationQualification!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingInsideLookContent: Bool = false

    fileprivate var businessQualificationInfoList:[BusinessQualificationInfo] = []
    fileprivate let formNumber = 6
    fileprivate var ilcAddSpecificViewController:ILCAddSpecificViewController?
    
    var ilcAddRequest:ILCAddRequest?
    
    fileprivate var presenter = ILCAddInstuctionalListingPresenter()
    var instructionalList:[ServiceListingList]?
    
    var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        businessInformationQualification.namePlaceHolder = LocalizationKeys.txt_link.getLocalized()
        businessInformationQualification.linkPlaceHolder = LocalizationKeys.txt_link.getLocalized()
        businessInformationQualification.textViewDesc.placeholder = LocalizationKeys.txt_description.getLocalized()
        businessInformationQualification.setScreenData()
        businessInformationQualification.updateDropDown(listInfo: [])

        presenter.connectView(view: self)
        presenter.getInstructionalService()
        
        self.setScreenData()
        if !isEditingInsideLookContent {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.ilcAddSpecificViewController = nil
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(businessQualificationInfoList.count == 0){
            self.viewTableView.isHidden = true
        } else {
            self.viewTableView.isHidden = false
        }
        
        if(self.tableView.contentSize.height > 0){
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension ILCAddInstructionalListingViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        if(self.instructionalList != nil && (self.instructionalList?.count)! > 0){
            self.businessInformationQualification.updateDropDown(listInfo: self.instructionalList ?? [])
        }
        
        if(self.ilcAddRequest != nil){
            
            if(self.ilcAddRequest?.look_instruction_content != nil && (self.ilcAddRequest?.look_instruction_content?.count)! > 0){
                self.businessQualificationInfoList = self.ilcAddRequest?.look_instruction_content ?? []
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        ilcAddSpecificViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InsideLookContentStoryboard, viewControllerName: ILCAddSpecificViewController.nameOfClass) as ILCAddSpecificViewController
        
        ilcAddSpecificViewController?.ilcAddRequest = self.ilcAddRequest
        ilcAddSpecificViewController?.isEditingInsideLookContent = isEditingInsideLookContent
        self.navigationController?.pushViewController(ilcAddSpecificViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
//        if(self.businessQualificationInfoList != nil && self.businessQualificationInfoList.count == 0){
//            message = LocalizationKeys.error_add_personnel_listing.getLocalized()
//            self.showBannerAlertWith(message: message, alert: .error)
//            return false
//        }
        if(UserDefault.getILC() != nil && (UserDefault.getILC()?.length)! > 0  && self.ilcAddRequest?.look_id == nil){
            self.ilcAddRequest?.look_id = UserDefault.getILC() ?? ""
        } else if(self.ilcAddRequest?.look_id == nil){
            self.ilcAddRequest?.look_id = ""
        }
        
        self.ilcAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        self.ilcAddRequest?.look_instruction_content = self.businessQualificationInfoList
        return true
    }
}

extension ILCAddInstructionalListingViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let ilcDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ILCDetailViewController }
            if let ilcDetailVC = ilcDetailVC as? ILCDetailViewController {
                ilcDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(ilcDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}


extension ILCAddInstructionalListingViewController{
    
    @IBAction func methodAddAwardAction(_ sender: UIButton){
        let businessQualificationInfo = BusinessQualificationInfo()
//        if((self.businessInformationQualification.inputName.text()?.length)! > 0){
//        if((self.businessInformationQualification.inputName.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: businessInformationQualification.inputName.text() ?? "")){
//            businessQualificationInfo.name = businessInformationQualification.inputName.text() ?? ""
//        } else {
//            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_invalid_url.getLocalized(), buttonTitle: nil, controller: nil)
//
//            return
//        }
        
        if(self.businessInformationQualification.selectedServiceListingList == nil){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_facility_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((self.businessInformationQualification.textViewDesc.text?.length)! == 0){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_website_invalid_url_desc.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if(self.businessQualificationInfoList.count < HelperConstant.minimumBlocks) {
            // Check if object already exists
            let listingIds = self.businessQualificationInfoList.compactMap { $0.id }
            let serviceId = businessInformationQualification.selectedServiceListingList?.id ?? ""
            if listingIds.contains(serviceId) {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                return
            }
            
            businessQualificationInfo.id = self.businessInformationQualification.selectedServiceListingList?.id
            businessQualificationInfo.name = self.businessInformationQualification.selectedServiceListingList?.title
            businessQualificationInfo.desc = businessInformationQualification.textViewDesc.text ?? ""
            
            self.isUpdate = true
            self.businessInformationQualification.dropDownList.text = ""
            self.businessInformationQualification.selectedServiceListingList = nil
            self.businessInformationQualification.inputName.input.text = ""
            
            self.businessInformationQualification.textViewDesc.text = ""
            self.businessQualificationInfoList.append(businessQualificationInfo)
            self.tableView.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ILCAddInstructionalListingViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return businessQualificationInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
        
        let objInfo = businessQualificationInfoList[indexPath.row]
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        cell.lblIndex.text = "Instructional content listing \(indexPath.row + 1)"
        cell.lblLink.text = objInfo.name
        cell.lblDesc.text = objInfo.desc
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ILCAddInstructionalListingViewController:BusinessInformationQualificationAwardCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
        if(self.businessQualificationInfoList.count > 0){
            self.businessQualificationInfoList.remove(at: cellIndex?.row ?? 0)
            self.tableView.reloadData()
            
            if(businessQualificationInfoList.count == 0){
                self.viewTableView.isHidden = true
            }
        }
    }
}

//
//  ILCDetailViewController.swift
//  MomCred
//
//  Created by consagous on 12/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import Lightbox
import AVKit

/**
 * ILCDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ILCDetailViewController: LMBaseViewController {
    
    // Page 1
    // Profile image view
    @IBOutlet weak var imgProfile: ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitleValue: UILabel!
    @IBOutlet weak var lblDescValue: UILabel!
    @IBOutlet weak var lblInsideLookTypeValue: UILabel!
    @IBOutlet weak var lblServiceTypeValue: UILabel!
    @IBOutlet weak var lblLocationAddress: UILabel!
    
    // Page 2
    @IBOutlet weak var tableViewFieldServiceView: BIDetailTableView! {
        didSet {
            tableViewFieldServiceView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewFieldServiceHeight: NSLayoutConstraint!
    
    // Page 3
    @IBOutlet weak var tableViewServiceListingView: BIDetailTableView! {
        didSet {
            tableViewServiceListingView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewServiceListingHeight: NSLayoutConstraint!
    
    // Page 4
    @IBOutlet weak var tableViewFacilityListingView: BIDetailTableView! {
        didSet {
            tableViewFacilityListingView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewFacilityListingHeight: NSLayoutConstraint!
    
    // Page 5
    @IBOutlet weak var tableViewPersonnelListingView: BIDetailTableView! {
        didSet {
            tableViewPersonnelListingView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewPersonnelListingHeight: NSLayoutConstraint!
    
    // Page 6
    @IBOutlet weak var tableViewInstructionalListingView: BIDetailTableView! {
        didSet {
            tableViewInstructionalListingView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewInstructionalListingHeight: NSLayoutConstraint!
    
    // Page 7
    @IBOutlet weak var lblStandardExceptionalValue: UILabel!
    @IBOutlet weak var lblExceptionalValue: UILabel!
    @IBOutlet weak var lblTargetGoalValue: UILabel!
    @IBOutlet weak var lblSpecialListingValue: UILabel!
    @IBOutlet weak var lblAgeRangeValue: UILabel!
    @IBOutlet weak var lblSkillLevelValue: UILabel!
    
    // Page 8
    @IBOutlet weak var tableViewOtherInsideLookView: BIDetailTableView! {
        didSet {
            tableViewOtherInsideLookView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewOtherInsideLookHeight: NSLayoutConstraint!
    
    // Form 9 Additional notes
    @IBOutlet weak var tableViewAddNotesListing: BIDetailTableView! {
        didSet {
            tableViewAddNotesListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewAddNotesHeight: NSLayoutConstraint!
    
    // Form 10 External link
    @IBOutlet weak var tableViewExtenalLinkListing: BIDetailTableView! {
        didSet {
            tableViewExtenalLinkListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewExtenalLinkLisitingHeight: NSLayoutConstraint!
    
    // Additional Document type display
    @IBOutlet weak var tableViewAdditionalDocument: BIDetailTableView! {
        didSet {
            tableViewAdditionalDocument.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewAdditionalDocumentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnOptionMenu: UIButton!
    
    var ilcAddRequest:ILCAddRequest?
    var isHideEditOption = false
    
    private var menuArray: [HSMenu] = []
    private var myFacilitiesAddOverviewViewController: ILCAddOverviewViewController?
    private var defaultDocumentCollectionHeight: CGFloat = 0
    
    private var exceptionalClassification: [ListingDataDetail] = []

    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    var collectionContentHeightObserver: NSKeyValueObservation?
    var shouldRefreshContent: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.isHidden = true
        defaultDocumentCollectionHeight = constraintDocumentCollectionHeight.constant
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.edit.getLocalized())
        menuArray = [menu1]
        
        // getPersonalServiceList
        setupScrInfo()
        
        btnOptionMenu.isHidden = isHideEditOption
        
        getParentListing()
        
        collectionContentHeightObserver = collectionView.observe(\.contentSize, options: [.new]) { [weak self] _, value in
            if let height = value.newValue?.height, height > 0 {
                self?.constraintDocumentCollectionHeight.constant = height
            }
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myFacilitiesAddOverviewViewController = nil
        
        if shouldRefreshContent {
            shouldRefreshContent = false
            if let lookId = ilcAddRequest?.look_id, !lookId.trim().isEmpty {
                ILCService.getInsideLookDetailFor(insideLookId: lookId) { (request, error) in
                    if let request = request {
                        DispatchQueue.main.async { [weak self] in
                            self?.ilcAddRequest = request
                            self?.setupScrInfo()
                        }
                    }
                }
            }
        }
    }
    
    deinit {
        collectionContentHeightObserver?.invalidate()
    }
    
    private func getParentListing() {
        if let exceptionalServices: [DetailData] = ilcAddRequest?.look_specific?.exceptionalServices, !exceptionalServices.isEmpty {
            BIServiceStep1.getParentListing { [weak self] status, parentList, message in
                DispatchQueue.main.async {
                    Spinner.hide()
                }
                if status, let parentList: [ListingDataDetail] = parentList?.parentListingData?.exceptional_classification {
                    self?.exceptionalClassification = parentList
                    for listItem in parentList {
                        if let listingId = listItem.listing_id {
                            let childListingRequest = ChildListingRequest(listing_id: listingId)
                            BIServiceStep1.getChildListing(requestData: childListingRequest) { [weak self] status, childList, message in
                                DispatchQueue.main.async {
                                    Spinner.hide()
                                }
                                if status, let childList = childList {
                                    listItem.subcat = childList.dataList
                                    DispatchQueue.main.async { [weak self] in
                                        self?.reloadExceptionalClassification()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func reloadExceptionalClassification() {
        if let exceptionalServices: [DetailData] = ilcAddRequest?.look_specific?.exceptionalServices, !exceptionalServices.isEmpty {
            let exceptionalServiceIds = exceptionalServices.compactMap { $0.id }
            var parentTitles: [String: [String]] = [:]
            
            for classification in exceptionalClassification {
                if let classificationTitle = classification.listing_title?.trim(), !classificationTitle.isEmpty {
                    if let categoryItems: [ListingDataDetail] = classification.subcat, !categoryItems.isEmpty {
                        let filteredList: [ListingDataDetail] = categoryItems.filter { (categoryItem) -> Bool in
                            if let listingId = categoryItem.listing_id {
                                return exceptionalServiceIds.contains(listingId)
                            }
                            return false
                        }
                        let titles = filteredList.compactMap { $0.listing_title }
                        parentTitles[classificationTitle] = titles
                    }
                }
            }
            
            let magentaColor: UIColor = UIColor(hex: "C8057A")
            let font: UIFont = UIFont(name: FontsConfig.FONT_TYPE_Regular, size: 18) ?? UIFont.systemFont(ofSize: 18)
            
            let exceptionalServiceText = NSMutableAttributedString(string: "")
            let parentTitleKeys = parentTitles.keys.sorted { $0 > $1 }
            for key in parentTitleKeys {
                if let value: [String] = parentTitles[key], !value.isEmpty {
                    let titleText: String
                    if exceptionalServiceText.string.isEmpty {
                        titleText = "\(key)\n"
                    } else {
                        titleText = "\n\n\(key)\n"
                    }
                    exceptionalServiceText.append(NSAttributedString(string: titleText, attributes: [.foregroundColor: magentaColor, .font: font]))
                    exceptionalServiceText.append(NSAttributedString(string: displayString(for: value), attributes: [.foregroundColor: UIColor.white, .font: font]))
                }
            }
            if !exceptionalServiceText.string.isEmpty {
                lblExceptionalValue.attributedText = exceptionalServiceText
            }
        }
    }
}

extension ILCDetailViewController{
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
    
    @IBAction func actionZoomOption(_ sender: AnyObject) {
        zoomableView()
    }
}


extension ILCDetailViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if index == 0 {
            myFacilitiesAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InsideLookContentStoryboard, viewControllerName: ILCAddOverviewViewController.nameOfClass)
            myFacilitiesAddOverviewViewController?.isEditingInsideLookContent = true
            myFacilitiesAddOverviewViewController?.ilcAddRequest = ilcAddRequest
            navigationController?.pushViewController(myFacilitiesAddOverviewViewController!, animated: true)
        }
    }
}

extension ILCDetailViewController {
    private func setupScrInfo() {
        if ilcAddRequest != nil {
            constraintDocumentCollectionHeight.constant = defaultDocumentCollectionHeight
            
            collectionView.reloadData()
            if let mediaBusiness: BusinessAdditionalDocumentInfo = ilcAddRequest?.mediabusiness?.first {
                if let thumb = mediaBusiness.thumb, let thumbURL = URL(string: thumb) {
                    imgProfile.sd_setImage(with: thumbURL)
                }
                // Check video on first pos
                imgPlayBtn.isHidden = mediaBusiness.media_extension != HelperConstant.MediaExtension.Video.MediaExtensionText
            } else {
                imgPlayBtn.isHidden = true
                imgProfile.image = nil
                constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
            }
            
            lblTitleValue.text = ilcAddRequest?.look_name
            
            lblDescValue.text = ilcAddRequest?.look_description
            
            if let titles: [String] = ilcAddRequest?.look_type_title, !titles.isEmpty {
                lblInsideLookTypeValue.text = displayString(for: titles)
            } else {
                lblInsideLookTypeValue.text = ""
            }
            
            if let titles: [String] = ilcAddRequest?.look_service_type_title, !titles.isEmpty {
                lblServiceTypeValue.text = displayString(for: titles)
            } else {
                lblServiceTypeValue.text = ""
            }
            
            lblLocationAddress.text = ""
            if let locationInfo = ilcAddRequest?.look_location_info {
                let attributedLocation: NSMutableAttributedString = .init()
                let boldFont: UIFont = UIFont(name: FontsConfig.FONT_TYPE_SEMI_Bold, size: 18) ?? UIFont.systemFont(ofSize: 18, weight: .semibold)
                let font = UIFont(name: FontsConfig.FONT_TYPE_Regular, size: 18) ?? UIFont.systemFont(ofSize: 18)
                let color: UIColor = UIColor(hex: "C8057A")

                if let name = locationInfo.location_name?.trim(), !name.isEmpty {
                    attributedLocation.append(NSAttributedString(string: name, attributes: [.foregroundColor: color, .font: boldFont]))
                    if let address = locationInfo.location_address?.trim(), !address.isEmpty {
                        attributedLocation.append(NSAttributedString(string: "\n\(address)", attributes: [.foregroundColor: color, .font: font]))
                    }
                } else if let name = locationInfo.location_name_sec?.trim(), !name.isEmpty {
                    attributedLocation.append(NSAttributedString(string: name, attributes: [.foregroundColor: color, .font: boldFont]))
                    if let address = locationInfo.location_address_sec?.trim(), !address.isEmpty {
                        attributedLocation.append(NSAttributedString(string: "\n\(address)", attributes: [.foregroundColor: color, .font: font]))
                    }
                }
                lblLocationAddress.attributedText = attributedLocation
            } else {
                lblLocationAddress.text = ""
            }
            
            if let stdServices = ilcAddRequest?.look_specific?.stdServices {
                let service: String
                if stdServices == "3" {
                    service = LocalizationKeys.std_exp.getLocalized()
                } else if stdServices == "2" {
                    service = LocalizationKeys.exceptional.getLocalized()
                } else {
                    service = LocalizationKeys.standard.getLocalized()
                }
                lblStandardExceptionalValue.text = service
            } else {
                lblStandardExceptionalValue.text = ""
            }
            
            if let exceptionalServices: [DetailData] = ilcAddRequest?.look_specific?.exceptionalServices, !exceptionalServices.isEmpty {
                let titles: [String] = exceptionalServices.compactMap { $0.title }
                lblExceptionalValue.text = displayString(for: titles)
            } else {
                lblExceptionalValue.text = ""
            }
            
            if let goalTitles = ilcAddRequest?.look_specific?.service_target_goal_title, !goalTitles.isEmpty {
                lblTargetGoalValue.text = displayString(for: goalTitles)
            } else {
                lblTargetGoalValue.text = ""
            }
            
            if let optionTitles: [String] = ilcAddRequest?.look_specific?.special_listing_option_title, !optionTitles.isEmpty {
                lblSpecialListingValue.text = displayString(for: optionTitles)
                
            } else {
                lblSpecialListingValue.text = ""
            }
            
            if let minAge = ilcAddRequest?.look_specific?.minAge, let maxAge = ilcAddRequest?.look_specific?.maxAge {
                lblAgeRangeValue.text = "\(minAge) - \(maxAge)"
            } else {
                lblAgeRangeValue.text = ""
            }
            
            if let skillLevel: [DetailData] = ilcAddRequest?.look_specific?.skillLevel, !skillLevel.isEmpty {
                let titles: [String] = skillLevel.compactMap { $0.title }
                lblSkillLevelValue.text = displayString(for: titles)
            } else {
                lblSkillLevelValue.text = ""
            }

            scrollView.isHidden = false
        } else {
            scrollView.isHidden = true
        }
    }
    
    private func displayString(for titles: [String]) -> String {
        let text: String = titles.reduce("") { result, input -> String in
            result.isEmpty ? "• \(input)" : "\(result)\n• \(input)"
        }
        return text
    }
    
    private func zoomableView() {
        if let mediaBusiness: [BusinessAdditionalDocumentInfo] = ilcAddRequest?.mediabusiness, !mediaBusiness.isEmpty {
            let lightBoxImages: [LightboxImage] = mediaBusiness.compactMap { media -> LightboxImage? in
                if media.media_extension == HelperConstant.MediaExtension.Video.MediaExtensionText {
                    if let thumb = media.thumb, let video = media.imageurl, let thumbURL = URL(string: thumb), let videoURL = URL(string: video) {
                        return LightboxImage(imageURL: thumbURL, text: "", videoURL: videoURL)
                    }
                } else if let image = media.imageurl, let imageURL = URL(string: image) {
                    return LightboxImage(imageURL: imageURL)
                }
                return nil
            }
            
            // Create an instance of LightboxController.
            let controller = LightboxController(images: lightBoxImages)
            // Present your controller.
            if #available(iOS 13.0, *) {
                controller.isModalInPresentation  = true
                controller.modalPresentationStyle = .fullScreen
            }
            
            // Set delegates.
            controller.pageDelegate = self
            controller.dismissalDelegate = self
            
            // Use dynamic background.
            controller.dynamicBackground = true
            
            // Present your controller.
            present(controller, animated: true, completion: nil)
            
            LightboxConfig.handleVideo = { from, videoURL in
                // Custom video handling
                let videoController = AVPlayerViewController()
                videoController.player = AVPlayer(url: videoURL)
                
                from.present(videoController, animated: true) {
                    videoController.player?.play()
                }
            }
        }
    }
}


extension ILCDetailViewController: LightboxControllerPageDelegate {
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) { }
}

extension ILCDetailViewController: LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) { }
}

extension ILCDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewFieldServiceView {
            return ilcAddRequest?.look_field?.count ?? 0
        } else if tableView == tableViewServiceListingView {
            return ilcAddRequest?.look_service_listing?.count ?? 0
        } else if tableView == tableViewFacilityListingView {
            return ilcAddRequest?.look_facility?.count ?? 0
        }  else if tableView == tableViewPersonnelListingView {
            return ilcAddRequest?.look_personal_listing?.count ?? 0
        }  else if tableView == tableViewInstructionalListingView {
            return ilcAddRequest?.look_instruction_content?.count ?? 0
        } else if tableView == tableViewOtherInsideLookView {
            return ilcAddRequest?.look_other?.count ?? 0
        } else if tableView == tableViewAddNotesListing {
            return ilcAddRequest?.look_notes?.additionalNotes?.count ?? 0
        } else if tableView == tableViewExtenalLinkListing {
            return ilcAddRequest?.look_ext_link?.serviceExternalLinkList?.count ?? 0
        } else if tableView == tableViewAdditionalDocument {
            return ilcAddRequest?.doc?.count ?? 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewFieldServiceView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BIFieldServiceCell.nameOfClass) as! BIFieldServiceCell
            
            let objInfo = ilcAddRequest?.look_field?[indexPath.row]
            cell.lblFieldIndex.text = "Field \(indexPath.row + 1)"
            cell.lblCatName.text = objInfo?.catFieldName
            
            if let specificFields = objInfo?.add_new_specific_field, !specificFields.isEmpty {
                cell.lblSubCatName.text = displayString(for: [specificFields])
            } else {
                cell.lblSubCatName.text = objInfo?.specificFieldName
            }
            
            if let classifications = objInfo?.classification, !classifications.isEmpty {
                cell.lblClassification.text = displayString(for: classifications)
            } else {
                cell.lblClassification.text = ""
            }
            
            if let techniques = objInfo?.technique, !techniques.isEmpty {
                cell.lblTechnique.text = displayString(for: techniques)
            } else {
                cell.lblTechnique.text = ""
            }

            cell.backgroundColor = UIColor.clear
            return cell
        } else if tableView == tableViewServiceListingView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = ilcAddRequest?.look_service_listing?[indexPath.row]
            cell.lblIndex.text = "Service Listing \(indexPath.row + 1)"
            cell.lblLink.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewFacilityListingView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = ilcAddRequest?.look_facility?[indexPath.row]
            cell.lblIndex.text = "Facility Listing \(indexPath.row + 1)"
            cell.lblLink.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewPersonnelListingView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = ilcAddRequest?.look_personal_listing?[indexPath.row]
            cell.lblIndex.text = "Personnel listing \(indexPath.row + 1)"
            cell.lblLink.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewInstructionalListingView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = ilcAddRequest?.look_instruction_content?[indexPath.row]
            
            cell.lblIndex.text = "Instructional content listing \(indexPath.row + 1)"
            cell.lblLink.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewOtherInsideLookView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = ilcAddRequest?.look_other?[indexPath.row]
            cell.lblIndex.text = "Inside-look content \(indexPath.row + 1)"
            cell.lblLink
                .text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewAddNotesListing {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: AdditionalTitleDescView.nameOfClass) as! AdditionalTitleDescView
            
            let objInfo = ilcAddRequest?.look_notes?.additionalNotes?[indexPath.row]
            cell.lblIndex.text = "Additional Note \(indexPath.row + 1)"
            cell.lblTitleDesc.text = objInfo
            
            return cell
            
        } else if tableView == tableViewExtenalLinkListing {
            let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
            
            let objInfo = ilcAddRequest?.look_ext_link?.serviceExternalLinkList?[indexPath.row]
            cell.lblIndex.text = "External link \(indexPath.row + 1)"
            cell.lblLink.text = objInfo?.link
            cell.lblDesc.text = objInfo?.desc
            cell.backgroundColor = UIColor.clear
            
            return cell
            
        } else if tableView == tableViewAdditionalDocument {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationDocumentCell.nameOfClass) as! BusinessInformationAddAdditionalInformationDocumentCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.documentSelectionDelegate = self
            cell.cellIndex = indexPath
            
            if let data = ilcAddRequest?.doc?[indexPath.row] {
                cell.lblIndex.text = "Informational document \(indexPath.row + 1)"
                cell.lblName.text = data.media_title
                cell.lblDesc.text = data.media_desc
                if let imageURLs = data.imageurls, !imageURLs.isEmpty {
                    cell.arrDocumentImagesUrl = imageURLs.compactMap { $0.thumb }
                }
                cell.refreshScreenInfo()
            }

            return cell
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewServiceListingView {
            if let serviceId: String = ilcAddRequest?.look_service_listing?[indexPath.row].id {
                showServiceDetail(for: serviceId)
            }
        } else if tableView == tableViewFacilityListingView {
            if let facilityId = ilcAddRequest?.look_facility?[indexPath.row].id {
                showFacilityDetail(for: facilityId)
            }
        } else if tableView == tableViewPersonnelListingView {
            if let personnelId = ilcAddRequest?.look_personal_listing?[indexPath.row].id {
                showPersonalDetail(for: personnelId)
            }
        } else if tableView == tableViewInstructionalListingView {
            if let instructionalId = ilcAddRequest?.look_instruction_content?[indexPath.row].id {
                showInstructionalDetail(for: instructionalId)
            }
        } else if tableView == tableViewOtherInsideLookView {
            if let insideLookId = ilcAddRequest?.look_other?[indexPath.row].id {
                showInsideLookDetail(for: insideLookId)
            }
        } else if tableView == tableViewExtenalLinkListing {
            if let externalLink = ilcAddRequest?.look_ext_link?.serviceExternalLinkList?[indexPath.row].link?.trim(), !externalLink.isEmpty {
                openLink(for: externalLink)
            }
        } else if tableView == tableViewFieldServiceView {
            // DO NOTHING
        } else if tableView == tableViewAddNotesListing {
            // DO NOTHING
        } else if tableView == tableViewAdditionalDocument {
            // DO NOTHING
        }
    }
}

extension ILCDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ilcAddRequest?.mediabusiness?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Default product cell
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath) as! SerivceImageCell
        cell.delegate =  self
        cell.cellIndex = indexPath.row
        
        let data = ilcAddRequest?.mediabusiness?[indexPath.row]
        
        if let thumb = data?.thumb {
            cell.imgService.sd_setImage(with: URL(string: thumb))
        }

        cell.imgPlay.isHidden = data?.media_extension != HelperConstant.MediaExtension.Video.MediaExtensionText

        return cell
    }
}

extension ILCDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:100, height: 80)
    }
}

extension ILCDetailViewController: ProductCellDelegate {
    func methodShowProductDetails(cellIndex: Int) {
        if let mediaBusiness = ilcAddRequest?.mediabusiness, !mediaBusiness.isEmpty {
            imgPlayBtn.isHidden = mediaBusiness[cellIndex].media_extension != HelperConstant.MediaExtension.Video.MediaExtensionText

            if let thumb = mediaBusiness[cellIndex].thumb {
                imgProfile.sd_setImage(with: URL(string: thumb))
            }
        }
    }
}

extension ILCDetailViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        if tableView == tableViewFieldServiceView {
            constraintsTableViewFieldServiceHeight.constant = size.height
        } else if tableView == tableViewServiceListingView {
            constraintsTableViewServiceListingHeight.constant = size.height
        } else if tableView == tableViewFacilityListingView {
            constraintsTableViewFacilityListingHeight.constant = size.height
        } else if tableView == tableViewPersonnelListingView {
            constraintsTableViewPersonnelListingHeight.constant = size.height
        } else if tableView == tableViewInstructionalListingView {
            constraintsTableViewInstructionalListingHeight.constant = size.height
        } else if tableView == tableViewOtherInsideLookView {
            constraintsTableViewOtherInsideLookHeight.constant = size.height
        } else if tableView == tableViewAddNotesListing {
            constraintTableViewAddNotesHeight.constant = size.height
        } else if tableView == tableViewExtenalLinkListing {
            constraintTableViewExtenalLinkLisitingHeight.constant = size.height
        } else if tableView == tableViewAdditionalDocument {
            constraintTableViewAdditionalDocumentHeight.constant = size.height
        }
    }
}

extension ILCDetailViewController: DocumentSelectionDelegate {
    func didSelectDocument(at indexPath: IndexPath, inCellAt cellIndexPath: IndexPath) {
        if let data = ilcAddRequest?.doc?[cellIndexPath.row], let imageURLs = data.imageurls, imageURLs.count > indexPath.item, let imageURLString = imageURLs[indexPath.item].imageurls, !imageURLString.isEmpty {
            openLink(for: imageURLString)
        }
    }
}

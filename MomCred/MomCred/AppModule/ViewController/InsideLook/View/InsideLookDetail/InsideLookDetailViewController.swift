//
//  InsideLookDetailViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * InsideLookDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class InsideLookDetailViewController : LMBaseViewController {
    
    @IBOutlet weak var lblTitleValue : UILabel!
    @IBOutlet weak var lblClassificationValue : UILabel!
    @IBOutlet weak var lblTypeValue : UILabel!
    
    @IBOutlet weak var lblInstrutionalUseValue : UILabel!
    @IBOutlet weak var lblInstrutionalServiceTypeValue : UILabel!
    @IBOutlet weak var lblInstrutionalOfficeServiceTypeValue : UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

//
//  ILCListViewController.swift
//  MomCred
//
//  Created by consagous on 11/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


class ILCListViewController: LMBaseViewController{
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tbServiceList : UITableView!
    
    @IBOutlet weak var lblNoRecordFound: UILabel!
    
    fileprivate var ilcAddOverviewViewController:ILCAddOverviewViewController?
    
    fileprivate var insideLookDetailViewController:InsideLookDetailViewController?
    fileprivate var ilcDetailViewController:ILCDetailViewController?
    fileprivate var menuArray: [HSMenu] = []
    
    fileprivate var presenter = ILCListPresenter()
    
//    var myServiceList:[ServiceAddRequest]?
    
    var ilcResponse:[ILCAddRequest]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter.connectView(view: self)
        
        tbServiceList.estimatedRowHeight = 100
        tbServiceList.rowHeight = UITableView.automaticDimension
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.add_new.getLocalized())
        
//        if(UserDefault.getPID() != nil && (UserDefault.getPID()?.length)! > 0){
//            let menu2 = HSMenu(icon: nil, title: LocalizationKeys.complete_pending.getLocalized())
//            menuArray = [menu1,menu2]
//        }  else{
            menuArray = [menu1]
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutSubviews()
        self.ilcAddOverviewViewController = nil
        self.ilcDetailViewController = nil
        self.insideLookDetailViewController = nil
        
        self.presenter.getInsideLookServiceList()
    }
    
}

extension ILCListViewController {
    func refreshScr(){
        if(self.tbServiceList != nil){
            self.tbServiceList.reloadData()
        }
    }
}

extension ILCListViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        self.ilcAddOverviewViewController = nil
        if(index == 0){
            
            UserDefault.removeILC()
            
            self.ilcAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InsideLookContentStoryboard, viewControllerName: ILCAddOverviewViewController.nameOfClass)
            self.navigationController?.pushViewController(self.ilcAddOverviewViewController!, animated: true)
        }
//        else if(index == 1){
//            self.facilitiesDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InsideLookContentStoryboard, viewControllerName: ILCDetailViewController.nameOfClass) as ILCDetailViewController
//            self.navigationController?.pushViewController(self.facilitiesDetailViewController!, animated: true)
//        }
    }
}

extension ILCListViewController{
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension ILCListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return self.arrNoteList.count
        return self.ilcResponse != nil ? (self.ilcResponse?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyServiceCell.nameOfClass, for: indexPath) as! MyServiceCell
        
        if let data = self.ilcResponse?[indexPath.row] {
            cell.lblTitle.text = data.look_name
            cell.lblDesciption.text = data.look_description
            cell.lblDate.text = data.create_dt?.dateFromFormat("yyyy-MM-dd HH:mm:ss")?.stringFromFormat("dd MMM yyyy") //2020-08-20 10:33:01
            
            if let thumb = data.mediabusiness?.first?.thumb, !thumb.isEmpty, let thumbURL = URL(string: thumb) {
                cell.imgProfilePic.sd_setImage(with: thumbURL, placeholderImage: #imageLiteral(resourceName: "profile"))
            } else {
                cell.imgProfilePic.image = #imageLiteral(resourceName: "profile")
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        if(role_id == AppUser.ServiceProvider.rawValue){
            
            self.ilcDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InsideLookContentStoryboard, viewControllerName: ILCDetailViewController.nameOfClass) as ILCDetailViewController
            
            if(self.ilcDetailViewController != nil){
                self.ilcDetailViewController?.ilcAddRequest = self.ilcResponse?[indexPath.row]
                self.navigationController?.pushViewController(self.ilcDetailViewController!, animated: true)
            }
            
        } else {
            self.insideLookDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InsideLookContentStoryboard, viewControllerName: InsideLookDetailViewController.nameOfClass) as InsideLookDetailViewController
            
            if(self.insideLookDetailViewController != nil){
                //            self.personnelDetailViewController?.serviceAddRequest = self.myServiceList?[indexPath.row]
                self.navigationController?.pushViewController(self.insideLookDetailViewController!, animated: true)
            }
        }
    }
}

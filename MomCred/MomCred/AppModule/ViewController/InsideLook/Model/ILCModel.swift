//
//  MyFacilityModel.swift
//  MomCred
//
//  Created by consagous on 10/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

///********************************/
////MARK:- Serialize Request
///*******************************/

/**
 *  PersonnelAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class ILCAddRequest: Mappable {
    
    var look_id:String?
    var create_dt: String?
    // Page 1
    var look_name:String?
    var look_type:[String]?
    var look_type_title:[String]?
    var look_service_type:[String]?
    var look_service_type_title:[String]?
    var look_description:String?
    var look_location_info:PersonnelBusiLocationResponseData?
    var look_location:String?
    
    // Page 2
    var look_field:[BusinessFieldServiceCategoryInfo]?
    
    // Page 3
    var look_service_listing:[BusinessQualificationInfo]?
    
    // Page 4
    var look_facility:[BusinessQualificationInfo]?
    
    // Page 5
    var look_personal_listing:[BusinessQualificationInfo]?
    
    // Page 6
    var look_instruction_content:[BusinessQualificationInfo]?
    
    // Page 7
    var look_specific:ILCSpecificInfo?
    
    // Page 8
    var look_other:[BusinessQualificationInfo]?
    
    // Page 9
    var look_notes:ServiceAddNotes?
    
    // Page 10
    var look_ext_link:ServiceExternalLink?
    
    
    // For business detail display
    var doc:[BusinessAdditionalDocumentInfo]?
    var mediabusiness:[BusinessAdditionalDocumentInfo]?
    var status:String? = "0"
    var number_of_purchase:String?
    var personal_user:String?
    var pageid:NSNumber?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init() {
        //        super.init()
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        //        super.init(map: map)
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->MyFacilityAddRequest?{
        var myFacilityAddRequest:MyFacilityAddRequest?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<MyFacilityAddRequest>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    myFacilityAddRequest = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<MyFacilityAddRequest>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        myFacilityAddRequest = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return myFacilityAddRequest ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        look_id                  <- map["look_id"]
        create_dt                <- map["create_dt"]

        //Page 1
        look_name                   <- map["look_name"]
        look_type                   <- map["look_type"]
        look_type_title             <- map["look_type_title"]
        look_service_type           <- map["look_service_type"]
        look_service_type_title     <- map["look_service_type_title"]
        look_description            <- map["look_description"]
        look_location_info          <- map["look_location_info"]
        look_location               <- map["look_location"]
        
        //Page 2
        look_field                  <- map["look_field"]
        
        // Page 3
        look_service_listing        <- map["look_service_listing"]
        
        // Page 4
        look_facility               <- map["look_facility"]
        
        // Page 5
        look_personal_listing       <- map["look_personal_listing"]
        
        // Page 6
        look_instruction_content    <- map["look_instruction_content"]
        
        // Page 7
        look_specific               <- map["look_specific"]
        
        // Page 8
        look_other                  <- map["look_other"]
        
        // Page 9
        look_notes                  <- map["look_notes"]
        
        // Page 10
        look_ext_link               <- map["look_ext_link"]
        
        
        // Page  only in response, For business detail
        doc                         <- map["externalmedia"]
        
        // For business detail
        mediabusiness               <- map["look_media"]
        
        pageid                      <- map["pageid"]
        
        number_of_purchase          <- map["number_of_purchase"]
        
        // In-reponse
        status                      <- map["status"]
        
        personal_user               <- map["look_user"]
        
    }
}

class ILCClassificationInfo: Mappable{
    var name:String?
    var descList:[String]?
    var desc:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->FacilityClassificationInfo?{
        var facilityClassificationInfo:FacilityClassificationInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<FacilityClassificationInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    facilityClassificationInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<FacilityClassificationInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        facilityClassificationInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return facilityClassificationInfo ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        name        <- map["name"]
        descList    <- map["descList"]
        desc        <- map["desc"]
    }
}



class ILCSpecificInfo: Mappable{
    
    var stdServices:String? = ""
    var exceptionalServices:[DetailData]? = []
    var service_target_goal:[String]?
    var service_target_goal_title:[String]?
    var special_listing_option:[String]?
    var special_listing_option_title:[String]?
    var minAge:String?
    var maxAge:String?
    var skillLevel:[DetailData]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ILCSpecificInfo?{
        var ilcSpecificInfo:ILCSpecificInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ILCSpecificInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    ilcSpecificInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ILCSpecificInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        ilcSpecificInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return ilcSpecificInfo ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        stdServices                     <- map["stdServices"]
        exceptionalServices             <- map["exceptionalServices"]
        service_target_goal             <- map["look_service_target_goal"]
        service_target_goal_title       <- map["look_service_target_goal_title"]
        special_listing_option          <- map["special_listing_option"]
        special_listing_option_title    <- map["special_listing_option_title"]
        minAge                          <- map["minAge"]
        maxAge                          <- map["maxAge"]
        skillLevel                      <- map["skillLevel"]
    }
}

class InsideLookDetailResponse: APIResponse {
    var data: ILCAddRequest?
    
    required init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

///********************************/
////MARK:- Serialize Response
///*******************************/

/**
 *  ServiceResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class ILCResponse : APIResponse {
    
    var data:[ILCAddRequest]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ILCResponse?{
        var ILCResponse:ILCResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ILCResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    ILCResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ILCResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        ILCResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return ILCResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
}


class ILCAddResponse : APIResponse {
    
    var data:ILCAddResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ILCAddResponse?{
        var myFacilityAddResponse:ILCAddResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ILCAddResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    myFacilityAddResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ILCAddResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        myFacilityAddResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return myFacilityAddResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class ILCAddResponseData: Mappable{
        var look_id : NSNumber?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ILCAddResponseData?{
            var myFacilityAddResponseData:ILCAddResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<ILCAddResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        myFacilityAddResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ILCAddResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            myFacilityAddResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return myFacilityAddResponseData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            look_id     <- map["look_id"]
            
        }
    }
}

//
//  MyFacilitiesAddFacilityListingPresenter.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  ILCAddSpecificPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ILCAddSpecificPresenter {
    
    var view:ILCAddSpecificViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ILCAddSpecificViewController) {
        self.view = view
    }
}

extension ILCAddSpecificPresenter{
    
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverChildListingRequest(parentId:String){
        
        let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        BIServiceStep1.getChildListing(requestData: childListingRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    
                    
                    if(response?.dataList != nil && (response?.dataList?.count)! > 0){
                        for parentList in self.view.exceptional_classification!{
                            if((response?.dataList?.count)! > 0 && response?.dataList?[0].listing_parent?.caseInsensitiveCompare(parentList.listing_id ?? "0") == ComparisonResult.orderedSame){
                                parentList.subcat = response?.dataList
                                break
                            }
                        }
                    }
                    
                    // check all child list complete
                    var isFound = true
                    for parentList in self.view.exceptional_classification!{
                        
                        if(parentList.subcat == nil){
                            isFound = false
                            break
                        }
                    }
                    
                    if(isFound){
//                        Helper.sharedInstance.hideProgressHudView()
                        Spinner.hide()
                        self.view.updateExceptionalTableList()
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
//                    Helper.sharedInstance.hideProgressHudView()
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: ILCAddResponse?, _ message: String?) -> Void){
        
        ILCService.updateData(serviceAddRequest: view.ilcAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
//                    Helper.sharedInstance.hideProgressHudView()
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
}

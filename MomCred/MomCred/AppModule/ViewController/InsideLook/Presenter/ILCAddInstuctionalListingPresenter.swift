//
//  MyFacilitiesAddFacilityListingPresenter.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  ILCAddInstuctionalListingPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ILCAddInstuctionalListingPresenter {
    
    var view:ILCAddInstructionalListingViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ILCAddInstructionalListingViewController) {
        self.view = view
    }
}

extension ILCAddInstuctionalListingPresenter{
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: ILCAddResponse?, _ message: String?) -> Void){
        
        ILCService.updateData(serviceAddRequest: view.ilcAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
//                    Helper.sharedInstance.hideProgressHudView()
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
//                    Helper.sharedInstance.hideProgressHudView()
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
    
    /**
     *  Get Personal service list from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getInstructionalService(){
        
        ICService.getInstructionalService(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
//                    Helper.sharedInstance.hideProgressHudView()
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        //                        self.view.instructionalList = response?.data ?? []
                        let instructionalList = response?.data ?? []
                        
                        if(instructionalList.count > 0){
                            self.view.instructionalList = instructionalList.map({ServiceListingList(id: $0.content_id ?? "", title: $0.content_name ?? "")})
                        }
                    }
                    
                    self.view.setScreenData()
                    
                }
            } else {
                OperationQueue.main.addOperation() {
//                    Helper.sharedInstance.hideProgressHudView()
                    Spinner.hide()
                }
            }
        })
    }
}

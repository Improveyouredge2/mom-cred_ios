//
//  LBAddWebsiteViewController.swift
//  MomCred
//
//  Created by MD on 25/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class LBAddWebsiteViewController: LMBaseViewController, BIAddLocationPresentable {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var primaryWebLink: BusinessInformationWeblink!
    @IBOutlet weak var viewTableView: UIView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    fileprivate var weblinkList: [BusinessWebLinkInfo] = []
    fileprivate let formNumber = 4
    
    var biAddRequest: BIAddRequest?
    fileprivate var isUpdate = false
    
    fileprivate var presenter = BIAddWebLinkPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        viewTableView.isHidden = true
        
        setScreenData()

        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(weblinkList.count == 0){
            viewTableView.isHidden = true
        } else {
            viewTableView.isHidden = false
        }
        
        if(tableView.contentSize.height > 0){
            tableView.scrollToBottom()
            constraintstableViewHeight?.constant = tableView.contentSize.height
            
            updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: { [weak self] in
            self?.tableView.layoutIfNeeded()
        }, completion: { [weak self] (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            if let cells = self?.tableView.visibleCells {
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                self?.constraintstableViewHeight.constant = heightOfTableView
            }
        })
    }
}

extension LBAddWebsiteViewController {
    fileprivate func setScreenData(){
        if(biAddRequest != nil){
            if(biAddRequest?.busi_weblink_list != nil && (biAddRequest?.busi_weblink_list?.count)! > 0){
                weblinkList = biAddRequest?.busi_weblink_list ?? []
                tableView.reloadData()
            }
        }
    }

    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(biAddRequest?.busi_weblink_list != nil && (biAddRequest?.busi_weblink_list?.count)! != weblinkList.count){
            isUpdate = true
        }
        
        if(isUpdate){
            self.isUpdate = isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr() {
        let addSocialMediaVC: LBAddSocialMediaViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.LocalBusinessStoryboard, viewControllerName: LBAddSocialMediaViewController.nameOfClass)
        
        addSocialMediaVC.biAddRequest = biAddRequest
        navigationController?.pushViewController(addSocialMediaVC, animated: true)
    }
    
    fileprivate func isFormValid() -> Bool {
        var message = ""
        
        if (weblinkList.count == 0) {
            message = LocalizationKeys.error_add_website.getLocalized()
            showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0  && biAddRequest?.busi_id == nil){
            biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(biAddRequest?.busi_id == nil){
            biAddRequest?.busi_id = ""
        }
        
        biAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        biAddRequest?.busi_weblink_list = weblinkList
        
        return true
    }
}

extension LBAddWebsiteViewController{
    @IBAction func methodAddWebLinkAction(_ sender: UIButton){
        var isValid = true
        let businessWebLinkInfo = BusinessWebLinkInfo()
        
        if((primaryWebLink.inputName.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: primaryWebLink.inputName.text() ?? "")){
            businessWebLinkInfo.webLinkTitle = primaryWebLink.inputName.text() ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_invalid_url.getLocalized(), buttonTitle: nil, controller: nil)
            
            isValid = false
            return
        }
        
        if((primaryWebLink.textViewDesc.text?.length)! > 0){
            businessWebLinkInfo.webLinkDesc = primaryWebLink.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_website_invalid_url_desc.getLocalized(), buttonTitle: nil, controller: nil)
            
            isValid = false
            return
        }
        
        if primaryWebLink.btnMainWebsiteYes.isSelected {
            businessWebLinkInfo.mainWebsite = NSNumber(booleanLiteral: true)
        }
        
        if(isValid && weblinkList.count < HelperConstant.minimumBlocks){
            
            isUpdate = true
            primaryWebLink.inputName.input.text = ""
            primaryWebLink.textViewDesc.text = ""
            
            weblinkList.append(businessWebLinkInfo)
            tableView.reloadData()
            viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is LocalBusinessDetailViewController }
            if let biDetailVC = biDetailVC as? LocalBusinessDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK: - UITableViewDataSource & UITableViewDelegate
extension LBAddWebsiteViewController: UITableViewDataSource , UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weblinkList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationWebsiteCell.nameOfClass) as! BusinessInformationWebsiteCell
        
        let objInfo = weblinkList[indexPath.row]
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        cell.lblMainWebsite.text = (objInfo.mainWebsite?.boolValue ?? false ) ? "Yes": "No"
        cell.lblWebsiteLink.text = objInfo.webLinkTitle
        cell.lblWebsiteDesc.text = objInfo.webLinkDesc
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension LBAddWebsiteViewController: BusinessInformationWebsiteCellDelegate {
    func removeWebsiteInfo(cellIndex: IndexPath?){
        isUpdate = true
        if(weblinkList.count > 0){
            weblinkList.remove(at: cellIndex?.row ?? 0)
            tableView.reloadData()
            if(weblinkList.count == 0){
                viewTableView.isHidden = true
            }
        }
    }
}

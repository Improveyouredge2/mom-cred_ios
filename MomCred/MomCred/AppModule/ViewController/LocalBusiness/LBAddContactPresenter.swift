//
//  LBAddContactPresenter.swift
//  MomCred
//
//  Created by MD on 25/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class LBAddContactPresenter {
    private weak var view: LBAddContactViewController!

    func connectView(view: LBAddContactViewController) {
        self.view = view
    }
}

extension LBAddContactPresenter {
    func submitData(callback:@escaping (_ status:Bool, _ response: BIAddResponse?, _ message: String?) -> Void){
        BIServiceStep1.updateData(biRequest: view.biAddRequest, imageList: nil, callback: { (status, response, message) in
            DispatchQueue.main.async {
                Spinner.hide()
                if !status {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
                callback(status, response, message)
            }
        })
    }
}

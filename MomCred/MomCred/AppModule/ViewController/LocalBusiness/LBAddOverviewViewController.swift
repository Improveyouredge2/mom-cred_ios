//
//  LBAddOverviewViewController.swift
//  MomCred
//
//  Created by MD on 24/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown
import AVKit

class LBAddOverviewViewController: LMBaseViewController {
    
    @IBOutlet weak var imgProfile: ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn: UIImageView!
    @IBOutlet weak var documentImgCollectionView: UICollectionView!
    
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var textViewMissionStmt: KMPlaceholderTextView!
    @IBOutlet weak var lblUploadImageVideo: UILabel!
    
    @IBOutlet weak var dropDownOrganisationType: DropDown! {
        didSet {
            dropDownOrganisationType.listHeight = 100
        }
    }
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    fileprivate var presenter = LBAddOverviewPresenter()
    fileprivate let formNumber = 1
    fileprivate var selectedDocumentImages: UploadImageData?
    fileprivate var selectedIndex = -1
    private var selectedBusinessEntity: String?
    private var defaultDocumentCollectionHeight: CGFloat = 0.00

    private var businessNewTypeView: BusinessAddNewTypeViewController?
    
    var isScreenDataReload = false
    
    var selectedBusinessTypeId: String = ""
    var arrDocumentImages = [UploadImageData]()
    var biAddRequest: BIAddRequest? = BIAddRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        isScreenDataReload = true
        
        documentImgCollectionView.backgroundColor = UIColor.clear
        
        checkBusinessLocalImage()
        
        defaultDocumentCollectionHeight = constraintDocumentCollectionHeight.constant
        constraintDocumentCollectionHeight.constant = 0.0
        
        setScreenData()
        
        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
}

extension LBAddOverviewViewController {
    override func pickedImage(fromImagePicker: UIImage, imageData: Data, mediaType: String, imageName: String, fileUrl: String?) {
        
        let imageDataTemp = UploadImageData()
        let theExt = (imageName as NSString).pathExtension.lowercased()
        
        if ["jpg", "jpeg", "png"].contains(theExt) {
            imageDataTemp.imageType = "1"
        } else {
            imageDataTemp.imageType = "2"
        }
        
        if let busiId = biAddRequest?.busi_id {
            imageDataTemp.id = busiId
        } else if let busiId = UserDefault.getBIID(), !busiId.isEmpty {
            imageDataTemp.id = busiId
        }
        
        imageDataTemp.imageData = imageData
        imageDataTemp.imageName = "\((arrDocumentImages.count + 1))\(imageName)"
        imageDataTemp.imageKeyName = "busi_img"
        imageDataTemp.filePath = fileUrl
        
        imageDataTemp.imageType = mediaType
        imageDataTemp.imageThumbnailData = fromImagePicker.pngData()
        
        // Check of video size
        if imageDataTemp.imageType == "2" {
            if(Int(Helper.sharedInstance.checkVideoSize(reqData: imageData)) < HelperConstant.LIMIT_VIDEO_SIZE) {
                imgProfile.image = fromImagePicker
                imgPlayBtn.isHidden = false
                var arrTempDocumentImages = [UploadImageData]()
                arrTempDocumentImages.append(imageDataTemp)
                for arrDocument in arrDocumentImages{
                    arrTempDocumentImages.append(arrDocument)
                }
                arrDocumentImages = arrTempDocumentImages
            } else {
                showBannerAlertWith(message: LocalizationKeys.error_file_size.getLocalized(), alert: .error)
            }
        } else {
            imgProfile.image = fromImagePicker
            imgPlayBtn.isHidden = true
            arrDocumentImages.append(imageDataTemp)
        }
        
        if !arrDocumentImages.isEmpty,  arrDocumentImages.count < HelperConstant.maximumProfileMedia {
            constraintDocumentCollectionHeight.constant = defaultDocumentCollectionHeight
            documentImgCollectionView.reloadData()
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}


extension LBAddOverviewViewController {
    fileprivate func checkBusinessLocalImage() {
        if let busi_id = UserDefault.getBIID(), !busi_id.trim().isEmpty {
            let folderName = "busi_\(busi_id)"
            if PMFileUtils.directoryExistsAtPath(folderName) {
                let fileUrls: [URL]? = PMFileUtils.getFolderAllFileUrls(folderName: folderName)
                print("Urls: \(fileUrls ?? [])")
                
                if(fileUrls != nil && (fileUrls?.count)! > 0) {
                    do {
                        for fileUrl in (fileUrls)! {
                            
                            let imageData = try Data(contentsOf: fileUrl)
                            
                            let theFileName = (fileUrl.absoluteString as NSString).lastPathComponent
                            let theExt = (fileUrl.absoluteString as NSString).pathExtension

                            let imageDataTemp = UploadImageData()
                            
                            imageDataTemp.imageData = imageData
                            imageDataTemp.imageName = theFileName
                            imageDataTemp.imageKeyName = "busi_img"
                            if(theExt.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpeg") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame) {
                                imageDataTemp.imageType = "1"
                            } else {
                                imageDataTemp.imageType = "2"
                            }
                            arrDocumentImages.append(imageDataTemp)
                        }
                    } catch {
                        print("Error while enumerating files")
                    }
                    
                    if !arrDocumentImages.isEmpty {
                        documentImgCollectionView.reloadData()
                    }
                }
            }
        }
    }

    fileprivate func setScreenData() {
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Business Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
                
        if let mediaItem = biAddRequest?.mediabusiness?.first, let mediaThumb = mediaItem.thumb, let url = URL(string: mediaThumb) {
            imgProfile.sd_setImage(with: url)
            imgPlayBtn.isHidden = (mediaItem.media_extension != HelperConstant.MediaExtension.Video.MediaExtensionText)

            constraintDocumentCollectionHeight.constant = defaultDocumentCollectionHeight
            documentImgCollectionView.reloadData()
        }

        inputName.input.text = biAddRequest?.busi_title
        textViewDesc.text = biAddRequest?.busi_desc
        textViewMissionStmt.text = biAddRequest?.busi_statement

        dropDownOrganisationType.text = biAddRequest?.busi_entity_title
        selectedBusinessEntity = biAddRequest?.busi_entity
        setupNonInstructionalBusinessEntityType()
        
        if let mediaItems = biAddRequest?.mediabusiness, !mediaItems.isEmpty {
            for mediaBusiness in mediaItems {
                let mediaImage = UploadImageData()
                mediaImage.imageUrl = mediaBusiness.thumb ?? ""
                mediaImage.id = mediaBusiness.media_id ?? ""
                if let mediaExt = Int(mediaBusiness.media_extension ?? "0") {
                    if(mediaExt == 0 || mediaExt == 1) {
                        mediaImage.imageType = "1"
                    } else {
                        mediaImage.imageType = "2"
                    }
                } else {
                    mediaImage.imageType = "1"
                }
                
                arrDocumentImages.append(mediaImage)
            }
            
            documentImgCollectionView.reloadData()
        }
    }
    
    func setupNonInstructionalBusinessEntityType() {
        dropDownOrganisationType.optionArray = []
        dropDownOrganisationType.optionIds = []
        if let businessType = biAddRequest?.business_type, !businessType.isEmpty {
            for busiType in businessType {
                let busiTitle = busiType.listing_title ?? ""
                let busiId = Int(busiType.listing_id ?? "") ?? 0
                dropDownOrganisationType.optionArray.append(busiTitle)
                dropDownOrganisationType.optionIds?.append(busiId)
            }
        }

        dropDownOrganisationType.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.selectedBusinessEntity = "\(id)"
        }
    }
    
    fileprivate func isFormValid() -> Bool {
        var message = ""
        
        if arrDocumentImages.isEmpty {
            message = LocalizationKeys.error_select_business_image.getLocalized()
        } else if (inputName.text()?.trim().isEmpty)! {
            message = LocalizationKeys.error_select_business_name.getLocalized()
        } else if (textViewDesc.text?.trim().isEmpty)! {
            message = LocalizationKeys.error_enter_business_desc.getLocalized()
        } else if (textViewMissionStmt.text?.trim().isEmpty)! {
            message = LocalizationKeys.error_enter_mission_statement.getLocalized()
        } else if selectedBusinessEntity == nil || selectedBusinessEntity == "" {
            message = "Please select Non-instructional local business type."
        }
        
        if !message.isEmpty {
            showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if biAddRequest?.busi_id == nil || biAddRequest?.busi_id?.trim() == "" {
            biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        }
        
        biAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        biAddRequest?.busi_title = inputName.text()?.trim()
        biAddRequest?.busi_desc = textViewDesc.text?.trim()
        biAddRequest?.busi_statement = textViewMissionStmt.text?.trim()
        
        return true
    }

    fileprivate func isDataUpdate() -> Bool {
        
        var isUpdate = false
        
        if biAddRequest?.busi_title != inputName.text()?.trim() {
            isUpdate = true
        } else if biAddRequest?.busi_desc != textViewDesc.text?.trim() {
            isUpdate = true
        } else if biAddRequest?.busi_statement != textViewMissionStmt.text?.trim() {
          isUpdate = true
        } else if biAddRequest?.busi_entity != selectedBusinessEntity {
            isUpdate = true
        }
        
        for imageObj in arrDocumentImages{
            if(imageObj.imageData != nil) {
                isUpdate = true
            }
        }

        return isUpdate
    }
    
    fileprivate func openNextScr() {
        let bilocationViewController: LBAddLocationViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.LocalBusinessStoryboard, viewControllerName: LBAddLocationViewController.nameOfClass)
        bilocationViewController.biAddRequest = biAddRequest
        bilocationViewController.isEditingBusiness = isEditingBusiness
        navigationController?.pushViewController(bilocationViewController, animated: true)
    }
    
    fileprivate func isVideoInList() -> Bool {
        let firstIndex = arrDocumentImages.firstIndex { $0.imageType == "2" }
        return firstIndex != nil
    }
}

//MARK: - Button Action method implementation
extension LBAddOverviewViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is LocalBusinessDetailViewController }
            if let biDetailVC = biDetailVC as? LocalBusinessDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }

    @IBAction func btnImagePickerAction(_ sender: UIButton) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            displayPhotoSelectionOption(withCircularAllow: false, isSelectVideo: true)
        }
    }
}

extension LBAddOverviewViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDocumentImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath as IndexPath) as! SerivceImageCell
        
        let imageData = arrDocumentImages[indexPath.row]
        
        cell.delegate = self
        cell.cellIndex = indexPath.row
        cell.btnDelete.isHidden = false
        cell.btnDelete.tag = indexPath.row

        let isVideo = imageData.imageType == "2"
        cell.imgPlay.isHidden = !isVideo
        
        if let imageURLStr = imageData.imageUrl, let url = URL(string: imageURLStr) {
            cell.imgService.sd_setImage(with: url)
        } else if isVideo, let data = imageData.imageThumbnailData {
            cell.imgService.image = UIImage(data: data)
        } else if let data = imageData.imageData {
            cell.imgService.image = UIImage(data: data)
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 80)
    }
}

extension LBAddOverviewViewController: ProductCellDelegate {
    func methodShowProductDetails(cellIndex: Int) {
        if arrDocumentImages.count > cellIndex {
            let imageData = arrDocumentImages[cellIndex]

            let isVideo = imageData.imageType == "2"
            imgPlayBtn.isHidden = !isVideo
            
            if let imageURLStr = imageData.imageUrl, let url = URL(string: imageURLStr) {
                imgProfile.sd_setImage(with: url)
            } else if isVideo, let data = imageData.imageThumbnailData {
                imgProfile.image = UIImage(data: data)
            } else if let data = imageData.imageData {
                imgProfile.image = UIImage(data: data)
            }
        }
    }
}

extension LBAddOverviewViewController {
    @IBAction func actionBtnAddImg(_ sender: UIButton) {
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)) {
            if(arrDocumentImages.count < HelperConstant.maximumProfileMedia) {
                if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                    //already authorized
                    debugPrint("already authorized")
                    DispatchQueue.main.async { [weak self] in
                        let haveVideo: Bool = self?.isVideoInList() ?? false
                        self?.displayPhotoSelectionOption(withCircularAllow: false, isSelectVideo: !haveVideo)
                    }
                } else {
                    AVCaptureDevice.requestAccess(for: .video) { (granted: Bool) in
                        DispatchQueue.main.async { [weak self] in
                            if granted {
                                //access allowed
                                debugPrint("access allowed")
                                let haveVideo: Bool = self?.isVideoInList() ?? false
                                self?.displayPhotoSelectionOption(withCircularAllow: false, isSelectVideo: !haveVideo)
                            } else {
                                //access denied
                                debugPrint("Access denied")
                                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_this_app_needs_access_to_your_camera_and_gallery.getLocalized(), buttonTitle: nil, controller: nil)
                            }
                        }
                    }
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_video_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
    
    @IBAction func actionBtnDeleteStoreImg(_ sender: UIButton) {
        
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            let imageData = arrDocumentImages[sender.tag]
            
            if imageData.imageUrl != nil, let emptyURL = imageData.imageUrl?.trim().isEmpty, !emptyURL {
                selectedDocumentImages = imageData
                let logoutViewController: PMLogoutView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
                logoutViewController.delegate = self
                logoutViewController.scrTitle = ""
                logoutViewController.scrDesc = LocalizationKeys.error_remove_media.getLocalized()
                selectedIndex = sender.tag
                logoutViewController.modalPresentationStyle = .overFullScreen
                present(logoutViewController, animated: true, completion: nil)
            } else {
                if arrDocumentImages.count > sender.tag {
                    arrDocumentImages.remove(at: sender.tag)
                }
                
                if arrDocumentImages.isEmpty {
                    constraintDocumentCollectionHeight.constant = 0.00
                    imgProfile.image = nil
                } else if let imageData = arrDocumentImages.first {

                    let isVideo = imageData.imageType == "2"
                    imgPlayBtn.isHidden = !isVideo
                    
                    if let imageURLStr = imageData.imageUrl, let url = URL(string: imageURLStr) {
                        imgProfile.sd_setImage(with: url)
                    } else if isVideo, let data = imageData.imageThumbnailData {
                        imgProfile.image = UIImage(data: data)
                    } else if let data = imageData.imageData {
                        imgProfile.image = UIImage(data: data)
                    }
                    documentImgCollectionView.reloadData()
                }
            }
        }
    }
}

extension LBAddOverviewViewController: PMLogoutViewDelegate {
    func cancelLogoutViewScr() { }
    
    func acceptLogoutViewScr() {
        if selectedDocumentImages != nil {
            let deleteMediaRequest = DeleteMediaRequest(media_id: selectedDocumentImages?.id ?? "")
            presenter.deleteMediaData(deleteMediaRequest: deleteMediaRequest, callback: { [weak self] (status, response, message) in
                if status, let self = self {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    if !self.arrDocumentImages.isEmpty {
                        self.arrDocumentImages.remove(at: self.selectedIndex)
                    }
                    
                    if self.arrDocumentImages.isEmpty {
                        self.constraintDocumentCollectionHeight.constant = 0.00
                        self.imgPlayBtn.isHidden = true
                        self.imgProfile.image = nil
                    }
                    self.documentImgCollectionView.reloadData()
                }
            })
        }
    }
}

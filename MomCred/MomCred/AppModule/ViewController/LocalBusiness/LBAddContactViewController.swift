//
//  LBAddContactViewController.swift
//  MomCred
//
//  Created by MD on 25/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class LBAddContactViewController: LMBaseViewController {
    
    var expandTableNumber = [Int] ()

    @IBOutlet weak var primaryEmailContact: BusinessInformationEmailContactView!
    @IBOutlet weak var primaryPhoneContact: BusinessInformationEmailContactView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    var biAddRequest: BIAddRequest?
    var personnelList: [ServiceListingList]? {
        didSet {
            if let list = personnelList {
                primaryEmailContact.affilatedInfo = list
                primaryPhoneContact.affilatedInfo = list
            }
        }
    }
    
    fileprivate var presenter = LBAddContactPresenter()
    fileprivate let formNumber = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)
        setScreenData()
        
        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
}

extension LBAddContactViewController{
    func setScreenData() {
        primaryEmailContact.placeHolderText = "Email "
        primaryPhoneContact.placeHolderText = "Phone Number "
        
        primaryEmailContact.inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Email ")
                .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        primaryPhoneContact.inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Mobile Number ")
                .maxLength(HelperConstant.LIMIT_UPPER_PHONE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        primaryPhoneContact.inputName.input.keyboardType = UIKeyboardType.phonePad

        
        primaryEmailContact.inputName.input.text = biAddRequest?.busi_email?.name ?? ""
        primaryEmailContact.textViewDesc.text = biAddRequest?.busi_email?.desc ?? ""

        primaryPhoneContact.inputName.input.text = biAddRequest?.busi_phone?.name ?? ""
        primaryPhoneContact.textViewDesc.text = biAddRequest?.busi_phone?.desc ?? ""
    }

    fileprivate func isDataUpdate() -> Bool {
        var isUpdate = false
        
        if biAddRequest != nil {
            if biAddRequest?.busi_email != nil || primaryEmailContact.inputName.input.text != nil {
                let contactInfo: ContactInfo? = biAddRequest?.busi_email ?? ContactInfo()
                if primaryEmailContact.inputName.input.text != contactInfo?.name {
                    isUpdate = true
                } else if primaryEmailContact.textViewDesc.text != contactInfo?.desc {
                    isUpdate = true
                } else if primaryEmailContact.affilatedSelectedId != contactInfo?.affilatedList {
                    isUpdate = true
                }
            }

            // Primary Phone number
            if(biAddRequest?.busi_phone != nil || primaryPhoneContact.inputName.input.text != nil){
                let contactInfo: ContactInfo? = biAddRequest?.busi_phone ?? ContactInfo()
                if primaryPhoneContact.inputName.input.text != contactInfo?.name {
                    isUpdate = true
                } else if primaryPhoneContact.textViewDesc.text != contactInfo?.desc {
                    isUpdate = true
                } else if primaryPhoneContact.affilatedSelectedId != contactInfo?.affilatedList {
                    isUpdate = true
                }
            }
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr() {
        let addWebsiteVC: LBAddWebsiteViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.LocalBusinessStoryboard, viewControllerName: LBAddWebsiteViewController.nameOfClass)
        addWebsiteVC.biAddRequest = biAddRequest
        addWebsiteVC.isEditingBusiness = isEditingBusiness
        navigationController?.pushViewController(addWebsiteVC, animated: true)
    }

    fileprivate func isFormValid() -> Bool {
        var message = ""
        
        if let empty = primaryEmailContact.inputName.text()?.trim().isEmpty, empty {
            message = LocalizationKeys.error_enter_primary_email.getLocalized()
        } else if let isEmail = primaryEmailContact.inputName.text()?.isEmail(), !isEmail {
            message = LocalizationKeys.invalidEmail.getLocalized()
        } else if let empty = primaryEmailContact.textViewDesc.text?.trim().isEmpty, empty {
            message = LocalizationKeys.error_enter_primaryEmailDesc.getLocalized()
        }
        
        if let empty = primaryPhoneContact.inputName.text()?.trim().isEmpty, empty {
            message = LocalizationKeys.error_primary_number.getLocalized()
        } else if let length = primaryPhoneContact.inputName.text()?.trim().length, (length < HelperConstant.LIMIT_LOWER_PHONE || length > HelperConstant.LIMIT_UPPER_PHONE) {
            message = LocalizationKeys.invalidMobile.getLocalized()
        } else if let empty = primaryPhoneContact.textViewDesc.text?.trim().isEmpty, empty {
            message = LocalizationKeys.error_primary_phone_desc.getLocalized()
        }

        if !message.isEmpty {
            showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if biAddRequest?.busi_id == nil || biAddRequest?.busi_id?.trim() == "" {
            biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        }
        
        biAddRequest?.pageid = NSNumber(integerLiteral: formNumber)

        var contactInfo = ContactInfo()
        contactInfo.name = primaryEmailContact.inputName.text() ?? ""
        contactInfo.desc = primaryEmailContact.textViewDesc.text ?? ""
        contactInfo.affilatedList = primaryEmailContact.affilatedSelectedId
        
        biAddRequest?.busi_email = contactInfo

        contactInfo = ContactInfo()
        contactInfo.name = primaryPhoneContact.inputName.text() ?? ""
        contactInfo.desc = primaryPhoneContact.textViewDesc.text ?? ""
        contactInfo.affilatedList = primaryPhoneContact.affilatedSelectedId
        biAddRequest?.busi_phone = contactInfo

        return true
    }
}

extension LBAddContactViewController {
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is LocalBusinessDetailViewController }
            if let biDetailVC = biDetailVC as? LocalBusinessDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

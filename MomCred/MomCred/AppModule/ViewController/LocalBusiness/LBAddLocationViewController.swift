//
//  LBAddLocationViewController.swift
//  MomCred
//
//  Created by MD on 24/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

class LBAddLocationViewController: LMBaseViewController, BIAddLocationPresentable {
    
    var expandTableNumber = [Int] ()
    
    @IBOutlet weak var btnYesPrimLoc: UIButton!
    @IBOutlet weak var btnNoPrimLoc: UIButton!
    @IBOutlet weak var primaryLocation: LocalBusinessLocationView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    var biAddRequest: BIAddRequest?
    
    fileprivate var presenter = BIAddLocationPresenter()
    fileprivate let formNumber = 2
    var isScreenDataReload = false
    var isUpdate = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)

        let businessLocationInfo = LocalBusinessLocationInfo()
        primaryLocation.businessLocationInfo = businessLocationInfo
        primaryLocation.setScreenData()
        isScreenDataReload = true
        setScreenData()

        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    @objc fileprivate func updateScreen() {
        let businessLocationInfo = LocalBusinessLocationInfo()
        primaryLocation.businessLocationInfo = businessLocationInfo
        primaryLocation.setScreenData()
        isScreenDataReload = true
        setScreenData()
    }
}

extension LBAddLocationViewController {
    fileprivate func setScreenData() {
        if let biAddRequest = biAddRequest {
            primaryLocation.businessLocationInfo?.locationAddress = biAddRequest.location_address ?? ""
            primaryLocation.businessLocationInfo?.locationCity = biAddRequest.location_city ?? ""
            primaryLocation.businessLocationInfo?.locationState = biAddRequest.location_state ?? ""
            primaryLocation.businessLocationInfo?.busniessHour = biAddRequest.location_busi_hours ?? []

            primaryLocation.setScreenData()
            setYesNOStatusOnExtraAddedBtns()
        }
    }

    fileprivate func isDataUpdate() -> Bool {
        var isUpdate = false
        if biAddRequest?.location_address != primaryLocation.businessLocationInfo?.locationAddress {
            isUpdate = true
        }
        
        if biAddRequest?.location_city != primaryLocation.businessLocationInfo?.locationCity {
            isUpdate = true
        }
        
        if biAddRequest?.location_state != primaryLocation.businessLocationInfo?.locationState {
            isUpdate = true
        }
        
        if primaryLocation.isUpdateNewVal {
            isUpdate = primaryLocation.isUpdateNewVal
        }

        if isUpdate {
            self.isUpdate = isUpdate
        }
        return isUpdate
    }
    
    fileprivate func openNextScr() {
        let addContactVC: LBAddContactViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.LocalBusinessStoryboard, viewControllerName: LBAddContactViewController.nameOfClass)
        if primaryLocation.constraintstableViewBusinessHourHeight.constant == 0 {
            biAddRequest?.location_busi_hours = []
        }
        addContactVC.isEditingBusiness = isEditingBusiness
        addContactVC.biAddRequest = biAddRequest
        navigationController?.pushViewController(addContactVC, animated: true)
    }

    fileprivate func isFormValid() -> Bool {
        if primaryLocation?.inputCity != nil, primaryLocation?.inputCity.text()?.trim() == "" {
            let message = LocalizationKeys.error_city.getLocalized()
            showBannerAlertWith(message: message, alert: .error)
            return false
        }

        if biAddRequest?.busi_id == nil || biAddRequest?.busi_id?.trim() == "" {
            biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        }
        
        biAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        biAddRequest?.location_address = primaryLocation.businessLocationInfo?.locationAddress ?? ""
        biAddRequest?.location_city = primaryLocation.businessLocationInfo?.locationCity ?? ""
        biAddRequest?.location_state = primaryLocation.businessLocationInfo?.locationState ?? ""
        biAddRequest?.location_busi_hours = primaryLocation.businessLocationInfo?.busniessHour ?? []
        
        return true
    }
}

extension LBAddLocationViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is LocalBusinessDetailViewController }
            if let biDetailVC = biDetailVC as? LocalBusinessDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension LBAddLocationViewController {
    func setYesNOStatusOnExtraAddedBtns() {
        if let emptyHours = biAddRequest?.location_busi_hours?.isEmpty, !emptyHours {
            actionBtnPrimeLocYesNo(btnYesPrimLoc)
        } else {
            actionBtnPrimeLocYesNo(btnNoPrimLoc)
        }
    }

    @IBAction func actionBtnPrimeLocYesNo(_ sender: UIButton) {
        btnYesPrimLoc.isSelected = false
        btnNoPrimLoc.isSelected = false
        
        if btnYesPrimLoc == sender {
            btnYesPrimLoc.isSelected = true
            primaryLocation.constraintstableViewBusinessHourHeight.constant = primaryLocation.tableViewBusinessHour.contentSize.height
            primaryLocation.tableViewBusinessHour.isHidden = false
        } else {
            primaryLocation.constraintstableViewBusinessHourHeight.constant = 0
            primaryLocation.tableViewBusinessHour.isHidden = true
            btnNoPrimLoc.isSelected = true
        }
    }
}

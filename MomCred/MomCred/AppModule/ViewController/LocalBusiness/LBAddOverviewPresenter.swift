//
//  LBAddOverviewPresenter.swift
//  MomCred
//
//  Created by MD on 24/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class LBAddOverviewPresenter {
    private weak var view: LBAddOverviewViewController?
    
    func connectView(view: LBAddOverviewViewController) {
        self.view = view
    }
    
    func submitData(callback:@escaping (_ status: Bool, _ response: BIAddResponse?, _ message: String?) -> Void){
        BIServiceStep1.updateData(biRequest: view?.biAddRequest, imageList: nil, callback: { (status, response, message) in
            DispatchQueue.main.async() { [weak self] in
                guard let self = self else { return }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0 , execute: {
                    Spinner.hide()
                })
                if status {
                    if let businessId = response?.data?.business_id?.stringValue {
                        UserDefault.saveBIID(id: businessId)
                        self.view?.biAddRequest?.busi_id = businessId
                        
                        let folderPref = "busi_\(businessId)"
                        var isVideo = false
                        
                        if let documentImages = self.view?.arrDocumentImages {
                            for imageData in documentImages {
                                if imageData.imageUrl == nil, !imageData.status, let fileData = imageData.imageData {
                                    if imageData.imageType == "2", let fileName = imageData.imageName?.split(separator: "/").last {
                                        isVideo = true
                                        // save video file
                                        _ = PMFileUtils.saveFileAtLocalPath(fileData: fileData as NSData, fileName: String(fileName), folderName: folderPref)
                                        // save thumbnail
                                        if let thumbData = imageData.imageThumbnailData {
                                            let thumbName = "thumb_\(fileName.split(separator: ".").first ?? "").png"
                                            _ = PMFileUtils.saveFileAtLocalPath(fileData: thumbData as NSData, fileName: thumbName, folderName: folderPref)
                                        }
                                        
                                        // Upload image in background
                                        Helper.sharedInstance.uploadBIPendingImages()
                                        imageData.status = true
                                    } else if imageData.imageType == "1" {
                                        let randomString = "\(Date().timeIntervalSince1970)".split(separator: ".").last ?? "\(arc4random())"
                                        let imageName = "uploadImage_\(randomString).jpg"
                                        _ = PMFileUtils.saveFileAtLocalPath(fileData: fileData as NSData, fileName: imageName, folderName: folderPref)
                                        imageData.status = true
                                    }
                                }
                                // Upload image in background
                                if(!isVideo){
                                    Helper.sharedInstance.uploadBIPendingImages()
                                }
                            }
                        }
                    }
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
                callback(status, response, message)
            }
        })
    }

    func deleteMediaData(deleteMediaRequest: DeleteMediaRequest?, callback:@escaping (_ status:Bool, _ response: DeleteMediaModelResponse?, _ message: String?) -> Void) {
        BIServiceStep1.deleteIndividualMediaInfo(requestData: deleteMediaRequest, callback: { (status, response, message) in
            DispatchQueue.main.async {
                Spinner.hide()
                if !status {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
                callback(status, response, message)
            }
        })
    }
}

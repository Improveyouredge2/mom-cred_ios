//
//  LocalLocalBusinessLocationView.swift
//  MomCred
//
//  Created by MD on 24/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

protocol LocalLocalBusinessLocationViewDelegate {
    func reloadCellInfo(cellIndex: IndexPath?, businessLocationInfo: LocalBusinessLocationInfo?)
    func openAddressPicker(cellIndex: IndexPath?)
}

class LocalBusinessLocationView: UIView {
    @IBOutlet weak var inputAddress: RYFloatingInput!
    @IBOutlet weak var inputCity: RYFloatingInput!
    @IBOutlet weak var inputState: RYFloatingInput!

    @IBOutlet weak var tableViewBusinessHour: BIDetailTableView! {
        didSet {
            tableViewBusinessHour.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintstableViewBusinessHourHeight: NSLayoutConstraint!
    
    var delegate: LocalLocalBusinessLocationViewDelegate?
    var cellIndex: IndexPath?
    var businessLocationInfo: LocalBusinessLocationInfo?
    var isUpdateNewVal = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableViewBusinessHour.register(UINib(nibName: BusinessLocationTimeInputCell.nameOfClass, bundle: nil), forCellReuseIdentifier: BusinessLocationTimeInputCell.nameOfClass)
        Helper.setViewCornerRadius(views: [inputCity, inputState, inputAddress], borderColor: .white, borderWidth: 1.0, cornerRadius: 8)
    }
}

extension LocalBusinessLocationView {
    func setScreenData() {
        inputAddress.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(businessLocationInfo?.locationAddress ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Address ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputAddress.setBorderColorClear()
        inputAddress.input.delegate = self
        
        inputCity.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(businessLocationInfo?.locationCity ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("City ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputCity.setBorderColorClear()
        inputCity.input.delegate = self
        
        inputState.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(businessLocationInfo?.locationState ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("State ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputState.setBorderColorClear()
        inputState.input.delegate = self

        inputAddress.input.text = businessLocationInfo?.locationAddress
        inputCity.input.text = businessLocationInfo?.locationCity
        inputState.input.text = businessLocationInfo?.locationState

        tableViewBusinessHour.reloadData()
    }
}

//MARK: - Button Action method implementation
extension LocalBusinessLocationView {
    @IBAction func methodAddressAction(_ sender: UIButton) {
        openPlacePicker()
    }
    
    @IBAction func methodStateAction(_ sender: UIButton) {
        openPlacePicker()
    }
    
    @IBAction func methodCityAction(_ sender: UIButton) {
        openPlacePicker()
    }
 }

// MARK: - UITableViewDataSource & UITableViewDelegate
extension LocalBusinessLocationView: UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewBusinessHour {
            return HelperConstant.weekDayName.count // show as many rows as supported in week day names
        }
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewBusinessHour {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTimeInputCell.nameOfClass) as! BusinessLocationTimeInputCell
            
            cell.delegate  = self
            cell.cellIndex = indexPath
            
            let businessHour = businessLocationInfo?.busniessHour.first { $0.index == indexPath.row }

            let dayName = HelperConstant.weekDayName[indexPath.row]
            cell.lblTitle.text = dayName

            if let businessHour = businessHour {
                cell.status = businessHour.status?.boolValue ?? false
                cell.openTime = businessHour.openTime ?? ""
                cell.closeTime = businessHour.closeTime ?? ""
            } else {
                cell.status = false
                cell.openTime = ""
                cell.closeTime = ""
            }

            cell.setScreenData()
            return cell
        }
        
        return UITableViewCell(frame: tableView.frame)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension LocalBusinessLocationView: UITextFieldDelegate {
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if inputAddress.input == textField {
            businessLocationInfo?.locationAddress = inputAddress.text() ?? ""
        } else if inputCity.input == textField {
            businessLocationInfo?.locationCity = inputCity.text() ?? ""
        } else if inputState.input == textField {
            businessLocationInfo?.locationState = inputState.text() ?? ""
        }
        return true
    }
}

extension LocalBusinessLocationView: BusinessLocationTimeInputCellDelegate{
    func sectionSwitchStatus(cellIndex: IndexPath?, status: Bool) {
        guard let cellIndex = cellIndex else { return }
        
        isUpdateNewVal = true
        let existingBusinessHour = businessLocationInfo?.busniessHour.first { $0.index == cellIndex.row }
        if let existingBusinessHour = existingBusinessHour {
            existingBusinessHour.status = NSNumber(booleanLiteral: status)
        } else {
            let businessHour = BusniessHour()
            businessHour.index = cellIndex.row
            businessHour.dayTitle = HelperConstant.weekDayName[cellIndex.row]
            businessHour.status = NSNumber(booleanLiteral: status)
            businessHour.openTime = ""
            businessHour.closeTime = ""
            businessLocationInfo?.busniessHour.append(businessHour)
        }
        
        if(delegate != nil){
            delegate?.reloadCellInfo(cellIndex: cellIndex, businessLocationInfo: businessLocationInfo)
        }
        
        tableViewBusinessHour.reloadData()
    }
    
    func updateTimeInfo(cellIndex: IndexPath?, openTime: String, closeTime: String) {
        guard let cellIndex = cellIndex else { return }
        let businessHour = businessLocationInfo?.busniessHour.first { $0.index == cellIndex.row }
        if let businessHour = businessHour {
            businessHour.openTime = openTime
            businessHour.closeTime = closeTime
        }
    }
}

extension LocalBusinessLocationView {
    func openPlacePicker() {
        let autocompleteController = GMSAutocompleteViewController()
        let filter  = GMSAutocompleteFilter()
        filter.country = "IN"
        filter.type = GMSPlacesAutocompleteTypeFilter.address
        
        autocompleteController.autocompleteFilter = filter
        autocompleteController.delegate = self
        autocompleteController.autocompleteBoundsMode = .restrict
        
        HelperConstant.appDelegate.navigationController?.present(autocompleteController, animated: true, completion: nil)
    }
}

extension LocalBusinessLocationView: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name)) with Coordinate - Lat - \(place.coordinate.latitude) Long - \(place.coordinate.longitude)")
        print("Place address: \(place.formattedAddress!)")
        print("Place attributions: \(String(describing: place.attributions))")
        
        let address = place.formattedAddress ?? ""
        let city = place.addressComponents?.first(where: { $0.type == "locality" })?.name
        let state = place.addressComponents?.first(where: { $0.type == "administrative_area_level_1" })?.name
        
        isUpdateNewVal = true
        inputAddress.input.text = address
        inputCity.input.text  = city
        inputState.input.text = state
        
        businessLocationInfo?.locationAddress = address
        businessLocationInfo?.locationCity = city ?? ""
        businessLocationInfo?.locationState = state ?? ""
        HelperConstant.appDelegate.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        HelperConstant.appDelegate.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension LocalBusinessLocationView: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        if tableView == tableViewBusinessHour {
            constraintstableViewBusinessHourHeight.constant = size.height
        }
    }
}

//
//  LBAddSocialMediaViewController.swift
//  MomCred
//
//  Created by MD on 25/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class LBAddSocialMediaViewController: LMBaseViewController, BIAddLocationPresentable {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    @IBOutlet weak var primarySocialMedia: BusinessInformationSocialMedia!

    fileprivate var socialMediaInfoList: [BusinessSocialMediaInfo] = []
    
    var biAddRequest: BIAddRequest?
    
    fileprivate var presenter = BIAddSocialMediaPresenter()
    fileprivate var biaddExceptionalViewController: BIAddExceptionalViewController?
    fileprivate let formNumber = 5
    fileprivate var isUpdate = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        presenter.connectView(view: self)
        
        setScreenData()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(socialMediaInfoList.count == 0) {
            viewTableView.isHidden = true
        } else {
            viewTableView.isHidden = false
        }
        
        if(tableView.contentSize.height > 0) {
            tableView.scrollToBottom()
            constraintstableViewHeight?.constant = tableView.contentSize.height
            updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: { [weak self] in
            self?.tableView.layoutIfNeeded()
        }, completion: { [weak self] (complete) in
            var heightOfTableView: CGFloat = 0.0
            if let cells = self?.tableView.visibleCells {
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                self?.constraintstableViewHeight.constant = heightOfTableView
            }
        })
    }
}

extension LBAddSocialMediaViewController {
    fileprivate func setScreenData() {
        if(biAddRequest != nil) {
            if(biAddRequest?.busi_sociallink_list != nil && (biAddRequest?.busi_sociallink_list?.count)! > 0) {
                socialMediaInfoList = biAddRequest?.busi_sociallink_list ?? []
                tableView.reloadData()
            }
        }
    }

    fileprivate func isDataUpdate() -> Bool {
        var isUpdate = false
        if(biAddRequest?.busi_sociallink_list != nil && (biAddRequest?.busi_sociallink_list?.count)! != socialMediaInfoList.count) {
            isUpdate = true
        }
        
        if(isUpdate) {
            self.isUpdate = isUpdate
        }
        return isUpdate
    }
    
    fileprivate func isFormValid() -> Bool {
        var message = ""
        
        if (socialMediaInfoList.count == 0) {
            message = LocalizationKeys.error_add_social_webiste.getLocalized()
            showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0  && biAddRequest?.busi_id == nil) {
            biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(biAddRequest?.busi_id == nil) {
            biAddRequest?.busi_id = ""
        }
        
        biAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        biAddRequest?.busi_sociallink_list = socialMediaInfoList
        
        return true
    }
}

extension LBAddSocialMediaViewController {
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is LocalBusinessDetailViewController }
            if let biDetailVC = biDetailVC as? LocalBusinessDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension LBAddSocialMediaViewController {
    @IBAction func methodAddSocialMediaAction(_ sender: UIButton) {
        var isValid = true
        let businessSocialMediaInfo = BusinessSocialMediaInfo()
        
        if((primarySocialMedia.inputName.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: primarySocialMedia.inputName.text() ?? "")) {
            businessSocialMediaInfo.webLinkTitle = primarySocialMedia.inputName.text() ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_media_link.getLocalized(), buttonTitle: nil, controller: nil)
            
            isValid = false
            return
        }
        
        if((primarySocialMedia.textViewDesc.text?.length)! > 0) {
            businessSocialMediaInfo.webLinkDesc = primarySocialMedia.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_media_content.getLocalized(), buttonTitle: nil, controller: nil)
            
            isValid = false
            return
        }
        
        
        if(primarySocialMedia.selectedSocialMedia.length > 0) {
            businessSocialMediaInfo.socialMediaName = primarySocialMedia.selectedSocialMedia
        } else {
            if(!primarySocialMedia.inputNameCustomEntry.isHidden && (primarySocialMedia.inputNameCustomEntry.text()?.trim().isEmpty)!) {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_platform_name.getLocalized(), buttonTitle: nil, controller: nil)
                isValid = false
            } else if(primarySocialMedia.selectedSocialMedia.length == 0 && primarySocialMedia.inputNameCustomEntry.isHidden) {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_social_media_platform.getLocalized(), buttonTitle: nil, controller: nil)
                isValid = false
            }
            
            
            if(isValid == false) {
                return
            }
        }
        
        if(!primarySocialMedia.inputNameCustomEntry.isHidden && !(primarySocialMedia.inputNameCustomEntry.text()?.trim().isEmpty)!) {
            businessSocialMediaInfo.socialMediaName = primarySocialMedia.inputNameCustomEntry.text() ?? ""
        }
        
        
        if(isValid && socialMediaInfoList.count < HelperConstant.minimumBlocks) {
            
            isUpdate = true
            primarySocialMedia.inputName.input.text = ""
            primarySocialMedia.inputNameCustomEntry.input.text = ""
            primarySocialMedia.inputNameCustomEntry.isHidden = true
            primarySocialMedia.textViewDesc.text = ""
            primarySocialMedia.dropDownSocialMediaPlatform.text = ""
            socialMediaInfoList.append(businessSocialMediaInfo)
            tableView.reloadData()
            viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK: - UITableViewDataSource & UITableViewDelegate
extension LBAddSocialMediaViewController: UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return socialMediaInfoList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationSocialMediaCell.nameOfClass) as! BusinessInformationSocialMediaCell
        
        let objInfo = socialMediaInfoList[indexPath.row]
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        cell.lblMainWebsite.text = objInfo.socialMediaName
        cell.lblWebsiteLink.text = objInfo.webLinkTitle
        cell.lblWebsiteDesc.text = objInfo.webLinkDesc
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension LBAddSocialMediaViewController: BusinessInformationSocialMediaCellDelegate {
    
    func removeWebsiteInfo(cellIndex: IndexPath?) {
        isUpdate = true
        if(socialMediaInfoList.count > 0) {
            socialMediaInfoList.remove(at: cellIndex?.row ?? 0)
            tableView.reloadData()
            
            if(socialMediaInfoList.count == 0) {
                viewTableView.isHidden = true
            }
        }
    }
}

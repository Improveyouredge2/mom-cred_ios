//
//  LocalBusinessDetailViewController.swift
//  MomCred
//
//  Created by MD on 23/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation
import Lightbox
import AVKit
import ImageSlideshow

class LocalBusinessDetailViewController: LMBaseViewController {
    @IBOutlet weak var btnOptionMenu: UIButton!
    
    // Profile image view
    @IBOutlet weak var imgProfile: ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitleValue: UILabel!
    
    @IBOutlet weak var lblBIName: UILabel!
    
    @IBOutlet weak var lblBIDesc: UILabel!
    @IBOutlet weak var lblBIMissionStmt: UILabel!
        
    @IBOutlet weak var lblBIPrimaryEmail: UILabel! {
        didSet {
            lblBIPrimaryEmail.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleEmailTap(_:)))
            lblBIPrimaryEmail.addGestureRecognizer(tapGesture)
        }
    }
    @IBOutlet weak var lblBIPrimaryContact: UILabel! {
        didSet {
            lblBIPrimaryContact.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handlePhoneTap(_:)))
            lblBIPrimaryContact.addGestureRecognizer(tapGesture)
        }
    }
    
    // Website display
    @IBOutlet weak var tableViewWebsite: BIDetailTableView!
    @IBOutlet weak var constraintTableViewWebsiteHeight: NSLayoutConstraint!
    
    // Social Media display
    @IBOutlet weak var tableViewSocialMedia: BIDetailTableView!
    @IBOutlet weak var constraintTableViewSocialMediaHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewServiceProviderDetail: UIView!
    
    @IBOutlet weak var lblTown: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblBusinessType: UILabel!
    @IBOutlet weak var businessHoursStackView: UIStackView!

    private var menuArray: [HSMenu] = []
    private var collectionContentHeightObserver: NSKeyValueObservation?

    var biAddRequest:BIAddRequest?
    var shouldRefreshContent: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.edit.getLocalized())
        menuArray.append(menu1)
        scrollView.isHidden = true
        btnOptionMenu.isHidden = true

        tableViewWebsite.sizeDelegate = self
        tableViewSocialMedia.sizeDelegate = self

        constraintTableViewWebsiteHeight.constant = 0
        constraintTableViewSocialMediaHeight.constant = 0

        collectionContentHeightObserver = collectionView.observe(\.contentSize, options: [.new]) { [weak self] _, value in
            if let height = value.newValue?.height, height > 0 {
                self?.constraintDocumentCollectionHeight.constant = height
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let profileImageUrl = PMUserDefault.getProfilePic()
        if(profileImageUrl != nil){
            imgProfile.sd_setImage(with: URL(string: (profileImageUrl!)))
        }

        updateScreenInfo()
    }
    
    deinit {
        collectionContentHeightObserver?.invalidate()
    }
}

extension LocalBusinessDetailViewController{
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }

    @IBAction func actionCreateBI(_ sender: UIButton) { }
    
    @IBAction func actionZoomOption(_ sender: AnyObject) {
        zoomableView()
    }

    @objc private func handleEmailTap(_ gestureRecognizer: UITapGestureRecognizer) {
        if let label = gestureRecognizer.view as? UILabel, let email = label.text?.trim(), !email.isEmpty {
            openMail(for: email)
        }
    }
    
    @objc private func handlePhoneTap(_ gestureRecognizer: UITapGestureRecognizer) {
        if let label = gestureRecognizer.view as? UILabel, let phone = label.text?.trim(), !phone.isEmpty, let url = URL(string: "tel:\(phone)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

extension LocalBusinessDetailViewController: HSPopupMenuDelegate{
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        let addOverviewVC: LBAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.LocalBusinessStoryboard, viewControllerName: LBAddOverviewViewController.nameOfClass)
        
        addOverviewVC.biAddRequest = self.biAddRequest
        addOverviewVC.isEditingBusiness = true
        navigationController?.pushViewController(addOverviewVC, animated: true)
    }

    func updateScreenInfo() {
        if let addRequest = biAddRequest {
            viewServiceProviderDetail.isHidden = false
            collectionView.reloadData()
            
            if let mediaItem = addRequest.mediabusiness?.first, let mediaThumb = mediaItem.thumb {
                imgProfile.sd_setImage(with: URL(string: mediaThumb))
                imgPlayBtn.isHidden = (mediaItem.media_extension != HelperConstant.MediaExtension.Video.MediaExtensionText)
            } else {
                imgPlayBtn.isHidden = true
                imgProfile.image = nil
                constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
            }
            
            lblAddress.text = addRequest.location_address
            lblTown.text = addRequest.location_city
            lblState.text = addRequest.location_state

            lblBIName.text = addRequest.busi_title
            lblBIDesc.text = addRequest.busi_desc
            lblBIMissionStmt.text = addRequest.busi_statement
            
            lblBIPrimaryEmail.text = addRequest.busi_email?.name
            lblBIPrimaryContact.text = biAddRequest?.busi_phone?.name

            tableViewWebsite.reloadData()
            tableViewSocialMedia.reloadData()
            
            lblBusinessType.text = addRequest.busi_entity_title
            if let businessHours = addRequest.location_busi_hours {
                addStackView(locationHour: businessHours)
            }
            
            scrollView.isHidden = false
            btnOptionMenu.isHidden = false
        } else {
            scrollView.isHidden = true
            btnOptionMenu.isHidden = true
        }
    }
    
    private func addStackView(locationHour: [BusniessHour]) {
        removeFromStack()
        for obj in locationHour {
            if let customView = Bundle.main.loadNibNamed("StackXIB", owner: self, options: nil)?.first as? LoopStackView {
                if let openTime = obj.openTime, !openTime.isEmpty, let closeTime = obj.closeTime, !closeTime.isEmpty {
                    customView.lblDay.text = HelperConstant.weekDayName[obj.index]
                    customView.lblStartTime.text = closeTime
                    customView.lblEndTime.text = openTime
                    businessHoursStackView.addArrangedSubview(customView)
                }
            }
        }
    }
    
    private func removeFromStack() {
        businessHoursStackView.removeAllArrangedSubviews()
    }
    
    fileprivate func zoomableView() {
        // Create an array of images.
        var images: [LightboxImage] = []
        
        if let mediaItems = biAddRequest?.mediabusiness, !mediaItems.isEmpty {
            for item in mediaItems {
                if item.media_extension == HelperConstant.MediaExtension.Video.MediaExtensionText, let imageThumb = item.thumb, let thumbURL = URL(string: imageThumb), let video = item.imageurl, let videoURL = URL(string: video) {
                    images.append(LightboxImage(imageURL: thumbURL, text: "", videoURL: videoURL))
                } else if let imageURLStr = item.imageurl, let imageURL = URL(string: imageURLStr) {
                    images.append(LightboxImage(imageURL: imageURL))
                }
            }
        }
        
        if !images.isEmpty {
            let controller = LightboxController(images: images)
            if #available(iOS 13.0, *) {
                controller.isModalInPresentation  = true
                controller.modalPresentationStyle = .fullScreen
            }
            // Set delegates.
            controller.pageDelegate = self
            controller.dismissalDelegate = self
            
            // Use dynamic background.
            controller.dynamicBackground = true
            
            // Present your controller.
            present(controller, animated: true, completion: nil)
            
            LightboxConfig.handleVideo = { from, videoURL in
                // Custom video handling
                let videoController = AVPlayerViewController()
                videoController.player = AVPlayer(url: videoURL)
                
                from.present(videoController, animated: true) {
                    videoController.player?.play()
                }
            }
        }
    }
}

extension LocalBusinessDetailViewController: LightboxControllerPageDelegate {
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension LocalBusinessDetailViewController: LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
    }
}

extension LocalBusinessDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewWebsite {
            return biAddRequest?.busi_weblink_list?.count ?? 0
        } else if tableView == tableViewSocialMedia {
            return biAddRequest?.busi_sociallink_list?.count ?? 0
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewWebsite {
            let cell = tableView.dequeueReusableCell(withIdentifier: WebsiteListCell.nameOfClass) as! WebsiteListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let data = biAddRequest?.busi_weblink_list?[indexPath.row]
            cell.lblWebsiteIndex.text = "Website \(indexPath.row + 1)"
            cell.lblLink.text = data?.webLinkTitle ?? ""
            cell.lblDesc.text = data?.webLinkDesc ?? ""
            
            return cell
        } else if(tableView == tableViewSocialMedia) {
            let cell = tableView.dequeueReusableCell(withIdentifier: WebsiteListCell.nameOfClass) as! WebsiteListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let data = biAddRequest?.busi_sociallink_list?[indexPath.row]
            cell.lblWebsiteIndex.text = "Social Media \(indexPath.row + 1)"
            cell.lblMainWebsiteStatus.text = data?.socialMediaName ?? ""
            cell.lblLink.text = data?.webLinkTitle ?? ""
            cell.lblDesc.text = data?.webLinkDesc ?? ""
            
            return cell
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewWebsite {
            if let link: String = biAddRequest?.busi_weblink_list?[indexPath.row].webLinkTitle, !link.isEmpty {
                openLink(for: link)
            }
        } else if tableView == tableViewSocialMedia {
            if let link: String = biAddRequest?.busi_sociallink_list?[indexPath.row].webLinkTitle, !link.isEmpty {
                openLink(for: link)
            }
        }
    }
}

extension LocalBusinessDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return biAddRequest?.mediabusiness?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath) as! SerivceImageCell
        cell.delegate =  self
        cell.cellIndex = indexPath.row
        
        let data = biAddRequest?.mediabusiness?[indexPath.row]
        if let thumb = data?.thumb, let url = URL(string: thumb) {
            cell.imgService.sd_setImage(with: url)
        }
        cell.imgPlay.isHidden = data?.media_extension != HelperConstant.MediaExtension.Video.MediaExtensionText
        
        return cell
    }
}

extension LocalBusinessDetailViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) { }
}

extension LocalBusinessDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:100, height: 80)
    }
}

extension LocalBusinessDetailViewController: ProductCellDelegate {
    func methodShowProductDetails(cellIndex: Int) {
        if let mediaItem = biAddRequest?.mediabusiness?[cellIndex], let thumb = mediaItem.thumb, let url = URL(string: thumb) {
            imgPlayBtn.isHidden = mediaItem.media_extension != HelperConstant.MediaExtension.Video.MediaExtensionText
            imgProfile.sd_setImage(with: url)
        }
    }
}

extension LocalBusinessDetailViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        switch tableView {
        case tableViewWebsite: constraintTableViewWebsiteHeight.constant = size.height
        case tableViewSocialMedia: constraintTableViewSocialMediaHeight.constant = size.height
        default:
            break
        }
    }
}

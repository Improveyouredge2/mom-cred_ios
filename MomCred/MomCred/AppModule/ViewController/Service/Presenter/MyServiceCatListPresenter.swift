//
//  MyServiceCatListPresenter.swift
//  MomCred
//
//  Created by consagous on 04/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  ServiceAddExternalLinkPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class MyServiceCatListPresenter {
    
    var view:MyServiceCatListViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: MyServiceCatListViewController) {
        self.view = view
    }
}

extension MyServiceCatListPresenter{
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getServiceCat(){
        
        ServiceService.getServiceCategory(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        self.view.myServiceList = response?.data ?? []
                    }
                    
                    self.view.lblNoRecordFound.isHidden = true
                    self.view.tbCatServiceList.isHidden = false
                    self.view.lblTitle.isHidden = false

                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.lblNoRecordFound.text = message ?? ""
                    self.view.lblNoRecordFound.isHidden = false
                    self.view.tbCatServiceList.isHidden = true
                    self.view.lblTitle.isHidden = true
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
}

//
//  ServiceAddAdditionalInformationDocumentPresenter.swift
//  MomCred
//
//  Created by consagous on 23/08/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  ServiceAddAdditionalInformationDocumentPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddAdditionalInformationDocumentPresenter {
    
    var view:ServiceAddAdditionalInformationDocumentViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ServiceAddAdditionalInformationDocumentViewController) {
        self.view = view
    }
}

extension ServiceAddAdditionalInformationDocumentPresenter{
    
    /**
     *  Delete Media Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func deleteMediaData(deleteMediaRequest:DeleteMediaRequest?,  callback:@escaping (_ status:Bool, _ response: DeleteMediaModelResponse?, _ message: String?) -> Void){
        
        BIServiceStep1.deleteMediaInfo(requestData: deleteMediaRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(businessAdditionalDocumentInfo:MultipleAdditionalDocumentInfo?, imageList:[UploadImageData]?,  callback:@escaping (_ status:Bool, _ response: ImageModelResponse?, _ message: String?) -> Void){
        
        ServiceService.updateAdditionalInfoData(businessAdditionalDocumentInfo: businessAdditionalDocumentInfo, imageList: imageList, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
}

//
//public function multiuploadimage_post()
//{
//    $arg = array();
//    $version_result = version_check_helper1();
//    if($version_result['status'] != 1 )
//    {
//        $arg = $version_result;
//    }
//    else
//    {
//        $result = check_authorization();
//        if($result != 'true')
//        {
//            $arg['status']  = STATUS_AUTHORIZATION_CODE;
//            $arg['error']   = ERROR_AUTHORIZATION_CODE;
//            $arg['error_code']   = REST_Controller::HTTP_UNAUTHORIZED;
//            $arg['error_line']= __line__;
//            $arg['message'] = $result;
//        }
//        else
//        {
//            $this->form_validation->set_rules('busi_id', 'Business', 'required');
//            if ($this->form_validation->run() == FALSE)
//            {
//                $arg['status']  = 0;
//                $arg['error_code']   = ERROR_FAILED_CODE;
//                $arg['error_line']= __line__;
//                $arg['message'] = get_form_error($this->form_validation->error_array());
//            }
//            else
//            {
//                $time=time();
//                $auth_token = $this->input->get_request_header('Authorization');
//                $user_token = json_decode(base64_decode($auth_token));
//                $usid =  $user_token->userid;
//                $loguser = $this->dynamic_model->get_user_by_id($usid);
//                
//                $busi_id = $this->input->post('busi_id');
//                $media_service = !empty($this->input->post('media_service')) ? $this->input->post('media_service') : "";
//                $newuserid = '';
//                
//                $count = sizeof($this->input->post('media_title'));
//                for ($i = 0;  $i < $count ; $i++) {
//                    if ($_FILES) {
//                        //print_r($this->input->post('media_title')[$i]);
//                        $title = $this->input->post('media_title')[$i];
//                        $desc = $this->input->post('media_desc')[$i];
//                        $countimg = $this->input->post('count')[$i];
//                        $arr_count = array_sum($this->input->post('count'));
//                        $docNamevi = '';
//                        
//                        $val = 0;
//                        if ($i == 0) {
//                            $start = $val;
//                            $to = $countimg;
//                            
//                        } elseif ($i == 1) {
//                            $start = $val+$countimg;
//                            $to = $arr_count;
//                            
//                        }
//                        
//                        for ($j = $start;  $j < $to ; $j++) {
//                            if(!empty($_FILES["doc_img"]["name"][$j])) {
//                                $target_filedocvi = $_FILES["doc_img"]["name"][$j];
//                                $imageFileTypedocvi = pathinfo($target_filedocvi,PATHINFO_EXTENSION);
//                                $docNamevi = 'VIMG_'.$j.time().'.'.$imageFileTypedocvi;
//                                move_uploaded_file($_FILES['doc_img']['tmp_name'][$j], "uploads/document/" . $docNamevi);
//                                $userdata = array(
//                                'media_title'      => $title,
//                                'media_desc'      => $desc,
//                                'media_name'      => $docNamevi,
//                                'media_business' => $busi_id,
//                                'media_service'  => $media_service,
//                                'media_extension'=> 1,
//                                'media_category' => 2,
//                                'media_snap' => $i,
//                                'media_date'     => $time
//                                );
//                                $newuserid = $this->dynamic_model->insertdata('media',$userdata);
//                                $userdataget[] = $userdata;
//                            }
//                        }
//                        
//                    }
//                }
//                
//                $msg="Image uploaded";
//                if ($newuserid) {
//                    $imgarr = $userdataget;
//                    $arg['status']  = 1;
//                    $arg['error_code']  = REST_Controller::HTTP_OK;
//                    $arg['error_line']= __line__;
//                    $arg['data']=$imgarr;
//                    $arg['message'] = $msg;
//                } else {
//                    $arg['status']     = 0;
//                    $arg['error_code']  = ERROR_FAILED_CODE;
//                    $arg['error_line']= __line__;
//                    $arg['message'] = "Media not uploaded!";
//                }
//                
//            }
//            
//        }
//    }
//    echo json_encode($arg);
//}

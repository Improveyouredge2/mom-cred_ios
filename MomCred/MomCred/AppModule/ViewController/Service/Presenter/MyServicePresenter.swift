//
//  MyServicePresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  MyServicePresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class MyServicePresenter {
    
    var view:MyServiceViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: MyServiceViewController) {
        self.view = view
    }
}

extension MyServicePresenter{
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getServiceProviderDetail(){
        
        ServiceService.getServiceProviderDetail(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        self.view.myServiceList = response?.data ?? []
                    }
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
    
    /**
     *  Get sub cat service provider detail list.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getSubCatServiceProviderList(subCatId:String?){
        
        let getListingCategoryModelRequest = GetListingCategoryModelRequest(listing_id: subCatId)
        
        ServiceService.getServiceByCategory(requestData: getListingCategoryModelRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        self.view.myServiceList = response?.data ?? []
                    }
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
}

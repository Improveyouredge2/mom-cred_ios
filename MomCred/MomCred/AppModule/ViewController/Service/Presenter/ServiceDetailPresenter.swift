//
//  ServiceDetailPresenter.swift
//  MomCred
//
//  Created by Rajesh Yadav on 24/02/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import Foundation

/**
 *  ServiceDetailPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceDetailPresenter {
    
    var view:ServiceDetailView! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ServiceDetailView) {
        self.view = view
    }
}

extension ServiceDetailPresenter{
    /**
     *  Get sub cat service provider detail list.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getSubCatServiceProviderList(subCatId:String?){
        
        let serviceDetail = ServiceDetail(service_id: subCatId)
        
        ServiceService.getServiceDetail(requestData: serviceDetail, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil){
//                        self.view.myServiceList = response?.data ?? []
                        self.view.serviceAddRequest = response?.data
                        self.view.updateScreenInfo()
                    }
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
}

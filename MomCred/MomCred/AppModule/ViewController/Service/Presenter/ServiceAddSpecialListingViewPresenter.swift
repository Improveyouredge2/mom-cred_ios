//
//  ServiceAddSpecialListingViewPresenter.swift
//  MomCred
//
//  Created by consagous on 14/08/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  ServiceAddSpecialListingViewPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddSpecialListingViewPresenter {
    
    var view:ServiceAddSpecialListingViewPresenter! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ServiceAddExceptionalViewController) {
        self.view = view
    }
}

extension ServiceAddExceptionalPresenter{
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverChildListingRequest(parentId:String){
        
        let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        BIServiceStep1.getChildListing(requestData: childListingRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    
                    
                    if(response?.dataList != nil && (response?.dataList?.count)! > 0){
                        for parentList in self.view.exceptional_classification!{
                            if((response?.dataList?.count)! > 0 && response?.dataList?[0].listing_parent?.caseInsensitiveCompare(parentList.listing_id ?? "0") == ComparisonResult.orderedSame){
                                parentList.childListing = response?.dataList
                                break
                            }
                        }
                    }
                    
                    // check all child list complete
                    var isFound = true
                    for parentList in self.view.exceptional_classification!{
                        
                        if(parentList.childListing == nil){
                            isFound = false
                            break
                        }
                    }
                    
                    if(isFound){
                        Helper.sharedInstance.hideProgressHudView()
                        self.view.updateExceptionalTableList()
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Helper.sharedInstance.hideProgressHudView()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: ServiceAddResponse?, _ message: String?) -> Void){
        
        ServiceService.updateData(serviceAddRequest: view.serviceAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Helper.sharedInstance.hideProgressHudView()
                    
                    // Upload image in background
                    //                    Helper.sharedInstance.uploadBIPendingImages()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Helper.sharedInstance.hideProgressHudView()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
}

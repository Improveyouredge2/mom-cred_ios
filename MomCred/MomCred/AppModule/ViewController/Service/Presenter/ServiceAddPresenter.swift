//
//  ServiceAddPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  ServiceAddPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddPresenter {
    
    var view:ServiceAddOverviewViewController! // Object of account view screen
    
    static var parentListingList:ParentListingResponse?
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ServiceAddOverviewViewController) {
        self.view = view
    }
}

extension ServiceAddPresenter{
    /**
     *  Get Parent listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverParentListingRequest(){
        
        BIServiceStep1.getParentListing(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    ServiceAddPresenter.parentListingList = response
                    
                    self.view.updateScreenListing()
                    self.view.isScreenDataReload = false
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                }
            }
        })
    }
    
    /**
     *  Get Business location from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverBusiLocationRequest(){
        
        PersonnelService.getBusiLocation(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    if(response != nil && response?.data != nil){
                        self.view.busiLocation = response?.data
                    } else {
                        self.view.busiLocation = nil
                    }
                    
                    self.view.updateLocationInfo()
                    self.view.isScreenDataReload = false
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                }
            }
        })
    }
    
    /**
     *  Create Service information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: ServiceAddResponse?, _ message: String?) -> Void){
        
        ServiceService.updateData(serviceAddRequest: view.serviceAddRequest, imageList: nil, callback: { [weak self] (status, response, message) in
            guard let self = self else { return }
            
            if status {
                DispatchQueue.main.async() {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0 , execute: {
                        Spinner.hide()
                    })

                    if let serviceId = response?.data?.service_id?.stringValue {
                        UserDefault.saveSID(id: serviceId)
                        self.view.serviceAddRequest?.service_id = serviceId
                        UserDefault.saveServiceInfo(userInfo: NSDictionary(dictionary: self.view?.serviceAddRequest?.toJSON() ?? ["":""]))

                        let folderPref = "service_\(serviceId)"
                        var isVideo = false

                        for imageData in self.view.arrDocumentImages {
                            if imageData.imageUrl == nil, !imageData.status, let fileData = imageData.imageData {
                                if imageData.imageType == "2", let fileName = imageData.imageName?.split(separator: "/").last {
                                    isVideo = true
                                    // save video file
                                    _ = PMFileUtils.saveFileAtLocalPath(fileData: fileData as NSData, fileName: String(fileName), folderName: folderPref)
                                    // save thumbnail
                                    if let thumbData = imageData.imageThumbnailData {
                                        let thumbName = "thumb_\(fileName.split(separator: ".").first ?? "").png"
                                        _ = PMFileUtils.saveFileAtLocalPath(fileData: thumbData as NSData, fileName: thumbName, folderName: folderPref)
                                    }
                                    
                                    // Upload image in background
                                    Helper.sharedInstance.uploadBIPendingImages()
                                    imageData.status = true
                                } else if imageData.imageType == "1" {
                                    let randomString = "\(Date().timeIntervalSince1970)".split(separator: ".").last ?? "\(arc4random())"
                                    let imageName = "uploadImage_\(randomString).jpg"
                                    _ = PMFileUtils.saveFileAtLocalPath(fileData: fileData as NSData, fileName: imageName, folderName: folderPref)
                                    imageData.status = true
                                }
                            }
                            // Upload image in background
                            if(!isVideo){
                                Helper.sharedInstance.uploadBIPendingImages()
                            }
                        }
                    }

                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
    
    /**
     *  Delete Media Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func deleteMediaData(deleteMediaRequest:DeleteMediaRequest?,  callback:@escaping (_ status:Bool, _ response: DeleteMediaModelResponse?, _ message: String?) -> Void){
        
        BIServiceStep1.deleteIndividualMediaInfo(requestData: deleteMediaRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
}


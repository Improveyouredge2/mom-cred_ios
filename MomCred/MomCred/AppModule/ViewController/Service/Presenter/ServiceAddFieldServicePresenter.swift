//
//  ServiceAddFieldServicePresenter.swift
//  MomCred
//
//  Created by consagous on 21/08/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  ServiceAddFieldServicePresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddFieldServicePresenter {
    
    var view:ServiceAddFieldServiceViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ServiceAddFieldServiceViewController) {
        self.view = view
    }
}

extension ServiceAddFieldServicePresenter{
    
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverChildListingRequest(parentId:String, tagIndex:Int){
        
        let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        BIServiceStep1.getChildListing(requestData: childListingRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    self.view.updateSpecificFeildInfo(response: response?.dataList, tagIndex: tagIndex)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: ServiceAddResponse?, _ message: String?) -> Void){
        
        ServiceService.updateData(serviceAddRequest: view.serviceAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
}

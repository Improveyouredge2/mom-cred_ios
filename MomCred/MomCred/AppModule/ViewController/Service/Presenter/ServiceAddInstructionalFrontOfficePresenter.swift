//
//  ServiceAddInstructionalFrontOfficePresenter.swift
//  MomCred
//
//  Created by consagous on 19/08/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  ServiceAddPhysicalOccupationalTherapyPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddInstructionalFrontOfficePresenter {
    
    var view:ServiceAddInstructionalFrontOfficeViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ServiceAddInstructionalFrontOfficeViewController) {
        self.view = view
    }
}

extension ServiceAddInstructionalFrontOfficePresenter{
    
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverBatchChildListingRequest(parentId:String){
        
        let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        BIServiceStep1.getChildListing(requestData: childListingRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    
                    
                    if(response?.dataList != nil && (response?.dataList?.count)! > 0){
                        for parentList in self.view.serviceType!{
                            if((response?.dataList?.count)! > 0 && response?.dataList?[0].listing_parent?.caseInsensitiveCompare(parentList.listing_id ?? "0") == ComparisonResult.orderedSame){
                                parentList.subcat = response?.dataList
                                break
                            }
                        }
                    }
                    
                    // check all child list complete
                    var isFound = true
                    for parentList in self.view.serviceType!{
                        
                        if(parentList.subcat == nil){
                            isFound = false
                            break
                        }
                    }
                    
                    if(isFound){
                        Spinner.hide()
                        self.view.updateServiceTypeTableList()
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: ServiceAddResponse?, _ message: String?) -> Void){
        
        ServiceService.updateData(serviceAddRequest: view.serviceAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
    
    
    /**
     *  Get Personal service list from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getFacilityServiceList(){
        
        MyFacilitiesService.getFacilityService(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        
                        let facilityList = response?.data ?? []
                        
                        //                        let data = facilityList.filter({$0.facility_id != self.view.serviceAddRequest?.service_id}).map({ServiceListingList(id: $0.facility_id ?? "", title: $0.facility_name ?? "")})
                        
                        if(facilityList.count > 0){
                            self.view.faciliteGainList = facilityList.map({
                                ServiceListingList(id: $0.facility_id ?? "", title: $0.facility_name ?? "")})
                            self.view.faciliteRentalList = facilityList.map { ServiceListingList(id: $0.facility_id ?? "", title: $0.facility_name ?? "") }
                        }
                    }
                    
                    if(self.view.btnDirectMemberShipYes.isSelected){
                        self.view.viewFacilityGain.isHidden = false
                        if(self.view.faciliteGainList != nil && (self.view.faciliteGainList?.count)! > 0){
                            self.view.constraintFaciliteGainHeight.constant = 10
                        }
                        self.view.tableViewFaciliteGain.reloadData()
                    }
                    
                    self.view.tableViewFacilityRental.reloadData()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    //                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getServiceProviderList(){
        
        ServiceService.getServiceProviderDetail(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        let serviceList = response?.data ?? []
                        
                        if(serviceList.count > 0){
                            
                            let data = serviceList.filter({$0.service_id != self.view.serviceAddRequest?.service_id}).map({ServiceListingList(id: $0.service_id ?? "", title: $0.service_name ?? "")})
                            
                            //                                self.view.serviceList = data
                            self.view.serviceGainList = data
                        }
                        
                    }
                    
                    if(self.view.btnDirectMemberShipYes.isSelected){
                        if(self.view.serviceGainList != nil && (self.view.serviceGainList?.count)! > 0){
                            self.view.constraintServiceGainHeight.constant = 10
                        }
                        
                        self.view.tableViewServiceGain.reloadData()
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    //                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    //
                    //                    self.view.updateScreenInfo()
                }
            }
        })
    }
}

//
//  ServiceAddListingServicePresenter.swift
//  MomCred
//
//  Created by consagous on 23/08/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  ServiceAddListingContentAssociatedPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddListingServicePresenter {
    
    var view:ServiceAddListingServiceViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ServiceAddListingServiceViewController) {
        self.view = view
    }
}

extension ServiceAddListingServicePresenter{
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: ServiceAddResponse?, _ message: String?) -> Void){
        
        ServiceService.updateData(serviceAddRequest: view.serviceAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getServiceProviderList(){
        
        ServiceService.getServiceProviderDetail(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        let serviceList = response?.data ?? []
                        
                        if(serviceList.count > 0){
                            
                            let data = serviceList.filter({$0.service_id != self.view.serviceAddRequest?.service_id}).map({ServiceListingList(id: $0.service_id ?? "", title: $0.service_name ?? "")})
                            
                            self.view.serviceList = data
                        }
                        
                    }
                    self.view.tableViewSpecificTarget.reloadData()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
//
//                    self.view.updateScreenInfo()
                }
            }
        })
    }
}

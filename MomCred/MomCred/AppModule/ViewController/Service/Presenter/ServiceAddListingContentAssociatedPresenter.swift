//
//  ServiceAddListingContentAssociatedPresenter.swift
//  MomCred
//
//  Created by consagous on 22/08/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  ServiceAddListingContentAssociatedPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddListingContentAssociatedPresenter {
    
    var view:ServiceAddListingContentAssociatedViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ServiceAddListingContentAssociatedViewController) {
        self.view = view
    }
}

extension ServiceAddListingContentAssociatedPresenter{
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: ServiceAddResponse?, _ message: String?) -> Void){
        
        ServiceService.updateData(serviceAddRequest: view.serviceAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
    
    /**
     *  Get Personal service list from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getPersonalServiceList(){
        
        PersonnelService.getPersonnelService(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        let personnelList = response?.data ?? []
                        
                        if(personnelList.count > 0){
                            self.view.personnelList = personnelList.map({ServiceListingList(id: $0.personal_id ?? "", title: $0.personal_name ?? "")})
                        }
                    }
                    
                    self.view.tableViewPersonnelService.reloadData()
                    
                    //                    self.view.refreshScr()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    //                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
        
    /**
     *  Get Personal service list from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getFacilityServiceList(){
        
        MyFacilitiesService.getFacilityService(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        
                        let facilityList = response?.data ?? []
                        
//                        let data = facilityList.filter({$0.facility_id != self.view.serviceAddRequest?.service_id}).map({ServiceListingList(id: $0.facility_id ?? "", title: $0.facility_name ?? "")})
                        
                        if(facilityList.count > 0){
                            self.view.facilityList = facilityList.map({
                                ServiceListingList(id: $0.facility_id ?? "", title: $0.facility_name ?? "")})
                        }
                    }
                    
                    self.view.tableViewFacilityService.reloadData()
                    
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    //                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

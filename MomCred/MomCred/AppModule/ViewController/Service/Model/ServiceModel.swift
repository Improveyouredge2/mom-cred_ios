//
//  ServiceModel.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

class ServiceListingList{
    var id:String? = ""
    var title:String? = ""
    var selectionStatus:Bool = false
    
    init(id : String? , title : String?) {
        self.id =   id
        self.title = title
    }
}

class ServiceDetail: Mappable{
    required init?(map: Map) {
        
    }
    
    var service_id:String? = ""
    
    
    init(service_id : String?) {
        self.service_id =   service_id
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        service_id                  <- map["service_id"]
    }
}

class ServiceModelInfo{
    var exclusiveTravel = false
    var locationType = false
    var locationBelong = false
    
    var locationName = ""
    var locationAddress = ""
    var locationState = ""
    var locationCity = ""
    
    var locationClassification:[String] = []
    var locationSpeciality:[String] = []
    
    var busniessHour:[BusniessHour] = [BusniessHour(),BusniessHour(),BusniessHour(),BusniessHour(),BusniessHour(),BusniessHour(),BusniessHour()]
}

///********************************/
////MARK:- Serialize Request
///*******************************/

/**
 *  ServiceAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class ServiceAddRequest: Mappable {
    
    var service_id:String?
    var service_user:String?
    var service_desc: String?
    
    // Page 1
    var service_name:String?
    var purchasablity:String?
    var credit_system:String?
    var number_of_purchase:String?
    var service_target_goal:[String]?
    var service_target_goal_title:[String]?
    var service_location_info:PersonnelBusiLocationResponseData?
    var service_location:String?
    
    // Page 2
    var service_calendar:ServiceCalendar?
    
    // Page 3
    var service_information:ServiceInformation?
    
    // Page 4
    var service_certificate:ServiceCertificate?
    
    // Page 5
    var service_therapy:ServiceTherapy?
    
    // Page 6
    var service_instructor:ServiceInstructor?
    
    // Page 7
    var service_event:ServiceEvent?
    
    // Page 8
    var service_specific:ServiceSpecific?
    
    // Page 9
    var service_specific_target:ServiceSpecificTarget?
    
    // Page 10
    var service_field:[BusinessFieldServiceCategoryInfo]?
    
    // Page 11
    var service_price:ServicePrice?
    
    // Page 12
    var service_skill:ServiceSkill?
    
    // Page 13
    var service_listing:ServiceListing?
    
    // Page 13
    var service_insidelook: [ServiceInsideLookItem]?

    // Page 13
    var service_instructionalcontent: [ServiceInsideLookItem]?
    
    // this is an extra/duplicate key just to save data back to service, because andoid can't send the data with long key names
    var service_instruction: [ServiceInsideLookItem]?

    // Page 14
    var service_other_good_services:ServiceOtherGoodServices?
    
    // Page 15
    var service_add_notes:ServiceAddNotes?
    
    // Page 16
    var service_external_link:ServiceExternalLink?
    
    // Page 17
    var service_prepage_requirement:[ServicePrepageRequirement]?
    
    // Page 18
    
    // For business detail display
    var doc:[BusinessAdditionalDocumentInfo]?
    var mediabusiness:[BusinessAdditionalDocumentInfo]?
    var status:String? = "0"
    
    var pageid:NSNumber?
    
    // Enthusiast
    var busitype:String?
    var provider_image:String?
    var rating:NSNumber?
    var busi_id:String?
    var busi_title:String?
    
    // donation values
    var donationamount: String?
    var service_donation: String?

    var provider_name: String?
    // For static use
    var donationPer:Float?
    var donationAmt:Float?
    
    var creditpercent:String?
    var total_purchase: String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init() {
//        super.init()
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
//        super.init(map: map)
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceAddRequest?{
        var serviceAddRequest:ServiceAddRequest?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceAddRequest>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceAddRequest = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceAddRequest>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceAddRequest = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceAddRequest ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
//        super.mapping(map: map)
        
        service_id                  <- map["service_id"]
        service_user                <- map["service_user"]
        service_desc                <- map["service_desc"]

        //Page 1
        service_name                    <- map["overview"]
        purchasablity               <- map["purchasablity"]
        credit_system               <- map["credit_system"]
        number_of_purchase          <- map["number_of_purchase"]
        service_target_goal         <- map["service_target_goal"]
        service_target_goal_title   <- map["service_target_goal_title"]
        service_location_info       <- map["service_location_info"]
        service_location            <- map["service_location"]
        provider_name               <- map["provider_name"]
        //Page 2
        service_calendar            <- map["service_calendar"]
        
        // Page 3
        service_information         <- map["service_information"]
        
        // Page 4
        service_certificate         <- map["service_certificate"]
        
        // Page 5
        service_therapy             <- map["service_therapy"]
        
        // Page 6
        service_instructor          <- map["service_instructor"]
        
        // Page 7
        service_event               <- map["service_event"]
        
        // Page 8
        service_specific            <- map["service_specific"]
        
        // Page 9
        service_specific_target     <- map["service_specific_target"]
        
        // Page 10
        service_field               <- map["service_field"]
        
        // Page 11
        service_price               <- map["service_price"]
        
        // Page 12
        service_skill               <- map["service_skill"]
        
        // Page 13
        service_listing             <- map["service_listing"]
        
        service_insidelook           <- map["service_insidelook"]
        service_instructionalcontent <- map["service_instructionalcontent"]
        service_instruction        <- map["service_instruction"]

        // Page 14
        service_other_good_services <- map["service_other_good_services"]
        
        // Page 15
        service_add_notes           <- map["service_add_notes"]
        
        // Page 16
        service_external_link       <- map["service_external_link"]
        
        // Page  only in response, For business detail
        doc                             <- map["externalmedia"]
        
        // Page 17
        service_prepage_requirement <- map["service_prepage_requirement"]
        
        // For business detail
        mediabusiness                   <- map["service_media"]
        
        pageid                          <- map["pageid"]
        
        // In-reponse
        status                          <- map["status"]
        
        // donation
        if let donationAmt: String = map["donationamount"].value() {
            donationamount = donationAmt
        } else if let donationAmt: Int = map["donationamount"].value() {
            donationamount = "\(donationAmt)"
        }
        service_donation    <- map["service_donation"]
        
        // Enthusiast
        busitype        <- map["busitype"]
        provider_image  <- map["provider_image"]
        rating          <- map["rating"]
        busi_id         <- map["busi_id"]
        busi_title      <- map["busi_title"]
        creditpercent   <- map["creditpercent"]
        total_purchase  <- map["total_purchase"]
    }
}

//
class ServiceCalendar: Mappable{
    var date:String? = ""
    var time:String? = ""
    var repeatStatus:String? = ""
    var repeatTimeFrame:String? = ""
    var repeatTimeCount:String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceCalendar?{
        var serviceCalendar:ServiceCalendar?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceCalendar>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceCalendar = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceCalendar>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceCalendar = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceCalendar ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
//        name    <- map["name"]
        
        date            <- map["date"]
        time            <- map["time"]
        repeatStatus    <- map["repeatStatus"]
        repeatTimeFrame <- map["repeatTimeFrame"]
        repeatTimeCount <- map["repeatTimeCount"]
    }
}

//
class ServiceInformation: Mappable{
    var stdServices:String? = ""
    var exceptionalServices:[DetailData]? = []
    var listDisabilities:String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceInformation?{
        var serviceInformation:ServiceInformation?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceInformation>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceInformation = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceInformation>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceInformation = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceInformation ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        stdServices         <- map["stdServices"]
        exceptionalServices <- map["exceptionalServices"]
        listDisabilities    <- map["listDisabilities"]
    }
}

class DetailData: Mappable{
    var id:String? = ""
    var title:String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->DetailData?{
        var detailData:DetailData?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<DetailData>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    detailData = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<DetailData>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        detailData = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return detailData ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        id              <- map["id"]
        title           <- map["title"]
    }
}

//
class ServiceCertificate: Mappable{
    var certificateName:String? = ""
    var freeService:String? = ""
    var trailService:String? = ""
    var travelServiceTypeId:String? = ""
    var travelServiceTypeTitle:String? = ""
    var travelServiceReq:String? = ""
    var travelLocationId:String? = ""
    var travelLocationTitle:String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceCertificate?{
        var serviceCertificate:ServiceCertificate?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceCertificate>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceCertificate = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceCertificate>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceCertificate = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceCertificate ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        certificateName         <- map["certificateName"]
        freeService             <- map["freeService"]
        trailService            <- map["trailService"]
        travelServiceTypeId     <- map["travelServiceTypeId"]
        travelServiceTypeTitle  <- map["travelServiceTypeTitle"]
        travelServiceReq        <- map["travelServiceReq"]
        travelLocationId        <- map["travelLocationId"]
        travelLocationTitle     <- map["travelLocationTitle"]
        
    }
}

//
class ServiceTherapy: Mappable{
//    var therapyTypeId:[String? = ""
//    var therapyTypeTitle:String? = ""
    var therapyType:[DetailData]? = []
    var therapyFieldCover:String? = ""
//    var paymentTypeId:String? = ""
//    var paymentTypeTitle:String? = ""
    var paymentType:DetailData?
    var acceptedInsuancePlan:String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceTherapy?{
        var serviceTherapy:ServiceTherapy?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceTherapy>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceTherapy = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceTherapy>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceTherapy = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceTherapy ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
//        therapyTypeId           <- map["therapyTypeId"]
//        therapyTypeTitle        <- map["therapyTypeTitle"]
        therapyType             <- map["therapyType"]
        therapyFieldCover       <- map["therapyFieldCover"]
//        paymentTypeId           <- map["paymentTypeId"]
//        paymentTypeTitle        <- map["paymentTypeTitle"]
        paymentType             <- map["paymentType"]
        acceptedInsuancePlan    <- map["acceptedInsuancePlan"]
        
    }
}

//
class ServiceInstructor: Mappable{
    // instructional or front office is selected
    // values are // front_office / Instructional
    var inFo_office: String?

    // instructional office
    var instructionalTypeId:String?
    var instructionalTypeTitle:String?
    var instructionalTypeNumber:String?
    
    // front office
    var directServiceGainText: String?
    var additionalServiceOptionDesc: [String]?
    var facilitiesGainedDirectMembership: [DetailData]?
    var facilityRentalId: Int?
    var rental: String?
    var directServiceGain: String?
    var additionalServicePriceList: [FacilityRentalAddedCost]?
    var facilitiesGained: [DetailData]?
    var frontOfficeDirectMembership: String?
    var facilityRentalClassification: String?
    var requirementFacility: [String]?
    var requirementFacilityRentalList: [DetailData]?
    var serviceGained: [DetailData]?
    var priceInclude: [String]?
    var facilityRental: Int?
    var additionalServiceOptionName: [String]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceInstructor?{
        var serviceInstructor:ServiceInstructor?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceInstructor>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceInstructor = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceInstructor>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceInstructor = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceInstructor ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        // instructional office
        instructionalTypeId             <- map["instructionalTypeId"]
        instructionalTypeTitle          <- map["instructionalTypeTitle"]
        instructionalTypeNumber         <- map["instructionalTypeNumber"]
        
        // front office
        directServiceGainText               <- map["direct_service_gain_text"]
        additionalServiceOptionDesc         <- map["additionalServiceOptionDesc"]
        facilitiesGainedDirectMembership    <- map["facilitiesGaineddm"]
        facilityRentalId                    <- map["facilityRentalId"]
        inFo_office                         <- map["inFo_office"]
        rental                              <- map["rental"]
        directServiceGain                   <- map["direct_service_gain"]
        additionalServicePriceList          <- map["additionalServicePriceList"]
        facilitiesGained                    <- map["facilitiesGained"]
        frontOfficeDirectMembership         <- map["frontOfficeDirectMembership"]
        facilityRentalClassification        <- map["facilityRentalClassification"]
        requirementFacility                 <- map["requirement_facility"]
        requirementFacilityRentalList       <- map["requirementFacilityRentalList"]
        serviceGained                       <- map["serviceGained"]
        priceInclude                        <- map["price_include"]
        facilityRental                      <- map["facility_rental"]
        additionalServiceOptionName         <- map["additionalServiceOptionName"]
    }
}

extension Mappable {
    func getModelObjectFromServerResponse<T: BaseMappable>(jsonResponse: AnyObject) -> T? {
        var object: T?
        let mapper = Mapper<T>()

        if let tempDic = jsonResponse as? [String: Any] {
            if let testObject = mapper.map(JSON: tempDic){
                object = testObject
            }
        } else if let jsonString = jsonResponse as? String, let data = jsonString.data(using: .utf8) {
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    object = mapper.map(JSON: jsonResult)
                }
            } catch {
                print(error)
            }
        }
        return object
    }
    
    var debugDescription: String? {
        return toJSONString()
    }
}

class FacilityRentalAddedCost: Mappable {
    var name: String = ""
    var pricedesc: String = ""

    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        name        <- map["name"]
        pricedesc   <- map["pricedesc"]
    }
}

class AddedCostInfo: Mappable{
    var name:String = ""
    var desc:String = ""
    var price:String = ""
    var pricedesc:String = ""
    var status:Bool?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->AddedCostInfo?{
        var addedCostInfo:AddedCostInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<AddedCostInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    addedCostInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<AddedCostInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        addedCostInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return addedCostInfo ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        name        <- map["name"]
        desc        <- map["desc"]
        price       <- map["price"]
        pricedesc   <- map["pricedesc"]
        status      <- map["status"]
        
    }
}

//
class ServiceEvent: Mappable {
    var typeTitleId: String?
    var eventRadio: String?
    var typeTitle: String?
    var specificEventType: String?
    var causeCharitableEvent: String?
    var causeCharitablePercentage: String?
    var eventClassification: String?

    init() { }
    
    required init?(map: Map) { }

    func mapping(map: Map) {
        eventRadio                  <- map["event_radio"]
        eventClassification         <- map ["event_classification"]
        typeTitleId                 <- map["typeTitleId"]
        typeTitle                   <- map["typeTitle"]
        specificEventType           <- map["specificEventType"]
        causeCharitableEvent        <- map["causeCharitableEvent"]
        causeCharitablePercentage   <- map["causeCharitablePercentage"]
    }
}

//
class ServiceSpecific: Mappable{
    var desc:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceSpecific?{
        var serviceSpecific:ServiceSpecific?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceSpecific>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceSpecific = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceSpecific>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceSpecific = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceSpecific ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        desc                 <- map["desc"]
    }
}

//
class ServiceSpecificTarget: Mappable{
    var specificWorkTarget:[String]?
    var workDuringService:[String]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceSpecificTarget?{
        var serviceSpecificTarget:ServiceSpecificTarget?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceSpecificTarget>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceSpecificTarget = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceSpecificTarget>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceSpecificTarget = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceSpecificTarget ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        specificWorkTarget  <- map["specificWorkTarget"]
        workDuringService   <- map["workDuringService"]
    }
}

//
class ServicePrice: Mappable{
    var service_price:String?
//    var costOptionTitle:String?
//    var costOptionDesc:String?
//    var additionalPrice:String?
//    var additionalPriceDesc:String?
    var additionalCost :[AddedCostInfo]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServicePrice?{
        var servicePrice:ServicePrice?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServicePrice>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    servicePrice = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServicePrice>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        servicePrice = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return servicePrice ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        service_price        <- map["servicePrice"]
//        costOptionTitle     <- map["costOptionTitle"]
//        costOptionDesc      <- map["costOptionDesc"]
//        additionalPrice     <- map["additionalPrice"]
//        additionalPriceDesc <- map["additionalPriceDesc"]
        
        additionalCost      <- map["additionalCost"]
    }
}

//
class ServiceSkill: Mappable{
    var skillLevel:[DetailData]?
    var minAge:String?
    var maxAge:String?
    var durationId:String?
    var durationTitle:String?
    var durationTime:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceSkill?{
        var serviceSkill:ServiceSkill?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceSkill>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceSkill = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceSkill>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceSkill = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceSkill ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        skillLevel      <- map["skillLevel"]
        minAge          <- map["minAge"]
        maxAge          <- map["maxAge"]
        durationId      <- map["durationId"]
        durationTitle   <- map["durationTitle"]
        durationTime    <- map["durationTime"]
    }
}

// ServiceListing
class ServiceListing: Mappable{
    var fieldListing:[ServiceListData]?
    var personnelListing:[ServiceListData]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceListing?{
        var serviceListing:ServiceListing?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceListing>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceListing = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceListing>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceListing = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceListing ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        fieldListing        <- map["fieldListing"]
        personnelListing    <- map["personnelListing"]
    }
}

class ServiceListData: Mappable{
    var id:String? = ""
    var link:String? = ""
    var desc:String? = ""
    var roleDesc:String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceListData?{
        var serviceListData:ServiceListData?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceListData>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceListData = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceListData>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceListData = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceListData ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        id          <- map["id"]
        link        <- map["link"]
        desc        <- map["desc"]
        roleDesc    <- map["roleDesc"]
    }
}

//
class ServiceOtherGoodServices: Mappable{
    var service:[ServiceListData]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceOtherGoodServices?{
        var serviceOtherGoodServices:ServiceOtherGoodServices?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceOtherGoodServices>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceOtherGoodServices = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceOtherGoodServices>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceOtherGoodServices = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceOtherGoodServices ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        service        <- map["service"]
    }
}

class ServiceInsideLookItem: Mappable {
    var id:String? = ""
    var name:String? = ""
    var desc:String? = ""
    
    init() { }
    
    required init?(map: Map) { }
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> ServiceInsideLookItem? {
        var listData: ServiceInsideLookItem?
        
        if let response = jsonResponse as? [String: Any] {
            let mapper = Mapper<ServiceInsideLookItem>()
            if let testObject = mapper.map(JSON: response) {
                listData = testObject
            }
        } else if let response = jsonResponse as? String, let data = response.data(using: String.Encoding.utf8) {
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    let mapper = Mapper<ServiceInsideLookItem>()
                    if let testObject = mapper.map(JSON: jsonResult){
                        listData = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return listData ?? nil
    }

    func mapping(map: Map) {
        id          <- map["id"]
        name        <- map["name"]
        desc        <- map["desc"]
    }
}

//
class ServiceAddNotes: Mappable{
    var additionalNotes:[String]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceAddNotes?{
        var serviceAddNotes:ServiceAddNotes?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceAddNotes>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceAddNotes = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceAddNotes>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceAddNotes = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceAddNotes ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        additionalNotes        <- map["additionalNotes"]
    }
}

//
class ServiceExternalLink: Mappable{
    
    var serviceExternalLinkList:[ServiceExternalLinkData]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceExternalLink?{
        var serviceExternalLink:ServiceExternalLink?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceExternalLink>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceExternalLink = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceExternalLink>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceExternalLink = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceExternalLink ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        serviceExternalLinkList        <- map["serviceExternalLinkList"]
    }
}

class ServiceExternalLinkData: Mappable{
    var link: String?
    var desc:String?
    var name : String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceExternalLinkData?{
        var serviceExternalLinkData:ServiceExternalLinkData?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceExternalLinkData>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceExternalLinkData = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceExternalLinkData>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceExternalLinkData = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceExternalLinkData ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        name <- map["name"]
        link <- map["link"]
        desc <- map["description"]
        
        if link == nil {
            link <- map["name"]
        }
        if desc == nil {
            desc <- map["desc"]
        }
    }
}

//
class ServicePrepageRequirement: Mappable{
    var requirementType:String?
    var description:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServicePrepageRequirement?{
        var servicePrepageRequirement:ServicePrepageRequirement?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServicePrepageRequirement>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    servicePrepageRequirement = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServicePrepageRequirement>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        servicePrepageRequirement = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return servicePrepageRequirement ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        requirementType <- map["requirementType"]
        description     <- map["description"]
    }
}

///********************************/
////MARK:- Serialize Response
///*******************************/

/**
 *  ServiceResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class ServiceResponse : APIResponse {
    
    var data:[ServiceAddRequest]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceResponse?{
        var serviceResponse:ServiceResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
}

/**
 *  ServiceResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class ServiceDetailResponse : APIResponse {
    
    var data:ServiceAddRequest?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceDetailResponse?{
        var serviceDetailResponse:ServiceDetailResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceDetailResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceDetailResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceDetailResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceDetailResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceDetailResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
}

class ServiceAddResponse : APIResponse {
    
    var data:ServiceAddResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceAddResponse?{
        var serviceAddResponse:ServiceAddResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ServiceAddResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    serviceAddResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ServiceAddResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        serviceAddResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return serviceAddResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class ServiceAddResponseData: Mappable{
        var service_id : NSNumber?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ServiceAddResponseData?{
            var serviceAddResponseData:ServiceAddResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<ServiceAddResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        serviceAddResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ServiceAddResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            serviceAddResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return serviceAddResponseData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            service_id     <- map["service_id"]
            
        }
    }
}

//
//  ServiceAddSpecificGeneralDescriptionViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 02/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * ServiceAddSpecificGeneralDescriptionViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddSpecificGeneralDescriptionViewController : LMBaseViewController{
    
    fileprivate let formNumber = 8
    
    // Event
    @IBOutlet weak var textViewSpecificDesc: KMPlaceholderTextView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var serviceAddSpecificTargetGoalViewController:ServiceAddSpecificTargetGoalViewController?
    fileprivate var serviceSpecific:ServiceSpecific?
    fileprivate var presenter = ServiceAddSpecificGeneralDescriptionPresenter()
    
    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.serviceAddSpecificTargetGoalViewController = nil
    }
}
extension ServiceAddSpecificGeneralDescriptionViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        if(self.serviceAddRequest?.service_specific != nil){
            self.serviceSpecific = self.serviceAddRequest?.service_specific
            
            self.textViewSpecificDesc.text = self.serviceSpecific?.desc ?? ""
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if (self.textViewSpecificDesc.text.isEmpty){
            message = LocalizationKeys.error_specific_desc.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if(self.serviceAddRequest == nil){
            self.serviceAddRequest = ServiceAddRequest()
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        if(self.serviceSpecific == nil){
            self.serviceSpecific = ServiceSpecific()
        }
        
        self.serviceSpecific?.desc = self.textViewSpecificDesc.text ?? ""
        
        self.serviceAddRequest?.service_specific = self.serviceSpecific
    
        return true
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.serviceAddRequest?.service_specific != nil){
            
            if(self.serviceAddRequest?.service_specific?.desc != (self.textViewSpecificDesc.text ?? "")){
                isUpdate = true
            }
        } else {
            isUpdate = true
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.serviceAddSpecificTargetGoalViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddSpecificTargetGoalViewController.nameOfClass) as ServiceAddSpecificTargetGoalViewController
        
        self.serviceAddSpecificTargetGoalViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddSpecificTargetGoalViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddSpecificTargetGoalViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension ServiceAddSpecificGeneralDescriptionViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

//
//  ServiceAddSpecificTargetGoalViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 02/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * ServiceAddSpecificTargetGoalViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddSpecificTargetGoalViewController : LMBaseViewController{
    
    fileprivate let formNumber = 9
    
    // Event
    @IBOutlet weak var tableViewSpecificTarget: UITableView!
    @IBOutlet weak var tableViewWorkDuringService: UITableView!
    
    @IBOutlet weak var constraintstableViewSpecificTargetHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintstableViewWorkDuringServiceHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var specificTargetList:[String] = []
    fileprivate var workDuringServiceList:[String] = []
    fileprivate var serviceAddFieldServiceViewController:ServiceAddFieldServiceViewController?
    fileprivate var presenter = ServiceAddSpecificTargetGoalPresenter()
    fileprivate var serviceSpecificTarget:ServiceSpecificTarget?
    fileprivate var isUpdate = false
    
    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serviceAddFieldServiceViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView: UITableView) {
//        super.updateViewConstraints()
        
        if(self.tableViewSpecificTarget.contentSize.height > 0){
            tableViewSpecificTarget.scrollToBottom()
             if((self.specificTargetList.count) == 0){
                self.constraintstableViewSpecificTargetHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                
                self.constraintstableViewSpecificTargetHeight?.constant = self.tableViewSpecificTarget.contentSize.height
            }
        }
        
        if(self.tableViewWorkDuringService.contentSize.height > 0){
            tableViewWorkDuringService.scrollToBottom()
            if((self.workDuringServiceList.count) == 0){
                self.constraintstableViewWorkDuringServiceHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                self.constraintstableViewWorkDuringServiceHeight?.constant = self.tableViewWorkDuringService.contentSize.height
            }
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewSpecificTarget == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewSpecificTarget.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewSpecificTarget.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewSpecificTargetHeight.constant = heightOfTableView
            }
        } else if(tableViewWorkDuringService == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewWorkDuringService.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewWorkDuringService.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewWorkDuringServiceHeight.constant = heightOfTableView
            }
        }
    }
}

//MARK:- Button Action method implementation
extension ServiceAddSpecificTargetGoalViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        if(self.serviceAddRequest?.service_specific_target != nil){
            self.serviceSpecificTarget = self.serviceAddRequest?.service_specific_target
            
            self.specificTargetList = self.serviceSpecificTarget?.specificWorkTarget ?? []
            self.workDuringServiceList = self.serviceSpecificTarget?.workDuringService ?? []
            
            self.tableViewSpecificTarget.reloadData()
            self.tableViewWorkDuringService.reloadData()

        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        if(self.serviceAddRequest == nil){
            self.serviceAddRequest = ServiceAddRequest()
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        if(self.serviceSpecificTarget == nil){
            self.serviceSpecificTarget = ServiceSpecificTarget()
        }
        
        if(self.specificTargetList.count > 0){
            self.serviceSpecificTarget?.specificWorkTarget = self.specificTargetList
        } else {
            self.serviceSpecificTarget?.specificWorkTarget = nil
        }
        
        if(self.workDuringServiceList.count > 0){
            self.serviceSpecificTarget?.workDuringService = self.workDuringServiceList
        } else {
            self.serviceSpecificTarget?.workDuringService = nil
        }
        
        self.serviceAddRequest?.service_specific_target = self.serviceSpecificTarget
        
        return true
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.serviceAddRequest?.service_specific != nil){
            isUpdate = self.isUpdate
        } else {
            isUpdate = true
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.serviceAddFieldServiceViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddFieldServiceViewController.nameOfClass) as ServiceAddFieldServiceViewController
        
        self.serviceAddFieldServiceViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddFieldServiceViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddFieldServiceViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension ServiceAddSpecificTargetGoalViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddSpecificTargetGoalViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tableViewSpecificTarget){
//            if((self.specificTargetList.count) != HelperConstant.minimumBlocks){
                return (specificTargetList.count) + 1
//            } else {
//                return specificTargetList.count
//            }
        } else if(tableView == tableViewWorkDuringService){
//            if((self.workDuringServiceList.count) != HelperConstant.minimumBlocks){
                return (workDuringServiceList.count) + 1
//            } else {
//                return workDuringServiceList.count
//            }
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints(tableView: tableView)
        
        if(self.tableViewSpecificTarget == tableView){
            
            if(indexPath.row == 0){
                
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                
                cell.textField.placeholder = "Target Goals"
                cell.delegate = self
                cell.refTableView = self.tableViewSpecificTarget
                
                cell.backgroundColor = UIColor.clear
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                
                cell.delegate = self
                //                cell.cellIndex = indexPath
                cell.refTableView = self.tableViewSpecificTarget
                
//                if(self.specificTargetList.count) != HelperConstant.minimumBlocks{
                    cell.lblTitle.text = specificTargetList[indexPath.row-1]
                    
                    cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
//                } else {
//                    cell.cellIndex = indexPath
//                    cell.lblTitle.text = specificTargetList[indexPath.row]
//                }
//
                cell.backgroundColor = UIColor.clear
                return cell
            }
            
        } else if(self.tableViewWorkDuringService == tableView){
            
            if(indexPath.row == 0){
                
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                
                cell.textField.placeholder = "Work Service"
                cell.delegate = self
                cell.refTableView = self.tableViewWorkDuringService
                
                cell.backgroundColor = UIColor.clear
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                
                cell.delegate = self
                //                cell.cellIndex = indexPath
                cell.refTableView = self.tableViewSpecificTarget
                
//                if(self.workDuringServiceList.count) != HelperConstant.minimumBlocks{
                    cell.lblTitle.text = workDuringServiceList[indexPath.row-1]
                    
                    cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
//                } else {
//                    cell.cellIndex = indexPath
//                    cell.lblTitle.text = workDuringServiceList[indexPath.row]
//                }
                
                cell.backgroundColor = UIColor.clear
                return cell
            }
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension ServiceAddSpecificTargetGoalViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        if(refTableView == self.tableViewSpecificTarget){
            
            if((self.specificTargetList.count) < HelperConstant.minimumBlocks){
                self.isUpdate = true
                self.specificTargetList.append(text)
                self.tableViewSpecificTarget.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else if(refTableView == self.tableViewWorkDuringService){
            if((self.workDuringServiceList.count) < HelperConstant.minimumBlocks){
                self.isUpdate = true
                self.workDuringServiceList.append(text)
                self.tableViewWorkDuringService.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

extension ServiceAddSpecificTargetGoalViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        self.isUpdate = true
        
        if(refTableView == self.tableViewSpecificTarget && self.specificTargetList.count > 0){
            
            self.specificTargetList.remove(at: cellIndex?.row ?? 0)
            self.tableViewSpecificTarget.reloadData()
            
        } else if(refTableView == self.tableViewWorkDuringService && self.workDuringServiceList.count > 0){
            
            self.workDuringServiceList.remove(at: cellIndex?.row ?? 0)
            self.tableViewWorkDuringService.reloadData()
            
        }
    }
}



//
//  ServiceAddCalenderListingViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 24/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
//import Koyomi
import FSCalendar

class Cell: ScalingCarouselCell {
    @IBOutlet weak var lblText: UILabel!
}

protocol ServiceAddCalenderListingViewControllerDelegate {
    func saveSelectedInfo(date:String, time:String)
}

class ServiceAddCalenderListingViewController: LMBaseViewController {
    
    
    //MARK:- IBOutlet(s)
//    @IBOutlet fileprivate weak var koyomi: Koyomi! {
//        didSet {
//
//            koyomi.calendarDelegate = self
//            koyomi.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//            koyomi.weeks = ("S", "M", "T", "W", "T", "F", "S")
//            koyomi.style = .standard
//            koyomi.dayPosition = .center
//
//            koyomi.currentDateFormat = koyomi.currentDateString(withFormat: "yyyy-MM-dd HH:mm:ss Z")
//
//            koyomi.selectionMode = .sequence(style: .semicircleEdge)
////            koyomi.selectionMode = .sequence(style: .circle)
//
//
//            koyomi.select(date: Date())
//            koyomi.weekBackgrondColor = UIColor(hexString: ColorCode.tabInActiveColor) ?? UIColor.clear
//
//            koyomi.dayBackgrondColor = UIColor(hexString: ColorCode.tabInActiveColor) ?? UIColor.clear
//            koyomi.weekdayColor = UIColor.white
//            koyomi.weekColor = UIColor.white
//            koyomi.holidayColor = (UIColor.white, UIColor.white)
//
//            koyomi.selectedStyleColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
//
//            koyomi
//                .setDayFont(size: 16)
//                .setWeekFont(size: 16)
//        }
//    }
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var viewCalendar: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var scalingCarousel: ScalingCarouselView!
    @IBOutlet weak var carouselBottomConstraint: NSLayoutConstraint!

    fileprivate let invalidPeriodLength = 7
    fileprivate var selectedIndex = 0
    fileprivate var selectedTime = ""
    fileprivate var selectedDate = ""
    
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    var singleSelection = false
    
    private struct Constants {
        static let carouselHideConstant: CGFloat = -250
        static let carouselShowConstant: CGFloat = 15
    }
    
    var delegate:ServiceAddCalenderListingViewControllerDelegate?
    var strStartDate : String = ""
    var strEndDate : String = ""
    var arrSelectedDate : [String] = []
    
    // MARK: - For Only Current and Next Month Selection
    // MARK: -
    var mnthSelector : Int = 0
    
    fileprivate var timeString:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.calendar.swipeToChooseGesture.isEnabled = true
        // Swipe-To-Choose
        self.calendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
        self.calendar.select(Date())
        self.calendar.allowsMultipleSelection = false
        self.calendar.delegate = nil
        self.calendar.dataSource = nil
        
        //        self.presenter.connectView(view: self)
        
//        lblDate.text = koyomi.currentDateString(withFormat: "MMM yyyy")
        
        self.selectedDate = Date().appSpecificStringFromFormat()
        
        self.configTimeList()
        
//        scalingCarousel.register(CodeCell.self, forCellWithReuseIdentifier: "cell")
        
//        carouselBottomConstraint.constant = Constants.carouselHideConstant
        self.scalingCarousel.backgroundColor = UIColor.clear
        
        if(singleSelection){
//            koyomi.selectionMode = .single(style: .background)
        }
        
    }
    
    func configTimeList(){
        //timeString
        timeString.append("00:00 AM")
        for index in 1...11 {
            timeString.append("\(index):00 AM")
        }
        
        timeString.append("12:00 PM")
        for index in 1...11 {
            timeString.append("\(index):00 PM")
        }
    }
}

// MARK: - UIButton Action
// MARK: -
extension ServiceAddCalenderListingViewController{
    /**
     *  Previous button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnPrevious(_ sender: UIButton) {
        
        if  self.mnthSelector == 1{
            self.mnthSelector = 0
//            koyomi.display(in: .previous)
        }
    }
    
    /**
     *  Next button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnNext(_ sender: UIButton) {
        
        if self.mnthSelector == 0  {
            self.mnthSelector += 1
//            koyomi.display(in: .next)
        }
    }
    
    /**
     *  Close button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionClose(_ sender : UIButton){
        dismiss(animated: true, completion: nil)
    }
    
    /**
     *  Apply Filter button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionApplyFilter(_ sender : UIButton){
        dismiss(animated: true, completion: nil)
        
        selectedDate = (self.formatter.string(from: self.calendar.selectedDate ?? Date()))
        
        if(delegate != nil){
            delegate?.saveSelectedInfo(date: selectedDate, time: selectedTime)
        }
    }
}


// MARK: - KoyomiDelegate -
// MARK: -
//extension ServiceAddCalenderListingViewController: KoyomiDelegate {
    
    /**
     *  Koyomi  Calendar  Delegate.
     *
     *  @param .
     *
     *  @Developed By: Team Consagous
     */
    
//    func koyomi(_ koyomi: Koyomi, didSelect date: Date?, forItemAt indexPath: IndexPath) {
//
//        //print(koyomi.model.selectedDates.filter{$0.value}.map{$0.key.appSpecificStringFromFormat()})
//
//        self.selectedDate = date?.appSpecificStringFromFormat() ?? ""
//
//        self.arrSelectedDate = []
//        //        self.arrSelectedDate = koyomi.model.selectedDates.filter{$0.value}.map{$0.key.appSpecificStringFromFormat()}.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
//        print( self.arrSelectedDate )
//    }
    
    
//    func koyomi(_ koyomi: Koyomi, currentDateString dateString: String) {
//        lblDate.text = koyomi.currentDateString(withFormat: "MMM yyyy")
//
//    }
//
//    @objc(koyomi:shouldSelectDates:to:withPeriodLength:)
//    func koyomi(_ koyomi: Koyomi, shouldSelectDates date: Date?, to toDate: Date?, withPeriodLength length: Int) -> Bool {
//
//        //        if length > invalidPeriodLength {
//        //            print("More than \(invalidPeriodLength) days are invalid period.")
//        //            return false
//        //        }
//        //
//        //        if date != nil{
//        //            let strStartDate = date?.appSpecificStringFromFormat()
//        //            let strTodayDate = Date().appSpecificStringFromFormat()
//        //            let finalStartDate = self.toLocalDate(strStartDate!)
//        //            let finalTodayDate = self.toLocalDate(strTodayDate)
//        //
//        //            return finalStartDate >= finalTodayDate
//        //        }
//
//
//        // Update text color of Today's date
//        //        koyomi
//        //            .setDayColor(.white, of: today, to: weekLaterDay)
//        //            .setDayBackgrondColor(.black, of: today, to: weekLaterDay)
//
//        self.selectedDate = date?.appSpecificStringFromFormat() ?? ""
//
//        if(date != Date()){
//            koyomi
//                .setDayColor(#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), of: Date())
//                .setDayBackgrondColor(#colorLiteral(red: 0.2823529412, green: 0.3137254902, blue: 0.3490196078, alpha: 1), of: Date())
//        } else {
//            koyomi
//                .setDayColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), of: Date())
//        }
//
//
//        return true
//    }
    
//    func toLocalDate(_ withDateString : String) -> Date {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd MMM yyyy"
//        //        dateFormatter.dateFormat =  "yyyy-MM-dd"
//        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
//        return dateFormatter.date(from: withDateString)!
//    }
//}

extension ServiceAddCalenderListingViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeString.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Cell
        
//        if let scalingCell = cell as? ScalingCarouselCell {
//            scalingCell.mainView.backgroundColor = .blue
//        }
        
        cell.lblText.text = timeString[indexPath.row]
        if(selectedIndex == indexPath.row){
            cell.lblText.font = FontsConfig.FontHelper.defaultBoldFontWithSize(20)
            
            self.selectedTime = timeString[indexPath.row]
        } else {
            cell.lblText.font = FontsConfig.FontHelper.defaultRegularFontWithSize(18)
        }
        
        DispatchQueue.main.async {
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.row
        self.scalingCarousel.reloadData()
    }
}

extension ServiceAddCalenderListingViewController: UICollectionViewDelegate {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //carousel.didScroll()
        
//        guard let currentCenterIndex = scalingCarousel.currentCenterCellIndex?.row else { return }
//
////        output.text = String(describing: currentCenterIndex)
//        print(String(describing: currentCenterIndex))
    }
}

extension ServiceAddCalenderListingViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
}
//
//extension ServiceAddCalenderListingViewController:FSCalendarDataSource{
//   // MARK:- FSCalendarDataSource
//
//   func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
//       let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
//       return cell
//   }
//
//   func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
////       self.configure(cell: cell, for: date, at: position)
//   }
//
////   func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
////       if self.gregorian.isDateInToday(date) {
////           return "今"
////       }
////       return nil
////   }
//
//   func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
//       return 0
//   }
//}
//
//extension ServiceAddCalenderListingViewController:FSCalendarDelegate{
//
//    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
//        //        self.calendarHeightConstraint.constant = bounds.height
//        self.view.layoutIfNeeded()
//    }
//
//    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
//
////        print("did select date \(self.formatter.string(from: date))")
////        if date.isAfterDate(Date().endOfDay!) {
////          calendar.deselect(date)
////        } else {
////          selectedDateArray.append(date)
////        }
////
//        self.configureVisibleCells()
//    }
//
//    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
////        print("did deselect date \(self.formatter.string(from: date))")
//
//
////        if calendar.selectedDates.count > 2 {
////            let datesToDeselect: [Date] = calendar.selectedDates.filter{ $0 > date }
////            datesToDeselect.forEach{ calendar.deselect($0) }
////            calendar.select(date) // adds back the end date that was just deselected so it matches selectedDateArray
////        }
////        selectedDateArray = selectedDateArray.filter{ $0 < date }
////        selectedDateArray.forEach{self.calendar.select($0)}
//
//        self.configureVisibleCells()
//    }
//
//    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
//        if self.gregorian.isDateInToday(date) {
//            return [UIColor.orange]
//        }
//        return [appearance.eventDefaultColor]
//    }
//
//    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
//        print("\(self.dateFormatter.string(from: calendar.currentPage))")
//    }
////
////    // MARK: - Private functions
//    private func configureVisibleCells() {
//        calendar.visibleCells().forEach { (cell) in
//            let date = calendar.date(for: cell)
//            let position = calendar.monthPosition(for: cell)
//            self.configure(cell: cell, for: date!, at: position)
//        }
//    }
////
//    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
//
//        let diyCell = (cell as! DIYCalendarCell)
//        // Custom today circle
////        diyCell.circleImageView.isHidden = !self.gregorian.isDateInToday(date)
//        // Configure selection layer
//        if position == .current {
//
//            var selectionType = SelectionType.none
//
//            if calendar.selectedDates.contains(date) {
//                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
//                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
//                if calendar.selectedDates.contains(date) {
//                    if calendar.selectedDates.contains(previousDate) && calendar.selectedDates.contains(nextDate) {
//                        selectionType = .middle
//                    }
//                    else if calendar.selectedDates.contains(previousDate) && calendar.selectedDates.contains(date) {
//                        selectionType = .rightBorder
//                    }
//                    else if calendar.selectedDates.contains(nextDate) {
//                        selectionType = .leftBorder
//                    }
//                    else {
//
//                        if(self.calendar.selectedDates.count > 0){
//                            for date in self.calendar.selectedDates{
//                                self.calendar.deselect(date)
//                            }
//                        }
//
//                        selectionType = .single
//                    }
//                }
//            }
//            else {
//                selectionType = .none
//            }
//            if selectionType == .none {
//                diyCell.selectionLayer.isHidden = true
//                return
//            }
//            diyCell.selectionLayer.isHidden = false
//            diyCell.selectionType = selectionType
//
//        } else {
////            diyCell.circleImageView.isHidden = true
//            diyCell.selectionLayer.isHidden = true
//        }
//    }
//}

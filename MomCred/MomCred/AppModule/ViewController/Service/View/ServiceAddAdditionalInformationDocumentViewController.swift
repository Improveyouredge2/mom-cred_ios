//
//  ServiceAddAdditionalInformationDocumentViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 26/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import MobileCoreServices

/**
 * ServiceAddAdditionalInformationDocumentViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddAdditionalInformationDocumentViewController : LMBaseViewController{
    
//    fileprivate let formNumber = 17
    
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    
    @IBOutlet weak var documentImgCollectionView: UICollectionView!
    
    var arrDocumentImages = [UploadImageData]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var businessAdditionalDocumentInfoList:[BusinessAdditionalDocumentInfo] = []
    fileprivate var presenter = ServiceAddAdditionalInformationDocumentPresenter()
    fileprivate var serviceAddPrePostViewController:ServiceAddPrePostViewController?
    fileprivate var isUpdate = false
    
    var optionType:String? = "2"
    
    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        documentImgCollectionView.backgroundColor = UIColor.clear
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        
        presenter.connectView(view: self)
        
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serviceAddPrePostViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(businessAdditionalDocumentInfoList.count == 0){
            self.viewTableView.isHidden = true
        }
        
        if(self.tableView.contentSize.height > 0){
            tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            self.updateTableViewHeight()
        }
    }
    
    override func pickedImage(fromImagePicker: UIImage, imageData: Data, mediaType:String, imageName:String, fileUrl: String?) {
        let imageDataTemp = UploadImageData()
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            imageDataTemp.id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id != nil){
            imageDataTemp.id =  self.serviceAddRequest?.service_id
        }
        
        imageDataTemp.imageData = imageData
        imageDataTemp.imageName = "Test_\(imageName)"
        imageDataTemp.imageKeyName = "doc_img[]"
        
        if let theExt = imageDataTemp.imageName as NSString?, ["png", "jpg", "jpeg"].contains(theExt.pathExtension.lowercased()) {
            imageDataTemp.imageType = "1"
        } else {
            imageDataTemp.imageType = "2"
        }
                
        if let optionType = optionType, !optionType.isEmpty {
            imageDataTemp.mediaCategory = optionType
        }
        
        self.arrDocumentImages.append(imageDataTemp)
        
        self.documentImgCollectionView.reloadData()
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

//MARK:- Button Actions
//MARK:-
extension ServiceAddAdditionalInformationDocumentViewController{
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnAddImg(_ sender: UIButton) {
        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String, "com.microsoft.word.doc", "org.openxmlformats.wordprocessingml.document"], in: .import)
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    /*
    @IBAction func actionBtnAddImg(_ sender: UIButton) {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            debugPrint("already authorized")
            self.displayPhotoSelectionOption(withCircularAllow: false)
            
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    debugPrint("access allowed")
                    self.displayPhotoSelectionOption(withCircularAllow: false)
                    
                } else {
                    //access denied
                    debugPrint("Access denied")
//                    LMBaseApp.sharedInstance.presentCustomAlertsViewController(self, title: LocalizationKeys.app_name.getLocalized(), message: LocalizationKeys.msg_validation_on_camera.getLocalized(), buttonTitle: LocalizationKeys.btn_ok.getLocalized().uppercased(), actionOk: {
                    
//                    })
                }
            })
        }
    }
     */
    
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnDeleteStoreImg(_ sender: UIButton) {
        
        if(self.arrDocumentImages.count > 0){
            self.arrDocumentImages.remove(at: sender.tag)
            //let imageArray = self.update_img_id.map{(String($0))}.joined(separator: ",")
            
            if self.arrDocumentImages.count == 0{
                //            self.imgDataVenuePics = nil
            }
            self.documentImgCollectionView.reloadData()
        }
    }
}

extension ServiceAddAdditionalInformationDocumentViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("didPickDocumentsAt: \(urls)")
        if let url = urls.first {
            let imageDataTemp = UploadImageData()
            
            if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
                imageDataTemp.id = UserDefault.getSID() ?? ""
            } else if(self.serviceAddRequest?.service_id != nil){
                imageDataTemp.id =  self.serviceAddRequest?.service_id
            }
            
            imageDataTemp.filePath = url.path
            imageDataTemp.imageData = try? Data(contentsOf: url)
            if let filename = url.pathComponents.last {
                imageDataTemp.imageName = "Test_\(filename)"
            }
            imageDataTemp.imageKeyName = "doc_img[]"
            imageDataTemp.imageType = "2"
            
            if url.pathExtension.lowercased() == "pdf" {
                imageDataTemp.imageThumbnailData = UIImage(named: "ic_pdf")?.jpegData(compressionQuality: 1)
                imageDataTemp.mimetype = "application/pdf"
            } else {
                imageDataTemp.imageThumbnailData = UIImage(named: "ic_doc")?.jpegData(compressionQuality: 1)
                if url.pathExtension.lowercased() == "docx" {
                    imageDataTemp.mimetype = "application/vnd.openxmlformats-officedocument.wordprocessingml"
                } else {
                    imageDataTemp.mimetype = "application/msword"
                }
            }

            if let optionType = optionType, !optionType.isEmpty {
                imageDataTemp.mediaCategory = optionType
            }
            
            self.arrDocumentImages.append(imageDataTemp)
            
            self.documentImgCollectionView.reloadData()
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("documentPickerWasCancelled")
    }
}

extension ServiceAddAdditionalInformationDocumentViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Document Name")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        if(self.serviceAddRequest != nil){
            
            if(self.serviceAddRequest?.doc != nil && (self.serviceAddRequest?.doc?.count)! > 0){
                self.businessAdditionalDocumentInfoList = self.serviceAddRequest?.doc ?? []
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        isUpdate = self.isUpdate
        
        return isUpdate
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
//        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
//        self.serviceAddRequest?.doc = self.businessAdditionalDocumentInfoList
        
        return true
    }
    
    fileprivate func openNextScr(){
        
        self.serviceAddPrePostViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddPrePostViewController.nameOfClass) as ServiceAddPrePostViewController
        
        self.serviceAddPrePostViewController?.serviceAddRequest = self.serviceAddRequest
        self.navigationController?.pushViewController(self.serviceAddPrePostViewController!, animated: true)
    }
}

extension ServiceAddAdditionalInformationDocumentViewController{
    fileprivate func uploadInfoOnServer(completion: @escaping () -> Void) {
        
        var isFound = false
        for businessDocumentInfo in self.businessAdditionalDocumentInfoList{
            if(!businessDocumentInfo.uploadStatus && businessDocumentInfo.imageurls == nil){
                
                let multipleAdditionalDocumentInfo = MultipleAdditionalDocumentInfo()
                
                multipleAdditionalDocumentInfo.busi_id = businessDocumentInfo.busi_id
                multipleAdditionalDocumentInfo.name = businessDocumentInfo.name
                multipleAdditionalDocumentInfo.desc = businessDocumentInfo.desc
                multipleAdditionalDocumentInfo.imageData = businessDocumentInfo.imageData
                multipleAdditionalDocumentInfo.arrImages = businessDocumentInfo.arrImages
                multipleAdditionalDocumentInfo.arrImageIdList = businessDocumentInfo.arrImageIdList
                multipleAdditionalDocumentInfo.uploadStatus = businessDocumentInfo.uploadStatus
                
                multipleAdditionalDocumentInfo.media_id = businessDocumentInfo.media_id
                multipleAdditionalDocumentInfo.media_service = businessDocumentInfo.media_service
                multipleAdditionalDocumentInfo.imageurl = businessDocumentInfo.imageurl
                multipleAdditionalDocumentInfo.imageurls = businessDocumentInfo.imageurls
                multipleAdditionalDocumentInfo.thumb = businessDocumentInfo.thumb
                multipleAdditionalDocumentInfo.media_extension = businessDocumentInfo.media_extension
                multipleAdditionalDocumentInfo.media_title = businessDocumentInfo.media_title
                multipleAdditionalDocumentInfo.media_desc = businessDocumentInfo.media_desc
                multipleAdditionalDocumentInfo.count = businessDocumentInfo.count
                
                isFound = true
                multipleAdditionalDocumentInfo.count = NSNumber(integerLiteral: businessDocumentInfo.arrImages?.count ?? 0)
                presenter.submitData(businessAdditionalDocumentInfo: multipleAdditionalDocumentInfo, imageList: businessDocumentInfo.arrImages, callback: { [weak self] (status, response, message) in
                    
                    if(status == true){
                        businessDocumentInfo.uploadStatus = true
                        //                        self.navigationController?.popToRootViewController(animated: true)
                        self?.uploadInfoOnServer(completion: completion)
                    }
                })
                
                break
            }
        }
        
        if(!isFound){
            completion()
//            self.openNextScr()
        }
    }
}

extension ServiceAddAdditionalInformationDocumentViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                uploadInfoOnServer() {
                    completion()
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension ServiceAddAdditionalInformationDocumentViewController{
    
    @IBAction func methodAddDocumentAction(_ sender: UIButton){
        let businessAdditionalDocumentInfo = BusinessAdditionalDocumentInfo()
        
        if((self.inputName.text()?.trim().length)! > 0){
            businessAdditionalDocumentInfo.name = self.inputName.text() ?? ""
            businessAdditionalDocumentInfo.media_title = self.inputName.text() ?? ""
            businessAdditionalDocumentInfo.media_service = "1"
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_document_name.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if((self.textViewDesc.text?.trim().length)! > 0){
            businessAdditionalDocumentInfo.desc = self.textViewDesc.text ?? ""
            businessAdditionalDocumentInfo.media_desc = self.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_document_description.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(arrDocumentImages.count > 0){
            if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
                businessAdditionalDocumentInfo.busi_id = UserDefault.getSID() ?? ""
            } else if(self.serviceAddRequest?.service_id != nil){
                businessAdditionalDocumentInfo.busi_id =  self.serviceAddRequest?.service_id
            }

            businessAdditionalDocumentInfo.arrImages = arrDocumentImages
            
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_document_image_not_found.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(self.businessAdditionalDocumentInfoList.count < HelperConstant.minimumMediaBlocks){
            
            self.isUpdate = true
            self.businessAdditionalDocumentInfoList.append(businessAdditionalDocumentInfo)
            
            self.inputName.input.text = ""
            self.textViewDesc.text = ""
            arrDocumentImages = []
            documentImgCollectionView.reloadData()
            
            
            self.viewTableView.isHidden = false
            self.tableView.reloadData()
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddAdditionalInformationDocumentViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return businessAdditionalDocumentInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationDocumentCell.nameOfClass) as! BusinessInformationAddAdditionalInformationDocumentCell
        
        let objInfo = businessAdditionalDocumentInfoList[indexPath.row]
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        if(objInfo.media_title != nil){
            cell.lblName.text = objInfo.media_title ?? ""
        } else {
            cell.lblName.text = objInfo.name
        }
        
        if(objInfo.media_desc != nil){
            cell.lblDesc.text = objInfo.media_desc ?? ""
        } else {
            cell.lblDesc.text = objInfo.desc
        }
        
        if let imageURLs = objInfo.imageurls, !imageURLs.isEmpty {
            cell.arrDocumentImagesUrl = imageURLs.compactMap { $0.thumb }
        } else {
            cell.arrDocumentImages = objInfo.arrImages?.compactMap { uploadData in
                var image: UIImage?
                if let thumbnailData = uploadData.imageThumbnailData {
                    image = UIImage(data: thumbnailData)
                } else if let imageData = uploadData.imageData {
                    image = UIImage(data: imageData)
                }
                return image
            }
        }
        
        cell.refreshScreenInfo()
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension ServiceAddAdditionalInformationDocumentViewController:BusinessInformationAddAdditionalInformationDocumentCellDelegate{
    func zoomableImage(cellIndex: IndexPath?) {
        
    }
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
        let objInfo = businessAdditionalDocumentInfoList[cellIndex?.row ?? 0]
        if(objInfo.imageurls != nil && (objInfo.imageurls?.count)! > 0){
            let deleteMediaRequest = DeleteMediaRequest(media_id: objInfo.media_id ?? "")
            
            self.presenter.deleteMediaData(deleteMediaRequest: deleteMediaRequest, callback: {
                (status, response, message) in
                
                if(status){
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    if(self.businessAdditionalDocumentInfoList.count > 0){
                        self.businessAdditionalDocumentInfoList.remove(at: cellIndex?.row ?? 0)
                        self.tableView.reloadData()
                        
                        if(self.businessAdditionalDocumentInfoList.count == 0){
                            self.viewTableView.isHidden = true
                            self.businessAdditionalDocumentInfoList = []
                        }
                    }
                } else {
                    
                }
            })
            
        } else {
            if(self.businessAdditionalDocumentInfoList.count > 0){
                self.businessAdditionalDocumentInfoList.remove(at: cellIndex?.row ?? 0)
            }
            self.tableView.reloadData()
        }
        
        if(businessAdditionalDocumentInfoList.count == 0){
            self.viewTableView.isHidden = true
            self.businessAdditionalDocumentInfoList = []
        }
    }
}


/*******************************************/
//MARK: - UICollectionViewDelegate
/*******************************************/
extension ServiceAddAdditionalInformationDocumentViewController : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if arrDocumentImages.count < 2{ // For 2 images
            return arrDocumentImages.count + 1
        }else{
            return arrDocumentImages.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //MARK:- Store Images views
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DocumentImageCVCell.nameOfClass, for: indexPath as IndexPath) as! DocumentImageCVCell
        
        
        if (arrDocumentImages.count == 0 || arrDocumentImages.count + 1 == indexPath.row + 1){
            //cell.btnAddImage.isUserInteractionEnabled = true
            cell.btnAddImage.isHidden = false
            cell.imgViewDocumnt.image = nil
            cell.btnDelete.isHidden = true
            
        } else {
            // cell.btnAddImage.isUserInteractionEnabled = false
            cell.btnAddImage.isHidden = true
            cell.imgViewDocumnt.clipsToBounds = true

            if let thumbnailData = arrDocumentImages[indexPath.row].imageThumbnailData {
                cell.imgViewDocumnt.image = UIImage(data: thumbnailData)
            } else if let imageData = arrDocumentImages[indexPath.row].imageData {
                cell.imgViewDocumnt.image = UIImage(data: imageData)
            }
            cell.btnDelete.isHidden = false
            cell.btnDelete.tag = indexPath.row
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
}

///********************************/
////MARK:- UICollectionViewCell
///*******************************/


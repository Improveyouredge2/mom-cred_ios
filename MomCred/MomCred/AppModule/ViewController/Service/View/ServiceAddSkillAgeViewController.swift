//
//  ServiceAddSkillAgeViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 03/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

/**
 * ServiceAddSkillAgeViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddSkillAgeViewController : LMBaseViewController{
    
    fileprivate let formNumber = 12
    
    @IBOutlet weak var btnChkBeginner: UIButton!
    @IBOutlet weak var btnChkIntermediate: UIButton!
    @IBOutlet weak var btnChkExpert: UIButton!
    @IBOutlet weak var dropDownDurationType: DropDown!
    
    // Additional Service option
    @IBOutlet weak var textFieldMiniAge: RYFloatingInput!
    @IBOutlet weak var textFieldMaxAge: RYFloatingInput!
    @IBOutlet weak var textFieldDurationTime: RYFloatingInput!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var serviceSkill:ServiceSkill?
    fileprivate var serviceAddListingContentAssociatedViewController:ServiceAddListingContentAssociatedViewController?
    fileprivate var durationType:DetailData?
    fileprivate var isUpdate = false
    fileprivate var presenter = ServiceAddSkillAgePresenter()
    
    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()

        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension ServiceAddSkillAgeViewController{
    fileprivate func setScreenData(){
        
        self.durationType = DetailData()
        
//        @IBOutlet weak var textFieldMiniAge: RYFloatingInput!
//        @IBOutlet weak var textFieldMaxAge: RYFloatingInput!
//        @IBOutlet weak var textFieldDurationTime: RYFloatingInput!
        
        self.textFieldMiniAge.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Min Age")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.textFieldMiniAge.input.keyboardType = UIKeyboardType.numberPad
        
        self.textFieldMaxAge.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Max Age")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        self.textFieldMaxAge.input.keyboardType = UIKeyboardType.numberPad
        
        
        self.textFieldDurationTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Duration time")
                .maxLength(HelperConstant.LIMIT_DURATION, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        self.textFieldDurationTime.input.keyboardType = UIKeyboardType.numberPad
        
        
        // Duration
        ///////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        self.dropDownDurationType.optionArray = HelperConstant.durationType
        //Its Id Values and its optional
        self.dropDownDurationType.optionIds = [1,2,3,4,5]
        
//        self.dropDownDurationType.selectedIndex = 0
        //        selectedMonth = dropDown.optionArray[0]
        //        dropDownStdExpectional.text = selectedMonth
        // The the Closure returns Selected Index and String
        self.dropDownDurationType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            
            self.durationType?.id = "\(id)"
            self.durationType?.title = selectedText
            
        }
        ////////////////////////////////////////////////////////////
        
        if(self.serviceAddRequest?.service_skill != nil){
            self.serviceSkill = self.serviceAddRequest?.service_skill
            
            if(self.serviceSkill?.skillLevel != nil){
                
                if(self.serviceSkill?.skillLevel?.map{$0.id}.contains("1") ?? false){
                    self.btnChkBeginner.isSelected = true
                }
                
                if(self.serviceSkill?.skillLevel?.map{$0.id}.contains("2") ?? false){
                    self.btnChkIntermediate.isSelected = true
                }
                
                if(self.serviceSkill?.skillLevel?.map{$0.id}.contains("3") ?? false){
                    self.btnChkExpert.isSelected = true
                }
            }
            
            self.textFieldMiniAge.input.text = self.serviceSkill?.minAge ?? ""
            self.textFieldMaxAge.input.text = self.serviceSkill?.maxAge ?? ""
            
            self.durationType?.id = self.serviceSkill?.durationId ?? ""
            self.durationType?.title = self.serviceSkill?.durationTitle ?? ""
            
            self.dropDownDurationType.text = self.serviceSkill?.durationTitle ?? ""
            self.textFieldDurationTime.input.text = self.serviceSkill?.durationTime ?? ""
            
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        if (!self.btnChkBeginner.isSelected && !self.btnChkIntermediate.isSelected && !self.btnChkExpert.isSelected) {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_skill_level.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        
        if (self.textFieldMiniAge.text()?.trim().isEmpty)!{
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_skill_min_age.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        
        if (self.textFieldMaxAge.text()?.trim().isEmpty)!{
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_skill_max_age.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        
        let minAge:String = self.textFieldMiniAge.text() ?? ""
        let maxAge:String = self.textFieldMaxAge.text() ?? ""
        
        if (Int(minAge) ?? 0) >= (Int(maxAge) ?? 0){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_skill_age_diff.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        
        if(self.durationType?.id == nil || (self.durationType?.id != nil && (self.durationType?.id?.length)! == 0)){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_skill_duration_type.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        
        if (self.textFieldDurationTime.text()?.trim().isEmpty)!{
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_skill_duration_time.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if(self.serviceSkill == nil){
            self.serviceSkill = ServiceSkill()
        }
        
        self.serviceSkill?.skillLevel = []
        
        if(self.serviceSkill?.skillLevel != nil){
            var detailData = DetailData()
            
            if(self.btnChkBeginner.isSelected){
                detailData.id = "1"
                detailData.title = self.btnChkBeginner.titleLabel?.text ?? ""
                 self.serviceSkill?.skillLevel?.append(detailData)
            }
            
            if(self.btnChkIntermediate.isSelected){
                detailData = DetailData()
                detailData.id = "2"
                detailData.title = self.btnChkIntermediate.titleLabel?.text ?? ""
                self.serviceSkill?.skillLevel?.append(detailData)
            }
            
            if(self.btnChkExpert.isSelected){
                detailData = DetailData()
                detailData.id = "3"
                detailData.title = self.btnChkExpert.titleLabel?.text ?? ""
                self.serviceSkill?.skillLevel?.append(detailData)
            }
        }
        
        self.serviceSkill?.minAge = self.textFieldMiniAge.input.text ?? ""
        self.serviceSkill?.maxAge = self.textFieldMaxAge.input.text ?? ""
        
        self.serviceSkill?.durationId = self.durationType?.id ?? ""
        self.serviceSkill?.durationTitle = self.durationType?.title ?? ""
        
        self.serviceSkill?.durationTime = self.textFieldDurationTime.input.text ?? ""
        
        self.serviceAddRequest?.service_skill = self.serviceSkill
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.serviceAddRequest?.service_skill != nil){
            if(self.textFieldMiniAge.input.text != (self.serviceSkill?.minAge ?? "")){
                isUpdate = true
            }
            
            if(self.textFieldMaxAge.input.text != (self.serviceSkill?.maxAge ?? "")){
                isUpdate = true
            }
            
            if(self.durationType?.id != (self.serviceSkill?.durationId ?? "")){
                isUpdate = true
            }
            
            if(self.textFieldDurationTime.input.text != (self.serviceSkill?.durationTime ?? "")){
                isUpdate = true
            }
            
            if(self.isUpdate){
                isUpdate = self.isUpdate
            }
        } else {
            isUpdate = true
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
       
        self.serviceAddListingContentAssociatedViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddListingContentAssociatedViewController.nameOfClass) as ServiceAddListingContentAssociatedViewController
        
        self.serviceAddListingContentAssociatedViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddListingContentAssociatedViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddListingContentAssociatedViewController!, animated: true)
    }
}

extension ServiceAddSkillAgeViewController{
    
    @IBAction func methodChkSkillSelectionAction(_ sender: UIButton) {
        self.isUpdate = true
//        self.btnChkBeginner.isSelected = false
//        self.btnChkIntermediate.isSelected = false
//        self.btnChkExpert.isSelected = false
        if(self.btnChkBeginner == sender){
            self.btnChkBeginner.isSelected = !self.btnChkBeginner.isSelected
        } else if(self.btnChkIntermediate == sender){
            self.btnChkIntermediate.isSelected = !self.btnChkIntermediate.isSelected
        } else if(self.btnChkExpert == sender){
            self.btnChkExpert.isSelected = !self.btnChkExpert.isSelected
        }
    }
    
    
    //MARK:- Button Action method implementation
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

//MARK: Textfield delegates
//extension ServiceAddSkillAgeViewController : UITextFieldDelegate{
//    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        guard let text = textField.text else { return true }
//
//        var limitLength = 0
//
//        if textField == textFieldServicePrice {
//            limitLength = HelperConstant.LIMIT_AMOUNT
//        }
//
//        // Amt field validation
//        if textField == textFieldServicePrice {
//            return textFieldServicePrice.validatePriceField(string: string, text: textField.text!, maxLength: HelperConstant.LIMIT_AMOUNT, range: range)
//        }
//
//        let  char = string.cString(using: String.Encoding.utf8)!
//        let isBackSpace = strcmp(char, "\\b")
//
//        if (isBackSpace == -92) {
//            return true
//        }
//
//        let newLength = text.count + string.count - range.length
//        if newLength > limitLength {
//            return false
//        }
//        return true
//    }
//}

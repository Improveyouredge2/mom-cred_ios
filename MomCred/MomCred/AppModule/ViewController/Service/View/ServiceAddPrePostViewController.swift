//
//  ServiceAddPrePostViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 03/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class PrePostDetail{
    var type:String = ""
    var desc:String = ""
}

/**
 * ServiceAddPrePostViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddPrePostViewController : LMBaseViewController{
    
    fileprivate let formNumber = 17
    
    // Event
    @IBOutlet weak var tableViewExternalLink: UITableView!
    
    @IBOutlet weak var dropDownRequirementType: DropDown!
    
    @IBOutlet weak var constraintstableViewExternalLinkHeight: NSLayoutConstraint!
    
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var viewExternalLinkTableView: UIView!

    fileprivate var prePostDetailList:[ServicePrepageRequirement] = []
    fileprivate var isUpdate = false
    fileprivate var presenter = ServiceAddPrePostPresenter()
    fileprivate var selectedType = ""
    
    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        
        // Duration
        ///////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        self.dropDownRequirementType.optionArray = ["Pre-service", "Post-service"]
        //Its Id Values and its optional
        self.dropDownRequirementType.optionIds = [1,2,3,4,5]
        
        self.dropDownRequirementType.selectedIndex = 0
        //        selectedMonth = dropDown.optionArray[0]
        //        dropDownStdExpectional.text = selectedMonth
        // The the Closure returns Selected Index and String
        self.dropDownRequirementType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.selectedType = selectedText
        }
        ////////////////////////////////////////////////////////////
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.prePostDetailList.count == 0){
            self.viewExternalLinkTableView.isHidden = true
        } else {
            self.viewExternalLinkTableView.isHidden = false
        }
        
        if(self.tableViewExternalLink.contentSize.height > 0){
            tableViewExternalLink.scrollToBottom()
            self.constraintstableViewExternalLinkHeight?.constant = self.tableViewExternalLink.contentSize.height
            
            self.updateTableViewHeight(tableView: self.tableViewExternalLink)
        }
        
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
         if(tableViewExternalLink == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewExternalLink.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewExternalLink.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewExternalLinkHeight.constant = heightOfTableView
            }
        }
    }
}


extension ServiceAddPrePostViewController{
    fileprivate func setScreenData(){
        
        if(self.serviceAddRequest?.service_prepage_requirement != nil){
            
            self.prePostDetailList = self.serviceAddRequest?.service_prepage_requirement ?? []
            
            self.tableViewExternalLink.reloadData()
        }
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        /*
        if (self.prePostDetailList.count == 0){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_content_associated_personal_listing.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        */
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if(self.prePostDetailList.count > 0){
            self.serviceAddRequest?.service_prepage_requirement = self.prePostDetailList
        } else {
            self.serviceAddRequest?.service_prepage_requirement = nil
        }
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr() {
        let serviceDetailVC = navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
        if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
            serviceDetailVC.shouldRefreshContent = true
            navigationController?.popToViewController(serviceDetailVC, animated: true)
        } else {
            navigationController?.popToRootViewController(animated: true)
        }
    }
}

//MARK:- Button Action method implementation
extension ServiceAddPrePostViewController{
    @IBAction func methodAddExternalLinkAction(_ sender: UIButton){
        if(textViewDesc.text.length > 0){
            self.isUpdate = true
            let externalLinkDetailInfo = ServicePrepageRequirement()
            externalLinkDetailInfo.requirementType = self.selectedType
            externalLinkDetailInfo.description = self.textViewDesc.text ?? ""
            
            prePostDetailList.append(externalLinkDetailInfo)
            
            self.textViewDesc.text = ""
            
            if(self.prePostDetailList.count == 1){
                self.constraintstableViewExternalLinkHeight?.constant = 10
            }
            
            self.tableViewExternalLink.reloadData()
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_description.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
    }
    
    @IBAction func methodNextAction(_ sender: UIButton){
//        self.navigationController?.popToRootViewController(animated: true)
        
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            if(!self.isDataUpdate()){
                if(self.isFormValid()){
                    self.openNextScr()
                }
            } else if(self.isFormValid()){
                Spinner.show()
                self.presenter.submitData(callback: {
                    (status, response, message) in
                    
                    if(status){
                        
                        UserDefault.removeSID()
                        
                        self.openNextScr()
                    }
                })
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddPrePostViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tableViewExternalLink){
            return prePostDetailList.count
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
        
        if(prePostDetailList.count > indexPath.row){
            let objInfo = prePostDetailList[indexPath.row]
            
            cell.cellIndex = indexPath
            cell.delegate = self
            
            cell.lblLink.text = objInfo.requirementType
            cell.lblDesc.text = objInfo.description
        }
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension ServiceAddPrePostViewController:ExternalLinkTableViewCellDelegate{
    
    func removeExternalLinkInfo(cellIndex:IndexPath?){
        
        if(self.prePostDetailList.count > 0){
            self.prePostDetailList.remove(at: cellIndex?.row ?? 0)
            self.tableViewExternalLink.reloadData()
            
            self.isUpdate = true
            
            if(prePostDetailList.count == 0){
                self.viewExternalLinkTableView.isHidden = true
            }
        }
    }
}

//
//  ServiceAddListingContentAssociatedViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 03/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class AssociatedListDetail{
    var title:String = ""
    var desc:String = ""
    var roleDesc:String = ""
}

/**
 * ServiceAddListingContentAssociatedViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddListingContentAssociatedViewController : LMBaseViewController{
    
    fileprivate let formNumber = 13
    
    // Event
//    @IBOutlet weak var tableViewSpecificTarget: UITableView!
//    @IBOutlet weak var tableViewWorkDuringService: UITableView!
    
    @IBOutlet weak var tableViewFacilityService: BIDetailTableView! {
        didSet {
            tableViewFacilityService.sizeDelegate = self
        }
    }
    @IBOutlet weak var tableViewPersonnelService: BIDetailTableView! {
        didSet {
            tableViewPersonnelService.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintstableViewFacilityServiceHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintstableViewPersonnelServiceHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var facilityServiceList:[ServiceListData] = []
    fileprivate var personnelServiceList:[ServiceListData] = []
    
//    fileprivate var serviceAddListingServiceViewController:ServiceAddListingServiceViewController?
    fileprivate var serviceAddILCAViewController: ServiceAddInsideLookContentAssociatedViewController?
    fileprivate var isUpdate = false
    fileprivate var presenter = ServiceAddListingContentAssociatedPresenter()
    
    
    var serviceAddRequest:ServiceAddRequest?
    var facilityList:[ServiceListingList]?
    var personnelList:[ServiceListingList]?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.tableViewFacilityService.backgroundColor = UIColor.clear
        self.tableViewPersonnelService.backgroundColor = UIColor.clear
        
        self.presenter.getPersonalServiceList()
        self.presenter.getFacilityServiceList()
        
        self.setScreenData()

        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.serviceAddListingServiceViewController = nil
        serviceAddILCAViewController = nil
    }
}


extension ServiceAddListingContentAssociatedViewController{
    fileprivate func setScreenData(){
        
        if(self.serviceAddRequest?.service_listing != nil){
            
            self.facilityServiceList = self.serviceAddRequest?.service_listing?.fieldListing ?? []
            self.personnelServiceList = self.serviceAddRequest?.service_listing?.personnelListing ?? []
            
            self.tableViewFacilityService.reloadData()
            self.tableViewPersonnelService.reloadData()
        }
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
//        if (self.facilityServiceList.count == 0){
//            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_content_associated_field_listing.getLocalized(), buttonTitle: nil, controller: nil)
//            return false
//        }
//        
//        if (self.personnelServiceList.count == 0){
//            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_content_associated_personal_listing.getLocalized(), buttonTitle: nil, controller: nil)
//            return false
//        }
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        let serviceListing = ServiceListing()
        serviceListing.fieldListing = self.facilityServiceList
        serviceListing.personnelListing = self.personnelServiceList
        
        
        self.serviceAddRequest?.service_listing = serviceListing
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        serviceAddILCAViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddInsideLookContentAssociatedViewController.nameOfClass) as ServiceAddInsideLookContentAssociatedViewController
        
        serviceAddILCAViewController?.serviceAddRequest = serviceAddRequest
        serviceAddILCAViewController?.isEditingService = isEditingService
        navigationController?.pushViewController(serviceAddILCAViewController!, animated: true)

//        self.serviceAddListingServiceViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddListingServiceViewController.nameOfClass) as ServiceAddListingServiceViewController
//
//        self.serviceAddListingServiceViewController?.serviceAddRequest = self.serviceAddRequest
//        self.navigationController?.pushViewController(self.serviceAddListingServiceViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension ServiceAddListingContentAssociatedViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddListingContentAssociatedViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tableViewFacilityService){
//            if((self.facilityServiceList.count) != HelperConstant.minimumBlocks){
                return (facilityServiceList.count) + 1
//            } else {
//                return facilityServiceList.count
//            }
        } else if(tableView == tableViewPersonnelService){
//            if((self.personnelServiceList.count) != HelperConstant.minimumBlocks){
                return (personnelServiceList.count) + 1
//            } else {
//                return personnelServiceList.count
//            }
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(self.tableViewFacilityService == tableView){
            
            if(indexPath.row == 0){
                
                let cell = tableView.dequeueReusableCell(withIdentifier: AddMoreCell.nameOfClass) as! AddMoreCell
                
                cell.textField.placeholder = "Link"
                if(cell.ViewRoleDesc != nil){
                    cell.ViewRoleDesc.isHidden = true
                }
                cell.delegate = self
                cell.refTableView = self.tableViewFacilityService
                
//                cell.dropDownList.
                cell.updateDropDown(listInfo: self.facilityList ?? [])
                
                cell.backgroundColor = UIColor.clear
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
                
                cell.delegate = self
                //                cell.cellIndex = indexPath
                cell.refTableView = self.tableViewFacilityService
                
//                if(self.facilityServiceList.count) != HelperConstant.minimumBlocks{
                    cell.lblTitle.text = facilityServiceList[indexPath.row-1].link
                    cell.lblTitleDesc.text = facilityServiceList[indexPath.row-1].desc
                    cell.lblRoleDesc.text = facilityServiceList[indexPath.row-1].roleDesc
                    
                    cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
//                } else {
//                    cell.cellIndex = indexPath
//                    cell.lblTitle.text = facilityServiceList[indexPath.row].link
//                    cell.lblTitleDesc.text = facilityServiceList[indexPath.row].desc
//                    cell.lblRoleDesc.text = facilityServiceList[indexPath.row].roleDesc
//                }
                
                cell.backgroundColor = UIColor.clear
                return cell
            }
            
        } else if(self.tableViewPersonnelService == tableView){
            
            if(indexPath.row == 0){
                
                let cell = tableView.dequeueReusableCell(withIdentifier: AddMoreCell.nameOfClass) as! AddMoreCell
                
                cell.textField.placeholder = "Link"
                cell.delegate = self
                cell.refTableView = self.tableViewPersonnelService
                
                cell.updateDropDown(listInfo: self.personnelList ?? [])
                
                cell.backgroundColor = UIColor.clear
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
                
                cell.delegate = self
                cell.refTableView = self.tableViewPersonnelService
                
//                if(self.personnelServiceList.count) != HelperConstant.minimumBlocks{
                    cell.lblTitle.text = personnelServiceList[indexPath.row-1].link
                    cell.lblTitleDesc.text = personnelServiceList[indexPath.row-1].desc
                    cell.lblRoleDesc.text = personnelServiceList[indexPath.row-1].roleDesc
                    
                    cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
//                } else {
//                    cell.cellIndex = indexPath
//                    cell.lblTitle.text = personnelServiceList[indexPath.row].link
//                    cell.lblTitleDesc.text = personnelServiceList[indexPath.row].desc
//                    cell.lblRoleDesc.text = personnelServiceList[indexPath.row].roleDesc
//                }
                
                cell.backgroundColor = UIColor.clear
                return cell
            }
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension ServiceAddListingContentAssociatedViewController: AddMoreCellDelegate{
    func newText(text: String, textDesc:String, textRoleDesc:String, refTableView: UITableView?, selectedServiceListingList:ServiceListingList?) {
        
        let associatedListDetail = ServiceListData()
        associatedListDetail.link = text
        associatedListDetail.desc = textDesc
        associatedListDetail.roleDesc = textRoleDesc
        
        if(refTableView == self.tableViewFacilityService) {
            if textDesc.isEmpty {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_primary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
            } else if let serviceList = selectedServiceListingList {
                associatedListDetail.id = serviceList.id
                associatedListDetail.link = serviceList.title
                
                if facilityServiceList.count < HelperConstant.minimumBlocks {
                    if facilityServiceList.map({ $0.id }).contains(serviceList.id) {
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                    } else {
                        isUpdate = true
                        facilityServiceList.append(associatedListDetail)
                        tableViewFacilityService.reloadData()
                    }
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_facility_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else if refTableView == tableViewPersonnelService {
            if textDesc.isEmpty {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_primary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
            } else if textRoleDesc.isEmpty {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_primary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
            } else if let serviceList = selectedServiceListingList {
                associatedListDetail.id = serviceList.id
                associatedListDetail.link = serviceList.title
                
                if personnelServiceList.count < HelperConstant.minimumBlocks {
                    if personnelServiceList.map({ $0.id }).contains(serviceList.id) {
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                        return
                    }
                    
                    isUpdate = true
                    personnelServiceList.append(associatedListDetail)
                    tableViewPersonnelService.reloadData()
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_personnel_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

extension ServiceAddListingContentAssociatedViewController : TitleDescViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        self.isUpdate = true
        if(refTableView == self.tableViewFacilityService && self.facilityServiceList.count > 0){
            self.facilityServiceList.remove(at: cellIndex?.row ?? 0)
            self.tableViewFacilityService.reloadData()
        } else if(refTableView == self.tableViewPersonnelService && self.personnelServiceList.count > 0){
            self.personnelServiceList.remove(at: cellIndex?.row ?? 0)
            self.tableViewPersonnelService.reloadData()
        }
    }
}

extension ServiceAddListingContentAssociatedViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        if tableView == tableViewFacilityService {
            constraintstableViewFacilityServiceHeight.constant = size.height
        } else if tableView == tableViewPersonnelService {
            constraintstableViewPersonnelServiceHeight.constant = size.height
        }
    }
}

/**
 * AddMoreCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol AddMoreCellDelegate {
    func newText(text:String, textDesc:String, textRoleDesc:String, refTableView: UITableView?, selectedServiceListingList:ServiceListingList?)
}

/**
 * AddMoreCell display Category list of product with Product option in collection on UITableViewCell.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class AddMoreCell : UITableViewCell {
    @IBOutlet weak var dropDownList: DropDown!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var textViewRoleDesc: KMPlaceholderTextView!
    @IBOutlet weak var ViewRoleDesc: UIView!
    @IBOutlet weak var btnAddMore: UIButton!
    
    var selectedServiceListingList:ServiceListingList?
    
    static let inputViewCellSize:CGFloat = 291
    
    var delegate: AddMoreCellDelegate?
    var refTableView: UITableView?
    
    @IBAction func methodAddMoreAction(_ sender: UIButton) {
        
        if(delegate != nil){
            
            if((self.textField.text?.length)! > 0){
                delegate?.newText(text: textField.text ?? "", textDesc:textViewDesc.text ?? "", textRoleDesc: textViewRoleDesc != nil ? (textViewRoleDesc.text ?? "") : "" , refTableView: refTableView, selectedServiceListingList: nil)
            } else if(selectedServiceListingList != nil){
                delegate?.newText(text: "", textDesc:textViewDesc.text ?? "", textRoleDesc: textViewRoleDesc != nil ? (textViewRoleDesc.text ?? "") : "" , refTableView: refTableView, selectedServiceListingList: selectedServiceListingList)
            }
        }
        
        self.textField.text = ""
        self.textViewDesc.text = ""
        self.dropDownList.text = ""
        
        if(self.textViewRoleDesc != nil && self.textViewRoleDesc.text != nil){
            self.textViewRoleDesc.text = ""
        }
    }
    
    func updateDropDown(listInfo:[ServiceListingList]){
        
        self.textField.isHidden = true
        
        if(listInfo.count > 0){
            
            // Array value listing
            dropDownList.optionArray = listInfo.map{($0.title ?? "") }
            
            //Its Id Values and its optional
            dropDownList.optionIds = listInfo.map{Int($0.id ?? "0" ) ?? 0}
            
            // The the Closure returns Selected Index and String
            dropDownList.didSelect{(selectedText , index ,id) in
                print("Selected String: \(selectedText) \n index: \(index), Id: \(id)")
                
                if(self.selectedServiceListingList == nil){
                    self.selectedServiceListingList = ServiceListingList(id: "\(id)", title: selectedText)
                } else {
                    self.selectedServiceListingList?.id = "\(id)"
                    self.selectedServiceListingList?.title = selectedText
                }
            }
            ////////////////////////////////////////////////////////////
        }
    }
}

/**
 * TitleDescViewDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol TitleDescViewDelegate {
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?)
}

/**
 * TitleDescView display Category list of product with Product option in collection on UITableViewCell.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class TitleDescView : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleDesc: UILabel!
    @IBOutlet weak var lblRoleDesc: UILabel!
    
    static let inputValueViewCellSize:CGFloat = 60
    
    var delegate:TitleDescViewDelegate?
    var cellIndex: IndexPath?
    var refTableView: UITableView?
    
    @IBAction func methodCloseAction(_ sender: UIButton) {
        
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            if(delegate != nil){
                delegate?.removeCellInfo(cellIndex: cellIndex, refTableView: refTableView)
            }
        }
    }
}

//
//  ServiceAddInstructionalFrontOfficeViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 28/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

import iOSDropDown

/**
 * ServiceAddInstructionalFrontOfficeViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddInstructionalFrontOfficeViewController : LMBaseViewController{
    
    fileprivate let formNumber = 6
    
    // Instructional or Front office
    @IBOutlet weak var btnInstructional: UIButton!
    @IBOutlet weak var btnFrontOffice: UIButton!
    @IBOutlet weak var viewInstructionalDetail: UIView!
    @IBOutlet weak var viewFrontOfficeDetail: UIView!
    
    // Instructional
    @IBOutlet weak var dropDownInstructionalType: DropDown! {
        didSet {
            dropDownInstructionalType.listHeight = 100
        }
    }
    @IBOutlet weak var textFieldInstructionalNumber: RYFloatingInput!
    fileprivate var radioBtnForTextField:UIButton?
    
    // Front office
    @IBOutlet weak var btnDirectMemberShipYes: UIButton!
    @IBOutlet weak var btnDirectMemberShipNo: UIButton!
    @IBOutlet weak var viewDirectMemberShipDetail: UIView!

    //Facility Rental
    @IBOutlet weak var btnFacilityRentalYes: UIButton!
    @IBOutlet weak var btnFacilityRentalNo: UIButton!
    @IBOutlet weak var viewServiceGain: UIView!
    @IBOutlet weak var viewFacilityGain: UIView!
    @IBOutlet weak var viewFacilityRental: UIView!
    
    @IBOutlet weak var tableViewServiceGain: BIDetailTableView! {
        didSet {
            tableViewServiceGain.sizeDelegate = self
        }
    }
    @IBOutlet weak var tableViewFaciliteGain: BIDetailTableView!{
        didSet {
            tableViewFaciliteGain.sizeDelegate = self
        }
    }
    
    @IBOutlet weak var constraintServiceGainHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintFaciliteGainHeight: NSLayoutConstraint!
    
//    @IBOutlet weak var dropDownFacilityRental: DropDown!
    @IBOutlet weak var tableViewFacilityRental: BIDetailTableView!{
        didSet {
            tableViewFacilityRental.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintFacilityRentalHeight: NSLayoutConstraint!
    @IBOutlet weak var textFieldFacilityRental: RYFloatingInput!
    
    // Listing Include
    @IBOutlet weak var btnIncludedInListingYes: UIButton!
    @IBOutlet weak var btnIncludedInListingNo: UIButton!
    @IBOutlet weak var viewIncludedInListing: UIView!
    @IBOutlet weak var tableViewRequireFacilityRental: BIDetailTableView!{
        didSet {
            tableViewRequireFacilityRental.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintRequireFacilityRentalHeight: NSLayoutConstraint!
    
    // Additional Service option
    @IBOutlet weak var textFieldAdditionalServiceOptionTitle: RYFloatingInput!
    @IBOutlet weak var textViewAdditionalServiceOptionDesc: KMPlaceholderTextView!
    
    @IBOutlet weak var tableViewAddedCost: BIDetailTableView!{
        didSet {
            tableViewAddedCost.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsAddCostTableViewHeight:NSLayoutConstraint!
    @IBOutlet weak var viewTableViewAddedCost: UIView!
    
    @IBOutlet weak var textFieldAdditionalCost: RYFloatingInput!
    @IBOutlet weak var textViewAdditionalCostDesc: KMPlaceholderTextView!
    
    // Event
    @IBOutlet weak var btnEventYes: UIButton!
    @IBOutlet weak var btnEventNo: UIButton!
    @IBOutlet weak var viewEventDetail: UIView!
    
    @IBOutlet weak var dropDownEventType: DropDown!
    @IBOutlet weak var textFieldSpecificEvent: RYFloatingInput!
    
    // Event Classification
    @IBOutlet weak var btnEventClassificationStandard: UIButton!
    @IBOutlet weak var btnEventClassificationCharitable: UIButton!
    @IBOutlet weak var viewEventClassificationCharitable: UIView!
    @IBOutlet weak var textFieldSpecificEventCharitable: RYFloatingInput!
    @IBOutlet weak var textFieldSpecificEventPercentDonate: RYFloatingInput!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var eventTypeDetail:DetailData?
    fileprivate var serviceAddSpecificTargetGoalViewController:ServiceAddSpecificTargetGoalViewController?
    fileprivate var serviceEvent:ServiceEvent?
    
    // Var
    fileprivate var serviceGainHeight:CGFloat = 0
    fileprivate var facilitieGainHeight:CGFloat = 0
    
    fileprivate var tableViewListSelection:TableViewListSelectionViewController?
    
    fileprivate var addedCostInfoList:[AddedCostInfo] = []
    fileprivate var requirementFacilityRentalList:[String] = []

    var serviceGainList:[ServiceListingList]? = []
    var faciliteGainList:[ServiceListingList]? = []
    var faciliteRentalList:[ServiceListingList]? = []
    
    fileprivate var isReloadServiceGainTableView = false
    fileprivate var isReloadFacilityGainTableView = false
    
    fileprivate var presenter = ServiceAddInstructionalFrontOfficePresenter()
    
    fileprivate var serviceInstructor:ServiceInstructor?
//    fileprivate var serviceAddEventClassificationViewController:ServiceAddEventClassificationViewController?
    
    fileprivate var instructionTypeDetail:DetailData?
//    fileprivate var facilityRentalDetail:DetailData?
    
    var serviceAddRequest:ServiceAddRequest?
    
    var serviceType : [ListingDataDetail]?
    var screenName:String = ""
    
    fileprivate var isUpdate = false
    fileprivate var isTableUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        serviceType = ServiceAddPresenter.parentListingList?.parentListingData?.services_types
        reloadChildListing()
                
        // TODO: for testing purpose
//        dummyData()
        
        presenter.getServiceProviderList()
        presenter.getFacilityServiceList()
        
        if(screenName.length > 0){
            lbl_NavigationTitle.text = screenName
        }
        
        setScreenData()
        
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableViewListSelection = nil
//        serviceAddEventClassificationViewController = nil
        serviceAddSpecificTargetGoalViewController = nil
    }
}

extension ServiceAddInstructionalFrontOfficeViewController{
    
    fileprivate func reloadChildListing(){
        
        if(serviceType != nil && (serviceType?.count)! > 0){
            for service in serviceType!{
                presenter.serverBatchChildListingRequest(parentId: service.listing_id ?? "")
            }
        }
    }
    
    func updateServiceTypeTableList(){
        //tableView.reloadData()
        
        configDropDown()
        
    }
    
//    fileprivate func dummyData(){
//        var selectionDataList:[TableViewListSelectionData] = []
//        var selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "1"
//        selectionDataInfo.title = "Test 1"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "2"
//        selectionDataInfo.title = "Test 2"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "3"
//        selectionDataInfo.title = "Test 3"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "4"
//        selectionDataInfo.title = "Test 4"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "5"
//        selectionDataInfo.title = "Test 5"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "6"
//        selectionDataInfo.title = "Test 6"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        serviceGainList = selectionDataList
//
//        selectionDataList = []
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "1"
//        selectionDataInfo.title = "Sample 1"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "2"
//        selectionDataInfo.title = "Sample 2"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "3"
//        selectionDataInfo.title = "Sample 3"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "4"
//        selectionDataInfo.title = "Sample 4"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "5"
//        selectionDataInfo.title = "Sample 5"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "6"
//        selectionDataInfo.title = "Sample 6"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        faciliteGainList = selectionDataList
//
//        selectionDataList = []
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "1"
//        selectionDataInfo.title = "Test 1"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "2"
//        selectionDataInfo.title = "Test 2"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "3"
//        selectionDataInfo.title = "Test 3"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "4"
//        selectionDataInfo.title = "Test 4"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "5"
//        selectionDataInfo.title = "Test 5"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        selectionDataInfo = TableViewListSelectionData()
//        selectionDataInfo.id = "6"
//        selectionDataInfo.title = "Test 6"
//        selectionDataInfo.selectionStatus = false
//        selectionDataList.append(selectionDataInfo)
//
//        faciliteRentalList = selectionDataList
//    }
}

extension ServiceAddInstructionalFrontOfficeViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData() {
        viewFrontOfficeDetail.isHidden = true
        
        textFieldInstructionalNumber.isHidden = true
        textFieldInstructionalNumber.input.text = ""
        btnInstructional.isSelected = false
        btnFrontOffice.isSelected = false
        
        viewServiceGain.isHidden = true
        viewFacilityGain.isHidden = true
        viewFacilityRental.isHidden = true
        viewIncludedInListing.isHidden = true
        viewTableViewAddedCost.isHidden = true
        
        btnIncludedInListingYes.isSelected = true
        btnIncludedInListingNo.isSelected = false
        
        tableViewRequireFacilityRental.backgroundColor = UIColor.clear
        
        // Front office
//        viewDirectMemberShipDetail.isHidden = true
        
        configTextField()
        configDropDown()
        
        //
        if let serviceInstructor = serviceAddRequest?.service_instructor {
            self.serviceInstructor = serviceInstructor
            
            // instructional office
            if serviceInstructor.inFo_office == "Instructional" { // Instructional
                methodInstructionalFrontOfficeAction(btnInstructional)
                
                dropDownInstructionalType.text = serviceInstructor.instructionalTypeTitle
                
                if instructionTypeDetail == nil {
                    instructionTypeDetail = DetailData()
                }
                
                instructionTypeDetail?.id = serviceInstructor.instructionalTypeId
                instructionTypeDetail?.title = serviceInstructor.instructionalTypeTitle
                
                textFieldInstructionalNumber.isHidden = serviceInstructor.instructionalTypeNumber?.isEmpty ?? true
                textFieldInstructionalNumber.input.text = serviceInstructor.instructionalTypeNumber

            } else { // front_office
                methodInstructionalFrontOfficeAction(btnFrontOffice)
                
                if serviceInstructor.frontOfficeDirectMembership == "1" {
                    methodFrontDirectMemberShipAction(btnDirectMemberShipYes)
                }
                
                if serviceInstructor.rental == "1" {
                    methodFacilityRentalAction(btnFacilityRentalYes)

                    textFieldFacilityRental.input.text = serviceInstructor.facilityRentalClassification
                    
                    requirementFacilityRentalList = serviceInstructor.requirementFacility ?? []
                    tableViewRequireFacilityRental.reloadData()
                    
                    textFieldAdditionalServiceOptionTitle.input.text = serviceInstructor.additionalServiceOptionName?.joined(separator: "\n")
                    
                    textViewAdditionalServiceOptionDesc.text = serviceInstructor.additionalServiceOptionDesc?.joined(separator: "\n")
                    
                    if let additionalPriceList = serviceInstructor.additionalServicePriceList {
                        let optionNames = serviceInstructor.additionalServiceOptionName
                        let optionDescs = serviceInstructor.additionalServiceOptionDesc
                        for (index, priceItem) in additionalPriceList.enumerated() {
                            let addedCost = AddedCostInfo()
                            
                            if let optionNames = optionNames, optionNames.count > index {
                                addedCost.name = optionNames[index]
                            }
                            if let optionDescs = optionDescs, optionDescs.count > index {
                                addedCost.desc = optionDescs[index]
                            }
                            
                            addedCost.price = priceItem.name
                            addedCost.pricedesc = priceItem.pricedesc

                            addedCostInfoList.append(addedCost)
                        }
                    }
                    
                    tableViewAddedCost.reloadData()
                    viewTableViewAddedCost.isHidden = addedCostInfoList.count == 0
                }
            }
        } else {
            methodInstructionalFrontOfficeAction(btnInstructional)
        }
        
        // Event form
        viewEventDetail.isHidden = true
        dropDownEventType.isHidden = true
        
        textFieldSpecificEvent.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Specific event type")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        textFieldSpecificEventCharitable.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Cause for the charitable event")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        textFieldSpecificEventPercentDonate.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Percent of funds that will be donated")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        textFieldSpecificEventPercentDonate.input.keyboardType = UIKeyboardType.numberPad
        
        // Travel Service
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownEventType.optionArray = ["Race", "Admission", "Tryouts"]
        //Its Id Values and its optional
        dropDownEventType.optionIds = [1, 2, 3]
        
        // The the Closure returns Selected Index and String
        dropDownEventType.didSelect { [weak self] (selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            if self?.eventTypeDetail == nil {
                self?.eventTypeDetail = DetailData()
            }
            self?.eventTypeDetail?.id = "\(id)"
            self?.eventTypeDetail?.title = selectedText
        }
        ////////////////////////////////////////////////////////////
        
        if(serviceAddRequest?.service_event != nil){
            methodEventAction(btnEventYes)
            serviceEvent = serviceAddRequest?.service_event
            
            eventTypeDetail = DetailData()
            eventTypeDetail?.id = serviceEvent?.typeTitleId
            eventTypeDetail?.title = serviceEvent?.typeTitle
            dropDownEventType.text = serviceEvent?.typeTitle
            textFieldSpecificEvent.input.text = serviceEvent?.specificEventType
            
            if(serviceEvent?.specificEventType == "1"){
                methodEventClassificationAction(btnEventClassificationCharitable)
            }
            
            if(serviceEvent?.causeCharitableEvent != nil && serviceEvent?.causeCharitablePercentage != nil){
                
                if(serviceEvent?.causeCharitableEvent != nil && (serviceEvent?.causeCharitableEvent?.length)! > 0){
                    textFieldSpecificEventCharitable.input.text = serviceEvent?.causeCharitableEvent ?? ""
                }
                
                if(serviceEvent?.causeCharitablePercentage != nil && (serviceEvent?.causeCharitablePercentage?.length)! > 0){
                    textFieldSpecificEventPercentDonate.input.text = serviceEvent?.causeCharitablePercentage ?? ""
                }
            } else {
                methodEventClassificationAction(btnEventClassificationStandard)
            }
        }
    }
    
    fileprivate func configTextField(){
        
        // Instructional
        textFieldInstructionalNumber.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Number")
                .maxLength(HelperConstant.LIMIT_REPEAT_TIME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        textFieldInstructionalNumber.input.keyboardType = UIKeyboardType.numberPad
        
        // Front office
        textFieldFacilityRental.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Facility rental classification")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        // textFieldAdditionalServiceOptionTitle
        textFieldAdditionalServiceOptionTitle.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("additional service name")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        textViewAdditionalServiceOptionDesc.placeholder = "Additional service description"
        
        // textFieldAdditionalCost
        textFieldAdditionalCost.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                //.placeholer("What is price of this additional service option?")
                .placeholer("price")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        textFieldAdditionalCost.input.keyboardType = UIKeyboardType.numberPad
        
        textViewAdditionalCostDesc.placeholder = "Instructions for purchasing \"additional service\" directly:"
    }
    
    fileprivate func configDropDown(){
        if(serviceType != nil && serviceType![0].subcat != nil){
            
            // Travel Service
            ////////////////////////////////////////////////////////////
            // The list of array to display. Can be changed dynamically
            dropDownInstructionalType.optionArray = serviceType![0].subcat?.map{$0.listing_title ?? ""} ?? []
            dropDownInstructionalType.optionIds = serviceType![0].subcat?.map{Int($0.listing_id ?? "0") ?? 0}
            
            //
            // The the Closure returns Selected Index and String
            dropDownInstructionalType.didSelect{ [weak self] selectedText, index, id in
                guard let self = self else { return }
                print("Selected String: \(selectedText) \n index: \(index)")
                
                if self.instructionTypeDetail == nil {
                    self.instructionTypeDetail = DetailData()
                }
                
                self.instructionTypeDetail?.id = "\(id)"
                self.instructionTypeDetail?.title = selectedText
                
                let optionIds = self.dropDownInstructionalType.optionIds
                let isTextFieldHidden: Bool
                let placeholder: String
                if id == optionIds?[1] {
                    isTextFieldHidden = false
                    placeholder = "Number of entrants for group lesson"
                } else if id == optionIds?[2] {
                    isTextFieldHidden = false
                    placeholder = "Number of sessions"
                } else if id == optionIds?[4] || id == optionIds?[5] {
                    isTextFieldHidden = false
                    placeholder = "Number of days"
                } else {
                    isTextFieldHidden = true
                    placeholder = "Number"
                }
                
                self.textFieldInstructionalNumber.input.placeholder = placeholder
                self.textFieldInstructionalNumber.isHidden = isTextFieldHidden
                self.textFieldInstructionalNumber.input.text = ""
            }
        }
        ////////////////////////////////////////////////////////////
    }
    
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if btnInstructional.isSelected {
            if instructionTypeDetail == nil {
                message = LocalizationKeys.error_instructional_selection.getLocalized()
                showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            //
            if let instructionId = instructionTypeDetail?.id?.trim(), instructionId.isEmpty {
                message = LocalizationKeys.error_instructional_count.getLocalized()
                showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            if !textFieldInstructionalNumber.isHidden, let instructionalNumber = textFieldInstructionalNumber.text()?.trim(), instructionalNumber.isEmpty {
                message = LocalizationKeys.error_instructional_count.getLocalized()
                showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            serviceInstructor = ServiceInstructor()
            
            serviceInstructor?.inFo_office = "Instructional"
            serviceInstructor?.instructionalTypeId = instructionTypeDetail?.id
            serviceInstructor?.instructionalTypeTitle = instructionTypeDetail?.title
            
            if !textFieldInstructionalNumber.isHidden {
                serviceInstructor?.instructionalTypeNumber = textFieldInstructionalNumber.input.text
            }
            
            serviceInstructor?.frontOfficeDirectMembership = nil
            serviceInstructor?.serviceGained = nil
            serviceInstructor?.facilitiesGained = nil
            serviceInstructor?.rental = nil
            serviceInstructor?.facilityRental = nil
            serviceInstructor?.facilityRentalClassification = nil
            serviceInstructor?.requirementFacilityRentalList = nil
            serviceInstructor?.additionalServiceOptionName = nil
            serviceInstructor?.additionalServiceOptionDesc = nil
            serviceInstructor?.additionalServicePriceList = nil
        } else {
            serviceInstructor = ServiceInstructor()
            serviceInstructor?.instructionalTypeId = nil
            serviceInstructor?.instructionalTypeTitle = nil
            serviceInstructor?.instructionalTypeNumber = nil
            
            serviceInstructor?.inFo_office = "front_office"
            
            if btnIncludedInListingNo.isSelected, addedCostInfoList.isEmpty {
                message = LocalizationKeys.error_listing_additional_cost.getLocalized()
                showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            if btnDirectMemberShipYes.isSelected {
                serviceInstructor?.frontOfficeDirectMembership = btnDirectMemberShipYes.isSelected ? "1" : "0"
                serviceInstructor?.serviceGained = serviceGainList?.compactMap { service -> DetailData? in
                    if service.selectionStatus {
                        let info = DetailData()
                        info.id = service.id
                        info.title = service.title
                        return info
                    }
                    return nil
                }
                
                serviceInstructor?.facilitiesGainedDirectMembership = faciliteGainList?.compactMap { service -> DetailData? in
                    if service.selectionStatus {
                        let info = DetailData()
                        info.id = service.id
                        info.title = service.title
                        return info
                    }
                    return nil
                }
            }
        
            if btnFacilityRentalYes.isSelected {
                serviceInstructor?.rental = btnFacilityRentalYes.isSelected ? "1" : "0"
                
                let rentalFacilitiesGained: [DetailData]? = faciliteRentalList?.compactMap { service -> DetailData? in
                    if service.selectionStatus {
                        let info = DetailData()
                        info.id = service.id
                        info.title = service.title
                        return info
                    }
                    return nil
                }
                
                if let noSelection = rentalFacilitiesGained?.isEmpty, noSelection {
                    message = LocalizationKeys.error_front_office_facility_rental_selection.getLocalized()
                    showBannerAlertWith(message: message, alert: .error)
                    return false
                }
                serviceInstructor?.facilitiesGained = rentalFacilitiesGained
                serviceInstructor?.facilityRentalClassification = textFieldFacilityRental.text()
                serviceInstructor?.requirementFacility = requirementFacilityRentalList
                
                // pricing object
                var additionalServiceOptionName = [String]()
                var additionalServiceOptionDesc = [String]()
                var additionalServicePriceList = [FacilityRentalAddedCost]()
                for addedCost in addedCostInfoList {
                    additionalServiceOptionName.append(addedCost.name)
                    additionalServiceOptionDesc.append(addedCost.desc)
                    if let priceItem = FacilityRentalAddedCost(JSON: ["name": addedCost.price, "pricedesc": addedCost.pricedesc]) {
                        additionalServicePriceList.append(priceItem)
                    } else {
                        additionalServicePriceList.append(FacilityRentalAddedCost())
                    }
                }
                serviceInstructor?.additionalServiceOptionName = additionalServiceOptionName
                serviceInstructor?.additionalServiceOptionDesc = additionalServiceOptionDesc
                serviceInstructor?.additionalServicePriceList = additionalServicePriceList
            }
        }
        
        if serviceAddRequest == nil {
            serviceAddRequest = ServiceAddRequest()
        }
        
        serviceAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        
        if serviceAddRequest?.service_id == nil {
            serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        }
        
        serviceAddRequest?.service_instructor = serviceInstructor
        
        // Event
        if btnEventYes.isSelected {
            if eventTypeDetail == nil {
                message = LocalizationKeys.error_event_type.getLocalized()
                showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            if let eventTitle = textFieldSpecificEvent.text()?.trim(), eventTitle.isEmpty {
                message = LocalizationKeys.error_event_type_manual.getLocalized()
                showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            //
            if btnEventClassificationCharitable.isSelected {
                if let charitable = textFieldSpecificEventCharitable.text()?.trim(), charitable.isEmpty {
                    message = LocalizationKeys.error_event_charitable.getLocalized()
                    showBannerAlertWith(message: message, alert: .error)
                    return false
                }
                
                if let percentage = textFieldSpecificEventPercentDonate.text()?.trim(), percentage.isEmpty {
                    message = LocalizationKeys.error_event_charitable_donate.getLocalized()
                    showBannerAlertWith(message: message, alert: .error)
                    return false
                }
            }
            
            if serviceEvent == nil {
                serviceEvent = ServiceEvent()
            }
            
            serviceEvent?.typeTitleId = eventTypeDetail?.id
            serviceEvent?.typeTitle = eventTypeDetail?.title?.trim()
            serviceEvent?.specificEventType = textFieldSpecificEvent.text()?.trim()
            
            if btnEventClassificationCharitable.isSelected {
                serviceEvent?.causeCharitableEvent = textFieldSpecificEventCharitable.text()
                serviceEvent?.causeCharitablePercentage = textFieldSpecificEventPercentDonate.text()
            } else {
                serviceEvent?.causeCharitableEvent = nil
                serviceEvent?.causeCharitablePercentage = nil
            }
            
            serviceAddRequest?.service_event = serviceEvent
        }
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        if(btnInstructional.isSelected){
            if(serviceAddRequest?.service_instructor?.instructionalTypeId == nil || ( serviceInstructor?.instructionalTypeTitle != nil && (serviceInstructor?.instructionalTypeTitle?.length)! > 0 && dropDownInstructionalType.text !=  (serviceInstructor?.instructionalTypeTitle ?? ""))){
                isUpdate = true
            }
            
            if(!textFieldInstructionalNumber.isHidden  && serviceInstructor?.instructionalTypeNumber != nil && (serviceInstructor?.instructionalTypeNumber?.length)! > 0 && textFieldInstructionalNumber.input.text !=  serviceInstructor?.instructionalTypeNumber){
                isUpdate = true
            }
        } else {
            if(btnDirectMemberShipYes.isSelected){
                
                let status: Bool = serviceInstructor?.frontOfficeDirectMembership == "1"
                if btnDirectMemberShipYes.isSelected != status {
                    isUpdate = true
                }
                
                if btnDirectMemberShipYes.isSelected {
                    if(isUpdate){
                        isUpdate = true
                    }
                    
//                    if(serviceInstructor?.facilityRentalId != (facilityRentalDetail?.id ?? "")){
//                        isUpdate = true
//                    }
//
//                    if(serviceInstructor?.facilityRentalTitle != (facilityRentalDetail?.title ?? "")){
//                        isUpdate = true
//                    }
                    
                    if(serviceInstructor?.facilityRentalClassification != (textFieldFacilityRental.text() ?? "")){
                        isUpdate = true
                    }
                    
                    if serviceInstructor?.additionalServiceOptionName?.first != textFieldAdditionalServiceOptionTitle.text() {
                        isUpdate = true
                    }
                    
                    if serviceInstructor?.additionalServiceOptionDesc?.first != textViewAdditionalServiceOptionDesc.text{
                        isUpdate = true
                    }
                }
            }
        }
        
        // Event
        if(serviceAddRequest?.service_event != nil){
            
            if(eventTypeDetail?.id != (serviceEvent?.typeTitleId ?? "")){
                isUpdate = true
            }
            
            if(eventTypeDetail?.title != (serviceEvent?.typeTitle ?? "")){
                isUpdate = true
            }
            
            if(serviceEvent?.specificEventType == "1" && !btnEventClassificationStandard.isSelected){
                isUpdate = true
            }
            
            if(serviceEvent?.causeCharitableEvent != textFieldSpecificEventCharitable.input.text){
                isUpdate = true
            }
            
            if(serviceEvent?.causeCharitablePercentage != textFieldSpecificEventPercentDonate.input.text){
                isUpdate = true
            }
            
        } else {
            isUpdate = true
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        serviceAddSpecificTargetGoalViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddSpecificTargetGoalViewController.nameOfClass) as ServiceAddSpecificTargetGoalViewController
        
        serviceAddSpecificTargetGoalViewController?.serviceAddRequest = serviceAddRequest
        serviceAddSpecificTargetGoalViewController?.isEditingService = isEditingService
        navigationController?.pushViewController(serviceAddSpecificTargetGoalViewController!, animated: true)
    }
}

//MARK:- IBAction method implementation
//MARK:-
extension ServiceAddInstructionalFrontOfficeViewController{
    
    // Instructional or Front office
    @IBAction func methodInstructionalFrontOfficeAction(_ sender: UIButton) {

        btnInstructional.isSelected = false
        btnFrontOffice.isSelected = false
        isUpdate = true

        if(btnInstructional == sender){
            btnInstructional.isSelected = true
            viewFrontOfficeDetail.isHidden = true
            viewInstructionalDetail.isHidden = false
        } else if(btnFrontOffice == sender){
            btnFrontOffice.isSelected = true
            viewInstructionalDetail.isHidden = true
            viewFrontOfficeDetail.isHidden = false
        }
    }
    
    // Front office: Direct membership
    @IBAction func methodFrontDirectMemberShipAction(_ sender: UIButton) {
        
        btnDirectMemberShipYes.isSelected = false
        btnDirectMemberShipNo.isSelected = false
        isUpdate = true
        
        if(btnDirectMemberShipYes == sender){
            btnDirectMemberShipYes.isSelected = true
//            viewDirectMemberShipDetail.isHidden = false
            viewServiceGain.isHidden = false
            tableViewServiceGain.reloadData()
            
            viewFacilityGain.isHidden = false
            tableViewFaciliteGain.reloadData()
            
        } else if(btnDirectMemberShipNo == sender){
            btnDirectMemberShipNo.isSelected = true
//            viewDirectMemberShipDetail.isHidden = true
            viewServiceGain.isHidden = true
            viewFacilityGain.isHidden = true
        }
    }
    
    @IBAction func methodFacilityRentalAction(_ sender: UIButton) {
        isUpdate = true
        btnFacilityRentalYes.isSelected = false
        btnFacilityRentalNo.isSelected = false
        
        if(btnFacilityRentalYes == sender){
            btnFacilityRentalYes.isSelected = true
            viewFacilityRental.isHidden = false
            
        } else if(btnFacilityRentalNo == sender){
            btnFacilityRentalNo.isSelected = true
            viewFacilityRental.isHidden = true
        }
    }
    
    @IBAction func methodIncludedInListingAction(_ sender: UIButton) {
        isUpdate = true
        btnIncludedInListingYes.isSelected = false
        btnIncludedInListingNo.isSelected = false
        
        if(btnIncludedInListingYes == sender){
            btnIncludedInListingYes.isSelected = true
            viewIncludedInListing.isHidden = true
            
        } else if(btnIncludedInListingNo == sender){
            btnIncludedInListingNo.isSelected = true
            viewIncludedInListing.isHidden = false
        }
    }
    
    @IBAction func methodAddCostAction(_ sender: UIButton) {
        let addedCostInfo = AddedCostInfo()
        
        if((textFieldAdditionalServiceOptionTitle.text()?.length)! > 0){
            addedCostInfo.name = textFieldAdditionalServiceOptionTitle.text() ?? ""
            
            textFieldAdditionalServiceOptionTitle.input.text = ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_plese_enter_cost_name.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        isUpdate = true
        if((textViewAdditionalServiceOptionDesc.text?.length)! > 0){
            addedCostInfo.desc = textViewAdditionalServiceOptionDesc.text ?? ""
            
            textViewAdditionalServiceOptionDesc.text = ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_please_enter_cost_description.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }

        if(btnIncludedInListingNo.isSelected){
            methodIncludedInListingAction(btnIncludedInListingYes)
            isUpdate = true
            if((textFieldAdditionalCost.text()?.length)! > 0){
                addedCostInfo.price = textFieldAdditionalCost.text() ?? ""
                
                textFieldAdditionalCost.input.text = ""
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_please_enter_price.getLocalized(), buttonTitle: nil, controller: nil)
                
                return
            }

            if((textViewAdditionalCostDesc.text?.length)! > 0){
                addedCostInfo.pricedesc = textViewAdditionalCostDesc.text ?? ""
                
                textViewAdditionalCostDesc.text = ""
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_please_enter_instructions.getLocalized(), buttonTitle: nil, controller: nil)
                
                return
            }
        }
        
        isUpdate = true
        
        addedCostInfoList.append(addedCostInfo)
        tableViewAddedCost.reloadData()
        viewTableViewAddedCost.isHidden = addedCostInfoList.count == 0
    }
    
    @IBAction func methodEventAction(_ sender: UIButton) {
        
        btnEventYes.isSelected = false
        btnEventNo.isSelected = false
        isUpdate = true
        if(btnEventYes == sender){
            
            if(btnEventYes.isSelected != sender.isSelected){
                methodEventClassificationAction(btnEventClassificationStandard)
            }
            
            dropDownEventType.isHidden = false
            btnEventYes.isSelected = true
            viewEventDetail.isHidden = false
            
            
        } else if(btnEventNo == sender){
            dropDownEventType.isHidden = true
            btnEventNo.isSelected = true
            viewEventDetail.isHidden = true
        }
    }
    
    @IBAction func methodEventClassificationAction(_ sender: UIButton) {
        
        btnEventClassificationStandard.isSelected = false
        btnEventClassificationCharitable.isSelected = false
        isUpdate = true
        if(btnEventClassificationStandard == sender){
            btnEventClassificationStandard.isSelected = true
            viewEventClassificationCharitable.isHidden = true
        } else if(btnEventClassificationCharitable == sender){
            btnEventClassificationCharitable.isSelected = true
            viewEventClassificationCharitable.isHidden = false
        }
    }
    
    //MARK:- Button Action method implementation
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddInstructionalFrontOfficeViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tableViewRequireFacilityRental){
//            if((requirementFacilityRentalList.count) != HelperConstant.minimumBlocks){
                return (requirementFacilityRentalList.count) + 1
//            } else {
//                return requirementFacilityRentalList.count
//            }
        } else if(tableViewServiceGain == tableView){
            return (serviceGainList != nil && (serviceGainList?.count)! > 0) ? (serviceGainList?.count)! : 0
        } else if(tableViewFaciliteGain == tableView){
            return (faciliteGainList != nil && (faciliteGainList?.count)! > 0) ?  (faciliteGainList?.count)! : 0
        } else if(tableViewFacilityRental == tableView){
            return (faciliteRentalList != nil && (faciliteRentalList?.count)! > 0) ?  (faciliteRentalList?.count)! : 0
        } else {
            return addedCostInfoList.count
        }
//        return 5
//
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableViewRequireFacilityRental == tableView){
            
            if(indexPath.row == 0){
                
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                
                cell.textField.placeholder = "Requirement for facility rental"
                cell.delegate = self
                cell.refTableView = tableViewRequireFacilityRental
                
                cell.backgroundColor = UIColor.clear
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                
                cell.delegate = self
                //                cell.cellIndex = indexPath
                cell.refTableView = tableViewRequireFacilityRental
                
//                if(requirementFacilityRentalList.count) != HelperConstant.minimumBlocks{
                    cell.lblTitle.text = requirementFacilityRentalList[indexPath.row-1]
                    
                    cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
//                } else {
//                    cell.cellIndex = indexPath
//                    cell.lblTitle.text = requirementFacilityRentalList[indexPath.row]
//                }
                
                cell.backgroundColor = UIColor.clear
                return cell
            }
            
        } else if(tableViewServiceGain == tableView){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessContactAddPersonnelCell.nameOfClass) as! BusinessContactAddPersonnelCell
            
            cell.cellIndex = indexPath
            cell.refTableView = tableView
            cell.delegate = self
            if let listingData: ServiceListingList = serviceGainList?[indexPath.row] {
                cell.lblTitle.text = listingData.title
                cell.btnCheck.isSelected = listingData.selectionStatus
                
                let isSelected = serviceAddRequest?.service_instructor?.serviceGained?.compactMap ({ $0.id }).contains(listingData.id) ?? false
                if !isTableUpdate, isSelected {
                    cell.btnCheck.isSelected = true
                    listingData.selectionStatus = true
                }
            }

            return cell
            
        } else if(tableViewFaciliteGain == tableView){
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessContactAddPersonnelCell.nameOfClass) as! BusinessContactAddPersonnelCell
            
            cell.cellIndex = indexPath
            cell.refTableView = tableView
            cell.delegate = self
            
            if let listingData: ServiceListingList = faciliteGainList?[indexPath.row] {
                cell.lblTitle.text = listingData.title
                cell.btnCheck.isSelected = listingData.selectionStatus
                
                let isSelected = serviceAddRequest?.service_instructor?.facilitiesGainedDirectMembership?.compactMap ({ $0.id }).contains(listingData.id) ?? false
                if !isTableUpdate, isSelected {
                    cell.btnCheck.isSelected = true
                    listingData.selectionStatus = true
                }
            }
            
            return cell
        } else if(tableViewFacilityRental == tableView){
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessContactAddPersonnelCell.nameOfClass) as! BusinessContactAddPersonnelCell
            
            cell.cellIndex = indexPath
            cell.refTableView = tableView
            cell.delegate = self
            
            if let listingData: ServiceListingList = faciliteRentalList?[indexPath.row] {
                cell.lblTitle.text = listingData.title
                cell.btnCheck.isSelected = listingData.selectionStatus
                
                let isSelected = serviceAddRequest?.service_instructor?.facilitiesGained?.compactMap ({ $0.id }).contains(listingData.id) ?? false
                if !isTableUpdate, isSelected {
                    cell.btnCheck.isSelected = true
                    listingData.selectionStatus = true
                }
            }

            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: AddedCostTableViewCell.nameOfClass) as! AddedCostTableViewCell
            
            if(addedCostInfoList.count > indexPath.row){
                let objInfo = addedCostInfoList[indexPath.row]
                
                cell.cellIndex = indexPath
                cell.delegate = self
                
                cell.lblName.text = objInfo.name
                cell.lblDesc.text = objInfo.desc
                cell.lblStatus.text = "Yes"
                
                if(objInfo.price.trim().length > 0){
                    cell.stackViewPrice.isHidden = false
                    cell.stackViewPriceDesc.isHidden = false
                    
                    cell.lblPrice.text = "$\(objInfo.price.trim())"
                    cell.lblPriceDesc.text = objInfo.pricedesc.trim()
                    
                    
                } else {
                    cell.stackViewPrice.isHidden = true
                    cell.stackViewPriceDesc.isHidden = true
                }
            }
            return cell
        }
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension ServiceAddInstructionalFrontOfficeViewController: BusinessContactAddPersonnelCellDelegate{
    func selectListItem(cellIndex: IndexPath?, status:Bool, tableView:UITableView?){
        
        if(tableView == tableViewFaciliteGain){
            //        if(therapyType != nil && (therapyType?.count)! > cellIndex?.row ?? 0){
            //            therapyType![cellIndex?.row ?? 0].selectionStaus = status
            //
            //            isUpdate = true
            //        }
            
            if(faciliteGainList != nil && (faciliteGainList?.count)! > cellIndex?.row ?? 0){
                faciliteGainList![cellIndex?.row ?? 0].selectionStatus = status
                isUpdate = true
                isTableUpdate = true
            }
        } else if(tableView == tableViewServiceGain){
            
            if(serviceGainList != nil && (serviceGainList?.count)! > cellIndex?.row ?? 0){
                serviceGainList![cellIndex?.row ?? 0].selectionStatus = status
                isUpdate = true
                isTableUpdate = true
            }
        } else if(tableView == tableViewFacilityRental){
            if(faciliteRentalList != nil && (faciliteRentalList?.count)! > cellIndex?.row ?? 0){
                faciliteRentalList![cellIndex?.row ?? 0].selectionStatus = status
                isUpdate = true
                isTableUpdate = true
            }
        }
    }
}

extension ServiceAddInstructionalFrontOfficeViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        if((requirementFacilityRentalList.count) < HelperConstant.minimumBlocks){
            isUpdate = true
            requirementFacilityRentalList.append(text)
            
            tableViewRequireFacilityRental.reloadData()
            
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension ServiceAddInstructionalFrontOfficeViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        if(requirementFacilityRentalList.count > 0){
            requirementFacilityRentalList.remove(at: cellIndex?.row ?? 0)
            
            tableViewRequireFacilityRental.reloadData()
        }
        
    }
}

extension ServiceAddInstructionalFrontOfficeViewController:AddedCostTableViewCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
        if(addedCostInfoList.count > 0){
            
            addedCostInfoList.remove(at: cellIndex?.row ?? 0)
            tableViewAddedCost.reloadData()

            viewTableViewAddedCost.isHidden = addedCostInfoList.count == 0
        }
    }
}

extension ServiceAddInstructionalFrontOfficeViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        if tableView == tableViewServiceGain {
            constraintServiceGainHeight.constant = size.height
        } else if tableView == tableViewFaciliteGain {
            constraintFaciliteGainHeight.constant = size.height
        } else if tableView == tableViewFacilityRental {
            constraintFacilityRentalHeight.constant = size.height
        } else if tableView == tableViewRequireFacilityRental {
            constraintRequireFacilityRentalHeight.constant = size.height
        } else if tableView == tableViewAddedCost {
            constraintsAddCostTableViewHeight.constant = size.height
        }
    }
}


/**
 * BusinessLocationAddMoreCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol AddedCostTableViewCellDelegate {
    func removeWebsiteInfo(cellIndex:IndexPath?)
}

class AddedCostTableViewCell: UITableViewCell {
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblPriceDesc : UILabel!
    @IBOutlet weak var btnRemove : UIButton!
    
    @IBOutlet weak var stackViewPrice: UIStackView!
    @IBOutlet weak var stackViewPriceDesc: UIStackView!
    
    var cellIndex:IndexPath?
    var delegate:AddedCostTableViewCellDelegate?
    
    @IBAction func methodCloseAction(_ sender: UIButton) {
        if(delegate != nil){
            delegate?.removeWebsiteInfo(cellIndex: cellIndex)
        }
    }
}

class ServiceAddedCostTableViewCell: UITableViewCell {
    @IBOutlet weak var lblAdditionalCostIndex : UILabel!
    @IBOutlet weak var lblIndex : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblPriceDesc : UILabel!
    @IBOutlet weak var stackViewPrice: UIStackView!
}

class ServiceTableViewCell: UITableViewCell {
    @IBOutlet weak var lblName : UILabel!
}

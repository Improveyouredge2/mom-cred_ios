//
//  ServiceDetailView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import Lightbox
import AVKit
import SafariServices

/**
 * ServiceDetailView is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagbtnous [CNSGSIN054]
 */
class ServiceDetailView : LMBaseViewController {
    
    @IBOutlet weak var btnOptionMenu : UIButton!
    
    // Profile image view
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn : UIImageView!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitleValue : UILabel!
    
    // Form 1 Overview
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblTotalPurchaseCount : UILabel!
    @IBOutlet weak var lblPurchasabilityStatus : UILabel!
    @IBOutlet weak var lblCreditStatus : UILabel!
    @IBOutlet weak var lblDonationStatus : UILabel!
    @IBOutlet weak var lblPurchaseCount : UILabel!
    @IBOutlet weak var lblTargetGoal : UILabel!
    @IBOutlet weak var lblLocationValue : UILabel!
    
    // Form 2 Calendar listing
    @IBOutlet weak var calendarInfoContainer: UIStackView!
    @IBOutlet weak var lblCalendarListingStatus : UILabel!
    @IBOutlet weak var lblCalendarDate : UILabel!
    @IBOutlet weak var lblCalendarRepeatStatus : UILabel!
    @IBOutlet weak var lblCalendarRepeatTimeFrame : UILabel!
    @IBOutlet weak var lblCalendarRepeat : UILabel!
    
    // Form 3 Standard / Exceptional
    @IBOutlet weak var lblExpType : UILabel!
    @IBOutlet weak var lblExpService : UILabel!
    @IBOutlet weak var lblListDisabilities : UILabel!
    
    // Form 4 Certificate
    @IBOutlet weak var lblCertificateName : UILabel!
    @IBOutlet weak var lblCertificateFreeServiceStatus: UILabel!
    @IBOutlet weak var lblCertificateFreeServiceCountLabel: UILabel!
    @IBOutlet weak var lblCertificateFreeService : UILabel!
    @IBOutlet weak var lblCertificateTrailService : UILabel!
    @IBOutlet weak var lblCertificateTrailServiceCountLabel: UILabel!
    @IBOutlet weak var lblCertificateTrailServiceCount: UILabel!

    @IBOutlet weak var viewExpTravelService : UIView!
    @IBOutlet weak var lblCertificateTravelServiceStatusTitle : UILabel!
    @IBOutlet weak var lblCertificateTravelServiceStatus : UILabel!
    @IBOutlet weak var lblCertificateTravelServiceType : UILabel!
    @IBOutlet weak var lblCertificateTravelServiceRequirement : UILabel!
    @IBOutlet weak var lblCertificateTravelServiceLocation : UILabel!
    
    // Form 5 Therapy
    @IBOutlet weak var viewTherapy : UIView!
    @IBOutlet weak var lblTherapyStatus : UILabel!
    @IBOutlet weak var lblTherapyType : UILabel!
    @IBOutlet weak var lblTherapyFieldCover : UILabel!
    @IBOutlet weak var lblTherapyPaymentType : UILabel!
    @IBOutlet weak var lblTherapyPaymentTypeDesc : UILabel!
    
    // Form 6 Instructional / Front office
    // Instructional
    @IBOutlet weak var viewInstructional : UIView!
    @IBOutlet weak var lblInstructionalType : UILabel!
    @IBOutlet weak var lblInstuctionalTypeCount : UILabel!
    
    // Front office
    @IBOutlet weak var viewFrontOffice : UIView!
    @IBOutlet weak var viewDirectMembership : UIView!
    @IBOutlet weak var lblDirectMembershipStatus : UILabel!
    
    @IBOutlet weak var lblFrontOffice : UILabel!
    @IBOutlet weak var lblServiceGainList : UIStackView!
    @IBOutlet weak var lblFacilitiesGainList : UIStackView!
    
    @IBOutlet weak var viewFacilities : UIView!
    @IBOutlet weak var lblFacilitiesStatus : UILabel!
    @IBOutlet weak var lblFacilitiesRental : UIStackView!
    @IBOutlet weak var lblFacilitiesRentalClassification : UILabel!
    @IBOutlet weak var lblRequirementFacilitiesRental : UILabel!
    @IBOutlet weak var lblAddedCostStatus : UILabel!
    
    // facilities added cost display
    @IBOutlet weak var tableViewFacilitiesAdditionalCost: BIDetailTableView! {
        didSet {
            tableViewFacilitiesAdditionalCost.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewFacilitiesAdditionalCostHeight: NSLayoutConstraint!
    
    // Form 7 Event
    @IBOutlet weak var viewEvent : UIView!
    @IBOutlet weak var lblEventStatus : UILabel!
    @IBOutlet weak var lblGeneralEventType : UILabel!
    @IBOutlet weak var lblEventType : UILabel!
    @IBOutlet weak var lblEventClassification : UILabel!
    @IBOutlet weak var viewCharitableOption : UIView!
    @IBOutlet weak var lblCharitableOptionName : UILabel!
    @IBOutlet weak var lblCharitableOptionPercentage : UILabel!
    
    // Form 8 Specific
    @IBOutlet weak var lblSpecificDesc : UILabel!
    
    // Form 9 Target
    @IBOutlet weak var lblSpecificTargetGoal : UILabel!
    @IBOutlet weak var lblWorkDuringService : UILabel!
    
    // Form 10 Service field
    // Field Service type display
    @IBOutlet weak var tableViewFieldServiceType: BIDetailTableView! {
        didSet {
            tableViewFieldServiceType.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewFieldServiceTypeHeight: NSLayoutConstraint!
    
    // Form 11 Price
    @IBOutlet weak var viewPrice : UIView!
    @IBOutlet weak var lblServicePrice : UILabel!
    @IBOutlet weak var lblServicePriceStatus : UILabel!
    @IBOutlet weak var tableViewServicePriceAdditionalCost: BIDetailTableView! {
        didSet {
            tableViewServicePriceAdditionalCost.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewServicePriceAdditionalCostHeight: NSLayoutConstraint!
    
    // Form 12 Skill and Age
    @IBOutlet weak var lblSkillLevel : UILabel!
    @IBOutlet weak var lblSkillMinAge : UILabel!
    @IBOutlet weak var lblSkillMaxAge : UILabel!
    @IBOutlet weak var lblSkillDurationType : UILabel!
    @IBOutlet weak var lblSkillDurationTime : UILabel!
    
    // Form 13 Content associated Service
    @IBOutlet weak var tableViewFieldListing: BIDetailTableView! {
        didSet {
            tableViewFieldListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewFieldListingHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewPersonnelListing: BIDetailTableView! {
        didSet {
            tableViewPersonnelListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewPersonnelListingHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tblInsideLookContentListing: BIDetailTableView! {
        didSet {
            tblInsideLookContentListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var tblInsideLookContentListingHeight: NSLayoutConstraint!
    @IBOutlet weak var tblInstructionalContentListing : BIDetailTableView! {
        didSet {
            tblInstructionalContentListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var tblInstructionalContentListingHeight: NSLayoutConstraint!

    
    // Form 14 Field Good Service
    @IBOutlet weak var tableViewGoodServiceListing: BIDetailTableView! {
        didSet {
            tableViewGoodServiceListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewGoodServiceListingHeight: NSLayoutConstraint!
    
    // Form 15 Additional notes
    @IBOutlet weak var tableViewAddNotesListing: BIDetailTableView! {
        didSet {
            tableViewAddNotesListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewAddNotesHeight: NSLayoutConstraint!
    
    // Form 16 External link
    @IBOutlet weak var tableViewExtenalLinkListing: BIDetailTableView! {
        didSet {
            tableViewExtenalLinkListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewExtenalLinkLisitingHeight: NSLayoutConstraint!
    
    // Form 17 External link
    @IBOutlet weak var tableViewPrePostReqListing: BIDetailTableView! {
        didSet {
            tableViewPrePostReqListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewPrePostReqListingHeight: NSLayoutConstraint!
    
    // Additional Document type display
    @IBOutlet weak var tableViewAdditionalDocument: BIDetailTableView! {
        didSet {
            tableViewAdditionalDocument.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewAdditionalDocumentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var viewCreateNew : UIView!
    
    // MARK:- Enthusiast
    @IBOutlet weak var viewServiceProviderDetail : UIView!
    @IBOutlet weak var viewServiceProviderEnthuaistDetail : UIView!
    
    // Profile image view
    @IBOutlet weak var imgServiceProviderProfile : ImageLayerSetup!
//    @IBOutlet weak var imgServiceProviderPlayBtn : UIImageView!
    @IBOutlet weak var collectionServiceProviderView : UICollectionView!
    @IBOutlet weak var constraintDocumentServiceProviderCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnPurchaseNow : UIButton!
    @IBOutlet weak var btnBuildPackage : UIButton!
    @IBOutlet weak var viewBuildPackage : UIView!
    
    @IBOutlet weak var lblServiceProviderTitleValue : UILabel!
    @IBOutlet weak var lblSpotValue : UILabel!
    @IBOutlet weak var lblPriceValue : UILabel!
    
    @IBOutlet private weak var navigationBar: NavigationBarView! {
        didSet {
            navigationBar.navigationController = navigationController
        }
    }
    
    fileprivate var menuArray: [HSMenu] = []
    fileprivate var serviceAddOverviewViewController:ServiceAddOverviewViewController?
    fileprivate var defaultDocumentCollectionHeight:CGFloat = 0.00
    
    fileprivate var logoutViewController:PMLogoutView?
    fileprivate var packagePaymentViewController:PackagePaymentViewController?
    fileprivate var presenter = ServiceDetailPresenter()
    
    var biAddRequest:BIAddRequest?
    var busiInfo:BIAddRequest?
    var serviceAddRequest:ServiceAddRequest? = ServiceAddRequest()
    var serviceId:String?
    
    var isHideEditOption = false
    var shouldRefreshContent: Bool = false
    
    var collectionContentHeightObserver: NSKeyValueObservation?

    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        self.scrollView.isHidden = true
        //self.btnOptionMenu.isHidden = true
        self.defaultDocumentCollectionHeight = self.constraintDocumentCollectionHeight.constant
        self.constraintDocumentCollectionHeight.constant = 0
        self.constraintDocumentServiceProviderCollectionHeight.constant = 0
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.edit.getLocalized())
        menuArray = [menu1]
        
        let profileImageUrl = PMUserDefault.getProfilePic()
        if(profileImageUrl != nil){
            self.imgProfile.sd_setImage(with: URL(string: (profileImageUrl!)))
        }
        
        self.constraintTableViewFacilitiesAdditionalCostHeight.constant = 10
        
        self.tableViewPersonnelListing.delegate = self
        self.tableViewPersonnelListing.dataSource = self

        // Remove after api success
        if(self.serviceId != nil && (self.serviceId?.length)! > 0){
            self.presenter.getSubCatServiceProviderList(subCatId: self.serviceId ?? "")
            
        } else {
            self.updateScreenInfo()
        }
        
        if(self.isHideEditOption){
            self.btnOptionMenu.isHidden = true
        }
        
        collectionContentHeightObserver = collectionView.observe(\.contentSize, options: [.new]) { [weak self] _, value in
            if let height = value.newValue?.height, height > 0 {
                self?.constraintDocumentCollectionHeight.constant = height
            }
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.serviceAddOverviewViewController = nil
        
        // update screen info with API calling
//        presenter.getBIDetail()
        updatePurchaseButton()
        NotificationCenter.default.addObserver(self, selector: #selector(updatePurchaseButton), name: .cartUpdated, object: nil)
        
        if shouldRefreshContent {
            shouldRefreshContent = false
            if let serviceId = serviceId, !serviceId.trim().isEmpty {
                presenter.getSubCatServiceProviderList(subCatId: serviceId)
            } else if let serviceId = serviceAddRequest?.service_id {
                presenter.getSubCatServiceProviderList(subCatId: serviceId)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .cartUpdated, object: nil)
    }
    
    @objc private func updatePurchaseButton() {
        if PMUserDefault.getAppToken() == nil || PMUserDefault.getAppToken()?.trim().isEmpty == true {
            btnPurchaseNow.isHidden = true
        } else {
            if let request = serviceAddRequest, ServiceCart.shared.serviceExistsInCart(request) {
                btnPurchaseNow.isEnabled = false
                btnPurchaseNow.setTitle("Added to Cart", for: .normal)
            } else {
                btnPurchaseNow.isEnabled = true
                btnPurchaseNow.setTitle("Add to Cart", for: .normal)
            }
        }
    }
    
    deinit {
        collectionContentHeightObserver?.invalidate()
    }
}

extension ServiceDetailView{
    @IBAction func actionCreateBI(_ sender: UIButton) {
//        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
//            biaddOverViewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddOverViewViewController.nameOfClass) as BIAddOverViewViewController
//
//            self.navigationController?.pushViewController(biaddOverViewViewController!, animated: true)
//        }
    }
    
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
    
    @IBAction func actionZoomOption(_ sender: AnyObject) {
        zoomableView()
    }
    
    // MARK:- Enthausist
    @IBAction func actionPurchase(_ sender: UIButton) {
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            
            break
        case AppUser.LocalBusiness.rawValue:
            
            break
        case AppUser.Enthusiast.rawValue:
            self.checkSubscriptionAndPurhase()
            break
        default:
            break
        }
        
    }
    
    @IBAction func actionBuildPackage(_ sender: UIButton) {
        //TODO: Open Package tab
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            
            break
        case AppUser.LocalBusiness.rawValue:
            
            break
        case AppUser.Enthusiast.rawValue:
            self.openPackageList()
            break
        default:
            break
        }
    }
}

extension ServiceDetailView: HSPopupMenuDelegate{
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if(index == 0){
            
            self.serviceAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName:HelperConstant.ServiceStoryboard , viewControllerName: ServiceAddOverviewViewController.nameOfClass) as ServiceAddOverviewViewController
            self.serviceAddOverviewViewController?.serviceAddRequest = self.serviceAddRequest
            serviceAddOverviewViewController?.isEditingService = true
             self.navigationController?.pushViewController(self.serviceAddOverviewViewController!, animated: true)
        }
    }
    
    func updateScreenInfo(){
        //if(biAddRequest != nil){
        if(serviceAddRequest != nil){
            // TODO: Update all controls
            // update Business Information media file
            
            let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
            
            switch  role_id {
            case AppUser.ServiceProvider.rawValue:
                
                self.viewServiceProviderDetail.isHidden = false
                self.viewServiceProviderEnthuaistDetail.isHidden = true
                
                self.constraintDocumentCollectionHeight.constant = self.defaultDocumentCollectionHeight
                if(self.serviceAddRequest?.mediabusiness != nil && (self.serviceAddRequest?.mediabusiness?.count)! > 0){
                    self.imgProfile.sd_setImage(with: URL(string: (self.serviceAddRequest?.mediabusiness![0].thumb ?? "")))
                    
                    // Check video on first pos
                    if((self.serviceAddRequest?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                        self.imgPlayBtn.isHidden = false
                    } else {
                        self.imgPlayBtn.isHidden = true
                    }
                    
                    
                } else {
                    self.imgPlayBtn.isHidden = true
                    self.imgServiceProviderProfile.image = nil
                    self.constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
                }
                
                self.collectionServiceProviderView.reloadData()
                break
            case AppUser.LocalBusiness.rawValue:
                
                break
            case AppUser.Enthusiast.rawValue:
                
                self.viewServiceProviderDetail.isHidden = true
                self.viewServiceProviderEnthuaistDetail.isHidden = false
                
                
                
                let profileImageUrl = PMUserDefault.getProfilePic()
                if(profileImageUrl != nil){
                    self.imgServiceProviderProfile.sd_setImage(with: URL(string: (profileImageUrl!)))
                }
                
                self.constraintDocumentServiceProviderCollectionHeight.constant = self.defaultDocumentCollectionHeight
                
                if(self.serviceAddRequest?.mediabusiness != nil && (self.serviceAddRequest?.mediabusiness?.count)! > 0){
                    self.imgServiceProviderProfile.sd_setImage(with: URL(string: (self.serviceAddRequest?.mediabusiness![0].thumb ?? "")))
                    
                    // Check video on first pos
                    if((self.serviceAddRequest?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                        self.imgPlayBtn.isHidden = false
                    } else {
                        self.imgPlayBtn.isHidden = true
                    }
                } else {
                    self.imgPlayBtn.isHidden = true
                    self.imgServiceProviderProfile.image = nil
                    self.constraintDocumentServiceProviderCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
                }
                
                self.collectionServiceProviderView.reloadData()
                
                self.lblTitleValue.text = self.serviceAddRequest?.service_name ?? ""
                self.lblSpotValue.text = self.serviceAddRequest?.number_of_purchase ?? ""
                self.lblPriceValue.text = HelperConstant.CURRENCY_SYMBOL + " " + (self.serviceAddRequest?.service_price?.service_price ?? "")
                
                break
            default:
                break
            }

            // Ovewview
            self.lblName.text = self.serviceAddRequest?.service_name ?? ""
            
            // TODO get total purchase from server after implementation in user purchase
            self.lblTotalPurchaseCount.text = "0 / \(self.serviceAddRequest?.number_of_purchase ?? " ") purchased"
            lblPurchasabilityStatus.text = serviceAddRequest?.purchasablity == "1" ? "Yes" : "No"
            
            self.lblCreditStatus.text = (self.serviceAddRequest?.credit_system != nil && (self.serviceAddRequest?.credit_system?.length)! > 0 && (self.serviceAddRequest?.credit_system ?? "") == "1") ? "Yes" : "No"
            
            if let charitableEvent = serviceAddRequest?.service_event?.causeCharitableEvent, let charitablePercentage = serviceAddRequest?.service_event?.causeCharitablePercentage, !charitableEvent.isEmpty, !charitablePercentage.isEmpty {
                lblDonationStatus.text = "Yes"
            } else {
                lblDonationStatus.text = "No"
            }

            self.lblPurchaseCount.text = self.serviceAddRequest?.number_of_purchase ?? " "
            if let goalTitles = serviceAddRequest?.service_target_goal_title, !goalTitles.isEmpty {
                lblTargetGoal.text = "• \(goalTitles.joined(separator: "\n• "))"
            } else {
                lblTargetGoal.text = ""
            }
            
            if(self.serviceAddRequest?.service_location_info != nil){
                var address = ""
                
                if(self.serviceAddRequest?.service_location_info?.location_name != nil && (self.serviceAddRequest?.service_location_info?.location_name?.length)! > 0){
                    address = "\(self.serviceAddRequest?.service_location_info?.location_name ?? "") \n\(self.serviceAddRequest?.service_location_info?.location_address ?? "")"
                    
                } else if(self.serviceAddRequest?.service_location_info?.location_name_sec != nil && (self.serviceAddRequest?.service_location_info?.location_name_sec?.length)! > 0){
                    address = "\(self.serviceAddRequest?.service_location_info?.location_name_sec ?? "") \n\(self.serviceAddRequest?.service_location_info?.location_address_sec ?? "")"
                }
                
                self.lblLocationValue.text = address
            } else {
                self.lblLocationValue.text = ""
            }
            
            // Form 2 Calendar listing
            if let serviceCalender = serviceAddRequest?.service_calendar, let date = serviceCalender.date, !date.isEmpty {
                lblCalendarListingStatus.text = "Yes"
                calendarInfoContainer.isHidden = false
                lblCalendarDate.text = date
                if let time = serviceCalender.time,  !time.isEmpty {
                    lblCalendarDate.text = "\(date) \(time)"
                }
                self.lblCalendarRepeatStatus.text = serviceCalender.repeatStatus == "1" ? "Yes" : "No"
                
                var timeFrame = ""
                if let repeatTimeFrame = serviceAddRequest?.service_calendar?.repeatTimeFrame {
                    if repeatTimeFrame == "1" {
                        timeFrame = LocalizationKeys.every_day.getLocalized()
                    } else if repeatTimeFrame == "2" {
                        timeFrame = LocalizationKeys.every_week.getLocalized()
                    } else if repeatTimeFrame == "3" {
                        timeFrame = LocalizationKeys.every_month.getLocalized()
                    }
                }
                
                lblCalendarRepeatTimeFrame.text = timeFrame
                if let repeatCount = serviceCalender.repeatTimeCount, !repeatCount.trim().isEmpty {
                    lblCalendarRepeat.text = repeatCount
                } else {
                    lblCalendarRepeat.text = "0"
                }
            } else {
                calendarInfoContainer.isHidden = true
                lblCalendarListingStatus.text = "No"
                lblCalendarDate.text = ""
                lblCalendarRepeatStatus.text = ""
                lblCalendarRepeatTimeFrame.text = ""
                lblCalendarRepeat.text = ""
            }
            
            // Form 3 Standard / Exceptional
            if(self.serviceAddRequest?.service_information != nil){
                let expTypeTitleList = serviceAddRequest?.service_information?.exceptionalServices?.compactMap { $0.title }
                if let expTypeTitleList = expTypeTitleList, !expTypeTitleList.isEmpty {
                    lblExpService.text = "• \(expTypeTitleList.joined(separator: "\n• ") )"
                } else {
                    lblExpService.text = ""
                }
                
                self.lblExpType.text =  self.serviceAddRequest?.service_information?.stdServices != nil ? ( Int(self.serviceAddRequest?.service_information?.stdServices ?? "0") == 3 ? LocalizationKeys.std_exp.getLocalized() : (Int(self.serviceAddRequest?.service_information?.stdServices ?? "0") == 2 ? LocalizationKeys.exceptional.getLocalized() : LocalizationKeys.standard.getLocalized())) : LocalizationKeys.standard.getLocalized()
                self.lblListDisabilities.text = self.serviceAddRequest?.service_information?.listDisabilities ?? ""
            } else {
                self.lblExpType.text = ""
                self.lblExpService.text = ""
                self.lblListDisabilities.text = ""
            }
            
            // Form 4 Certificate
            if(self.serviceAddRequest?.service_certificate != nil){
                self.lblCertificateName.text = self.serviceAddRequest?.service_certificate?.certificateName
                
                if let freeService = serviceAddRequest?.service_certificate?.freeService, !freeService.trim().isEmpty {
                    lblCertificateFreeServiceStatus.text = "Yes"
                    lblCertificateFreeService.text = freeService
                } else {
                    lblCertificateFreeServiceCountLabel.isHidden = true
                    lblCertificateFreeService.isHidden = true
                    lblCertificateFreeServiceStatus.text = "No"
                    lblCertificateFreeService.text = "NA"
                }
                
                if let trialService = serviceAddRequest?.service_certificate?.trailService, !trialService.trim().isEmpty {
                    lblCertificateTrailService.text = "Yes"
                    lblCertificateTrailServiceCount.text = trialService
                } else {
                    lblCertificateTrailService.text = "No"
                    lblCertificateTrailServiceCountLabel.isHidden = true
                    lblCertificateTrailServiceCount.isHidden = true
                    lblCertificateTrailServiceCount.text = "NA"
                }
                
                if(self.serviceAddRequest?.service_certificate?.travelServiceTypeTitle != nil && (self.serviceAddRequest?.service_certificate?.travelServiceTypeTitle?.length)! > 0){
                    
                    self.viewExpTravelService.isHidden = false
                    lblCertificateTravelServiceStatusTitle.isHidden = true
                    self.lblCertificateTravelServiceStatus.isHidden = true
                    self.lblCertificateTravelServiceStatus.text = "No"
                    self.lblCertificateTravelServiceType.text = self.serviceAddRequest?.service_certificate?.travelServiceTypeTitle ?? ""
                    self.lblCertificateTravelServiceRequirement.text = self.serviceAddRequest?.service_certificate?.travelServiceReq ?? ""
                    self.lblCertificateTravelServiceLocation.text = self.serviceAddRequest?.service_certificate?.travelLocationTitle ?? ""
                    
                } else {
                    self.viewExpTravelService.isHidden = true
                    lblCertificateTravelServiceStatusTitle.isHidden = false
                    self.lblCertificateTravelServiceStatus.isHidden = false
                    self.lblCertificateTravelServiceStatus.text = "No"
                    self.lblCertificateTravelServiceType.text = ""
                    self.lblCertificateTravelServiceRequirement.text = ""
                    self.lblCertificateTravelServiceLocation.text = ""
                }
                
            } else {
                self.lblCertificateName.text = ""
                self.lblCertificateFreeService.text = "No"
                self.lblCertificateTrailService.text = "No"
                
                self.viewExpTravelService.isHidden = true
                self.lblCertificateTravelServiceStatus.text = "No"
                self.lblCertificateTravelServiceType.text = ""
                self.lblCertificateTravelServiceRequirement.text = ""
                self.lblCertificateTravelServiceLocation.text = ""
            }
            
            // Form 5 Therapy
            if(self.serviceAddRequest?.service_therapy != nil){
                self.viewTherapy.isHidden = false
                self.lblTherapyStatus.text = "Yes"
                
                if let therapyType = serviceAddRequest?.service_therapy?.therapyType, !therapyType.isEmpty {
                    let therapyTypeTitleList = therapyType.compactMap { $0.title }
                    lblTherapyType.text = "• \(therapyTypeTitleList.joined(separator: "\n• "))"
                } else {
                    lblTherapyType.text = ""
                }
                
                self.lblTherapyFieldCover.text = self.serviceAddRequest?.service_therapy?.therapyFieldCover ?? ""
                self.lblTherapyPaymentType.text = self.serviceAddRequest?.service_therapy?.paymentType?.title ?? ""
                
                if(self.serviceAddRequest?.service_therapy?.acceptedInsuancePlan != nil && (self.serviceAddRequest?.service_therapy?.acceptedInsuancePlan?.length)! > 0){
                    self.lblTherapyPaymentTypeDesc.isHidden = false
                    self.lblTherapyPaymentTypeDesc.text = self.serviceAddRequest?.service_therapy?.acceptedInsuancePlan ?? ""
                } else {
                    self.lblTherapyPaymentTypeDesc.isHidden = true
                }
            } else {
                self.viewTherapy.isHidden = true
                self.lblTherapyStatus.text = "No"
                self.lblTherapyType.text = ""
                self.lblTherapyFieldCover.text = ""
                self.lblTherapyPaymentType.text = ""
                self.lblTherapyPaymentTypeDesc.text = ""
            }
            
            // Form 6 Instructional / Front office
            viewFrontOffice.isHidden = true
            viewInstructional.isHidden = true
            if let serviceInstructor = serviceAddRequest?.service_instructor {
                // Instructional
                if let instructionalTitle = serviceInstructor.instructionalTypeTitle, !instructionalTitle.isEmpty {
                    viewInstructional.isHidden = false
                    lblInstructionalType.text = instructionalTitle
                    
                    lblInstuctionalTypeCount.isHidden = true
                    if let instructionalType = serviceInstructor.instructionalTypeNumber, !instructionalType.isEmpty {
                        lblInstuctionalTypeCount.isHidden = false
                        lblInstuctionalTypeCount.text = instructionalType
                    }
                } else {
                    // Front office
                    viewFrontOffice.isHidden = false
                    viewDirectMembership.isHidden = true
                    
                    if let directMembership = serviceInstructor.frontOfficeDirectMembership, !directMembership.isEmpty {
                        viewDirectMembership.isHidden = false
                        lblDirectMembershipStatus.text = directMembership == "1" ? "Yes" : "No"
                        
                        lblServiceGainList.removeAllArrangedSubviews()
                        if let serviceGained = serviceInstructor.serviceGained, !serviceGained.isEmpty {
                            let serviceLabels = labelsList(for: serviceGained, action: #selector(servicesTapped(gesture:)))
                            for lbl in serviceLabels {
                                lblServiceGainList.addArrangedSubview(lbl)
                            }
                        }
                        
                        lblFacilitiesGainList.removeAllArrangedSubviews()
                        if let facilitiesGained = serviceInstructor.facilitiesGainedDirectMembership, !facilitiesGained.isEmpty {
                            let facilityLabels = labelsList(for: facilitiesGained, action: #selector(facilitiesTapped(gesture:)))
                            for lbl in facilityLabels {
                                lblFacilitiesGainList.addArrangedSubview(lbl)
                            }
                        }
                    } else {
                        viewDirectMembership.isHidden = true
                        lblDirectMembershipStatus.isHidden = false
                        lblDirectMembershipStatus.text = "No"
                    }
                    
                    if serviceInstructor.rental == "1" {
                        self.viewFacilities.isHidden = false
                        self.lblFacilitiesStatus.text = "Yes"
                        
                        lblFacilitiesRental.removeAllArrangedSubviews()
                        if let facilitiesGained = serviceInstructor.facilitiesGained, !facilitiesGained.isEmpty {
                            let facilityLabels = labelsList(for: facilitiesGained, action: #selector(facilitiesTapped(gesture:)))
                            for lbl in facilityLabels {
                                lblFacilitiesRental.addArrangedSubview(lbl)
                            }
                        }
                        
                        lblFacilitiesRentalClassification.text = serviceInstructor.facilityRentalClassification
                        if let rentalList = serviceInstructor.requirementFacility, !rentalList.isEmpty {
                            lblRequirementFacilitiesRental.text = "• \(rentalList.joined(separator: "\n• "))"
                        } else {
                            lblRequirementFacilitiesRental.text = ""
                        }
                        
                        // facilities added cost display
                        if let priceList = serviceInstructor.additionalCostList, !priceList.isEmpty {
                            lblAddedCostStatus.isHidden = true
                        } else {
                            lblAddedCostStatus.isHidden = false
                        }
                        tableViewFacilitiesAdditionalCost.reloadData()
                    } else {
                        lblFacilitiesRental.removeAllArrangedSubviews()
                        viewFacilities.isHidden = true
                        lblFacilitiesStatus.text = "No"
                        lblFacilitiesRentalClassification.text = ""
                        lblRequirementFacilitiesRental.text = ""
                    }
                }
            } else {
                // Instructional
                self.viewInstructional.isHidden = true
                self.lblInstructionalType.text = ""
                self.lblInstuctionalTypeCount.text = ""
                
                // Front office
                self.viewFrontOffice.isHidden = true
                self.viewDirectMembership.isHidden = false
                self.lblDirectMembershipStatus.text = ""
                
                lblServiceGainList.removeAllArrangedSubviews()
                lblFacilitiesGainList.removeAllArrangedSubviews()
                lblFacilitiesRental.removeAllArrangedSubviews()

                self.viewFacilities.isHidden = false
                self.lblFacilitiesStatus.text = ""
                self.lblFacilitiesRentalClassification.text = ""
                self.lblRequirementFacilitiesRental.text = ""
                
                // facilities added cost display
                self.constraintTableViewFacilitiesAdditionalCostHeight.constant = 10
                self.tableViewFacilitiesAdditionalCost.reloadData()
            }
            
            
            // Form 7 Event
            if let service_event = serviceAddRequest?.service_event {
                viewEvent.isHidden = false
                lblEventStatus.isHidden = true
                lblEventStatus.text = ""
                
                lblGeneralEventType.text = service_event.typeTitle
                lblEventType.text = service_event.specificEventType
                
                if let charitableCause = service_event.causeCharitableEvent?.trim(), !charitableCause.isEmpty {
                    lblEventClassification.text = LocalizationKeys.charitable.getLocalized()
                    
                    viewCharitableOption.isHidden = false
                    lblCharitableOptionName.text = charitableCause
                    
                    let charitableCausePercent = service_event.causeCharitablePercentage?.trim() ?? "0"
                    lblCharitableOptionPercentage.text = "\(charitableCausePercent) %"
                } else {
                    self.lblEventClassification.text = LocalizationKeys.standard.getLocalized()
                    self.viewCharitableOption.isHidden = true
                    self.lblCharitableOptionName.text = ""
                    self.lblCharitableOptionPercentage.text = ""
                }
            } else {
                self.viewEvent.isHidden = true
                self.lblEventStatus.isHidden = false
                self.lblEventStatus.text = "No"
                self.lblEventType.text = ""
                lblGeneralEventType.text = ""
                self.lblEventClassification.text = ""
                self.viewCharitableOption.isHidden = true
                self.lblCharitableOptionName.text = ""
                self.lblCharitableOptionPercentage.text = ""
            }
            
            // Form 8 Specific
            self.lblSpecificDesc.text = self.serviceAddRequest?.service_desc

            // Form 9 Target
            if(self.serviceAddRequest?.service_specific_target != nil){
                if let workTargets = serviceAddRequest?.service_specific_target?.specificWorkTarget, !workTargets.isEmpty {
                    lblSpecificTargetGoal.text = "• \(workTargets.joined(separator: "\n• "))"
                } else {
                    lblSpecificTargetGoal.text = ""
                }
                
                if let workDuringService = serviceAddRequest?.service_specific_target?.workDuringService, !workDuringService.isEmpty {
                    lblWorkDuringService.text = "• \(workDuringService.joined(separator: "\n• "))"
                } else {
                    lblWorkDuringService.text = ""
                }
                
            } else {
                self.lblSpecificTargetGoal.text = ""
                self.lblWorkDuringService.text = ""
            }
            
            // Form 10 Service field
            // Field Service type display
            if(self.serviceAddRequest?.service_field != nil){
                self.constraintTableViewFieldServiceTypeHeight.constant = 10
                self.tableViewFieldServiceType.reloadData()
                
            } else {
                self.constraintTableViewFieldServiceTypeHeight.constant = 0
                self.tableViewFieldServiceType.reloadData()
            }
            
            // Form 11 Price
            if serviceAddRequest?.service_price != nil {
                self.viewPrice.isHidden = false
                if let servicePrice = serviceAddRequest?.service_price?.service_price {
                    lblServicePrice.text = "$ \(servicePrice)"
                } else {
                    lblServicePrice.text = "Not added"
                }
                
                lblServicePriceStatus.text = serviceAddRequest?.service_price?.additionalCost != nil ? "Yes" : "No"
                tableViewServicePriceAdditionalCost.reloadData()
            } else {
                viewPrice.isHidden = true
                lblServicePrice.text = ""
                lblServicePriceStatus.text = ""
            }
            tableViewServicePriceAdditionalCost.reloadData()

            // Form 12 Skill and Age
            if(self.serviceAddRequest?.service_skill != nil && self.serviceAddRequest?.service_skill?.skillLevel != nil){
                
                let skillLevels = serviceAddRequest?.service_skill?.skillLevel?.compactMap { $0.title }
                if let skillLevels = skillLevels {
                    lblSkillLevel.text = "• \(skillLevels.joined(separator: "\n• ") )"
                } else {
                    lblSkillLevel.text = ""
                }
                
                self.lblSkillMinAge.text = self.serviceAddRequest?.service_skill?.minAge ?? ""
                self.lblSkillMaxAge.text = self.serviceAddRequest?.service_skill?.maxAge ?? ""
                self.lblSkillDurationType.text = self.serviceAddRequest?.service_skill?.durationTitle ?? ""
                self.lblSkillDurationTime.text = self.serviceAddRequest?.service_skill?.durationTime ?? ""
            } else {
                self.lblSkillLevel.text = ""
                self.lblSkillMinAge.text = ""
                self.lblSkillMaxAge.text = ""
                self.lblSkillDurationType.text = ""
                self.lblSkillDurationTime.text = ""
            }
            
            // Form 13 Content associated Service
            self.tableViewFieldListing.reloadData()
            self.tableViewPersonnelListing.reloadData()

            // Form 14 Field Good Service
            self.tableViewGoodServiceListing.reloadData()

            // Form 15 Additional notes
            self.tableViewAddNotesListing.reloadData()

            // Form 16 External link
            self.tableViewExtenalLinkListing.reloadData()

            // Form 17 Pre-page service requirement
            self.tableViewPrePostReqListing.reloadData()

            // Form 18 Inside-look content service requirement
            self.tblInsideLookContentListing.reloadData()
            // Form 20 Instructional content service requirement
            self.tblInstructionalContentListing.reloadData()

            // Form Additional document link
            self.tableViewAdditionalDocument.reloadData()

            
            self.scrollView.isHidden = false
            self.viewCreateNew.isHidden = true
//            self.btnOptionMenu.isHidden = false
            if(self.isHideEditOption){
                self.btnOptionMenu.isHidden = true
            }
        } else {
            self.viewCreateNew.isHidden = false
            self.scrollView.isHidden = true
            self.btnOptionMenu.isHidden = true
        }
    }
    
    private func labelsList(for list: [DetailData], action: Selector) -> [UILabel] {
        let labels: [UILabel] = list.compactMap { detailItem -> UILabel? in
            if let id = detailItem.id?.trim(), !id.isEmpty, let title = detailItem.title?.trim(), !title.isEmpty {
                let label = UILabel()
                let font = UIFont(name: FontsConfig.FONT_TYPE_Regular, size: 18) ?? UIFont.systemFont(ofSize: 18)
                label.attributedText = NSAttributedString(string: "• \(title)", attributes: [.foregroundColor: UIColor.blue, .underlineStyle: 1, .font: font])
                label.tag = Int(id) ?? 0
                label.textColor = .blue
                label.isUserInteractionEnabled = true
                let tapGesture = UITapGestureRecognizer(target: self, action: action)
                label.addGestureRecognizer(tapGesture)
                return label
            }
            return nil
        }
        return labels
    }
    
    @objc private func servicesTapped(gesture: UITapGestureRecognizer) {
        if let label = gesture.view as? UILabel {
            showServiceDetail(for: "\(label.tag)")
        }
    }

    @objc private func facilitiesTapped(gesture: UITapGestureRecognizer) {
        if let label = gesture.view as? UILabel {
            showFacilityDetail(for: "\(label.tag)")
        }
    }
    
    fileprivate func zoomableView(){
        // Create an array of images.
        var images:[LightboxImage] = []
        
        if(self.serviceAddRequest?.mediabusiness != nil && (self.serviceAddRequest?.mediabusiness?.count)! > 0){
            
            for media in (self.serviceAddRequest?.mediabusiness)! {
                //            self.imgProfile.sd_setImage(with: URL(string: (self.biAddRequest?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((media.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    
                    images.append(LightboxImage(imageURL: URL(string: media.thumb ?? "")!, text: "", videoURL: URL(string: media.imageurl ?? "")!))
                    
                } else {
                    images.append(LightboxImage(
                        imageURL: URL(string: media.imageurl ?? "")!
                    ))
                }
            }
        }
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)
        // Present your controller.
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
        
        LightboxConfig.handleVideo = { from, videoURL in
            // Custom video handling
            let videoController = AVPlayerViewController()
            videoController.player = AVPlayer(url: videoURL)
            
            from.present(videoController, animated: true) {
                videoController.player?.play()
            }
        }
    }
    
    fileprivate func checkSubscriptionAndPurhase() {
        if let subscriptionInfo = PMUserDefault.getSubsriptionInfo() {
            let subscriptionDetail: SubscriptionDetail? = SubscriptionDetail().getModelObjectFromServerResponse(jsonResponse: subscriptionInfo as AnyObject)
            if subscriptionDetail?.sub_end == nil {
                let purchaseMembershipVC: PMLogoutView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
                purchaseMembershipVC.delegate = self
                purchaseMembershipVC.scrTitle = ""
                purchaseMembershipVC.scrDesc = LocalizationKeys.error_no_subscription.getLocalized()
                purchaseMembershipVC.modalPresentationStyle = .overFullScreen
                present(purchaseMembershipVC, animated: true, completion: nil)
            } else if let service = serviceAddRequest {
                var shouldAddToCart: Bool = true
                if let numberOfPurchase = service.number_of_purchase, let numberOfPurchaseVal = Int(numberOfPurchase), numberOfPurchaseVal > 0, let totalPurchase = service.total_purchase, let totalPurchaseValue = Int(totalPurchase), totalPurchaseValue < numberOfPurchaseVal {
                    shouldAddToCart = false
                }

                if shouldAddToCart {
                    ServiceCart.shared.addServiceToCart(service)
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_purchasability_limit.getLocalized(), buttonTitle: nil, controller: nil)
                }
            }
        }
        /*
        var isFound = false
        let subscriptionInfoData = PMUserDefault.getSubsriptionInfo()
        
        if(subscriptionInfoData != nil){
            let subscriptionInfo:SubscriptionDetail? = SubscriptionDetail().getModelObjectFromServerResponse(jsonResponse: (subscriptionInfoData ?? "") as AnyObject)
            
            if(subscriptionInfo != nil && subscriptionInfo?.sub_end != nil){
                isFound = true
                if(self.serviceAddRequest != nil && self.serviceAddRequest?.service_price != nil && self.serviceAddRequest?.service_price?.service_price != nil){
                    
                    if(self.packagePaymentViewController == nil){
                        
                        
                        //TODO: Open Credit card payment screen
                        self.packagePaymentViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackagePaymentViewController.nameOfClass) as PackagePaymentViewController
                        
                        if(self.busiInfo != nil){
                            //                        self.service?.busi_id = self.busiInfo?.busi_id ?? ""
                            self.serviceAddRequest?.busi_title = self.busiInfo?.busi_title ?? ""
                            self.serviceAddRequest?.busitype = self.busiInfo?.busitype ?? ""
                        }
                        self.packagePaymentViewController?.serviceList = [(self.serviceAddRequest!)]
                        
                        self.packagePaymentViewController?.packageAmount = CFloat(self.serviceAddRequest?.service_price?.service_price ?? "") ?? 0.00
                        
                        self.packagePaymentViewController?.creditAmount = 0
                        self.packagePaymentViewController?.totalAmount = packagePaymentViewController?.packageAmount ?? 0.00
                        
                        
                        self.packagePaymentViewController?.isSingleServicePurchase = true
                        
                        self.navigationController?.pushViewController(self.packagePaymentViewController!, animated: true)
                    }
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_price_missing.getLocalized(), buttonTitle: nil, controller: nil)
                }
            }
        }
        
        if(!isFound) {
            self.logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
            self.logoutViewController?.delegate = self
            self.logoutViewController?.scrTitle = ""
            self.logoutViewController?.scrDesc = LocalizationKeys.error_no_subscription_service.getLocalized()
            // self.selectedIndex = sender.tag
            self.logoutViewController?.modalPresentationStyle = .overFullScreen
            self.present(logoutViewController!, animated: true, completion: nil)
        }
         */
    }
    
    fileprivate func openPackageList(){
        Helper.sharedInstance.openTabScreenWithIndex(index: 3)
    }
}

extension ServiceDetailView: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
        
        Helper.sharedInstance.openPremiumMembershipView()
    }
}

extension ServiceDetailView: LightboxControllerPageDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension ServiceDetailView: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}

extension ServiceDetailView: UITableViewDataSource, UITableViewDelegate {
    
    /**
     *  Specify height of row in tableview with UITableViewDelegate method override.
     *
     *  @param key tableView object and heightForRowAt index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    /**
     *  Specify number of section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return 2
        if tableView == tableViewFacilitiesAdditionalCost {
            return serviceAddRequest?.service_instructor?.additionalCostList?.count ?? 0
        } else if tableView == tableViewFieldServiceType {
            return serviceAddRequest?.service_field?.count ?? 0
        } else if tableView == tableViewServicePriceAdditionalCost {
            return serviceAddRequest?.service_price?.additionalCost?.count ?? 0
        } else if tableView == tableViewFieldListing {
            return serviceAddRequest?.service_listing?.fieldListing?.count ?? 0
        } else if tableView == tblInsideLookContentListing {
            return serviceAddRequest?.service_insidelook?.count ?? 0
        } else if tableView == tblInstructionalContentListing {
            return serviceAddRequest?.service_instructionalcontent?.count ?? 0
        } else if tableView == tableViewPersonnelListing {
            return serviceAddRequest?.service_listing?.personnelListing?.count ?? 0
        } else if tableView == tableViewGoodServiceListing {
            return serviceAddRequest?.service_other_good_services?.service?.count ?? 0
        } else if tableView == tableViewAddNotesListing {
            return serviceAddRequest?.service_add_notes?.additionalNotes?.count ?? 0
        } else if tableView == tableViewExtenalLinkListing {
            return serviceAddRequest?.service_external_link?.serviceExternalLinkList?.count ?? 0
        } else if tableView == tableViewPrePostReqListing {
            return serviceAddRequest?.service_prepage_requirement?.count ?? 0
        } else if tableView == tableViewAdditionalDocument {
            return serviceAddRequest?.doc?.count ?? 0
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.tableViewFacilitiesAdditionalCost){
            let cell = tableView.dequeueReusableCell(withIdentifier: AddedCostTableViewCell.nameOfClass) as! ServiceAddedCostTableViewCell
            
            let optionNames = serviceAddRequest?.service_instructor?.additionalServiceOptionName
            let optionDescriptions = serviceAddRequest?.service_instructor?.additionalServiceOptionDesc
            let priceList = serviceAddRequest?.service_instructor?.additionalCostList
            
            cell.lblAdditionalCostIndex.text = "Additional service option \(indexPath.row + 1)"
            cell.lblIndex.text = "Option \(indexPath.row + 1)"
            
            if let optionNames = optionNames, optionNames.count > indexPath.row {
                cell.lblName.text = optionNames[indexPath.row]
            }
            
            if let optionDescriptions = optionDescriptions, optionDescriptions.count > indexPath.row {
                cell.lblDesc.text = optionDescriptions[indexPath.row]
            }

            if let priceList = priceList, priceList.count > indexPath.row {
                let priceItem = priceList[indexPath.row]
                cell.lblStatus.text = "Yes"
                
                if !priceItem.name.trim().isEmpty {
                    cell.stackViewPrice.isHidden = false
                    cell.lblPrice.text = "$\(priceItem.name.trim())"
                    cell.lblPriceDesc.text = priceItem.pricedesc.trim()
                } else {
                    cell.stackViewPrice.isHidden = true
                    cell.lblStatus.text = "No"
                }
            }
            return cell
        } else if(tableView == self.tableViewFieldServiceType){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BIFieldServiceCell.nameOfClass) as! BIFieldServiceCell
            
            cell.cellIndex = indexPath
            
            cell.lblFieldIndex.text = "Field \(indexPath.row + 1)"
            let objInfo = self.serviceAddRequest?.service_field![indexPath.row]
            
            cell.lblCatName.text = objInfo?.catFieldName ?? ""
            
            if(objInfo?.add_new_specific_field != nil && (objInfo?.add_new_specific_field?.length)! > 0){
                cell.lblSubCatName.text = objInfo?.add_new_specific_field ?? ""
            } else {
                cell.lblSubCatName.text = objInfo?.specificFieldName ?? ""
            }
            
            if(cell.btnRemove != nil){
                cell.btnRemove.isHidden = true
            }
            
            if let classifications = objInfo?.classification, !classifications.isEmpty {
                cell.lblClassification.text = "• \(classifications.joined(separator: "\n• "))"
            } else {
                cell.lblClassification.text = ""
            }
            
            if let techniques = objInfo?.technique, !techniques.isEmpty {
                cell.lblTechnique.text = "• \(techniques.joined(separator: "\n• "))"
            } else {
                cell.lblTechnique.text = ""
            }
            cell.backgroundColor = UIColor.clear
            return cell
        } else if(tableView == self.tableViewServicePriceAdditionalCost){
            //
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ServiceAddedCostTableViewCell.nameOfClass) as! ServiceAddedCostTableViewCell
            
            if let objInfo = self.serviceAddRequest?.service_price?.additionalCost?[indexPath.row] {
                cell.lblIndex.text = "Additional Cost \(indexPath.row + 1)"
                cell.lblName.text = objInfo.name
                cell.lblDesc.text = objInfo.desc
                cell.lblStatus.text = "No"

                if !objInfo.price.trim().isEmpty {
                    cell.lblStatus.text = "Yes"
                    cell.stackViewPrice.isHidden = false
                    cell.lblPrice.text = objInfo.price.trim()
                    cell.lblPriceDesc.text = objInfo.pricedesc.trim()
                } else {
                    cell.stackViewPrice.isHidden = true
                }
            }
            return cell

        } else if(tableView == self.tableViewFieldListing){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
            
            let objInfo = self.serviceAddRequest?.service_listing?.fieldListing![indexPath.row]
            
            cell.cellIndex = indexPath
            cell.lblTitle.text = objInfo?.link ?? ""
            cell.lblTitleDesc.text = objInfo?.desc ?? ""
            cell.lblRoleDesc.text = objInfo?.roleDesc ?? ""
            
            cell.backgroundColor = UIColor.clear
            return cell

        } else if(tableView == self.tableViewPersonnelListing){
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
            
            let objInfo = self.serviceAddRequest?.service_listing?.personnelListing![indexPath.row]
            
            cell.cellIndex = indexPath
            cell.lblTitle.text = objInfo?.link ?? ""
            cell.lblTitleDesc.text = objInfo?.desc ?? ""
            cell.lblRoleDesc.text = objInfo?.roleDesc ?? ""
            
            cell.backgroundColor = UIColor.clear
            return cell
        } else if tableView == tblInsideLookContentListing {
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
            
            if let objInfo = self.serviceAddRequest?.service_insidelook?[indexPath.row] {
                cell.cellIndex = indexPath
                cell.lblTitle.text = objInfo.name
                cell.lblTitleDesc.text = objInfo.desc
            }
            cell.lblRoleDesc.text = ""
            cell.lblRoleDesc.isHidden = true
            cell.backgroundColor = UIColor.clear
            return cell

        } else if tableView == tblInstructionalContentListing {
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
            
            if let objInfo = self.serviceAddRequest?.service_instructionalcontent?[indexPath.row] {
                cell.cellIndex = indexPath
                cell.lblTitle.text = objInfo.name
                cell.lblTitleDesc.text = objInfo.desc
            }
            cell.lblRoleDesc.text = ""
            cell.lblRoleDesc.isHidden = true
            cell.backgroundColor = UIColor.clear
            return cell
        } else if(tableView == self.tableViewGoodServiceListing){
            //
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
            
            let objInfo = self.serviceAddRequest?.service_other_good_services?.service![indexPath.row]
            
            cell.lblTitle.text = objInfo?.link
            cell.lblTitleDesc.text = objInfo?.desc
            
            cell.backgroundColor = UIColor.clear
            return cell
            
        } else if(tableView == self.tableViewAddNotesListing){
            let cell = tableView.dequeueReusableCell(withIdentifier: AdditionalTitleDescView.nameOfClass) as! AdditionalTitleDescView
            
            cell.cellIndex = indexPath
            cell.lblTitleDesc.text = self.serviceAddRequest?.service_add_notes?.additionalNotes![indexPath.row] ?? ""
            
            cell.backgroundColor = UIColor.clear
            return cell
        } else if(tableView == self.tableViewExtenalLinkListing){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
            
            let objInfo = self.serviceAddRequest?.service_external_link?.serviceExternalLinkList?[indexPath.row]
            cell.lblLink.text = objInfo?.link
            cell.lblDesc.text = objInfo?.desc
            cell.backgroundColor = UIColor.clear
            return cell
        } else if(tableView == self.tableViewPrePostReqListing){
            let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
            
            let objInfo = self.serviceAddRequest?.service_prepage_requirement![indexPath.row]
            
            cell.lblLink.text = objInfo?.requirementType
            cell.lblDesc.text = objInfo?.description
            
            return cell
        } else if tableView == tableViewAdditionalDocument {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationDocumentCell.nameOfClass) as! BusinessInformationAddAdditionalInformationDocumentCell
            
            let data = serviceAddRequest?.doc?[indexPath.row]
            
            cell.lblName.text = data?.media_title
            cell.lblDesc.text = data?.media_desc
            cell.cellIndex = indexPath
            cell.documentSelectionDelegate = self
            
            if let imageURLs = data?.imageurls, !imageURLs.isEmpty {
                cell.arrDocumentImagesUrl = imageURLs.compactMap { $0.thumb }
            } else if let arrImages = data?.arrImages {
                cell.arrDocumentImages = arrImages.compactMap { uploadData in
                    if let imageData = uploadData.imageThumbnailData {
                        return UIImage(data: imageData)
                    }
                    return nil
                }
            }
            
            cell.refreshScreenInfo()
            
            return cell
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewExtenalLinkListing {
            if let objInfo = self.serviceAddRequest?.service_external_link?.serviceExternalLinkList?[indexPath.row], let link = objInfo.link {
                openLink(for: link)
            }
        } else if tableView == tableViewGoodServiceListing {
            if let serviceInfo = serviceAddRequest?.service_other_good_services?.service?[indexPath.row], let serviceId = serviceInfo.id {
                showServiceDetail(for: serviceId)
            }
        } else if tableView == tableViewFieldListing {
            if let facilityItem: ServiceListData = serviceAddRequest?.service_listing?.fieldListing?[indexPath.row], let facilityId = facilityItem.id {
                showFacilityDetail(for: facilityId)
            }
        } else if tableView == tableViewPersonnelListing {
            if let personalItem = serviceAddRequest?.service_listing?.personnelListing?[indexPath.row], let personalId = personalItem.id {
                showPersonalDetail(for: personalId)
            }
        } else if tableView == tblInsideLookContentListing {
            if let insideLookItem = serviceAddRequest?.service_insidelook?[indexPath.row], let insideLookId = insideLookItem.id {
                showInsideLookDetail(for: insideLookId)
            }
        } else if tableView == tblInstructionalContentListing {
            if let instructionalItem = serviceAddRequest?.service_instructionalcontent?[indexPath.row], let instructionalId = instructionalItem.id {
                showInstructionalDetail(for: instructionalId)
            }
        }
    }
}

extension ServiceDetailView : UICollectionViewDataSource {
    
    /**
     *  Specify number of rows on section in collectionView with UICollectionViewDataSource method override.
     *
     *  @param key UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.serviceAddRequest != nil && self.serviceAddRequest?.mediabusiness != nil && (self.serviceAddRequest?.mediabusiness?.count)! > 0) ? (self.serviceAddRequest?.mediabusiness?.count)! : 0
    }
    
    /**
     *  Implement collectionview cell in collectionView with UICollectionViewDataSource method override.
     *
     *  @param UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        let productInfo = productList?[indexPath.row]
        
        // Default product cell
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath) as! SerivceImageCell
        cell.delegate =  self
        cell.cellIndex = indexPath.row
        
        let data = self.serviceAddRequest?.mediabusiness![indexPath.row]
        
        if(data?.thumb != nil){
            cell.imgService.sd_setImage(with: URL(string: (data?.thumb ?? "")))
        }
        
        if((data?.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
            cell.imgPlay.isHidden = false
        } else {
            cell.imgPlay.isHidden = true
        }
        
        return cell
    }
}

extension ServiceDetailView : UICollectionViewDelegate {
    
    /**
     *  Implement tap event on collectionview cell in collectionView with UICollectionViewDelegate method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
    }
}

extension ServiceDetailView : UICollectionViewDelegateFlowLayout {
    
    /**
     *  Implement collectionView with UICollectionViewDelegateFlowLayout method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:100, height: 80)
    }
}

extension ServiceDetailView: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        if(self.serviceAddRequest?.mediabusiness != nil && (self.serviceAddRequest?.mediabusiness?.count)! > 0){
            if((self.serviceAddRequest?.mediabusiness![cellIndex].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.imgProfile.sd_setImage(with: URL(string: (self.serviceAddRequest?.mediabusiness![cellIndex].thumb ?? "")))
            
            self.imgServiceProviderProfile.sd_setImage(with: URL(string: (self.serviceAddRequest?.mediabusiness![cellIndex].thumb ?? "")))
        }
        
    }
}

extension ServiceDetailView: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        if tableView == tableViewFacilitiesAdditionalCost {
             constraintTableViewFacilitiesAdditionalCostHeight.constant = size.height
        } else if tableView == tableViewFieldServiceType {
             constraintTableViewFieldServiceTypeHeight.constant = size.height
        } else if tableView == tableViewServicePriceAdditionalCost {
             constraintTableViewServicePriceAdditionalCostHeight.constant = size.height
        } else if tableView == tableViewFieldListing {
             constraintTableViewFieldListingHeight.constant = size.height
        } else if tableView == tableViewPersonnelListing {
             constraintTableViewPersonnelListingHeight.constant = size.height
        } else if tableView == tblInsideLookContentListing {
             tblInsideLookContentListingHeight.constant = size.height
        } else if tableView == tblInstructionalContentListing {
             tblInstructionalContentListingHeight.constant = size.height
        } else if tableView == tableViewGoodServiceListing {
             constraintTableViewGoodServiceListingHeight.constant = size.height
        } else if tableView == tableViewAddNotesListing {
             constraintTableViewAddNotesHeight.constant = size.height
        } else if tableView == tableViewExtenalLinkListing {
             constraintTableViewExtenalLinkLisitingHeight.constant = size.height
        } else if tableView == tableViewPrePostReqListing {
             constraintTableViewPrePostReqListingHeight.constant = size.height
        } else if tableView == tableViewAdditionalDocument {
             constraintTableViewAdditionalDocumentHeight.constant = size.height
        }
    }
}

extension ServiceDetailView: DocumentSelectionDelegate {
    func didSelectDocument(at indexPath: IndexPath, inCellAt cellIndexPath: IndexPath) {
        if let data = serviceAddRequest?.doc?[cellIndexPath.row], let imageURLs = data.imageurls, imageURLs.count > indexPath.item, let imageURLString = imageURLs[indexPath.item].imageurls, let imageURL = URL(string: imageURLString) {
            let safariVC = SFSafariViewController(url: imageURL)
            navigationController?.present(safariVC, animated: true, completion: nil)
        }
    }
}

extension ServiceInstructor {
    var additionalCostList: [FacilityRentalAddedCost]? {
        return additionalServicePriceList
//        let priceList: [FacilityRentalAddedCost]? = additionalServicePriceList?.compactMap { item -> FacilityRentalAddedCost? in
//            if item.name.trim().isEmpty, item.pricedesc.trim().isEmpty {
//                return nil
//            }
//            return item
//        }
//        return priceList
    }
}

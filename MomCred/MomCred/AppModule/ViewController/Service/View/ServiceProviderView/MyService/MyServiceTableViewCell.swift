//
//  MyServiceTableViewCell.swift
//  MomCred
//
//  Created by MD on 27/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class MyServiceTableViewCell: UITableViewCell {

    @IBOutlet private weak var imgService: UIImageView!
    @IBOutlet private weak var serviceFeatureStackView: UIStackView!
    @IBOutlet private weak var serviceInfoStackView: UIStackView!
    @IBOutlet private weak var btnAddToCart: UIButton!

    var serviceItem: ServiceAddRequest? {
        didSet {
            /*
            cell.lblTitle.text          = data?.service_name ?? ""
            cell.lblDate.text           = "0 / \(data?.number_of_purchase ?? "") purchased"
            
            if(data?.service_location_info != nil){
                var address = ""
                
                if(data?.service_location_info?.location_name != nil && (data?.service_location_info?.location_name?.length)! > 0){
                    address = "\(data?.service_location_info?.location_name ?? "") \n\(data?.service_location_info?.location_address ?? "")"
                    
                } else if(data?.service_location_info?.location_name_sec != nil && (data?.service_location_info?.location_name_sec?.length)! > 0){
                    address = "\(data?.service_location_info?.location_name_sec ?? "") \n\(data?.service_location_info?.location_address_sec ?? "")"
                }
                
                cell.lblDesciption.text = address
            } else {
                cell.lblDesciption.text = ""
            }
            
            if(data?.service_price != nil && data?.service_price?.service_price != nil && (data?.service_price?.service_price?.length)! > 0){
                cell.lblPrice.text = "$\(data?.service_price?.service_price ?? "")"
            } else {
                cell.lblPrice.text = ""
            }
            
            if(data?.mediabusiness != nil && (data?.mediabusiness?.count)! > 0){
                cell.imgProfilePic.sd_setImage(with: URL(string:data?.mediabusiness![0].thumb ?? ""), placeholderImage: #imageLiteral(resourceName: "profile"))
            } else {
                cell.imgProfilePic.image = #imageLiteral(resourceName: "profile")
            }
            
            var serviceTypes = [UIImage]()
            if let creditSystem = data?.credit_system, creditSystem == "1", let imgCredit = UIImage(named: "ic_credit") {
                serviceTypes.append(imgCredit)
            }
            if let isDateEmpty = data?.service_calendar?.date?.isEmpty, let isTimeEmpty = data?.service_calendar?.time?.isEmpty, !isDateEmpty, !isTimeEmpty, let imgCalendar = UIImage(named: "ic_calendar") {
                serviceTypes.append(imgCalendar)
            }
            if let charitableEvent = data?.service_event?.causeCharitableEvent, let charitablePercentage = data?.service_event?.causeCharitablePercentage, !charitableEvent.isEmpty, !charitablePercentage.isEmpty, let imgDonation = UIImage(named: "ic_donation") {
                serviceTypes.append(imgDonation)
            }
            if let purchasability = data?.purchasablity, purchasability == "1", let imgPurchasability = UIImage(named: "ic_purchase") {
                serviceTypes.append(imgPurchasability)
            }
            if let exceptional = data?.service_information?.exceptionalServices, !exceptional.isEmpty, let imgExceptional = UIImage(named: "ic_exceptional") {
                serviceTypes.append(imgExceptional)
            }

            // remove all arranged subviews in case of reusing the cell
            cell.serviceTypesView.removeAllArrangedSubviews()
            for serviceImage in serviceTypes {
                let imageView = UIImageView(image: serviceImage)
                imageView.contentMode = .scaleAspectFit
                cell.serviceTypesView.addArrangedSubview(imageView)
            }
             */
            var views: [InfoStackView] = []
            if let serviceName = serviceItem?.provider_name {
                let infoV = InfoStackView.infoView(title: "Service provider name", value: serviceName)

            }
            if let serviceName = serviceItem?.service_name {
                let infoV = InfoStackView.infoView(title: "Service provider name", value: serviceName)

            }
            if let serviceName = serviceItem?.service_name {
                let infoV = InfoStackView.infoView(title: "Service provider name", value: serviceName)

            }

            /*
            Service provider name
            Vivek Kaushik

            Service name
            Software Development

            Service type
            front office

            Field category
            Sports

            Specific field
            Basketball

            Field category
            Group fitness

            Specific field
            CrossFit

            Spots
            28 / 10 purchased

            Price
            $333

            Donation Amount
            $16.65

            Location by town
            Nakoda Tower A, Jawahar Marg, Nihalpura, Rajwada, Indore, Madhya Pradesh 452002, India
             */
        }
    }
}

extension InfoStackView {
    class func infoView(title: String, value: String) -> InfoStackView {
        let info = Bundle.main.loadNibNamed("InfoStackView", owner: self, options: nil)?.first as! InfoStackView
        info.lblTitle.text = title
        info.lblValue.text = value
        return info
    }
}

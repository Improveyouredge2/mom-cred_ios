//
//  MyServiceCatListViewController.swift
//  MomCred
//
//  Created by consagous on 04/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class MyServiceCatListViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tbCatServiceList : UITableView!
    
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
//    fileprivate var serviceDetailView:ServiceDetailView?
    fileprivate var myServiceViewController:MyServiceViewController?
    
    fileprivate var menuArray: [HSMenu] = []
    fileprivate var serviceAddOverviewViewController:ServiceAddOverviewViewController?
    
    fileprivate var presenter = MyServiceCatListPresenter()
    
    var myServiceList:[ListingDataDetail]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.add_new.getLocalized())
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0){
            let menu2 = HSMenu(icon: nil, title: LocalizationKeys.complete_pending.getLocalized())
            menuArray = [menu1,menu2]
        }  else{
            menuArray = [menu1]
        }
        
        self.presenter.connectView(view: self)
        
        tbCatServiceList.estimatedRowHeight = 100
        tbCatServiceList.rowHeight = UITableView.automaticDimension
        tbCatServiceList.estimatedSectionHeaderHeight = 80
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutSubviews()
        self.myServiceViewController = nil
        
        self.presenter.getServiceCat()
    }
}

extension MyServiceCatListViewController {
    func updateScreenInfo(){
        if(self.tbCatServiceList != nil){
            self.tbCatServiceList.reloadData()
        }
    }
}


extension MyServiceCatListViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        self.serviceAddOverviewViewController = nil
        if(index == 0){
            
            UserDefault.removeSID()
            
            self.serviceAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddOverviewViewController.nameOfClass)
             self.navigationController?.pushViewController(self.serviceAddOverviewViewController!, animated: true)
        } else if(index == 1){
            self.myServiceViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: MyServiceViewController.nameOfClass) as MyServiceViewController
            self.myServiceViewController?.isHideNavigationBar = true
            self.navigationController?.pushViewController(self.myServiceViewController!, animated: true)
        }
    }
}

extension MyServiceCatListViewController{
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension MyServiceCatListViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.myServiceList != nil && (self.myServiceList?.count)! > 0) ? (self.myServiceList?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 3
        
        return (self.myServiceList != nil && (self.myServiceList?.count)! > section && self.myServiceList![section].subcat != nil && (self.myServiceList![section].subcat?.count)! > 0) ? (self.myServiceList![section].subcat?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = .clear
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        //label.text = "Sports"
        label.text = self.myServiceList![section].listing_title ?? ""
        
//        label.font = UIFont().futuraPTMediumFont(16) // my custom font
//        label.textColor = UIColor.charcolBlackColour() // my custom colour
        label.font = FontsConfig.FontHelper.defaultBoldFontWithSize(16)
        label.textColor = UIColor(hexString: ColorCode.appYellowColor)
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let data       =    self.myServiceList?[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: MyServiceCatCell.nameOfClass, for: indexPath) as! MyServiceCatCell

        if(self.myServiceList![indexPath.section] != nil && self.myServiceList![indexPath.section].subcat![indexPath.row] != nil){
            cell.lblTitle.text  = self.myServiceList![indexPath.section].subcat![indexPath.row].listing_title  ?? ""
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.myServiceViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: MyServiceViewController.nameOfClass) as MyServiceViewController
        
        // Sub cat id
        self.myServiceViewController?.subCatId =  self.myServiceList![indexPath.section].subcat![indexPath.row].listing_id  ?? ""
        
            self.navigationController?.pushViewController(self.myServiceViewController!, animated: true)
    }
}

class MyServiceCatCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

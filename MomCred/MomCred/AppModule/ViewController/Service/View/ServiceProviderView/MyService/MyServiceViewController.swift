//
//  MyServiceViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 * BookingViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class MyServiceViewController: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var lblMyTransactions: UILabel!
    
    @IBOutlet weak var lbl_NavigationTitle:UILabel!
    
    @IBOutlet weak var constraintSectionTabHeight: NSLayoutConstraint!
    
    fileprivate var menuArray: [HSMenu] = []
    fileprivate var serviceAddOverviewViewController:ServiceAddOverviewViewController?
    fileprivate var availableMyServiceListView:MyServiceListView?
    fileprivate var unavailableMyServiceListView:MyServiceListView?
    fileprivate var presenter = MyServicePresenter()
    
    var myServiceList:[ServiceAddRequest]?
    
    var screenName:String = ""
    var subCatId:String?
    var isHideNavigationBar = false
    
    override func viewDidLoad() {
        
        presenter.connectView(view: self)
        
        if(self.isHideNavigationBar){
            self.constraintSectionTabHeight?.constant = 0
        }
        
        
        self.setupScreenData()
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.add_new.getLocalized())
        menuArray = [menu1]
    }
    
    func setupScreenData(){
        
        settings.style.buttonBarBackgroundColor = #colorLiteral(red: 0.2823529412, green: 0.3137254902, blue: 0.3490196078, alpha: 1)
        settings.style.buttonBarItemBackgroundColor = #colorLiteral(red: 0.2823529412, green: 0.3137254902, blue: 0.3490196078, alpha: 1)
        settings.style.selectedBarBackgroundColor = #colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1)
        settings.style.selectedBarHeight = 03
        settings.style.buttonBarMinimumLineSpacing = 05
        settings.style.buttonBarItemTitleColor = UIColor.white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        settings.style.buttonBarItemLeftRightMargin = 05
        settings.style.buttonBarItemFont = FontsConfig.FontHelper.defaultRegularFontWithSize(16)
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            //            oldCell?.label.font = FontsConfig.FontHelper.defaultRegularFontWithSize(16)
            newCell?.label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            //newCell?.label.font = FontsConfig.FontHelper.defaultBoldFontWithSize(18)
        }
        
        buttonBarItemSpec = ButtonBarItemSpec.cellClass(width: { _ -> CGFloat in
            return 90  // <-- Your desired width
        })
        super.viewDidLoad()
        //self.setUpLanguageText()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.notificationListViewController = nil
        self.view.layoutSubviews()
        //self.updateBadgeCount()
        
        //call API for refresh screen data
//        self.presenter.getServiceProviderDetail()
        self.presenter.getSubCatServiceProviderList(subCatId: self.subCatId)
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let storyboard = UIStoryboard(name: HelperConstant.ServiceStoryboard, bundle: nil)
        availableMyServiceListView = storyboard.instantiateViewController(withIdentifier: MyServiceListView.nameOfClass) as? MyServiceListView
        availableMyServiceListView?.itemInfo.title = "Available"
        
        unavailableMyServiceListView = storyboard.instantiateViewController(withIdentifier: MyServiceListView.nameOfClass) as? MyServiceListView
        
        unavailableMyServiceListView?.itemInfo.title = "Unavailable"
        
        return [availableMyServiceListView!, unavailableMyServiceListView!]
    }
}

extension MyServiceViewController{
    func updateScreenInfo(){
        if(self.myServiceList != nil && (myServiceList?.count)! > 0){
//            myServiceList
            if(self.availableMyServiceListView != nil){
                //        self.arrSelectedDate = koyomi.model.selectedDates.filter{$0.value}.map{$0.key.appSpecificStringFromFormat()}.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                
                self.availableMyServiceListView?.myServiceList = self.myServiceList?.filter{ $0.status == "1" || $0.status == "Active" }
                
//                self.view.lblNoRecordFound.isHidden = true
//                self.view.tbCatServiceList.isHidden = false
                
                self.availableMyServiceListView?.refreshScr()
            }
            
            if(self.unavailableMyServiceListView != nil){
                self.unavailableMyServiceListView?.myServiceList = self.myServiceList?.filter{$0.status! == "0"}
                
                self.unavailableMyServiceListView?.refreshScr()
            }
        }
    }
}

extension MyServiceViewController{
    /**
     *  Common Back button click event method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous
     */
    @IBAction func actionNavigationBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}


extension MyServiceViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        self.serviceAddOverviewViewController = nil
        if(index == 0){
            
            UserDefault.removeSID()
            
            self.serviceAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddOverviewViewController.nameOfClass)
             self.navigationController?.pushViewController(self.serviceAddOverviewViewController!, animated: true)
            
        }
    }
}

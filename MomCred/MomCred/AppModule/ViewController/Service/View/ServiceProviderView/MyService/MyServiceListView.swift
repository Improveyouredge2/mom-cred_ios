//
//  MyServiceListView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class MyServiceListView: LMBaseViewController, IndicatorInfoProvider {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tbServiceList : UITableView!
    
    @IBOutlet weak var lblNoRecordFound: UILabel!
    
    var itemInfo: IndicatorInfo = "Items"
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    fileprivate var serviceDetailView:ServiceDetailView?
    
    var myServiceList:[ServiceAddRequest]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.presenter.connectView(view: self)
        tbServiceList.estimatedRowHeight = 100
        tbServiceList.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutSubviews()
        self.serviceDetailView = nil
    }
    
}

extension MyServiceListView {
    func refreshScr(){
        if(self.tbServiceList != nil && self.myServiceList != nil && (self.myServiceList?.count)! > 0){
            
            if(self.lblNoRecordFound != nil){
                self.lblNoRecordFound.isHidden = true
            }
            if(self.tbServiceList != nil){
                self.tbServiceList.isHidden = false
                self.tbServiceList.reloadData()
            }
        } else {
            
            if(self.lblNoRecordFound != nil){
                self.lblNoRecordFound.isHidden = false
            }
            
            if(self.tbServiceList != nil){
                self.tbServiceList.isHidden = true
            }
        }
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension MyServiceListView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.arrNoteList.count
        return self.myServiceList != nil ? (myServiceList?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ServiceListTableViewCell = tableView.dequeueReusableCell(withIdentifier: ServiceListTableViewCell.nameOfClass) as! ServiceListTableViewCell
        cell.service = myServiceList?[indexPath.row]
        return cell

        /*
        let data       =    self.myServiceList?[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: MyServiceCell.nameOfClass, for: indexPath) as! MyServiceCell
        
        cell.lblTitle.text          = data?.service_name ?? ""
        
        // TODO: change as per purchase of service count
        cell.lblDate.text           = "0 / \(data?.number_of_purchase ?? "") purchased"
        
//        if(){
//            cell.lblDesciption.text     = data.service_tr
//        } else {
//            cell.lblDesciption.text     = ""
//        }
        
        if(data?.service_location_info != nil){
            var address = ""
            
            if(data?.service_location_info?.location_name != nil && (data?.service_location_info?.location_name?.length)! > 0){
                address = "\(data?.service_location_info?.location_name ?? "") \n\(data?.service_location_info?.location_address ?? "")"
                
            } else if(data?.service_location_info?.location_name_sec != nil && (data?.service_location_info?.location_name_sec?.length)! > 0){
                address = "\(data?.service_location_info?.location_name_sec ?? "") \n\(data?.service_location_info?.location_address_sec ?? "")"
            }
            
            cell.lblDesciption.text = address
        } else {
            cell.lblDesciption.text = ""
        }
        
        if(data?.service_price != nil && data?.service_price?.service_price != nil && (data?.service_price?.service_price?.length)! > 0){
            cell.lblPrice.text = "$\(data?.service_price?.service_price ?? "")"
        } else {
            cell.lblPrice.text = ""
        }
        
        if(data?.mediabusiness != nil && (data?.mediabusiness?.count)! > 0){
            cell.imgProfilePic.sd_setImage(with: URL(string:data?.mediabusiness![0].thumb ?? ""), placeholderImage: #imageLiteral(resourceName: "profile"))
        } else {
            cell.imgProfilePic.image = #imageLiteral(resourceName: "profile")
        }
        
        var serviceTypes = [UIImage]()
        if let creditSystem = data?.credit_system, creditSystem == "1", let imgCredit = UIImage(named: "ic_credit") {
            serviceTypes.append(imgCredit)
        }
        if let isDateEmpty = data?.service_calendar?.date?.isEmpty, let isTimeEmpty = data?.service_calendar?.time?.isEmpty, !isDateEmpty, !isTimeEmpty, let imgCalendar = UIImage(named: "ic_calendar") {
            serviceTypes.append(imgCalendar)
        }
        if let charitableEvent = data?.service_event?.causeCharitableEvent, let charitablePercentage = data?.service_event?.causeCharitablePercentage, !charitableEvent.isEmpty, !charitablePercentage.isEmpty, let imgDonation = UIImage(named: "ic_donation") {
            serviceTypes.append(imgDonation)
        }
        if let purchasability = data?.purchasablity, purchasability == "1", let imgPurchasability = UIImage(named: "ic_purchase") {
            serviceTypes.append(imgPurchasability)
        }
        if let exceptional = data?.service_information?.exceptionalServices, !exceptional.isEmpty, let imgExceptional = UIImage(named: "ic_exceptional") {
            serviceTypes.append(imgExceptional)
        }

        // remove all arranged subviews in case of reusing the cell
        cell.serviceTypesView.removeAllArrangedSubviews()
        for serviceImage in serviceTypes {
            let imageView = UIImageView(image: serviceImage)
            imageView.contentMode = .scaleAspectFit
            cell.serviceTypesView.addArrangedSubview(imageView)
        }
        return cell
 */
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.serviceDetailView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceDetailView.nameOfClass) as ServiceDetailView
        
        if(self.serviceDetailView != nil){
            self.serviceDetailView?.serviceAddRequest = self.myServiceList?[indexPath.row]
            self.navigationController?.pushViewController(self.serviceDetailView!, animated: true)
        }
    }
}

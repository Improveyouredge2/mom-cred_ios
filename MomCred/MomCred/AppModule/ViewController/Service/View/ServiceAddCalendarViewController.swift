//
//  ServiceAddCalendarViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 24/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class ServiceAddCalendarViewController : LMBaseViewController{
    
    fileprivate let formNumber = 2
    
    @IBOutlet weak var switchCalenderListing: UISwitch!
    @IBOutlet weak var listingContainerView: UIView! {
        didSet {
            listingContainerView.isHidden = true
        }
    }

    @IBOutlet weak var btnRepeatYes: UIButton!
    @IBOutlet weak var btnRepeatNo: UIButton!
    
    @IBOutlet weak var btnRepeatEveryDay: UIButton!
    @IBOutlet weak var btnRepeatEveryWeek: UIButton!
    @IBOutlet weak var btnRepeatEveryMonth: UIButton!

    @IBOutlet weak var textFieldDate: UITextField!
    @IBOutlet weak var textFieldPurchaseNumber: UITextField!
    
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var stackViewRepeat: UIStackView!
    @IBOutlet weak var stackViewTimeFrame: UIStackView!
    @IBOutlet weak var stackViewRepeatTime: UIStackView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var serviceCalendar:ServiceCalendar?
    fileprivate var serviceAddCalenderListingViewController:ServiceAddCalenderListingViewController?
    fileprivate var presenter = ServiceAddCalendarPresenter()
    fileprivate var isUpdate = false
    
    var serviceAddExceptionalViewController:ServiceAddExceptionalViewController?
    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        self.setScreenData()
        
        self.textFieldPurchaseNumber.delegate = self
        
        if !screenName.isEmpty {
            lbl_NavigationTitle.text = screenName
        }

        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        self.presenter.getHelpAndSupport()
    }
    
}

extension ServiceAddCalendarViewController: UITextFieldDelegate{
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        let numCheckStatus:Bool = false
        var lenCheckStatus:Bool = false
        let isNumCheckReq = false
        //        var isLenCheckReq = false
        
        // check backspace in input character
        if (isBackSpace == -92) {
            return true
        }
        
        // check new length of string after adding new character
        let newLength = text.count + string.count - range.length
        if newLength > HelperConstant.LIMIT_REPEAT_TIME{
            return false
        }
        
        lenCheckStatus = true
        
        if(isNumCheckReq == true){
            return numCheckStatus && lenCheckStatus
        }else if(lenCheckStatus == true){
            return lenCheckStatus
        }
    }
}

extension ServiceAddCalendarViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData() {
        
        if let serviceCalender = serviceAddRequest?.service_calendar, let serviceDate = serviceCalender.date, let serviceTime = serviceCalender.time {
            self.serviceCalendar = serviceCalender
            
            switchCalenderListing.isOn = !serviceDate.isEmpty || !serviceTime.isEmpty

            textFieldDate.text = "\(serviceDate) \(serviceTime)"
            
            if let repeatStatus = serviceCalender.repeatStatus {
                methodRepeatAction(repeatStatus == "1" ? btnRepeatYes : btnRepeatNo)
            }
            
            if serviceCalender.repeatTimeFrame == "1" {
               btnRepeatEveryDay.isSelected = true
            } else if serviceCalender.repeatTimeFrame == "2" {
                btnRepeatEveryWeek.isSelected = true
            } else if serviceCalender.repeatTimeFrame == "3" {
                btnRepeatEveryMonth.isSelected = true
            }
            
            textFieldPurchaseNumber.text = serviceCalendar?.repeatTimeCount
        } else {
            switchCalenderListing.isOn = false
            serviceCalendar = ServiceCalendar()
        }
        
        listingContainerView.isHidden = !switchCalenderListing.isOn
    }
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        if switchCalenderListing.isOn {
            if let selectedDate = textFieldDate.text?.trim(), selectedDate.isEmpty {
                showBannerAlertWith(message: LocalizationKeys.error_select_date_time.getLocalized(), alert: .error)
                return false
            }
//            else if(self.textFieldPurchaseNumber.text != nil && self.textFieldPurchaseNumber.text?.length == 0){
//                message = LocalizationKeys.error_select_repeat_time.getLocalized()
//                self.showBannerAlertWith(message: message, alert: .error)
//                return false
//            }
        }
        
        //Add info
        
        if (serviceAddRequest == nil) {
            serviceAddRequest = ServiceAddRequest()
        }
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        self.serviceCalendar?.repeatStatus = self.btnRepeatYes.isSelected ? "1" : "0"
        
        var timeFrame = ""
        if(self.btnRepeatEveryDay.isSelected){
            timeFrame = "1"
        } else if(self.btnRepeatEveryWeek.isSelected){
            timeFrame = "2"
        } else if(self.btnRepeatEveryMonth.isSelected){
            timeFrame = "3"
        }
        
        self.serviceCalendar?.repeatTimeFrame = timeFrame
        
        self.serviceCalendar?.repeatTimeCount = self.textFieldPurchaseNumber.text ?? ""
        
        self.serviceAddRequest?.service_calendar = self.serviceCalendar
        
        return true
    }
    
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.serviceAddRequest?.service_calendar?.repeatStatus != self.serviceCalendar?.repeatStatus){
            isUpdate = true
        }
        
        if(self.serviceAddRequest?.service_calendar?.repeatTimeFrame != self.serviceCalendar?.repeatTimeFrame){
            isUpdate = true
        }
        
        if(self.serviceAddRequest?.service_calendar?.repeatTimeCount != self.serviceCalendar?.repeatTimeCount){
            isUpdate = true
        }
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.serviceAddExceptionalViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddExceptionalViewController.nameOfClass) as ServiceAddExceptionalViewController
        self.serviceAddExceptionalViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddExceptionalViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddExceptionalViewController!, animated: true)
    }
    
}

//MARK:- Button Action method implementation
extension ServiceAddCalendarViewController{
    
    @IBAction func methodRepeatAction(_ sender: UIButton) {
        
        self.btnRepeatYes.isSelected = false
        self.btnRepeatNo.isSelected = false
        self.isUpdate = true
        
        if(self.btnRepeatYes == sender){
            self.btnRepeatYes.isSelected = true
            
            self.btnRepeatEveryDay.isEnabled = true
            self.btnRepeatEveryWeek.isEnabled = true
            self.btnRepeatEveryMonth.isEnabled = true
            self.textFieldPurchaseNumber.isEnabled = true
            
        } else {
            self.btnRepeatNo.isSelected = true
            
            self.btnRepeatEveryDay.isEnabled = false
            self.btnRepeatEveryWeek.isEnabled = false
            self.btnRepeatEveryMonth.isEnabled = false
            self.textFieldPurchaseNumber.isEnabled = false
        }
    }
    
    @IBAction func methodRepeatFrameAction(_ sender: UIButton) {
        
        self.btnRepeatEveryDay.isSelected = false
        self.btnRepeatEveryWeek.isSelected = false
        self.btnRepeatEveryMonth.isSelected = false
        self.isUpdate = true
        
        if(self.btnRepeatEveryDay == sender){
            self.btnRepeatEveryDay.isSelected = true
        } else if(self.btnRepeatEveryWeek == sender){
            self.btnRepeatEveryWeek.isSelected = true
        } else {
            self.btnRepeatEveryMonth.isSelected = true
        }
    }
    
    @IBAction func methodSwitchCalenderListingAction(_ sender: UISwitch){
        listingContainerView.isHidden = !sender.isOn
        self.isUpdate = true
        if(self.switchCalenderListing.isOn){
            self.btnChange.isEnabled = true
            
            self.btnRepeatYes.isEnabled = true
            self.btnRepeatNo.isEnabled = true
            
            self.btnRepeatEveryDay.isEnabled = true
            self.btnRepeatEveryWeek.isEnabled = true
            self.btnRepeatEveryMonth.isEnabled = true
            self.textFieldPurchaseNumber.isEnabled = true
            
        } else {
            self.btnChange.isEnabled = false

            self.btnRepeatYes.isEnabled = false
            self.btnRepeatNo.isEnabled = false
            
            self.btnRepeatEveryDay.isEnabled = false
            self.btnRepeatEveryWeek.isEnabled = false
            self.btnRepeatEveryMonth.isEnabled = false
            self.textFieldPurchaseNumber.isEnabled = false
        }
        
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }

    @IBAction func methodChangeTimeAction(_ sender: UIButton){
        
        if(serviceAddCalenderListingViewController == nil){
        serviceAddCalenderListingViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddCalenderListingViewController.nameOfClass) as ServiceAddCalenderListingViewController
            
            serviceAddCalenderListingViewController?.delegate = self
            serviceAddCalenderListingViewController?.singleSelection = true
        }
        
        if(self.serviceAddCalenderListingViewController != nil){
            HelperConstant.appDelegate.navigationController?.present(serviceAddCalenderListingViewController!, animated: true, completion: nil)
        }
    }
    
}

extension ServiceAddCalendarViewController: ServiceAddCalenderListingViewControllerDelegate{
    func saveSelectedInfo(date:String, time:String){
        self.textFieldDate.text = "\(date) \(time)"
        
        self.serviceCalendar?.date = date
        self.serviceCalendar?.time = time
        self.isUpdate = true
    }
}

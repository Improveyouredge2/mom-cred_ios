//
//  ServiceAddAdditionalCostViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 02/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * ServiceAddAdditionalCostViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddAdditionalCostViewController : LMBaseViewController{
    
    fileprivate let formNumber = 11
    
    @IBOutlet weak var btnAdditionalCostYes: UIButton!
    @IBOutlet weak var btnAdditionalCostNo: UIButton!
    @IBOutlet weak var viewAdditionalCost: UIView!
    
    @IBOutlet weak var textFieldServicePrice: UITextField!
    
    // Listing Include
    @IBOutlet weak var btnIncludedInListingYes: UIButton!
    @IBOutlet weak var btnIncludedInListingNo: UIButton!
    @IBOutlet weak var viewIncludedInListing: UIView!
    @IBOutlet weak var constraintRequireFacilityRentalHeight: NSLayoutConstraint!
    
    // Additional Service option
    @IBOutlet weak var textFieldAdditionalServiceOptionTitle: RYFloatingInput!
    @IBOutlet weak var textViewAdditionalServiceOptionDesc: KMPlaceholderTextView!
    
    @IBOutlet weak var tableViewAddedCost: UITableView!
    @IBOutlet weak var constraintsAddCostTableViewHeight:NSLayoutConstraint!
    @IBOutlet weak var viewTableViewAddedCost: UIView!
    
    @IBOutlet weak var textFieldAdditionalCost: RYFloatingInput!
    @IBOutlet weak var textViewAdditionalCostDesc: KMPlaceholderTextView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    // Var
    fileprivate var serviceGainHeight:CGFloat = 0
    fileprivate var facilitieGainHeight:CGFloat = 0
    
    fileprivate var tableViewListSelection:TableViewListSelectionViewController?
    
    fileprivate var addedCostInfoList:[AddedCostInfo] = []
    fileprivate var serviceAddSkillAgeViewController:ServiceAddSkillAgeViewController?
    fileprivate var servicePrice:ServicePrice?
    fileprivate var isUpdate = false
    fileprivate var presenter = ServiceAddAdditionalCostPresenter()
    
    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableViewListSelection = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(addedCostInfoList.count == 0){
            self.viewTableViewAddedCost.isHidden = true
        }
        
        if(self.tableViewAddedCost.contentSize.height > 0){
            self.constraintsAddCostTableViewHeight?.constant = self.tableViewAddedCost.contentSize.height
            
            self.updateTableViewHeight(tableView: self.tableViewAddedCost)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewAddedCost == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewAddedCost.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewAddedCost.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintsAddCostTableViewHeight.constant = heightOfTableView
            }
        }
    }
}

extension ServiceAddAdditionalCostViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        self.textFieldServicePrice.delegate = self
        
        self.viewIncludedInListing.isHidden = true
        
        self.btnIncludedInListingYes.isSelected = true
        self.btnIncludedInListingNo.isSelected = false
        
        self.methodAdditionalPriceAction(btnAdditionalCostNo)
  
        configTextField()
        
        servicePrice = serviceAddRequest?.service_price
        if let servicePrice = servicePrice {
            textFieldServicePrice.text = servicePrice.service_price

            if let additionalCost = servicePrice.additionalCost, !additionalCost.isEmpty {
                methodAdditionalPriceAction(btnAdditionalCostYes)
                addedCostInfoList = additionalCost
                tableViewAddedCost.reloadData()
            }
        }
    }
    
    fileprivate func configTextField(){
        
        // textFieldAdditionalServiceOptionTitle
        self.textFieldAdditionalServiceOptionTitle.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                //.placeholer("Option name")
                .placeholer("Price Name")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        // textFieldAdditionalCost
        self.textFieldAdditionalCost.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("What is price of this additional service option?")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        self.textFieldAdditionalCost.input.keyboardType = UIKeyboardType.numberPad
        
        self.textViewAdditionalServiceOptionDesc.placeholder = "Description of additional cost"
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        //if ((self.textFieldServicePrice.text?.isEmpty)! || (Float(textFieldServicePrice.text ?? "0.00") ?? 0) == 0){
        if ((self.textFieldServicePrice.text?.isEmpty)!){

            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_price.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        
        if(self.btnAdditionalCostYes.isSelected){
            if(self.addedCostInfoList.count == 0){
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_additional_cost.getLocalized(), buttonTitle: nil, controller: nil)
                return false
            }
        }
        
        if(self.serviceAddRequest == nil){
            self.serviceAddRequest = ServiceAddRequest()
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        if(self.servicePrice == nil){
            self.servicePrice = ServicePrice()
        }
        
        self.servicePrice?.service_price = self.textFieldServicePrice.text ?? ""
        self.servicePrice?.additionalCost = self.addedCostInfoList
        
        self.serviceAddRequest?.service_price = self.servicePrice
        
        return true
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.serviceAddRequest?.service_price != nil){
            
            if(self.servicePrice?.service_price != (self.textFieldServicePrice.text ?? "")){
                isUpdate = true
            }
            
            if(self.isUpdate){
                isUpdate = self.isUpdate
            }
            
        } else {
            isUpdate = true
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.serviceAddSkillAgeViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddSkillAgeViewController.nameOfClass) as ServiceAddSkillAgeViewController
        
        self.serviceAddSkillAgeViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddSkillAgeViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddSkillAgeViewController!, animated: true)
    }
    
}


//MARK: Textfield delegates
extension ServiceAddAdditionalCostViewController : UITextFieldDelegate{
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        
        var limitLength = 0
        
        if textField == textFieldServicePrice {
            limitLength = HelperConstant.LIMIT_AMOUNT
        }
        
        // Amt field validation
        if textField == textFieldServicePrice {
            return textFieldServicePrice.validatePriceField(string: string, text: textField.text!, maxLength: HelperConstant.LIMIT_AMOUNT, range: range)
        }
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            return true
        }
        
        let newLength = text.count + string.count - range.length
        if newLength > limitLength {
            return false
        }
        return true
    }
}

//MARK:- IBAction method implementation
//MARK:-
extension ServiceAddAdditionalCostViewController{
    
    @IBAction func methodAdditionalPriceAction(_ sender: UIButton) {
        
        self.btnAdditionalCostYes.isSelected = false
        self.btnAdditionalCostNo.isSelected = false
        
        if(self.btnAdditionalCostYes == sender){
            self.btnAdditionalCostYes.isSelected = true
            self.viewAdditionalCost.isHidden = false
            
        } else if(self.btnAdditionalCostNo == sender){
            self.btnAdditionalCostNo.isSelected = true
            self.viewAdditionalCost.isHidden = true
        }
    }
    
    @IBAction func methodIncludedInListingAction(_ sender: UIButton) {
        
        self.btnIncludedInListingYes.isSelected = false
        self.btnIncludedInListingNo.isSelected = false
        
        if(self.btnIncludedInListingYes == sender){
            self.btnIncludedInListingYes.isSelected = true
            self.viewIncludedInListing.isHidden = true
            
        } else if(self.btnIncludedInListingNo == sender){
            self.btnIncludedInListingNo.isSelected = true
            self.viewIncludedInListing.isHidden = false
        }
    }
    
    @IBAction func methodAddCostAction(_ sender: UIButton){
        let addedCostInfo = AddedCostInfo()
        
        if((self.textFieldAdditionalServiceOptionTitle.text()?.length)! > 0){
            addedCostInfo.name = self.textFieldAdditionalServiceOptionTitle.text() ?? ""
            
            self.textFieldAdditionalServiceOptionTitle.input.text = ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_plese_enter_cost_name.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }

        if((self.textViewAdditionalServiceOptionDesc.text?.length)! > 0){
            addedCostInfo.desc = self.textViewAdditionalServiceOptionDesc.text ?? ""
            
            self.textViewAdditionalServiceOptionDesc.text = ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_please_enter_cost_description.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }

        if(self.btnIncludedInListingNo.isSelected){
            self.methodIncludedInListingAction(btnIncludedInListingYes)
            if((self.textFieldAdditionalCost.text()?.length)! > 0){
                addedCostInfo.price = self.textFieldAdditionalCost.text() ?? ""
                
                self.textFieldAdditionalCost.input.text = ""
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_please_enter_price.getLocalized(), buttonTitle: nil, controller: nil)
                
                return
            }

            if((self.textViewAdditionalCostDesc.text?.length)! > 0){
                addedCostInfo.pricedesc = self.textViewAdditionalCostDesc.text ?? ""
                
                self.textViewAdditionalCostDesc.text = ""
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_please_enter_instructions.getLocalized(), buttonTitle: nil, controller: nil)
                
                return
            }
        }
        
        self.isUpdate = true
        self.addedCostInfoList.append(addedCostInfo)
        self.tableViewAddedCost.reloadData()
        self.viewTableViewAddedCost.isHidden = false
    }
    
    //MARK:- Button Action method implementation
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddAdditionalCostViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return addedCostInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AddedCostTableViewCell.nameOfClass) as! AddedCostTableViewCell
        
        if(addedCostInfoList.count > indexPath.row){
            let objInfo = addedCostInfoList[indexPath.row]
            
            cell.cellIndex = indexPath
            cell.delegate = self
            
            cell.lblName.text = objInfo.name
            cell.lblDesc.text = objInfo.desc
            cell.lblStatus.text = "Yes"
            
            if(objInfo.price.trim().length > 0){
                cell.stackViewPrice.isHidden = false
                cell.stackViewPriceDesc.isHidden = false
                
                cell.lblPrice.text = objInfo.price.trim()
                cell.lblPriceDesc.text = objInfo.pricedesc.trim()
            } else {
                cell.stackViewPrice.isHidden = true
                cell.stackViewPriceDesc.isHidden = true
            }
        }
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ServiceAddAdditionalCostViewController:AddedCostTableViewCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        if(self.addedCostInfoList.count > 0){
            self.addedCostInfoList.remove(at: cellIndex?.row ?? 0)
            self.tableViewAddedCost.reloadData()
            self.isUpdate = true
            if(addedCostInfoList.count == 0){
                self.viewTableViewAddedCost.isHidden = true
            }
        }
    }
}

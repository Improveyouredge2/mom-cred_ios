//
//  ServiceAddInsideLookContentAssociatedViewController.swift
//  MomCred
//
//  Created by MD on 25/07/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit
import iOSDropDown

/**
 * ServiceAddInsideLookContentAssociatedViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddInsideLookContentAssociatedViewController : LMBaseViewController{
    
    fileprivate let formNumber = 18
        
    @IBOutlet weak var tblInsideLook: BIDetailTableView! {
        didSet {
            tblInsideLook.sizeDelegate = self
        }
    }
    
    @IBOutlet weak var tblInsideLookHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var serviceAddICAController: ServiceAddInstructionalContentAssociatedViewController?
    fileprivate var isUpdate = false
    fileprivate var presenter = ServiceAddInsideLookContentAssociatedPresenter()
    
    private var serviceInsideLookItems: [ServiceInsideLookItem] = [] {
        didSet {
            tblInsideLook.reloadData()
        }
    }
    private var insideLookResponse: [ILCAddRequest] = [] {
        didSet {
            tblInsideLook.reloadData()
        }
    }

    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if !screenName.isEmpty {
           lbl_NavigationTitle.text = screenName
        }
        
        tblInsideLook.backgroundColor = UIColor.clear
        
        if let existingInsideLookItems = serviceAddRequest?.service_insidelook {
            serviceInsideLookItems = existingInsideLookItems
        }
        
        presenter.getInsideLook { [weak self] response in
            if let ilcResponse = response?.data {
                self?.insideLookResponse = ilcResponse
            }
        }
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
}

extension ServiceAddInsideLookContentAssociatedViewController {
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{

        if (UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && serviceAddRequest?.service_id == nil) {
            serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if (serviceAddRequest?.service_id == nil) {
            serviceAddRequest?.service_id = ""
        }
        
        serviceAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        serviceAddRequest?.service_insidelook = serviceInsideLookItems

        return true
    }
    
    fileprivate func openNextScr() {
        serviceAddICAController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddInstructionalContentAssociatedViewController.nameOfClass) as ServiceAddInstructionalContentAssociatedViewController
        serviceAddICAController?.serviceAddRequest = serviceAddRequest
        serviceAddICAController?.isEditingService = isEditingService
        navigationController?.pushViewController(serviceAddICAController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension ServiceAddInsideLookContentAssociatedViewController {
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isUpdate {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddInsideLookContentAssociatedViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceInsideLookItems.count + 1
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddMoreCell.nameOfClass) as! AddMoreCell
            
            cell.textField.placeholder = "Link"
            if (cell.ViewRoleDesc != nil) {
                cell.ViewRoleDesc.isHidden = true
            }
            cell.delegate = self
            cell.refTableView = tableView
            
            let listing = insideLookResponse.map { ServiceListingList(id: $0.look_id, title: $0.look_name) }
            cell.updateDropDown(listInfo: listing)

            cell.backgroundColor = UIColor.clear
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
            
            cell.delegate = self
            cell.refTableView = tableView
            
            cell.lblTitle.text = serviceInsideLookItems[indexPath.row - 1].name
            cell.lblTitleDesc.text = serviceInsideLookItems[indexPath.row - 1].desc
            cell.lblRoleDesc.isHidden = true

            cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
            
            cell.backgroundColor = UIColor.clear
            return cell
        }
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ServiceAddInsideLookContentAssociatedViewController: AddMoreCellDelegate {
    func newText(text: String, textDesc: String, textRoleDesc: String, refTableView: UITableView?, selectedServiceListingList: ServiceListingList?) {
        
        let insideLookItem = ServiceInsideLookItem()
        insideLookItem.name = text
        insideLookItem.desc = textDesc
        
        if textDesc.isEmpty {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_primary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
        } else if let serviceList = selectedServiceListingList {
            insideLookItem.id = serviceList.id
            insideLookItem.name = serviceList.title

            if serviceInsideLookItems.count < HelperConstant.minimumBlocks {
                if serviceInsideLookItems.map({ $0.id }).contains(serviceList.id) {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                } else {
                    isUpdate = true
                    serviceInsideLookItems.append(insideLookItem)
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_facility_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension ServiceAddInsideLookContentAssociatedViewController: TitleDescViewDelegate {
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?) {
        isUpdate = true
        if let index = cellIndex?.row, serviceInsideLookItems.count > index {
            serviceInsideLookItems.remove(at: index)
        }
    }
}

extension ServiceAddInsideLookContentAssociatedViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        tblInsideLookHeight.constant = size.height
    }
}

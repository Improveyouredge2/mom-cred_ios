//
//  ServiceAddInstructionalContentAssociatedViewController.swift
//  MomCred
//
//  Created by MD on 25/07/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit
import iOSDropDown

class ServiceAddInstructionalContentAssociatedViewController : LMBaseViewController{
    
    fileprivate let formNumber = 20
    
    @IBOutlet weak var tblInstructionalContent: BIDetailTableView! {
        didSet {
            tblInstructionalContent.sizeDelegate = self
        }
    }
    
    @IBOutlet weak var tblInstructionalContentHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var serviceAddListingServiceViewController: ServiceAddListingServiceViewController?
    fileprivate var isUpdate = false
    fileprivate var presenter = ServiceAddInstructionalContentAssociatedPresenter()
    
    private var serviceInstructionalContentItems: [ServiceInsideLookItem] = [] {
        didSet {
            tblInstructionalContent.reloadData()
        }
    }
    private var instructionalContentResponse: [ICAddRequest] = [] {
        didSet {
            tblInstructionalContent.reloadData()
        }
    }

    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if !screenName.isEmpty {
           lbl_NavigationTitle.text = screenName
        }
        
        tblInstructionalContent.backgroundColor = UIColor.clear
        
        if let existingInstructionalContentItems = serviceAddRequest?.service_instructionalcontent {
            serviceInstructionalContentItems = existingInstructionalContentItems
        }
        
        presenter.getInstructionalContent { [weak self] response in
            if let icResponse = response?.data {
                self?.instructionalContentResponse = icResponse
            }
        }
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
}

extension ServiceAddInstructionalContentAssociatedViewController {
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{

        if (UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && serviceAddRequest?.service_id == nil) {
            serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if (serviceAddRequest?.service_id == nil) {
            serviceAddRequest?.service_id = ""
        }
        
        serviceAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        serviceAddRequest?.service_instruction = serviceInstructionalContentItems

        return true
    }
    
    fileprivate func openNextScr() {
        serviceAddListingServiceViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddListingServiceViewController.nameOfClass) as ServiceAddListingServiceViewController
        
        serviceAddListingServiceViewController?.serviceAddRequest = serviceAddRequest
        serviceAddListingServiceViewController?.isEditingService = isEditingService
        navigationController?.pushViewController(serviceAddListingServiceViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension ServiceAddInstructionalContentAssociatedViewController {
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isUpdate {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddInstructionalContentAssociatedViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceInstructionalContentItems.count + 1
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddMoreCell.nameOfClass) as! AddMoreCell
            
            cell.textField.placeholder = "Link"
            if (cell.ViewRoleDesc != nil) {
                cell.ViewRoleDesc.isHidden = true
            }
            cell.delegate = self
            cell.refTableView = tableView
            
            let listing = instructionalContentResponse.map { ServiceListingList(id: $0.content_id, title: $0.content_name) }
            cell.updateDropDown(listInfo: listing)

            cell.backgroundColor = UIColor.clear
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
            
            cell.delegate = self
            cell.refTableView = tableView
            
            cell.lblTitle.text = serviceInstructionalContentItems[indexPath.row - 1].name
            cell.lblTitleDesc.text = serviceInstructionalContentItems[indexPath.row - 1].desc
            cell.lblRoleDesc.isHidden = true
            
            cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
            
            cell.backgroundColor = UIColor.clear
            return cell
        }
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ServiceAddInstructionalContentAssociatedViewController: AddMoreCellDelegate {
    func newText(text: String, textDesc: String, textRoleDesc: String, refTableView: UITableView?, selectedServiceListingList: ServiceListingList?) {
        
        let insideLookItem = ServiceInsideLookItem()
        insideLookItem.name = text
        insideLookItem.desc = textDesc
        
        if textDesc.isEmpty {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_primary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
        } else if let serviceList = selectedServiceListingList {
            insideLookItem.id = serviceList.id
            insideLookItem.name = serviceList.title

            if serviceInstructionalContentItems.count < HelperConstant.minimumBlocks {
                if serviceInstructionalContentItems.map({ $0.id }).contains(serviceList.id) {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                } else {
                    isUpdate = true
                    serviceInstructionalContentItems.append(insideLookItem)
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_facility_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension ServiceAddInstructionalContentAssociatedViewController: TitleDescViewDelegate {
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?) {
        isUpdate = true
        if let index = cellIndex?.row, serviceInstructionalContentItems.count > index {
            serviceInstructionalContentItems.remove(at: index)
        }
    }
}

extension ServiceAddInstructionalContentAssociatedViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        tblInstructionalContentHeight.constant = size.height
    }
}

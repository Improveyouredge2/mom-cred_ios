//
//  ServiceAddEventClassificationViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 02/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

/**
 * ServiceAddEventClassificationViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddEventClassificationViewController : LMBaseViewController{
    
    //fileprivate let formNumber = 7
    fileprivate let formNumber = 6
    
    // Event
    @IBOutlet weak var btnEventYes: UIButton!
    @IBOutlet weak var btnEventNo: UIButton!
    @IBOutlet weak var viewEventDetail: UIView!
    
    @IBOutlet weak var dropDownEventType: DropDown!
    @IBOutlet weak var textFieldSpecificEvent: RYFloatingInput!
    
    // Event Classification
    @IBOutlet weak var btnEventClassificationStandard: UIButton!
    @IBOutlet weak var btnEventClassificationCharitable: UIButton!
    @IBOutlet weak var viewEventClassificationCharitable: UIView!
    @IBOutlet weak var textFieldSpecificEventCharitable: RYFloatingInput!
    @IBOutlet weak var textFieldSpecificEventPercentDonate: RYFloatingInput!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    var serviceAddRequest:ServiceAddRequest?
    
    fileprivate var eventTypeDetail:DetailData?
    fileprivate var serviceAddSpecificGeneralDescriptionViewController:ServiceAddSpecificGeneralDescriptionViewController?
    fileprivate var serviceEvent:ServiceEvent?
    fileprivate var presenter = ServiceAddEventClassificationPresenter()
    
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serviceAddSpecificGeneralDescriptionViewController = nil
    }
}

extension ServiceAddEventClassificationViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        self.viewEventDetail.isHidden = true
        self.dropDownEventType.isHidden = true
        self.textFieldSpecificEvent.isHidden = true
        
        self.textFieldSpecificEvent.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Specific event type")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        self.textFieldSpecificEventCharitable.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Cause for the charitable event")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        self.textFieldSpecificEventPercentDonate.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Percent of funds that will be donated")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.textFieldSpecificEventPercentDonate.input.keyboardType = UIKeyboardType.numberPad
        
        // Travel Service
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        self.dropDownEventType.optionArray = ["Race", "Admission","Tryouts", "New event type..."]
        //Its Id Values and its optional
        self.dropDownEventType.optionIds = [1,2,3,1001]
        
//        self.dropDownEventType.selectedIndex = 0
        // The the Closure returns Selected Index and String
        self.dropDownEventType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            
            if(self.eventTypeDetail == nil){
                self.eventTypeDetail = DetailData()
            }
            
            if(id == 1001){
                self.textFieldSpecificEvent.isHidden = false
                self.eventTypeDetail?.id = ""
                self.eventTypeDetail?.title = ""
                
            } else {
                self.textFieldSpecificEvent.isHidden = true
                self.eventTypeDetail?.id = "\(id)"
                self.eventTypeDetail?.title = selectedText
            }
        }
        ////////////////////////////////////////////////////////////
        
        if(self.serviceAddRequest?.service_event != nil){
            self.methodEventAction(self.btnEventYes)
            self.serviceEvent = self.serviceAddRequest?.service_event
            
            self.eventTypeDetail = DetailData()
            self.eventTypeDetail?.id = self.serviceEvent?.typeTitleId ?? ""
            self.eventTypeDetail?.title = self.serviceEvent?.typeTitle ?? ""
            self.dropDownEventType.text = self.serviceEvent?.typeTitle ?? ""
            
            if(self.serviceEvent?.specificEventType == "1"){
                self.methodEventClassificationAction(self.btnEventClassificationCharitable)
            }
            
            if(self.serviceEvent?.causeCharitableEvent != nil && (self.serviceEvent?.causeCharitableEvent?.length)! > 0){
                self.textFieldSpecificEventCharitable.input.text = self.serviceEvent?.causeCharitableEvent ?? ""
            }
            
            if(self.serviceEvent?.causeCharitablePercentage != nil && (self.serviceEvent?.causeCharitablePercentage?.length)! > 0){
                self.textFieldSpecificEventPercentDonate.input.text = self.serviceEvent?.causeCharitablePercentage ?? ""
            }
        }
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if(self.btnEventYes.isSelected){
            
            if(self.eventTypeDetail == nil){
                message = LocalizationKeys.error_event_type.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            if (self.eventTypeDetail != nil && self.eventTypeDetail?.id == "" && (self.textFieldSpecificEvent.text()?.trim().isEmpty)! ){
                message = LocalizationKeys.error_event_type_manual.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            //
            if(self.btnEventClassificationCharitable.isSelected){
//
                if (self.textFieldSpecificEventCharitable.text()?.trim().isEmpty)!{
                    message = LocalizationKeys.error_event_charitable.getLocalized()
                    self.showBannerAlertWith(message: message, alert: .error)
                    return false
                }
                
                if (self.textFieldSpecificEventPercentDonate.text()?.trim().isEmpty)!{
                    message = LocalizationKeys.error_event_charitable_donate.getLocalized()
                    self.showBannerAlertWith(message: message, alert: .error)
                    return false
                }
            }
            
            if(self.serviceAddRequest == nil){
                self.serviceAddRequest = ServiceAddRequest()
            }
            
            self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
            
            if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
                self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
            } else if(self.serviceAddRequest?.service_id == nil){
                self.serviceAddRequest?.service_id = ""
            }
            
            if(self.serviceEvent == nil){
                self.serviceEvent = ServiceEvent()
            }
            
            self.serviceEvent?.typeTitleId = eventTypeDetail?.id
            self.serviceEvent?.typeTitle = eventTypeDetail?.title
            
            self.serviceEvent?.specificEventType = self.btnEventClassificationStandard.isSelected ? "1" : "0"
            
            if(self.btnEventClassificationCharitable.isSelected){
                self.serviceEvent?.causeCharitableEvent = self.textFieldSpecificEventCharitable.text() ?? ""
                self.serviceEvent?.causeCharitablePercentage = self.textFieldSpecificEventPercentDonate.text() ?? ""
            } else {
                self.serviceEvent?.causeCharitableEvent = nil
                self.serviceEvent?.causeCharitablePercentage = nil
            }
        }
        return true
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.serviceAddRequest?.service_event != nil){
            
            if(eventTypeDetail?.id != (self.serviceEvent?.typeTitleId ?? "")){
                isUpdate = true
            }
            
            if(eventTypeDetail?.title != (self.serviceEvent?.typeTitle ?? "")){
                isUpdate = true
            }
            
            if(self.serviceEvent?.specificEventType == "1" && !self.btnEventClassificationStandard.isSelected){
                isUpdate = true
            }
            
            if(self.serviceEvent?.causeCharitableEvent != self.textFieldSpecificEventCharitable.input.text){
                isUpdate = true
            }
            
            if(self.serviceEvent?.causeCharitablePercentage != self.textFieldSpecificEventPercentDonate.input.text){
                isUpdate = true
            }
            
        } else {
            isUpdate = true
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.serviceAddSpecificGeneralDescriptionViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddSpecificGeneralDescriptionViewController.nameOfClass) as ServiceAddSpecificGeneralDescriptionViewController
        
        self.serviceAddSpecificGeneralDescriptionViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddSpecificGeneralDescriptionViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddSpecificGeneralDescriptionViewController!, animated: true)
    }
}

extension ServiceAddEventClassificationViewController{
    
    @IBAction func methodEventAction(_ sender: UIButton) {
        
        self.btnEventYes.isSelected = false
        self.btnEventNo.isSelected = false
        
        if(self.btnEventYes == sender){
            
            if(self.btnEventYes.isSelected != sender.isSelected){
                self.methodEventClassificationAction(btnEventClassificationStandard)
            }
            
            self.dropDownEventType.isHidden = false
            self.btnEventYes.isSelected = true
            self.viewEventDetail.isHidden = false
            
            
        } else if(self.btnEventNo == sender){
            self.dropDownEventType.isHidden = true
            self.btnEventNo.isSelected = true
            self.viewEventDetail.isHidden = true
        }
    }
    
    @IBAction func methodEventClassificationAction(_ sender: UIButton) {
        
        self.btnEventClassificationStandard.isSelected = false
        self.btnEventClassificationCharitable.isSelected = false
        
        if(self.btnEventClassificationStandard == sender){
            self.btnEventClassificationStandard.isSelected = true
            self.viewEventClassificationCharitable.isHidden = true
        } else if(self.btnEventClassificationCharitable == sender){
            self.btnEventClassificationCharitable.isSelected = true
            self.viewEventClassificationCharitable.isHidden = false
        }
    }
    
    //MARK:- Button Action method implementation
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

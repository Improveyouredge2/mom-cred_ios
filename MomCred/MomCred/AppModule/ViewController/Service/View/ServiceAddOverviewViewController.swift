//
//  ServiceAddOverviewViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 04/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import AVKit
import iOSDropDown

class ServiceAddOverviewViewController : LMBaseViewController{
    
    fileprivate let formNumber = 1
    
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn : UIImageView!
    @IBOutlet weak var documentImgCollectionView: UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var textViewSpecificDesc: KMPlaceholderTextView!
    
    @IBOutlet weak var btnPurchasebleYes: UIButton!
    @IBOutlet weak var btnPurchasebleNo: UIButton!
    @IBOutlet weak var btnDonationServiceYes: UIButton!
    @IBOutlet weak var btnDonationServiceNo: UIButton!
    @IBOutlet weak var btnCreditSystemYes: UIButton!
    @IBOutlet weak var btnCreditSystemNo: UIButton!
    
    @IBOutlet weak var purchasableCountContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var textFieldPurchaseNumber: UITextField!

    @IBOutlet weak var targetGoalCollectionView: UICollectionView!
    
    @IBOutlet weak var dropDownLocation: DropDown!
    @IBOutlet weak var lblLocationAddress: UILabel!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false
    
    var arrDocumentImages = [UploadImageData]()

    fileprivate var presenter = ServiceAddPresenter()
    
    fileprivate var targetGoal : [ListingDataDetail]?
    
    fileprivate var isUpdate = false
    
    var serviceAddRequest:ServiceAddRequest? = ServiceAddRequest()
    
    fileprivate var defaultDocumentCollectionHeight:CGFloat = 0.00
    
    fileprivate var logoutViewController:PMLogoutView?
    fileprivate var selectedIndex = -1
    fileprivate var selectedDocumentImages:UploadImageData?
    fileprivate var serviceAddCalendarViewController:ServiceAddCalendarViewController?
    var isScreenDataReload = false
    var screenName:String = ""
    
    var busiLocation:PersonnelBusiLocationResponseData?
    var selectedLocationIndex:String? = "0"
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isScreenDataReload = true
        presenter.connectView(view: self)
        
        self.defaultDocumentCollectionHeight = self.constraintDocumentCollectionHeight.constant
        self.constraintDocumentCollectionHeight.constant = 0.0
        self.presenter.serverBusiLocationRequest()
        
        self.checkBusinessLocalImage()
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.serviceAddCalendarViewController = nil
        
        self.documentImgCollectionView.backgroundColor = UIColor.clear
        
        if(isScreenDataReload){
            self.presenter.serverParentListingRequest()
        }
    }
}

extension ServiceAddOverviewViewController{
    
    /**
     *  Check local image in business information id folder
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func checkBusinessLocalImage(){
        
        if((self.serviceAddRequest != nil && self.serviceAddRequest?.service_id != nil) || (UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0)){
            var busi_id = ""
            
            if(self.serviceAddRequest != nil && self.serviceAddRequest?.service_id != nil){
                busi_id = self.serviceAddRequest?.service_id ?? ""
            } else {
                busi_id = UserDefault.getSID() ?? ""
            }
            
            let folderName = "service_\(busi_id)"
            if(PMFileUtils.directoryExistsAtPath(folderName)){
                
                let fileUrls:[URL]? = PMFileUtils.getFolderAllFileUrls(folderName: folderName)
                print("Urls: \(fileUrls ?? [])")
                
                if(fileUrls != nil && (fileUrls?.count)! > 0){
                    do {
                        for fileUrl in (fileUrls)! {
                            
                            let imageData = try Data(contentsOf: fileUrl)
                            
                            let theFileName = (fileUrl.absoluteString as NSString).lastPathComponent
                            let theExt = (fileUrl.absoluteString as NSString).pathExtension
                            
                            
                            let imageDataTemp = UploadImageData()
                            
                            imageDataTemp.imageData = imageData
                            imageDataTemp.imageName = theFileName
                            imageDataTemp.imageKeyName = "busi_img"
                            if(theExt.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpeg") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame){
                                imageDataTemp.imageType = "1"
                            } else {
                                imageDataTemp.imageType = "2"
                            }
                            
                            self.arrDocumentImages.append(imageDataTemp)
                            
                        }
                    } catch {
                        //                    print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
                        print("Error while enumerating files")
                    }
                    
                    if !arrDocumentImages.isEmpty {
                        documentImgCollectionView.reloadData()
                    }
                }
            }
        }
    }
    
    func updateScreenListing(){
        self.targetGoal = ServiceAddPresenter.parentListingList?.parentListingData?.targetgoal ?? []
        
        self.targetGoalCollectionView.reloadData()
    }
    
    func updateLocationInfo(){
        
        // dropDownLocation
        // Location list
        ////////////////////////////////////////////////////////////
        dropDownLocation.optionArray = []
        dropDownLocation.optionIds = []
        
        if(self.busiLocation != nil){
            if(self.busiLocation?.location_name != nil && (self.busiLocation?.location_name?.length)! > 0){
                
                dropDownLocation.optionArray.append(self.busiLocation?.location_name ?? "")
                dropDownLocation.optionIds?.append(1)
            }
            
            if(self.busiLocation?.location_name_sec != nil && (self.busiLocation?.location_name_sec?.length)! > 0){
                dropDownLocation.optionArray.append(self.busiLocation?.location_name_sec ?? "")
                dropDownLocation.optionIds?.append(2)
            }
            
            // The the Closure returns Selected Index and String
            dropDownLocation.didSelect{(selectedText , index ,id) in
                print("Selected String: \(selectedText) \n index: \(index)")
                self.selectedLocationIndex = "\(id)"
                
                var address = ""
                
                if(self.busiLocation?.location_name != nil && (self.busiLocation?.location_name?.length)! > 0) && index == 0{
                    address = "\(self.busiLocation?.location_name ?? "") \n\(self.busiLocation?.location_address ?? "")"
                    
                } else if(self.self.busiLocation?.location_name_sec != nil && (self.busiLocation?.location_name_sec?.length)! > 0){
                    address = "\(self.busiLocation?.location_name_sec ?? "") \n\(self.busiLocation?.location_address_sec ?? "")"
                }
                self.lblLocationAddress.text = address
                
                self.isUpdate = true
            }
            ///////////////////////////////////////////////////////////
        }
    }
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData() {
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Service Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        self.textFieldPurchaseNumber.delegate = self
        
        if(self.serviceAddRequest?.mediabusiness != nil && (self.serviceAddRequest?.mediabusiness?.count)! > 0){
            self.imgProfile.sd_setImage(with: URL(string: (self.serviceAddRequest?.mediabusiness![0].thumb ?? "")))
            
            // Check video on first pos
            if((self.serviceAddRequest?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.constraintDocumentCollectionHeight.constant = self.defaultDocumentCollectionHeight
        }
        
        self.inputName.input.text = self.serviceAddRequest?.service_name ?? ""
        textViewSpecificDesc.text = serviceAddRequest?.service_desc
        
        let isPurchasable = serviceAddRequest?.purchasablity == "1"
        methodPurchasabilityAction(isPurchasable ? btnPurchasebleYes : btnPurchasebleNo)
        
        if(self.serviceAddRequest?.credit_system != nil){
            self.btnCreditSystemYes.isSelected = (self.serviceAddRequest?.credit_system != nil) ? (Int(self.serviceAddRequest?.credit_system ?? "0") == 0 ? false : true) : (false)
            
            if(!self.btnCreditSystemYes.isSelected){
                self.btnCreditSystemNo.isSelected = true
            }
        }
        
        let isDonationEnabled = serviceAddRequest?.service_donation == "1"
        methodDonationServiceAction(isDonationEnabled ? btnDonationServiceYes : btnDonationServiceNo)

        if(self.serviceAddRequest?.number_of_purchase != nil){
            self.textFieldPurchaseNumber.text = self.serviceAddRequest?.number_of_purchase ?? ""
        }
        
        // update collection info
        if(self.serviceAddRequest?.mediabusiness != nil && (self.serviceAddRequest?.mediabusiness?.count)! > 0){
            
            //            self.arrDocumentImages
            for mediaBusiness in (self.serviceAddRequest?.mediabusiness)! {
                let mediaImage = UploadImageData()
                mediaImage.imageUrl = mediaBusiness.thumb ?? ""
                mediaImage.id = mediaBusiness.media_id ?? ""
                if let mediaExt = Int(mediaBusiness.media_extension ?? "0"){
                    if(mediaExt == 0 || mediaExt == 1){
                        mediaImage.imageType = "1"
                    } else {
                        mediaImage.imageType = "2"
                    }
                } else {
                    mediaImage.imageType = "1"
                }
                
                self.arrDocumentImages.append(mediaImage)
            }
            self.documentImgCollectionView.reloadData()
        }
        
        if(self.serviceAddRequest?.service_location_info != nil){
            var locationName = ""
            
            if(self.serviceAddRequest?.service_location_info?.location_name != nil && (self.serviceAddRequest?.service_location_info?.location_name?.length)! > 0){
                locationName = self.serviceAddRequest?.service_location_info?.location_name ?? ""
            } else if(self.serviceAddRequest?.service_location_info?.location_name_sec != nil && (self.serviceAddRequest?.service_location_info?.location_name_sec?.length)! > 0){
                locationName = self.serviceAddRequest?.service_location_info?.location_name_sec ?? ""
            }
            
            self.dropDownLocation.text = locationName
        }
        self.selectedLocationIndex = self.serviceAddRequest?.service_location
        
        if(self.serviceAddRequest?.service_location_info != nil){
            var address = ""
            
            if(self.serviceAddRequest?.service_location_info?.location_name != nil && (self.serviceAddRequest?.service_location_info?.location_name?.length)! > 0){
                address = "\(self.serviceAddRequest?.service_location_info?.location_name ?? "") \n\(self.serviceAddRequest?.service_location_info?.location_address ?? "")"
                
            } else if(self.serviceAddRequest?.service_location_info?.location_name_sec != nil && (self.serviceAddRequest?.service_location_info?.location_name_sec?.length)! > 0){
                address = "\(self.serviceAddRequest?.service_location_info?.location_name_sec ?? "") \n\(self.serviceAddRequest?.service_location_info?.location_address_sec ?? "")"
            }
            
            self.lblLocationAddress.text = address
        } else {
            self.lblLocationAddress.text = ""
        }
        //        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.serviceAddRequest?.service_name != (self.inputName.text()?.trim() ?? "")){
            isUpdate = true
        }
        
        if serviceAddRequest?.service_desc != textViewSpecificDesc.text?.trim() {
            isUpdate = true
        }

        var status:Int = self.btnPurchasebleYes.isSelected ? 1 : 0
        if(Int(self.serviceAddRequest?.purchasablity ?? "0") != status){
            isUpdate = true
        }
        
        status = self.btnCreditSystemYes.isSelected ? 1 : 0
        if(Int(self.serviceAddRequest?.credit_system ?? "0") != status){
            isUpdate = true
        }
        
        let donationServiceStatus = serviceAddRequest?.service_donation == "1"
        let selectedStatus = btnDonationServiceYes.isSelected
        if donationServiceStatus != selectedStatus {
            isUpdate = true
        }
        
        if(self.serviceAddRequest?.number_of_purchase != self.textFieldPurchaseNumber.text){
            isUpdate = true
        }
        
        for imageObj in self.arrDocumentImages{
            if(imageObj.imageData != nil){
                isUpdate = true
            }
        }

        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.serviceAddCalendarViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddCalendarViewController.nameOfClass) as ServiceAddCalendarViewController

        self.serviceAddCalendarViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddCalendarViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddCalendarViewController!, animated: true)
        
//        let controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddAdditionalCostViewController.nameOfClass) as ServiceAddAdditionalCostViewController
//
//        controller.serviceAddRequest = self.serviceAddRequest
//
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    fileprivate func isVideoInList() -> Bool{
        
        if(self.arrDocumentImages.count > 0){
            var isFound = false
            for imageData in arrDocumentImages {
                
                
                if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                    
                    if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                        isFound = true
                    }
                } else {
                    if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                        isFound = true
                    }
                }
            }
            
            return isFound
        }
        
        return false
    }
    
}

extension ServiceAddOverviewViewController: UITextFieldDelegate{
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        let numCheckStatus: Bool = false
        var lenCheckStatus: Bool = false
        let isNumCheckReq = false
        
        // check backspace in input character
        if (isBackSpace == -92) {
            return true
        }
        
        // check new length of string after adding new character
        let newLength = text.count + string.count - range.length
        if newLength > HelperConstant.LIMIT_PURCHASEBILITY{
            return false
        }
        
        lenCheckStatus = true
        
        if isNumCheckReq {
            return numCheckStatus && lenCheckStatus
        } else if lenCheckStatus {
            return lenCheckStatus
        }
    }
}

extension ServiceAddOverviewViewController{
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func pickedImage(fromImagePicker: UIImage, imageData: Data, mediaType:String, imageName:String, fileUrl: String?) {
        
        let imageDataTemp = UploadImageData()
        
        let theExt = (imageName as NSString).pathExtension
        
        if(theExt.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpeg") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame){
            imageDataTemp.imageType = "1"
        } else {
            imageDataTemp.imageType = "2"
        }
        
        if(serviceAddRequest?.service_id != nil){
            imageDataTemp.id = serviceAddRequest?.service_id ?? ""
        } else if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0){
            let busi_id = UserDefault.getSID() ?? ""
            imageDataTemp.id = busi_id
        }
        
        imageDataTemp.imageData = imageData
        imageDataTemp.imageName = "\((self.arrDocumentImages.count + 1))\(imageName)"
        imageDataTemp.imageKeyName = "busi_img"
        imageDataTemp.filePath = fileUrl
        
        imageDataTemp.imageType = mediaType
        imageDataTemp.imageThumbnailData = fromImagePicker.pngData()
        
        // Check of video size
        if(imageDataTemp.imageType == "2"){
            if(Int(Helper.sharedInstance.checkVideoSize(reqData: imageData)) < HelperConstant.LIMIT_VIDEO_SIZE){
                
                self.imgProfile.image = fromImagePicker
                self.imgPlayBtn.isHidden = false
                
                var arrTempDocumentImages = [UploadImageData]()
                arrTempDocumentImages.append(imageDataTemp)
                for arrDocument in self.arrDocumentImages{
                    arrTempDocumentImages.append(arrDocument)
                }
                
                self.arrDocumentImages = arrTempDocumentImages
                
                //                self.documentImgCollectionView.reloadData()
            } else {
                self.showBannerAlertWith(message: LocalizationKeys.error_file_size.getLocalized(), alert: .error)
            }
        } else {
            
            // Display image on main view
            self.imgProfile.image = fromImagePicker
            self.imgPlayBtn.isHidden = true
            
            self.arrDocumentImages.append(imageDataTemp)
            //            self.documentImgCollectionView.reloadData()
        }
        
//        if(self.arrDocumentImages.count > 0){
        if(self.arrDocumentImages.count > 0 && self.arrDocumentImages.count < HelperConstant.maximumProfileMedia){
            self.isUpdate = true
            self.constraintDocumentCollectionHeight.constant = self.defaultDocumentCollectionHeight
            
            self.documentImgCollectionView.reloadData()
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension ServiceAddOverviewViewController{
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if arrDocumentImages.isEmpty {
            message = LocalizationKeys.error_select_service_image.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if let inputName = inputName.text()?.trim(), inputName.isEmpty {
            message = LocalizationKeys.error_select_service_name.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
//        } else if(self.btnPurchasebleYes.isSelected){ // if yes
//            if(self.textFieldPurchaseNumber.text != nil && self.textFieldPurchaseNumber.text?.length == 0){
//                message = LocalizationKeys.error_select_purchasebility_count.getLocalized()
//                self.showBannerAlertWith(message: message, alert: .error)
//                return false
//            }
        } else if textViewSpecificDesc.text.isEmpty {
                message = LocalizationKeys.error_specific_desc.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
        } /*else if targetGoalId.isEmpty {
            message = LocalizationKeys.error_select_target_goal.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } */
        
        if(self.serviceAddRequest == nil){
            self.serviceAddRequest = ServiceAddRequest()
        }
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        self.serviceAddRequest?.service_name = self.inputName.text()?.trim() ?? ""
        
        if serviceAddRequest?.service_specific == nil {
            serviceAddRequest?.service_specific = ServiceSpecific()
        }
        self.serviceAddRequest?.service_desc = textViewSpecificDesc.text

        self.serviceAddRequest?.purchasablity = self.btnPurchasebleYes.isSelected  ? "1" : "0"
        self.serviceAddRequest?.credit_system = self.btnCreditSystemYes.isSelected ? "1" : "0"
        serviceAddRequest?.service_donation = btnDonationServiceYes.isSelected ? "1" : "0"
        self.serviceAddRequest?.number_of_purchase = self.textFieldPurchaseNumber.text ?? ""
        /*
        if selectedId.isEmpty {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_target_goal.getLocalized(), buttonTitle: nil, controller: nil)
            
            return false
        }
        */
        
        let targetGoalIds = targetGoal?.compactMap { $0.selectionStaus ? $0.listing_id : nil }
        let targetGoalTitles = targetGoal?.compactMap { $0.selectionStaus ? $0.listing_title : nil }
        serviceAddRequest?.service_target_goal = targetGoalIds
        serviceAddRequest?.service_target_goal_title = targetGoalTitles
        
        self.serviceAddRequest?.service_location = self.selectedLocationIndex
        return true
    }
}

//MARK:- Button Action method implementation
extension ServiceAddOverviewViewController{
    
    @IBAction func methodPurchasabilityAction(_ sender: UIButton) {
        btnPurchasebleYes.isSelected = false
        btnPurchasebleNo.isSelected = false
        isUpdate = true
        sender.isSelected = true
        
        purchasableCountContainerHeight.priority = btnPurchasebleYes == sender ? .defaultLow : .defaultHigh
    }
    
    @IBAction func methodDonationServiceAction(_ sender: UIButton) {
        btnDonationServiceYes.isSelected = false
        btnDonationServiceNo.isSelected = false
        isUpdate = true
        sender.isSelected = true
    }

    @IBAction func methodCreditSystemAction(_ sender: UIButton) {
        btnCreditSystemYes.isSelected = false
        btnCreditSystemNo.isSelected = false
        isUpdate = true
        sender.isSelected = true
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    /**
     *  Image picker button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnImagePickerAction(_ sender : UIButton){
        self.displayPhotoSelectionOption(withCircularAllow:false)
    }
}

/*******************************************/
//MARK: - UICollectionViewDelegate
/*******************************************/
extension ServiceAddOverviewViewController : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(collectionView == self.documentImgCollectionView){
//            if arrDocumentImages.count < 5{ // For 2 images
//                return arrDocumentImages.count + 1
//            }else{
                return arrDocumentImages.count
//            }
        } else if(collectionView == self.targetGoalCollectionView){
            
            if(self.targetGoal != nil && (self.targetGoal?.count)! > 0){
                return self.targetGoal?.count ?? 0
            }
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //MARK:- Store Images views
        if(collectionView == self.documentImgCollectionView){
            
            //MARK:- Store Images views
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath as IndexPath) as! SerivceImageCell
            
            let imageData = arrDocumentImages[indexPath.row]
            
            cell.delegate = self
            cell.cellIndex = indexPath.row
            cell.btnDelete.isHidden = false
            cell.btnDelete.tag = indexPath.row
            
            if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                
                if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                    cell.imgPlay.isHidden = false
                    cell.imgService.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                } else {
                    cell.imgPlay.isHidden = true
                    cell.imgService.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                }
            } else {
                if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                    cell.imgPlay.isHidden = false
                    cell.imgService.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                } else {
                    cell.imgPlay.isHidden = true
                    cell.imgService.image = UIImage(data: imageData.imageData ?? Data())
                }
            }
            
            return cell
            
        } else if(collectionView == self.targetGoalCollectionView){
            let cell = self.targetGoalCollectionView.dequeueReusableCell(withReuseIdentifier: CollectionCheckboxListCVCell.nameOfClass, for: indexPath as IndexPath) as! CollectionCheckboxListCVCell
            
            var listingData:ListingDataDetail?
            if(self.targetGoal != nil && (self.targetGoal?.count)! > indexPath.section){
                listingData = self.targetGoal![indexPath.row]
            }
            
            if(listingData != nil){
                cell.delegate = self
                cell.cellIndex = indexPath
                cell.btnCheckBox.setTitle(listingData?.listing_title ?? "", for: UIControl.State.normal)
                cell.btnCheckBox.isSelected = listingData?.selectionStaus ?? false
                
                if(!self.isUpdate && self.serviceAddRequest?.service_target_goal != nil && (self.serviceAddRequest?.service_target_goal?.count)! > 0){
                    if(self.serviceAddRequest?.service_target_goal?.contains(listingData?.listing_id ?? "") ?? false){
                        cell.btnCheckBox.isSelected = true
                        listingData?.selectionStaus = true
                    }
                }
                
                cell.backgroundColor = .clear
            }
            
            return cell
        }
        
        return UICollectionViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(collectionView == self.documentImgCollectionView){
            return CGSize(width:100, height: 80)
        } else if(collectionView == self.targetGoalCollectionView){
             return CGSize(width: (collectionView.frame.size.width ) / 2, height: 45 )
        }
        
        return CGSize(width: 0, height: 0)
    }
}

extension ServiceAddOverviewViewController:CollectionCheckboxListCVCellDelegate{
    func selectListItem(cellIndex: IndexPath?, status:Bool, refCollection: UICollectionView?){
        if let indexPath = cellIndex, let item = targetGoal?[indexPath.item] {
            item.selectionStaus = status
            isUpdate = true
        }
    }
}


extension ServiceAddOverviewViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        //TODO: Update image as per selection
        
        if !arrDocumentImages.isEmpty {
            
            let imageData = self.arrDocumentImages[cellIndex]
            
            if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                
                if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                    self.imgPlayBtn.isHidden = false
                    self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                } else {
                    self.imgPlayBtn.isHidden = true
                    self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                }
            } else {
                if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                    self.imgPlayBtn.isHidden = false
                    self.imgProfile.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                } else {
                    self.imgPlayBtn.isHidden = true
                    self.imgProfile.image = UIImage(data: imageData.imageData ?? Data())
                }
            }
        }
        
    }
}


//MARK:- Button Actions
//MARK:-
extension ServiceAddOverviewViewController{
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnAddImg(_ sender: UIButton) {
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            
            if(self.arrDocumentImages.count < HelperConstant.maximumProfileMedia){
                
                if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                    //already authorized
                    debugPrint("already authorized")
                    
                    OperationQueue.main.addOperation() {
                        if(!self.isVideoInList()){
                            self.displayPhotoSelectionOption(withCircularAllow:false, isSelectVideo: true)
                        } else {
                            self.displayPhotoSelectionOption(withCircularAllow: false)
                        }
                    }
                    
                } else {
                    AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                        if granted {
                            //access allowed
                            debugPrint("access allowed")
                            
                            OperationQueue.main.addOperation() {
                                //                            if(self.arrDocumentImages.count == 0){
                                if(!self.isVideoInList()){
                                    self.displayPhotoSelectionOption(withCircularAllow:false, isSelectVideo: true)
                                } else {
                                    self.displayPhotoSelectionOption(withCircularAllow: false)
                                }
                            }
                            
                        } else {
                            //access denied
                            debugPrint("Access denied")
                            OperationQueue.main.addOperation() {
                                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_this_app_needs_access_to_your_camera_and_gallery.getLocalized(), buttonTitle: nil, controller: nil)
                            }
                        }
                    })
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_video_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnDeleteStoreImg(_ sender: UIButton) {
//        self.arrDocumentImages.remove(at: sender.tag)
//
//        if self.arrDocumentImages.count == 0{
//
//        }
//        self.documentImgCollectionView.reloadData()
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            
//            if(self.arrDocumentImages.count > 1){
                let imageData = arrDocumentImages[sender.tag]
                
                if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                    
                    //            self.lblUploadImageVideo.isHidden =
                    
                    self.selectedDocumentImages = imageData
                    // TODO: approval alert
                    self.logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
                    self.logoutViewController?.delegate = self
                    self.logoutViewController?.scrTitle = ""
                    self.logoutViewController?.scrDesc = LocalizationKeys.error_remove_media.getLocalized()
                    self.selectedIndex = sender.tag
                    self.logoutViewController?.modalPresentationStyle = .overFullScreen
                    self.present(logoutViewController!, animated: true, completion: nil)
                } else {
                    
                    if(self.arrDocumentImages.count > 0){
                        self.arrDocumentImages.remove(at: sender.tag)
                    }
                    
                    if self.arrDocumentImages.count == 0{
                        self.constraintDocumentCollectionHeight.constant = 0.00
                        self.imgPlayBtn.isHidden = true
                        self.imgProfile.image = nil
                    } else {
                        
                        // Update profile image
                        //////////////////////////////////////////////////////////////////////
                        let imageData = self.arrDocumentImages[0]
                        
                        if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                            
                            if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                                self.imgPlayBtn.isHidden = false
                                self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                            } else {
                                self.imgPlayBtn.isHidden = true
                                self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                            }
                            //                cell.btnDelete.isHidden = true
                        } else {
                            if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                                self.imgPlayBtn.isHidden = false
                                self.imgProfile.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                            } else {
                                self.imgPlayBtn.isHidden = true
                                self.imgProfile.image = UIImage(data: imageData.imageData ?? Data())
                            }
                        }
                        //////////////////////////////////////////////////////////////////////
                        
//                        func selectListItem(cellIndex: IndexPath?, status: Bool, refCollection: UICollectionView?) {
//
//                        }
                        
                        
                        self.documentImgCollectionView.reloadData()
                    }
                }
//            } else {
//                //                error_busi_minimum_media
//                self.showBannerAlertWith(message: LocalizationKeys.error_busi_minimum_media.getLocalized(), alert: .error)
//            }
        }
        
    }
}


extension ServiceAddOverviewViewController: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
        
        if(self.selectedDocumentImages != nil){
            let deleteMediaRequest = DeleteMediaRequest(media_id: self.selectedDocumentImages?.id ?? "")
            self.presenter.deleteMediaData(deleteMediaRequest: deleteMediaRequest, callback: {
                (status, response, message) in
                
                if(status){
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    if(self.arrDocumentImages.count > 0){
                        self.arrDocumentImages.remove(at: self.selectedIndex)
                    }
                    
                    if self.arrDocumentImages.count == 0{
                        self.constraintDocumentCollectionHeight.constant = 0.00
                        self.imgProfile.image = nil
                    }
                    
                    self.documentImgCollectionView.reloadData()
                    
                } else {
                    
                }
            })
        }
    }
}

/*******************************************/
//MARK: - UICollectionViewDelegate
/*******************************************/

///**
// * BusinessLocationAddMoreCellDelegate specify method signature.
// *
// *  @Developed By: Team Consagous [CNSGSIN054]
// */
protocol CollectionCheckboxListCVCellDelegate {
    func selectListItem(cellIndex: IndexPath?, status:Bool, refCollection: UICollectionView?)
}

class CollectionCheckboxListCVCell : UICollectionViewCell{
    
    //MARK:-IBOutlet
    @IBOutlet weak var btnCheckBox: UIButton!
    
    var cellIndex: IndexPath?
    
    var delegate: CollectionCheckboxListCVCellDelegate?
    var refCollection: UICollectionView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateStatus(status:Bool){
        btnCheckBox.isSelected = status
    }
    
    @IBAction func methodCheckAction(_ sender: UIButton) {
        
        btnCheckBox.isSelected = !btnCheckBox.isSelected
        
        if(delegate != nil){
            delegate?.selectListItem(cellIndex: cellIndex, status: btnCheckBox.isSelected, refCollection: refCollection)
        }
    }
    
}

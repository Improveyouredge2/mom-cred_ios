//
//  ServiceAddFieldServiceViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 25/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 * ServiceAddFieldServiceViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddFieldServiceViewController : LMBaseViewController{
    
    fileprivate let formNumber = 10
    
    @IBOutlet weak var businessInformationFieldCategoryView: BusinessInformationFieldCategoryView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint! 
    var isEditingService: Bool = false

    var serviceAddRequest: ServiceAddRequest?
    var serviceType : [ListingDataDetail]?
    
    fileprivate var presenter = ServiceAddFieldServicePresenter()
    fileprivate var externalLinkDetailList:[ExternalLinkDetail] = []
    fileprivate var addFieldServiceList:[BusinessFieldServiceCategoryInfo] = []
    fileprivate var serviceAddAdditionalCostViewController:ServiceAddAdditionalCostViewController?
    
    var isUpdate = false
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: For testing
        
        presenter.connectView(view: self)
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.businessInformationFieldCategoryView.techniqueTablePlaceHolder = "Technique Title"
        
        // disable manual entry
        self.businessInformationFieldCategoryView.isAllowManualCat = false
        var businessLocationInfo = BusinessFieldServiceCategoryInfo()
        self.businessInformationFieldCategoryView.businessLocationInfo = businessLocationInfo
        self.businessInformationFieldCategoryView.fieldCategoryList = ServiceAddPresenter.parentListingList?.parentListingData?.fields ?? []
        self.businessInformationFieldCategoryView.delegate = self
        self.businessInformationFieldCategoryView.tag = 1
        self.businessInformationFieldCategoryView.setScreenData()
        
        businessLocationInfo = BusinessFieldServiceCategoryInfo()
        self.serviceType = ServiceAddPresenter.parentListingList?.parentListingData?.services_types
        
        self.setScreenData()
        
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serviceAddAdditionalCostViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(addFieldServiceList.count == 0){
            self.viewTableView.isHidden = true
        } else {
            self.viewTableView.isHidden = false
        }
        
        if(self.tableView.contentSize.height > 0){
            tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension ServiceAddFieldServiceViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        
        if(self.serviceAddRequest != nil){
            if(self.serviceAddRequest?.service_field != nil && (self.serviceAddRequest?.service_field?.count)! > 0){
                
                self.addFieldServiceList = self.serviceAddRequest?.service_field ?? []
                
                
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.serviceAddRequest?.service_field != nil && (self.serviceAddRequest?.service_field?.count)! > 0 && (self.serviceAddRequest?.service_field?.count)! != self.addFieldServiceList.count){
            isUpdate = true
        }
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.serviceAddAdditionalCostViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddAdditionalCostViewController.nameOfClass) as ServiceAddAdditionalCostViewController
        
        self.serviceAddAdditionalCostViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddAdditionalCostViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddAdditionalCostViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        
        if (self.addFieldServiceList.count == 0) {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_add_service_type.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        self.serviceAddRequest?.service_field = addFieldServiceList
        
        return true
    }
}

extension ServiceAddFieldServiceViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension ServiceAddFieldServiceViewController{
    
    @IBAction func methodAddFeildServiceAction(_ sender: UIButton){
        
        var isValid = true
        if((self.businessInformationFieldCategoryView.businessLocationInfo?.catFieldName?.length)! == 0 && self.businessInformationFieldCategoryView.inputNameCustomEntry.isHidden){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_primary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
            isValid = false
            return
        }
        
        if(!self.businessInformationFieldCategoryView.inputNameCustomEntry.isHidden && (self.businessInformationFieldCategoryView.inputNameCustomEntry.text()?.trim().isEmpty)!){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_field_category_name.getLocalized(), buttonTitle: nil, controller: nil)
            isValid = false
            return
        }
        
        if((self.businessInformationFieldCategoryView.businessLocationInfo?.specificFieldName?.length)! == 0 && self.businessInformationFieldCategoryView.inputNameCustomEntry.isHidden){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_secondary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
            
            isValid = false
            return
        }
        
        if(isValid && self.addFieldServiceList.count < HelperConstant.minimumBlocks){
            
            self.isUpdate = true
            
            let businessLocationInfo = BusinessFieldServiceCategoryInfo()
            businessLocationInfo.catField = self.businessInformationFieldCategoryView.businessLocationInfo?.catField ?? ""
            businessLocationInfo.catFieldName = self.businessInformationFieldCategoryView.businessLocationInfo?.catFieldName ?? ""
            
            businessLocationInfo.specificField = ""
            businessLocationInfo.specificFieldName = ""
            
            if(!self.businessInformationFieldCategoryView.inputNameCustomEntry.isHidden){
                businessLocationInfo.add_new_specific_field = self.businessInformationFieldCategoryView.inputNameCustomEntry.text() ?? ""
            } else {
                businessLocationInfo.specificField = self.businessInformationFieldCategoryView.businessLocationInfo?.specificField ?? ""
                businessLocationInfo.specificFieldName = self.businessInformationFieldCategoryView.businessLocationInfo?.specificFieldName ?? ""
            }
            
            businessLocationInfo.classification = self.businessInformationFieldCategoryView.businessLocationInfo?.classification ?? []
            businessLocationInfo.technique = self.businessInformationFieldCategoryView.businessLocationInfo?.technique ?? []
            self.addFieldServiceList.append(businessLocationInfo)
            
            self.businessInformationFieldCategoryView.businessLocationInfo?.catField = ""
            self.businessInformationFieldCategoryView.businessLocationInfo?.catFieldName = ""
            self.businessInformationFieldCategoryView.businessLocationInfo?.specificField = ""
            self.businessInformationFieldCategoryView.businessLocationInfo?.specificFieldName = ""
            self.businessInformationFieldCategoryView.businessLocationInfo?.classification = []
            self.businessInformationFieldCategoryView.businessLocationInfo?.technique = []
            self.businessInformationFieldCategoryView.dropDownCategory.text = ""
            self.businessInformationFieldCategoryView.dropDownService.text = ""
            self.businessInformationFieldCategoryView.inputNameCustomEntry.input.text = ""
            self.businessInformationFieldCategoryView.inputNameCustomEntry.isHidden = true
            self.businessInformationFieldCategoryView.tableViewServiceClassification.reloadData()
            self.businessInformationFieldCategoryView.tableViewServiceTechnique.reloadData()
            
            
            self.tableView.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension ServiceAddFieldServiceViewController:BIFieldCategoryViewDelegate{
    func updateFieldCategorySelected(optionId:String, tagIndex:Int){
        self.presenter.serverChildListingRequest(parentId: optionId, tagIndex: tagIndex)
    }
    
    func updateSpecificFeildInfo(response:[ListingDataDetail]?, tagIndex:Int){
        
        if(self.businessInformationFieldCategoryView.tag == tagIndex){
            self.businessInformationFieldCategoryView.specificCategoryList = response ?? []
            self.businessInformationFieldCategoryView.updateServiceList()
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddFieldServiceViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addFieldServiceList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BIFieldServiceCell.nameOfClass) as! BIFieldServiceCell
        
        cell.delegate = self
        
        cell.cellIndex = indexPath
        
        let objInfo = addFieldServiceList[indexPath.row]
        cell.lblFieldIndex.text = "Field \(indexPath.row + 1)"
        cell.lblCatName.text = objInfo.catFieldName ?? ""
        
        if(objInfo.add_new_specific_field != nil && (objInfo.add_new_specific_field?.length)! > 0){
            cell.lblSubCatName.text = objInfo.add_new_specific_field ?? ""
        } else {
            cell.lblSubCatName.text = objInfo.specificFieldName ?? ""
        }
        
        
        if let classifications = objInfo.classification, !classifications.isEmpty {
            cell.lblClassification.text = "• \(classifications.joined(separator: "\n• "))"
        } else {
            cell.lblClassification.text = ""
        }
        
        if let techniques = objInfo.technique, !techniques.isEmpty {
            cell.lblTechnique.text = "• \(techniques.joined(separator: "\n• "))"
        } else {
            cell.lblTechnique.text = ""
        }
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension ServiceAddFieldServiceViewController:BIFieldServiceCellDelegate{
    func removeFieldServiceInfo(cellIndex:IndexPath?){
        
        if(self.addFieldServiceList.count > 0){
            self.addFieldServiceList.remove(at: cellIndex?.row ?? 0)
            self.tableView.reloadData()
            
            if(addFieldServiceList.count == 0){
                self.viewTableView.isHidden = true
            }
        }
    }
}

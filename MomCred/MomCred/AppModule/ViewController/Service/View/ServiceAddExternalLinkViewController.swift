//
//  ServiceAddExternalLinkViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 03/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class ExternalLinkDetail{
    var links:[String] = []
    var desc:String = ""
}

/**
 * ServiceAddExternalLinkViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddExternalLinkViewController : LMBaseViewController{
    
    fileprivate let formNumber = 16
    
    // Event
//    @IBOutlet weak var tableViewSpecificTarget: UITableView!
    @IBOutlet weak var tableViewExternalLink: UITableView!
    
//    @IBOutlet weak var constraintstableViewSpecificTargetHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintstableViewExternalLinkHeight: NSLayoutConstraint!
    
    @IBOutlet weak var inputLink: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var viewExternalLinkTableView: UIView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var specificTargetList:[String] = []
    fileprivate var externalLinkDetailList:[ServiceExternalLinkData] = []
    fileprivate var isUpdate = false
    fileprivate var serviceAddAdditionalInformationDocumentViewController:ServiceAddAdditionalInformationDocumentViewController?
    fileprivate var presenter = ServiceAddExternalLinkPresenter()
    
    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
//        self.tableViewSpecificTarget.backgroundColor = UIColor.clear
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serviceAddAdditionalInformationDocumentViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
//        if(self.tableViewSpecificTarget.contentSize.height > 0){
//            if((self.specificTargetList.count) == 0){
//                self.constraintstableViewSpecificTargetHeight?.constant = AddMoreCell.inputViewCellSize
//            } else {
//                self.constraintstableViewSpecificTargetHeight?.constant = self.tableViewSpecificTarget.contentSize.height
//            }
//
//            self.updateTableViewHeight(tableView: tableViewSpecificTarget)
//        }
        
        if(self.externalLinkDetailList.count == 0){
            self.viewExternalLinkTableView.isHidden = true
        } else {
            self.viewExternalLinkTableView.isHidden = false
        }
        
        if(self.tableViewExternalLink.contentSize.height > 0){
            tableViewExternalLink.scrollToBottom()
            self.constraintstableViewExternalLinkHeight?.constant = self.tableViewExternalLink.contentSize.height
            
            self.updateTableViewHeight(tableView: self.tableViewExternalLink)
        }
        
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
//        if(tableViewSpecificTarget == tableView){
//
//            UIView.animate(withDuration: 0, animations: {
//                self.tableViewSpecificTarget.layoutIfNeeded()
//            }) { (complete) in
//                var heightOfTableView: CGFloat = 0.0
//                // Get visible cells and sum up their heights
//                let cells = self.tableViewSpecificTarget.visibleCells
//                for cell in cells {
//                    heightOfTableView += cell.frame.height
//                }
//                // Edit heightOfTableViewConstraint's constant to update height of table view
//                self.constraintstableViewSpecificTargetHeight.constant = heightOfTableView
//            }
//        } else
        if(tableViewExternalLink == tableView){
        
            UIView.animate(withDuration: 0, animations: {
                self.tableViewExternalLink.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewExternalLink.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewExternalLinkHeight.constant = heightOfTableView
            }
        }
    }
}


extension ServiceAddExternalLinkViewController{
    fileprivate func setScreenData(){
        
        self.inputLink.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Link ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )

        
        if(self.serviceAddRequest?.service_listing != nil){
            
            self.externalLinkDetailList = self.serviceAddRequest?.service_external_link?.serviceExternalLinkList ?? []
            
            self.tableViewExternalLink.reloadData()
        }
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if (self.externalLinkDetailList.count > 0){
            let serviceListing = ServiceExternalLink()
            serviceListing.serviceExternalLinkList = self.externalLinkDetailList
            
            self.serviceAddRequest?.service_external_link = serviceListing
        } else {
            self.serviceAddRequest?.service_external_link = nil
        }
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.serviceAddAdditionalInformationDocumentViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddAdditionalInformationDocumentViewController.nameOfClass) as ServiceAddAdditionalInformationDocumentViewController
        
        self.serviceAddAdditionalInformationDocumentViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddAdditionalInformationDocumentViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddAdditionalInformationDocumentViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension ServiceAddExternalLinkViewController{
    @IBAction func methodAddExternalLinkAction(_ sender: UIButton){
        
//        if (self.inputLink.text()?.trim().isEmpty)! {
//
//        } else if(self.inputLink.text()?.validateUrl()){
//
//        }
        
        if(!((self.inputLink.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: self.inputLink.text() ?? ""))){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_invalid_url.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
//        && Helper.sharedInstance.verifyUrl(urlString: primaryWebLink.inputName.text() ?? "")
        
        if((self.inputLink.text()?.length)! > 0){
            self.isUpdate = true
            let externalLinkDetailInfo = ServiceExternalLinkData()
            externalLinkDetailInfo.link = self.inputLink.text()
            externalLinkDetailInfo.desc = self.textViewDesc.text
            
            if(self.externalLinkDetailList.count < HelperConstant.minimumBlocks){
                externalLinkDetailList.append(externalLinkDetailInfo)
                
                self.specificTargetList = []
                self.inputLink.input.text = ""
                self.textViewDesc.text = ""
                
                if(self.externalLinkDetailList.count == 1){
                    self.constraintstableViewExternalLinkHeight?.constant = 10
                }
                
                //            self.tableViewSpecificTarget.reloadData()
                self.tableViewExternalLink.reloadData()
                
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddExternalLinkViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tableViewExternalLink){
            return externalLinkDetailList.count
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
            
            if(externalLinkDetailList.count > indexPath.row){
                let objInfo = externalLinkDetailList[indexPath.row]
                
                cell.cellIndex = indexPath
                cell.delegate = self
                cell.lblLink.text = objInfo.link
                cell.lblDesc.text = objInfo.desc
            }
            return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension ServiceAddExternalLinkViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        self.isUpdate = true
//        if(refTableView == self.tableViewSpecificTarget){
//            self.specificTargetList.append(text)
//            self.tableViewSpecificTarget.reloadData()
//        }
    }
}

extension ServiceAddExternalLinkViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        self.isUpdate = true
//        if(refTableView == self.tableViewSpecificTarget){
//            self.specificTargetList.remove(at: cellIndex?.row ?? 0)
//            
//            self.tableViewSpecificTarget.reloadData()
//        }
    }
}

extension ServiceAddExternalLinkViewController:ExternalLinkTableViewCellDelegate{
    
    func removeExternalLinkInfo(cellIndex:IndexPath?){
        
        self.isUpdate = true
        if(self.externalLinkDetailList.count > 0){
            self.externalLinkDetailList.remove(at: cellIndex?.row ?? 0)
            self.tableViewExternalLink.reloadData()
            
            if(externalLinkDetailList.count == 0){
                self.viewExternalLinkTableView.isHidden = true
            }
        }
    }
}

/**
 * BusinessLocationAddMoreCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol ExternalLinkTableViewCellDelegate {
    func removeExternalLinkInfo(cellIndex:IndexPath?)
}

class ExternalLinkTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var lblLink: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnRemove : UIButton!
    
    var cellIndex:IndexPath?
    var delegate:ExternalLinkTableViewCellDelegate?
    
    @IBAction func methodCloseAction(_ sender: UIButton) {
        if(delegate != nil){
            delegate?.removeExternalLinkInfo(cellIndex: cellIndex)
        }
    }
}

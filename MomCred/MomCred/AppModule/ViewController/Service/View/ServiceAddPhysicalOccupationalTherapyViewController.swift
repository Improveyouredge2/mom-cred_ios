//
//  ServiceAddPhysicalOccupationalTherapyViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 28/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

/**
 * ServiceAddPhysicalOccupationalTherapyViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddPhysicalOccupationalTherapyViewController : LMBaseViewController{
   
    fileprivate let formNumber = 5
    
    @IBOutlet weak var btnTherapyYes: UIButton!
    @IBOutlet weak var btnTherapyNo: UIButton!
    @IBOutlet weak var viewTherapyDetail: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    
//    @IBOutlet weak var dropDownTherapyType: DropDown!
//    textFieldLocationServiceOther
    @IBOutlet weak var dropDownPaymentType: DropDown!
    @IBOutlet weak var textFieldTherapyFieldCover: RYFloatingInput!
    
    @IBOutlet weak var textViewInsurancePlans: KMPlaceholderTextView!
    
    @IBOutlet weak var viewInsurancePlans: UIView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var serviceAddInstructionalFrontOfficeViewController:ServiceAddInstructionalFrontOfficeViewController?
    fileprivate var businessNewTypeView:BusinessAddNewTypeViewController?
    fileprivate var serviceTherapy = ServiceTherapy()
    fileprivate var therapyType : [ListingDataDetail]?
    fileprivate var isUpdate = false
    fileprivate var presenter = ServiceAddPhysicalOccupationalTherapyPresenter()
    fileprivate var detailData = DetailData()
    
    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter.connectView(view: self)
        
        self.tableView.backgroundColor = .clear
        self.therapyType = ServiceAddPresenter.parentListingList?.parentListingData?.therapy ?? []
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()

        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.tableView.contentSize.height > 0){
            tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
        }
    }
}

extension ServiceAddPhysicalOccupationalTherapyViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        self.viewTherapyDetail.isHidden = true
        self.viewInsurancePlans.isHidden = true
        
        self.textFieldTherapyFieldCover.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Therapy fields covered")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        // Location Service
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        self.dropDownPaymentType.optionArray = ["Out of pocket", "Prescription based"]
        self.dropDownPaymentType.selectedIndex = 0
        
        // The the Closure returns Selected Index and String
        self.dropDownPaymentType.didSelect{ [weak self] (selectedText, index, id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            
            //            self.selectedSocialMedia = selectedText
            self?.detailData.id = "\(id)"
            self?.detailData.title = selectedText
            
            if let optionCount = self?.dropDownPaymentType.optionArray.count {
                self?.viewInsurancePlans.isHidden = (index != optionCount - 1)
            }
        }
        ////////////////////////////////////////////////////////////
        if(self.serviceAddRequest?.service_therapy != nil){
            self.methodTherapyAction(btnTherapyYes)
            
            let service_therapy = self.serviceAddRequest?.service_therapy
            
            self.textFieldTherapyFieldCover.input.text = service_therapy?.therapyFieldCover ?? ""
            
            if(service_therapy?.paymentType != nil){
                self.detailData = service_therapy?.paymentType ?? DetailData()
                if let paymentTitle = service_therapy?.paymentType?.title {
                    dropDownPaymentType.text = paymentTitle
                    if let index = dropDownPaymentType.optionArray.firstIndex(of: paymentTitle) {
                        dropDownPaymentType.selectedIndex = index
                        viewInsurancePlans.isHidden = (index != dropDownPaymentType.optionArray.count - 1)
                    }
                }
            }
            
            if(service_therapy?.acceptedInsuancePlan != nil && (service_therapy?.acceptedInsuancePlan?.length)! > 0){
                self.textViewInsurancePlans.text = service_therapy?.acceptedInsuancePlan ?? ""
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if(self.btnTherapyYes.isSelected){
            var selectedId:[DetailData] = []
            
            if(self.therapyType != nil && (self.therapyType?.count)! > 0){
                for service in self.therapyType!{
                        if(service.selectionStaus){
                            let info = DetailData()
                            info.id = service.listing_id ?? ""
                            info.title = service.listing_title ?? ""
                            
                            selectedId.append(info)
                    }
                }
            }
            
            if(selectedId.count == 0){
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_therapy_type.getLocalized(), buttonTitle: nil, controller: nil)
                
                return false
            }
            
            if let isTherapyCoverEmpty = self.textFieldTherapyFieldCover.text()?.trim().isEmpty, isTherapyCoverEmpty {
                message = LocalizationKeys.error_therapy_field_cover.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            //
            if(self.detailData.id != nil && self.detailData.id?.length == 0){
                message = LocalizationKeys.error_therapy_payment_type.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            if(!self.viewInsurancePlans.isHidden){
                if self.textViewInsurancePlans.text.isEmpty {
                    message = LocalizationKeys.error_therapy_insurance_desc.getLocalized()
                    self.showBannerAlertWith(message: message, alert: .error)
                    return false
                }
            }
            
            if(self.serviceAddRequest == nil){
                self.serviceAddRequest = ServiceAddRequest()
            }
            
            self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
            
            if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
                self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
            } else if(self.serviceAddRequest?.service_id == nil){
                self.serviceAddRequest?.service_id = ""
            }
            
            let service_therapy = ServiceTherapy()
            
            service_therapy.therapyType = selectedId
            service_therapy.therapyFieldCover = self.textFieldTherapyFieldCover.text() ?? ""
            service_therapy.paymentType = detailData
            service_therapy.acceptedInsuancePlan = self.textViewInsurancePlans.text ?? ""
            
            self.serviceAddRequest?.service_therapy = service_therapy
            
        }
        return true
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        if(self.serviceAddRequest?.service_therapy?.therapyFieldCover == nil || self.serviceAddRequest?.service_therapy?.therapyFieldCover != self.textFieldTherapyFieldCover.text()){
            isUpdate = true
        }
        
        if(self.serviceAddRequest?.service_therapy?.paymentType != nil && self.serviceAddRequest?.service_therapy?.paymentType?.id != self.detailData.id){
            isUpdate = true
        }
        
        if(self.serviceAddRequest?.service_therapy?.acceptedInsuancePlan != self.textViewInsurancePlans.text){
            isUpdate = true
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.serviceAddInstructionalFrontOfficeViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddInstructionalFrontOfficeViewController.nameOfClass) as ServiceAddInstructionalFrontOfficeViewController
        
        self.serviceAddInstructionalFrontOfficeViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddInstructionalFrontOfficeViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddInstructionalFrontOfficeViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension ServiceAddPhysicalOccupationalTherapyViewController{
    fileprivate func openNewTherapyTypeAlert(){
        businessNewTypeView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BusinessAddNewTypeViewController.nameOfClass) as BusinessAddNewTypeViewController
        
        businessNewTypeView?.titleText = "New Therapy Type"
        businessNewTypeView?.placeHolderText = "New Therapy"
        
        businessNewTypeView?.delegate = self
        businessNewTypeView?.modalPresentationStyle = .overFullScreen
        
        
        //        self.present(businessNewTypeView!, animated: true, completion: nil)
        
        HelperConstant.appDelegate.navigationController?.present(businessNewTypeView!, animated: true, completion: nil)
    }
}

//MARK:- Button Action method implementation
extension ServiceAddPhysicalOccupationalTherapyViewController : BusinessNewTypeViewDelegate {
    
    func cancelViewScr(){
        if(businessNewTypeView != nil){
            businessNewTypeView = nil
        }
    }
    
    func addNewType(newType: String){
        print("New Type: \(newType)")
    }
}

extension ServiceAddPhysicalOccupationalTherapyViewController{
    
    @IBAction func methodTherapyAction(_ sender: UIButton) {
        
        self.btnTherapyYes.isSelected = false
        self.btnTherapyNo.isSelected = false
        
        if(self.btnTherapyYes == sender){
            self.btnTherapyYes.isSelected = true
            self.viewTherapyDetail.isHidden = false
        } else if(self.btnTherapyNo == sender){
            self.btnTherapyNo.isSelected = true
            self.viewTherapyDetail.isHidden = true
        }
    }
    
    //MARK:- Button Action method implementation
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension ServiceAddPhysicalOccupationalTherapyViewController: UITableViewDelegate, UITableViewDataSource{
    // MARK: - Tableview Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.therapyType?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        //        let cell = tableView.dequeueReusableCell(withIdentifier: SingleSelectionCell.nameOfClass) as! SingleSelectionCell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessContactAddPersonnelCell.nameOfClass) as! BusinessContactAddPersonnelCell
        
        var listingData:ListingDataDetail?
        if(self.therapyType != nil && (self.therapyType?.count)! > indexPath.row){
            listingData = self.therapyType![indexPath.row]
        }
        
        if(listingData != nil){
            cell.delegate = self
            cell.cellIndex = indexPath
            cell.lblTitle.text = listingData?.listing_title
            cell.btnCheck.isSelected = listingData?.selectionStaus ?? false
            
            if(!self.isUpdate && serviceAddRequest?.service_therapy != nil && self.serviceAddRequest?.service_therapy?.therapyType != nil && (self.serviceAddRequest?.service_therapy?.therapyType?.count)! > 0){
                if(self.serviceAddRequest?.service_therapy?.therapyType?.map{$0.id}.contains(listingData?.listing_id ?? "") ?? false){
                    cell.btnCheck.isSelected = true
                    listingData?.selectionStaus = true
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ServiceAddPhysicalOccupationalTherapyViewController: BusinessContactAddPersonnelCellDelegate{
    func selectListItem(cellIndex: IndexPath?, status:Bool, tableView:UITableView?){
        
        if(self.therapyType != nil && (self.therapyType?.count)! > cellIndex?.row ?? 0){
            self.therapyType![cellIndex?.row ?? 0].selectionStaus = status
            
            self.isUpdate = true
        }
    }
}

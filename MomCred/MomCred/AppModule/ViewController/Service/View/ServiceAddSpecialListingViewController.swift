//
//  ServiceAddSpecialListingViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 27/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

/**
 * ServiceAddSpecialListingViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddSpecialListingViewController : LMBaseViewController{
    
    fileprivate let formNumber = 4
    
    @IBOutlet weak var textFieldCertificateName: RYFloatingInput! {
        didSet {
            textFieldCertificateName.input.autocapitalizationType = .words
        }
    }
    
    @IBOutlet weak var btnFreeServiceYes: UIButton!
    @IBOutlet weak var btnFreeServiceNo: UIButton!
    @IBOutlet weak var textFieldFreeService: RYFloatingInput!
    
    @IBOutlet weak var btnTrailServiceYes: UIButton!
    @IBOutlet weak var btnTrailServiceNo: UIButton!
    @IBOutlet weak var textFieldTrailFreeService: RYFloatingInput!
    
    @IBOutlet weak var btnTravelServiceYes: UIButton!
    @IBOutlet weak var btnTravelServiceNo: UIButton!
    @IBOutlet weak var viewTravelServiceDetail: UIView!
    
    @IBOutlet weak var dropDownTravelServiceType: DropDown!
    @IBOutlet weak var dropDownLocationService: DropDown!
    @IBOutlet weak var textFieldLocationServiceOther: RYFloatingInput!
    
    @IBOutlet weak var textViewTravelRequirement: KMPlaceholderTextView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var serviceAddPhysicalOccupationalTherapyViewController:ServiceAddPhysicalOccupationalTherapyViewController?
    fileprivate var serviceCertificate = ServiceCertificate()
    fileprivate var travelServiceTypeId = ""
    fileprivate var travelServiceTypeTitle = ""
    fileprivate var travelLocationId = ""
    fileprivate var travelLocationTitle = ""
    fileprivate var presenter = ServiceAddSpecialListingPresenter()
    fileprivate var isUpdate = false
    
    var screenName:String = ""
    
    var serviceAddRequest:ServiceAddRequest?
    
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serviceAddPhysicalOccupationalTherapyViewController = nil
    }
}

extension ServiceAddSpecialListingViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        self.textFieldFreeService.isHidden = true
        self.textFieldTrailFreeService.isHidden = true
        self.textFieldLocationServiceOther.isHidden = true
        self.viewTravelServiceDetail.isHidden = true
        
        self.textFieldCertificateName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Certification name")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        self.textFieldFreeService.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("How many times can service be accessed for free? ")
                .maxLength(HelperConstant.LIMIT_REPEAT_TIME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        self.textFieldFreeService.input.keyboardType = UIKeyboardType.numberPad
       
        self.textFieldTrailFreeService.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("How many times can trial service be accessed?")
                .maxLength(HelperConstant.LIMIT_REPEAT_TIME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        self.textFieldTrailFreeService.input.keyboardType = UIKeyboardType.numberPad
        
        self.textFieldLocationServiceOther.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Other")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        // Travel Service
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        self.dropDownTravelServiceType.optionArray = ["In-home", "3rd party"]
        //Its Id Values and its optional
        self.dropDownTravelServiceType.optionIds = [1,2]
        
        self.dropDownTravelServiceType.selectedIndex = 0
        // The the Closure returns Selected Index and String
        self.dropDownTravelServiceType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            
//            self.selectedSocialMedia = selectedText
            
//            if(index == (self.dropDownTravelServiceType.optionIds?.count)! - 1){
//                self.openNewSocialMediaTypeAlert()
//            }
            
            self.travelServiceTypeId = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Location Service
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        self.dropDownLocationService.optionArray = ["Enter New Location Service..."]
        //Its Id Values and its optional
        self.dropDownLocationService.optionIds = [1001]
        
        self.dropDownLocationService.selectedIndex = 0
        //        selectedMonth = dropDown.optionArray[0]
        //        dropDownStdExpectional.text = selectedMonth
        // The the Closure returns Selected Index and String
        self.dropDownLocationService.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            
//            self.selectedSocialMedia = selectedText
            
            if(id == 1001){
//                self.openNewSocialMediaTypeAlert()
                self.textFieldLocationServiceOther.isHidden = false
                self.travelLocationId = ""
                self.travelLocationTitle = ""
            } else {
                self.textFieldLocationServiceOther.isHidden = true
                self.travelLocationId = "\(id)"
                self.travelLocationTitle = selectedText
            }
        }
        ////////////////////////////////////////////////////////////
        
        if(self.serviceAddRequest?.service_certificate != nil){
            self.serviceCertificate = self.serviceAddRequest?.service_certificate ?? ServiceCertificate()
        }
        
        self.textFieldCertificateName.input.text = self.serviceCertificate.certificateName ?? ""
        
        if(self.serviceCertificate.freeService != nil && (self.serviceCertificate.freeService?.length)! > 0){
            
            self.methodFreeServiceAction(self.btnFreeServiceYes)
            self.textFieldFreeService.input.text = self.serviceCertificate.freeService ?? ""
        }
        
        if(self.serviceCertificate.trailService != nil && (self.serviceCertificate.trailService?.length)! > 0){
            self.methodTrailServiceAction(self.btnTrailServiceYes)
            
            self.textFieldTrailFreeService.input.text = self.serviceCertificate.trailService ?? ""
        }
        
        
        if(self.serviceCertificate.travelServiceTypeId != nil && (self.serviceCertificate.travelServiceTypeId?.length)! > 0){
            
            self.methodTravelServiceAction(self.btnTravelServiceYes)
            
            if(self.serviceCertificate.travelServiceReq != nil && (self.serviceCertificate.travelServiceReq?.length)! > 0){
                self.textViewTravelRequirement.text = self.serviceCertificate.travelServiceReq ?? ""
                
            }
            
            self.travelServiceTypeId = self.serviceCertificate.travelServiceTypeId ?? ""
            
            if(self.serviceCertificate.travelServiceTypeTitle != nil && (self.serviceCertificate.travelServiceTypeTitle?.length)! > 0){
                self.dropDownTravelServiceType.text = self.serviceCertificate.travelServiceTypeTitle ?? ""
            }
        }
        
        if(self.serviceCertificate.travelLocationId != nil && (self.serviceCertificate.travelLocationId?.length)! > 0){
            
            self.travelLocationId = self.serviceCertificate.travelLocationId ?? ""
            
            if(self.serviceCertificate.travelLocationTitle != nil && (self.serviceCertificate.travelLocationTitle?.length)! > 0){
                self.dropDownLocationService.text = self.serviceCertificate.travelLocationTitle ?? ""
            }
        }
        
        if(self.serviceCertificate.travelLocationTitle != nil && (self.serviceCertificate.travelLocationTitle?.length)! > 0){
            
            self.methodTravelServiceAction(self.btnTravelServiceYes)
            self.textFieldLocationServiceOther.isHidden = false
            self.textFieldLocationServiceOther.input.text = self.serviceCertificate.travelLocationTitle ?? ""
            
        }
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
//        if (self.textFieldCertificateName.text()?.trim().isEmpty)!{
//            message = LocalizationKeys.error_certificate_name.getLocalized()
//            self.showBannerAlertWith(message: message, alert: .error)
//            return false
//        }
        
        if(self.btnFreeServiceYes.isSelected){
            if (self.textFieldFreeService.text()?.trim().isEmpty)!{
                message = LocalizationKeys.error_free_service_name.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
        }
        
        if(self.btnTrailServiceYes.isSelected){
            if (self.textFieldTrailFreeService.text()?.trim().isEmpty)!{
                message = LocalizationKeys.error_trial_service_name.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
        }
        
        if(self.btnTravelServiceYes.isSelected){
            if travelServiceTypeId.isEmpty  {
                message = LocalizationKeys.error_travel_service_type.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            if(self.textViewTravelRequirement.text.isEmpty){
                message = LocalizationKeys.error_travel_service_req.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
            
            if !textFieldLocationServiceOther.isHidden, let isEmptyEntry = textFieldLocationServiceOther.text()?.trim().isEmpty, isEmptyEntry {
                message = LocalizationKeys.error_travel_service_location.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
        }
        
        if(self.serviceAddRequest == nil){
            self.serviceAddRequest = ServiceAddRequest()
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        self.serviceAddRequest?.service_certificate = self.serviceCertificate
        
        self.serviceCertificate.certificateName = self.textFieldCertificateName.input.text ?? ""
        
        if(self.btnFreeServiceYes.isSelected){
            self.serviceCertificate.freeService = self.textFieldFreeService.input.text ?? ""
        }
        
        if(self.btnTrailServiceYes.isSelected){
            self.serviceCertificate.trailService = self.textFieldTrailFreeService.input.text ?? ""
        }
        
        if(self.travelServiceTypeId.length > 0){
            self.serviceCertificate.travelServiceTypeId = self.travelServiceTypeId
        }
        
        if(self.dropDownTravelServiceType.text != nil && (self.dropDownTravelServiceType.text?.length)! > 0){
            self.serviceCertificate.travelServiceTypeTitle = self.dropDownTravelServiceType.text ?? ""
        }
        
        if(self.textViewTravelRequirement.text != nil && self.textViewTravelRequirement.text.length > 0){
            self.serviceCertificate.travelServiceReq = self.textViewTravelRequirement.text ?? ""
        }
        
        if(self.travelLocationId.length > 0){
            self.serviceCertificate.travelLocationId = self.travelLocationId
            self.serviceCertificate.travelLocationTitle = self.dropDownLocationService.text ?? ""
        }
        

        if(self.textFieldLocationServiceOther != nil && !self.textFieldLocationServiceOther.isHidden){
            self.serviceCertificate.travelLocationTitle = self.textFieldLocationServiceOther.text() ?? ""
        }
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.serviceAddRequest?.service_certificate != nil){
            
            if(self.serviceAddRequest?.service_certificate?.certificateName != (self.textFieldCertificateName.input.text ?? "")){
                isUpdate = true
            }
            
            if(self.serviceAddRequest?.service_certificate?.freeService != (self.textFieldFreeService.input.text ?? "")){
                isUpdate = true
            }
            
            if(self.serviceAddRequest?.service_certificate?.trailService != (self.textFieldTrailFreeService.input.text ?? "")){
                isUpdate = true
            }
            
            if(self.serviceAddRequest?.service_certificate?.travelServiceTypeId != self.travelServiceTypeId){
                isUpdate = true
            }
            
            if(self.serviceAddRequest?.service_certificate?.travelServiceTypeTitle != (self.dropDownTravelServiceType.text ?? "")){
                isUpdate = true
            }
            
            if(self.serviceAddRequest?.service_certificate?.travelServiceReq != self.textViewTravelRequirement.text){
                isUpdate = true
            }
            
            if(self.serviceAddRequest?.service_certificate?.travelLocationTitle != (self.dropDownLocationService.text ?? "")){
                isUpdate = true
            }
            
            if(self.textFieldLocationServiceOther != nil && !self.textFieldLocationServiceOther.isHidden && self.serviceAddRequest?.service_certificate?.travelLocationTitle != self.textFieldLocationServiceOther.text()){
                isUpdate = true
            }
        } else {
            isUpdate = true
        }
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.serviceAddPhysicalOccupationalTherapyViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddPhysicalOccupationalTherapyViewController.nameOfClass) as ServiceAddPhysicalOccupationalTherapyViewController
        
        self.serviceAddPhysicalOccupationalTherapyViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddPhysicalOccupationalTherapyViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddPhysicalOccupationalTherapyViewController!, animated: true)
    }
}

extension ServiceAddSpecialListingViewController{
    
    @IBAction func methodFreeServiceAction(_ sender: UIButton) {
        
        self.btnFreeServiceYes.isSelected = false
        self.btnFreeServiceNo.isSelected = false
        self.isUpdate = true
        if(self.btnFreeServiceYes == sender){
            self.btnFreeServiceYes.isSelected = true
            self.textFieldFreeService.isHidden = false
        } else if(self.btnFreeServiceNo == sender){
            self.btnFreeServiceNo.isSelected = true
            self.textFieldFreeService.isHidden = true
        }
    }
    
    @IBAction func methodTrailServiceAction(_ sender: UIButton) {
        
        self.btnTrailServiceYes.isSelected = false
        self.btnTrailServiceNo.isSelected = false
        self.isUpdate = true
        if(self.btnTrailServiceYes == sender){
            self.btnTrailServiceYes.isSelected = true
            self.textFieldTrailFreeService.isHidden = false
        } else if(self.btnTrailServiceNo == sender){
            self.btnTrailServiceNo.isSelected = true
            self.textFieldTrailFreeService.isHidden = true
        }
    }
    
    @IBAction func methodTravelServiceAction(_ sender: UIButton) {
        
        self.btnTravelServiceYes.isSelected = false
        self.btnTravelServiceNo.isSelected = false
        self.isUpdate = true
        if(self.btnTravelServiceYes == sender){
            self.btnTravelServiceYes.isSelected = true
            self.viewTravelServiceDetail.isHidden = false
        } else if(self.btnTravelServiceNo == sender){
            self.btnTravelServiceNo.isSelected = true
            self.viewTravelServiceDetail.isHidden = true
        }
    }
    
    //MARK:- Button Action method implementation
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

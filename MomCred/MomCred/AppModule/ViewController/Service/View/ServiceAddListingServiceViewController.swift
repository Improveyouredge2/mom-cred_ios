//
//  ServiceAddListingServiceViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 03/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * ServiceAddListingServiceViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddListingServiceViewController : LMBaseViewController{
    
    fileprivate let formNumber = 14
    
    // Event
    @IBOutlet weak var tableViewSpecificTarget: UITableView!
    
    @IBOutlet weak var constraintstableViewSpecificTargetHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    fileprivate var specificTargetList:[ServiceListData] = []
    fileprivate var serviceAddAdditionalInfoViewController:ServiceAddAdditionalInfoViewController?
    fileprivate var isUpdate = false
    fileprivate var presenter = ServiceAddListingServicePresenter()
    
    var serviceAddRequest:ServiceAddRequest?
    var screenName:String = ""
    var serviceList:[ServiceListingList]?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        self.tableViewSpecificTarget.backgroundColor = UIColor.clear
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        presenter.getServiceProviderList()
        
        self.setScreenData()
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serviceAddAdditionalInfoViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        //        @IBOutlet weak var tableViewSpecificTarget: UITableView!
        //        @IBOutlet weak var constraintstableViewSpecificTargetHeight: NSLayoutConstraint!
        //        fileprivate var specificTargetList:[String] = []
        
        
        if(self.tableViewSpecificTarget.contentSize.height > 0){
             tableViewSpecificTarget.scrollToBottom()
            if((self.specificTargetList.count) == 0){
                self.constraintstableViewSpecificTargetHeight?.constant = AddMoreCell.inputViewCellSize
            } else {
                self.constraintstableViewSpecificTargetHeight?.constant = self.tableViewSpecificTarget.contentSize.height
            }
            
            self.updateTableViewHeight(tableView: tableViewSpecificTarget)
        }
        
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewSpecificTarget == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewSpecificTarget.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewSpecificTarget.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewSpecificTargetHeight.constant = heightOfTableView
            }
        }
    }
}

extension ServiceAddListingServiceViewController{
    fileprivate func setScreenData(){
        
        if(self.serviceAddRequest?.service_other_good_services != nil){
            
            self.specificTargetList = self.serviceAddRequest?.service_other_good_services?.service ?? []
            
            self.tableViewSpecificTarget.reloadData()
        }
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        /*
        if (self.specificTargetList.count == 0){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_other_good_service_listing.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        */
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        let serviceListing = ServiceOtherGoodServices()
        serviceListing.service = self.specificTargetList
        
        self.serviceAddRequest?.service_other_good_services = serviceListing
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.serviceAddAdditionalInfoViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddAdditionalInfoViewController.nameOfClass) as ServiceAddAdditionalInfoViewController
        
        self.serviceAddAdditionalInfoViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddAdditionalInfoViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddAdditionalInfoViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension ServiceAddListingServiceViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceAddListingServiceViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tableViewSpecificTarget){
//            if((self.specificTargetList.count) != HelperConstant.minimumBlocks){
                return (specificTargetList.count) + 1
//            } else {
//                return specificTargetList.count
//            }
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        if(self.tableViewSpecificTarget == tableView){
            
            if(indexPath.row == 0){
                
                let cell = tableView.dequeueReusableCell(withIdentifier: AddMoreCell.nameOfClass) as! AddMoreCell
                
                cell.textField.placeholder = "Link"
                if(cell.ViewRoleDesc != nil){
                    cell.ViewRoleDesc.isHidden = true
                }
                cell.delegate = self
                cell.refTableView = tableViewSpecificTarget
                if let list = serviceList {
                    cell.updateDropDown(listInfo: list)
                }
                
                cell.backgroundColor = UIColor.clear
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
                
                cell.delegate = self
                //                cell.cellIndex = indexPath
                cell.refTableView = self.tableViewSpecificTarget
                
//                if(self.specificTargetList.count) != HelperConstant.minimumBlocks{
                    cell.lblTitle.text = specificTargetList[indexPath.row-1].link
                    cell.lblTitleDesc.text = specificTargetList[indexPath.row-1].desc
                    
                    cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
//                } else {
//                    cell.cellIndex = indexPath
//                    cell.lblTitle.text = specificTargetList[indexPath.row].link
//                    cell.lblTitleDesc.text = specificTargetList[indexPath.row].desc
//                }
                
                cell.backgroundColor = UIColor.clear
                return cell
            }
            
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension ServiceAddListingServiceViewController: AddMoreCellDelegate{
    func newText(text: String, textDesc:String, textRoleDesc:String, refTableView: UITableView?, selectedServiceListingList: ServiceListingList?) {
        
        let associatedListDetail = ServiceListData()
        associatedListDetail.link = text
        associatedListDetail.desc = textDesc
        associatedListDetail.roleDesc = textRoleDesc
        
        if(refTableView == self.tableViewSpecificTarget){
            if((self.specificTargetList.count) < HelperConstant.minimumBlocks){
                if(selectedServiceListingList == nil){
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_facility_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
                    return
                }
                
                associatedListDetail.id = selectedServiceListingList?.id ?? ""
                associatedListDetail.link = selectedServiceListingList?.title ?? ""
                
                if(self.specificTargetList.map{$0.id}.contains(selectedServiceListingList?.id)){
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                    return
                }
                
                self.isUpdate = true
                self.specificTargetList.append(associatedListDetail)
                self.tableViewSpecificTarget.reloadData()
                
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

extension ServiceAddListingServiceViewController : TitleDescViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        self.isUpdate = true
        if(refTableView == self.tableViewSpecificTarget && self.specificTargetList.count > 0){
            
            self.specificTargetList.remove(at: cellIndex?.row ?? 0)
            self.tableViewSpecificTarget.reloadData()
        }
    }
}

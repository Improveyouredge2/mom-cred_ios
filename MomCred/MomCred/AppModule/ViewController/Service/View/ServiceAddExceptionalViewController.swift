//
//  ServiceAddExceptionalViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 25/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 * ServiceAddExceptionalViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceAddExceptionalViewController : LMBaseViewController{
    
    fileprivate let formNumber = 3
    
    @IBOutlet weak var btnStandard: UIButton!
    @IBOutlet weak var btnExceptional: UIButton!
    @IBOutlet weak var btnStandardExceptional: UIButton!
    
    @IBOutlet weak var btnAustim: UIButton!
    @IBOutlet weak var btnDownSyndrone: UIButton!
    @IBOutlet weak var btnMotor: UIButton!
    @IBOutlet weak var btnVision: UIButton!
    @IBOutlet weak var btnHearing: UIButton!
    @IBOutlet weak var textView:KMPlaceholderTextView!
    
    @IBOutlet weak var stackViewPrimarilyExceptional: UIStackView!
    @IBOutlet weak var stackViewClassificationExceptional: UIStackView!
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingService: Bool = false

    var exceptional_classification : [ListingDataDetail]?
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    let kHeaderSectionTag: Int = 6900;
    var expandTableNumber:[Int] = []
    var serviceAddRequest:ServiceAddRequest?
    
    fileprivate var presenter = ServiceAddExceptionalPresenter()
    fileprivate var serviceAddSpecialListingViewController:ServiceAddSpecialListingViewController?
    
    fileprivate let serviceInformation = ServiceInformation()
    fileprivate var isUpdate = false
    
    var screenName:String = ""
    
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        
        if !isEditingService {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serviceAddSpecialListingViewController = nil
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.tableView.contentSize.height > 0){
            tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
        }
    }
    
}

extension ServiceAddExceptionalViewController{
    fileprivate func reloadChildListing(){
        
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > 0){
            for expectional in self.exceptional_classification!{
                self.presenter.serverChildListingRequest(parentId: expectional.listing_id ?? "")
            }
        }
    }
    
    func updateExceptionalTableList(){
        self.tableView.reloadData()
    }
}

extension ServiceAddExceptionalViewController{
    
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        self.exceptional_classification = ServiceAddPresenter.parentListingList?.parentListingData?.exceptional_classification
        
        self.reloadChildListing()
        
        if(self.serviceAddRequest?.service_information != nil){
            if(self.serviceAddRequest?.service_information?.stdServices != nil && (self.serviceAddRequest?.service_information?.stdServices?.length)! > 0){
                let serviceProvider:Int = Int(self.serviceAddRequest?.service_information?.stdServices ?? "1") ?? 1
                
                self.btnStandard.isSelected = false
                self.btnExceptional.isSelected = false
                self.btnStandardExceptional.isSelected = false
                
                self.serviceInformation.stdServices = self.serviceAddRequest?.service_information?.stdServices ?? ""
                
                switch serviceProvider{
                case 1:
                    self.btnStandard.isSelected = true
                    break
                case 2:
                    self.btnExceptional.isSelected = true
                    break
                case 3:
                    self.btnStandardExceptional.isSelected = true
                    break
                default:
                    break
                }
            }
            
            if(self.serviceAddRequest?.service_information?.listDisabilities != nil && (self.serviceAddRequest?.service_information?.listDisabilities?.length)! > 0){
                self.textView.text = self.serviceAddRequest?.service_information?.listDisabilities ?? ""
                self.serviceInformation.listDisabilities = self.serviceAddRequest?.service_information?.listDisabilities ?? ""
            }
        }
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var selectedId:[DetailData] = []
        
            if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > 0){
                for expectional in self.exceptional_classification!{
                    if(expectional.subcat != nil && (expectional.subcat?.count)! > 0){
                        for childExceptional in expectional.subcat!{
                            if(childExceptional.selectionStaus){
                                let detailData = DetailData()
                                detailData.id = childExceptional.listing_id ?? ""
                                detailData.title = childExceptional.listing_title ?? ""
                                
                                
                                selectedId.append(detailData)
                            }
                        }
                    }
                }
            }
        
        if(self.btnExceptional.isSelected || self.btnStandardExceptional.isSelected){
            if(selectedId.count == 0){
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_exceptional_service_offered.getLocalized(), buttonTitle: nil, controller: nil)
                
                return false
            }
        }
        
        if(self.serviceAddRequest == nil){
            self.serviceAddRequest = ServiceAddRequest()
        }
        
        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if(UserDefault.getSID() != nil && (UserDefault.getSID()?.length)! > 0  && self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = UserDefault.getSID() ?? ""
        } else if(self.serviceAddRequest?.service_id == nil){
            self.serviceAddRequest?.service_id = ""
        }
        
        
        if(self.btnStandard.isSelected){
            serviceInformation.stdServices = "1"
        } else if(self.btnExceptional.isSelected){
            serviceInformation.stdServices = "2"
        } else {
            serviceInformation.stdServices = "3"
        }
        
        serviceInformation.exceptionalServices = selectedId
        serviceInformation.listDisabilities = self.textView.text ?? ""
        
        self.serviceAddRequest?.service_information = serviceInformation
        
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.serviceAddRequest?.service_information?.listDisabilities != (self.textView.text ?? "")){
            isUpdate = true
        }
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.serviceAddSpecialListingViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddSpecialListingViewController.nameOfClass) as ServiceAddSpecialListingViewController
        
        self.serviceAddSpecialListingViewController?.serviceAddRequest = self.serviceAddRequest
        serviceAddSpecialListingViewController?.isEditingService = isEditingService
        self.navigationController?.pushViewController(self.serviceAddSpecialListingViewController!, animated: true)
    }
}

extension ServiceAddExceptionalViewController{
    //MARK:- Button Action method implementation
    
    @IBAction func methodStandardAction(_ sender: UIButton) {
        
        self.btnStandard.isSelected = false
        self.btnExceptional.isSelected = false
        self.btnStandardExceptional.isSelected = false
        self.isUpdate = true
        
        if(self.btnStandard == sender){
            self.btnStandard.isSelected = true
        } else if(self.btnExceptional == sender){
            self.btnExceptional.isSelected = true
        } else {
            self.btnStandardExceptional.isSelected = true
        }
    }
    
    @IBAction func methodExceptionalClassificationSpeicalNeedAction(_ sender: UIButton) {
        
        self.btnAustim.isSelected = false
        self.btnDownSyndrone.isSelected = false
        
        if(self.btnAustim == sender){
            self.btnAustim.isSelected = true
        }else if (self.btnDownSyndrone == sender){
            self.btnDownSyndrone.isSelected = true
        }
        
    }
    
    @IBAction func methodExceptionalClassificationAdaptiveAction(_ sender: UIButton) {
        
        self.btnMotor.isSelected = false
        self.btnVision.isSelected = false
        self.btnHearing.isSelected = false
        
        if(self.btnMotor == sender){
            self.btnMotor.isSelected = true
        } else if(self.btnVision == sender){
            self.btnVision.isSelected = true
        } else if(self.btnHearing == sender){
            self.btnHearing.isSelected = true
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let serviceDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ServiceDetailView }
            if let serviceDetailVC = serviceDetailVC as? ServiceDetailView {
                serviceDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(serviceDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension ServiceAddExceptionalViewController: UITableViewDelegate, UITableViewDataSource{
    // MARK: - Tableview Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.exceptional_classification?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rowCount = 0
        
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > section){
            rowCount = self.exceptional_classification![section].subcat?.count ?? 0
        }
        
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > section){
            return self.exceptional_classification![section].listing_title ?? ""
        }
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(hexString: ColorCode.spinnerInnerColor)
        
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.backgroundColor = UIColor.clear
        headerLabel.font = FontsConfig.FontHelper.defaultRegularFontWithSize(20)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > section){
            headerLabel.text = self.exceptional_classification![section].listing_title ?? ""
        }
        
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: SingleSelectionCell.nameOfClass) as! SingleSelectionCell
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessContactAddPersonnelCell.nameOfClass) as! BusinessContactAddPersonnelCell
        
        var listingData:ListingDataDetail?
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > indexPath.section){
            listingData = self.exceptional_classification![indexPath.section].subcat?[indexPath.row]
        }
        
        if(listingData != nil){
            cell.delegate = self
            cell.cellIndex = indexPath
            cell.lblTitle.text = listingData?.listing_title
            cell.btnCheck.isSelected = listingData?.selectionStaus ?? false
            
            if(!self.isUpdate && self.serviceAddRequest?.service_information?.exceptionalServices != nil && (self.serviceAddRequest?.service_information?.exceptionalServices?.count)! > 0){
                if(self.serviceAddRequest?.service_information?.exceptionalServices?.map{$0.id}.contains(listingData?.listing_id ?? "") ?? false){
                    cell.btnCheck.isSelected = true
                    listingData?.selectionStaus = true
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Expand / Collapse Methods
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tableView!.beginUpdates()
            self.tableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
}

// TODO: Single selection cell delegate
//extension ServiceAddExceptionalViewController:SingleSelectionCellDelegate {
//    func selectListItem(cellIndex: IndexPath?, status:Bool){
//
//        self.isUpdate = true
//        // remove all selection
//        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > 0){
//            let serviceList = self.exceptional_classification![cellIndex?.section ?? 0].subcat!
//
//            for service in serviceList{
//                service.selectionStaus = false
//            }
//
//        }
//
//        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > cellIndex?.section ?? 0){
//            self.exceptional_classification![cellIndex?.section ?? 0].subcat?[cellIndex?.row ?? 0].selectionStaus = status
//        }
//
//        self.tableView.reloadData()
//    }
//}

// TODO: Multiple selection cell delegate
extension ServiceAddExceptionalViewController: BusinessContactAddPersonnelCellDelegate{
    
    func selectListItem(cellIndex: IndexPath?, status:Bool, tableView: UITableView?){

        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > cellIndex?.section ?? 0){
            self.exceptional_classification![cellIndex?.section ?? 0].subcat?[cellIndex?.row ?? 0].selectionStaus = status
        }
    }
}

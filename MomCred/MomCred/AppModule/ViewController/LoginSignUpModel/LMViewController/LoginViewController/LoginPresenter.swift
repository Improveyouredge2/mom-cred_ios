//
//  LoginPresenter.swift
//  LoginModule
//
//  Created by Apple on 15/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//


import Foundation
import UIKit


class LoginPresenter {
    fileprivate var view: LoginViewController!
    fileprivate var forgotView: ForgotPasswordViewController!
    
    /**
     *  Common view connection method.
     *
     *  @Use: attach LoginViewController view to the LoginPresenter
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func connectView(view: LoginViewController) {
        self.view = view
    }
}

extension LoginPresenter{
    /**
     *  Common navigate button click event action method.
     *
     *  Redirect on the other screen
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func moveToHome() {
        HelperConstant.appDelegate.setupCustomTabBar()
    }
    
    func moveToSignUp() {
        
        let userSelectionViewController : UserSelectionViewController = LMBaseApp.sharedInstance.getViewController(storyboardName: HelperConstant.onBoardStoryBoard, viewControllerName: UserSelectionViewController.nameOfClass)
        
        view.navigationController?.pushViewController(userSelectionViewController, animated: true)
    }
    
    /**
     *  Open forgot password pop up method.
     *
     *  @Use: Redirect on the forgot password screen
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func forgotPassword(){
        
        let forgotPasswordViewController : ForgotPasswordViewController = LMBaseApp.sharedInstance.getViewController(storyboardName: HelperConstant.onBoardStoryBoard, viewControllerName: ForgotPasswordViewController.nameOfClass)
        view.navigationController?.pushViewController(forgotPasswordViewController, animated: true)
    }
}

//**************************
//MARK:- Server Request
//**************************

extension LoginPresenter{
    /**
     *  Send user Login information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func submitData(loginData: LoginRequest){
        
        let request = LoginRequest(username: loginData.username, password: loginData.password, device_type: "ios")
        
        Spinner.show()
        
        LMLoginService.updateData(loginData: request, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    //                    LMHelper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    if(!Helper.sharedInstance.navigateToScreen()){
                        self.moveToHome()
                    }
                    
                    Spinner.hide()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    Spinner.hide()
                }
            }
        })
    }
}


extension LoginPresenter{
    /**
     *  Redeem Code from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverAPIRedeemCode(request:RedeemCodeRequest?){
        
        LMLoginService.serverAPIRedeemCode(redeemCodeRequest: request, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showAlertNativeDialog(title: LocalizationKeys.success.getLocalized(), message: message ?? "", completion: { (status) in
                    })
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

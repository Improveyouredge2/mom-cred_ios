//
//  LoginViewController.swift
//  LoginModule
//
//  Created by Apple on 15/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

//import GoogleSignIn

protocol LMLoginViewInterface {
    func showLoading()
    func hideLoading()
    func showEmptyView()
}

class LoginViewController: LMBaseViewController {
    
    //MARK:- @IBOutlet
//    @IBOutlet weak var txtEmail: UITextField!
//    @IBOutlet weak var txtPassword: UITextField!
    
    
    @IBOutlet weak var inputEmail: RYFloatingInput!
    @IBOutlet weak var inputPassword: RYFloatingInput!
    @IBOutlet weak var btnScanner: UIButton!

    //MARK:- Var and Let
    fileprivate let presenter = LoginPresenter()
    
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        let email_regex_pattern = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
            "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
        "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        inputEmail.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .placeholer("User Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .build()
        )
        inputEmail.input.keyboardType = .emailAddress
        
        inputPassword.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .warningColor(.white)
                .placeholer("Password")
                .maxLength(HelperConstant.LIMIT_UPPER_PASSWORD, onViolated: (message: "", callback: nil))
                .secure(true)
                .build()
        )
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        if(role_id == AppUser.ServiceProvider.rawValue){
            self.btnScanner.isHidden = false
        } else {
            self.btnScanner.isHidden = true
        }
        
    }
}

//*******************************
//MARK:- @IBAction
//*******************************

extension LoginViewController {
    /**
     *  Common Submit button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Redirect on the Home screen when mandatroy text field will be filled
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    @IBAction func btnActionSubmit(_ sender: Any){
        if validation() {
            
            let loginData = LoginRequest(username: self.inputEmail.text() ?? "", password: self.inputPassword.text() ?? "", device_type: "ios")
            presenter.submitData(loginData: loginData)
        }
    }
    
    /**
     *  Common Signup button method.
     *
     *  @Use: Redirect on the Signup screen when user first time access the app
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    @IBAction func btnActionSignUp(_ sender: Any){
        
        presenter.moveToSignUp()
        
    }
    /**
     *  Common Forgot Password button method.
     *
     *  @Use: Redirect on the Signup screen when user first time access the app
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func btnActionForgotPass(_ sender: Any){
        
        presenter.forgotPassword()
        
    }
    
    /**
     *  Common Google social login button method and get facebook user profile result record in result key.
     *
     *  Redirect on the Home screen
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func btnActionSkip(_ sender: UIButton){
        HelperConstant.appDelegate.setupCustomTabBar()
    }
}

//*********************************************
//MARK:- All Fileprivate and Internal Function
//*********************************************

extension LoginViewController{
    /**
     *  Common Home button method.
     *
     *  @Use: Test Validation
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func moveToHome(){
        presenter.moveToHome()
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    fileprivate func validation() -> Bool {
        
        if (inputEmail.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.emptyemail.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }else if (inputPassword.text()?.isEmpty)!{
            let message = LocalizationKeys.passwordEmpty.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }else if((inputPassword.text()?.length)! < HelperConstant.LIMIT_LOWER_PASSWORD || (inputPassword.text()?.length)! > HelperConstant.LIMIT_UPPER_PASSWORD){
            let message = LocalizationKeys.passwordLowerLimit.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        return true
    }
    
}


extension LoginViewController: QRScannerViewControllerDelegate {
    
    @IBAction private func btnScanAction(sender: UIButton) {
        let scanner = QRScannerViewController()
        scanner.delegate = self
        present(scanner, animated: true, completion: nil)
    }
    
    func scannerViewController(_ controller: QRScannerViewController, didScanCode code: String) {
        //inputRedeemCode.input.text = code
        let authToken = PMUserDefault.getAuthToken() ?? ""
        let userId = PMUserDefault.getUserId() ?? ""
        self.presenter.serverAPIRedeemCode(request: RedeemCodeRequest(code: code,auth_token:authToken,user_id:userId))
        dismiss(animated: true, completion: nil)
    }
    
    func scannerViewControllerDidFail(_ controller: QRScannerViewController) {
        dismiss(animated: true, completion: nil)
    }
}

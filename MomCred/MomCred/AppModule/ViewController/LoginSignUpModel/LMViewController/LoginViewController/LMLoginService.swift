//
//  LMLoginService.swift
//  LoginModule
//
//  Created by Apple_iOS on 15/01/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class LMLoginService  {
    
    /**
     *  Method connect with api call to server and provide information to screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    
    static func updateData(loginData: LoginRequest?, callback:@escaping (_ status:Bool, _ response: LoginRespone?, _ message: String?) -> Void) {
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = loginData?.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_LOGIN, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
       
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:LoginRespone? = LoginRespone().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    if(res?.loginResponseData != nil){
                        PMUserDefault.saveUserFirstName(userFirstName: res?.loginResponseData?.full_name ?? "")
                        PMUserDefault.saveUserUserBank(isAdded:  res?.loginResponseData?.bank_details_added ?? false)
                        
                        PMUserDefault.saveUserName(userName: res?.loginResponseData?.username ?? "")
                        PMUserDefault.setUserEmail(email: res?.loginResponseData?.email ?? "")
                        PMUserDefault.saveAppToken(appToken: res?.loginResponseData?.authorization ?? "")
                        PMUserDefault.saveUserPhone(userPhone: res?.loginResponseData?.mobile_no ?? "")
                        PMUserDefault.saveProfilePic(profilePic: res?.loginResponseData?.profile_pic ?? "")
                        PMUserDefault.saveUserRole(role: res?.loginResponseData?.role_id ?? "")
                        
                        PMUserDefault.saveUserId(userId: res?.loginResponseData?.user_id ?? "")

                        PMUserDefault.saveLoginInfo(loginInfo: res?.toJSON() as! NSDictionary)
                    }
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

extension LMLoginService{
    /**
     *  Method connect to Redeem Code.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverAPIRedeemCode(redeemCodeRequest:RedeemCodeRequest?, callback:@escaping (_ status:Bool, _ response: APIResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = redeemCodeRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_REDEEM_CODE, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:APIResponse? = APIResponse().getModelObjectFromServerResponse(response: responseDict)
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

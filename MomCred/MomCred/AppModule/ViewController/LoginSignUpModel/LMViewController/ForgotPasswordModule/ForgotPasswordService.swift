
//
//  ForgotPasswordService.swift
//  LoginModule
//
//  Created by Apple on 07/02/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
class ForgotPasswordService  {
    
    /**
     *  Method connect with api call to server and provide information to screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func updateData(requestData: ForgotPasswordRequest?, callback:@escaping (_ status:Bool, _ response: APIResponse?, _ message: String?) -> Void) {
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_FORGOTPASS, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:APIResponse? = APIResponse().getModelObjectFromServerResponse(response: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

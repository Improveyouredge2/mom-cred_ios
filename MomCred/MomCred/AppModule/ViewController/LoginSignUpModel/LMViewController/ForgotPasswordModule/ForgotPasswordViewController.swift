//
//  ForgotPasswordViewController.swift
//  LoginModule
//
//  Created by Apple on 15/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import UIKit

class ForgotPasswordViewController: BaseAlertViewController {
    
    //MARK:- @IBOutlet
    @IBOutlet weak var inputEmail: RYFloatingInput!
    
    //MARK:- Var and Let
    let presenter = ForgotPasswordPresenter()
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        inputEmail.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .placeholer("Email ")
                .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                .build()
        )
        inputEmail.input.keyboardType = .emailAddress
        
    }
}

//****************** @IBAction ****************** //

extension ForgotPasswordViewController {
    /**
     *  Common Submit button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Get textfield data and send to the server
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    @IBAction func methodSumbitAction(_ sender : Any){
        if validation() {
            presenter.serverRequest(forgotData: ForgotPasswordRequest(email: self.inputEmail.text() ?? ""))
        }
    }
    
    /**
     *  Common Close button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Close the pop up
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func methodCloseAction(_ sender : Any){
        self.dismiss(animated: true) {
            self.onClickActionOK(false)
        }
    }
}

//*********************************************
//MARK:- All Fileprivate Function
//*********************************************

extension ForgotPasswordViewController{
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    fileprivate func validation() -> Bool {
        if (self.inputEmail.text()?.trim().isEmpty)! {
            let message = LocalizationKeys.emptyemail.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }else if !(self.inputEmail.text()?.isEmail())!{
            let message = LocalizationKeys.invalidEmail.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }else if((self.inputEmail.text()?.length)! > HelperConstant.LIMIT_EMAIL){
            let message = LocalizationKeys.invalidEmail.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        return true
    }
}

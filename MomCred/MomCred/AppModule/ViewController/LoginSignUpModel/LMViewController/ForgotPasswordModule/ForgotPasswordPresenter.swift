//
//  ForgotPasswordPresenter.swift
//  LoginModule
//
//  Created by Apple on 18/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

class ForgotPasswordPresenter {
    
    fileprivate var view: ForgotPasswordViewController!
    
    /**
     *  Common view connection method.
     *
     *  @Use: attach LoginViewController view to the LoginPresenter
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func connectView(view: ForgotPasswordViewController) {
        self.view = view
    }
}

//MARK:- Server Request
extension ForgotPasswordPresenter{
    /**
     *  Send Forgot password request information to server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverRequest(forgotData: ForgotPasswordRequest){
        
        let request = ForgotPasswordRequest(email: forgotData.email)
        
        ForgotPasswordService.updateData(requestData: request, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.navigationController?.popToRootViewController(animated: true)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

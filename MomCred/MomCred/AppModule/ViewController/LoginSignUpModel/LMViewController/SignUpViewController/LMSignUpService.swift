//
//  LMSignUpService.swift
//  LoginModule
//
//  Created by Apple on 06/02/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
class LMSignUpService  {
    
    /**
     *  Method connect with api call to server and provide information to screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func updateData(requestData: SignUpRequest?, callback:@escaping (_ status:Bool, _ response: SignUpResponse?, _ message: String?) -> Void) {
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_SIGNUP, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:SignUpResponse? = SignUpResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    if(res?.signUpResponseData != nil){
                        PMUserDefault.saveUserId(userId: res?.signUpResponseData?.user_id ?? "")
                        PMUserDefault.setUserEmail(email: res?.signUpResponseData?.email ?? "")
                        PMUserDefault.saveAppToken(appToken: res?.signUpResponseData?.token ?? "")
                        PMUserDefault.saveUserPhone(userPhone: res?.signUpResponseData?.mobile ?? "")
                        PMUserDefault.saveAddress(address: res?.signUpResponseData?.address ?? "")
                        PMUserDefault.saveProfilePic(profilePic: res?.signUpResponseData?.profile_img ?? "")
//                        POSUserDefault.saveReferralCode(referral_code: res?.signUpResponseData?.referral_code ?? "")
                        
                        PMUserDefault.saveLoginInfo(loginInfo: res?.toJSON() as! NSDictionary)
                    }
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

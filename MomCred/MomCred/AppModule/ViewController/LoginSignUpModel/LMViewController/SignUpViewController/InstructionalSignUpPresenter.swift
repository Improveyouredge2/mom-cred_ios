//
//  InstructionalSignUpPresenter.swift
//  MomCred
//
//  Created by MD on 14/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class InstructionalSignUpPresenter {
    private weak var view: InstructionalSignUpViewController!

    func connectView(view: InstructionalSignUpViewController) {
        self.view = view
    }
}

extension InstructionalSignUpPresenter {
    private func moveToHome(){
        HelperConstant.appDelegate.setupCustomTabBar()
    }
}

extension InstructionalSignUpPresenter {
    func serverRequest() {
        if let name = view.inputFullName.text(), let username = view.inputUserName.text(), let email = view.inputEmail.text(), let mobile = view.inputMobNumber.text(), let password = view.inputPassword.text(), let question1 = view.question1.selectedAnswer, let question2 = view.question2.selectedAnswer, let question3 = view.question3.selectedAnswer, let question4 = view.question4.selectedAnswer, let question5 = view.question5.selectedAnswer {
            let request = InstructionalSignUpRequest(name: name, username: username, email: email, mobile_no: mobile, password: password, role_id: "\(view.selectedUserId)", question1: question1, question2: question2, question3: question3, question4: question4, question5: question5)
            
            LMSignUpService.updateData(requestData: request, callback: { (status, response, message) in
                DispatchQueue.main.async { [weak self] in
                    Spinner.hide()
                    if status {
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                            self?.view.navigationController?.popToRootViewController(animated: true)
                    } else {
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    }
                }
            })
        }
    }
}

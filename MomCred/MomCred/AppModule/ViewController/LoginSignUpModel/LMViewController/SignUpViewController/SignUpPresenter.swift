//
//  SignUpPresenter.swift
//  LoginModule
//
//  Created by Apple on 16/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import UIKit

class SignUpPresenter {
    
    fileprivate var view : SignUpViewController!
    /**
     *  Common view connection method.
     *
     *  @Use: attach LoginViewController view to the LoginPresenter
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func connectView(view: SignUpViewController) {
        self.view = view
    }
}

//**************************
//MARK:- Navigation
//**************************

extension SignUpPresenter{
    /**
     *  Common navigate button click event action method.
     *
     *  Redirect on the Home screen
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func moveToHome(){
        HelperConstant.appDelegate.setupCustomTabBar()
    }
}

//**************************
//MARK:- Server Request
//**************************
extension SignUpPresenter{
    /**
     *  Register social user information to server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverRequest(){
        
        let request = SignUpRequest(name: self.view.inputFullName.text() ?? "", username: self.view.inputUserName.text() ?? "", email: self.view.inputEmail.text() ?? "", mobile_no: self.view.inputMobNumber.text() ?? "", password: self.view.inputPassword.text() ?? "", role_id: "\(self.view.selectedUserId)")
        
        LMSignUpService.updateData(requestData: request, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
//                    if(!PMHelper.sharedInstance.navigateToScreen()){
//                        self.moveToHome()
                        
//                        AppUser.Enthusiast
                        self.view.navigationController?.popToRootViewController(animated: true)
//                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                }
            }
        })
    }
}

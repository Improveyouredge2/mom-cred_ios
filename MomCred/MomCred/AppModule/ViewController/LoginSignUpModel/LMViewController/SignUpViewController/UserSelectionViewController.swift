//
//  UserSelectionViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 28/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


class UserSelectionViewController: LMBaseViewController {
    
    //MARK:- @IBOutlet
    @IBOutlet private weak var viewEnthusiast: UIView! {
        didSet {
            viewEnthusiast.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleUserSelection(gesture:)))
            viewEnthusiast.addGestureRecognizer(tapGesture)
        }
    }
    @IBOutlet private weak var viewInstructional: UIView! {
        didSet {
            viewInstructional.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleUserSelection(gesture:)))
            viewInstructional.addGestureRecognizer(tapGesture)
        }
    }
    @IBOutlet private weak var viewNonInstructional: UIView! {
        didSet {
            viewNonInstructional.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleUserSelection(gesture:)))
            viewNonInstructional.addGestureRecognizer(tapGesture)
        }
    }

    //MARK:- Var and Let
    fileprivate var signUpViewController:SignUpViewController?
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.signUpViewController = nil
    }
    
}

//MARK:- Action method implementation
//MARK:-
extension UserSelectionViewController{
    @objc private func handleUserSelection(gesture: UIGestureRecognizer) {
        if gesture.state == .recognized {
            if gesture.view == viewInstructional {
                let signUpVC: InstructionalSignUpViewController = LMBaseApp.sharedInstance.getViewController(storyboardName: HelperConstant.onBoardStoryBoard, viewControllerName: InstructionalSignUpViewController.nameOfClass)
                signUpViewController?.selectedUserId = Int(AppUser.ServiceProvider.rawValue) ?? 0
                navigationController?.pushViewController(signUpVC, animated: true)
            } else {
                signUpViewController = LMBaseApp.sharedInstance.getViewController(storyboardName: HelperConstant.onBoardStoryBoard, viewControllerName: SignUpViewController.nameOfClass)
                if gesture.view == viewNonInstructional {
                    signUpViewController?.selectedUserId = Int(AppUser.LocalBusiness.rawValue) ?? 0
                } else {
                    signUpViewController?.selectedUserId = Int(AppUser.Enthusiast.rawValue) ?? 0
                }
                navigationController?.pushViewController(signUpViewController!, animated: true)
            }
        }
    }

    @IBAction func btnLoginSelection(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     *  Common Google social login button method and get facebook user profile result record in result key.
     *
     *  Redirect on the Home screen
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func btnActionSkip(_ sender: UIButton){
        HelperConstant.appDelegate.setupCustomTabBar()
    }
}

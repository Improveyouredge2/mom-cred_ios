//
//  InstructionalSignUpViewController.swift
//  MomCred
//
//  Created by MD on 14/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class InstructionalSignUpQuestionView: UIView {
    @IBOutlet private weak var lblQuestion: UILabel!
    @IBOutlet private weak var btnYes: UIButton!
    @IBOutlet private weak var btnNo: UIButton!
    @IBOutlet private weak var btnNA: UIButton!

    var selectedAnswer: String? {
        if btnYes.isSelected {
            return "Yes"
        } else if btnNo.isSelected {
            return "No"
        } else if btnNA.isSelected {
            return "N/A"
        }
        return nil
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        resetSelection()
    }
    
    private func resetSelection() {
        btnYes.isSelected = false
        btnNo.isSelected = false
        btnNA.isSelected = false
    }
    
    @IBAction private func btnAction(sender: UIButton) {
        if !sender.isSelected {
            btnYes.isSelected = false
            btnNo.isSelected = false
            btnNA.isSelected = false
            
            sender.isSelected = true
        }
    }
    
}

class InstructionalSignUpViewController: LMBaseViewController {
    
    //MARK:- @IBOutlet
    @IBOutlet weak var inputFullName: RYFloatingInput!
    @IBOutlet weak var inputUserName: RYFloatingInput!
    @IBOutlet weak var inputMobNumber: RYFloatingInput!
    @IBOutlet weak var inputEmail: RYFloatingInput!
    @IBOutlet weak var inputPassword: RYFloatingInput!
    @IBOutlet weak var inputConfPassword: RYFloatingInput!
    
    @IBOutlet weak var question1: InstructionalSignUpQuestionView!
    @IBOutlet weak var question2: InstructionalSignUpQuestionView!
    @IBOutlet weak var question3: InstructionalSignUpQuestionView!
    @IBOutlet weak var question4: InstructionalSignUpQuestionView!
    @IBOutlet weak var question5: InstructionalSignUpQuestionView!
    
    @IBOutlet weak var btnTermsCondition: UIButton!
    @IBOutlet weak var btnServiceProviderAgreement: UIButton!

    private var presenter  = InstructionalSignUpPresenter()
    private var imgData = Data()
    private var selectedProfileImage: UIImage?

    private let arrGender = ["Male","Female"]
    private var defaultDate = Date()
    private var datePickerBool: Bool = false
    var selectedUserId = Int(AppUser.Enthusiast.rawValue) ?? 2

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)
        
        inputFullName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Full Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputUserName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("User Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputMobNumber.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Mobile Number ")
                .maxLength(HelperConstant.LIMIT_UPPER_PHONE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        inputMobNumber.input.keyboardType = UIKeyboardType.phonePad
        
        inputEmail.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Email ")
                .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputEmail.input.keyboardType = .emailAddress
        
        inputPassword.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Password ")
                .maxLength(HelperConstant.LIMIT_UPPER_PASSWORD, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputConfPassword.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Confirm Password ")
                .maxLength(HelperConstant.LIMIT_UPPER_PASSWORD, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
    }

}

extension InstructionalSignUpViewController {
    private func performValidation() -> Bool {
        var message = ""
        
        if let isEmpty = inputFullName.text()?.trim().isEmpty, isEmpty {
            message = LocalizationKeys.onboard_emptyFullName.getLocalized()
        }
        
        if message.isEmpty, let isEmpty = inputUserName.text()?.trim().isEmpty, isEmpty {
            message = LocalizationKeys.onboard_emptyUserName.getLocalized()
        }
        
        if message.isEmpty, let emailText = inputEmail.text()?.trim() {
            if emailText.isEmpty {
                message = LocalizationKeys.emptyemail.getLocalized()
            } else if !emailText.isEmail() {
                message = LocalizationKeys.invalidEmail.getLocalized()
            } else if emailText.length > HelperConstant.LIMIT_EMAIL {
                message = LocalizationKeys.invalidEmail.getLocalized()
            }
        }
        
        if message.isEmpty, let mobileText = inputMobNumber.text()?.trim() {
            if mobileText.isEmpty {
                message = LocalizationKeys.emptyMobile.getLocalized()
            } else if mobileText.length < HelperConstant.LIMIT_LOWER_PHONE || mobileText.length > HelperConstant.LIMIT_UPPER_PHONE {
                message = LocalizationKeys.invalidMobile.getLocalized()
            }
        }
        
        if message.isEmpty, let passwordText = inputPassword.text() {
            if passwordText.trim().isEmpty {
                message = LocalizationKeys.passwordEmpty.getLocalized()
            } else if passwordText.length < HelperConstant.LIMIT_LOWER_PASSWORD || passwordText.length > HelperConstant.LIMIT_UPPER_PASSWORD {
                message = LocalizationKeys.passwordLowerLimit.getLocalized()
            } else if let confirmPasswordText = inputConfPassword.text(), passwordText != confirmPasswordText {
                message = LocalizationKeys.passwordNotMatch.getLocalized()
            }
        }
        
        if question1.selectedAnswer == nil || question2.selectedAnswer == nil || question3.selectedAnswer == nil || question4.selectedAnswer == nil || question5.selectedAnswer == nil {
            message = "Please answer all the questions."
        }
        
        if message.isEmpty, !btnTermsCondition.isSelected {
            message = LocalizationKeys.terms_of_service_error.getLocalized()
        }
        if !message.trim().isEmpty {
            showBannerAlertWith(message: message, alert: .error)
            return false
        }
        return true
    }
}

extension InstructionalSignUpViewController {
    @IBAction func btnActionSubmit(_ sender: UIButton) {
        if performValidation() {
            presenter.serverRequest()
        }
    }

    @IBAction func btnAcceptTermsConditionAction(_ sender : UIButton) {
        btnTermsCondition.isSelected = !btnTermsCondition.isSelected
    }

    @IBAction func btnServiceProviderAgreementAction(_ sender : UIButton) {
        btnServiceProviderAgreement.isSelected = !btnServiceProviderAgreement.isSelected
    }

    @IBAction func actionTermsCondition(_ sender:UIButton) {
        let controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMWebView.nameOfClass) as PMWebView
        controller.downloadUrl =  APIKeys.API_INSTRUCTIONAL_SIGNUP_TERMS
        controller.screenTitle = LocalizationKeys.terms_condition.getLocalized()
        navigationController?.pushViewController(controller, animated: true)
    }

    @IBAction func actionServiceProviderAgreement(_ sender:UIButton) {
        let controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMWebView.nameOfClass) as PMWebView
        controller.downloadUrl =  APIKeys.API_INSTRUCTIONAL_SIGNUP_AGREEMENT
        controller.screenTitle = "Service Provider Agreement"
        navigationController?.pushViewController(controller, animated: true)
    }

    @IBAction func btnActionSkip(_ sender: UIButton) {
        HelperConstant.appDelegate.setupCustomTabBar()
    }
}

//
//  SingUpViewController.swift
//  LoginModule
//
//  Created by Apple on 15/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//


import UIKit
//import DropDown

class SignUpViewController: LMBaseViewController {
    
    //MARK:- @IBOutlet
    @IBOutlet weak var inputFullName: RYFloatingInput!
    @IBOutlet weak var inputUserName: RYFloatingInput!
    @IBOutlet weak var inputMobNumber: RYFloatingInput!
    @IBOutlet weak var inputEmail: RYFloatingInput!
    @IBOutlet weak var inputPassword: RYFloatingInput!
    @IBOutlet weak var inputConfPassword: RYFloatingInput!
    @IBOutlet weak var btnTermsCondition: UIButton!
    
    //MARK:- Var and Let
    fileprivate var presenter  = SignUpPresenter()
    fileprivate var imgData    = Data()
    fileprivate var selectedProfileImage : UIImage?
//    let dropDown = DropDown()
    let arrGender = ["Male","Female"]
    var defaultDate = Date()
    var datePickerBool: Bool = false
    var selectedUserId = Int(AppUser.Enthusiast.rawValue) ?? 2
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)
        
        inputFullName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Full Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputUserName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("User Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputMobNumber.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Mobile Number ")
                .maxLength(HelperConstant.LIMIT_UPPER_PHONE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        self.inputMobNumber.input.keyboardType = UIKeyboardType.phonePad
        
        inputEmail.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Email ")
                .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputEmail.input.keyboardType = .emailAddress
        
        inputPassword.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Password ")
                .maxLength(HelperConstant.LIMIT_UPPER_PASSWORD, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputConfPassword.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Confirm Password ")
                .maxLength(HelperConstant.LIMIT_UPPER_PASSWORD, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
    }

}

//*******************************
//MARK:- TextField Validation
//*******************************

extension SignUpViewController{
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func performValidation() -> Bool{
        var message = ""
        
        if (self.inputFullName.text()?.trim().isEmpty)! {
            message = LocalizationKeys.onboard_emptyFullName.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputUserName.text()?.trim().isEmpty)! {
            message = LocalizationKeys.onboard_emptyUserName.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputEmail.text()?.trim().isEmpty)! {
            message = LocalizationKeys.emptyemail.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if !(self.inputEmail.text()?.isEmail())!{
            message = LocalizationKeys.invalidEmail.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputMobNumber.text()?.trim().isEmpty)! {
            message = LocalizationKeys.emptyMobile.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if ((self.inputMobNumber.text()?.trim().length)! < HelperConstant.LIMIT_LOWER_PHONE || (self.inputMobNumber.text()?.trim().length)! > HelperConstant.LIMIT_UPPER_PHONE ) {
            message = LocalizationKeys.invalidMobile.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if((self.inputEmail.text()?.length)! > HelperConstant.LIMIT_EMAIL){
            message = LocalizationKeys.invalidEmail.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputPassword.text()?.isEmpty)!{
            message = LocalizationKeys.passwordEmpty.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if((self.inputPassword.text()?.length)! < HelperConstant.LIMIT_LOWER_PASSWORD || (self.inputPassword.text()?.length)! > HelperConstant.LIMIT_UPPER_PASSWORD){
            message = LocalizationKeys.passwordLowerLimit.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputConfPassword.text()?.isEmpty)! {
            message = LocalizationKeys.confirmPassworEmpty.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if((self.inputConfPassword.text()?.length)! < HelperConstant.LIMIT_LOWER_PASSWORD){
            message = LocalizationKeys.confirmpasswordLowerLimit.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputPassword.text()?.compare(self.inputConfPassword.text()!) != ComparisonResult.orderedSame) {
            message = LocalizationKeys.passwordNotMatch.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if !btnTermsCondition.isSelected{
            message = LocalizationKeys.terms_of_service_error.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        return true
    }
}

//*******************************
//MARK:- @IBAction
//*******************************

extension SignUpViewController{
    /**
     *  Common Submit button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Redirect on the Home screen when mandatroy text field will be filled
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    @IBAction func btnActionSubmit(_ sender: UIButton) {
        if performValidation() {
            presenter.serverRequest()
        }
    }
    
    /**
     *  Common Camera button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    @IBAction func btnAcceptTermsConditionAction(_ sender : UIButton){
        self.btnTermsCondition.isSelected = !self.btnTermsCondition.isSelected
    }
    
    /**
     *  Terms and Condition view action button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func actionTermsCondition(_ sender:UIButton) {
        let controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMWebView.nameOfClass) as PMWebView
        controller.downloadUrl =  APIKeys.API_INSTRUCTIONAL_SIGNUP_TERMS
        controller.screenTitle = LocalizationKeys.terms_condition.getLocalized()
        navigationController?.pushViewController(controller, animated: true)
    }

    /**
     *  Common Google social login button method and get facebook user profile result record in result key.
     *
     *  Redirect on the Home screen
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func btnActionSkip(_ sender: UIButton){
        HelperConstant.appDelegate.setupCustomTabBar()
    }
}

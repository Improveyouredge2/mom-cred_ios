//
//  BaseViewController.swift
//  LoginModule
//
//  Created by Apple on 15/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import AFMInfoBanner

let screenSize = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height
let WIDTH_FACTOR = screenWidth/320
let HEIGHT_FACTOR = screenHeight/568

import Foundation
import UIKit
import AVKit

/*
 PMBaseViewController is a generic enum for specifying banner alert types used in app to display message and errors
 */
enum PMBannerAlertType {
    case error
    case success
    case info
}

/*
 PMBaseViewController is a generic controller base class that manages common method and functionality of application view.  It has methods that are called when a view appears or disappears, manage custom navigation height, events of navigation bar, implement image selection from photo gallery or device camera.
 
 Subclasses can override methods to create their custom view hierarchy, or specify a nib name to be loaded automatically.  This class is also a good place for delegate & datasource methods, and other controller stuff.
 */

class LMBaseViewController: UIViewController {
    
    @IBOutlet weak var constraintNavigationHeight:NSLayoutConstraint?
    @IBOutlet weak var lbl_NavigationTitle:UILabel!
    
    // specific selected side menu selection text
    var selectedMenuItem : String?
    var downloadUrl:String?
    var browseUrl:String?
    
    var screenTitle:String = ""
    
    //MARK:- Image Picker var
    //MARK:-
    var temp_ProfileImageData:Data?
    var temp_ProfileImage:UIImage?
    var temp_ProfielImageUrl:String?
    var actionSheetControllerIOS8: UIAlertController!
    var isScreenDisplayFromImageController = true
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(self.constraintNavigationHeight != nil){
            self.constraintNavigationHeight?.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
        }
        
        if(screenTitle.length > 0 && self.lbl_NavigationTitle != nil){
            self.lbl_NavigationTitle.text = screenTitle
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // These methods control the attributes of the status bar when this view controller is shown. They can be overridden in view controller subclasses to return the desired status bar attributes.
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    /**
     *  Method app default top banner with lingual text as per types. It is either error or success or information.
     *
     *  @param key message display on top banner and banner type.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showBannerAlertWith(message : String? , alert type : PMBannerAlertType){
        
        let bannerTitle = type == .error ? LocalizationKeys.error.getLocalized() : type == .success ? LocalizationKeys.success.getLocalized() : LocalizationKeys.info.getLocalized()
        
        LMBaseApp.sharedInstance.showAlertViewControllerWith(title: bannerTitle, message: message!, buttonTitle: LMBaseApp.sharedInstance.getMessageForCode("ok")!, controller: self)
    }
}

//MARK:- @IBAction's method implementation
extension LMBaseViewController{
    
    /**
     *  Method can be override by inherited class for getting image object and data.
     *
     *  @param key image object and image bytes data.
     *
     *  @Developed By: Team Consagous
     */
    @objc func pickedImage(fromImagePicker:UIImage,imageData:Data, mediaType:String, imageName:String, fileUrl:String?){
        
        
    }
    
    /**
     *  Common Back button click event method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous
     */
    @IBAction func actionNavigationBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     *  Common Menu button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous
     */
    @IBAction func actionNavigationMenu(_ sender: UIButton) {
        //BaseApp.sharedInstance.showSideBarMenu(viewController: self)
    }
    
    /**
     *  Common Home button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous
     */
    @IBAction func actionNavigationHome(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     *  Common Notification button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous
     */
    @IBAction func actionNavigationNotification(_ sender: Any) {
       // BaseApp.appDelegate.openNotificationScreen()
    }
}


//MARK: Class common method
//MARK:-
extension LMBaseViewController {
    
    /**
     *  Pause on UI thread.
     *
     *  @param key delay is for pausing thread for executing and Closure is callback method.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    /**
     *  Method display Picture selection option whether it is from Photo Gallery or Device Camera.
     *
     *  @param key empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func displayPhotoSelectionOption(withCircularAllow: Bool = true, isSelectVideo: Bool = false, onlyVideos: Bool = false, videoDuration: TimeInterval = 60) {
        //Create the AlertController and add Its action like button in Actionsheet
        actionSheetControllerIOS8 = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: Helper.sharedInstance.getMessageForCode("cancel")!, style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        // Checking device is having camera capability or not
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            let option1ActionButton: UIAlertAction = UIAlertAction(title: LocalizationKeys.settings_dialog_camera_text.getLocalized(), style: .default) { [weak self] action -> Void in
                guard let self = self else { return }
                self.isScreenDisplayFromImageController = false
                print(Helper.sharedInstance.getMessageForCode("settings_dialog_camera_text")!)

                if isSelectVideo {
                    self.displayVideoPhotoOption(videoDuration: videoDuration)
                } else {
                    MediaSelector.shared.showSelector(on: self, delegate: self, mediaTypes: [kUTTypeImage as String], sourceType: .camera, shouldCrop: true, requiredImageSize: .zero, cropType: withCircularAllow ? .circular : .default, videoDuration: videoDuration)
                }
            }
            actionSheetControllerIOS8.addAction(option1ActionButton)
        }
        
        // Checking device having photo gallery capability or not
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)){
            let option2ActionButton: UIAlertAction = UIAlertAction(title: LocalizationKeys.settings_dialog_gallery_text.getLocalized(), style: .default) { [weak self] action -> Void in
                guard let self = self else { return }
                self.isScreenDisplayFromImageController = false
                
                print(Helper.sharedInstance.getMessageForCode("settings_dialog_gallery_text")!)

                var mediaTypes: [String] = []
                if !onlyVideos { mediaTypes.append(kUTTypeImage as String) }
                if isSelectVideo { mediaTypes.append(kUTTypeMovie as String) }
                MediaSelector.shared.showSelector(on: self, delegate: self, mediaTypes: mediaTypes, sourceType: .photoLibrary, shouldCrop: true, requiredImageSize: .zero, cropType: withCircularAllow ? .circular : .default, videoDuration: videoDuration)
            }
            actionSheetControllerIOS8.addAction(option2ActionButton)
        }
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    fileprivate func displayVideoPhotoOption(videoDuration: TimeInterval) {
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelActionButton: UIAlertAction = UIAlertAction(title: Helper.sharedInstance.getMessageForCode("cancel")!, style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        let option1ActionButton: UIAlertAction = UIAlertAction(title: LocalizationKeys.photo.getLocalized(), style: .default){ action -> Void in
            self.isScreenDisplayFromImageController = false
            print(Helper.sharedInstance.getMessageForCode("photo")!)

            MediaSelector.shared.showSelector(on: self, delegate: self, mediaTypes: [kUTTypeImage as String], sourceType: .camera, shouldCrop: true, requiredImageSize: .zero, cropType: .default, videoDuration: videoDuration)
        }
        actionSheetController.addAction(option1ActionButton)

        let option2ActionButton: UIAlertAction = UIAlertAction(title: LocalizationKeys.video.getLocalized(), style: .default){ action -> Void in
            self.isScreenDisplayFromImageController = false
            
            print(Helper.sharedInstance.getMessageForCode("video")!)

            MediaSelector.shared.showSelector(on: self, delegate: self, mediaTypes: [kUTTypeMovie as String], sourceType: .camera, shouldCrop: true, requiredImageSize: .zero, cropType: .default, videoDuration: videoDuration)
        }
        actionSheetController.addAction(option2ActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
}

extension LMBaseViewController: MediaSelectorDelegate {
    func mediaSelected(name: String, type: String, data: Data, thumb: UIImage, url: URL?) {
        print("mediaSelected")
        temp_ProfileImage = thumb
        temp_ProfileImageData = data
        temp_ProfielImageUrl = url?.path

        pickedImage(fromImagePicker: thumb, imageData: data, mediaType: type, imageName: name, fileUrl: url?.path)
    }
    
    func cancelMediaSelector() {
        print("cancelMediaSelector")
    }
}

//MARK: - CameraCaptureDelegate method implementation
//MARK:-
extension LMBaseViewController: CameraCaptureDelegate {
    
    /**
     *  Delegate method on CameraCaptureDelegate for getting image object with imagename, image type, image data object.
     *
     *  @param key imageName, imageType and imageData.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
//    func mediaCaptureInfo(withMediaName fileName: String!, withMediaType mediaType: String!, withMediaData mediaData: Data!, thumnailImage: UIImage!, withUrl fileUrl: String!) {
//        <#code#>
//    }
    @objc func mediaCaptureInfo(withMediaName fileName: String?, withMediaType mediaType: String?, withMediaData mediaData: Data?, thumnailImage: UIImage?, withUrl fileUrl: String!) {
        
        if(mediaData != nil){
            if(thumnailImage != nil){
                temp_ProfileImage = thumnailImage
                temp_ProfileImageData = mediaData
                temp_ProfielImageUrl = fileUrl
                self.pickedImage(fromImagePicker: thumnailImage!, imageData: mediaData!, mediaType:mediaType ?? "", imageName:fileName ?? "", fileUrl: fileUrl)
            } else {
                let image = UIImage(data:mediaData!,scale:1.0)
                temp_ProfileImage = image
                temp_ProfileImageData = mediaData
                
                if image != nil {
                    self.pickedImage(fromImagePicker: image!, imageData: mediaData!, mediaType:mediaType ?? "", imageName:fileName ?? "", fileUrl: fileUrl)
                } else {
                    Spinner.show()
                    DispatchQueue.main.async {
                        if let imgSnapShot = fileUrl.toURL()?.generateThumbnail() {
                            Spinner.hide()
                            self.pickedImage(fromImagePicker: imgSnapShot, imageData: mediaData!, mediaType:mediaType ?? "", imageName:fileName ?? "", fileUrl: fileUrl)
                        }
                    }
                }
                temp_ProfielImageUrl = fileUrl
            }
        }else{
            let alert = UIAlertController(title:nil, message:LocalizationKeys.settings_dialog_unable_to_pick_file.getLocalized(), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title:LocalizationKeys.ok.getLocalized(), style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     *  Delegate method on CameraCaptureDelegate for cancel event on image capture.
     *
     *  @param key empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelMediaCapture() {
        
    }
}



extension URL {
    func generateThumbnail() -> UIImage? {
        do {
            let asset = AVURLAsset(url: self)
            
            var thumbTime = asset.duration
            thumbTime.value = 0
            
            
            
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            // Select the right one based on which version you are using
            // Swift 4.2
            let cgImage = try imageGenerator.copyCGImage(at: thumbTime, actualTime: nil)//imageGenerator.copyCGImage(at: .zero,  actualTime: nil)
            
            //            // Swift 4.0
            //            let cgImage = try imageGenerator.copyCGImage(at: CMTime.zero,
            //                                                         actualTime: nil)
            
            return UIImage(cgImage: cgImage)
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func getVideoThumbnail(completion: @escaping (_ image: UIImage?) -> Void) {
        DispatchQueue.global(qos: .default).async {
            let asset = AVAsset(url: self)
            let assetImgGenerate = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            
            let time = CMTimeMake(value: 2, timescale: 1)
            do {
                let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                DispatchQueue.main.async {
                    let thumbnail = UIImage(cgImage: img)
                    completion(thumbnail)
                }
            } catch let error{
                DispatchQueue.main.async {
                    print("Error :: ", error)
                    completion(nil)
                }
            }
        }
    }
}

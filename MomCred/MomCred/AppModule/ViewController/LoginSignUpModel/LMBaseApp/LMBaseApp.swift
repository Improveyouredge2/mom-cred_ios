//
//  LMBaseApp.swift
//  LoginModule
//
//  Created by Apple on 18/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NotificationBannerSwift

//Roleid = 2 for enthusiast
//Roleid = 3 for service provider
//Roleid = 4 for local business
enum AppUser:String{
    case ServiceProvider = "3"
    case LocalBusiness = "4"
    case Enthusiast = "2"
}

class LMBaseApp: NSObject {
    
    static let sharedInstance = LMBaseApp()
    
    // MARK:- Load viewController from storyboard (used in multi-storyboard)
    func getViewController<T>(storyboardName:String, viewControllerName:String) -> T {
        let storyBoard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: viewControllerName) as! T
        return viewController
    }
    
    //MARK:- Load value from string file to show text in diffrent language
    func getMessageForCode(_ constantName:String) -> String? {
        let fileName = HelperConstant.DefaultLang
        return NSLocalizedString(constantName, tableName: fileName, bundle: Bundle.main, value: "", comment: "")
    }
    /**
     *  Method app default top banner with lingual text as per types. It is either error or success or information.
     *
     *  @param key message display on top banner and banner type.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func showAlertViewControllerWith(title:String?, message:String, buttonTitle:String?, controller:UIViewController?){
        let banner = NotificationBanner(title: title!, subtitle: message, style: .success)
        
        banner.duration =   HelperConstant.NOTIFICATION_BANNER_MIN_DURATION
        banner.backgroundColor = UIColor(hexString: ColorCode.successBannerBGColor)
        
        if (title?.contains(LocalizationKeys.info.getLocalized()))! || (title?.lowercased().contains("oops"))!{
            banner.backgroundColor  =   UIColor(hexString: ColorCode.infoBannerBGColor)
        } else  if (title?.contains(LocalizationKeys.error.getLocalized()))! {
            banner.backgroundColor = UIColor(hexString: ColorCode.errorBannerBGColor)
        }
        
        banner.titleLabel?.textColor = UIColor(hexString: ColorCode.whiteColor)
        banner.subtitleLabel?.textColor = UIColor(hexString: ColorCode.whiteColor)
        banner.show()
    }
    
}

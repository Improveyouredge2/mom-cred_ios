//
//  SignUpModel.swift
//  LoginModule
//
//  Created by Apple on 17/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  SignUp Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class SignUpRequest: Mappable {
    
    var name        :String?
    var username    :String?
    var email       :String?
    var mobile_no   :String?
    var password    :String?
    var role_id     :String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init(name:String?, username:String?, email:String?, mobile_no:String?, password:String?, role_id:String?) {
        self.name       = name
        self.username   = username
        self.email      = email
        self.mobile_no  = mobile_no
        self.password   = password
        self.role_id    = role_id
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
       
        name        <- map["name"]
        username    <- map["username"]
        email       <- map["email"]
        mobile_no   <- map["mobile_no"]
        role_id     <- map["role_id"]
        password    <- map["password"]
        
    }
}

class InstructionalSignUpRequest: SignUpRequest {
    
    var question_1: String?
    var question_2: String?
    var question_3: String?
    var question_4: String?
    var question_5: String?
    
    init(name: String?, username: String?, email: String?, mobile_no: String?, password: String?, role_id: String?, question1: String?, question2: String?, question3: String?, question4: String?, question5: String?) {
        super.init(name: name, username: username, email: email, mobile_no: mobile_no, password: password, role_id: role_id)
        self.question_1 = question1
        self.question_2 = question2
        self.question_3 = question3
        self.question_4 = question4
        self.question_5 = question5
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        name        <- map["name"]
        username    <- map["username"]
        email       <- map["email"]
        mobile_no   <- map["mobile_no"]
        role_id     <- map["role_id"]
        password    <- map["password"]
        question_1  <- map["question_1"]
        question_2  <- map["question_2"]
        question_3  <- map["question_3"]
        question_4  <- map["question_4"]
        question_5  <- map["question_5"]
    }
}

/**
 *  SignUpRespone is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class SignUpResponse : APIResponse {
    
    var signUpResponseData:SignUpResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->SignUpResponse?{
        var signUpResponse:SignUpResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<SignUpResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    signUpResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<SignUpResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        signUpResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return signUpResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        signUpResponseData        <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class SignUpResponseData: Mappable{
        var user_id : String?
        var token : String?
        var first_name : String?
        var last_name : String?
        var mobile : String?
        var email : String?
        var profile_img : String?
        var dob : String?
        var gender : String?
        var address : String?
        var create_dt : String?
        var update_dt : String?
        var referral_code : String?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->SignUpResponseData?{
            var signUpResponseData:SignUpResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<SignUpResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        signUpResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<SignUpResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            signUpResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return signUpResponseData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            user_id             <- map["user_id"]
            token               <- map["token"]
            first_name          <- map["first_name"]
            last_name           <- map["last_name"]
            mobile              <- map["mobile"]
            email               <- map["email"]
            profile_img         <- map["profile_img"]
            dob                 <- map["dob"]
            gender              <- map["gender"]
            address             <- map["address"]
            create_dt           <- map["create_dt"]
            update_dt           <- map["update_dt"]
            referral_code       <- map["referral_code"]
            
        }
    }
}

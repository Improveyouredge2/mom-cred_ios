//
//  SocialModel.swift
//  LoginModule
//
//  Created by Apple on 06/02/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
class SocialModel{
    
    /**
     *  Social data model.
     *
     *  Fetch Social user data
     *
     *  @Developed By: Team Consagous
     */
    
    var firstName = String()
    var lastName = String()
    var email = String()
    var id = String()
    var name = String()
    var pictureUrl = String()
    var socialtype = String()
    
    
    init(firstName: String, lastName: String, email: String, id: String, name: String, pictureUrl: String, socialtype: String) {
    self.firstName = firstName
    self.lastName = lastName
    self.email = email
    self.id = id
    self.name = name
    self.pictureUrl = pictureUrl
    self.socialtype = socialtype
    }
    
}

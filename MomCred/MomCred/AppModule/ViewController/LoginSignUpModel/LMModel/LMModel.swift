
//
//  Model.swift
//  LoginModule
//
//  Created by Apple on 17/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  Login is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class LoginRequest: Mappable {
    
    var username           :String?
    var password        :String?
    var device_type     :String?

    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    init(username:String?, password:String?, device_type:String?) {
        self.username          = username
        self.password       = password
        self.device_type    = device_type
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        
    }
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func mapping(map: Map) {
        username                <- map["username"]
        password             <- map["password"]
        device_type          <- map["device_type"]
    }
}

/**
 *  LoginResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class LoginRespone : APIResponse {
    
    var loginResponseData:LoginResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->LoginRespone?{
        var loginResponse:LoginRespone?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<LoginRespone>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    loginResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<LoginRespone>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        loginResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return loginResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        loginResponseData        <- map["data"]
    }
    
    /**
     *  Sub-class LoginResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class LoginResponseData: Mappable{
        
        var authorization: String?
        var email: String?
        var mobile_no: String?
        var full_name: String?
        var profile_pic: String?
        var is_logged_in: String?
        var notification_status: String?
        var address: String?
        var dob: String?
        var role_id: String?
        var username:String?
        var membershipprice:String?
        var subscription:SubscriptionDetail?
        var stripefees:String?
        
        var user_id:String?
        var bank_details_added:Bool = false

        var plan_id:String?
        var account_detail:AccountDetailResponseData?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->LoginResponseData?{
            var loginResponseData:LoginResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<LoginResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        loginResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<LoginResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            loginResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return loginResponseData ?? nil
        }        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
           
            authorization       <- map["Authorization"]
            email               <- map["email"]
            mobile_no           <- map["mobile_no"]
            full_name           <- map["full_name"]
            profile_pic         <- map["profile_pic"]
            is_logged_in        <- map["is_logged_in"]
            notification_status <- map["notification_status"]
            address             <- map["address"]
            dob                 <- map["dob"]
            role_id             <- map["role_id"]
            username            <- map["username"]
            
            membershipprice     <- map["membershipprice"]
            subscription        <- map["subscription"]
            stripefees          <- map["stripefees"]
            plan_id             <- map["plan_id"]
            
            account_detail      <- map["account_detail"]
            user_id             <- map["user_id"]
            bank_details_added             <- map["bank_details_added"]

            
        }
    }
    
    /**
     *  Sub-class LoginResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class AccountDetailResponseData: Mappable{
        
        var accountnu: String?
        var email: String?
        var name: String?
        var routing: String?
        
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->AccountDetailResponseData?{
            var accountDetailResponseData:AccountDetailResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<AccountDetailResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        accountDetailResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<AccountDetailResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            accountDetailResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return accountDetailResponseData ?? nil
        }
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            accountnu   <- map["accountnu"]
            email       <- map["email"]
            name        <- map["name"]
            routing     <- map["routing"]
        }
    }
}

/**
 *  LoginResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class SubscriptionDetail : APIResponse {
    
    var sub_end:String?
    var plan_status:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->SubscriptionDetail?{
        var subscriptionDetail:SubscriptionDetail?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<SubscriptionDetail>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    subscriptionDetail = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<SubscriptionDetail>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        subscriptionDetail = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return subscriptionDetail ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        
        sub_end     <- map["sub_end"]
        plan_status <- map["plan_status"]
    }
}

//
//  PMSettingView.swift
//  ProfileLib
//
//  Created by Apple_iOS on 18/01/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit

class PMSettingView: LMBaseViewController {
    @IBOutlet weak var labelNotificationTitle:UILabel!
    @IBOutlet weak var labelLanguageTitle:UILabel!
    @IBOutlet weak var textFieldLanguage:UITextField!
    
    fileprivate var pickerArray:[String]!
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        //pickerArray = [HelperConstant.LanguageType.English.rawValue, HelperConstant.LanguageType.Spanish.rawValue, HelperConstant.LanguageType.Chinese.rawValue]
        
        pickerArray = [HelperConstant.LanguageType.English.rawValue]
        
        self.configLanguagePicker()
    }
}

//MARK: UITextFieldDelegate Method implementation
//MARK:-
extension PMSettingView: UITextFieldDelegate{
    
    /**
     *  Remove cursor from uitextfield
     *
     *  @param key textField object, range specify and string is input character used in textfield.
     *
     *  @return specify status of character replacement on textfield
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
//    override func caretRect(for position: UITextPosition); -&gt; CGRect {
//    return CGRect.zero
//    }
//    func caretRectForPositio
}

extension PMSettingView{
    
    /**
     *  Notification switch button change event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodNotificationChangeAction(_ sender:UISwitch){
        if(sender.isOn){
            //TODO: Replace with actual functionality
            Helper.sharedInstance.showNotificationBanner(title: "Notificaitons", message: "Push notification allow.", buttonTitle: nil, controller: nil)
        } else {
            //TODO: Replace with actual functionality
            Helper.sharedInstance.showNotificationBanner(title: "Notificaitons", message: "Push notification disable.", buttonTitle: nil, controller: nil)
        }
    }
}

extension PMSettingView:UIPickerViewDelegate, UIPickerViewDataSource{
    /**
     *  Configure language picker on textfield.
     *
     *  @param key empty.
     *
     *  @return empty
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func configLanguagePicker(){
        let picker: UIPickerView = UIPickerView.init()
        
        //set the pickers datasource and delegate
        picker.delegate = self;
        picker.dataSource = self;
        
        //Add the picker to the alert controller
        self.textFieldLanguage.inputView = picker
        picker.selectedRow(inComponent: 0)
        textFieldLanguage.text = pickerArray[0]
    }
    
    
    /**
     *  Specify number components in picker view.
     *
     *  @param key UIPickerView object.
     *
     *  @return empty
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    /**
     *  Set number rows in each component of picker view.
     *
     *  @param key UIPickerView object and number of row in component.
     *
     *  @return returns number of rows in each component
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return pickerArray.count
    }
    
    /**
     *  Update title on picker view.
     *
     *  @param key UIPickerView object, title for row in picker view, component index.
     *
     *  @return the title of each row in your picker ... In my case that will be the profile name or the username string
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    
    /**
     *  Update selection of picker view.
     *
     *  @param key UIPickerView object, selected rows and component index.
     *
     *  @return empty
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.textFieldLanguage.text = pickerArray[row]
        HelperConstant.DefaultLang = self.textFieldLanguage.text ?? " "
    }
}

//
//  MoreViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

enum MoreOptionIndex:Int{
    case Help
    case ShareApp
    case Login
    case SignUp
}

struct MyProfileMenuItem{
    var title:String?
    var imageIcon:UIImage?
    var isArrowHidden = false
    var isSwitchHidden = true
    var optionIndex:MoreOptionIndex = MoreOptionIndex.Help
    var controller:LMBaseViewController?
}


/**
 * MoreViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */

class MoreViewController: LMBaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var menuList:[MyProfileMenuItem] = []
    fileprivate var editProfileView:PMEditProfileView?
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset = UIEdgeInsets.zero
        
//        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
//        userImageView.layer.borderWidth = 2.0
//        userImageView.layer.borderColor =  UIColor.white.cgColor
//        userImageView.clipsToBounds = true
        
//        self.presenter.connectView(view: self)
        
        self.accountMenuList()
        
        //self.presenter.fetchData()
        
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        self.editProfileView = nil
//        logoutViewController = nil
//        updateProfile()
    }
    
    // Called when the view has been fully transitioned onto the screen. Default does nothing
    override func viewDidAppear(_ animated: Bool) {
        
    }
}

// MARK:- Action method implementation
extension MoreViewController{
    
    /**
     *  Back button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodProfileAction(_ sender: AnyObject) {
        
        if(self.editProfileView == nil){
            self.editProfileView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMEditProfileView.nameOfClass) as PMEditProfileView
            
            self.navigationController?.pushViewController(self.editProfileView!, animated: true)
        }
    }
}

//MARK:- Fileprivate method implementation
extension MoreViewController{
    
    /**
     *  Prepare user account setting menu option for user to select and update information.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func accountMenuList(){
        
        if(menuList.count > 0){
            menuList.removeAll()
        }
        self.loadMoreMenu()
        
        tableView.reloadData()
    }
    
    fileprivate func loadMoreMenu(){
        
        if PMUserDefault.getAppToken() == nil || PMUserDefault.getAppToken()?.trim().isEmpty == true {
            
            var menuItem = MyProfileMenuItem()
            menuItem.title = LocalizationKeys.sign_up.getLocalized()
            menuItem.optionIndex = .SignUp
            menuList.append(menuItem)
            
            menuItem = MyProfileMenuItem()
            menuItem.title = LocalizationKeys.login.getLocalized()
            menuItem.optionIndex = .Login
            menuList.append(menuItem)
        }

        var menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.additional_information.getLocalized()
        //menuItem.imageIcon = UIImage(named: "profile_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMWebView.nameOfClass) as PMWebView
        
        menuItem.controller?.downloadUrl =  APIKeys.API_ADDITIONAL_INFO
        menuItem.controller?.screenTitle = menuItem.title ?? ""
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.additional_service.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMWebView.nameOfClass) as PMWebView
        
        menuItem.controller?.screenTitle = menuItem.title ?? ""
        menuItem.controller?.downloadUrl =  APIKeys.API_ADDITIONAL_SERVICE
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.contact_us.getLocalized()
//        menuItem.imageIcon = UIImage(named: "key_gray")
//        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMContactUsView.nameOfClass) as PMContactUsView
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMWebView.nameOfClass) as PMWebView
        menuItem.controller?.screenTitle = menuItem.title ?? ""
        menuItem.controller?.downloadUrl =  APIKeys.API_CONTACT_US_WEB
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.help_support.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
//        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMContactUsView.nameOfClass) as PMContactUsView
        
       menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: HelpAndSupportVC.nameOfClass) as HelpAndSupportVC
        
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.privacy_policy_option.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMWebView.nameOfClass) as PMWebView
        
        menuItem.controller?.downloadUrl =  APIKeys.API_PRIVACY_POLICY
        menuItem.controller?.screenTitle = menuItem.title ?? ""
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.terms_condition.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMWebView.nameOfClass) as PMWebView
        
        menuItem.controller?.downloadUrl =  APIKeys.API_TERMS_CONDITION
        menuItem.controller?.screenTitle = menuItem.title ?? ""
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.article.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: ArticleListViewController.nameOfClass) as ArticleListViewController
        menuList.append(menuItem)
        
        /* Feedback is removed because we already show contact us page with contact us menu
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.feedback.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMContactUsView.nameOfClass) as PMContactUsView
        menuList.append(menuItem)
        */
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.share_app.getLocalized()
        menuItem.optionIndex = .ShareApp
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.settings.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMSettingView.nameOfClass) as PMSettingView
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.help.getLocalized()
        //        menuItem.imageIcon = UIImage(named: "key_gray")
        menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMWebView.nameOfClass) as PMWebView
        menuList.append(menuItem)
    }
    
    func shareApp(){
        let text = "Improve Your Edge, Easiest & Fastest"
        let trimmed = text.trimmingCharacters(in: .whitespaces)
        let textToShare = [trimmed]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        self.present(activityViewController, animated: true, completion: nil)
    }
}


// MARK:- Action method implementation
extension MoreViewController {
    func backButtonAction(_ sender:Any){
        _ = navigationController?.popViewController(animated: true)
    }
}

extension MoreViewController: UITableViewDelegate,UITableViewDataSource {
    
    /**
     *  Specify number of section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PMProfileViewCell.nameOfClass, for: indexPath) as! PMProfileViewCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.btnTextNImage.setTitle(menuList[indexPath.row].title, for: .normal)
        cell.btnTextNImage.setImage(menuList[indexPath.row].imageIcon ?? nil, for: UIControl.State.normal)
        cell.arrow.isHidden = menuList[indexPath.row].isArrowHidden
        return cell
    }
    
    /**
     *  Implement action on tapping tableview cell with UITableViewDataDelegate method implementation.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myAccountMenuItem = menuList[indexPath.row]
        
        if(myAccountMenuItem.controller != nil){
            self.navigationController?.pushViewController(myAccountMenuItem.controller!, animated: true)
        } else if myAccountMenuItem.optionIndex == .SignUp {
            // remove login screen
            PMUserDefault.clearAllData()
            Helper.sharedInstance.openSignUpViewController()
        } else if myAccountMenuItem.optionIndex == .Login {
            // remove login screen
            PMUserDefault.clearAllData()
            Helper.sharedInstance.userLogout()
        } else if(myAccountMenuItem.optionIndex == .ShareApp){
            self.shareApp()
        }
    }
}

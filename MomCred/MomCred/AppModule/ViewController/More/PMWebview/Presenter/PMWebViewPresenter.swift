//
//  PMWebViewPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 04/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMWebViewPresenter {
    
    var view:PMWebView! // Object of change password view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key change password view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PMWebView) {
        self.view = view
    }
    
    /**
     *  update user password information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getWebViewData(url:String){
        
       Spinner.show()
        
        PMWebViewService.webViewData(url:url, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    //self.view.prepareScreenInfo()
                    //
                    if(response?.webViewResponseData?.url != nil){
                        let url = URL (string: response?.webViewResponseData?.url ?? "")
                        let requestObj = URLRequest(url: url!)
                        self.view.webView.loadRequest(requestObj)
                    }
                    
                    Spinner.hide()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

//
//  WebViewController.swift
//  ProfileLib
//
//  Copyright © 2019 consagous. All rights reserved.
//

import Foundation
import UIKit

class PMWebView: LMBaseViewController{
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var labelScreenTitle: UILabel!
    
    // Class object of presenter class
    fileprivate let presenter = PMWebViewPresenter()
    
    var screenName:String = "" // screen name for displaying on header
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // TODO: remove hardcoded text
        
        self.presenter.connectView(view: self)
        
        if(self.screenTitle.length > 0){
            screenName = self.screenTitle
        } else{
            screenName = "Help"
        }

        if(self.browseUrl != nil){
            let url = URL (string: self.browseUrl ?? "")
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        } else if(self.downloadUrl != nil){
            presenter.getWebViewData(url: self.downloadUrl ?? "")
        } else {
            let url = URL (string: "https://www.google.com/")
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        }
        
        
        labelScreenTitle.text = screenName
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(self.browseUrl != nil){
            let url = URL (string: self.browseUrl ?? "")
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        } else if(self.downloadUrl != nil){
            presenter.getWebViewData(url: self.downloadUrl ?? "")
        } else {
            let url = URL (string: "https://www.google.com/")
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        }
    }
    
    // Called when the parent application receives a memory warning. On iOS 6.0 it will no longer clear the view by default.
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:- Action  method implementation
extension PMWebView{
    
    /**
     *  Back button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func backButtonAction(_ sender:Any){
        _ = navigationController?.popViewController(animated: true)
    }
}

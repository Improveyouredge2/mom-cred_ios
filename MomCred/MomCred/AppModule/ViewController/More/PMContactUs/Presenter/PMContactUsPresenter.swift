//
//  PMContactUsPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 04/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PMContactUsPresenter {
    
    var view:PMContactUsView! // Object of change password view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key change password view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PMContactUsView) {
        self.view = view
    }
    
    /**
     *  update user password information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getContactUsData(){
        
        Spinner.show()
        
        PMContactUsService.contactUsData(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    //self.view.prepareScreenInfo()
                    
                    self.view.lblCompanyAddress.text = response?.contactUsResponseData?.site_address ?? ""
                    self.view.lblPhoneNo.text = response?.contactUsResponseData?.mobile ?? ""
                    self.view.lblEmail1.text = response?.contactUsResponseData?.site_email1 ?? ""
                    self.view.lblEmail2.text = response?.contactUsResponseData?.site_email2 ?? ""
                    
                    Spinner.hide()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

//
//  PMContactUsService.swift
//  MomCred
//
//  Created by Apple_iOS on 04/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


class PMContactUsService{
    
    /**
     *  Method update new password on server.
     *
     *  @param key new password. Callback method to update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func contactUsData(callback:@escaping (_ status:Bool, _ response: ContactUsResponse?, _ message: String?) -> Void) {
        
        // POST
        let reqPost = APIManager().sendGetRequest(urlString: APIKeys.API_CONTACT_US, header: APIHeaders().getDefaultHeaders())
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:ContactUsResponse? = ContactUsResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
            
        }
        
    }
}

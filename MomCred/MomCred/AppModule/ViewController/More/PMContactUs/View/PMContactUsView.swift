//
//  ContactUsView.swift
//  ProfileLib
//
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit
import SafariServices
import SwiftEventBus
import AVFoundation

class PMContactUsView: LMBaseViewController {
    
    @IBOutlet weak var lblCompanyAddress : UILabel!
    @IBOutlet weak var lblPhoneNo : UILabel!
    @IBOutlet weak var lblEmail1 : UILabel!
    @IBOutlet weak var lblEmail2 : UILabel!
    
    // Class object of presenter class
    fileprivate let presenter = PMContactUsPresenter()
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter.connectView(view: self)
        
        self.presenter.getContactUsData()
        
        self.lblCompanyAddress.text = ""
        self.lblPhoneNo.text = ""
        self.lblEmail1.text = ""
        self.lblEmail2.text = ""
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SwiftEventBus.onMainThread(self, name: HelperConstant.LanguageChanged, handler: self.setupLanguage)
        self.setupLanguage(notification: nil)
        
        self.prepareScr()
    }
    
    // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        SwiftEventBus.unregister(self)
    }
}

extension PMContactUsView {
    
    /**
     *  Email button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnEmailAction(_ sender : UIButton){
        
        var email = self.lblEmail1.text?.trim()
        if(sender.tag == 2){
            email = self.lblEmail2.text?.trim()
        }
        
        if let url = URL(string: "mailto:\(email ?? "")") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    /**
     *  WebSite button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnWebSiteAction(_ sender : UIButton){
//        let svc = SFSafariViewController(url: URL(string: (self.lblWebsite.text?.trim())!)!)
//        self.present(svc, animated: true, completion: nil)
    }
    
    /**
     *  Call button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnCallAction(_ sender : UIButton){
        
        if let url = URL(string: "tel://\(self.lblPhoneNo.text?.trim() ?? "" )"), UIApplication.shared.canOpenURL(url) {
            if let url = URL(string: "tel://\(self.lblPhoneNo.text?.replacingOccurrences(of: " ", with: "") ?? "" )"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            else {
                print("Your device doesn't support this feature.")
            }
        }
    }
}

extension PMContactUsView {
    
    /**
     *  Update infromation on screen controls as per server response.
     *
     *  @param key empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func prepareScr(){
//        do{
//            if let fileData = PMFileUtils.getFileDataAtLocalPath(fileName: HelperConstant.StaticPage.ContactUs.StaticPageText) {
//                print(fileData)
//
//                let jsonResult: NSDictionary? = try JSONSerialization.jsonObject(with: fileData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
//
//                print(jsonResult)
//                if(jsonResult != nil){
//                    if let result  = jsonResult!["data"]{
//                        let resultDict = result as? NSDictionary
//                        if let email  =   resultDict!["site_email"] {
//                            self.lblEmail.text  =   email as? String
//                        }
//                        if let phone  =   resultDict!["site_phone"] {
//                            self.lblPhone.text  =   phone as? String
//                        }
//                        if let url  =   resultDict!["site_url"] {
//                            self.lblWebsite.text  =   url as? String
//                        }
//                    }
//
//                } else {
//                    //TODO: implement server api if required
////                    self.serverAPI()
//                }
//
//            } else {
//                //TODO: implement server api if required
////                self.serverAPI()
//            }
//
//        }catch {
//            print(error.localizedDescription)
//        }
    }
}
//MARK:- Set Up Localized Strings
//MARK:-
extension PMContactUsView {
    
    /**
     *  Update lingual information on screen controls.
     *
     *  @param key notification object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setupLanguage(notification:Notification?) -> Void{
        self.lbl_NavigationTitle.text = LocalizationKeys.hamburger_contact_us.getLocalized()
//        self.lblCallUs.text                 =   LocalizationKeys.call_us.getLocalized()
//        self.lblEmailUs.text                  =   LocalizationKeys.email_us.getLocalized()
//        self.lblWebSiteTitle.text            =   LocalizationKeys.website.getLocalized()
    }
    
}

//MARK:-
//MARK:- @end

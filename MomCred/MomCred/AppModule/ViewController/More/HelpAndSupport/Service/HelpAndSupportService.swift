//
//  HelpAndSupportService.swift
//  Facemed
//
//  Created by Apple on 21/05/19.
//  Copyright © 2019 Consagous Tect Pvt Ltd. All rights reserved.
//

import Foundation

class HelpAndSupportService {
    
    static func getData(callback:@escaping (_ status:Bool, _ response: HelpAndSupportResponse?, _ message: String?) -> Void) {
        
        //Get Header Parameters
        
        let reqPost = APIManager().sendGetRequest(urlString: APIKeys.API_HELP_SUPPORT, header: APIHeaders().getDefaultHeaders())
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:HelpAndSupportResponse? = HelpAndSupportResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
            
        }
    }
}

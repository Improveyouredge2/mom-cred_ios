//
//  HelpAndSupportModel.swift
//  Facemed
//
//  Created by Apple on 21/05/19.
//  Copyright © 2019 Consagous Tect Pvt Ltd. All rights reserved.
//

import ObjectMapper
import Foundation

class HelpAndSupportResponse : APIResponse {
    
    var helpAndSupportResponseData : [HelpAndSupportResponseData]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->HelpAndSupportResponse?{
        
        var helpAndSupportResponse : HelpAndSupportResponse?
        
        if jsonResponse is NSDictionary{
            
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<HelpAndSupportResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    helpAndSupportResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<HelpAndSupportResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        helpAndSupportResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return helpAndSupportResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        
        helpAndSupportResponseData    <- map["result"]
        
    }
    
    /**
     *  Sub-class OTPVerificationResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    class HelpAndSupportResponseData : Mappable {
        
        var question : String?
        var answer : String?
        
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN079]
         */
        
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN079]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)-> HelpAndSupportResponseData?{
            
            var helpAndSupportResponseData : HelpAndSupportResponseData?
            
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<HelpAndSupportResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        helpAndSupportResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<HelpAndSupportResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            helpAndSupportResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return helpAndSupportResponseData ?? nil
        }
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN079]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN079]
         */
        func mapping(map: Map){
            
            question                <- map["question"]
            answer                  <- map["answer"]
            
        }
    }
}


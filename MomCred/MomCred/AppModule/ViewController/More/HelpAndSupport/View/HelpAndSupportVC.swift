//
//  HelpAndSupportVC.swift
//  Facemed
//
//  Created by Apple on 10/05/19.
//  Copyright © 2019 Consagous Tect Pvt Ltd. All rights reserved.
//

import UIKit

class HelpAndSupportVC : LMBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var expandTableNumber = [Int] ()
    let presenter = HelpAndSupportPresenter()
    var helpArr = [HelpAndSupportResponse.HelpAndSupportResponseData]()
    
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dummyData()
        
        presenter.connectView(view: self)
    }
    
    func dummyData(){
        var helpOption = HelpAndSupportResponse.HelpAndSupportResponseData()
        helpOption.question = "Question1"
        helpOption.answer = "I am wondering is there any method to resize it first before it use to place in UIImageView? And if any tips for download an image to save memory usage, I'd like to hear that."
        helpArr.append(helpOption)
        
        helpOption = HelpAndSupportResponse.HelpAndSupportResponseData()
        helpOption.question = "Question2"
        helpOption.answer = "I am wondering is there any method to resize it first before it use to place in UIImageView? And if any tips for download an image to save memory usage, I'd like to hear that."
        helpArr.append(helpOption)
        
        helpOption = HelpAndSupportResponse.HelpAndSupportResponseData()
        helpOption.question = "Question3"
        helpOption.answer = "I am wondering is there any method to resize it first before it use to place in UIImageView? And if any tips for download an image to save memory usage, I'd like to hear that."
        helpArr.append(helpOption)
        
        helpOption = HelpAndSupportResponse.HelpAndSupportResponseData()
        helpOption.question = "Question4"
        helpOption.answer = "I am wondering is there any method to resize it first before it use to place in UIImageView? And if any tips for download an image to save memory usage, I'd like to hear that."
        helpArr.append(helpOption)
        
        helpOption = HelpAndSupportResponse.HelpAndSupportResponseData()
        helpOption.question = "Question5"
        helpOption.answer = "I am wondering is there any method to resize it first before it use to place in UIImageView? And if any tips for download an image to save memory usage, I'd like to hear that."
        helpArr.append(helpOption)
        
        helpOption = HelpAndSupportResponse.HelpAndSupportResponseData()
        helpOption.question = "Question6"
        helpOption.answer = "I am wondering is there any method to resize it first before it use to place in UIImageView? And if any tips for download an image to save memory usage, I'd like to hear that."
        helpArr.append(helpOption)
        
        helpOption = HelpAndSupportResponse.HelpAndSupportResponseData()
        helpOption.question = "Question7"
        helpOption.answer = "I am wondering is there any method to resize it first before it use to place in UIImageView? And if any tips for download an image to save memory usage, I'd like to hear that."
        helpArr.append(helpOption)
        
        helpOption = HelpAndSupportResponse.HelpAndSupportResponseData()
        helpOption.question = "Question8"
        helpOption.answer = "I am wondering is there any method to resize it first before it use to place in UIImageView? And if any tips for download an image to save memory usage, I'd like to hear that."
        helpArr.append(helpOption)
        
        helpOption = HelpAndSupportResponse.HelpAndSupportResponseData()
        helpOption.question = "Question9"
        helpOption.answer = "I am wondering is there any method to resize it first before it use to place in UIImageView? And if any tips for download an image to save memory usage, I'd like to hear that."
        helpArr.append(helpOption)
        
        helpOption = HelpAndSupportResponse.HelpAndSupportResponseData()
        helpOption.question = "Question10"
        helpOption.answer = "I am wondering is there any method to resize it first before it use to place in UIImageView? And if any tips for download an image to save memory usage, I'd like to hear that."
        helpArr.append(helpOption)
    }
    
    // Called when the view is about to made visible. Default does nothing

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.presenter.getHelpAndSupport()
    }
    
}

//MARK:- Set view Data
extension HelpAndSupportVC {
    
    func setData(response: [HelpAndSupportResponse.HelpAndSupportResponseData]){
        
        self.helpArr = response
        self.tableView.reloadData()
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate

extension HelpAndSupportVC : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return helpArr.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpSupportCell") as! HelpSupportCell
        
        let indexData = self.helpArr[indexPath.row]
        
        cell.lblTitle.text = indexData.question?.trim()
        
        cell.btnExpand.tag = indexPath.row
        cell.btnExpand.addTarget(self, action: #selector(expandButtonAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    @objc func expandButtonAction(sender: UIButton){
        
        self.tableView.beginUpdates()
        var indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.tableView.cellForRow(at: indexPath) as! HelpSupportCell
        
        if self.expandTableNumber.contains(sender.tag) {
            UIView.animate(withDuration: 0.5) {
                
                cell.lblSubTitle.text = ""
                
                if let index = self.expandTableNumber.index(of: indexPath.row) {
                    
                    if(self.expandTableNumber.count > 0){
                        self.expandTableNumber.remove(at: index)
                    }
                }
                cell.lblLine.backgroundColor = UIColor.white
//                cell.btnExpand.setImage(#imageLiteral(resourceName: "arrow_down"), for: .normal)
//                cell.imgArrow.image = #imageLiteral(resourceName: "arrow_down")
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow_next")
                
            }
        }
        else{
            
            UIView.animate(withDuration: 0.5) {
                
                let indexData = self.helpArr[indexPath.row]
                
                let strData = indexData.answer?.stripOutHtml()
                cell.lblSubTitle.text = strData?.trim()
                
                self.expandTableNumber.append(indexPath.row)
//                cell.btnExpand.setImage(UIImage.init(named: "arrow_next"), for: .normal)
                cell.imgArrow.image = #imageLiteral(resourceName: "arrow_down")
//                cell.imgArrow.image = #imageLiteral(resourceName: "arrow_next")
                
                cell.lblLine.backgroundColor = UIColor.lightGray
            }
        }
        self.tableView.endUpdates()
    }
    
}


class HelpSupportCell: UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblLine : UILabel!
    @IBOutlet weak var btnExpand : UIButton!
    @IBOutlet weak var imgArrow : UIImageView!
}

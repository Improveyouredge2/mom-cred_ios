//
//  HelpAndSupportPresenter.swift
//  Facemed
//
//  Created by Apple on 21/05/19.
//  Copyright © 2019 Consagous Tect Pvt Ltd. All rights reserved.
//

import UIKit

class HelpAndSupportPresenter {
    
    fileprivate var view : HelpAndSupportVC!
    
    /**
     *  Common view connection method.
     *
     *  @Use: attach HomeViewController view to the HomePresenter
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func connectView(view: HelpAndSupportVC) {
        self.view = view
    }
    
}

//**************************
// MARK:- Server Request
//*************************


extension HelpAndSupportPresenter {
    
    func getHelpAndSupport(){
        
        HelpAndSupportService.getData(callback: { (status, response, message) in
            
            if(status == true){
                
                OperationQueue.main.addOperation() {
                    self.view.setData(response: ((response?.helpAndSupportResponseData)!))
                }
                
            } else {
                OperationQueue.main.addOperation() {
                    Helper.sharedInstance.showAlertViewControllerWith(title: "Error", message: message! , buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

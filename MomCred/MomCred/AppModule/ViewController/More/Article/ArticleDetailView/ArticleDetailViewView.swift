//
//  ArticleView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class ArticleDetailViewView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var imgProfilePic : ImageLayerSetup!
    @IBOutlet weak var lblDesciption : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    
    fileprivate var menuArray: [HSMenu] = []
    
    var articleListData:ArticleListResponse.ArticleDataResponse?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let profileImageUrl = articleListData?.article_image {
            imgProfilePic.sd_setImage(with: URL(string: (profileImageUrl)), completed: nil)
        }
        
        lblTitle.text = articleListData?.title
        lblDesciption.text = articleListData?.description
        lblDate.text = articleListData?.created_date
    }
}

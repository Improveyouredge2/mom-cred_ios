//
//  ArticleListViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class ArticleListViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tbArticleList : UITableView!
    
    //MARK:- Var(s)
    var presenter       =       ArticleListViewPresenter()

    var currentPageIndex = 0
    var isAdvanceSearchResultShow = false
    
    var articleList:[ArticleListResponse.ArticleDataResponse]?
    
    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var isDisplayLoader = true
    
    fileprivate var menuArray: [HSMenu] = []
    
    fileprivate var articleDetailViewView:ArticleDetailViewView?
    var navTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)
        refreshView.delegate = self
        tbArticleList.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tbArticleList.addPullLoadableView(loadMoreView, type: .loadMore)
        tbArticleList.estimatedRowHeight = 100
        tbArticleList.rowHeight = UITableView.automaticDimension
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.add_new.getLocalized())
        menuArray = [menu1]
        
        if let navTitle = navTitle {
            lbl_NavigationTitle.text = navTitle
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.articleDetailViewView = nil
        
        presenter.getArticleList(callback: {
            (status, response, message) in
            
        })
    }
}

extension ArticleListViewController: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case .pulling:
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                presenter.getArticleList(callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            presenter.getArticleList(callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension ArticleListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleList != nil ? (articleList?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = articleList?[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleListViewTableViewCell.nameOfClass, for: indexPath) as! ArticleListViewTableViewCell
        
        //TODO: For testing
        if let profileImageUrl = data?.article_image, let url = URL(string: profileImageUrl) {
            cell.imgProfilePic.sd_setImage(with: url, completed: nil)
        }
        cell.lblDesciption.text = data?.description
        cell.lblTitle.text = data?.title
        cell.lblDate.text = data?.created_date

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = articleList?[indexPath.row]
        if(self.articleDetailViewView == nil && data != nil){
            self.articleDetailViewView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreStoryboard, viewControllerName: ArticleDetailViewView.nameOfClass) as ArticleDetailViewView
            
            self.articleDetailViewView?.articleListData = data
            self.navigationController?.pushViewController(self.articleDetailViewView!, animated: true)
        }
    }
}

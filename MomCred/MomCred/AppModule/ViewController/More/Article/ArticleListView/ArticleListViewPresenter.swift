//
//  ArticleListViewPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  ArticleListViewPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ArticleListViewPresenter {
    
    var view:ArticleListViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ArticleListViewController) {
        self.view = view
    }
}

extension ArticleListViewPresenter{
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getArticleList(callback:@escaping (_ status:Bool, _ response: ArticleListResponse?, _ message: String?) -> Void){
        
        let requestData = PaginationRequest()
        requestData.page_id = NSNumber(integerLiteral: self.view.currentPageIndex)
        
        ArticleListViewService.getArticleList(paginationRequest:requestData, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response != nil && response?.data != nil){
                        
                        if(self.view.currentPageIndex == 0){
                            self.view.articleList = []
                            self.view.articleList = response?.data
                        } else {
                            self.view.articleList?.append(contentsOf: response?.data ?? [])
                        }
                    }
                    
                    self.view.tbArticleList.reloadData()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
//                    self.view.updateScreenInfo()
                    callback(status, response, message)
                }
            }
        })
    }
}

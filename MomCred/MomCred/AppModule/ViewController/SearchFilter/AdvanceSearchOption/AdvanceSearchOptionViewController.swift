//
//  AdvanceSearchOptionViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class AdvanceSearchOptionViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- Var(s)
    //    var presenter       =       MyNotesPresenter()
    //    var arrNoteList :   [GetMyNotesResponse.GetMyNotesResponseData.GetMyNotesInfoData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.presenter.connectView(view: self)
    }
}


//MARK:- UITableView Datasource and Delegates
//MARK:-
extension AdvanceSearchOptionViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: SearchOptionCell.nameOfClass, for: indexPath) as! SearchOptionCell
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        //        let purchaseRefundView:PMPurchaseRefundView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMPurchaseRefundView.nameOfClass) as PMPurchaseRefundView
        //        self.navigationController?.pushViewController(purchaseRefundView, animated: true)
        
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        return UITableView.automaticDimension
        return 120
    }
    
    
}

class SearchOptionCell: UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblLine : UILabel!
}

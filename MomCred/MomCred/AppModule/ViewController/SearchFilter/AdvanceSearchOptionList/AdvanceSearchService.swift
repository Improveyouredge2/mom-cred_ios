//
//  AdvanceSearchService.swift
//  MomCred
//
//  Created by Rajesh Yadav on 19/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  AdvanceSearchService is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class AdvanceSearchService{
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverServiceProviderSearchListing(serviceProviderSearchRequest:ServiceProviderSearchRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceProviderResponseData?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = serviceProviderSearchRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_ADVANCE_SEARCH_SERVICE_PROVIDER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:DashboardAllServiceProviderResponseData? = DashboardAllServiceProviderResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }

    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverServiceSearchListing(serviceSearchRequest:ServiceSearchRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceResponseData?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = serviceSearchRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_ADVANCE_SEARCH_SERVICE, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:DashboardAllServiceResponseData? = DashboardAllServiceResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverPersonnelSearchListing(personnelSearchRequest:PersonnelSearchRequest?, callback:@escaping (_ status:Bool, _ response: PersonnelResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = personnelSearchRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_ADVANCE_SEARCH_PERSONAL, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:PersonnelResponse? = PersonnelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverFacilitySearchListing(facilitySearchRequest:FacilitySearchRequest?, callback:@escaping (_ status:Bool, _ response: MyFacilityResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = facilitySearchRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_ADVANCE_SEARCH_FACILITY, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:MyFacilityResponse? = MyFacilityResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverInsideLookSearchListing(insideLookSearchRequest:InsideLookSearchRequest?, callback:@escaping (_ status:Bool, _ response: ILCResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = insideLookSearchRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_ADVANCE_SEARCH_INSIDE_LOOK, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:ILCResponse? = ILCResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverIstructionalSearchListing(instructionalSearchRequest:InstructionalSearchRequest?, callback:@escaping (_ status:Bool, _ response: ICResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = instructionalSearchRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_ADVANCE_SEARCH_INSTRUCTIONAL, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:ICResponse? = ICResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

//
//  AdvanceSearchOptionListViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class AdvanceSearchOptionListViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnClose: UIButton!
    
    var menuList:[MyProfileMenuItem] = []
    
    var callbackListing:((_ serviceList:[ServiceAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceSearchRequest:ServiceSearchRequest?) -> Void?)?
    var callbackServiceProvider:((_ busiList:[BIAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceProviderSearchRequest:ServiceProviderSearchRequest?) -> Void?)?
    var callbackFacilityListing:((_ facilityList:[MyFacilityAddRequest]?, _ facilitySearchRequest: FacilitySearchRequest?) -> Void?)?
    var callbackPersonalListing:((_ personnelList:[PersonnelAddRequest]?, _ personnelSearchRequest :PersonnelSearchRequest?) -> Void?)?
    var callbackInsideLookListing:((_ insideLookList:[ILCAddRequest]?, _ insideLookSearchRequest: InsideLookSearchRequest?) -> Void?)?
    var callbackInstructionalListing:((_ instructionalList:[ICAddRequest]?, _ instructionalSearchRequest: InstructionalSearchRequest?) -> Void?)?
    
    var isServiceOnly = false
    
    fileprivate var isViewControllerPushed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.presenter.connectView(view: self)
        
        loadMoreMenu()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.isViewControllerPushed = false
    }
}

extension AdvanceSearchOptionListViewController{
    @IBAction func actionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AdvanceSearchOptionListViewController{
    
    fileprivate func loadMoreMenu(){
        
//        case service_provider
//        case individual_service
//        case calender_listing
//        case facility
//        case personnel
//        case inside_look_content
//        case instructional_content
        
        var menuItem = MyProfileMenuItem()
        if(!isServiceOnly){
            menuItem.title = LocalizationKeys.service_provider.getLocalized()
            
            let serviceProviderViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: ServiceProviderViewController.nameOfClass) as ServiceProviderViewController
            
            serviceProviderViewController.callbackServiceProvider = self.callbackServiceProvider
            menuItem.controller = serviceProviderViewController
            menuItem.controller?.screenTitle = menuItem.title ?? ""
            
            menuList.append(menuItem)
        }
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.individual_service.getLocalized()
        let controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: ServiceIndividualViewController.nameOfClass) as ServiceIndividualViewController
        controller.callbackListing = self.callbackListing
        menuItem.controller = controller
        menuItem.controller?.screenTitle = menuItem.title ?? ""
        menuList.append(menuItem)
        
        if(!isServiceOnly){
            
//            menuItem = MyProfileMenuItem()
//            menuItem.title = LocalizationKeys.calender_listing.getLocalized()
//            menuItem.controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: CalendarViewController.nameOfClass) as CalendarViewController
//            menuList.append(menuItem)
            
            menuItem = MyProfileMenuItem()
            menuItem.title = LocalizationKeys.facility.getLocalized()
            
            let facilityViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: FacilityViewController.nameOfClass) as FacilityViewController
            facilityViewController.callbackFacilityListing = self.callbackFacilityListing
            menuItem.controller = facilityViewController
            menuList.append(menuItem)
            
            menuItem = MyProfileMenuItem()
            menuItem.title = LocalizationKeys.personnel.getLocalized()
            
            let personnelViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: PersonnelViewController.nameOfClass) as PersonnelViewController
            
            personnelViewController.callbackListing = self.callbackPersonalListing
            
            menuItem.controller = personnelViewController
            menuItem.controller?.screenTitle = menuItem.title ?? ""
            menuList.append(menuItem)
            
            menuItem = MyProfileMenuItem()
            menuItem.title = LocalizationKeys.inside_look_content.getLocalized()
            
            let controller1 = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: InsideLookViewController.nameOfClass) as InsideLookViewController
            controller1.callbackListing = self.callbackInsideLookListing
            
            menuItem.controller = controller1
            menuItem.controller?.screenTitle = menuItem.title ?? ""
            menuList.append(menuItem)
            
            menuItem = MyProfileMenuItem()
            
            let instructionalViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: InstructionalViewController.nameOfClass) as InstructionalViewController
            instructionalViewController.callbackListing = self.callbackInstructionalListing
            menuItem.title = LocalizationKeys.instructional_content.getLocalized()
            menuItem.controller = instructionalViewController
            menuList.append(menuItem)
        }
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.non_instructional_local_business.getLocalized()
        let nilbVC: LocalBusinessSearchViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: LocalBusinessSearchViewController.nameOfClass)
        nilbVC.callbackServiceProvider = callbackServiceProvider
        menuItem.controller = nilbVC
        menuItem.controller?.screenTitle = menuItem.title ?? ""
        menuList.append(menuItem)
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension AdvanceSearchOptionListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: PMProfileViewCell.nameOfClass, for: indexPath) as! PMProfileViewCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.btnTextNImage.setTitle(menuList[indexPath.row].title, for: .normal)
        cell.btnTextNImage.setImage(menuList[indexPath.row].imageIcon ?? nil, for: UIControl.State.normal)
        cell.arrow.isHidden = menuList[indexPath.row].isArrowHidden
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(self.btnClose)){
            
            if(!self.isViewControllerExist()){
                self.dismiss(animated: false, completion: nil)
                
                let myAccountMenuItem = menuList[indexPath.row]
                
                if(myAccountMenuItem.controller != nil && !self.isViewControllerPushed){
                    self.isViewControllerPushed = true
                    HelperConstant.appDelegate.navigationController?.present(myAccountMenuItem.controller!, animated: true, completion: nil)
                }
            }
        }
    }
}

extension AdvanceSearchOptionListViewController{
    func isViewControllerExist() -> Bool{
        
        var isFound = false
        
        for controller in (HelperConstant.appDelegate.navigationController?.viewControllers)! {
            
            if (controller.nameOfClass == ServiceProviderViewController.nameOfClass){
                isFound = true
                break
            } else if (controller.nameOfClass == ServiceIndividualViewController.nameOfClass){
                isFound = true
                break
            } else if(controller.nameOfClass == CalendarViewController.nameOfClass){
                isFound = true
                break
            }
        }
        
        return isFound
    }
}

class SearchOptionListCell: UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblLine : UILabel!
}

//
//  FacilityViewController.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class FacilityViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
//    @IBOutlet weak var dropDownInstructionalUse : DropDown!
    @IBOutlet weak var dropDownFacilityType : DropDown!
//    @IBOutlet weak var dropDownServiceProviderClassification : DropDown!
//    @IBOutlet weak var dropDownLocationType : DropDown!
//    @IBOutlet weak var dropDownLocationOwnership : DropDown!
    
    @IBOutlet weak var dropDownInstructionalServiceTypeOption : DropDown!
//    @IBOutlet weak var dropDownFrontOfficeServiceTypeOption : DropDown!
    @IBOutlet weak var dropDownFieldCategory : DropDown!
    @IBOutlet weak var dropDownSpecificCategory : DropDown!
//    @IBOutlet weak var dropDownTargetGoal : DropDown!

    @IBOutlet weak var tableViewJob: UITableView!
    @IBOutlet weak var constraintTableViewJobHeight: NSLayoutConstraint!
    @IBOutlet weak var dropDownLocationCityZipType: DropDown!
    
    
    //MARK:- Var(s)
    
    // Location Select
    private var selectedLocationCity: Bool = true
    private var selectedCities: [CityModel]?
    private var selectedZipcode: String?
    fileprivate var locationsList: [(name: String, zipCode: String)] = []
    
    var fieldCategoryList:[ListingDataDetail] = []
    var specificCategoryList:[ListingDataDetail] = []
    var instructionalServiceTypesList:[ListingDataDetail] = []
    var frontOfficeServiceTypesList:[ListingDataDetail] = []
    var exceptional:[ListingDataDetail] = []
    var serviceProviderClassification:[ListingDataDetail] = []
    var presenter = FacilityPresenter()
    
    var isResultTypeListing = false
    var keywordSearch:String?
    
//    var callbackListing:((_ serviceList:[HomeServiceResponse]?) -> Void?)?
    var callbackFacilityListing:((_ facilityList:[MyFacilityAddRequest]?, _ facilitySearchRequest: FacilitySearchRequest?) -> Void?)?
    
    fileprivate var isDisplayProvider = true
    fileprivate var facilitySearchRequest = FacilitySearchRequest()
    fileprivate var locationTitleList:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)
        
        
        // Select Location
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownLocationCityZipType.optionArray = ["Town", "Zipcode"]
        dropDownLocationCityZipType.optionIds = [1, 2]
        dropDownLocationCityZipType.didSelect { [weak self] (selectedText , index ,id) in
            self?.selectedLocationCity = index == 0
            self?.tableViewJob.reloadData()
        }
        
//        self.facilitySearchRequest = FacilitySearchRequest()
        
        self.fieldCategoryList = DashboardPresenter.parentListingList?.parentListingData?.fields ?? []

        self.instructionalServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.services_types![1].subcat ?? [])

//        self.frontOfficeServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![1].subcat ?? [])

        self.exceptional = (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![1].subcat ?? [])

        self.serviceProviderClassification = (DashboardPresenter.parentListingList?.parentListingData?.service_provider![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.service_provider![1].subcat ?? [])

        // Instructional Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownInstructionalServiceTypeOption.optionArray = self.instructionalServiceTypesList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownInstructionalServiceTypeOption.optionIds = self.instructionalServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownInstructionalServiceTypeOption.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")

            self.facilitySearchRequest.facility_instruction = "\(id)"
        }
        ////////////////////////////////////////////////////////////

        // Front Office Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
//        dropDownFrontOfficeServiceTypeOption.optionArray = self.frontOfficeServiceTypesList.map{$0.listing_title ?? ""}
//
//        //Its Id Values and its optional
//        dropDownFrontOfficeServiceTypeOption.optionIds = self.frontOfficeServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}
//
//        // The the Closure returns Selected Index and String
//        dropDownFrontOfficeServiceTypeOption.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//
////            self.quickSearchRequest.serviceType = "\(id)"
//        }
        ////////////////////////////////////////////////////////////
        
        // Field Category
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownFieldCategory.optionArray = fieldCategoryList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownFieldCategory.optionIds = fieldCategoryList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownFieldCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.facilitySearchRequest.fieldcat = "\(id)"
            self.facilitySearchRequest.specificfield = ""
            self.presenter.serverChildListingRequest(parentId: "\(id)")
        }
        ////////////////////////////////////////////////////////////
        
        // Target Goal
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
//        dropDownTargetGoal.optionArray = DashboardPresenter.parentListingList?.parentListingData?.targetgoal!.map{$0.listing_title ?? ""} ?? []
//        //Its Id Values and its optional
//        dropDownTargetGoal.optionIds = DashboardPresenter.parentListingList?.parentListingData?.targetgoal!.map{Int($0.listing_id ?? "") ?? 0} ?? []
//
//        // The the Closure returns Selected Index and String
//        dropDownTargetGoal.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//            //            self.selectedMonth = selectedText
//            self.facilitySearchRequest?.credit_system = "\(id)"
//        }
        ////////////////////////////////////////////////////////////
        
        // Instructional Use
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
//        dropDownInstructionalUse.optionArray = ["Yes", "No"]
//        //Its Id Values and its optional
//        dropDownInstructionalUse.optionIds = [1,2]
//
//        // The the Closure returns Selected Index and String
//        dropDownInstructionalUse.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//            //            self.selectedMonth = selectedText
//            self.facilitySearchRequest.credit_system = "\(id)"
//        }
        ////////////////////////////////////////////////////////////
        
//
        // Facility Type
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownFacilityType.optionArray = DashboardPresenter.parentListingList?.parentListingData?.facilitytype!.map{$0.listing_title ?? ""} ?? []
        //Its Id Values and its optional
        dropDownFacilityType.optionIds = DashboardPresenter.parentListingList?.parentListingData?.facilitytype!.map{Int($0.listing_id ?? "") ?? 0} ?? []
        
        // The the Closure returns Selected Index and String
        dropDownFacilityType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.facilitySearchRequest.facility_type = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Servie Provider Classification
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
//        dropDownServiceProviderClassification.optionArray = self.serviceProviderClassification.map{$0.listing_title ?? ""}
//        //Its Id Values and its optional
//        dropDownServiceProviderClassification.optionIds = self.serviceProviderClassification.map{Int($0.listing_id ?? "0") ?? 0}
//
//        // The the Closure returns Selected Index and String
//        dropDownServiceProviderClassification.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//            //            self.selectedMonth = selectedText
//            self.quickSearchRequest.credit_system = "\(id)"
//        }
        ////////////////////////////////////////////////////////////
        
        // Location Type
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
//        dropDownLocationType.optionArray = [LocalizationKeys.public_str.getLocalized(), LocalizationKeys.private_str.getLocalized()]
//        //Its Id Values and its optional
//        dropDownLocationType.optionIds = [1,2]
//
//        // The the Closure returns Selected Index and String
//        dropDownLocationType.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//            //            self.selectedMonth = selectedText
//            self.quickSearchRequest.credit_system = "\(id)"
//        }
        ////////////////////////////////////////////////////////////
        
        // Location Ownership
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
//        dropDownLocationOwnership.optionArray = ["Yes", "No"]
//        //Its Id Values and its optional
//        dropDownLocationOwnership.optionIds = [1,2]
//
//        // The the Closure returns Selected Index and String
//        dropDownLocationOwnership.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//            //            self.selectedMonth = selectedText
//            self.quickSearchRequest.credit_system = "\(id)"
//        }
        ////////////////////////////////////////////////////////////
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.actionReset(UIButton())
    }
    
   
}

extension FacilityViewController {
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView:UITableView) {
        
        if(tableView == tableViewJob){
            tableViewJob.scrollToBottom()
            if locationsList.isEmpty {
                constraintTableViewJobHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                
                constraintTableViewJobHeight?.constant = tableViewJob.contentSize.height
            }
            
            updateTableViewHeight(tableView: tableView)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewJob == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewJob.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewJob.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewJobHeight.constant = heightOfTableView
            }
        }
    }
}

extension FacilityViewController{
    
    func updateSpecificFeildInfo(response:[ListingDataDetail]?){
        self.specificCategoryList = response ?? []
        self.updateServiceList()
    }
    
    func updateServiceList(){
        // Specific Category
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSpecificCategory.optionArray = specificCategoryList.map{$0.listing_title ?? ""}
        //Its Id Values and its optional
        dropDownSpecificCategory.optionIds = specificCategoryList.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownSpecificCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.facilitySearchRequest.specificfield = "\(id)"
        }
        ////////////////////////////////////////////////////////////
    }
}

extension FacilityViewController{
    @IBAction func actionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionGetResult(_ sender: UIButton) {
//        self.dismiss(animated: true, completion: nil)
        if dropDownLocationCityZipType.text?.count ?? 0 > 0 && (selectedCities?.count ?? 0 > 0 || selectedZipcode?.count ?? 0 > 0) {
            
            self.facilitySearchRequest.page_id = "0"
            
            if selectedLocationCity {
                facilitySearchRequest.select_location = "1"
                facilitySearchRequest.location_city = selectedCities?.compactMap { $0.name }
            } else {
                facilitySearchRequest.select_location = "2"
                facilitySearchRequest.zipcode = selectedZipcode
            }
            
            Spinner.show()
            self.presenter.serverFacilitySearchListing(facilitySearchRequest: self.facilitySearchRequest)
        }else{
            
            self.showBannerAlertWith(message:selectedLocationCity ?  LocalizationKeys.location_select.getLocalized() :LocalizationKeys.zipcode_enter.getLocalized() , alert: .error)

        }
        
       
        
    }
    
    @IBAction func actionReset(_ sender: UIButton) {
        self.dropDownFieldCategory.text = ""
        self.dropDownSpecificCategory.text = ""
//        self.dropDownFrontOfficeServiceTypeOption.text = ""
//        self.dropDownInstructionalUse.text = ""
        self.dropDownFacilityType.text = ""
//        self.dropDownServiceProviderClassification.text = ""
//        self.dropDownLocationType.text = ""
//        self.dropDownLocationOwnership.text = ""
        self.dropDownInstructionalServiceTypeOption.text = ""
//        self.dropDownTargetGoal.text = ""
        
        self.dropDownFieldCategory.selectedIndex = -1
        self.dropDownSpecificCategory.selectedIndex = -1
//        self.dropDownFrontOfficeServiceTypeOption.selectedIndex = -1
//        self.dropDownInstructionalUse.selectedIndex = -1
        self.dropDownFacilityType.selectedIndex = -1
//        self.dropDownServiceProviderClassification.selectedIndex = -1
//        self.dropDownLocationType.selectedIndex = -1
//        self.dropDownLocationOwnership.selectedIndex = -1
        self.dropDownInstructionalServiceTypeOption.selectedIndex = -1
//        self.dropDownTargetGoal.selectedIndex = -1
        
        self.facilitySearchRequest.page_id = ""
        self.facilitySearchRequest.specificfield = ""
        self.facilitySearchRequest.fieldcat = ""
        self.facilitySearchRequest.facility_instruction = ""
        self.facilitySearchRequest.facility_type = ""
        
        self.isDisplayProvider = true
    }
}

extension FacilityViewController{
    func showServiceListing(facilityListing:[MyFacilityAddRequest]?){
        self.dismiss(animated: true, completion: nil)
        if(self.callbackFacilityListing != nil){
            self.callbackFacilityListing!(facilityListing, self.facilitySearchRequest)
        }
    }
}



extension FacilityViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        if(refTableView == tableViewJob){
            if((self.locationTitleList.count) < HelperConstant.maximumSearchLocation){
                self.locationTitleList.append(text)
                self.tableViewJob.reloadData()
                
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension FacilityViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints(tableView: tableView)
        
        let cell: ProviderSelectCityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProviderSelectCityTableViewCell") as! ProviderSelectCityTableViewCell
        if selectedLocationCity {
            cell.selectedCities = selectedCities
            cell.cellType = .town
        } else {
            cell.selectedZipcode = selectedZipcode
            cell.cellType = .zipcode
        }
        cell.delegate = self
        return cell
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension FacilityViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        if(refTableView == tableViewJob){
            
            locationsList.remove(at: cellIndex?.row ?? 0)
            tableViewJob.reloadData()
        }
    }
}

extension FacilityViewController: ProviderSelectCityTableViewCellDelegate {
    func didSelectCities(_ cities: [CityModel]) {
        selectedCities = cities
    }
    
    func didEnterZipcode(_ zipcode: String?) {
        selectedZipcode = zipcode
    }
}


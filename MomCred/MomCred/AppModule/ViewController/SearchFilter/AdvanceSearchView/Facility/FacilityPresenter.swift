//
//  FacilityPresenter.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  FacilityPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class FacilityPresenter {
    
    var view:FacilityViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: FacilityViewController) {
        self.view = view
    }
}

extension FacilityPresenter {
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverChildListingRequest(parentId:String){
        
        let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        BIServiceStep1.getChildListing(requestData: childListingRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    self.view.updateSpecificFeildInfo(response: response?.dataList)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
    
    /**
     *  Get Search Facility listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverFacilitySearchListing(facilitySearchRequest:FacilitySearchRequest?){
        
        //let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        AdvanceSearchService.serverFacilitySearchListing(facilitySearchRequest: facilitySearchRequest, callback:{ (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                   if(response != nil && response?.data != nil){
                    self.view.showServiceListing(facilityListing: response?.data)
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

//
//  InsideLookViewController.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class InsideLookViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var dropDownType : DropDown!
    @IBOutlet weak var dropDownServiceType : DropDown!
//    @IBOutlet weak var dropDownStdExpectional : DropDown!
//    @IBOutlet weak var dropDownExceptionalOffer : DropDown!
//    @IBOutlet weak var dropDownTargetGoal : DropDown!
//    @IBOutlet weak var dropDownSpecialListingOptional : DropDown!
//    @IBOutlet weak var dropDownSkillLevel : DropDown!
    
    @IBOutlet weak var dropDownFieldCategory : DropDown!
    @IBOutlet weak var dropDownSpecificCategory : DropDown!
    
    @IBOutlet weak var tableViewJob: UITableView!
    @IBOutlet weak var constraintTableViewJobHeight: NSLayoutConstraint!
    @IBOutlet weak var dropDownLocationCityZipType: DropDown!

    
    //MARK:- Var(s)
    
    // Location Select
    private var selectedLocationCity: Bool = true
    private var selectedCities: [CityModel]?
    private var selectedZipcode: String?
    fileprivate var locationsList: [(name: String, zipCode: String)] = []
    
    var fieldCategoryList:[ListingDataDetail] = []
    var specificCategoryList:[ListingDataDetail] = []
    var instructionalServiceTypesList:[ListingDataDetail] = []
    var exceptional:[ListingDataDetail] = []
    var presenter = InsideLookPresenter()
    
    var isResultTypeListing = false
    var keywordSearch:String?
    
    
    var callbackListing:((_ insideLookList:[ILCAddRequest]?, _ insideLookSearchRequest: InsideLookSearchRequest?) -> Void?)?
    
    fileprivate var isDisplayProvider = true
    fileprivate var insideLookSearchRequest = InsideLookSearchRequest()
    fileprivate var locationTitleList:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)
        
        // Select Location
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownLocationCityZipType.optionArray = ["Town", "Zipcode"]
        dropDownLocationCityZipType.optionIds = [1, 2]
        dropDownLocationCityZipType.didSelect { [weak self] (selectedText , index ,id) in
            self?.selectedLocationCity = index == 0
            self?.tableViewJob.reloadData()
        }
        
//        self.insideLookSearchRequest = InsideLookSearchRequest()
        
        self.fieldCategoryList = DashboardPresenter.parentListingList?.parentListingData?.fields ?? []
        
        self.exceptional = (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![1].subcat ?? [])
        
        self.instructionalServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.services_types![1].subcat ?? [])

        // Purchase Option
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownType.optionArray = DashboardPresenter.parentListingList?.parentListingData?.insidelooktype!.map{$0.listing_title ?? ""} ?? []
        //Its Id Values and its optional
        dropDownType.optionIds = DashboardPresenter.parentListingList?.parentListingData?.insidelooktype!.map{Int($0.listing_id ?? "") ?? 0} ?? []

        // The the Closure returns Selected Index and String
        dropDownType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.insideLookSearchRequest.look_type = "\(id)"

        }
        ////////////////////////////////////////////////////////////
        
        // Instructional Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownServiceType.optionArray = self.instructionalServiceTypesList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownServiceType.optionIds = self.instructionalServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownServiceType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")

            self.insideLookSearchRequest.look_service_type = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Field Category
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownFieldCategory.optionArray = fieldCategoryList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownFieldCategory.optionIds = fieldCategoryList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownFieldCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.insideLookSearchRequest.fieldcat = "\(id)"
            self.insideLookSearchRequest.specificfield = ""
            self.presenter.serverChildListingRequest(parentId: "\(id)")
        }
        ////////////////////////////////////////////////////////////
        
//        // Standard Expectional
//        ////////////////////////////////////////////////////////////
//        // The list of array to display. Can be changed dynamically
//        // Array value listing
//        //dropDownStdExpectional.optionArray = self.exceptional.map{$0.listing_title ?? ""}
//
//        dropDownStdExpectional.optionArray = [LocalizationKeys.standard.getLocalized(), LocalizationKeys.exceptional.getLocalized(), LocalizationKeys.std_exp.getLocalized()]
//
//        //Its Id Values and its optional
//        //        dropDownStdExpectional.optionIds = self.exceptional.map{Int($0.listing_id ?? "0") ?? 0}
//        dropDownStdExpectional.optionIds = [1,2,3]
//
//        // The the Closure returns Selected Index and String
//        dropDownStdExpectional.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//            //            self.selectedMonth = selectedText
//            self.quickSearchRequest.service_provider_classification = "\(id)"
//        }
//        ////////////////////////////////////////////////////////////
//
//        // Exceptional Offer exceptional_classification
//        ////////////////////////////////////////////////////////////
//        // The list of array to display. Can be changed dynamically
//        dropDownExceptionalOffer.optionArray = self.exceptional.map{$0.listing_title ?? ""}
//
//        //Its Id Values and its optional
//        dropDownExceptionalOffer.optionIds = self.exceptional.map{Int($0.listing_id ?? "") ?? 0}
//
//        // The the Closure returns Selected Index and String
//        dropDownExceptionalOffer.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//            //            self.selectedMonth = selectedText
//            self.quickSearchRequest.credit_system = "\(id)"
//        }
//        ////////////////////////////////////////////////////////////
//
//        // Target Goal
//        ////////////////////////////////////////////////////////////
//        // The list of array to display. Can be changed dynamically
//        dropDownTargetGoal.optionArray = DashboardPresenter.parentListingList?.parentListingData?.targetgoal!.map{$0.listing_title ?? ""} ?? []
//        //Its Id Values and its optional
//        dropDownTargetGoal.optionIds = DashboardPresenter.parentListingList?.parentListingData?.targetgoal!.map{Int($0.listing_id ?? "") ?? 0} ?? []
//
//        // The the Closure returns Selected Index and String
//        dropDownTargetGoal.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//            //            self.selectedMonth = selectedText
//            self.quickSearchRequest.credit_system = "\(id)"
//        }
//        ////////////////////////////////////////////////////////////
//
//        // Special Listing Option
//        ////////////////////////////////////////////////////////////
//        // The list of array to display. Can be changed dynamically
//        dropDownSpecialListingOptional.optionArray = DashboardPresenter.parentListingList?.parentListingData?.speciallisting!.map{$0.listing_title ?? ""} ?? []
//        //Its Id Values and its optional
//        dropDownSpecialListingOptional.optionIds = DashboardPresenter.parentListingList?.parentListingData?.speciallisting!.map{Int($0.listing_id ?? "") ?? 0} ?? []
//
//        // The the Closure returns Selected Index and String
//        dropDownSpecialListingOptional.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//            //            self.selectedMonth = selectedText
//            self.quickSearchRequest.credit_system = "\(id)"
//        }
//        ////////////////////////////////////////////////////////////
//
//        // Skill Level Cost
//        ////////////////////////////////////////////////////////////
//        // The list of array to display. Can be changed dynamically
//        dropDownSkillLevel.optionArray = ["Beginner", "Intermediate","Expert"]
//        //Its Id Values and its optional
//        dropDownSkillLevel.optionIds = [1,2,3]
//
//        // The the Closure returns Selected Index and String
//        dropDownSkillLevel.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//            //            self.selectedMonth = selectedText
//            self.quickSearchRequest.credit_system = "\(id)"
//        }
//        ////////////////////////////////////////////////////////////
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.actionReset(UIButton())
    }
}

extension InsideLookViewController {
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView:UITableView) {
        
        if(tableView == tableViewJob){
            tableViewJob.scrollToBottom()
            if locationsList.isEmpty {
                constraintTableViewJobHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                
                constraintTableViewJobHeight?.constant = tableViewJob.contentSize.height
            }
            
            updateTableViewHeight(tableView: tableView)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewJob == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewJob.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewJob.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewJobHeight.constant = heightOfTableView
            }
        }
    }
}

extension InsideLookViewController{
    
    func updateSpecificFeildInfo(response:[ListingDataDetail]?){
        self.specificCategoryList = response ?? []
        self.updateServiceList()
    }
    
    func updateServiceList(){
        // Specific Category
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSpecificCategory.optionArray = specificCategoryList.map{$0.listing_title ?? ""}
        //Its Id Values and its optional
        dropDownSpecificCategory.optionIds = specificCategoryList.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownSpecificCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
                
            self.insideLookSearchRequest.specificfield = "\(id)"
        }
        ////////////////////////////////////////////////////////////
    }
}

extension InsideLookViewController{
    @IBAction func actionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionGetResult(_ sender: UIButton) {
        //        self.dismiss(animated: true, completion: nil)
        if dropDownLocationCityZipType.text?.count ?? 0 > 0 && (selectedCities?.count ?? 0 > 0 || selectedZipcode?.count ?? 0 > 0) {
            
            if selectedLocationCity {
                insideLookSearchRequest.select_location = "1"
                insideLookSearchRequest.location_city = selectedCities?.compactMap { $0.name }
            } else {
                insideLookSearchRequest.select_location = "2"
                insideLookSearchRequest.zipcode = selectedZipcode
            }
            
            Spinner.show()
            self.presenter.serverInsideLookSearchListing(insideLookSearchRequest: self.insideLookSearchRequest)
            
        }else {
            
            self.showBannerAlertWith(message:selectedLocationCity ?  LocalizationKeys.location_select.getLocalized() :LocalizationKeys.zipcode_enter.getLocalized() , alert: .error)

        }
        
      
        
    }
    
    @IBAction func actionReset(_ sender: UIButton) {
        self.dropDownFieldCategory.text = ""
        self.dropDownSpecificCategory.text = ""
        self.dropDownType.text = ""
        self.dropDownServiceType.text = ""
//        self.dropDownStdExpectional.text = ""
//        self.dropDownExceptionalOffer.text = ""
//        self.dropDownTargetGoal.text = ""
//        self.dropDownSpecialListingOptional.text = ""
//        self.dropDownSkillLevel.text = ""
        
        self.dropDownFieldCategory.selectedIndex = -1
        self.dropDownSpecificCategory.selectedIndex = -1
        self.dropDownType.selectedIndex = -1
        self.dropDownServiceType.selectedIndex = -1
//        self.dropDownStdExpectional.selectedIndex = -1
//        self.dropDownExceptionalOffer.selectedIndex = -1
//        self.dropDownTargetGoal.selectedIndex = -1
//        self.dropDownSpecialListingOptional.selectedIndex = -1
//        self.dropDownSkillLevel.selectedIndex = -1
        
        
        self.insideLookSearchRequest.page_id = ""
        self.insideLookSearchRequest.specificfield = ""
        self.insideLookSearchRequest.fieldcat = ""
        self.insideLookSearchRequest.look_service_type = ""
        self.insideLookSearchRequest.look_type = ""
        
        self.isDisplayProvider = true
    }
}

extension InsideLookViewController{
    func showInsideLookListing(insideLookList:[ILCAddRequest]?){
        self.dismiss(animated: true, completion: nil)
        if(self.callbackListing != nil){
            self.callbackListing!(insideLookList, self.insideLookSearchRequest)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension InsideLookViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints(tableView: tableView)
        
        let cell: ProviderSelectCityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProviderSelectCityTableViewCell") as! ProviderSelectCityTableViewCell
        if selectedLocationCity {
            cell.selectedCities = selectedCities
            cell.cellType = .town
        } else {
            cell.selectedZipcode = selectedZipcode
            cell.cellType = .zipcode
        }
        cell.delegate = self
        return cell
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension InsideLookViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        if(refTableView == tableViewJob){
            
            locationsList.remove(at: cellIndex?.row ?? 0)
            tableViewJob.reloadData()
        }
    }
}

extension InsideLookViewController: ProviderSelectCityTableViewCellDelegate {
    func didSelectCities(_ cities: [CityModel]) {
        selectedCities = cities
    }
    
    func didEnterZipcode(_ zipcode: String?) {
        selectedZipcode = zipcode
    }
}


//
//  ProviderSelectCityTableViewCellDelegate.swift
//  MomCred
//
//  Created by MD on 23/02/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

protocol ProviderSelectCityTableViewCellDelegate: class {
    func didSelectCities(_ cities: [CityModel])
    func didEnterZipcode(_ zipcode: String?)
}

enum ProviderSelectTableCellType: CustomStringConvertible {
    case town, zipcode
    
    var description: String {
        return self == .town ? "Location" : "Zipcode"
    }
}

class ProviderSelectCityTableViewCell : UITableViewCell {
    @IBOutlet private weak var lblLocation: UILabel! {
        didSet {
            lblLocation.text = cellType.description
        }
    }
    @IBOutlet private weak var txtCity: UITextField! {
        didSet {
            txtCity.text = ""
            txtCity.delegate = self
        }
    }
    weak var delegate: ProviderSelectCityTableViewCellDelegate?
    
    var selectedCities: [CityModel]? {
        didSet {
            if let selectedCities = selectedCities {
                txtCity.text = selectedCities.compactMap({ $0.name }).joined(separator: ", ")
            }
        }
    }
    var selectedZipcode: String? {
        didSet {
            if let selectedZipcode = selectedZipcode {
                txtCity.text = selectedZipcode
            }
        }
    }
    var cellType: ProviderSelectTableCellType = .town {
        didSet {
            lblLocation.text = cellType.description
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lblLocation.text = cellType.description
        txtCity.text = ""
    }
    
    func openCityPicker() {
        let picker: CitiesListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: CitiesListViewController.nameOfClass)
        picker.delegate = self
        picker.selectedCities = selectedCities
        (HelperConstant.appDelegate.window?.rootViewController as! UINavigationController).visibleViewController?.present(picker, animated: true, completion: nil)
    }
 }

extension ProviderSelectCityTableViewCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if cellType == .town {
            openCityPicker()
            return false
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.didEnterZipcode(textField.text)
        selectedZipcode = textField.text
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ProviderSelectCityTableViewCell: CitiesListViewControllerDelegate {
    func citiesListDidSelectDone(_ cities: [CityModel]) {
        selectedCities = cities
        delegate?.didSelectCities(cities)
    }
}

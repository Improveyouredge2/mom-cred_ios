//
//  CitiesListViewController.swift
//  MomCred
//
//  Created by MD on 22/02/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class CitiesTableViewCell: UITableViewCell {
    @IBOutlet private weak var lblCityName: UILabel! { didSet { lblCityName.text = "" } }
    @IBOutlet private weak var imgSelection: UIImageView! { didSet { imgSelection.image = UIImage(named: "unchecked_box") } }
    
    var model: CityModel? {
        didSet {
            guard let model = model else {
                reset()
                return
            }
            lblCityName.text = model.name
            imgSelection.image = model.selected ? UIImage(named: "checked_box") : UIImage(named: "unchecked_box")
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        reset()
    }
    
    private func reset() {
        lblCityName.text = ""
        imgSelection.image = UIImage(named: "unchecked_box")
    }
}

protocol CitiesListViewControllerDelegate: class {
    func citiesListDidSelectDone(_ cities: [CityModel])
}

class CitiesListViewController: LMBaseViewController {

    @IBOutlet private weak var tblView: UITableView! {
        didSet {
            tblView.delegate = self
            tblView.dataSource = self
            tblView.estimatedRowHeight = 80
            tblView.rowHeight = UITableView.automaticDimension
        }
    }
    private let presenter = CitiesListPresenter()
    
    weak var delegate: CitiesListViewControllerDelegate?
    
    var cityNames: [String] = []
    var selectedCities: [CityModel]? {
        didSet {
            if let selectedCities = selectedCities {
                let cityNs: [String] = selectedCities.compactMap { $0.name }
                cityNames.append(contentsOf: cityNs)
            }
        }
    }

    var cities: [CityModel]? {
        didSet {
            tblView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)
        presenter.getCitiesList()
    }
    
    @IBAction private func btnCloseAction(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction private func btnResetAction(sender: UIButton) {
        cities?.forEach { $0.selected = false }
    }
    
    @IBAction private func btnDoneAction(sender: UIButton) {
        let filtered: [CityModel]? = cities?.filter { $0.selected }
        if let filtered = filtered, !filtered.isEmpty {
            delegate?.citiesListDidSelectDone(filtered)
            dismiss(animated: true, completion: nil)
        } else {
            showBannerAlertWith(message: "Please select cities to continue.", alert: PMBannerAlertType.error)
        }
    }
}

extension CitiesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CitiesTableViewCell") as! CitiesTableViewCell
        cell.model = cities?[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
}

extension CitiesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = cities?[indexPath.row] {
            model.selected = !model.selected
            tblView.reloadData()
        }
    }
}

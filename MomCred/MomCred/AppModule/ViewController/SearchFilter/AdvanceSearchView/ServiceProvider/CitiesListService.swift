//
//  CitiesListService.swift
//  MomCred
//
//  Created by MD on 22/02/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation

class CitiesListService {
    class func getCities(callback: @escaping (_ status:Bool, _ response: CitiesResponse?, _ message: String?) -> Void) {
        let request = APIManager().sendGetRequest(urlString: APIKeys.API_GET_CITIES, header: APIHeaders().getDefaultHeaders())
        APIManager.request(urlRequest: request) { (status, response, error) in
            var citiesResponse: CitiesResponse?
            if let responseDict = response {
                citiesResponse = CitiesResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
            }
            let errorMessage: String? = citiesResponse?.message ?? error
            callback(status, citiesResponse, errorMessage)
        }
    }
}

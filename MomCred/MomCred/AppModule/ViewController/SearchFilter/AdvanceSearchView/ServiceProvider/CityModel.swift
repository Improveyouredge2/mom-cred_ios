//
//  CityModel.swift
//  MomCred
//
//  Created by MD on 22/02/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import ObjectMapper

class CityModel {
    var name: String?
    var selected: Bool = false
}

class CitiesResponse: APIResponse {
    var data: [[String: String]]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

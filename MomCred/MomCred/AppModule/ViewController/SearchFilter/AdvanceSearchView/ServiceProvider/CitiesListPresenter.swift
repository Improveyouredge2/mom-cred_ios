//
//  CitiesListPresenter.swift
//  MomCred
//
//  Created by MD on 22/02/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation

class CitiesListPresenter {
    weak var view: CitiesListViewController?
    
    func connectView(view: CitiesListViewController) {
        self.view = view
    }
    
    func getCitiesList() {
        CitiesListService.getCities() { (status, response, message) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if status {
                    let cities: [CityModel]? = response?.data?.compactMap { [weak self] dict -> CityModel? in
                        if let name = dict["location_city"], !name.trim().isEmpty {
                            let cityModel = CityModel()
                            cityModel.name = name
                            cityModel.selected = self?.view?.cityNames.contains(name) ?? false
                            return cityModel
                        }
                        return nil
                    }
                    if let cities = cities, !cities.isEmpty {
                        self?.view?.cities = cities
                    }
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        }
    }
}

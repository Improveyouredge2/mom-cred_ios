//
//  ServiceProviderViewController.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class ServiceProviderViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var lblPackageSearchInfo : UILabel!

    @IBOutlet weak var dropDownFieldCategory : DropDown!
    @IBOutlet weak var dropDownSpecificCategory : DropDown!
    @IBOutlet weak var dropDownInstructionalServiceTypeOption : DropDown!
    @IBOutlet weak var dropDownPurchaseOption : DropDown!
    @IBOutlet weak var dropDownStdExpectional : DropDown!
    
    @IBOutlet weak var dropDownOfferExceptional : DropDown!
    @IBOutlet weak var dropDownPrimarilyExceptional : DropDown!
    @IBOutlet weak var dropDownExceptionalOffer : DropDown!
    @IBOutlet weak var dropDownEventType: DropDown!
    @IBOutlet weak var dropDownServiceProvideClassificaiton : DropDown!
    @IBOutlet weak var dropDownBusinessTypeEntity : DropDown!
    @IBOutlet weak var dropDownExclusiveTravel : DropDown!
    @IBOutlet weak var dropDownLocationType : DropDown!
    @IBOutlet weak var dropDownLocationOwnership : DropDown!
    @IBOutlet weak var dropDownBusinessHours : DropDown!
    @IBOutlet weak var starRatingView : HCSStarRatingView!

    @IBOutlet weak var viewBusinessHr : UIView!
    @IBOutlet weak var inputOpenTime: RYFloatingInput!
    @IBOutlet weak var inputCloseTime: RYFloatingInput!
    
    fileprivate var pickerCloseTime: UIDatePicker?
    var openTime:String = ""
    var closeTime:String = ""

    @IBOutlet weak var tableViewJob: UITableView!
    @IBOutlet weak var dropDownLocationCityZipType: DropDown!
    @IBOutlet weak var constraintTableViewJobHeight: NSLayoutConstraint!
    
    //MARK:- Var(s)
    var fieldCategoryList:[ListingDataDetail] = []
    var specificCategoryList:[ListingDataDetail] = []
    var instructionalServiceTypesList:[ListingDataDetail] = []
    var frontOfficeServiceTypesList:[ListingDataDetail] = []
    var exceptional:[ListingDataDetail] = []
    var serviceProviderClassification:[ListingDataDetail] = []
    var presenter = ServiceProviderPresenter()
    
    var isResultTypeListing = false
    var keywordSearch:String?
    var isQuickSearch: Bool = false
    
    private var selectedLocationCity: Bool = true
    private var selectedCities: [CityModel]?
    private var selectedZipcode: String?
    var currentPage: Int = 1
    
    var callbackServiceProvider:((_ busiList:[BIAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceProviderSearchRequest:ServiceProviderSearchRequest?) -> Void?)?
    
    fileprivate var isDisplayProvider = true
    fileprivate var serviceProviderSearchRequest: ServiceProviderSearchRequest?
    fileprivate var locationsList: [(name: String, zipCode: String)] = []
    
    var packageSearchInfo: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        starRatingView.value = 0.0
        presenter.connectView(view: self)
        
        serviceProviderSearchRequest = ServiceProviderSearchRequest()
        serviceProviderSearchRequest?.initializeServiceProviderSearch()
        
        if isQuickSearch {
            serviceProviderSearchRequest?.page_title = "Quick"
        }
        if let packageSearchInfo = packageSearchInfo {
            lblPackageSearchInfo.text = packageSearchInfo
        } else {
            lblPackageSearchInfo.isHidden = true
        }
        
        fieldCategoryList = DashboardPresenter.parentListingList?.parentListingData?.fields ?? []
        
        instructionalServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.services_types![1].subcat ?? [])
        
//        frontOfficeServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![1].subcat ?? [])
        
        exceptional = (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![1].subcat ?? [])
        
        serviceProviderClassification = (DashboardPresenter.parentListingList?.parentListingData?.service_provider![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.service_provider![1].subcat ?? [])
        
        
        dropDownLocationCityZipType.optionArray = ["Town", "Zipcode"]
        dropDownLocationCityZipType.optionIds = [1, 2]
        dropDownLocationCityZipType.didSelect { [weak self] (selectedText , index ,id) in
            self?.selectedLocationCity = index == 0
            self?.tableViewJob.reloadData()
        }

        // Field Category
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownFieldCategory.optionArray = fieldCategoryList.map{$0.listing_title ?? ""}
        
        //Its Id Values and its optional
        dropDownFieldCategory.optionIds = fieldCategoryList.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownFieldCategory.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.specificfield = ""
            self?.presenter.serverChildListingRequest(parentId: "\(id)")
        }
        ////////////////////////////////////////////////////////////
        
        // Instructional Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownInstructionalServiceTypeOption.optionArray = instructionalServiceTypesList.map{$0.listing_title ?? ""}
        
        //Its Id Values and its optional
        dropDownInstructionalServiceTypeOption.optionIds = instructionalServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownInstructionalServiceTypeOption.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.instructional_service_type = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
//        // Front Office Service type
//        ////////////////////////////////////////////////////////////
//        // Array value listing
//        dropDownFrontOfficeServiceTypeOption.optionArray = frontOfficeServiceTypesList.map{$0.listing_title ?? ""}
//
//        //Its Id Values and its optional
//        dropDownFrontOfficeServiceTypeOption.optionIds = frontOfficeServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}
//
//        // The the Closure returns Selected Index and String
//        dropDownFrontOfficeServiceTypeOption.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//
//            serviceProviderSearchRequest?.f = "\(id)"
//        }
//        ////////////////////////////////////////////////////////////
        
        // Purchase Option
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownPurchaseOption.optionArray = ["Yes","No"]
        //Its Id Values and its optional
        dropDownPurchaseOption.optionIds = [1,2]
        
        // The the Closure returns Selected Index and String
        dropDownPurchaseOption.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.purchasability = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Standard Expectional
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        // Array value listing
        //dropDownStdExpectional.optionArray = exceptional.map{$0.listing_title ?? ""}
        
        dropDownStdExpectional.optionArray = [LocalizationKeys.standard.getLocalized(), LocalizationKeys.exceptional.getLocalized(), LocalizationKeys.std_exp.getLocalized()]
        
        //Its Id Values and its optional
//        dropDownStdExpectional.optionIds = exceptional.map{Int($0.listing_id ?? "0") ?? 0}
        dropDownStdExpectional.optionIds = [1,2,3]
        
        // The the Closure returns Selected Index and String
        dropDownStdExpectional.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.busi_service_provider_type = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Offer Exceptional
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownOfferExceptional.optionArray = ["Yes", "No"]
        //Its Id Values and its optional
        dropDownOfferExceptional.optionIds = [1,2]
        
        // The the Closure returns Selected Index and String
        dropDownOfferExceptional.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.exceptional_classification_offered = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Primarily Exceptional
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownPrimarilyExceptional.optionArray = ["Yes", "No"]
        //Its Id Values and its optional
        dropDownPrimarilyExceptional.optionIds = [1,2]
        
        // The the Closure returns Selected Index and String
        dropDownPrimarilyExceptional.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.primarily_exceptional = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Exceptional Offer exceptional_classification
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownExceptionalOffer.optionArray = DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification!.map{$0.listing_title ?? ""} ?? []
        
        //Its Id Values and its optional
        dropDownExceptionalOffer.optionIds = DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification!.map{Int($0.listing_id ?? "") ?? 0} ?? []
        
        // The the Closure returns Selected Index and String
        dropDownExceptionalOffer.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.exceptional_services = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        dropDownEventType.optionArray = ["Race", "Admission", "Tryouts"]
        dropDownEventType.optionIds = [1, 2, 3]
        dropDownEventType.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.event = "\(id)"
        }

        // Servie Provider Classification
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownServiceProvideClassificaiton.optionArray = serviceProviderClassification.map{$0.listing_title ?? ""}
        //Its Id Values and its optional
        dropDownServiceProvideClassificaiton.optionIds = serviceProviderClassification.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownServiceProvideClassificaiton.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.busi_service_provider_type = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Business Type Entity
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownBusinessTypeEntity.optionArray = DashboardPresenter.parentListingList?.parentListingData?.business_entity!.map{$0.listing_title ?? ""} ?? []
        //Its Id Values and its optional
        dropDownBusinessTypeEntity.optionIds = DashboardPresenter.parentListingList?.parentListingData?.business_entity!.map{Int($0.listing_id ?? "") ?? 0} ?? []
        
        // The the Closure returns Selected Index and String
        dropDownBusinessTypeEntity.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.busi_entity = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Exclusive Travel
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownExclusiveTravel.optionArray = ["Yes", "No"]
        //Its Id Values and its optional
        dropDownExclusiveTravel.optionIds = [1,2]
        
        // The the Closure returns Selected Index and String
        dropDownExclusiveTravel.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.exclusivetravel = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Location Type
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownLocationType.optionArray = [LocalizationKeys.public_str.getLocalized(), LocalizationKeys.private_str.getLocalized()]
        //Its Id Values and its optional
        dropDownLocationType.optionIds = [1,2]
        
        // The the Closure returns Selected Index and String
        dropDownLocationType.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.location_type = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Location Ownership
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownLocationOwnership.optionArray = ["Yes", "No"]
        //Its Id Values and its optional
        dropDownLocationOwnership.optionIds = [1,2]
        
        // The the Closure returns Selected Index and String
        dropDownLocationOwnership.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.location_ownership = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        viewBusinessHr.isHidden = true
        // Business Hours
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownBusinessHours.optionArray = HelperConstant.weekDayName
        //Its Id Values and its optional
        dropDownBusinessHours.optionIds = [1,2,3,4,5,6,7]
        
        // The the Closure returns Selected Index and String
        dropDownBusinessHours.didSelect { [weak self] (selectedText , index ,id) in
            self?.viewBusinessHr.isHidden = false
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.business_hours = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        inputOpenTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(openTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Open ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputCloseTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(closeTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Close ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        openDatePicker()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        actionReset(UIButton())
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView:UITableView) {
        
        if(tableView == tableViewJob){
            tableViewJob.scrollToBottom()
            if locationsList.isEmpty {
                constraintTableViewJobHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                
                constraintTableViewJobHeight?.constant = tableViewJob.contentSize.height
            }
            
            updateTableViewHeight(tableView: tableView)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewJob == tableView){
            
            UIView.animate(withDuration: 0, animations: { [weak self] in
                self?.tableViewJob.layoutIfNeeded()
            }, completion: { [weak self] (complete) in
                guard let self = self else { return }
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewJob.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewJobHeight.constant = heightOfTableView
            })
        }
    }
}

extension ServiceProviderViewController{
    
    fileprivate func openDatePicker(){
        
        // Time picker for Open time
        ////////////////////////////////////////////////////////
        var picker: UIDatePicker = UIDatePicker.init()
        
        // Set some of UIDatePicker properties
        picker.datePickerMode = .time
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }
        picker.timeZone = NSTimeZone.local
        picker.backgroundColor = UIColor.white
        
        // Add an event to call onDidChangeDate function when value is changed.
        picker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        
        inputOpenTime.input.inputView = picker
        ////////////////////////////////////////////////////////
        
        // Set picker for close time
        ////////////////////////////////////////////////////////
        picker = UIDatePicker.init()
        
        // Set some of UIDatePicker properties
        picker.datePickerMode = .time
        //        picker.maximumDate = Date()
        picker.timeZone = NSTimeZone.local
        picker.backgroundColor = UIColor.white
        
        // Add an event to call onDidChangeDate function when value is changed.
        picker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        
        inputCloseTime.input.inputView = picker
        pickerCloseTime = picker
        ////////////////////////////////////////////////////////
    }
    
    @objc fileprivate func dateValueChanged(_ sender: UIDatePicker){
        
        if(inputOpenTime.input.inputView == sender){
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            // Set date format
            dateFormatter.dateFormat = "hh:mm a"
            
            // Apply date format
            let selectedDate: String = dateFormatter.string(from: sender.date)
            
            pickerCloseTime?.minimumDate = sender.date
            
            inputOpenTime.input.text = selectedDate
            openTime = selectedDate
            
            serviceProviderSearchRequest?.hoursfrom = selectedDate
//            if(delegate != nil){
//                delegate?.updateTimeInfo(cellIndex: cellIndex, openTime: openTime, closeTime: closeTime)
//            }
            
        } else if(inputCloseTime.input.inputView == sender){
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            // Set date format
            dateFormatter.dateFormat = "hh:mm a"
            
            // Apply date format
            let selectedDate: String = dateFormatter.string(from: sender.date)
            
            inputCloseTime.input.text = selectedDate
            closeTime = selectedDate
            
            serviceProviderSearchRequest?.hoursto = selectedDate
//            if(delegate != nil){
//                delegate?.updateTimeInfo(cellIndex: cellIndex, openTime: openTime, closeTime: closeTime)
//            }
        }
        
    }
    
    func updateSpecificFeildInfo(response:[ListingDataDetail]?){
        specificCategoryList = response ?? []
        updateServiceList()
    }
    
    func updateServiceList(){
        // Specific Category
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSpecificCategory.optionArray = specificCategoryList.map{$0.listing_title ?? ""}
        //Its Id Values and its optional
        dropDownSpecificCategory.optionIds = specificCategoryList.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownSpecificCategory.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.specificfield = "\(id)"
        }
        ////////////////////////////////////////////////////////////
    }
}

extension ServiceProviderViewController{
    @IBAction func actionClose(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
//        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionGetResult(_ sender: UIButton) {
//        dismiss(animated: true, completion: nil)
        
//        serviceSearchRequest.duration = inputDurationTime.text() ?? ""
//        serviceSearchRequest.age = MinMax()
//        serviceSearchRequest.age?.min  = inputAgeMin.text() ?? ""
//        serviceSearchRequest.age?.max = inputAgeMax.text() ?? ""
//
//        serviceSearchRequest.price = MinMax()
//        serviceSearchRequest.price?.min = inputPriceMin.text() ?? ""
//        serviceSearchRequest.price?.max = inputPriceMax.text() ?? ""

        if dropDownLocationCityZipType.text?.count ?? 0 > 0 && (selectedCities?.count ?? 0 > 0 || selectedZipcode?.count ?? 0 > 0) {
            
            if selectedLocationCity {
                serviceProviderSearchRequest?.select_location = "1"
                serviceProviderSearchRequest?.location_city = selectedCities?.compactMap { $0.name }
            } else {
                serviceProviderSearchRequest?.select_location = "2"
                serviceProviderSearchRequest?.zipcode = selectedZipcode
            }
            serviceProviderSearchRequest?.rating = "\(starRatingView.value)"
            
            Spinner.show()
            presenter.serverSearchProvider(serviceProviderSearchRequest: serviceProviderSearchRequest)
            
        }else {
            
            self.showBannerAlertWith(message:selectedLocationCity ?  LocalizationKeys.location_select.getLocalized() :LocalizationKeys.zipcode_enter.getLocalized() , alert: .error)

            
        }
        
        
       
        
    }
    
    @IBAction func actionReset(_ sender: UIButton) {
        dropDownFieldCategory.text = ""
        dropDownSpecificCategory.text = ""
        dropDownInstructionalServiceTypeOption.text = ""
        dropDownPurchaseOption.text = ""
        dropDownStdExpectional.text = ""
        dropDownOfferExceptional.text = ""
        dropDownPrimarilyExceptional.text = ""
        dropDownExceptionalOffer.text = ""
        dropDownServiceProvideClassificaiton.text = ""
        dropDownBusinessTypeEntity.text = ""
        dropDownExclusiveTravel.text = ""
        dropDownLocationType.text = ""
        dropDownLocationOwnership.text = ""
        dropDownBusinessHours.text = ""
        
        dropDownFieldCategory.selectedIndex = -1
        dropDownSpecificCategory.selectedIndex = -1
        dropDownInstructionalServiceTypeOption.selectedIndex = -1
        dropDownPurchaseOption.selectedIndex = -1
        dropDownStdExpectional.selectedIndex = -1
        dropDownOfferExceptional.selectedIndex = -1
        dropDownPrimarilyExceptional.selectedIndex = -1
        dropDownExceptionalOffer.selectedIndex = -1
        dropDownServiceProvideClassificaiton.selectedIndex = -1
        dropDownBusinessTypeEntity.selectedIndex = -1
        dropDownExclusiveTravel.selectedIndex = -1
        dropDownLocationType.selectedIndex = -1
        dropDownLocationOwnership.selectedIndex = -1
        dropDownBusinessHours.selectedIndex = -1
        
        isDisplayProvider = true
        
        serviceProviderSearchRequest = ServiceProviderSearchRequest()
        serviceProviderSearchRequest?.initializeServiceProviderSearch()
        currentPage = 1
        if isQuickSearch {
            serviceProviderSearchRequest?.page_title = "Quick"
        }
        resignAllList()
    }
}

extension ServiceProviderViewController {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if(dropDownFieldCategory.isSelected){
            dropDownFieldCategory.touchAction()
        }
    }
    
    fileprivate func resignAllList(){
        
        if(dropDownFieldCategory.isSelected){
            dropDownFieldCategory.touchAction()
        }
        
        if(dropDownSpecificCategory.isSelected){
            dropDownSpecificCategory.touchAction()
        }
        
        if(dropDownPurchaseOption.isSelected){
            dropDownPurchaseOption.touchAction()
        }
        
        if(dropDownStdExpectional.isSelected){
            dropDownStdExpectional.touchAction()
        }
        
        if(dropDownOfferExceptional.isSelected){
            dropDownOfferExceptional.touchAction()
        }
        
        if(dropDownPrimarilyExceptional.isSelected){
            dropDownPrimarilyExceptional.touchAction()
        }
        
        if(dropDownExceptionalOffer.isSelected){
            dropDownExceptionalOffer.touchAction()
        }
        
        if(dropDownServiceProvideClassificaiton.isSelected){
            dropDownServiceProvideClassificaiton.touchAction()
        }
        
        if(dropDownBusinessTypeEntity.isSelected){
            dropDownBusinessTypeEntity.touchAction()
        }
        
        if(dropDownExclusiveTravel.isSelected){
            dropDownExclusiveTravel.touchAction()
        }
        
        if(dropDownLocationType.isSelected){
            dropDownLocationType.touchAction()
        }
        
        if(dropDownLocationOwnership.isSelected){
            dropDownLocationOwnership.touchAction()
        }
        
        if(dropDownBusinessHours.isSelected){
            dropDownBusinessHours.touchAction()
        }
        
    }
    
    func showServiceProvider(busiList:[BIAddRequest]?){
        
        dismiss(animated: false, completion: nil)
        if(callbackServiceProvider != nil){
            callbackServiceProvider!(busiList, nil, serviceProviderSearchRequest)
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceProviderViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        updateViewConstraints(tableView: tableView)
        let cell: ProviderSelectCityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProviderSelectCityTableViewCell") as! ProviderSelectCityTableViewCell
        if selectedLocationCity {
            cell.selectedCities = selectedCities
            cell.cellType = .town
        } else {
            cell.selectedZipcode = selectedZipcode
            cell.cellType = .zipcode
        }
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension ServiceProviderViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        if(refTableView == tableViewJob){
            if((locationsList.count) < HelperConstant.maximumSearchLocation){
                locationsList.append((text, zipcode))
                tableViewJob.reloadData()
                
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

extension ServiceProviderViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        if(refTableView == tableViewJob){
            
            locationsList.remove(at: cellIndex?.row ?? 0)
            tableViewJob.reloadData()
        }
    }
}

extension ServiceProviderViewController: ProviderSelectCityTableViewCellDelegate {
    func didSelectCities(_ cities: [CityModel]) {
        selectedCities = cities
    }
    
    func didEnterZipcode(_ zipcode: String?) {
        selectedZipcode = zipcode
    }
}

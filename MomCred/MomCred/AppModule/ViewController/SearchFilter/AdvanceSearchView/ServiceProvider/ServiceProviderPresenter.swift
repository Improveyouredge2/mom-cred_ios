//
//  ServiceProviderPresenter.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  QuickSearchPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceProviderPresenter {
    
    weak var view: ServiceProviderViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ServiceProviderViewController) {
        self.view = view
    }
}

extension ServiceProviderPresenter {
    func serverChildListingRequest(parentId: String) {
        let childListingRequest = ChildListingRequest(listing_id: parentId)
        BIServiceStep1.getChildListing(requestData: childListingRequest, callback: { (status, response, message) in
            DispatchQueue.main.async { [weak self] in
                    Spinner.hide()
                if status {
                    self?.view.updateSpecificFeildInfo(response: response?.dataList)
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
    
    func serverSearchProvider(serviceProviderSearchRequest: ServiceProviderSearchRequest?) {
        serviceProviderSearchRequest?.page_index = "\(view.currentPage)"
        
        let callback: (_ status:Bool, _ response: DashboardAllServiceProviderResponseData?, _ message: String?) -> Void = { (status, response, message) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if status {
                    self?.view.currentPage += 1
                    self?.view.showServiceProvider(busiList: response?.busi)
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        }
        
        if view.isQuickSearch {
            QuickSearchService.serverQuickSearchProvider(quickSearchRequest: serviceProviderSearchRequest, callback: callback)
        } else {
            AdvanceSearchService.serverServiceProviderSearchListing(serviceProviderSearchRequest: serviceProviderSearchRequest, callback: callback)
        }
    }
}

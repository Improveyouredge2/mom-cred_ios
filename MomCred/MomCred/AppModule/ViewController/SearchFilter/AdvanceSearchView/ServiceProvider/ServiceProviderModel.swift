//
//  ServiceProviderModel.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  ServiceSearchRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceProviderSearchRequest : Mappable {
    var page_index: String?
    var select_location: String?
    var location_city: [String]?
    var zipcode: String?
    var fieldcat: String?
    var specificfield: String?
    var instruction_service_type: String?
    var purchasability: String?
    var instructional_service_type: String?
    var busi_service_provider_type: String?
    var exceptional_services: String?
    var event: String?
    var exceptional_classification_offered: String?
    var primarily_exceptional: String?
    var busi_service_provider: String?
    var busi_entity: String?
    var exclusivetravel: String?
    var location_type: String?
    var location_ownership: String?
    var business_hours: String?
    var hoursfrom: String?
    var hoursto: String?
    var rating: String?
    var page_title: String?
    var quick_search: String?
    var busi_hours: String?
    
    required init() { }

    required init?(map: Map) { }

    func initializeServiceProviderSearch() {
        select_location = "1"
        location_city = [String]()
        zipcode = ""
        fieldcat = ""
        specificfield = ""
        instruction_service_type = ""
        purchasability = ""
        instructional_service_type = ""
        busi_service_provider_type = ""
        exceptional_services = ""
        event = ""
        exceptional_classification_offered = ""
        primarily_exceptional = ""
        busi_service_provider = ""
        busi_entity = ""
        exclusivetravel = ""
        location_type = ""
        location_ownership = ""
        business_hours = ""
        hoursfrom = ""
        hoursto = ""
        rating = ""
        page_title = "Advanced"
        quick_search = "Get Results"
        busi_hours = ""
    }

    func initializeLocalSearch() {
        hoursto = ""
        specificfield = ""
        busi_hours = ""
        hoursfrom = ""
        hoursto = ""
        busi_entity = ""
        location_city = [String]()
        select_location = "1"
    }
    
    func mapping(map: Map) {
        page_index <- map["page_index"]
        select_location <- map["select_location"]
        location_city <- map["location_city"]
        zipcode <- map["zipcode"]
        fieldcat <- map["fieldcat"]
        specificfield <- map["specificfield"]
        instruction_service_type <- map["instruction_service_type"]
        instructional_service_type <- map["instructional_service_type"]
        purchasability <- map["purchasability"]
        busi_service_provider_type <- map["busi_service_provider_type"]
        exceptional_services <- map["exceptional_services"]
        event <- map["event"]
        exceptional_classification_offered <- map["exceptional_classification_offered"]
        primarily_exceptional <- map["primarily_exceptional"]
        busi_service_provider <- map["busi_service_provider"]
        busi_entity <- map["busi_entity"]
        exclusivetravel <- map["exclusivetravel"]
        location_type <- map["location_type"]
        location_ownership <- map["location_ownership"]
        business_hours <- map["busi_hours"]
        hoursfrom <- map["hoursfrom"]
        hoursto <- map["hoursto"]
        rating <- map["rating"]
        busi_hours <- map["busi_hours"]
    }
}

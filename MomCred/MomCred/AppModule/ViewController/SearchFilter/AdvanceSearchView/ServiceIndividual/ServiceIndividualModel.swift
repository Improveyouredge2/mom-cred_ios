//
//  ServiceIndividualModel.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  ServiceSearchRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceSearchRequest : Mappable {
    
    var page_id:String?
    //var location_city: String?
    var select_location: String?
    var location_city: [String]?
    var zipcode: String?
    var purchasability: String?
    var fieldcat: String?
    var specificfield: String?
    var credit_system: String?
    var number_of_purchase: String?
    var instruction_service_type: String?
    var front_office_service: String?
    var facility_rental: String?
    var service_event: String?
    var service_information: String?
    var price: MinMax?
    var service_exceptional: String?
    var service_target_goal: String?
    var special_listing: String?
    var additionalcost: String?
    var duration: String?
    var durationtype: String?
    var skilllevel: String?
    var age: MinMax?
    var service_provider_classification: String?

    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        page_id                     <- map["page_id"]
        location_city               <- map["location_city"]
        select_location             <- map["select_location"]
        zipcode                     <- map["zipcode"]
        purchasability              <- map["purchasability"]
        fieldcat                    <- map["fieldcat"]
        specificfield               <- map["specificfield"]
        credit_system               <- map["credit_system"]
        number_of_purchase          <- map["number_of_purchase"]
        instruction_service_type    <- map["instruction_service_type"]
        front_office_service        <- map["front_office_service"]
        facility_rental             <- map["facility_rental"]
        service_event               <- map["service_event"]
        service_information         <- map["service_information"]
        price                       <- map["price"]
        service_exceptional         <- map["service_exceptional"]
        service_target_goal         <- map["service_target_goal"]
        special_listing             <- map["special_listing"]
        additionalcost              <- map["additionalcost"]
        duration                    <- map["duration"]
        durationtype                <- map["durationtype"]
        skilllevel                  <- map["skilllevel"]
        age                         <- map["age"]
        service_provider_classification <- map["service_provider_classification"]
    }
}

/**
 *  Age is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class MinMax : Mappable {
    
    var min: String?
    var max: String?
//{
//    ""min"": ""5"",
//    ""max"": ""60""
//    }
    
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        
        min <- map["min"]
        max <- map["max"]
        
    }
}

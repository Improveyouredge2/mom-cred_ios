//
//  ServiceIndividualViewController.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class ServiceIndividualViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    
    @IBOutlet weak var dropDownPurchaseOption : DropDown!
    @IBOutlet weak var dropDownCreditSystem : DropDown!
    @IBOutlet weak var dropDownInstructionalServiceTypeOption : DropDown!
    @IBOutlet weak var dropDownFrontOfficeServiceTypeOption : DropDown!
    @IBOutlet weak var dropDownEventType : DropDown!
    @IBOutlet weak var dropDownFieldCategory : DropDown!
    @IBOutlet weak var dropDownSpecificCategory : DropDown!
    @IBOutlet weak var dropDownStdExpectional : DropDown!
    @IBOutlet weak var dropDownExceptionalOffer : DropDown!
    @IBOutlet weak var dropDownTargetGoal : DropDown!
    @IBOutlet weak var dropDownSpecialListingOptional : DropDown!
    @IBOutlet weak var dropDownDurationType : DropDown!
    @IBOutlet weak var dropDownSkillLevel : DropDown!
    @IBOutlet weak var dropDownAdditional : DropDown!
    
    @IBOutlet weak var inputDurationTime: RYFloatingInput!
    @IBOutlet weak var inputAgeMin: RYFloatingInput!
    @IBOutlet weak var inputAgeMax: RYFloatingInput!
    @IBOutlet weak var inputPriceMin: RYFloatingInput!
    @IBOutlet weak var inputPriceMax: RYFloatingInput!
    
    var openTime:String = ""
    var closeTime:String = ""

    @IBOutlet weak var tableViewJob: UITableView!
    @IBOutlet weak var constraintTableViewJobHeight: NSLayoutConstraint!
    @IBOutlet weak var dropDownLocationCityZipType: DropDown!

    
    //MARK:- Var(s)
    var fieldCategoryList:[ListingDataDetail] = []
    var specificCategoryList:[ListingDataDetail] = []
    var instructionalServiceTypesList:[ListingDataDetail] = []
    var frontOfficeServiceTypesList:[ListingDataDetail] = []
    var exceptional:[ListingDataDetail] = []
    var presenter = ServiceIndividualPresenter()
    
    var isResultTypeListing = false
    var keywordSearch:String?
    
    
    private var selectedLocationCity: Bool = true
    private var selectedCities: [CityModel]?
    private var selectedZipcode: String?
    fileprivate var locationsList: [(name: String, zipCode: String)] = []

    
    var callbackListing:((_ serviceList:[ServiceAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceSearchRequest:ServiceSearchRequest?) -> Void?)?
    
    fileprivate var isDisplayProvider = true
    fileprivate var serviceSearchRequest = ServiceSearchRequest()
    fileprivate var locationTitleList:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)
        
    
        // Select Location
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownLocationCityZipType.optionArray = ["Town", "Zipcode"]
        dropDownLocationCityZipType.optionIds = [1, 2]
        dropDownLocationCityZipType.didSelect { [weak self] (selectedText , index ,id) in
            self?.selectedLocationCity = index == 0
            self?.tableViewJob.reloadData()
        }
        
        // Purchase Option
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownPurchaseOption.optionArray = ["Yes","No"]
        //Its Id Values and its optional
        dropDownPurchaseOption.optionIds = [1,2]

        // The the Closure returns Selected Index and String
        dropDownPurchaseOption.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.serviceSearchRequest.purchasability = "\(id)"

        }
        ////////////////////////////////////////////////////////////
        
        // Credit System
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownCreditSystem.optionArray = ["Yes", "No"]
        //Its Id Values and its optional
        dropDownCreditSystem.optionIds = [1,2]

        // The the Closure returns Selected Index and String
        dropDownCreditSystem.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.serviceSearchRequest.credit_system = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        self.fieldCategoryList = DashboardPresenter.parentListingList?.parentListingData?.fields ?? []

        self.instructionalServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![0].subcat ?? [])

        self.frontOfficeServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![1].subcat ?? [])

        self.exceptional = (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![1].subcat ?? [])

        // Instructional Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownInstructionalServiceTypeOption.optionArray = self.instructionalServiceTypesList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownInstructionalServiceTypeOption.optionIds = self.instructionalServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownInstructionalServiceTypeOption.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")

            self.serviceSearchRequest.instruction_service_type = "\(id)"
        }
        ////////////////////////////////////////////////////////////

        // Front Office Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownFrontOfficeServiceTypeOption.optionArray = self.frontOfficeServiceTypesList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownFrontOfficeServiceTypeOption.optionIds = self.frontOfficeServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownFrontOfficeServiceTypeOption.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")

            self.serviceSearchRequest.front_office_service = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Event Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownEventType.optionArray = self.frontOfficeServiceTypesList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownEventType.optionIds = self.frontOfficeServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownEventType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")

            self.serviceSearchRequest.service_event = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Limit 2 digit
        self.inputPriceMin.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(openTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Min ")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputPriceMin.input.keyboardType = UIKeyboardType.numberPad

        // Limit 8 digit
        self.inputPriceMax.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(closeTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Max ")
                .maxLength(HelperConstant.LIMIT_DURATION, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputPriceMax.input.keyboardType = UIKeyboardType.numberPad
        
        // Field Category
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownFieldCategory.optionArray = fieldCategoryList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownFieldCategory.optionIds = fieldCategoryList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownFieldCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.serviceSearchRequest.fieldcat = "\(id)"
            self.serviceSearchRequest.specificfield = ""
            self.presenter.serverChildListingRequest(parentId: "\(id)")
        }
        ////////////////////////////////////////////////////////////

        // Standard Expectional
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        // Array value listing
        //dropDownStdExpectional.optionArray = self.exceptional.map{$0.listing_title ?? ""}

        dropDownStdExpectional.optionArray = [LocalizationKeys.standard.getLocalized(), LocalizationKeys.exceptional.getLocalized(), LocalizationKeys.std_exp.getLocalized()]

        //Its Id Values and its optional
//        dropDownStdExpectional.optionIds = self.exceptional.map{Int($0.listing_id ?? "0") ?? 0}
        dropDownStdExpectional.optionIds = [1,2,3]

        // The the Closure returns Selected Index and String
        dropDownStdExpectional.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.serviceSearchRequest.service_provider_classification = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Limit 8 digit
        self.inputDurationTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(closeTime)
                .accentColor(.white)
                .warningColor(.white)
                .maxLength(HelperConstant.LIMIT_DURATION, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputDurationTime.input.keyboardType = UIKeyboardType.numberPad
        
        // Duration Type
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownDurationType.optionArray = HelperConstant.durationType
        //Its Id Values and its optional
        dropDownDurationType.optionIds = [1,2,3,4,5]

        // The the Closure returns Selected Index and String
        dropDownDurationType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.serviceSearchRequest.durationtype = "\(id)"
        }
        ////////////////////////////////////////////////////////////

        // Exceptional Offer exceptional_classification
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownExceptionalOffer.optionArray = self.exceptional.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownExceptionalOffer.optionIds = self.exceptional.map{Int($0.listing_id ?? "") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownExceptionalOffer.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.serviceSearchRequest.service_exceptional = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Target Goal
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownTargetGoal.optionArray = DashboardPresenter.parentListingList?.parentListingData?.targetgoal!.map{$0.listing_title ?? ""} ?? []
        //Its Id Values and its optional
        dropDownTargetGoal.optionIds = DashboardPresenter.parentListingList?.parentListingData?.targetgoal!.map{Int($0.listing_id ?? "") ?? 0} ?? []
        
        // The the Closure returns Selected Index and String
        dropDownTargetGoal.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.serviceSearchRequest.service_target_goal = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Special Listing Option
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSpecialListingOptional.optionArray = DashboardPresenter.parentListingList?.parentListingData?.speciallisting!.map{$0.listing_title ?? ""} ?? []
        //Its Id Values and its optional
        dropDownSpecialListingOptional.optionIds = DashboardPresenter.parentListingList?.parentListingData?.speciallisting!.map{Int($0.listing_id ?? "") ?? 0} ?? []
        
        // The the Closure returns Selected Index and String
        dropDownSpecialListingOptional.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.serviceSearchRequest.special_listing = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        inputAgeMin.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(openTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Min ")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputAgeMin.input.keyboardType = UIKeyboardType.numberPad

        inputAgeMax.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(closeTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Max ")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputAgeMax.input.keyboardType = UIKeyboardType.numberPad
        
        // Skill Level Cost
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSkillLevel.optionArray = ["Beginner", "Intermediate","Expert"]
        //Its Id Values and its optional
        dropDownSkillLevel.optionIds = [1,2,3]
        
        // The the Closure returns Selected Index and String
        dropDownSkillLevel.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.serviceSearchRequest.skilllevel = "\(id)"
        }
        ////////////////////////////////////////////////////////////

        // Additional Cost
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownAdditional.optionArray = ["Yes", "No"]
        //Its Id Values and its optional
        dropDownAdditional.optionIds = [1,2]
        
        // The the Closure returns Selected Index and String
        dropDownAdditional.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.serviceSearchRequest.additionalcost = "\(id)"
        }
        ////////////////////////////////////////////////////////////
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.actionReset(UIButton())
    }
    
    
}

extension ServiceIndividualViewController {
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView:UITableView) {
        
        if(tableView == tableViewJob){
            tableViewJob.scrollToBottom()
            if locationsList.isEmpty {
                constraintTableViewJobHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                
                constraintTableViewJobHeight?.constant = tableViewJob.contentSize.height
            }
            
            updateTableViewHeight(tableView: tableView)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewJob == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewJob.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewJob.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewJobHeight.constant = heightOfTableView
            }
        }
    }
}

extension ServiceIndividualViewController{
    
    func updateSpecificFeildInfo(response:[ListingDataDetail]?){
        self.specificCategoryList = response ?? []
        self.updateServiceList()
    }
    
    func updateServiceList(){
        // Specific Category
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSpecificCategory.optionArray = specificCategoryList.map{$0.listing_title ?? ""}
        //Its Id Values and its optional
        dropDownSpecificCategory.optionIds = specificCategoryList.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownSpecificCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.serviceSearchRequest.specificfield = "\(id)"
        }
        ////////////////////////////////////////////////////////////
    }
}

extension ServiceIndividualViewController{
    @IBAction func actionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionGetResult(_ sender: UIButton) {
        if dropDownLocationCityZipType.text?.count ?? 0 > 0 && (selectedCities?.count ?? 0 > 0 || selectedZipcode?.count ?? 0 > 0) {
            
            self.serviceSearchRequest.duration = self.inputDurationTime.text() ?? ""
            self.serviceSearchRequest.age = MinMax()
            self.serviceSearchRequest.age?.min  = self.inputAgeMin.text() ?? ""
            self.serviceSearchRequest.age?.max = self.inputAgeMax.text() ?? ""
            
            self.serviceSearchRequest.price = MinMax()
            self.serviceSearchRequest.price?.min = self.inputPriceMin.text() ?? ""
            self.serviceSearchRequest.price?.max = self.inputPriceMax.text() ?? ""
            
            if(self.locationTitleList.count > 0){
              //  self.serviceSearchRequest.location_city = self.locationTitleList[0]
            }
            
            if selectedLocationCity {
                serviceSearchRequest.select_location = "1"
                serviceSearchRequest.location_city = selectedCities?.compactMap { $0.name }
            } else {
                serviceSearchRequest.select_location = "2"
                serviceSearchRequest.zipcode = selectedZipcode
            }
            
            Spinner.show()
            self.presenter.serverSearchListing(serviceSearchRequest: self.serviceSearchRequest)
            
        }else {
            
            self.showBannerAlertWith(message:selectedLocationCity ?  LocalizationKeys.location_select.getLocalized() :LocalizationKeys.zipcode_enter.getLocalized() , alert: .error)

        }
      
    }
    
    @IBAction func actionReset(_ sender: UIButton) {
        self.dropDownFieldCategory.text = ""
        self.dropDownSpecificCategory.text = ""
        self.dropDownInstructionalServiceTypeOption.text = ""
        self.dropDownFrontOfficeServiceTypeOption.text = ""
        self.dropDownPurchaseOption.text = ""
        self.dropDownStdExpectional.text = ""
        self.dropDownCreditSystem.text = ""
        self.dropDownEventType.text = ""
        self.dropDownExceptionalOffer.text = ""
        self.dropDownTargetGoal.text = ""
        self.dropDownSpecialListingOptional.text = ""
        self.dropDownDurationType.text = ""
        self.dropDownSkillLevel.text = ""
        self.dropDownAdditional.text = ""
        
        self.dropDownFieldCategory.selectedIndex = -1
        self.dropDownSpecificCategory.selectedIndex = -1
        self.dropDownInstructionalServiceTypeOption.selectedIndex = -1
        self.dropDownFrontOfficeServiceTypeOption.selectedIndex = -1
        self.dropDownPurchaseOption.selectedIndex = -1
        self.dropDownStdExpectional.selectedIndex = -1
        self.dropDownCreditSystem.selectedIndex = -1
        self.dropDownEventType.selectedIndex = -1
        self.dropDownExceptionalOffer.selectedIndex = -1
        self.dropDownTargetGoal.selectedIndex = -1
        self.dropDownSpecialListingOptional.selectedIndex = -1
        self.dropDownDurationType.selectedIndex = -1
        self.dropDownSkillLevel.selectedIndex = -1
        self.dropDownAdditional.selectedIndex = -1
        
        self.inputDurationTime.input.text = ""
        self.inputAgeMin.input.text = ""
        self.inputAgeMax.input.text = ""
        self.inputPriceMin.input.text = ""
        self.inputPriceMax.input.text = ""
        
        //self.serviceSearchRequest.location_city  = ""
        self.serviceSearchRequest.location_city  = []
        self.serviceSearchRequest.purchasability  = ""
        self.serviceSearchRequest.fieldcat  = ""
        self.serviceSearchRequest.specificfield  = ""
        self.serviceSearchRequest.credit_system  = ""
        self.serviceSearchRequest.number_of_purchase  = ""
        self.serviceSearchRequest.instruction_service_type  = ""
        self.serviceSearchRequest.front_office_service  = ""
        self.serviceSearchRequest.facility_rental  = ""
        self.serviceSearchRequest.service_event  = ""
        self.serviceSearchRequest.service_information  = ""
        self.serviceSearchRequest.price  = nil
        self.serviceSearchRequest.service_exceptional  = ""
        self.serviceSearchRequest.service_target_goal  = ""
        self.serviceSearchRequest.special_listing  = ""
        self.serviceSearchRequest.additionalcost  = ""
        self.serviceSearchRequest.duration  = ""
        self.serviceSearchRequest.durationtype  = ""
        self.serviceSearchRequest.skilllevel  = ""
        self.serviceSearchRequest.age  = nil
        
        self.isDisplayProvider = true
    }
}

extension ServiceIndividualViewController{
    func showServiceListing(service:[ServiceAddRequest]?){
        self.dismiss(animated: true, completion: nil)
        if(self.callbackListing != nil){
            self.callbackListing!(service, nil, self.serviceSearchRequest)
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ServiceIndividualViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints(tableView: tableView)
        
        let cell: ProviderSelectCityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProviderSelectCityTableViewCell") as! ProviderSelectCityTableViewCell
        if selectedLocationCity {
            cell.selectedCities = selectedCities
            cell.cellType = .town
        } else {
            cell.selectedZipcode = selectedZipcode
            cell.cellType = .zipcode
        }
        cell.delegate = self
        return cell
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ServiceIndividualViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        if(refTableView == tableViewJob){
            
            locationsList.remove(at: cellIndex?.row ?? 0)
            tableViewJob.reloadData()
        }
    }
}

extension ServiceIndividualViewController: ProviderSelectCityTableViewCellDelegate {
    func didSelectCities(_ cities: [CityModel]) {
        selectedCities = cities
    }
    
    func didEnterZipcode(_ zipcode: String?) {
        selectedZipcode = zipcode
    }
}



extension ServiceIndividualViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        if(refTableView == tableViewJob){
            if((locationsList.count) < HelperConstant.maximumSearchLocation){
                locationsList.append((text, zipcode))
                tableViewJob.reloadData()
                
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}


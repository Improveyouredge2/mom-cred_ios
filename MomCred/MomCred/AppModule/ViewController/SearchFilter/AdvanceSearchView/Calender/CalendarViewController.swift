//
//  CalendarViewController.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class CalendarViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    
    @IBOutlet weak var inputOpenTime: RYFloatingInput!
    @IBOutlet weak var inputCloseTime: RYFloatingInput!
    @IBOutlet weak var inputSpot: RYFloatingInput!
    @IBOutlet weak var dropDownPurchaseOption : DropDown!
    @IBOutlet weak var dropDownInstructionalServiceTypeOption : DropDown!
    @IBOutlet weak var dropDownFrontOfficeServiceTypeOption : DropDown!
    @IBOutlet weak var dropDownEventType : DropDown!
    @IBOutlet weak var inputPriceMin: RYFloatingInput!
    @IBOutlet weak var inputPriceMax: RYFloatingInput!
    @IBOutlet weak var dropDownFieldCategory : DropDown!
    @IBOutlet weak var dropDownSpecificCategory : DropDown!
    @IBOutlet weak var dropDownStdExpectional : DropDown!
    @IBOutlet weak var dropDownExceptionalOffer : DropDown!
    @IBOutlet weak var dropDownTargetGoal : DropDown!
    @IBOutlet weak var dropDownSpecialListingOptional : DropDown!
    @IBOutlet weak var inputDurationTime: RYFloatingInput!
    @IBOutlet weak var dropDownDurationType : DropDown!
    @IBOutlet weak var inputAgeMin: RYFloatingInput!
    @IBOutlet weak var inputAgeMax: RYFloatingInput!
    @IBOutlet weak var dropDownSkillLevel : DropDown!
    @IBOutlet weak var dropDownAdditional : DropDown!
    
    fileprivate var pickerCloseTime: UIDatePicker?
    var openTime:String = ""
    var closeTime:String = ""

    @IBOutlet weak var tableViewJob: UITableView!
    @IBOutlet weak var constraintTableViewJobHeight: NSLayoutConstraint!
    
    //MARK:- Var(s)
    var fieldCategoryList:[ListingDataDetail] = []
    var specificCategoryList:[ListingDataDetail] = []
    var instructionalServiceTypesList:[ListingDataDetail] = []
    var frontOfficeServiceTypesList:[ListingDataDetail] = []
    var exceptional:[ListingDataDetail] = []
    var presenter = CalendarPresenter()
    
    var isResultTypeListing = false
    var keywordSearch:String?
    
    var callbackListing:((_ serviceList:[ServiceAddRequest]?) -> Void?)?
    var callbackServiceProvider:((_ busiList:[BIAddRequest]?, _ serviceProviderSearchRequest: ServiceProviderSearchRequest?) -> Void?)?
    
    fileprivate var isDisplayProvider = true
    fileprivate var quickSearchRequest = QuickSearchRequest()
    fileprivate var serviceProviderSearchRequest = ServiceProviderSearchRequest()
    fileprivate var locationTitleList:[String] = []
    
    var currentPage: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)
        
        self.fieldCategoryList = DashboardPresenter.parentListingList?.parentListingData?.fields ?? []

        self.instructionalServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![0].subcat ?? [])

        self.frontOfficeServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![1].subcat ?? [])

        self.exceptional = (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![1].subcat ?? [])
        
        inputOpenTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(openTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Open ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputCloseTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(closeTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Close ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        self.openDatePicker()
        
        inputSpot.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Spots Available ")
                .maxLength(HelperConstant.LIMIT_DURATION, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputPriceMin.input.keyboardType = UIKeyboardType.numberPad
        
        // Purchase Option
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownPurchaseOption.optionArray = ["Yes","No"]
        //Its Id Values and its optional
        dropDownPurchaseOption.optionIds = [1,2]

        // The the Closure returns Selected Index and String
        dropDownPurchaseOption.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.quickSearchRequest.purchasablity = "\(id)"

        }
        ////////////////////////////////////////////////////////////
        

        // Instructional Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownInstructionalServiceTypeOption.optionArray = self.instructionalServiceTypesList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownInstructionalServiceTypeOption.optionIds = self.instructionalServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownInstructionalServiceTypeOption.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")

            self.quickSearchRequest.instruction_service_type = "\(id)"
        }
        ////////////////////////////////////////////////////////////

        // Front Office Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownFrontOfficeServiceTypeOption.optionArray = self.frontOfficeServiceTypesList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownFrontOfficeServiceTypeOption.optionIds = self.frontOfficeServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownFrontOfficeServiceTypeOption.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")

            self.quickSearchRequest.instruction_service_type = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Event Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownEventType.optionArray = self.frontOfficeServiceTypesList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownEventType.optionIds = self.frontOfficeServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownEventType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")

            self.quickSearchRequest.instruction_service_type = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Limit 2 digit
        self.inputPriceMin.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Min ")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputPriceMin.input.keyboardType = UIKeyboardType.numberPad

        // Limit 8 digit
        self.inputPriceMax.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Max ")
                .maxLength(HelperConstant.LIMIT_DURATION, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputPriceMax.input.keyboardType = UIKeyboardType.numberPad
        
        // Field Category
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownFieldCategory.optionArray = fieldCategoryList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownFieldCategory.optionIds = fieldCategoryList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownFieldCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.quickSearchRequest.specificField = ""
            self.presenter.serverChildListingRequest(parentId: "\(id)")
        }
        ////////////////////////////////////////////////////////////

        // Standard Expectional
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        // Array value listing
        //dropDownStdExpectional.optionArray = self.exceptional.map{$0.listing_title ?? ""}

        dropDownStdExpectional.optionArray = [LocalizationKeys.standard.getLocalized(), LocalizationKeys.exceptional.getLocalized(), LocalizationKeys.std_exp.getLocalized()]

        //Its Id Values and its optional
//        dropDownStdExpectional.optionIds = self.exceptional.map{Int($0.listing_id ?? "0") ?? 0}
        dropDownStdExpectional.optionIds = [1,2,3]

        // The the Closure returns Selected Index and String
        dropDownStdExpectional.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.quickSearchRequest.service_provider_classification = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Limit 8 digit
        self.inputDurationTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(closeTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Duration Time ")
                .maxLength(HelperConstant.LIMIT_DURATION, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputDurationTime.input.keyboardType = UIKeyboardType.numberPad
        
        // Duration Type
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownDurationType.optionArray = HelperConstant.durationType
        //Its Id Values and its optional
        dropDownDurationType.optionIds = [1,2,3,4,5]

        // The the Closure returns Selected Index and String
        dropDownDurationType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.quickSearchRequest.credit_system = "\(id)"
        }
        ////////////////////////////////////////////////////////////

        // Exceptional Offer exceptional_classification
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownExceptionalOffer.optionArray = self.exceptional.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownExceptionalOffer.optionIds = self.exceptional.map{Int($0.listing_id ?? "") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownExceptionalOffer.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.quickSearchRequest.credit_system = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Target Goal
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownTargetGoal.optionArray = DashboardPresenter.parentListingList?.parentListingData?.targetgoal!.map{$0.listing_title ?? ""} ?? []
        //Its Id Values and its optional
        dropDownTargetGoal.optionIds = DashboardPresenter.parentListingList?.parentListingData?.targetgoal!.map{Int($0.listing_id ?? "") ?? 0} ?? []
        
        // The the Closure returns Selected Index and String
        dropDownTargetGoal.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.quickSearchRequest.credit_system = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Special Listing Option
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSpecialListingOptional.optionArray = DashboardPresenter.parentListingList?.parentListingData?.speciallisting!.map{$0.listing_title ?? ""} ?? []
        //Its Id Values and its optional
        dropDownSpecialListingOptional.optionIds = DashboardPresenter.parentListingList?.parentListingData?.speciallisting!.map{Int($0.listing_id ?? "") ?? 0} ?? []
        
        // The the Closure returns Selected Index and String
        dropDownSpecialListingOptional.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.quickSearchRequest.credit_system = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        inputAgeMin.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Open ")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputAgeMin.input.keyboardType = UIKeyboardType.numberPad

        inputAgeMax.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Close ")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputAgeMax.input.keyboardType = UIKeyboardType.numberPad
        
        // Skill Level Cost
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSkillLevel.optionArray = ["Beginner", "Intermediate","Expert"]
        //Its Id Values and its optional
        dropDownSkillLevel.optionIds = [1,2,3]
        
        // The the Closure returns Selected Index and String
        dropDownSkillLevel.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.quickSearchRequest.credit_system = "\(id)"
        }
        ////////////////////////////////////////////////////////////

        // Additional Cost
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownAdditional.optionArray = ["Yes", "No"]
        //Its Id Values and its optional
        dropDownAdditional.optionIds = [1,2]
        
        // The the Closure returns Selected Index and String
        dropDownAdditional.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            self.selectedMonth = selectedText
            self.quickSearchRequest.credit_system = "\(id)"
        }
        ////////////////////////////////////////////////////////////
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView:UITableView) {
        
        if(tableView == self.tableViewJob){
            self.tableViewJob.scrollToBottom()
            if((self.locationTitleList.count) == 0){
                self.constraintTableViewJobHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                
                self.constraintTableViewJobHeight?.constant = self.tableViewJob.contentSize.height
            }
            
            self.updateTableViewHeight(tableView: tableView)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewJob == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewJob.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewJob.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewJobHeight.constant = heightOfTableView
            }
        }
    }
}

extension CalendarViewController{
    
    fileprivate func openDatePicker(){
        
        // Time picker for Open time
        ////////////////////////////////////////////////////////
        var picker: UIDatePicker = UIDatePicker.init()
        
        // Set some of UIDatePicker properties
        picker.datePickerMode = .time
        picker.timeZone = NSTimeZone.local
        picker.backgroundColor = UIColor.white
        
        // Add an event to call onDidChangeDate function when value is changed.
        picker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        
        self.inputOpenTime.input.inputView = picker
        ////////////////////////////////////////////////////////
        
        // Set picker for close time
        ////////////////////////////////////////////////////////
        picker = UIDatePicker.init()
        
        // Set some of UIDatePicker properties
        picker.datePickerMode = .time
        //        picker.maximumDate = Date()
        picker.timeZone = NSTimeZone.local
        picker.backgroundColor = UIColor.white
        
        // Add an event to call onDidChangeDate function when value is changed.
        picker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        
        self.inputCloseTime.input.inputView = picker
        self.pickerCloseTime = picker
        ////////////////////////////////////////////////////////
    }
    
    @objc fileprivate func dateValueChanged(_ sender: UIDatePicker){
        
        if(self.inputOpenTime.input.inputView == sender){
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            // Set date format
            dateFormatter.dateFormat = "hh:mm a"
            
            // Apply date format
            let selectedDate: String = dateFormatter.string(from: sender.date)
            
            self.pickerCloseTime?.minimumDate = sender.date
            
            self.inputOpenTime.input.text = selectedDate
            self.openTime = selectedDate
            
            //            if(self.delegate != nil){
            //                self.delegate?.updateTimeInfo(cellIndex: cellIndex, openTime: self.openTime, closeTime: self.closeTime)
            //            }
            
        } else if(self.inputCloseTime.input.inputView == sender){
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            // Set date format
            dateFormatter.dateFormat = "hh:mm a"
            
            // Apply date format
            let selectedDate: String = dateFormatter.string(from: sender.date)
            
            self.inputCloseTime.input.text = selectedDate
            self.closeTime = selectedDate
            
            //            if(self.delegate != nil){
            //                self.delegate?.updateTimeInfo(cellIndex: cellIndex, openTime: self.openTime, closeTime: self.closeTime)
            //            }
        }
        
    }
    
    func updateSpecificFeildInfo(response:[ListingDataDetail]?){
        self.specificCategoryList = response ?? []
        self.updateServiceList()
    }
    
    func updateServiceList(){
        // Specific Category
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSpecificCategory.optionArray = specificCategoryList.map{$0.listing_title ?? ""}
        //Its Id Values and its optional
        dropDownSpecificCategory.optionIds = specificCategoryList.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownSpecificCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
                
//                self.businessLocationInfo?.specificFieldName = selectedText
//                self.businessLocationInfo?.specificField = "\(id)"
            
            self.quickSearchRequest.specificField = "\(id)"
        }
        ////////////////////////////////////////////////////////////
    }
}

extension CalendarViewController{
    @IBAction func actionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionGetResult(_ sender: UIButton) {
//        self.dismiss(animated: true, completion: nil)
        
        if(self.isDisplayProvider){
            self.presenter.serverQuickSearchProvider(searchRequest: serviceProviderSearchRequest)
        } else {
            self.presenter.serverQuickSearchListing(quickSearchRequest: self.quickSearchRequest)
        }
    }
    
    @IBAction func actionReset(_ sender: UIButton) {
        self.dropDownFieldCategory.text = ""
        self.dropDownSpecificCategory.text = ""
        self.dropDownInstructionalServiceTypeOption.text = ""
        self.dropDownFrontOfficeServiceTypeOption.text = ""
        self.dropDownPurchaseOption.text = ""
        self.dropDownStdExpectional.text = ""
        self.dropDownEventType.text = ""
        self.dropDownExceptionalOffer.text = ""
        self.dropDownTargetGoal.text = ""
        self.dropDownSpecialListingOptional.text = ""
        self.dropDownDurationType.text = ""
        self.dropDownSkillLevel.text = ""
        self.dropDownAdditional.text = ""
        
        self.dropDownFieldCategory.selectedIndex = -1
        self.dropDownSpecificCategory.selectedIndex = -1
        self.dropDownInstructionalServiceTypeOption.selectedIndex = -1
        self.dropDownFrontOfficeServiceTypeOption.selectedIndex = -1
        self.dropDownPurchaseOption.selectedIndex = -1
        self.dropDownStdExpectional.selectedIndex = -1
        self.dropDownEventType.selectedIndex = -1
        self.dropDownExceptionalOffer.selectedIndex = -1
        self.dropDownTargetGoal.selectedIndex = -1
        self.dropDownSpecialListingOptional.selectedIndex = -1
        self.dropDownDurationType.selectedIndex = -1
        self.dropDownSkillLevel.selectedIndex = -1
        self.dropDownAdditional.selectedIndex = -1
        
        self.inputOpenTime.input.text = ""
        self.inputCloseTime.input.text = ""
        self.inputSpot.input.text = ""
        self.inputPriceMin.input.text = ""
        self.inputPriceMax.input.text = ""
        self.inputDurationTime.input.text = ""
        self.inputAgeMin.input.text = ""
        self.inputAgeMax.input.text = ""
        
        currentPage = 1
        self.quickSearchRequest = QuickSearchRequest()
        
        self.isDisplayProvider = true
    }
}

extension CalendarViewController{
    func showServiceListing(service:[ServiceAddRequest]?){
        self.dismiss(animated: true, completion: nil)
        if(self.callbackListing != nil){
            self.callbackListing!(service)
        }
    }
    
    func showServiceProvider(busiList:[BIAddRequest]?){
        
        self.dismiss(animated: false, completion: nil)
        if(self.callbackServiceProvider != nil){
            self.callbackServiceProvider!(busiList, nil)
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension CalendarViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tableViewJob){
            if((self.locationTitleList.count) != (HelperConstant.maximumSearchLocation + 1)){
                return (locationTitleList.count) + 1
            } else {
                return locationTitleList.count
            }
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints(tableView: tableView)
        
        if(self.tableViewJob == tableView){
            
            
            if(indexPath.row == 0){
                
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                
                cell.textField.placeholder = "Title"
                cell.delegate = self
                cell.refTableView = self.tableViewJob
//                cell.btnLocation.isHidden = true
                
                cell.backgroundColor = UIColor.clear
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                
                cell.delegate = self
                cell.refTableView = self.tableViewJob
                cell.lblTitle.text = locationTitleList[indexPath.row-1]
                cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
                
                cell.backgroundColor = UIColor.clear
                return cell
            }
        }
        
        return UITableViewCell(frame: CGRect(x:0, y: 0, width: 0, height: 0))
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension CalendarViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        if(refTableView == tableViewJob){
            if((self.locationTitleList.count) < HelperConstant.maximumSearchLocation){
                self.locationTitleList.append(text)
                self.tableViewJob.reloadData()
                
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

extension CalendarViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        if(refTableView == tableViewJob){
            
            self.locationTitleList.remove(at: cellIndex?.row ?? 0)
            self.tableViewJob.reloadData()
        }
    }
}

//
//  InstructionalModel.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  InstructionalSearchRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class InstructionalSearchRequest : Mappable {
    
    var page_id:String?
    var specificfield: String?
    var fieldcat: String?
    
    var content_type: String?
    var standard_exceptional: String?
    var exception_classification: String?
    var skilllevel: String?
    var speciallisting: String?
    var targetgoal: String?

    var location_zipcode: String?
    //var location_city: String?
    
    var select_location: String?
    var location_city: [String]?
    var zipcode: String?

    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        
        page_id                     <- map["page_id"]
        fieldcat                    <- map["fieldcat"]
        specificfield               <- map["specificfield"]
        
        content_type                <- map["content_type"]
        standard_exceptional        <- map["standard_exceptional"]
        exception_classification    <- map["exception_classification"]
        skilllevel                  <- map["skilllevel"]
        speciallisting              <- map["speciallisting"]
        targetgoal                  <- map["targetgoal"]
        location_zipcode            <- map["location_zipcode"]
       // location_city               <- map["location_city"]
        
        location_city      <- map["location_city"]
        select_location    <- map["select_location"]
        zipcode            <- map["zipcode"]
    }
}

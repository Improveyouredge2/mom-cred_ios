//
//  LocalBusinessSearchViewController.swift
//  MomCred
//
//  Created by MD on 17/02/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit
import iOSDropDown

class LocalBusinessSearchViewController: LMBaseViewController {
    
    //MARK: - IBOutlet(s)
    @IBOutlet weak var lblPackageSearchInfo: UILabel!

    @IBOutlet weak var dropDownBusinessTypeEntity: DropDown!
    @IBOutlet weak var dropDownSpecificBusinessType: DropDown!
    @IBOutlet weak var dropDownBusinessHours: DropDown!

    @IBOutlet weak var viewBusinessHr: UIView!
    @IBOutlet weak var inputOpenTime: RYFloatingInput!
    @IBOutlet weak var inputCloseTime: RYFloatingInput!
    
    fileprivate var pickerCloseTime: UIDatePicker?
    var openTime: String = ""
    var closeTime: String = ""
    
    @IBOutlet weak var tableViewJob: UITableView!
    @IBOutlet weak var dropDownLocationCityZipType: DropDown!
    @IBOutlet weak var constraintTableViewJobHeight: NSLayoutConstraint!
    
    //MARK: - Var(s)
    var presenter = LocalBusinessSearchPresenter()
    
    var isResultTypeListing = false
    var keywordSearch: String?
    var isQuickSearch: Bool = false
    
    private var selectedLocationCity: Bool = true
    private var selectedCities: [CityModel]?
    private var selectedZipcode: String?

    var callbackServiceProvider: ((_ busiList: [BIAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceProviderSearchRequest: ServiceProviderSearchRequest?) -> Void?)?
    
    fileprivate var isDisplayProvider = true
    fileprivate var serviceProviderSearchRequest: ServiceProviderSearchRequest?
    fileprivate var locationsList: [(name: String, zipCode: String)] = []
    var currentPage: Int = 1
    
    var packageSearchInfo: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)
        
        serviceProviderSearchRequest = ServiceProviderSearchRequest()
        serviceProviderSearchRequest?.initializeLocalSearch()
        
        if isQuickSearch {
            serviceProviderSearchRequest?.page_title = "Quick"
        }
        if let packageSearchInfo = packageSearchInfo {
            lblPackageSearchInfo.text = packageSearchInfo
        } else {
            lblPackageSearchInfo.isHidden = true
        }
        
        dropDownLocationCityZipType.optionArray = ["Town", "Zipcode"]
        dropDownLocationCityZipType.optionIds = [1, 2]
        dropDownLocationCityZipType.didSelect { [weak self] (selectedText , index ,id) in
            self?.selectedLocationCity = index == 0
            self?.tableViewJob.reloadData()
        }

        // Business Type Entity
        dropDownBusinessTypeEntity.optionArray = DashboardPresenter.parentListingList?.parentListingData?.business_entity?.compactMap { $0.listing_title } ?? []
        dropDownBusinessTypeEntity.optionIds = DashboardPresenter.parentListingList?.parentListingData?.business_entity?.compactMap({ $0.listing_id }).compactMap({ Int($0) }) ?? []
        dropDownBusinessTypeEntity.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.busi_entity = "\(id)"
        }
        
        // Exclusive Travel
        dropDownSpecificBusinessType.optionArray = DashboardPresenter.parentListingList?.parentListingData?.noninstructionbusitype?.compactMap { $0.listing_title } ?? []
        dropDownSpecificBusinessType.optionIds = DashboardPresenter.parentListingList?.parentListingData?.noninstructionbusitype?.compactMap({ $0.listing_id }).compactMap({ Int($0) }) ?? []
        dropDownSpecificBusinessType.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.specificfield = "\(id)"
        }
        
        viewBusinessHr.isHidden = true
        
        // Business Hours
        dropDownBusinessHours.optionArray = HelperConstant.weekDayName
        dropDownBusinessHours.optionIds = [1, 2, 3, 4, 5, 6, 7]
        dropDownBusinessHours.didSelect { [weak self] (selectedText , index ,id) in
            self?.viewBusinessHr.isHidden = false
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.serviceProviderSearchRequest?.business_hours = "\(id)"
            
            self?.serviceProviderSearchRequest?.busi_hours = "\(id)"

        }
        
        inputOpenTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(openTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Open ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputCloseTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(closeTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Close ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        openDatePicker()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        actionReset(UIButton())
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView: UITableView) {
        
        if(tableView == tableViewJob){
            tableViewJob.scrollToBottom()
            if locationsList.isEmpty {
                constraintTableViewJobHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                
                constraintTableViewJobHeight?.constant = tableViewJob.contentSize.height
            }
            
            updateTableViewHeight(tableView: tableView)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewJob == tableView){
            
            UIView.animate(withDuration: 0, animations: { [weak self] in
                self?.tableViewJob.layoutIfNeeded()
            }, completion: { [weak self] (complete) in
                guard let self = self else { return }
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewJob.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewJobHeight.constant = heightOfTableView
            })
        }
    }
}

extension LocalBusinessSearchViewController{
    
    fileprivate func openDatePicker(){
        
        // Time picker for Open time
        ////////////////////////////////////////////////////////
        var picker: UIDatePicker = UIDatePicker.init()
        
        // Set some of UIDatePicker properties
        picker.datePickerMode = .time
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }
        picker.timeZone = NSTimeZone.local
        picker.backgroundColor = UIColor.white
        
        // Add an event to call onDidChangeDate function when value is changed.
        picker.addTarget(self, action: #selector(dateValueChanged(_: )), for: .valueChanged)
        
        inputOpenTime.input.inputView = picker
        ////////////////////////////////////////////////////////
        
        // Set picker for close time
        ////////////////////////////////////////////////////////
        picker = UIDatePicker.init()
        
        // Set some of UIDatePicker properties
        picker.datePickerMode = .time
        //        picker.maximumDate = Date()
        picker.timeZone = NSTimeZone.local
        picker.backgroundColor = UIColor.white
        
        // Add an event to call onDidChangeDate function when value is changed.
        picker.addTarget(self, action: #selector(dateValueChanged(_: )), for: .valueChanged)
        
        inputCloseTime.input.inputView = picker
        pickerCloseTime = picker
        ////////////////////////////////////////////////////////
    }
    
    @objc fileprivate func dateValueChanged(_ sender: UIDatePicker){
        
        if(inputOpenTime.input.inputView == sender){
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            // Set date format
            dateFormatter.dateFormat = "hh: mm a"
            
            // Apply date format
            let selectedDate: String = dateFormatter.string(from: sender.date)
            
            pickerCloseTime?.minimumDate = sender.date
            
            inputOpenTime.input.text = selectedDate
            openTime = selectedDate
            
            serviceProviderSearchRequest?.hoursfrom = selectedDate
//            if(delegate != nil){
//                delegate?.updateTimeInfo(cellIndex: cellIndex, openTime: openTime, closeTime: closeTime)
//            }
            
        } else if(inputCloseTime.input.inputView == sender){
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            // Set date format
            dateFormatter.dateFormat = "hh: mm a"
            
            // Apply date format
            let selectedDate: String = dateFormatter.string(from: sender.date)
            
            inputCloseTime.input.text = selectedDate
            closeTime = selectedDate
            
            serviceProviderSearchRequest?.hoursto = selectedDate
        }
        
    }
}

extension LocalBusinessSearchViewController{
    @IBAction func actionClose(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionGetResult(_ sender: UIButton) {
        
        if dropDownLocationCityZipType.text?.count ?? 0 > 0 && (selectedCities?.count ?? 0 > 0 || selectedZipcode?.count ?? 0 > 0) {
            
            if selectedLocationCity {
                serviceProviderSearchRequest?.select_location = "1"
                serviceProviderSearchRequest?.location_city = selectedCities?.compactMap { $0.name }
                serviceProviderSearchRequest?.zipcode = nil
            } else {
                serviceProviderSearchRequest?.select_location = "2"
                serviceProviderSearchRequest?.zipcode = selectedZipcode
                serviceProviderSearchRequest?.location_city = nil
            }

            Spinner.show()
            presenter.serverSearchProvider(serviceProviderSearchRequest: serviceProviderSearchRequest)
            
        }else{
            
            self.showBannerAlertWith(message:selectedLocationCity ?  LocalizationKeys.location_select.getLocalized() :LocalizationKeys.zipcode_enter.getLocalized() , alert: .error)

        }
        
       
    }
    
    @IBAction func actionReset(_ sender: UIButton) {
        dropDownBusinessTypeEntity.text = ""
        dropDownSpecificBusinessType.text = ""
        dropDownBusinessHours.text = ""
        
        dropDownBusinessTypeEntity.selectedIndex = -1
        dropDownSpecificBusinessType.selectedIndex = -1
        dropDownBusinessHours.selectedIndex = -1
        
        isDisplayProvider = true
        
        serviceProviderSearchRequest = ServiceProviderSearchRequest()
        serviceProviderSearchRequest?.initializeLocalSearch()

        currentPage = 1
        if isQuickSearch {
            serviceProviderSearchRequest?.page_title = "Quick"
        }
        resignAllList()
    }
}

extension LocalBusinessSearchViewController {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    fileprivate func resignAllList(){
        if dropDownBusinessTypeEntity.isSelected {
            dropDownBusinessTypeEntity.touchAction()
        }
        
        if dropDownSpecificBusinessType.isSelected {
            dropDownSpecificBusinessType.touchAction()
        }
        
        if dropDownBusinessHours.isSelected {
            dropDownBusinessHours.touchAction()
        }
    }
    
    func showServiceProvider(busiList: [BIAddRequest]?){
        dismiss(animated: false, completion: nil)
        callbackServiceProvider?(busiList, nil, serviceProviderSearchRequest)
    }
}

// MARK: - UITableViewDataSource & UITableViewDelegate
extension LocalBusinessSearchViewController: UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        updateViewConstraints(tableView: tableView)
        let cell: ProviderSelectCityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProviderSelectCityTableViewCell") as! ProviderSelectCityTableViewCell
        if selectedLocationCity {
            cell.selectedCities = selectedCities
            cell.cellType = .town
        } else {
            cell.selectedZipcode = selectedZipcode
            cell.cellType = .zipcode
        }
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension LocalBusinessSearchViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        if(refTableView == tableViewJob){
            if((locationsList.count) < HelperConstant.maximumSearchLocation){
                locationsList.append((text, zipcode))
                tableViewJob.reloadData()
                
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

extension LocalBusinessSearchViewController: BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        if(refTableView == tableViewJob){
            
            locationsList.remove(at: cellIndex?.row ?? 0)
            tableViewJob.reloadData()
        }
    }
}

extension LocalBusinessSearchViewController: ProviderSelectCityTableViewCellDelegate {
    func didSelectCities(_ cities: [CityModel]) {
        selectedCities = cities
    }
    
    func didEnterZipcode(_ zipcode: String?) {
        selectedZipcode = zipcode
    }
}

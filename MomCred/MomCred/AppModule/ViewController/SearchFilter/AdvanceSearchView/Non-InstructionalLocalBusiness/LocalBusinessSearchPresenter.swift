//
//  LocalBusinessSearchPresenter.swift
//  MomCred
//
//  Created by MD on 17/02/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class LocalBusinessSearchPresenter {
    weak var view: LocalBusinessSearchViewController!
    
    func connectView(view: LocalBusinessSearchViewController) {
        self.view = view
    }
}

extension LocalBusinessSearchPresenter {
    func serverSearchProvider(serviceProviderSearchRequest: ServiceProviderSearchRequest?) {
        serviceProviderSearchRequest?.page_index = "\(view.currentPage)"
        
        let callback: (_ status:Bool, _ response: DashboardAllServiceProviderResponseData?, _ message: String?) -> Void = { (status, response, message) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if status {
                    self?.view.showServiceProvider(busiList: response?.busi)
                    self?.view.currentPage += 1
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        }
        
        if view.isQuickSearch {
            QuickSearchService.serverQuickSearchProvider(quickSearchRequest: serviceProviderSearchRequest, callback: callback)
        } else {
            AdvanceSearchService.serverServiceProviderSearchListing(serviceProviderSearchRequest: serviceProviderSearchRequest, callback: callback)
        }
    }
}

//
//  PersonnelViewController.swift
//  MomCred
//
//  Created by Rajesh Yadav on 12/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class PersonnelViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    
    @IBOutlet weak var dropDownType : DropDown!
//    @IBOutlet weak var dropDownInstructionalServiceTypeOption : DropDown!
    @IBOutlet weak var dropDownFieldCategory : DropDown!
    @IBOutlet weak var dropDownSpecificCategory : DropDown!
    
    @IBOutlet weak var inputExp: RYFloatingInput!
    
    @IBOutlet weak var tableViewJob: UITableView!
    @IBOutlet weak var constraintTableViewJobHeight: NSLayoutConstraint!
    @IBOutlet weak var dropDownLocationCityZipType: DropDown!

    
    //MARK:- Var(s)
    
    // Location Select
    private var selectedLocationCity: Bool = true
    private var selectedCities: [CityModel]?
    private var selectedZipcode: String?
    fileprivate var locationsList: [(name: String, zipCode: String)] = []
    
    var fieldCategoryList:[ListingDataDetail] = []
    var specificCategoryList:[ListingDataDetail] = []
    var instructionalServiceTypesList:[ListingDataDetail] = []
    var frontOfficeServiceTypesList:[ListingDataDetail] = []
    var exceptional:[ListingDataDetail] = []
    var presenter = PersonnelPresenter()
    
    var isResultTypeListing = false
    var keywordSearch:String?
    
    var callbackListing:((_ personnelList:[PersonnelAddRequest]?, _ personnelSearchRequest :PersonnelSearchRequest?) -> Void?)?
    
    fileprivate var isDisplayProvider = true
    fileprivate var personnelSearchRequest = PersonnelSearchRequest()
    fileprivate var locationTitleList:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)
        
        // Select Location
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownLocationCityZipType.optionArray = ["Town", "Zipcode"]
        dropDownLocationCityZipType.optionIds = [1, 2]
        dropDownLocationCityZipType.didSelect { [weak self] (selectedText , index ,id) in
            self?.selectedLocationCity = index == 0
            self?.tableViewJob.reloadData()
        }

        self.fieldCategoryList = DashboardPresenter.parentListingList?.parentListingData?.fields ?? []

        self.instructionalServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.services_types![1].subcat ?? [])

        self.frontOfficeServiceTypesList = (DashboardPresenter.parentListingList?.parentListingData?.services_types![1].subcat ?? [])

        self.exceptional = (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![0].subcat ?? []) + (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification![1].subcat ?? [])
        
        // Limit 2 digit
        self.inputExp.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.inputExp.input.keyboardType = UIKeyboardType.numberPad
//
        // Purchase Option
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownType.optionArray = DashboardPresenter.parentListingList?.parentListingData?.personaltype!.map{$0.listing_title ?? ""} ?? []
        //Its Id Values and its optional
        dropDownType.optionIds = DashboardPresenter.parentListingList?.parentListingData?.personaltype!.map{Int($0.listing_id ?? "") ?? 0} ?? []

        // The the Closure returns Selected Index and String
        dropDownType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.personnelSearchRequest.personal_type = "\(id)"

        }
        ////////////////////////////////////////////////////////////

        // Instructional Service type
        ////////////////////////////////////////////////////////////
        // Array value listing
//        dropDownInstructionalServiceTypeOption.optionArray = self.instructionalServiceTypesList.map{$0.listing_title ?? ""}
//
//        //Its Id Values and its optional
//        dropDownInstructionalServiceTypeOption.optionIds = self.instructionalServiceTypesList.map{Int($0.listing_id ?? "0") ?? 0}
//
//        // The the Closure returns Selected Index and String
//        dropDownInstructionalServiceTypeOption.didSelect{(selectedText , index ,id) in
//            print("Selected String: \(selectedText) \n index: \(index)")
//
//            self.personnelSearchRequest.facility_type = "\(id)"
//        }
        ////////////////////////////////////////////////////////////

        
        // Field Category
        ////////////////////////////////////////////////////////////
        // Array value listing
        dropDownFieldCategory.optionArray = fieldCategoryList.map{$0.listing_title ?? ""}

        //Its Id Values and its optional
        dropDownFieldCategory.optionIds = fieldCategoryList.map{Int($0.listing_id ?? "0") ?? 0}

        // The the Closure returns Selected Index and String
        dropDownFieldCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.personnelSearchRequest.fieldcat = "\(id)"
            self.personnelSearchRequest.specificfield = ""
            self.presenter.serverChildListingRequest(parentId: "\(id)")
        }
        ////////////////////////////////////////////////////////////
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.actionReset(UIButton())
    }
}

extension PersonnelViewController{
    
    func updateSpecificFeildInfo(response:[ListingDataDetail]?){
        self.specificCategoryList = response ?? []
        self.updateServiceList()
    }
    
    func updateServiceList(){
        // Specific Category
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSpecificCategory.optionArray = specificCategoryList.map{$0.listing_title ?? ""}
        //Its Id Values and its optional
        dropDownSpecificCategory.optionIds = specificCategoryList.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownSpecificCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
                
//                self.businessLocationInfo?.specificFieldName = selectedText
//                self.businessLocationInfo?.specificField = "\(id)"
            
            self.personnelSearchRequest.specificfield = "\(id)"
        }
        ////////////////////////////////////////////////////////////
    }
}

extension PersonnelViewController {
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView:UITableView) {
        
        if(tableView == tableViewJob){
            tableViewJob.scrollToBottom()
            if locationsList.isEmpty {
                constraintTableViewJobHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                
                constraintTableViewJobHeight?.constant = tableViewJob.contentSize.height
            }
            
            updateTableViewHeight(tableView: tableView)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewJob == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewJob.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewJob.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewJobHeight.constant = heightOfTableView
            }
        }
    }
}

extension PersonnelViewController{
    @IBAction func actionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionGetResult(_ sender: UIButton) {
        if dropDownLocationCityZipType.text?.count ?? 0 > 0 && (selectedCities?.count ?? 0 > 0 || selectedZipcode?.count ?? 0 > 0) {
            if selectedLocationCity {
                personnelSearchRequest.select_location = "1"
                personnelSearchRequest.location_city = selectedCities?.compactMap { $0.name }
            } else {
                personnelSearchRequest.select_location = "2"
                personnelSearchRequest.zipcode = selectedZipcode
            }
            
            self.personnelSearchRequest.page_id = "0"
            self.personnelSearchRequest.personal_experience = self.inputExp.text() ?? ""
            Spinner.show()
            self.presenter.serverPersonnelSearchListing(personnelSearchRequest: self.personnelSearchRequest)
            
        }else {
            
            self.showBannerAlertWith(message:selectedLocationCity ?  LocalizationKeys.location_select.getLocalized() :LocalizationKeys.zipcode_enter.getLocalized() , alert: .error)

        }
        
    }
   
    @IBAction func actionReset(_ sender: UIButton) {
        self.dropDownFieldCategory.text = ""
        self.dropDownSpecificCategory.text = ""
//        self.dropDownInstructionalServiceTypeOption.text = ""
        self.dropDownType.text = ""
        
        self.dropDownFieldCategory.selectedIndex = -1
        self.dropDownSpecificCategory.selectedIndex = -1
//        self.dropDownInstructionalServiceTypeOption.selectedIndex = -1
        self.dropDownType.selectedIndex = -1
        
        self.personnelSearchRequest.specificfield = ""
        self.personnelSearchRequest.fieldcat = ""
//        self.personnelSearchRequest.facility_type = ""
        self.personnelSearchRequest.personal_type = ""
        
        self.inputExp.input.text = ""
        
        self.isDisplayProvider = true
    }
}

extension PersonnelViewController{
    func showServiceListing(personnelList:[PersonnelAddRequest]?){
        self.dismiss(animated: true, completion: nil)
        if(self.callbackListing != nil){
            self.callbackListing!(personnelList, self.personnelSearchRequest)
        }
    }
}


// MARK:- UITableViewDataSource & UITableViewDelegate
extension PersonnelViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints(tableView: tableView)
        
        let cell: ProviderSelectCityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProviderSelectCityTableViewCell") as! ProviderSelectCityTableViewCell
        if selectedLocationCity {
            cell.selectedCities = selectedCities
            cell.cellType = .town
        } else {
            cell.selectedZipcode = selectedZipcode
            cell.cellType = .zipcode
        }
        cell.delegate = self
        return cell
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension PersonnelViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        if(refTableView == tableViewJob){
            
            locationsList.remove(at: cellIndex?.row ?? 0)
            tableViewJob.reloadData()
        }
    }
}

extension PersonnelViewController: ProviderSelectCityTableViewCellDelegate {
    func didSelectCities(_ cities: [CityModel]) {
        selectedCities = cities
    }
    
    func didEnterZipcode(_ zipcode: String?) {
        selectedZipcode = zipcode
    }
}


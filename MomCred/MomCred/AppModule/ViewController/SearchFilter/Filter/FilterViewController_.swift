//
//  FilterViewController_.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import Koyomi

class FilterViewController_: LMBaseViewController {
    
    @IBOutlet weak var tagListService:TagListView!
    
    @IBOutlet weak var constraintTagListHeight:NSLayoutConstraint!
    
    //MARK:- IBOutlet(s)
    @IBOutlet fileprivate weak var koyomi: Koyomi! {
        didSet {
            
            koyomi.calendarDelegate = self
            koyomi.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            koyomi.weeks = ("S", "M", "T", "W", "T", "F", "S")
            koyomi.style = .standard
            koyomi.dayPosition = .center
            
            koyomi.currentDateFormat = koyomi.currentDateString(withFormat: "yyyy-MM-dd HH:mm:ss Z")
            
            koyomi.selectionMode = .sequence(style: .semicircleEdge)
            
//            koyomi.
            koyomi.select(date: Date())
//            koyomi.select(date: Date().add(years: -100), to: Date().add(years: 100))
            koyomi.weekBackgrondColor = UIColor(hexString: ColorCode.tabInActiveColor) ?? UIColor.clear
            
            koyomi.dayBackgrondColor = UIColor(hexString: ColorCode.tabInActiveColor) ?? UIColor.clear
            koyomi.weekdayColor = UIColor.white
            koyomi.weekColor = UIColor.white
            koyomi.holidayColor = (UIColor.white, UIColor.white)
            
            koyomi.selectedStyleColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            
            koyomi
                .setDayFont(size: 16)
                .setWeekFont(size: 16)
        }
    }
    
    @IBOutlet weak var viewCalendar: UIView!
    @IBOutlet weak var lblDate: UILabel!
    
    var callbackListing:((_ dateList:[String]?, _ tagIndexList:[String]?) -> Void?)?
    
//    fileprivate let invalidPeriodLength = 7
    fileprivate let invalidPeriodLength = 30
//    fileprivate let invalidPeriodLength = 90
    
    var strStartDate : String = ""
    var strEndDate : String = ""
    var arrSelectedDate : [String] = []
    var selectedTagIndex:[String] = []
    
    var purchaseFilterDataList:[PurchaseListFilterResponse.PurchaseFilterDataResponse]?
    
    // MARK: - For Only Current and Next Month Selection
    // MARK: -
    var mnthSelector : Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.presenter.connectView(view: self)
        
        lblDate.text = koyomi.currentDateString(withFormat: "MMM yyyy")
        
        tagListService.delegate = self
        tagListService.textFont = FontsConfig.FontHelper.defaultRegularFontWithSize(16)
        
        if(self.purchaseFilterDataList != nil && (self.purchaseFilterDataList?.count)! > 0){
            for tagInfo in (self.purchaseFilterDataList)!{
                tagListService.addTag(tagInfo.servicename ?? "")
            }
        }

        constraintTagListHeight.constant = tagListService.intrinsicContentSize.height ?? 50
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
}

extension FilterViewController_: TagListViewDelegate{
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}

// MARK: - UIButton Action
// MARK: -
extension FilterViewController_{
    /**
     *  Previous button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnPrevious(_ sender: UIButton) {
        
        if  self.mnthSelector == 1{
            self.mnthSelector = 0
            koyomi.display(in: .previous)
        }
    }
    
    /**
     *  Next button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnNext(_ sender: UIButton) {
        
        if self.mnthSelector == 0  {
            self.mnthSelector += 1
            koyomi.display(in: .next)
        }
    }
    
    /**
     *  Continue button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnContinue(_ sender : UIButton){
        
//        if self.arrSelectedDate.count == 0{
//
//            let toast: Toast = Toast.default(text: LocalizationKeys.msg_validation_select_one_date.getLocalized())
//            // Show
//            toast.show()
//        } else if self.checkDateAailability(arrBookingSlot: self.arrBookingTimeSlot) {
//            print("Slots are Available")
//            self.openBookingSlotsViewController()
//        }else{
//            let toast: Toast = Toast.default(text: LocalizationKeys.msg_validation_slot_passed.getLocalized())
//            // Show
//            toast.show()
//        }
    }
    
    /**
     *  Close button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionClose(_ sender : UIButton){
        dismiss(animated: true, completion: nil)
    }
    
    /**
     *  Reset button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionReset(_ sender : UIButton){
        
        // Reset calendar
        
        
        // Deselect all tag list
        for tempTagView in self.tagListService.tagViews {
                tempTagView.isSelected = false
        }
    }
    
    /**
     *  Apply Filter button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionApplyFilter(_ sender : UIButton){
        dismiss(animated: true, completion: nil)
        var tagIdList:[String] = []
        
        if(self.arrSelectedDate.count == 0){
            self.arrSelectedDate = koyomi.model.selectedDates.filter{$0.value}.map{$0.key.apiSpecificStringFromFormat()}.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            print( self.arrSelectedDate )
        }
        
        if(self.purchaseFilterDataList != nil && (self.purchaseFilterDataList?.count)! > 0){
            for service in  (self.purchaseFilterDataList)!{
                
                for tagInfo in (self.tagListService.tagViews) {
                    
                    if(tagInfo.isSelected && service.servicename?.caseInsensitiveCompare(tagInfo.currentTitle ?? "") == ComparisonResult.orderedSame){
                        tagIdList.append(service.service_id ?? "")
                    }
                }
            }
            
            print(tagIdList)
        }
        
        if(self.callbackListing != nil){
            self.callbackListing!(self.arrSelectedDate, tagIdList)
        }
    }
}


// MARK: - KoyomiDelegate -
// MARK: -
extension FilterViewController_: KoyomiDelegate {
    
    /**
     *  Koyomi  Calendar  Delegate.
     *
     *  @param .
     *
     *  @Developed By: Team Consagous
     */
    
    func koyomi(_ koyomi: Koyomi, didSelect date: Date?, forItemAt indexPath: IndexPath) {
        
        //print(koyomi.model.selectedDates.filter{$0.value}.map{$0.key.appSpecificStringFromFormat()})
        
        self.arrSelectedDate = []
        self.arrSelectedDate = koyomi.model.selectedDates.filter{$0.value}.map{$0.key.apiSpecificStringFromFormat()}.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        print( self.arrSelectedDate )
    }
    
    
    func koyomi(_ koyomi: Koyomi, currentDateString dateString: String) {
        lblDate.text = koyomi.currentDateString(withFormat: "MMM yyyy")
        
    }
    
    @objc(koyomi:shouldSelectDates:to:withPeriodLength:)
    func koyomi(_ koyomi: Koyomi, shouldSelectDates date: Date?, to toDate: Date?, withPeriodLength length: Int) -> Bool {
        
        if length > invalidPeriodLength {
            print("More than \(invalidPeriodLength) days are invalid period.")
            return false
        }

        if date != nil{
            let strStartDate = date?.appSpecificStringFromFormat()
            let strTodayDate = Date().appSpecificStringFromFormat()
            let finalStartDate = self.toLocalDate(strStartDate!)
            let finalTodayDate = self.toLocalDate(strTodayDate)

            return finalStartDate >= finalTodayDate
        }
        
        
        // Update text color of Today's date
//        koyomi
//            .setDayColor(.white, of: today, to: weekLaterDay)
//            .setDayBackgrondColor(.black, of: today, to: weekLaterDay)
        
        if(date != Date()){
            koyomi
                .setDayColor(#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), of: Date())
                .setDayBackgrondColor(#colorLiteral(red: 0.2823529412, green: 0.3137254902, blue: 0.3490196078, alpha: 1), of: Date())
        } else {
            koyomi
                .setDayColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), of: Date())
        }
        
        
        return true
    }
    
    func toLocalDate(_ withDateString : String) -> Date {
        let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "dd-MM-yyyy"
//        dateFormatter.dateFormat =  "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        return dateFormatter.date(from: withDateString)!
    }
    
    
}

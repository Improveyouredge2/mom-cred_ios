//
//  FilterViewController.swift
//  MomCred
//
//  Created by Rajesh Yadav on 18/02/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import Foundation
import FSCalendar

class FilterViewController: LMBaseViewController {
    
    @IBOutlet weak var tagListService:TagListView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var constraintTagListHeight:NSLayoutConstraint!

    @IBOutlet weak var viewCalendar: UIView!
    @IBOutlet weak var lblDate: UILabel!
    
    var callbackListing:((_ dateList:[String]?, _ tagIndexList:[String]?) -> Void?)?
    
    var strStartDate : String = ""
    var strEndDate : String = ""
    var arrSelectedDate : [String] = []
    var selectedTagIndex:[String] = []
    
    fileprivate let invalidPeriodLength = 90
    
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    var selectedDateArray: [Date] = [] {
      didSet {
        // sort the array
        selectedDateArray = calendar.selectedDates.sorted()
        
        switch selectedDateArray.count {
        case 0:
          startDate = nil
          endDate = nil
        case 1:
          startDate = selectedDateArray.first
          endDate = nil
        case _ where selectedDateArray.count > 1:
          startDate = selectedDateArray.first
          endDate = selectedDateArray.last
          
          var nextDay = Calendar.current.date(byAdding: .day, value: 1, to: startDate!)
          while nextDay!.startOfDay <= endDate! {
            
            if(self.calendar.selectedDates.count < invalidPeriodLength){
                
                self.calendar.select(nextDay)
                nextDay = Calendar.current.date(byAdding: .day, value: 1, to: nextDay!)
            } else {
              break
            }
          }
        default:
          return
        }
      }
    }
    
    var startDate: Date? {
      didSet {
        startDate = startDate?.startOfDay
      }
    }
    
    var endDate: Date? {
      didSet {
        endDate = endDate?.endOfDay
      }
    }
    
    var purchaseFilterDataList:[PurchaseListFilterResponse.PurchaseFilterDataResponse]?
    
    // MARK: - For Only Current and Next Month Selection
    // MARK: -
    var mnthSelector : Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.calendar.select(Date())
        self.calendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
//        self.calendar.swipeToChooseGesture.isEnabled = true // Swipe-To-Choose
//        let scopeGesture = UIPanGestureRecognizer(target: calendar, action: #selector(calendar.handleScopeGesture(_:)));
//        self.calendar.addGestureRecognizer(scopeGesture)
        
        tagListService.delegate = self
        tagListService.textFont = FontsConfig.FontHelper.defaultRegularFontWithSize(16)
        
        if(self.purchaseFilterDataList != nil && (self.purchaseFilterDataList?.count)! > 0){
            for tagInfo in (self.purchaseFilterDataList)!{
                tagListService.addTag(tagInfo.servicename ?? "")
            }
        }

        constraintTagListHeight.constant = tagListService.intrinsicContentSize.height ?? 50
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
}

extension FilterViewController: TagListViewDelegate{
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}

// MARK: - UIButton Action
// MARK: -
extension FilterViewController{
    /**
     *  Previous button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnPrevious(_ sender: UIButton) {
        
        if  self.mnthSelector == 1{
            self.mnthSelector = 0
        }
    }
    
    /**
     *  Next button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnNext(_ sender: UIButton) {
        
        if self.mnthSelector == 0  {
            self.mnthSelector += 1
        }
    }
    
    /**
     *  Continue button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnContinue(_ sender : UIButton){
        
//        if self.arrSelectedDate.count == 0{
//
//            let toast: Toast = Toast.default(text: LocalizationKeys.msg_validation_select_one_date.getLocalized())
//            // Show
//            toast.show()
//        } else if self.checkDateAailability(arrBookingSlot: self.arrBookingTimeSlot) {
//            print("Slots are Available")
//            self.openBookingSlotsViewController()
//        }else{
//            let toast: Toast = Toast.default(text: LocalizationKeys.msg_validation_slot_passed.getLocalized())
//            // Show
//            toast.show()
//        }
    }
    
    /**
     *  Close button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionClose(_ sender : UIButton){
        dismiss(animated: true, completion: nil)
    }
    
    /**
     *  Reset button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionReset(_ sender : UIButton){
        
        // Reset calendar
        if(calendar.selectedDates != nil && (calendar.selectedDates.count) > 0){
            for date in calendar.selectedDates {
                calendar.deselect(date)
            }
        }
        
        self.calendar.select(Date())
        self.configureVisibleCells()
        
        // Deselect all tag list
        for tempTagView in self.tagListService.tagViews {
                tempTagView.isSelected = false
        }
    }
    
    /**
     *  Apply Filter button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionApplyFilter(_ sender : UIButton){
        dismiss(animated: true, completion: nil)
        var tagIdList:[String] = []
        
        let startDateStr = (self.formatter.string(from: self.startDate ?? Date()))
        let endDateStr = (self.formatter.string(from: self.endDate ?? Date()))
        print("Start date: \(startDateStr) \nEnd date: \(endDateStr)")
        
        self.arrSelectedDate.removeAll()
        self.arrSelectedDate.append(startDateStr)
        self.arrSelectedDate.append(endDateStr)
        
        if(self.purchaseFilterDataList != nil && (self.purchaseFilterDataList?.count)! > 0){
            for service in  (self.purchaseFilterDataList)!{
                
                for tagInfo in (self.tagListService.tagViews) {
                    
                    if(tagInfo.isSelected && service.servicename?.compare(tagInfo.currentTitle ?? "") == ComparisonResult.orderedSame){
                        tagIdList.append(service.service_id ?? "")
                    }
                }
            }
            
            print(tagIdList)
        }
        
        if(self.callbackListing != nil){
            self.callbackListing!(self.arrSelectedDate, tagIdList)
        }
    }
}

extension FilterViewController:FSCalendarDataSource{
   // MARK:- FSCalendarDataSource
   
   func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
       let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
       return cell
   }
   
   func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
       self.configure(cell: cell, for: date, at: position)
   }
   
//   func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
//       if self.gregorian.isDateInToday(date) {
//           return "今"
//       }
//       return nil
//   }
   
   func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
       return 0
   }
}

extension FilterViewController:FSCalendarDelegate{
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        //        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        print("did select date \(self.formatter.string(from: date))")
        if date.isAfterDate(Date().endOfDay!) {
          calendar.deselect(date)
        } else {
          selectedDateArray.append(date)
        }
        
        self.configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        print("did deselect date \(self.formatter.string(from: date))")
        
        
        if calendar.selectedDates.count > 2 {
            let datesToDeselect: [Date] = calendar.selectedDates.filter{ $0 > date }
            datesToDeselect.forEach{ calendar.deselect($0) }
            calendar.select(date) // adds back the end date that was just deselected so it matches selectedDateArray
        }
        selectedDateArray = selectedDateArray.filter{ $0 < date }
        selectedDateArray.forEach{self.calendar.select($0)}
        
        self.configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        if self.gregorian.isDateInToday(date) {
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
    
    // MARK: - Private functions
    private func configureVisibleCells() {
        calendar.visibleCells().forEach { (cell) in
            let date = calendar.date(for: cell)
            let position = calendar.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        let diyCell = (cell as! DIYCalendarCell)
        // Custom today circle
//        diyCell.circleImageView.isHidden = !self.gregorian.isDateInToday(date)
        // Configure selection layer
        if position == .current {
            
            var selectionType = SelectionType.none
            
            if calendar.selectedDates.contains(date) {
                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
                if calendar.selectedDates.contains(date) {
                    if calendar.selectedDates.contains(previousDate) && calendar.selectedDates.contains(nextDate) {
                        selectionType = .middle
                    }
                    else if calendar.selectedDates.contains(previousDate) && calendar.selectedDates.contains(date) {
                        selectionType = .rightBorder
                    }
                    else if calendar.selectedDates.contains(nextDate) {
                        selectionType = .leftBorder
                    }
                    else {
                        selectionType = .single
                    }
                }
            }
            else {
                selectionType = .none
            }
            if selectionType == .none {
                diyCell.selectionLayer.isHidden = true
                return
            }
            diyCell.selectionLayer.isHidden = false
            diyCell.selectionType = selectionType
            
        } else {
//            diyCell.circleImageView.isHidden = true
            diyCell.selectionLayer.isHidden = true
        }
    }
}

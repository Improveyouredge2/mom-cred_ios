//
//  QuickSearchService.swift
//  MomCred
//
//  Created by Rajesh Yadav on 19/10/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  DashboardService is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class QuickSearchService{

    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverQuickSearchListing(quickSearchRequest:QuickSearchRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceResponseData?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = quickSearchRequest?.toJSONString()
        
        //        internal static let API_QUICK_SEARCH = "quicksearch"
        //        internal static let API_QUICK_SEARCH_SEARCH = "quicksearchservice"
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_QUICK_SEARCH_SEARCH, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:DashboardAllServiceResponseData? = DashboardAllServiceResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverQuickSearchProvider(quickSearchRequest: ServiceProviderSearchRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceProviderResponseData?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = quickSearchRequest?.toJSONString()
        
        //        internal static let API_QUICK_SEARCH = "quicksearch"
        //        internal static let API_QUICK_SEARCH_SEARCH = "quicksearchservice"
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_QUICK_SEARCH, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:DashboardAllServiceProviderResponseData? = DashboardAllServiceProviderResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

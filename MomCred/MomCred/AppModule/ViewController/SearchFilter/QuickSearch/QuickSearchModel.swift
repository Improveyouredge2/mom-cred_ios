//
//  QuickSearchModel.swift
//  MomCred
//
//  Created by Rajesh Yadav on 17/10/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

import ObjectMapper

/**
 *  NotificationRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class QuickSearchRequest: Mappable {
    var page_index: String?
    var catField: String?
    var specificField: String?
    var instruction_service_type: String?
    var front_office_service: String?
    var service_event: String?
    var exceptional_classification_offered: String?
    var credit_system: String?
    var purchasablity: String?
    var service_provider_classification: String?
    var minprice: String?
    var maxprice: String?
    var quick_search: String? = "Get Results"
    
    required init() { }
    required init?(map: Map) { }

    func mapping(map: Map) {
        page_index <- map["page_index"]
        catField <- map["catField"]
        specificField <- map["specificField"]
        instruction_service_type <- map["instruction_service_type"]
        front_office_service <- map["front_office_service"]
        service_event <- map["service_event"]
        exceptional_classification_offered <- map["exceptional_classification_offered"]
        credit_system <- map["credit_system"]
        purchasablity <- map["purchasablity"]
        service_provider_classification <- map["service_provider_classification"]
        minprice <- map["minprice"]
        maxprice <- map["maxprice"]
        quick_search <- map["quick_search"]
    }
}


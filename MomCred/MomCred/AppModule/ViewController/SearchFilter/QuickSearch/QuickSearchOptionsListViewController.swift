//
//  QuickSearchOptionsListViewController.swift
//  MomCred
//
//  Created by MD on 17/02/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class QuickSearchOptionsListViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnClose: UIButton!
    
    var menuList:[MyProfileMenuItem] = []
    
    var callbackListing:((_ serviceList:[ServiceAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceSearchRequest:ServiceSearchRequest?) -> Void?)?
    var callbackServiceProvider:((_ busiList:[BIAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceProviderSearchRequest:ServiceProviderSearchRequest?) -> Void?)?
    
    var isServiceOnly = false
    
    fileprivate var isViewControllerPushed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadMoreMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.isViewControllerPushed = false
    }
}

extension QuickSearchOptionsListViewController{
    @IBAction func actionClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension QuickSearchOptionsListViewController{
    fileprivate func loadMoreMenu(){
        var menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.service_provider.getLocalized()
        
        let serviceProviderViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: ServiceProviderViewController.nameOfClass) as ServiceProviderViewController
        
        serviceProviderViewController.callbackServiceProvider = callbackServiceProvider
        serviceProviderViewController.isQuickSearch = true
        menuItem.controller = serviceProviderViewController
        menuItem.controller?.screenTitle = menuItem.title ?? ""
        
        menuList.append(menuItem)

        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.listing.getLocalized()
        let controller: QuickSearchViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: QuickSearchViewController.nameOfClass)
        controller.callbackListing = callbackListing
        menuItem.controller = controller
        menuItem.controller?.screenTitle = menuItem.title ?? ""
        menuList.append(menuItem)
        
        menuItem = MyProfileMenuItem()
        menuItem.title = LocalizationKeys.non_instructional_local_business.getLocalized()
        let nilbVC: LocalBusinessSearchViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: LocalBusinessSearchViewController.nameOfClass)
        nilbVC.callbackServiceProvider = callbackServiceProvider
        nilbVC.isQuickSearch = true
        menuItem.controller = nilbVC
        menuItem.controller?.screenTitle = menuItem.title ?? ""
        menuList.append(menuItem)
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension QuickSearchOptionsListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: PMProfileViewCell.nameOfClass, for: indexPath) as! PMProfileViewCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.btnTextNImage.setTitle(menuList[indexPath.row].title, for: .normal)
        cell.btnTextNImage.setImage(menuList[indexPath.row].imageIcon ?? nil, for: UIControl.State.normal)
        cell.arrow.isHidden = menuList[indexPath.row].isArrowHidden
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(self.btnClose)){
            
            if(!self.isViewControllerExist()) {
                dismiss(animated: false, completion: nil)
                
                let myAccountMenuItem = menuList[indexPath.row]

                if(myAccountMenuItem.controller != nil && !self.isViewControllerPushed){
                    self.isViewControllerPushed = true
                    HelperConstant.appDelegate.navigationController?.present(myAccountMenuItem.controller!, animated: true, completion: nil)
                }
            }
        }
    }
}

extension QuickSearchOptionsListViewController{
    func isViewControllerExist() -> Bool{
        
        var isFound = false
        
        for controller in (HelperConstant.appDelegate.navigationController?.viewControllers)! {
            
            if (controller.nameOfClass == ServiceProviderViewController.nameOfClass){
                isFound = true
                break
            } else if (controller.nameOfClass == ServiceIndividualViewController.nameOfClass){
                isFound = true
                break
            } else if(controller.nameOfClass == CalendarViewController.nameOfClass){
                isFound = true
                break
            }
        }
        
        return isFound
    }
}

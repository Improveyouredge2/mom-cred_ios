//
//  QuickSearchViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class QuickSearchViewController: LMBaseViewController {
    
    //MARK: - IBOutlet(s)
    @IBOutlet weak var dropDownFieldCategory: DropDown!
    @IBOutlet weak var dropDownSpecificCategory: DropDown!
    @IBOutlet weak var dropDownServiceTypeOption: DropDown!
    @IBOutlet weak var dropDownFrontOffice: DropDown!
    @IBOutlet weak var dropDownEventType: DropDown!
    @IBOutlet weak var dropDownStdExpectional: DropDown!
    @IBOutlet weak var dropDownPurchaseOption: DropDown!
    @IBOutlet weak var dropDownCreditSystem: DropDown!
    @IBOutlet weak var dropDownCharitable: DropDown!
    @IBOutlet weak var inputPriceMin: RYFloatingInput!
    @IBOutlet weak var inputPriceMax: RYFloatingInput!

    @IBOutlet weak var viewAdvanceSearch: UIView!
    
    //MARK: - Var(s)
    var specificCategoryList: [ListingDataDetail]?
    var presenter = QuickSearchPresenter()
        
    var callbackListing: ((_ serviceList: [ServiceAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceSearchRequest: ServiceSearchRequest?) -> Void?)?
    var callbackServiceProvider: ((_ busiList: [BIAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceProviderSearchRequest: ServiceProviderSearchRequest?) -> Void?)?

    fileprivate var quickSearchRequest = QuickSearchRequest()
    var currentPage: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)
                       
        // Field Category
        let fields = DashboardPresenter.parentListingList?.parentListingData?.fields
        dropDownFieldCategory.optionArray = fields?.compactMap {  $0.listing_title } ?? []
        dropDownFieldCategory.optionIds = fields?.compactMap({ $0.listing_id }).compactMap({ Int($0) }) ?? []
        
        // The the Closure returns Selected Index and String
        dropDownFieldCategory.didSelect{ [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.quickSearchRequest.specificField = ""
            self?.presenter.serverChildListingRequest(parentId: "\(id)")
        }
        
        // Service type
        let subcats = DashboardPresenter.parentListingList?.parentListingData?.services_types?.first?.subcat
        dropDownServiceTypeOption.optionArray = subcats?.compactMap { $0.listing_title } ?? []
        dropDownServiceTypeOption.optionIds = subcats?.compactMap({ $0.listing_id }).compactMap({ Int($0) }) ?? []
        
        // The the Closure returns Selected Index and String
        dropDownServiceTypeOption.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.quickSearchRequest.instruction_service_type = "\(id)"
        }
        
        // Front Office Service type
        let subcatsFrontOffice = DashboardPresenter.parentListingList?.parentListingData?.services_types?.last?.subcat
        dropDownFrontOffice.optionArray = subcatsFrontOffice?.compactMap { $0.listing_title } ?? []
        dropDownFrontOffice.optionIds = subcatsFrontOffice?.compactMap({ $0.listing_id }).compactMap({ Int($0) }) ?? []
        
        // The the Closure returns Selected Index and String
        dropDownFrontOffice.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.quickSearchRequest.front_office_service = "\(id)"
        }

        // Event type
        dropDownEventType.optionArray = ["Admission", "Race", "Tryout"]
        dropDownEventType.optionIds = [1, 2, 3]
        
        // The the Closure returns Selected Index and String
        dropDownEventType.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.quickSearchRequest.service_event = "\(id)"
        }

        // Purchase Option
        dropDownPurchaseOption.optionArray = ["Yes", "No"]
        dropDownPurchaseOption.optionIds = [1, 2]
        
        // The the Closure returns Selected Index and String
        dropDownPurchaseOption.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.quickSearchRequest.purchasablity = "\(id)"
        }
        
        // Standard Expectional
        let exceptionalClassification = DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification
        let exceptional = (exceptionalClassification?.first?.subcat ?? []) + (exceptionalClassification?.last?.subcat ?? [])
        dropDownStdExpectional.optionArray = exceptional.map{$0.listing_title ?? ""}
        dropDownStdExpectional.optionIds = exceptional.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownStdExpectional.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.quickSearchRequest.service_provider_classification = "\(id)"
        }
        
        // Credit System
        dropDownCreditSystem.optionArray = ["Yes", "No"]
        dropDownCreditSystem.optionIds = [1,2]
        dropDownCreditSystem.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            //            selectedMonth = selectedText
            self?.quickSearchRequest.credit_system = "\(id)"
        }
        
        // Charitable System
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownCharitable.optionArray = ["No external background image", "No wrapper"]
        //Its Id Values and its optional
        dropDownCharitable.optionIds = [1, 2]
        
        // The the Closure returns Selected Index and String
        dropDownCharitable.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.quickSearchRequest.service_provider_classification = "\(id)"
        }
        ////////////////////////////////////////////////////////////
        
        // Limit 2 digit
        inputPriceMin.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Min ")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputPriceMin.input.keyboardType = UIKeyboardType.numberPad

        // Limit 8 digit
        inputPriceMax.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text("")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Max ")
                .maxLength(HelperConstant.LIMIT_DURATION, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputPriceMax.input.keyboardType = UIKeyboardType.numberPad
    }
    
    override func viewWillAppear(_ animated: Bool) {
        actionReset(UIButton())
    }
}

extension QuickSearchViewController{
    
    func updateSpecificFeildInfo(response: [ListingDataDetail]?){
        specificCategoryList = response ?? []
        updateServiceList()
    }
    
    func updateServiceList(){
        // Specific Category
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSpecificCategory.optionArray = specificCategoryList?.compactMap({ $0.listing_title }) ?? []
        //Its Id Values and its optional
        dropDownSpecificCategory.optionIds = specificCategoryList?.compactMap({ $0.listing_id }).compactMap({ Int($0) }) ?? []
        
        // The the Closure returns Selected Index and String
        dropDownSpecificCategory.didSelect { [weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self?.quickSearchRequest.specificField = "\(id)"
        }
        ////////////////////////////////////////////////////////////
    }
}

extension QuickSearchViewController{
    @IBAction func actionClose(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionGetResult(_ sender: UIButton) {
        Spinner.show()

        quickSearchRequest.minprice = inputPriceMin.text() ?? ""
        quickSearchRequest.maxprice = inputPriceMax.text() ?? ""

        presenter.serverQuickSearchListing(quickSearchRequest: quickSearchRequest)
    }
    
    @IBAction func actionAdvanceSearch(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
        
        Helper.sharedInstance.openAdvanceSearchOptionList(callbackListing: callbackListing, callbackServiceProvider: callbackServiceProvider)
    }
    
    @IBAction func actionReset(_ sender: UIButton) {
        dropDownFieldCategory.text = ""
        dropDownSpecificCategory.text = ""
        dropDownServiceTypeOption.text = ""
        dropDownPurchaseOption.text = ""
        dropDownStdExpectional.text = ""
        dropDownCreditSystem.text = ""

        currentPage = 1
        quickSearchRequest = QuickSearchRequest()
    }
}

extension QuickSearchViewController{
    func showServiceListing(service: [ServiceAddRequest]?){
        callbackListing?(service, quickSearchRequest, nil)
        dismiss(animated: true, completion: nil)
    }
    
    func showServiceProvider(busiList: [BIAddRequest]?){
        callbackServiceProvider?(busiList, quickSearchRequest, nil)
        dismiss(animated: false, completion: nil)
    }
}

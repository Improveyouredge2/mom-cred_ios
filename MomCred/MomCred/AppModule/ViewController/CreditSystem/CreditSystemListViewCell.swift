//
//  CreditSystemListViewCell.swift
//  MomCred
//
//  Created by consagous on 05/06/19.
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit

protocol  CreditSystemListViewCellDelegate {
    func updateCellInfo(cellIndex:IndexPath)
}

class CreditSystemListViewCell: UITableViewCell {

    @IBOutlet weak var cardView : ViewLayerSetup!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblServiceCount : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var btnShowHide : UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintTableViewYPosHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblSepartor: UILabel!
    
    var delegate:CreditSystemListViewCellDelegate?
    var cellIndex:IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.constraintTableViewHeight.constant = 0
        self.lblSepartor.backgroundColor = UIColor.clear
        
        self.constraintTableViewYPosHeight.constant = -20
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints() {
        //super.updateViewConstraints()
        
        if(self.tableView.contentSize.height > 0){
             tableView.scrollToBottom()
            self.constraintTableViewHeight?.constant =
                self.tableView.contentSize.height
            
            print("\(self.tableView.contentSize.height)")
        }
    }
}

extension CreditSystemListViewCell{
    
    @IBAction func actionShowHideDetail(_ sender: UIButton) {
        self.btnShowHide.isSelected = !self.btnShowHide.isSelected
        
        //TODO: Update with new functionality
        let cellDefaultHeight = 110
        if(self.btnShowHide.isSelected){
            self.constraintTableViewHeight?.constant = CGFloat(cellDefaultHeight * 5) // 5 row count
            self.constraintTableViewYPosHeight.constant = 0
            
            self.lblSepartor.backgroundColor = UIColor.lightGray
        } else {
            self.constraintTableViewYPosHeight.constant = -20
            self.constraintTableViewHeight?.constant = 0
            self.lblSepartor.backgroundColor = UIColor.clear
        }

        self.tableView.reloadData()
        
        if(delegate != nil){
            delegate?.updateCellInfo(cellIndex: cellIndex!)
        }
        
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension CreditSystemListViewCell : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        updateViewConstraints()
//        if(self.btnShowHide.isSelected){
            return 5
//        } else {
//            return 0
//        }
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CreditSystemListViewDetailCell.nameOfClass) as! CreditSystemListViewDetailCell
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //return 200
    }
}
class CreditSystemListViewDetailCell: UITableViewCell {
    
    @IBOutlet weak var cardView : ViewLayerSetup!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblCredit : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

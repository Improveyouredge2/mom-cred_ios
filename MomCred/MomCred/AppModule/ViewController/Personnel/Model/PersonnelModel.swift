//
//  PersonnelModel.swift
//  MomCred
//
//  Created by consagous on 10/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

///********************************/
////MARK:- Serialize Request
///*******************************/

/**
 *  PersonnelAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class PersonnelSocialLink: Mappable {
    var desc: String?
    var name: String?
    var title: String?

    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        desc <- map["desc"]
        name <- map["name"]
        title <- map["title"]
    }
}

class PersonnelAddRequest: Mappable {
    
    var personal_id:String?
    
    // Page 1
    var personal_name:String?
    var personal_type:[String]?
    var personal_type_title:[String]?
    var personal_jobtitle:[String]?
    var personal_classification:[String]?
    var personal_description:String?
    var personal_location_info:PersonnelBusiLocationResponseData?
    var personal_location:String?
    
    // Page 2
    var personal_field:[BusinessFieldServiceCategoryInfo]?
    
    // Page 3
    var personal_experience:String?
    var personal_certification:[BusinessQualificationInfo]?
    
    var personal_experience2 : String? //set when it is second time
    var personal_experience3 : String? //set when it is third time
    
    var personal_certification2 : [BusinessQualificationInfo]? //set when it is second time
    var personal_certification3 : [BusinessQualificationInfo]? //set when it is third time
    
    var personal_primary : String? // 0 or 1 if it is second time form
    var personal_secondary : String? // 0 or 1 if it is third time form
    
    // Page 4
    var personal_affiliates:[BusinessQualificationInfo]?
    var personal_affiliates2 : [BusinessQualificationInfo]? //set when it is second time
    var personal_affiliates3  : [BusinessQualificationInfo]? //set when it is third time
    
    // Page 5
    var personal_training_history:[BusinessQualificationInfo]?
    var personal_training_history2:[BusinessQualificationInfo]?
    var personal_training_history3:[BusinessQualificationInfo]?

    // Page 6
    var personal_accomplishment:[BusinessQualificationInfo]?
    var personal_accomplishment2 :[BusinessQualificationInfo]?
    var personal_accomplishment3 :[BusinessQualificationInfo]?
    
    // Page 7
    var personal_contact:[BusinessQualificationInfo]?
    
    // Page 8
    var personal_email:[BusinessQualificationInfo]?
    
    // Page 9
    var personal_social_link: [PersonnelSocialLink]?
    var title : String?
    
    // Page 10
    var personal_service_listing:[BusinessQualificationInfo]?
    
    // Page 11
    var personal_facility_listing:[BusinessQualificationInfo]?
    
    // Page 12
    var personal_other_associated:[BusinessQualificationInfo]?
    
    // Page 13
    var personal_additional_information:ServiceAddNotes?
    
    // Page 14
    var personal_external_link: MyFacilityExternalLink?
    
    var personal_insidelook: [ServiceInsideLookItem]?
    var personal_instructionalcontent: [ServiceInsideLookItem]?
    var personal_instructional: [ServiceInsideLookItem]?

    // For business detail display
    var doc:[BusinessAdditionalDocumentInfo]?
    var mediabusiness:[BusinessAdditionalDocumentInfo]?
    var status:String? = "0"
    var number_of_purchase:String?
    var personal_user:String?
    var pageid:NSNumber?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init() {
        //        super.init()
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        //        super.init(map: map)
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PersonnelAddRequest?{
        var personnelAddRequest:PersonnelAddRequest?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PersonnelAddRequest>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    personnelAddRequest = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PersonnelAddRequest>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        personnelAddRequest = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return personnelAddRequest ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        personal_id                  <- map["personal_id"]
        
        //Page 1
        personal_name                   <- map["personal_name"]
        personal_type                   <- map["personal_type"]
        personal_type_title             <- map["personal_type_title"]
        personal_jobtitle               <- map["personal_jobtitle"]
        personal_classification         <- map["personal_classification"]
        personal_description            <- map["personal_description"]
        personal_location_info         <- map["personal_location_info"]
        personal_location               <- map["personal_location"]
        
        //Page 2
        personal_field                  <- map["personal_field"]
        
        // Page 3
        personal_experience             <- map["personal_experience"]
        personal_certification          <- map["personal_certification"]
        
        
        personal_experience2            <-          map["personal_experience2"]
        personal_experience3            <-          map["personal_experience3"]
        personal_certification2         <-          map["personal_certification2"]
        personal_certification3         <-          map["personal_certification3"]
        personal_primary            <-          map["personal_primary"]
        personal_secondary          <-          map["personal_secondary"]
        
        // Page 4
        personal_affiliates             <- map["personal_affiliates"]
        personal_affiliates2            <- map["personal_affiliates2"]
        personal_affiliates3            <- map["personal_affiliates3"]
        // Page 5
        personal_training_history       <- map["personal_training_history"]
        personal_training_history2              <-      map["personal_training_history2"]
        personal_training_history3              <-      map["personal_training_history3"]
        
        // Page 6
        personal_accomplishment         <- map["personal_accomplishment"]
        personal_accomplishment2            <-      map["personal_accomplishment2"]
        personal_accomplishment3            <-      map["personal_accomplishment3"]
        
        // Page 7
        personal_contact                <- map["personal_contact"]
        
        // Page 8
        personal_email                  <- map["personal_email"]
        
        // Page 9
        personal_social_link            <- map["personal_social_link"]
        title           <- map["title"]
        // Page 10
        personal_service_listing        <- map["personal_service_listing"]
        
        // Page 11
        personal_facility_listing       <- map["personal_facility_listing"]
        
        personal_insidelook <- map["personal_insidelook"]
        personal_instructionalcontent <- map["personal_instructionalcontent"]
        personal_instructional <- map["personal_instructional"]

        // Page 12
        personal_other_associated       <- map["personal_other_associated"]
        
        // Page 13
        personal_additional_information <- map["personal_additional_information"]
        
        // Page 14
        personal_external_link <- map["personal_external_link"]
        
        // Page  only in response, For business detail
        doc                             <- map["externalmedia"]
        
        // For business detail
        mediabusiness                   <- map["personal_media"]
        
        pageid                          <- map["pageid"]
        
        number_of_purchase              <- map["number_of_purchase"]
        
        // In-reponse
        status                          <- map["status"]
        
        personal_user                   <- map["personal_user"]
        
    }
}

///********************************/
////MARK:- Serialize Response
///*******************************/

/**
 *  ServiceResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class PersonnelResponse : APIResponse {
    
    var data:[PersonnelAddRequest]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PersonnelResponse?{
        var personnelResponse:PersonnelResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PersonnelResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    personnelResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PersonnelResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        personnelResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return personnelResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
}

class PersonalDetailResponse: APIResponse {
    var data: PersonnelAddRequest?
    
    required init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map){
        super.mapping(map: map)
        data <- map["data"]
    }
}

class PersonnelAddResponse : APIResponse {
    
    var data:PersonnelAddResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PersonnelAddResponse?{
        var personnelAddResponse:PersonnelAddResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PersonnelAddResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    personnelAddResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PersonnelAddResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        personnelAddResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return personnelAddResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class PersonnelAddResponseData: Mappable{
        var personal_id : NSNumber?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PersonnelAddResponseData?{
            var personnelAddResponseData:PersonnelAddResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<PersonnelAddResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        personnelAddResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<PersonnelAddResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            personnelAddResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return personnelAddResponseData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            personal_id     <- map["personal_id"]
            
        }
    }
}

/**
 *  ServiceResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class PersonnelBusiLocationResponse : APIResponse {
    
    var data:PersonnelBusiLocationResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PersonnelBusiLocationResponse?{
        var personnelBusiLocationResponse:PersonnelBusiLocationResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PersonnelBusiLocationResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    personnelBusiLocationResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PersonnelBusiLocationResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        personnelBusiLocationResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return personnelBusiLocationResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
}

/**
 *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class PersonnelBusiLocationResponseData: Mappable{
    var location_exclusive_travel:String?
    var location_name:String?
    var location_address:String?
    var location_city:String?
    var location_state:String?
    var location_lat:String?
    var location_long:String?
    var location_type:String?
    var location_ownership:String?
    var location_classification:[String]?
    var location_speciality:[String]?
    var location_busi_hours:[BusniessHour]?
    
    var location_exclusive_travel_sec:String?
    var location_name_sec:String?
    var location_address_sec:String?
    var location_city_sec:String?
    var location_state_sec:String?
    var location_lat_sec:String?
    var location_long_sec:String?
    var location_type_sec:String?
    var location_ownership_sec:String?
    var location_classification_sec:[String]?
    var location_speciality_sec:[String]?
    var location_busi_hours_sec:[BusniessHour]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init(){
        
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PersonnelBusiLocationResponseData?{
        var personnelBusiLocationResponseData:PersonnelBusiLocationResponseData?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PersonnelBusiLocationResponseData>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    personnelBusiLocationResponseData = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PersonnelBusiLocationResponseData>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        personnelBusiLocationResponseData = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return personnelBusiLocationResponseData ?? nil
    }
    
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map){
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map){
        
        location_name                   <- map["location_name"]
        location_address                <- map["location_address"]
        location_city                   <- map["location_city"]
        location_state                  <- map["location_state"]
        location_lat                    <- map["location_lat"]
        location_long                   <- map["location_long"]
        location_type                   <- map["location_type"]
        location_ownership              <- map["location_ownership"]
        location_classification         <- map["location_classification"]
        location_speciality             <- map["location_speciality"]
        location_busi_hours             <- map["busi_hours"]
        location_name_sec               <- map["location_name_sec"]
        location_address_sec            <- map["location_address_sec"]
        location_city_sec               <- map["location_city_sec"]
        location_state_sec              <- map["location_state_sec"]
        location_lat_sec                <- map["location_lat_sec"]
        location_long_sec               <- map["location_long_sec"]
        location_type_sec               <- map["location_type_sec"]
        location_ownership_sec          <- map["location_ownership_sec"]
        location_classification_sec     <- map["location_classification_sec"]
        location_speciality_sec         <- map["location_speciality_sec"]
        location_busi_hours_sec         <- map["busi_hours_sec"]
        
    }
}

//
//  PersonnelAddInstructionalContentPresenter.swift
//  MomCred
//
//  Created by Sourabh on 10/08/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import Foundation

class PersonnelAddInstructionalContentPresenter {
    
    var view: PersonnelAddInstructionalContentViewController! // Object of account view screen
    
    func connectView(view: PersonnelAddInstructionalContentViewController) {
        self.view = view
    }
}

extension PersonnelAddInstructionalContentPresenter {
    func submitData(callback:@escaping (_ status:Bool, _ response: PersonnelAddResponse?, _ message: String?) -> Void){
        
        PersonnelService.updateData(serviceAddRequest: view.personnelAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, response, message)
                }
            }
        })
    }
    
    func getInstructionalContent(completionHandler: @escaping (_ response: ICResponse?) -> Void) {
        ICService.getInstructionalService { status, response, message in
            OperationQueue.main.addOperation() {
                if !status {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
                completionHandler(response)
                Spinner.hide()
            }
        }
    }
}

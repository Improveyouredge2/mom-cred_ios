//
//  PersonnelService.swift
//  MomCred
//
//  Created by consagous on 10/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  BookingService is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PersonnelService{
    
    /**
     *  Method upload screen information on server with profile data and user profiel image.
     *
     *  @param key profile information and selected image for user profile. Callback method to update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func updateData(serviceAddRequest: PersonnelAddRequest?, imageList:[UploadImageData]?, callback:@escaping (_ status:Bool, _ response: PersonnelAddResponse?, _ message: String?) -> Void) {
        
        // MultiForm
        let jsonArray = serviceAddRequest?.toJSON()
        
        //TODO: update qpi request
        let reqMultiForm = APIManager().sendPostMultiFormRequest(urlString: APIKeys.API_ADD_PERSONNEL_SERVICE_ALL, header: APIHeaders().getDefaultHeaders(), formDataParameter: jsonArray, uploadImageList: imageList)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqMultiForm, isDisplayLoader: false) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:PersonnelAddResponse? = PersonnelAddResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method upload screen information on server with profile data and user profiel image.
     *
     *  @param key profile information and selected image for user profile. Callback method to update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func updateAdditionalInfoData(businessAdditionalDocumentInfo: MultipleAdditionalDocumentInfo?, imageList:[UploadImageData]?, callback:@escaping (_ status:Bool, _ response: ImageModelResponse?, _ message: String?) -> Void) {
        
        
        // MultiForm
        var jsonArray:[String:Any] = businessAdditionalDocumentInfo?.toJSON() ?? [:]
        
        jsonArray.updateValue("\(imageList![0].imageType!)", forKey: "media_extension")
        jsonArray.updateValue("\(imageList![0].mediaCategory!)", forKey: "media_category")
        
        
        let reqMultiForm = APIManager().sendPostMultiFormRequest(urlString: APIKeys.API_UPLOAD_MULTIPLE_IMAGE, header: APIHeaders().getDefaultHeaders(), formDataParameter: jsonArray, uploadImageList: imageList)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqMultiForm, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:ImageModelResponse? = ImageModelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method to get Forms Option Parent listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func getServiceProviderDetail(callback:@escaping (_ status:Bool, _ response: PersonnelAddResponse?, _ message: String?) -> Void) {
        
        // GET
        // Convert Model request object into JSONString
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_SERVICE_ALL, header: APIHeaders().getDefaultHeaders(), strJSON: nil)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:PersonnelAddResponse? = PersonnelAddResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method to get Forms Option Parent listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func getPersonnelService(callback:@escaping (_ status:Bool, _ response: PersonnelResponse?, _ message: String?) -> Void) {
        
        // GET
        // Convert Model request object into JSONString
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_PERSONAL, header: APIHeaders().getDefaultHeaders(), strJSON: nil)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:PersonnelResponse? = PersonnelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method to get Forms Option Parent listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func getServiceByCategory(requestData: GetListingCategoryModelRequest?, callback:@escaping (_ status:Bool, _ response: ServiceResponse?, _ message: String?) -> Void) {
        
        // GET
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        var reqPost:URLRequest?
        if(requestData?.listing_category != nil || requestData?.listing_id != nil){
            reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_SERVICE_BY_CATEGORY, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        } else {
            reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_SERVICE_BY_CATEGORY, header: APIHeaders().getDefaultHeaders(), strJSON: nil)
        }
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:ServiceResponse? = ServiceResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    
//
    /**
     *  Method to get Forms Option Parent listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func getBusiLocation(callback:@escaping (_ status:Bool, _ response: PersonnelBusiLocationResponse?, _ message: String?) -> Void) {
        
        // GET
        // Convert Model request object into JSONString
//        let strJSON = requestData?.toJSONString()
        
        var reqPost:URLRequest?
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_LOCATION, header: APIHeaders().getDefaultHeaders(), strJSON: nil)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:PersonnelBusiLocationResponse? = PersonnelBusiLocationResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    class func getPersonalDetailFor(personalId: String, completion: @escaping (_ response: PersonnelAddRequest?, _ error: Error?) -> Void) {
        let requestBody = "{\"personal_id\": \(personalId)}"
        let reqPost: URLRequest? = APIManager().sendPostRequest(urlString: APIKeys.API_PERSONAL_BY_ID, header: APIHeaders().getDefaultHeaders(), strJSON: requestBody)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            if status, let responseDict = response {
                let res: PersonalDetailResponse? = PersonalDetailResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                completion(res?.data, errorWithMessage(res?.message))
            } else { // Failed
                completion(nil, errorWithMessage(error))
            }
        }
    }
    
    private class func errorWithMessage(_ message: String?) -> NSError {
        return NSError(domain: "MomCredErrorDomain", code: 1234567890, userInfo: [NSLocalizedDescriptionKey: message ?? "Unable to get service details at this time. Please try again later."])
    }
}



//
//  PersonnelDetailViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * PersonnelDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PersonnelDetailViewController : LMBaseViewController {
    
    @IBOutlet weak var lblTitleValue : UILabel!
    @IBOutlet weak var lblTypeValue : UILabel!
    @IBOutlet weak var lblJobTitleValue : UILabel!
    @IBOutlet weak var lblClassificationValue : UILabel!
    @IBOutlet weak var lblDescValue : UILabel!

    fileprivate var menuArray: [HSMenu] = []
    fileprivate var personnelAddOverviewViewController:PersonnelAddOverviewViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.edit.getLocalized())
        menuArray = [menu1]
    }
}

extension PersonnelDetailViewController{
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}

extension PersonnelDetailViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if index == 0 {
            personnelAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddOverviewViewController.nameOfClass)
            navigationController?.pushViewController(personnelAddOverviewViewController!, animated: true)
        }
    }
}

//
//  MyPersonnelDetailViewController.swift
//  MomCred
//
//  Created by consagous on 12/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import Lightbox
import AVKit

/**
 * MyPersonnelDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class MyPersonnelDetailViewController : LMBaseViewController {
    
    // Page 1
    // Profile image view
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn : UIImageView!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitleValue : UILabel!
    @IBOutlet weak var lblTypeValue : UILabel!
    @IBOutlet weak var lblJobTitleValue : UILabel!
    @IBOutlet weak var lblClassificationValue : UILabel!
    @IBOutlet weak var lblDescValue : UILabel!
    @IBOutlet weak var lblLocationValue : UILabel!
    
    // Page 2
    @IBOutlet weak var tableViewFieldServiceView: BIDetailTableView! {
        didSet {
            tableViewFieldServiceView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewFieldServiceHeight: NSLayoutConstraint!
    
    // Page 3
    @IBOutlet weak var lblExpValue : UILabel!
    @IBOutlet weak var tableViewCertificateView: BIDetailTableView! {
        didSet {
            tableViewCertificateView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewCertificateHeight: NSLayoutConstraint!
    
    // Page 4
    @IBOutlet weak var tableViewAffilicationView: BIDetailTableView! {
        didSet {
            tableViewAffilicationView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewAffilicationHeight: NSLayoutConstraint!
    
    // Page 5
    @IBOutlet weak var tableViewTrainingHistoryView: BIDetailTableView! {
        didSet {
            tableViewTrainingHistoryView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewTrainingHistoryHeight: NSLayoutConstraint!
    
    // Page 6
    @IBOutlet weak var tableViewAccomplishmentView: BIDetailTableView! {
        didSet {
            tableViewAccomplishmentView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewAccomplishmentHeight: NSLayoutConstraint!
    
    // Page 7
    @IBOutlet weak var tableViewPhoneView: BIDetailTableView! {
        didSet {
            tableViewPhoneView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewPhoneHeight: NSLayoutConstraint!
    
    // Page 8
    @IBOutlet weak var tableViewEmailView: BIDetailTableView! {
        didSet {
            tableViewEmailView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewEmailHeight: NSLayoutConstraint!
    
    // Page 9
    @IBOutlet weak var tableViewSocialLinkView: BIDetailTableView! {
        didSet {
            tableViewSocialLinkView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewSocialLinkHeight: NSLayoutConstraint!
    
    // Page 10
    @IBOutlet weak var tableViewServiceListingView: BIDetailTableView! {
        didSet {
            tableViewServiceListingView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewServiceListingHeight: NSLayoutConstraint!
    
    // Page 11
    @IBOutlet weak var tableViewFacilityListingView: BIDetailTableView! {
        didSet {
            tableViewFacilityListingView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewFacilityListingHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewInsideLookListingView: BIDetailTableView! {
        didSet {
            tableViewInsideLookListingView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewInsideLookListingHeight: NSLayoutConstraint!

    @IBOutlet weak var tableViewInstructionalContentListingView: BIDetailTableView! {
        didSet {
            tableViewInstructionalContentListingView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewInstructionalContentListingHeight: NSLayoutConstraint!

    // Page 7
    @IBOutlet weak var tableViewOtherPersonnelView: BIDetailTableView! {
        didSet {
            tableViewOtherPersonnelView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewOtherPersonalHeight: NSLayoutConstraint!
    
    // Form 15 Additional notes
    @IBOutlet weak var tableViewAddNotesListing : BIDetailTableView! {
        didSet {
            tableViewAddNotesListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewAddNotesHeight: NSLayoutConstraint!
    
    // Form 16 External link
    @IBOutlet weak var tableViewExtenalLinkListing : BIDetailTableView! {
        didSet {
            tableViewExtenalLinkListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewExtenalLinkLisitingHeight: NSLayoutConstraint!
    
    // Additional Document type display
    @IBOutlet weak var tableViewAdditionalDocument : BIDetailTableView! {
        didSet {
            tableViewAdditionalDocument.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewAdditionalDocumentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var btnOptionMenu : UIButton!
    
    var isHideEditOption = false
    
    var personnelAddRequest:PersonnelAddRequest?
    
    private var menuArray: [HSMenu] = []
    private var personnelAddOverviewViewController:PersonnelAddOverviewViewController?
    private var defaultDocumentCollectionHeight:CGFloat = 0.00
    
    private var collectionContentHeightObserver: NSKeyValueObservation?
    var shouldRefreshContent: Bool = true
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.isHidden = true
        defaultDocumentCollectionHeight = constraintDocumentCollectionHeight.constant
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.edit.getLocalized())
        menuArray = [menu1]

        setupScrInfo()
        if isHideEditOption {
            btnOptionMenu.isHidden = true
        }
        
        collectionContentHeightObserver = collectionView.observe(\.contentSize, options: [.new]) { [weak self] _, value in
            if let height = value.newValue?.height, height > 0 {
                self?.constraintDocumentCollectionHeight.constant = height
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if shouldRefreshContent, let personnelId = personnelAddRequest?.personal_id {
            PersonnelService.getPersonalDetailFor(personalId: personnelId) { (request, error) in
                DispatchQueue.main.async { [weak self] in
                    Spinner.hide()
                    if let request = request {
                        self?.personnelAddRequest = request
                        self?.setupScrInfo()
                    }
                }
            }
        }
    }
    
    private func displayString(for titles: [String]) -> String {
        let text: String = titles.reduce("") { result, input -> String in
            result.isEmpty ? "• \(input)" : "\(result)\n• \(input)"
        }
        return text
    }
    
    deinit {
        collectionContentHeightObserver?.invalidate()
    }
}

extension MyPersonnelDetailViewController {
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
    
    @IBAction func actionZoomOption(_ sender: AnyObject) {
        zoomableView()
    }
}

extension MyPersonnelDetailViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if index == 0 {
            personnelAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddOverviewViewController.nameOfClass)
            personnelAddOverviewViewController?.personnelAddRequest = personnelAddRequest
            personnelAddOverviewViewController?.isEditingPersonnel = true
            navigationController?.pushViewController(personnelAddOverviewViewController!, animated: true)
        }
    }
}

extension MyPersonnelDetailViewController{
    private func setupScrInfo() {
        
        if let personnelAddRequest = personnelAddRequest {
            constraintDocumentCollectionHeight.constant = defaultDocumentCollectionHeight
            
            collectionView.reloadData()
            if let thumb = personnelAddRequest.mediabusiness?.first?.thumb, !thumb.trim().isEmpty {
                imgProfile.sd_setImage(with: URL(string: thumb))
                
                // Check video on first pos
                if personnelAddRequest.mediabusiness?.first?.media_extension == HelperConstant.MediaExtension.Video.MediaExtensionText {
                    imgPlayBtn.isHidden = false
                } else {
                    imgPlayBtn.isHidden = true
                }
            } else {
                imgPlayBtn.isHidden = true
                imgProfile.image = nil
                constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
            }
            
            lblTitleValue.text = personnelAddRequest.personal_name
            
            if let titles = personnelAddRequest.personal_type_title, !titles.isEmpty {
                lblTypeValue.text = displayString(for: titles)
            } else {
                lblTypeValue.text = ""
            }

            if let titles = personnelAddRequest.personal_jobtitle, !titles.isEmpty {
                lblJobTitleValue.text = displayString(for: titles)
            } else {
                lblJobTitleValue.text = ""
            }

            if let titles = personnelAddRequest.personal_classification, !titles.isEmpty {
                lblClassificationValue.text = displayString(for: titles)
            } else {
                lblClassificationValue.text = ""
            }
            
            lblDescValue.text = personnelAddRequest.personal_description
            
            lblLocationValue.text = ""
            if let locationInfo = personnelAddRequest.personal_location_info {
                let attributedLocation: NSMutableAttributedString = .init()
                let boldFont: UIFont = UIFont(name: FontsConfig.FONT_TYPE_SEMI_Bold, size: 18) ?? UIFont.systemFont(ofSize: 18, weight: .semibold)
                let font = UIFont(name: FontsConfig.FONT_TYPE_Regular, size: 18) ?? UIFont.systemFont(ofSize: 18)
                let color: UIColor = UIColor(hex: "C8057A")

                if let name = locationInfo.location_name?.trim(), !name.isEmpty {
                    attributedLocation.append(NSAttributedString(string: name, attributes: [.foregroundColor: color, .font: boldFont]))
                    if let address = locationInfo.location_address?.trim(), !address.isEmpty {
                        attributedLocation.append(NSAttributedString(string: "\n\(address)", attributes: [.foregroundColor: color, .font: font]))
                    }
                } else if let name = locationInfo.location_name_sec?.trim(), !name.isEmpty {
                    attributedLocation.append(NSAttributedString(string: name, attributes: [.foregroundColor: color, .font: boldFont]))
                    if let address = locationInfo.location_address_sec?.trim(), !address.isEmpty {
                        attributedLocation.append(NSAttributedString(string: "\n\(address)", attributes: [.foregroundColor: color, .font: font]))
                    }
                }
                lblLocationValue.attributedText = attributedLocation
            } else {
                lblLocationValue.text = ""
            }

            scrollView.isHidden = false
        } else {
            scrollView.isHidden = true
        }
    }
    
    private func zoomableView() {
        // Create an array of images.
        let images: [LightboxImage]? = personnelAddRequest?.mediabusiness?.compactMap { mediaItem -> LightboxImage? in
            if let imageUrl = mediaItem.imageurl, !imageUrl.trim().isEmpty, let imageURL = URL(string: imageUrl) {
                if mediaItem.media_extension == HelperConstant.MediaExtension.Video.MediaExtensionText, let thumb = mediaItem.thumb, !thumb.trim().isEmpty, let thumbURL = URL(string: thumb) {
                    return LightboxImage(imageURL: thumbURL, text: "", videoURL: imageURL)
                } else {
                    return LightboxImage(imageURL: imageURL)
                }
            }
            return nil
        }
        if let images = images {
            // Create an instance of LightboxController.
            let controller = LightboxController(images: images)
            // Present your controller.
            if #available(iOS 13.0, *) {
                controller.isModalInPresentation  = true
                controller.modalPresentationStyle = .fullScreen
            }
            
            // Set delegates.
            controller.pageDelegate = self
            controller.dismissalDelegate = self
            
            // Use dynamic background.
            controller.dynamicBackground = true
            
            // Present your controller.
            present(controller, animated: true, completion: nil)
            
            LightboxConfig.handleVideo = { from, videoURL in
                // Custom video handling
                let videoController = AVPlayerViewController()
                videoController.player = AVPlayer(url: videoURL)
                
                from.present(videoController, animated: true) {
                    videoController.player?.play()
                }
            }
        }
    }
}


extension MyPersonnelDetailViewController: LightboxControllerPageDelegate {
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension MyPersonnelDetailViewController: LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}

extension MyPersonnelDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    /**
     *  Specify height of row in tableview with UITableViewDelegate method override.
     *
     *  @param key tableView object and heightForRowAt index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return 2
        if tableView == tableViewFieldServiceView {
            return personnelAddRequest?.personal_field?.count ?? 0
        } else if tableView == tableViewCertificateView {
            return personnelAddRequest?.personal_certification?.count ?? 0
        } else if tableView == tableViewAffilicationView {
            return personnelAddRequest?.personal_affiliates?.count ?? 0
        } else if tableView == tableViewTrainingHistoryView {
            return personnelAddRequest?.personal_training_history?.count ?? 0
        } else if tableView == tableViewAccomplishmentView {
            return personnelAddRequest?.personal_accomplishment?.count ?? 0
        } else if tableView == tableViewPhoneView {
            return personnelAddRequest?.personal_contact?.count ?? 0
        } else if tableView == tableViewEmailView {
            return personnelAddRequest?.personal_email?.count ?? 0
        } else if tableView == tableViewSocialLinkView {
            return personnelAddRequest?.personal_social_link?.count ?? 0
        } else if tableView == tableViewServiceListingView {
            return personnelAddRequest?.personal_service_listing?.count ?? 0
        } else if tableView == tableViewFacilityListingView {
            return personnelAddRequest?.personal_facility_listing?.count ?? 0
        } else if tableView == tableViewInsideLookListingView {
            return personnelAddRequest?.personal_insidelook?.count ?? 0
        } else if tableView == tableViewInstructionalContentListingView {
            return personnelAddRequest?.personal_instructionalcontent?.count ?? 0
        } else if tableView == tableViewOtherPersonnelView {
            return personnelAddRequest?.personal_other_associated?.count ?? 0
        } else if tableView == tableViewAddNotesListing {
            return personnelAddRequest?.personal_additional_information?.additionalNotes?.count ?? 0
        } else if tableView == tableViewExtenalLinkListing {
            return personnelAddRequest?.personal_external_link?.serviceExternalLinks?.count ?? 0
        } else if tableView == tableViewAdditionalDocument {
            return personnelAddRequest?.doc?.count ?? 0
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewFieldServiceView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BIFieldServiceCell.nameOfClass) as! BIFieldServiceCell
            
            let objInfo = personnelAddRequest?.personal_field![indexPath.row]
            cell.lblFieldIndex.text = "Field \(indexPath.row + 1)"
            cell.lblCatName.text = objInfo?.catFieldName
            
            if let specificField = objInfo?.add_new_specific_field, !specificField.trim().isEmpty {
                cell.lblSubCatName.text = specificField
            } else {
                cell.lblSubCatName.text = objInfo?.specificFieldName
            }
            
            if let titles = objInfo?.classification, !titles.isEmpty {
                cell.lblClassification.text = displayString(for: titles)
            } else {
                cell.lblClassification.text = ""
            }
            
            if let titles = objInfo?.technique, !titles.isEmpty {
                cell.lblTechnique.text = displayString(for: titles)
            } else {
                cell.lblTechnique.text = ""
            }

            cell.backgroundColor = UIColor.clear
            return cell
            
        } else if tableView == tableViewCertificateView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_certification![indexPath.row]
            cell.lblIndex.text = "Certification \(indexPath.row + 1)"
            cell.lblDesc.text = objInfo?.desc
            if let name = objInfo?.name, !name.trim().isEmpty {
                let titles = name.components(separatedBy: ",").joined(separator: "\n• ")
                cell.lblName.text = "• \(titles)"
            } else {
                cell.lblName.text = objInfo?.name
            }
            
            return cell
            
        } else if tableView == tableViewAffilicationView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_affiliates![indexPath.row]
            cell.lblIndex.text = "Affiliation \(indexPath.row + 1)"
            cell.lblDesc.text = objInfo?.desc
            if let name = objInfo?.name, !name.trim().isEmpty {
                let titles = name.components(separatedBy: ",").joined(separator: "\n• ")
                cell.lblName.text = "• \(titles)"
            } else {
                cell.lblName.text = objInfo?.name
            }

            return cell
            
        } else if tableView == tableViewTrainingHistoryView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_training_history![indexPath.row]
            cell.lblIndex.text = "Training history \(indexPath.row + 1)"
            cell.lblDesc.text = objInfo?.desc
            if let name = objInfo?.name, !name.trim().isEmpty {
                let titles = name.components(separatedBy: ",").joined(separator: "\n• ")
                cell.lblName.text = "• \(titles)"
            } else {
                cell.lblName.text = objInfo?.name
            }

            return cell
            
        } else if tableView == tableViewAccomplishmentView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_accomplishment![indexPath.row]
            cell.lblIndex.text = "Accomplishment \(indexPath.row + 1)"
            cell.lblDesc.text = objInfo?.desc
            if let name = objInfo?.name, !name.trim().isEmpty {
                let titles = name.components(separatedBy: ",").joined(separator: "\n• ")
                cell.lblName.text = "• \(titles)"
            } else {
                cell.lblName.text = objInfo?.name
            }

            return cell
            
        } else if tableView == tableViewPhoneView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_contact![indexPath.row]
            cell.lblIndex.text = "Number \(indexPath.row + 1)"
            cell.lblLink.text = objInfo?.type?.title
            cell.lblName.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewEmailView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_email![indexPath.row]
            cell.lblIndex.text = "Email \(indexPath.row + 1)"
            cell.lblName.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewSocialLinkView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_social_link![indexPath.row]
            cell.lblIndex.text = "Social link \(indexPath.row + 1)"
            cell.lblName.text = objInfo?.title
            cell.lblLink.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewServiceListingView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_service_listing![indexPath.row]
            cell.lblIndex.text = "Service Listing \(indexPath.row + 1)"
            cell.lblName.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewFacilityListingView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_facility_listing![indexPath.row]
            cell.lblIndex.text = "Facility Listing \(indexPath.row + 1)"
            cell.lblName.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewInsideLookListingView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_insidelook?[indexPath.row]
            cell.lblIndex.text = "Inside-look content listing \(indexPath.row + 1)"
            cell.lblName.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewInstructionalContentListingView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_instructionalcontent?[indexPath.row]
            cell.lblIndex.text = "Instructional content listing \(indexPath.row + 1)"
            cell.lblName.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewOtherPersonnelView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = personnelAddRequest?.personal_other_associated![indexPath.row]
            cell.lblIndex.text = "Personnel \(indexPath.row + 1)"
            cell.lblName.text = objInfo?.name
            cell.lblDesc.text = objInfo?.desc
            
            return cell
            
        } else if tableView == tableViewAddNotesListing {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: AdditionalTitleDescView.nameOfClass) as! AdditionalTitleDescView
            
            let objInfo = personnelAddRequest?.personal_additional_information?.additionalNotes![indexPath.row]
            cell.lblIndex.text = "Additional notes \(indexPath.row + 1)"
            cell.lblTitleDesc.text = objInfo
            return cell
            
        } else if tableView == tableViewExtenalLinkListing {
            let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
            
            let objInfo = personnelAddRequest?.personal_external_link?.serviceExternalLinks?[indexPath.row]
            cell.lblIndex.text = "External link \(indexPath.row + 1)"
            cell.lblLink.text = objInfo?.links?.first
            cell.lblDesc.text = objInfo?.description
            cell.backgroundColor = UIColor.clear
            
            return cell
            
        } else if tableView == tableViewAdditionalDocument {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationDocumentCell.nameOfClass) as! BusinessInformationAddAdditionalInformationDocumentCell
            
            let data = personnelAddRequest?.doc?[indexPath.row]
            
            cell.lblName.text = data?.media_title
            cell.lblDesc.text = data?.media_desc
            cell.cellIndex = indexPath
            cell.documentSelectionDelegate = self
            
            if let imageURLs = data?.imageurls, !imageURLs.isEmpty {
                cell.arrDocumentImagesUrl = imageURLs.compactMap { $0.thumb }
            } else if let arrImages = data?.arrImages {
                cell.arrDocumentImages = arrImages.compactMap { uploadData in
                    if let imageData = uploadData.imageThumbnailData {
                        return UIImage(data: imageData)
                    }
                    return nil
                }
            }
            
            cell.refreshScreenInfo()
            return cell
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewPhoneView {
            if let objInfo = personnelAddRequest?.personal_contact?[indexPath.row], let phone = objInfo.name?.trim(), !phone.isEmpty, let url = URL(string: "tel:\(phone)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        } else if tableView == tableViewEmailView {
            if let objInfo = personnelAddRequest?.personal_email?[indexPath.row], let email = objInfo.name {
                openMail(for: email)
            }
        } else if tableView == tableViewSocialLinkView {
            if let objInfo = personnelAddRequest?.personal_social_link?[indexPath.row], let link = objInfo.name {
                openLink(for: link)
            }
        } else if tableView == tableViewServiceListingView {
            if let serviceId = personnelAddRequest?.personal_service_listing?[indexPath.row].id {
                showServiceDetail(for: serviceId)
            }
        } else if tableView == tableViewFacilityListingView {
            if let facilityId = personnelAddRequest?.personal_facility_listing?[indexPath.row].id {
                showFacilityDetail(for: facilityId)
            }
        } else if tableView == tableViewInsideLookListingView {
            if let insideLookId = personnelAddRequest?.personal_insidelook?[indexPath.row].id {
                showInsideLookDetail(for: insideLookId)
            }
        } else if tableView == tableViewInstructionalContentListingView {
            if let instructionalId = personnelAddRequest?.personal_instructionalcontent?[indexPath.row].id {
                showInstructionalDetail(for: instructionalId)
            }
        } else if tableView == tableViewOtherPersonnelView {
            if let personnelId = personnelAddRequest?.personal_other_associated?[indexPath.row].id {
                showPersonalDetail(for: personnelId)
            }
        }else if tableView == tableViewExtenalLinkListing {
            if let link = personnelAddRequest?.personal_external_link?.serviceExternalLinks?[indexPath.row].links?.first {
                openLink(for: link)
            }
        }
        else if tableView == tableViewFieldServiceView { }
        else if tableView == tableViewCertificateView { }
        else if tableView == tableViewAffilicationView { }
        else if tableView == tableViewTrainingHistoryView { }
        else if tableView == tableViewAccomplishmentView { }
        else if tableView == tableViewAddNotesListing { }
        else if tableView == tableViewAdditionalDocument { }
    }
}

extension MyPersonnelDetailViewController : UICollectionViewDataSource {
    
    /**
     *  Specify number of rows on section in collectionView with UICollectionViewDataSource method override.
     *
     *  @param key UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (personnelAddRequest != nil && personnelAddRequest?.mediabusiness != nil && (personnelAddRequest?.mediabusiness?.count)! > 0) ? (personnelAddRequest?.mediabusiness?.count)! : 0
    }
    
    /**
     *  Implement collectionview cell in collectionView with UICollectionViewDataSource method override.
     *
     *  @param UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        let productInfo = productList?[indexPath.row]
        
        // Default product cell
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath) as! SerivceImageCell
        cell.delegate =  self
        cell.cellIndex = indexPath.row
        
        let data = personnelAddRequest?.mediabusiness?[indexPath.row]
        if let thumb = data?.thumb, !thumb.trim().isEmpty {
            cell.imgService.sd_setImage(with: URL(string: thumb))
        }
        if data?.media_extension == HelperConstant.MediaExtension.Video.MediaExtensionText {
            cell.imgPlay.isHidden = false
        } else {
            cell.imgPlay.isHidden = true
        }
        
        return cell
    }
}

extension MyPersonnelDetailViewController : UICollectionViewDelegate {
    
    /**
     *  Implement tap event on collectionview cell in collectionView with UICollectionViewDelegate method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension MyPersonnelDetailViewController : UICollectionViewDelegateFlowLayout {
    
    /**
     *  Implement collectionView with UICollectionViewDelegateFlowLayout method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:100, height: 80)
    }
}

extension MyPersonnelDetailViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        if let mediaBusiness = personnelAddRequest?.mediabusiness {
            if mediaBusiness[cellIndex].media_extension == HelperConstant.MediaExtension.Video.MediaExtensionText {
                imgPlayBtn.isHidden = false
            } else {
                imgPlayBtn.isHidden = true
            }
            if let thumb = mediaBusiness[cellIndex].thumb {
                imgProfile.sd_setImage(with: URL(string: thumb))
            }
        }
    }
}

extension MyPersonnelDetailViewController: DocumentSelectionDelegate {
    func didSelectDocument(at indexPath: IndexPath, inCellAt cellIndexPath: IndexPath) {
        if let data = personnelAddRequest?.doc?[cellIndexPath.row], let imageURLs = data.imageurls, imageURLs.count > indexPath.item, let imageURLString = imageURLs[indexPath.item].imageurls, !imageURLString.isEmpty {
            openLink(for: imageURLString)
        }
    }
}

extension MyPersonnelDetailViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        if tableView == tableViewFieldServiceView {
            constraintsTableViewFieldServiceHeight.constant = size.height
        } else if tableView == tableViewCertificateView {
            constraintsTableViewCertificateHeight.constant = size.height
        } else if tableView == tableViewAffilicationView {
            constraintsTableViewAffilicationHeight.constant = size.height
        } else if tableView == tableViewTrainingHistoryView {
            constraintsTableViewTrainingHistoryHeight.constant = size.height
        } else if tableView == tableViewAccomplishmentView {
            constraintsTableViewAccomplishmentHeight.constant = size.height
        } else if tableView == tableViewPhoneView {
            constraintsTableViewPhoneHeight.constant = size.height
        } else if tableView == tableViewEmailView {
            constraintsTableViewEmailHeight.constant = size.height
        } else if tableView == tableViewSocialLinkView {
            constraintsTableViewSocialLinkHeight.constant = size.height
        } else if tableView == tableViewServiceListingView {
            constraintsTableViewServiceListingHeight.constant = size.height
        } else if tableView == tableViewFacilityListingView {
            constraintsTableViewFacilityListingHeight.constant = size.height
        } else if tableView == tableViewInsideLookListingView {
            constraintsTableViewInsideLookListingHeight.constant = size.height
        } else if tableView == tableViewInstructionalContentListingView {
            constraintsTableViewInstructionalContentListingHeight.constant = size.height
        } else if tableView == tableViewOtherPersonnelView {
            constraintsTableViewOtherPersonalHeight.constant = size.height
        } else if tableView == tableViewAddNotesListing {
            constraintTableViewAddNotesHeight.constant = size.height
        } else if tableView == tableViewExtenalLinkListing {
            constraintTableViewExtenalLinkLisitingHeight.constant = size.height
        } else if tableView == tableViewAdditionalDocument {
            constraintTableViewAdditionalDocumentHeight.constant = size.height
        }
    }
}

//
//  MyPersonalListViewController.swift
//  MomCred
//
//  Created by consagous on 11/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


class MyPersonalListViewController: LMBaseViewController{
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tbServiceList : UITableView!
    
    @IBOutlet weak var lblNoRecordFound: UILabel!
    
    fileprivate var personnelAddOverviewViewController:PersonnelAddOverviewViewController?
    fileprivate var personnelDetailViewController:PersonnelDetailViewController?
    fileprivate var myPersonnelDetailViewController:MyPersonnelDetailViewController?
    fileprivate var menuArray: [HSMenu] = []
    
    fileprivate var presenter = MyPersonalListPresenter()
    var personnelResponse:[PersonnelAddRequest]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        tbServiceList.estimatedRowHeight = 100
        tbServiceList.rowHeight = UITableView.automaticDimension
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.add_new.getLocalized())
        menuArray = [menu1]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view.layoutSubviews()
        personnelAddOverviewViewController = nil
        personnelDetailViewController = nil
        presenter.getPersonalServiceList()
    }
}

extension MyPersonalListViewController {
    func refreshScr() {
        if tbServiceList != nil {
            tbServiceList.reloadData()
        }
    }
}

extension MyPersonalListViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        personnelDetailViewController = nil
        if index == 0 {
            
            UserDefault.removePID()
            
            personnelAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddOverviewViewController.nameOfClass)
            navigationController?.pushViewController(personnelAddOverviewViewController!, animated: true)
        } else if index == 1 {
            personnelDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelDetailViewController.nameOfClass) as PersonnelDetailViewController
            navigationController?.pushViewController(personnelDetailViewController!, animated: true)
        }
    }
}

extension MyPersonalListViewController{
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension MyPersonalListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return arrNoteList.count
        return personnelResponse != nil ? (personnelResponse?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyServiceCell.nameOfClass, for: indexPath) as! MyServiceCell
        
        if let personnelData = personnelResponse?[indexPath.row] {
            cell.lblTitle.text = personnelData.personal_name
            cell.lblDesciption.text = personnelData.personal_description            
            if let imageThumb = personnelData.mediabusiness?.first?.thumb, !imageThumb.trim().isEmpty {
                cell.imgProfilePic.sd_setImage(with: URL(string: imageThumb), placeholderImage: #imageLiteral(resourceName: "profile"))
            } else {
                cell.imgProfilePic.image = #imageLiteral(resourceName: "profile")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        if role_id == AppUser.ServiceProvider.rawValue {
            myPersonnelDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: MyPersonnelDetailViewController.nameOfClass) as MyPersonnelDetailViewController
            
            if myPersonnelDetailViewController != nil {
                myPersonnelDetailViewController?.personnelAddRequest = personnelResponse?[indexPath.row]
                navigationController?.pushViewController(myPersonnelDetailViewController!, animated: true)
            }
        } else {
            personnelDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelDetailViewController.nameOfClass) as PersonnelDetailViewController
            if personnelDetailViewController != nil {
                navigationController?.pushViewController(personnelDetailViewController!, animated: true)
            }
        }
    }
}

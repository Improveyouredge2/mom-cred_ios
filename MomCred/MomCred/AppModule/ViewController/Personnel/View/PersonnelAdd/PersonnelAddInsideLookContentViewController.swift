//
//  PersonnelAddInsideLookContentViewController.swift
//  MomCred
//
//  Created by Sourabh on 10/08/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit

class PersonnelAddInsideLookContentViewController : LMBaseViewController {
    
    fileprivate let formNumber = 23
        
    @IBOutlet weak var tblInsideLook: BIDetailTableView! {
        didSet {
            tblInsideLook.sizeDelegate = self
        }
    }
    
    @IBOutlet weak var tblInsideLookHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingPersonnel: Bool = false

    fileprivate var personnelAddICAController: PersonnelAddInstructionalContentViewController?
    fileprivate var isUpdate = false
    fileprivate var presenter = PersonnelAddInsideLookContentPresenter()
    
    private var serviceInsideLookItems: [ServiceInsideLookItem] = [] {
        didSet {
            tblInsideLook.reloadData()
        }
    }
    private var insideLookResponse: [ILCAddRequest] = [] {
        didSet {
            tblInsideLook.reloadData()
        }
    }

    var personnelAddRequest: PersonnelAddRequest?
    var screenName: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if !screenName.isEmpty {
           lbl_NavigationTitle.text = screenName
        }
        
        tblInsideLook.backgroundColor = UIColor.clear
        
        if let existingInsideLookItems = personnelAddRequest?.personal_insidelook {
            serviceInsideLookItems = existingInsideLookItems
        }
        
        presenter.getInsideLook { [weak self] response in
            if let ilcResponse = response?.data {
                self?.insideLookResponse = ilcResponse
            }
        }

        if !isEditingPersonnel {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
}

extension PersonnelAddInsideLookContentViewController {
    fileprivate func isFormValid() -> Bool{
        if let personalId = personnelAddRequest?.personal_id, !personalId.trim().isEmpty {
            personnelAddRequest?.personal_id = personalId
        } else if let personalId = UserDefault.getPID(), !personalId.trim().isEmpty {
            personnelAddRequest?.personal_id = personalId
        }
        
        personnelAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        personnelAddRequest?.personal_insidelook = serviceInsideLookItems
        return true
    }
    
    fileprivate func openNextScr() {
        personnelAddICAController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddInstructionalContentViewController.nameOfClass) as PersonnelAddInstructionalContentViewController
        personnelAddICAController?.personnelAddRequest = personnelAddRequest
        personnelAddICAController?.isEditingPersonnel = isEditingPersonnel
        navigationController?.pushViewController(personnelAddICAController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension PersonnelAddInsideLookContentViewController {
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isUpdate {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let mpDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyPersonnelDetailViewController }
            if let mpDetailVC = mpDetailVC as? MyPersonnelDetailViewController {
                mpDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(mpDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension PersonnelAddInsideLookContentViewController : UITableViewDataSource , UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceInsideLookItems.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddMoreCell.nameOfClass) as! AddMoreCell
            
            cell.textField.placeholder = "Link"
            if (cell.ViewRoleDesc != nil) {
                cell.ViewRoleDesc.isHidden = true
            }
            cell.delegate = self
            cell.refTableView = tableView
            
            let listing = insideLookResponse.map { ServiceListingList(id: $0.look_id, title: $0.look_name) }
            cell.updateDropDown(listInfo: listing)

            cell.backgroundColor = UIColor.clear
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            cell.delegate = self
            cell.lblIndex.text = "Inside-look content listing \(indexPath.row)"
            cell.lblName.text = serviceInsideLookItems[indexPath.row - 1].name
            cell.lblDesc.text = serviceInsideLookItems[indexPath.row - 1].desc

            cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
            
            cell.backgroundColor = UIColor.clear
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension PersonnelAddInsideLookContentViewController: AddMoreCellDelegate {
    func newText(text: String, textDesc: String, textRoleDesc: String, refTableView: UITableView?, selectedServiceListingList: ServiceListingList?) {
        
        let insideLookItem = ServiceInsideLookItem()
        insideLookItem.name = text
        insideLookItem.desc = textDesc
        
        if textDesc.isEmpty {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_primary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
        } else if let serviceList = selectedServiceListingList {
            insideLookItem.id = serviceList.id
            insideLookItem.name = serviceList.title

            if serviceInsideLookItems.count < HelperConstant.minimumBlocks {
                if serviceInsideLookItems.map({ $0.id }).contains(serviceList.id) {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                } else {
                    isUpdate = true
                    serviceInsideLookItems.append(insideLookItem)
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_facility_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension PersonnelAddInsideLookContentViewController: BusinessInformationQualificationAwardCellDelegate {
    func removeWebsiteInfo(cellIndex: IndexPath?) {
        isUpdate = true
        if let index = cellIndex?.row, serviceInsideLookItems.count > index {
            serviceInsideLookItems.remove(at: index)
        }
    }
}

extension PersonnelAddInsideLookContentViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        tblInsideLookHeight.constant = size.height
    }
}

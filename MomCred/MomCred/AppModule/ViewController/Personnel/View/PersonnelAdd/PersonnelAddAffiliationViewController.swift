//
//  PersonnelAddAffiliationViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

// Page 4


/**
 * PersonnelAddAffiliationViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PersonnelAddAffiliationViewController : LMBaseViewController{
    
    //    var expandTableNumber = [Int] ()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    
    @IBOutlet weak var tableViewCertificationName: UITableView!
    @IBOutlet weak var constraintsTableViewHeightForCertificationList: NSLayoutConstraint!

    @IBOutlet weak var businessInformationQualification:BusinessInformationQualification!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingPersonnel: Bool = false

    fileprivate var businessQualificationInfoList:[BusinessQualificationInfo] = []
    fileprivate let formNumber = 4
    fileprivate var personnelAddTrainingHistoryViewController:PersonnelAddTrainingHistoryViewController?
    
    var personnelAddRequest:PersonnelAddRequest?
    
    fileprivate var presenter = PersonnelAddAffiliationPresenter()
    
    var isUpdate = false
    
    var arrCertificationNameList : [String] = []

    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: For testing
        //        biAddRequest = BIAddRequest()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        businessInformationQualification.namePlaceHolder = LocalizationKeys.txt_name.getLocalized()
        businessInformationQualification.linkPlaceHolder = LocalizationKeys.txt_link.getLocalized()
        businessInformationQualification.textViewDesc.placeholder = LocalizationKeys.txt_description.getLocalized()
        businessInformationQualification.setScreenData()

        presenter.connectView(view: self)
        
        self.setScreenData()

        if !isEditingPersonnel {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableViewCertificationName.reloadData()
        self.updateCertificationNameTableHeight()

        self.personnelAddTrainingHistoryViewController = nil
        self.businessInformationQualification.inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(#colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1)) //(.white)
                .warningColor(.white)
                .placeholer("Affiliation name")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(businessQualificationInfoList.count == 0){
            self.viewTableView.isHidden = true
        } else {
            self.viewTableView.isHidden = false
        }
        
        if(self.tableView.contentSize.height > 0){
            tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension PersonnelAddAffiliationViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        if(self.personnelAddRequest != nil){
            
            if(self.personnelAddRequest?.personal_affiliates != nil && (self.personnelAddRequest?.personal_affiliates?.count)! > 0){
                self.businessQualificationInfoList = (Helper.Add_Exp_Count == 1 ) ? self.personnelAddRequest?.personal_affiliates ?? [] : []
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.personnelAddTrainingHistoryViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddTrainingHistoryViewController.nameOfClass) as PersonnelAddTrainingHistoryViewController
        
        self.personnelAddTrainingHistoryViewController?.personnelAddRequest = self.personnelAddRequest
        personnelAddTrainingHistoryViewController?.isEditingPersonnel = isEditingPersonnel
        self.navigationController?.pushViewController(self.personnelAddTrainingHistoryViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if(UserDefault.getPID() != nil && (UserDefault.getPID()?.length)! > 0  && self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = UserDefault.getPID() ?? ""
        } else if(self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = ""
        }
        
        if Helper.Add_Exp_Count == 1 {
            self.personnelAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
            
            self.personnelAddRequest?.personal_affiliates = self.businessQualificationInfoList
            
            self.personnelAddRequest?.personal_primary  =    "0"
            self.personnelAddRequest?.personal_secondary  =    "0"


        } else if Helper.Add_Exp_Count == 2 {
            self.personnelAddRequest?.pageid = 16//NSNumber(integerLiteral: self.formNumber)
            
            self.personnelAddRequest?.personal_affiliates2 = self.businessQualificationInfoList
            
            self.personnelAddRequest?.personal_primary  =    "1"
            self.personnelAddRequest?.personal_secondary  =    "0"


        } else if Helper.Add_Exp_Count == 3 {
            self.personnelAddRequest?.pageid = 20//NSNumber(integerLiteral: self.formNumber)
            
            self.personnelAddRequest?.personal_affiliates3 = self.businessQualificationInfoList
            
            self.personnelAddRequest?.personal_primary  =    "0"
            self.personnelAddRequest?.personal_secondary  =    "1"


        }
        /*
        self.personnelAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        self.personnelAddRequest?.personal_affiliates = self.businessQualificationInfoList
        */
        return true
    }
}

extension PersonnelAddAffiliationViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let mpDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyPersonnelDetailViewController }
            if let mpDetailVC = mpDetailVC as? MyPersonnelDetailViewController {
                mpDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(mpDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}


extension PersonnelAddAffiliationViewController{
    
    @IBAction func btnAddMore(_ sender : UIButton){
        if let textToAdd = self.businessInformationQualification.inputName.text(), textToAdd.count > 0{
            self.arrCertificationNameList.append(textToAdd)
            self.tableViewCertificationName.reloadData()
            self.updateCertificationNameTableHeight()
            self.businessInformationQualification.inputName.input.text = ""
        }
        
    }
    
    @IBAction func methodAddAwardAction(_ sender: UIButton){
        
        var isValid = true
        let businessQualificationInfo = BusinessQualificationInfo()
        if self.arrCertificationNameList.count > 0 {//((self.businessInformationQualification.inputName.text()?.length)! > 0){
            businessQualificationInfo.descList = self.arrCertificationNameList
            businessQualificationInfo.name = self.arrCertificationNameList.joined(separator: ",")//businessInformationQualification.inputName.text() ?? ""
            self.arrCertificationNameList.removeAll()
            self.tableViewCertificationName.reloadData()
            self.updateCertificationNameTableHeight()

        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_affiliation_name.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if((self.businessInformationQualification.textViewDesc.text?.length)! > 0){
            businessQualificationInfo.desc = businessInformationQualification.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_media_content.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(isValid && self.businessQualificationInfoList.count < HelperConstant.minimumBlocks){
            
            self.isUpdate = true
            
            self.businessInformationQualification.inputName.input.text = ""
//            self.businessInformationQualification.inputLink.input.text = ""
            
            self.businessInformationQualification.textViewDesc.text = ""
            self.businessQualificationInfoList.append(businessQualificationInfo)
            self.tableView.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension PersonnelAddAffiliationViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == self.tableViewCertificationName ? self.arrCertificationNameList.count : businessQualificationInfoList.count

//        return businessQualificationInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableViewCertificationName {
            let cell : BusinessLocationTitleView = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass, for: indexPath) as! BusinessLocationTitleView
            
            let data = self.arrCertificationNameList[indexPath.row]
            
            cell.lblTitle.text      =       data
            
            cell.btnClose?.tag = indexPath.row
            
            return cell
            
        } else {
            self.updateViewConstraints()
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = businessQualificationInfoList[indexPath.row]
            cell.lblIndex.text = "Affiliation \(indexPath.row + 1)"
            cell.cellIndex = indexPath
            cell.delegate = self
            
            //        cell.lblLink.text = objInfo.link
            let g = "• \(objInfo.name?.components(separatedBy: ",").joined(separator: "\n• ") ?? "")"

            cell.lblName.text = g //objInfo.name
            cell.lblDesc.text = objInfo.desc
            
            return cell
        }
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func closeButtonTapped(_ sender : UIButton){
        self.arrCertificationNameList.remove(at: sender.tag)
        self.tableViewCertificationName.reloadData()
        self.updateCertificationNameTableHeight()
    }
    
    func updateCertificationNameTableHeight(){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0, animations: {
                self.tableViewCertificationName.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewCertificationName.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintsTableViewHeightForCertificationList.constant = self.tableViewCertificationName.contentSize.height//heightOfTableView
            }
        }
    }
}

extension PersonnelAddAffiliationViewController:BusinessInformationQualificationAwardCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
        if(self.businessQualificationInfoList.count > 0){
            self.businessQualificationInfoList.remove(at: cellIndex?.row ?? 0)
            self.tableView.reloadData()
            
            if(businessQualificationInfoList.count == 0){
                self.viewTableView.isHidden = true
            }
        }
    }
}

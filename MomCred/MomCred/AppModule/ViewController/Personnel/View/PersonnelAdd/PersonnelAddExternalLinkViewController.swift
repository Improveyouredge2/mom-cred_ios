//
//  PersonnelAddExternalLinkViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PersonnelAddExternalLinkViewController : LMBaseViewController{
    
    fileprivate let formNumber = 14
    
    @IBOutlet weak var tableViewExternalLink: BIDetailTableView! { didSet { tableViewExternalLink.sizeDelegate = self } }
    @IBOutlet weak var constraintstableViewExternalLinkHeight: NSLayoutConstraint!
    
    @IBOutlet weak var inputLink: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var viewExternalLinkTableView: UIView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingPersonnel: Bool = false

    fileprivate var specificTargetList:[String] = []
    fileprivate var externalLinkDetailList: [MyFacilityExternalLinkItem] = []
    fileprivate var isUpdate = false
    fileprivate var personnelAddDocumentViewController:PersonnelAddDocumentViewController?
    fileprivate var presenter = PersonnelAddExternalLinkPresenter()
    
    var personnelAddRequest:PersonnelAddRequest?
    var screenName:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()

        if !isEditingPersonnel {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.personnelAddDocumentViewController = nil
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
        if(self.externalLinkDetailList.count == 0){
            self.viewExternalLinkTableView.isHidden = true
        } else {
            self.viewExternalLinkTableView.isHidden = false
        }
        
        if(self.tableViewExternalLink.contentSize.height > 0){
             tableViewExternalLink.scrollToBottom()
        }
    }
}


extension PersonnelAddExternalLinkViewController{
    fileprivate func setScreenData(){
        
        self.inputLink.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(#colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1)) //(.white)
                .warningColor(.white)
                .placeholer("Link ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )

        if(self.personnelAddRequest?.personal_external_link != nil){
            self.externalLinkDetailList = self.personnelAddRequest?.personal_external_link?.serviceExternalLinks ?? []
            self.tableViewExternalLink.reloadData()
        }
    }
    
    fileprivate func isFormValid() -> Bool{
        if(UserDefault.getPID() != nil && (UserDefault.getPID()?.length)! > 0  && self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = UserDefault.getPID() ?? ""
        } else if(self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = ""
        }
        
        self.personnelAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if (self.externalLinkDetailList.count > 0){
            let serviceListing = MyFacilityExternalLink()
            serviceListing.serviceExternalLinks = self.externalLinkDetailList
            
            self.personnelAddRequest?.personal_external_link = serviceListing
        } else {
            self.personnelAddRequest?.personal_external_link = nil
        }
        
        return true
    }

    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.personnelAddDocumentViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddDocumentViewController.nameOfClass) as PersonnelAddDocumentViewController
        
        self.personnelAddDocumentViewController?.personnelAddRequest = self.personnelAddRequest
        self.navigationController?.pushViewController(self.personnelAddDocumentViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension PersonnelAddExternalLinkViewController{
    @IBAction func methodAddExternalLinkAction(_ sender: UIButton){
        
        if(!((self.inputLink.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: self.inputLink.text() ?? ""))){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_invalid_url.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }

        if((self.inputLink.text()?.length)! > 0){
            self.isUpdate = true
            let externalLinkDetailInfo = MyFacilityExternalLinkItem()
            externalLinkDetailInfo.links = [inputLink.text() ?? ""]
            externalLinkDetailInfo.description = textViewDesc.text
            
            if(self.externalLinkDetailList.count < HelperConstant.minimumBlocks){
                externalLinkDetailList.append(externalLinkDetailInfo)
                
                self.specificTargetList = []
                self.inputLink.input.text = ""
                self.textViewDesc.text = ""
                
                self.tableViewExternalLink.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let mpDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyPersonnelDetailViewController }
            if let mpDetailVC = mpDetailVC as? MyPersonnelDetailViewController {
                mpDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(mpDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension PersonnelAddExternalLinkViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tableViewExternalLink){
            return externalLinkDetailList.count
        }
        
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
        
        if(externalLinkDetailList.count > indexPath.row){
            let objInfo = externalLinkDetailList[indexPath.row]
            cell.lblIndex.text = "External link \(indexPath.row + 1)"
            cell.cellIndex = indexPath
            cell.delegate = self
            cell.lblLink.text = objInfo.links?.first
            cell.lblDesc.text = objInfo.description
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension PersonnelAddExternalLinkViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        self.isUpdate = true
    }
}

extension PersonnelAddExternalLinkViewController: BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        self.isUpdate = true
    }
}

extension PersonnelAddExternalLinkViewController: ExternalLinkTableViewCellDelegate{
    func removeExternalLinkInfo(cellIndex:IndexPath?){
        self.isUpdate = true
        if !externalLinkDetailList.isEmpty {
            externalLinkDetailList.remove(at: cellIndex?.row ?? 0)
            tableViewExternalLink.reloadData()
        }
        
        viewExternalLinkTableView.isHidden = externalLinkDetailList.isEmpty
    }
}

extension PersonnelAddExternalLinkViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        constraintstableViewExternalLinkHeight.constant = size.height
    }
}

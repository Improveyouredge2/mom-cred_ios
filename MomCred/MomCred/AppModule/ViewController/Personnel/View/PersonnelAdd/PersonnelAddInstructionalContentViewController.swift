//
//  PersonnelAddInstructionalContentViewController.swift
//  MomCred
//
//  Created by Sourabh on 10/08/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit
import iOSDropDown

class PersonnelAddInstructionalContentViewController: LMBaseViewController {
    
    fileprivate let formNumber = 24
    
    @IBOutlet weak var tblInstructionalContent: BIDetailTableView! {
        didSet {
            tblInstructionalContent.sizeDelegate = self
        }
    }
    
    @IBOutlet weak var tblInstructionalContentHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingPersonnel: Bool = false

    fileprivate var serviceAddListingServiceViewController: ServiceAddListingServiceViewController?
    fileprivate var isUpdate = false
    fileprivate var presenter = PersonnelAddInstructionalContentPresenter()
    
    private var serviceInstructionalContentItems: [ServiceInsideLookItem] = [] {
        didSet {
            tblInstructionalContent.reloadData()
        }
    }
    private var instructionalContentResponse: [ICAddRequest] = [] {
        didSet {
            tblInstructionalContent.reloadData()
        }
    }

    var personnelAddRequest: PersonnelAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if !screenName.isEmpty {
           lbl_NavigationTitle.text = screenName
        }
        
        tblInstructionalContent.backgroundColor = UIColor.clear
        
        if let existingInstructionalContentItems = personnelAddRequest?.personal_instructionalcontent {
            serviceInstructionalContentItems = existingInstructionalContentItems
        }
        
        presenter.getInstructionalContent { [weak self] response in
            if let icResponse = response?.data {
                self?.instructionalContentResponse = icResponse
            }
        }

        if !isEditingPersonnel {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
}

extension PersonnelAddInstructionalContentViewController {
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        if let personalId = personnelAddRequest?.personal_id, !personalId.trim().isEmpty {
            personnelAddRequest?.personal_id = personalId
        } else if let personalId = UserDefault.getPID(), !personalId.trim().isEmpty {
            personnelAddRequest?.personal_id = personalId
        }

        personnelAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        personnelAddRequest?.personal_instructional = serviceInstructionalContentItems

        return true
    }
    
    fileprivate func openNextScr() {
        let personnelAddOtherPersonnelViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddOtherPersonnelViewController.nameOfClass) as PersonnelAddOtherPersonnelViewController
        
        personnelAddOtherPersonnelViewController.personnelAddRequest = self.personnelAddRequest
        personnelAddOtherPersonnelViewController.isEditingPersonnel = isEditingPersonnel
        self.navigationController?.pushViewController(personnelAddOtherPersonnelViewController, animated: true)
    }
}

//MARK:- Button Action method implementation
extension PersonnelAddInstructionalContentViewController {
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isUpdate {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let mpDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyPersonnelDetailViewController }
            if let mpDetailVC = mpDetailVC as? MyPersonnelDetailViewController {
                mpDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(mpDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension PersonnelAddInstructionalContentViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceInstructionalContentItems.count + 1
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddMoreCell.nameOfClass) as! AddMoreCell
            
            cell.textField.placeholder = "Link"
            if (cell.ViewRoleDesc != nil) {
                cell.ViewRoleDesc.isHidden = true
            }
            cell.delegate = self
            cell.refTableView = tableView
            
            let listing = instructionalContentResponse.map { ServiceListingList(id: $0.content_id, title: $0.content_name) }
            cell.updateDropDown(listInfo: listing)

            cell.backgroundColor = UIColor.clear
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            cell.delegate = self
            cell.lblIndex.text = "Instructional content listing \(indexPath.row)"
            cell.lblName.text = serviceInstructionalContentItems[indexPath.row - 1].name
            cell.lblDesc.text = serviceInstructionalContentItems[indexPath.row - 1].desc

            cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
            
            cell.backgroundColor = UIColor.clear
            return cell
        }
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension PersonnelAddInstructionalContentViewController: AddMoreCellDelegate {
    func newText(text: String, textDesc: String, textRoleDesc: String, refTableView: UITableView?, selectedServiceListingList: ServiceListingList?) {
        
        let insideLookItem = ServiceInsideLookItem()
        insideLookItem.name = text
        insideLookItem.desc = textDesc
        
        if textDesc.isEmpty {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_primary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
        } else if let serviceList = selectedServiceListingList {
            insideLookItem.id = serviceList.id
            insideLookItem.name = serviceList.title

            if serviceInstructionalContentItems.count < HelperConstant.minimumBlocks {
                if serviceInstructionalContentItems.map({ $0.id }).contains(serviceList.id) {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                } else {
                    isUpdate = true
                    serviceInstructionalContentItems.append(insideLookItem)
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_facility_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension PersonnelAddInstructionalContentViewController: BusinessInformationQualificationAwardCellDelegate {
    func removeWebsiteInfo(cellIndex: IndexPath?) {
        isUpdate = true
        if let index = cellIndex?.row, serviceInstructionalContentItems.count > index {
            serviceInstructionalContentItems.remove(at: index)
        }
    }
}

extension PersonnelAddInstructionalContentViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        tblInstructionalContentHeight.constant = size.height
    }
}

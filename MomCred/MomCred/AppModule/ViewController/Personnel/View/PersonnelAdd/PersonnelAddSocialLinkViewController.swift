//
//  PersonnelAddSocialLinkViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

// Page 9


/**
 * PersonnelAddSocialLinkViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PersonnelAddSocialLinkViewController : LMBaseViewController{
    
    //    var expandTableNumber = [Int] ()
    @IBOutlet weak var dropDownCategory: DropDown!

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    
    @IBOutlet weak var businessInformationQualification:BusinessInformationQualification!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingPersonnel: Bool = false

    private var socialLinkList: [PersonnelSocialLink] = []
//    fileprivate var businessQualificationInfoList:[BusinessQualificationInfo] = []
    fileprivate let formNumber = 9
    fileprivate var personnelAddServiceListingViewController:PersonnelAddServiceListingViewController?
    
    var personnelAddRequest:PersonnelAddRequest?
    
    var selectedSocialLink : String?
    
    fileprivate var presenter = PersonnelAddSocialLinkPresenter()
    
    var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: For testing
        //        biAddRequest = BIAddRequest()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        businessInformationQualification.namePlaceHolder = LocalizationKeys.txt_link.getLocalized()
        businessInformationQualification.textViewDesc.placeholder = LocalizationKeys.txt_description.getLocalized()
        businessInformationQualification.setScreenData()

        self.businessInformationQualification.inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(#colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1)) //(.white)
                .warningColor(.white)
                .placeholer(LocalizationKeys.txt_link.getLocalized())
//                .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        presenter.connectView(view: self)
        
        self.setScreenData()

        if !isEditingPersonnel {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.personnelAddServiceListingViewController = nil
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        viewTableView.isHidden = socialLinkList.isEmpty
        if(self.tableView.contentSize.height > 0){
            tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension PersonnelAddSocialLinkViewController:BIFieldCategoryViewDelegate{
    func updateFieldCategorySelected(optionId:String, tagIndex:Int){
//        self.presenter.serverChildListingRequest(parentId: optionId, tagIndex: tagIndex)
    }
}
extension PersonnelAddSocialLinkViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        if(self.personnelAddRequest != nil){
            
            if let existingSocialLink = personnelAddRequest?.personal_social_link {
                socialLinkList = existingSocialLink
                self.tableView.reloadData()
            }
        }
        
        dropDownCategory.optionArray = ["Facebook" , "Twitter", "YouTube", "Linkedin", "Instagram", "Snapchat", "Pintrest"]
        
        //Its Id Values and its optional
        dropDownCategory.optionIds = [1,2,3,4,5,6,7]
        
        // The the Closure returns Selected Index and String
        dropDownCategory.didSelect{[weak self] (selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index), Id: \(id)")
            self?.selectedSocialLink = selectedText
        }
        
        self.view.bringSubviewToFront(dropDownCategory)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.personnelAddServiceListingViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddServiceListingViewController.nameOfClass) as PersonnelAddServiceListingViewController
        
        self.personnelAddServiceListingViewController?.personnelAddRequest = self.personnelAddRequest
        personnelAddServiceListingViewController?.isEditingPersonnel = isEditingPersonnel
        self.navigationController?.pushViewController(self.personnelAddServiceListingViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        if(UserDefault.getPID() != nil && (UserDefault.getPID()?.length)! > 0  && self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = UserDefault.getPID() ?? ""
        } else if(self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = ""
        }
        
        self.personnelAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        self.personnelAddRequest?.title = self.selectedSocialLink
        self.personnelAddRequest?.personal_social_link = socialLinkList
        return true
    }
}

extension PersonnelAddSocialLinkViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let mpDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyPersonnelDetailViewController }
            if let mpDetailVC = mpDetailVC as? MyPersonnelDetailViewController {
                mpDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(mpDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}


extension PersonnelAddSocialLinkViewController{
    
    @IBAction func methodAddAwardAction(_ sender: UIButton){
        let socialLinkItem = PersonnelSocialLink()
        if((self.businessInformationQualification.inputName.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: businessInformationQualification.inputName.text() ?? "")){
            socialLinkItem.name = businessInformationQualification.inputName.text() ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_media_link.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((self.businessInformationQualification.textViewDesc.text?.length)! > 0){
            socialLinkItem.desc = businessInformationQualification.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_media_content.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((self.businessInformationQualification.textViewDesc.text?.length)! > 0){
            socialLinkItem.title = selectedSocialLink
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_social_media_platform.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }

        
        if socialLinkList.count < HelperConstant.minimumBlocks {
            self.isUpdate = true
            self.businessInformationQualification.inputName.input.text = ""
            self.businessInformationQualification.textViewDesc.text = ""
            dropDownCategory.selectedIndex = nil
            dropDownCategory.text = nil
            
            self.socialLinkList.append(socialLinkItem)
            self.tableView.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension PersonnelAddSocialLinkViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return socialLinkList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
        
        let objInfo = socialLinkList[indexPath.row]
        cell.lblIndex.text = "Social link \(indexPath.row + 1)"
        cell.cellIndex = indexPath
        cell.delegate = self
        cell.lblName.text = objInfo.title
        cell.lblLink.text = objInfo.name
        cell.lblDesc.text = objInfo.desc
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension PersonnelAddSocialLinkViewController:BusinessInformationQualificationAwardCellDelegate{
    func removeWebsiteInfo(cellIndex:IndexPath?){
        if let cellIndex = cellIndex, socialLinkList.count > cellIndex.row {
            self.socialLinkList.remove(at: cellIndex.row)
            self.tableView.reloadData()
            
            viewTableView.isHidden = socialLinkList.isEmpty
        }
    }
}

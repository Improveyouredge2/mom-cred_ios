//
//  PersonnelAddExpViewController
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

extension Helper {
    static var Add_Exp_Count = 1
}

import Foundation

// Page 3

/**
 * PersonnelAddExpViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PersonnelAddExpViewController : LMBaseViewController{
    
    @IBOutlet weak var addExpAnotherContainer : UIView?
    @IBOutlet weak var lblAddExpAnother : UILabel?
    @IBOutlet weak var btnYes : UIButton?
    @IBOutlet weak var btnNo : UIButton?

    //    var expandTableNumber = [Int] ()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!

    @IBOutlet weak var tableViewCertificationName: UITableView!
    @IBOutlet weak var constraintsTableViewHeightForCertificationList: NSLayoutConstraint!
    
    @IBOutlet weak var inputNumExp: RYFloatingInput!
    
    @IBOutlet weak var businessInformationQualification:BusinessInformationQualification!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingPersonnel: Bool = false

    fileprivate var businessQualificationInfoList:[BusinessQualificationInfo] = []
    fileprivate let formNumber = 3
    fileprivate var personnelAddAffiliationViewController:PersonnelAddAffiliationViewController?
    
    var personnelAddRequest:PersonnelAddRequest?
    
    fileprivate var presenter = PersonnelAddExpPresenter()
    
    var isUpdate = false
    
    var arrCertificationNameList : [String] = []
    
    var isForAddAnotherExpereince : Bool = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: For testing
        //        biAddRequest = BIAddRequest()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        businessInformationQualification.namePlaceHolder = LocalizationKeys.txt_name.getLocalized()
        businessInformationQualification.linkPlaceHolder = LocalizationKeys.txt_link.getLocalized()
        businessInformationQualification.textViewDesc.placeholder = LocalizationKeys.txt_description.getLocalized()
        businessInformationQualification.setScreenData()
        
        self.businessInformationQualification.inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(#colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1)) //(.white)
                .warningColor(.white)
                .placeholer("Certification Name")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )

        self.inputNumExp.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(#colorLiteral(red: 0.9921568627, green: 0.831372549, blue: 0, alpha: 1)) //(.white)
                .warningColor(.white)
                //.placeholer("Years delivering services")
                .maxLength(3, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        self.inputNumExp.input.keyboardType = UIKeyboardType.numberPad
        
        presenter.connectView(view: self)
        
        self.setScreenData()

        if !isEditingPersonnel {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let count_text = Helper.Add_Exp_Count == 1 ? "first" : (Helper.Add_Exp_Count == 2 ? "second" : "third")
        self.lblAddExpAnother?.text = "Do you want to add experience for a \(count_text) field?"
        if self.isForAddAnotherExpereince {
            self.addExpAnotherContainer?.isHidden = false
            self.businessInformationQualification?.isHidden = true
            self.btnToggleActions(self.btnNo!)
        } else {
            self.addExpAnotherContainer?.isHidden = true
            self.businessInformationQualification?.isHidden = false
        }
        
        self.personnelAddAffiliationViewController = nil
        self.tableViewCertificationName.reloadData()
        self.updateCertificationNameTableHeight()
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(businessQualificationInfoList.count == 0){
            self.viewTableView.isHidden = true
        } else {
            self.viewTableView.isHidden = false
        }
        
        if(self.tableView.contentSize.height > 0){
            tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension PersonnelAddExpViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        if(self.personnelAddRequest != nil){
            
            if(self.personnelAddRequest?.personal_certification != nil && (self.personnelAddRequest?.personal_certification?.count)! > 0){
                self.businessQualificationInfoList = (Helper.Add_Exp_Count == 1 ) ? (self.personnelAddRequest?.personal_certification ?? []) : []
                self.tableView.reloadData()
            }
            
            if(self.personnelAddRequest?.personal_experience != nil){
                self.inputNumExp.input.text = self.personnelAddRequest?.personal_experience ?? ""
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.personnelAddRequest?.personal_experience != self.inputNumExp.text()){
            isUpdate = true
        }
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.personnelAddAffiliationViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddAffiliationViewController.nameOfClass) as PersonnelAddAffiliationViewController
        
        self.personnelAddAffiliationViewController?.personnelAddRequest = self.personnelAddRequest
        personnelAddAffiliationViewController?.isEditingPersonnel = isEditingPersonnel
        self.navigationController?.pushViewController(self.personnelAddAffiliationViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if(self.inputNumExp.text()?.trim().length == 0){
            message = LocalizationKeys.error_provide_personnel_yr_exp.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if(UserDefault.getPID() != nil && (UserDefault.getPID()?.length)! > 0  && self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = UserDefault.getPID() ?? ""
        } else if(self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = ""
        }
        
        if Helper.Add_Exp_Count == 1 {
            self.personnelAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
            
            self.personnelAddRequest?.personal_certification = self.businessQualificationInfoList
            
            self.personnelAddRequest?.personal_experience = self.inputNumExp.text() ?? "0"
            
            self.personnelAddRequest?.personal_primary  =    "0"
            self.personnelAddRequest?.personal_secondary  =    "0"

        } else if Helper.Add_Exp_Count == 2 {
            self.personnelAddRequest?.pageid = 15//NSNumber(integerLiteral: self.formNumber)
            
            self.personnelAddRequest?.personal_certification2 = self.businessQualificationInfoList
            
            self.personnelAddRequest?.personal_experience2 = self.inputNumExp.text() ?? "0"
            
            self.personnelAddRequest?.personal_primary  =    "1"
            self.personnelAddRequest?.personal_secondary  =    "0"

        } else {
            self.personnelAddRequest?.pageid = 19//NSNumber(integerLiteral: self.formNumber)
            
            self.personnelAddRequest?.personal_certification3 = self.businessQualificationInfoList
            
            self.personnelAddRequest?.personal_experience3 = self.inputNumExp.text() ?? "0"

            self.personnelAddRequest?.personal_primary  =    "0"
            self.personnelAddRequest?.personal_secondary  =    "1"
        }
        /*
        self.personnelAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        self.personnelAddRequest?.personal_certification = self.businessQualificationInfoList
        
        self.personnelAddRequest?.personal_experience = self.inputNumExp.text() ?? "0"
        */
        return true
    }
}

extension PersonnelAddExpViewController{
    
    
    override func actionNavigationBack(_ sender: Any) {
        if Helper.Add_Exp_Count != 1 {
            Helper.Add_Exp_Count -= 1
        }
        super.actionNavigationBack(sender)
    }
    
    @IBAction func btnToggleActions(_ sender : UIButton){
        sender.isSelected = true
        
        if sender == self.btnYes {
            self.btnNo?.isSelected = false
            self.businessInformationQualification?.isHidden = false
            self.viewTableView.isHidden = false
        } else {
            self.btnYes?.isSelected = false
            self.businessInformationQualification?.isHidden = true
            self.viewTableView.isHidden = true
        }
        
    }
    
    @IBAction func btnAddMore(_ sender : UIButton){
        if let textToAdd = self.businessInformationQualification.inputName.text(), textToAdd.count > 0{
            self.arrCertificationNameList.append(textToAdd)
            self.tableViewCertificationName.reloadData()
            self.updateCertificationNameTableHeight()
            self.businessInformationQualification.inputName.input.text = ""
        }
        
    }

    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if Helper.Add_Exp_Count > 1 && self.btnNo?.isSelected ?? false {
                let personnelAddPhoneViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddPhoneViewController.nameOfClass) as PersonnelAddPhoneViewController
                personnelAddPhoneViewController.personnelAddRequest = self.personnelAddRequest
                navigationController?.pushViewController(personnelAddPhoneViewController, animated: true)
                return
            }

            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }
        
    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let mpDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyPersonnelDetailViewController }
            if let mpDetailVC = mpDetailVC as? MyPersonnelDetailViewController {
                mpDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(mpDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }

}


extension PersonnelAddExpViewController{
    
    @IBAction func methodAddAwardAction(_ sender: UIButton){
        let businessQualificationInfo = BusinessQualificationInfo()
        if self.arrCertificationNameList.count > 0 {//((self.businessInformationQualification.inputName.text()?.length)! > 0){
            businessQualificationInfo.descList = self.arrCertificationNameList
            businessQualificationInfo.name = self.arrCertificationNameList.joined(separator: ",")//businessInformationQualification.inputName.text() ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_certification_name.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if((self.businessInformationQualification.textViewDesc.text?.length)! > 0){
            businessQualificationInfo.desc = businessInformationQualification.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_media_content.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(self.businessQualificationInfoList.count < HelperConstant.minimumBlocks){
            
            self.isUpdate = true
            
            self.businessInformationQualification.inputName.input.text = ""
//            self.businessInformationQualification.inputLink.input.text = ""
            
            businessQualificationInfo.name = self.arrCertificationNameList.joined(separator: ",")//businessInformationQualification.inputName.text() ?? ""
            self.arrCertificationNameList.removeAll()
            self.tableViewCertificationName.reloadData()
            self.updateCertificationNameTableHeight()

            self.businessInformationQualification.textViewDesc.text = ""
            self.businessQualificationInfoList.append(businessQualificationInfo)
            self.tableView.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension PersonnelAddExpViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == self.tableViewCertificationName ? self.arrCertificationNameList.count : businessQualificationInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableViewCertificationName {
            let cell : BusinessLocationTitleView = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass, for: indexPath) as! BusinessLocationTitleView
            
            let data = self.arrCertificationNameList[indexPath.row]
        
            cell.lblTitle.text      =       data
            
            cell.btnClose?.tag = indexPath.row
                        
            return cell
            
        } else {
            self.updateViewConstraints()
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = businessQualificationInfoList[indexPath.row]
            cell.lblIndex.text = "Certification \(indexPath.row + 1)"
            cell.cellIndex = indexPath
            cell.delegate = self

            if let name = objInfo.name, !name.trim().isEmpty {
                let titles = name.components(separatedBy: ",").joined(separator: "\n• ")
                cell.lblName.text = "• \(titles)"
            } else {
                cell.lblName.text = ""
            }
            cell.lblDesc.text = objInfo.desc
            
            return cell
        }
    }
    
    @IBAction func closeButtonTapped(_ sender : UIButton){
        self.arrCertificationNameList.remove(at: sender.tag)
        self.tableViewCertificationName.reloadData()
        self.updateCertificationNameTableHeight()
    }
    
    func updateCertificationNameTableHeight(){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0, animations: {
                self.tableViewCertificationName.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewCertificationName.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintsTableViewHeightForCertificationList.constant = self.tableViewCertificationName.contentSize.height//heightOfTableView
            }
        }
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension PersonnelAddExpViewController:BusinessInformationQualificationAwardCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
        if(self.businessQualificationInfoList.count > 0){
            self.businessQualificationInfoList.remove(at: cellIndex?.row ?? 0)
            self.tableView.reloadData()
            
            if(businessQualificationInfoList.count == 0){
                self.viewTableView.isHidden = true
            }
        }
    }
}

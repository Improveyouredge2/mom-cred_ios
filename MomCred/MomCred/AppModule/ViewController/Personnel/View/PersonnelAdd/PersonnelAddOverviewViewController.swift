//
//  PersonnelAddOverviewViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import AVKit
import iOSDropDown

// Page 1
class PersonnelAddOverviewViewController : LMBaseViewController{
    
    private let formNumber = 1
    
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn : UIImageView!
    @IBOutlet weak var documentImgCollectionView: UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var inputName: RYFloatingInput!
    
    @IBOutlet weak var collectionViewPeronnelType: DynamicHeightCollectionView!
    @IBOutlet weak var constraintCollectionPersonnelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewJob: BIDetailTableView! { didSet { tableViewJob.sizeDelegate = self } }
    @IBOutlet weak var constraintTableViewJobHeight: NSLayoutConstraint!
    @IBOutlet weak var tableViewClassification: BIDetailTableView! { didSet { tableViewClassification.sizeDelegate = self } }
    @IBOutlet weak var constraintTableViewClassificationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var textViewDescription: KMPlaceholderTextView!
    
    @IBOutlet weak var dropDownLocation: DropDown!
    @IBOutlet weak var lblLocationAddress: UILabel!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingPersonnel: Bool = false

    var arrDocumentImages = [UploadImageData]()
    
    private var personalTypeId:[String] = []
    
    private var presenter = PersonnelAddOverviewPresenter()
    
    private var personalType : [ListingDataDetail]?
    private var jobTitleList:[String] = []
    private var classificationTitleList:[String] = []
    private var isUpdate = false
    
    var personnelAddRequest:PersonnelAddRequest? = PersonnelAddRequest()
    var busiLocation:PersonnelBusiLocationResponseData?
    
    private var defaultDocumentCollectionHeight:CGFloat = 0.00
    
    private var logoutViewController:PMLogoutView?
    private var selectedIndex = -1
    private var selectedDocumentImages:UploadImageData?
    private var personnelAddFieldServiceViewController:PersonnelAddFieldServiceViewController?
    var isScreenDataReload = false
    var screenName:String = ""
    var selectedLocationIndex:String? = "0"
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isScreenDataReload = true
        presenter.connectView(view: self)
        
        defaultDocumentCollectionHeight = constraintDocumentCollectionHeight.constant
        constraintDocumentCollectionHeight.constant = 0.0
        
        checkBusinessLocalImage()
        
        presenter.serverBusiLocationRequest()
        
        if !screenName.trim().isEmpty {
            lbl_NavigationTitle.text = screenName
        }
        
        setScreenData()
        
        if !isEditingPersonnel {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        personnelAddFieldServiceViewController = nil
        
        documentImgCollectionView.backgroundColor = UIColor.clear
        
        if isScreenDataReload {
            presenter.serverParentListingRequest()
        }
    }
}

extension PersonnelAddOverviewViewController{
    
    /**
     *  Check local image in business information id folder
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    private func checkBusinessLocalImage() {
        if let personalRequest = personnelAddRequest {
            var busi_id = ""
            if let personalId = personalRequest.personal_id, !personalId.trim().isEmpty {
                busi_id = personalId
            } else if let personalId = UserDefault.getPID(), !personalId.trim().isEmpty {
                busi_id = personalId
            }
            
            let folderName = "service_\(busi_id)"
            if PMFileUtils.directoryExistsAtPath(folderName) {
                
                let fileUrls:[URL]? = PMFileUtils.getFolderAllFileUrls(folderName: folderName)
                print("Urls: \(fileUrls ?? [])")
                
                if let fileUrls = fileUrls, !fileUrls.isEmpty {
                    do {
                        for fileUrl in fileUrls {
                            let imageData = try Data(contentsOf: fileUrl)
                            let theFileName = (fileUrl.absoluteString as NSString).lastPathComponent
                            let theExt = (fileUrl.absoluteString as NSString).pathExtension

                            let imageDataTemp = UploadImageData()
                            
                            imageDataTemp.imageData = imageData
                            imageDataTemp.imageName = theFileName
                            imageDataTemp.imageKeyName = "busi_img"
                            
                            if ["png", "jpeg", "jpg"].contains(theExt.lowercased()) {
                                imageDataTemp.imageType = "1"
                            } else {
                                imageDataTemp.imageType = "2"
                            }
                            
                            arrDocumentImages.append(imageDataTemp)
                        }
                    } catch {
//                    print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
                        print("Error while enumerating files")
                    }
                    
                    if !arrDocumentImages.isEmpty {
                        documentImgCollectionView.reloadData()
                    }
                }
            }
        }
    }
    
    func updateScreenListing() {
        personalType = PersonnelAddOverviewPresenter.parentListingList?.parentListingData?.personaltype ?? []
        
        collectionViewPeronnelType.reloadData()
        collectionViewPeronnelType.layoutIfNeeded()
    }
    
    func updateLocationInfo() {
        
        // dropDownLocation
        // Location list
        ////////////////////////////////////////////////////////////
        dropDownLocation.optionArray = []
        dropDownLocation.optionIds = []
        
        if let busiLocation = busiLocation {
            if let locationName = busiLocation.location_name, !locationName.trim().isEmpty {
                dropDownLocation.optionArray.append(locationName)
                dropDownLocation.optionIds?.append(1)
            }
            
            if let locationName = busiLocation.location_name_sec, !locationName.trim().isEmpty {
                dropDownLocation.optionArray.append(locationName)
                dropDownLocation.optionIds?.append(2)
            }
            
            // The the Closure returns Selected Index and String
            dropDownLocation.didSelect { [weak self, weak busiLocation] (selectedText , index ,id) in
                print("Selected String: \(selectedText) \n index: \(index)")
                self?.selectedLocationIndex = "\(id)"
                
                var address = ""
                
                if let locationName = busiLocation?.location_name, !locationName.isEmpty, index == 0 {
                    address = "\(locationName) \n\(busiLocation?.location_address ?? "")"
                } else if let locationName = busiLocation?.location_name_sec, !locationName.isEmpty {
                    address = "\(locationName) \n\(busiLocation?.location_address_sec ?? "")"
                }
                self?.lblLocationAddress.text = address
                self?.isUpdate = true
            }
        }
    }
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData() {
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(#colorLiteral(red: 0.9921568627, green: 0.831372549, blue: 0, alpha: 1)) //(.white)
                .warningColor(.white)
                .placeholer("Personnel Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        if let mediaBusiness = personnelAddRequest?.mediabusiness, let mediaItem = mediaBusiness.first {
            if let thumb = mediaItem.thumb, let thumbURL = URL(string: thumb) {
                imgProfile.sd_setImage(with: thumbURL)
            }
            
            // Check video on first pos
            if mediaItem.media_extension == HelperConstant.MediaExtension.Video.MediaExtensionText {
                imgPlayBtn.isHidden = false
            } else {
                imgPlayBtn.isHidden = true
            }
            
            constraintDocumentCollectionHeight.constant = defaultDocumentCollectionHeight
        }
        
        inputName.input.text = personnelAddRequest?.personal_name
        jobTitleList = personnelAddRequest?.personal_jobtitle ?? []
        classificationTitleList = personnelAddRequest?.personal_classification ?? []
        textViewDescription.text = personnelAddRequest?.personal_description
        personalTypeId = personnelAddRequest?.personal_type ?? []
        
        if let locationInfo = personnelAddRequest?.personal_location_info {
            var locationName = ""
            var address = ""

            if let name = locationInfo.location_name, !name.trim().isEmpty {
                locationName = name
                if let add = locationInfo.location_address {
                    address = "\(name)\n\(add)"
                }
            } else if let name = locationInfo.location_name_sec, !name.trim().isEmpty {
                locationName = name
                if let add = locationInfo.location_name_sec {
                    address = "\(name)\n\(add)"
                }
            }
            
            dropDownLocation.text = locationName
            lblLocationAddress.text = address
        } else {
            lblLocationAddress.text = ""
        }
        selectedLocationIndex = personnelAddRequest?.personal_location
                
        // update collection info
        if let mediaBusiness = personnelAddRequest?.mediabusiness, !mediaBusiness.isEmpty {
            for mediaItem in mediaBusiness {
                let mediaImage = UploadImageData()
                mediaImage.imageUrl = mediaItem.thumb
                mediaImage.id = mediaItem.media_id
                if let mediaExt = Int(mediaItem.media_extension ?? "0") {
                    if mediaExt == 0 || mediaExt == 1 {
                        mediaImage.imageType = "1"
                    } else {
                        mediaImage.imageType = "2"
                    }
                } else {
                    mediaImage.imageType = "1"
                }
                arrDocumentImages.append(mediaImage)
            }
            documentImgCollectionView.reloadData()
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    private func isDataUpdate() -> Bool {
        var isUpdate = false
        if personnelAddRequest?.personal_name != inputName.text()?.trim() {
            isUpdate = true
        }

        if textViewDescription.text != personnelAddRequest?.personal_description {
            isUpdate = true
        }
        
        if self.isUpdate {
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    private func openNextScr() {
        personnelAddFieldServiceViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddFieldServiceViewController.nameOfClass) as PersonnelAddFieldServiceViewController
        
        personnelAddFieldServiceViewController?.personnelAddRequest = personnelAddRequest
        personnelAddFieldServiceViewController?.isEditingPersonnel = isEditingPersonnel
        navigationController?.pushViewController(personnelAddFieldServiceViewController!, animated: true)
        
        //        let controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceAddAdditionalCostViewController.nameOfClass) as ServiceAddAdditionalCostViewController
        //
        //        controller.serviceAddRequest = serviceAddRequest
        //
        //        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func isVideoInList() -> Bool{
        if !arrDocumentImages.isEmpty {
            var isFound = false
            for imageData in arrDocumentImages {
                if let imageUrl = imageData.imageUrl, !imageUrl.trim().isEmpty {
                    if imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame {
                        isFound = true
                    }
                } else {
                    if imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame {
                        isFound = true
                    }
                }
            }
            return isFound
        }
        return false
    }
    
}

extension PersonnelAddOverviewViewController: UITextFieldDelegate{
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        let numCheckStatus:Bool = false
        var lenCheckStatus:Bool = false
        let isNumCheckReq = false
        
        // check backspace in input character
        if (isBackSpace == -92) {
            return true
        }
        
        // check new length of string after adding new character
        let newLength = text.count + string.count - range.length
        if newLength > HelperConstant.LIMIT_PURCHASEBILITY{
            return false
        }
        
        lenCheckStatus = true
        
        if isNumCheckReq {
            return numCheckStatus && lenCheckStatus
        }else if lenCheckStatus {
            return lenCheckStatus
        }
    }
}

extension PersonnelAddOverviewViewController{
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func pickedImage(fromImagePicker: UIImage, imageData: Data, mediaType:String, imageName:String, fileUrl: String?) {
        let imageDataTemp = UploadImageData()
        let theExt = (imageName as NSString).pathExtension.lowercased()
        
        if ["png", "jpg", "jpeg"].contains(theExt) {
            imageDataTemp.imageType = "1"
        } else {
            imageDataTemp.imageType = "2"
        }
        
        if let personalId = personnelAddRequest?.personal_id, !personalId.trim().isEmpty {
            imageDataTemp.id = personalId
        } else if let personalId = UserDefault.getPID(), !personalId.trim().isEmpty {
            imageDataTemp.id = personalId
        }
        
        imageDataTemp.imageData = imageData
        imageDataTemp.imageName = "\((arrDocumentImages.count + 1))\(imageName)"
        imageDataTemp.imageKeyName = "busi_img"
        imageDataTemp.filePath = fileUrl
        
        imageDataTemp.imageType = mediaType
        imageDataTemp.imageThumbnailData = fromImagePicker.pngData()
        
        // Check of video size
        if imageDataTemp.imageType == "2" {
            if Int(Helper.sharedInstance.checkVideoSize(reqData: imageData)) < HelperConstant.LIMIT_VIDEO_SIZE {
                
                imgProfile.image = fromImagePicker
                imgPlayBtn.isHidden = false
                
                var arrTempDocumentImages = [UploadImageData]()
                arrTempDocumentImages.append(imageDataTemp)
                for arrDocument in arrDocumentImages{
                    arrTempDocumentImages.append(arrDocument)
                }
                arrDocumentImages = arrTempDocumentImages
            } else {
                showBannerAlertWith(message: LocalizationKeys.error_file_size.getLocalized(), alert: .error)
            }
        } else {
            // Display image on main view
            imgProfile.image = fromImagePicker
            imgPlayBtn.isHidden = true
            arrDocumentImages.append(imageDataTemp)
        }

        if arrDocumentImages.count > 0 && arrDocumentImages.count < HelperConstant.maximumProfileMedia {
            isUpdate = true
            constraintDocumentCollectionHeight.constant = defaultDocumentCollectionHeight
            documentImgCollectionView.reloadData()
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension PersonnelAddOverviewViewController{
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    private func isFormValid() -> Bool {
        var message = ""
        
        if let personalType = personalType {
            personalTypeId = []
            for target in personalType {
                if target.selectionStaus, let listingId = target.listing_id {
                    personalTypeId.append(listingId)
                }
            }
        }
        
        if arrDocumentImages.count == 0 {
            message = LocalizationKeys.error_select_personnel_service_image.getLocalized()
            showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (inputName.text()?.trim().isEmpty)! {
            message = LocalizationKeys.error_select_personnel_service_name.getLocalized()
            showBannerAlertWith(message: message, alert: .error)
            return false
        } else if personalTypeId.count == 0 {
            message = LocalizationKeys.error_select_personnel_type.getLocalized()
            showBannerAlertWith(message: message, alert: .error)
            return false
        } else if jobTitleList.count == 0 {
            message = LocalizationKeys.error_select_personnel_job_title.getLocalized()
            showBannerAlertWith(message: message, alert: .error)
            return false
        } else if textViewDescription.text.trim().isEmpty {
            message = LocalizationKeys.error_select_personnel_desc.getLocalized()
            showBannerAlertWith(message: message, alert: .error)
            return false
        } else if lblLocationAddress.text == "" {
            message = "Location is required" //LocalizationKeys.error_select_personnel_desc.getLocalized()
            showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if let personalId = personnelAddRequest?.personal_id, !personalId.trim().isEmpty {
            personnelAddRequest?.personal_id = personalId
        } else if let personalId = UserDefault.getPID(), !personalId.trim().isEmpty {
            personnelAddRequest?.personal_id = personalId
        }

        personnelAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        personnelAddRequest?.personal_name = inputName.text()?.trim()
        
        personnelAddRequest?.personal_jobtitle = jobTitleList
        personnelAddRequest?.personal_classification = classificationTitleList
        personnelAddRequest?.personal_description = textViewDescription.text
        personnelAddRequest?.personal_type = personalTypeId
        personnelAddRequest?.personal_location = selectedLocationIndex
        
        return true
    }
}

//MARK:- Button Action method implementation
extension PersonnelAddOverviewViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let mpDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyPersonnelDetailViewController }
            if let mpDetailVC = mpDetailVC as? MyPersonnelDetailViewController {
                mpDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(mpDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }

    /**
     *  Image picker button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnImagePickerAction(_ sender : UIButton) {
        displayPhotoSelectionOption(withCircularAllow:false)
    }
}


// MARK:- UITableViewDataSource & UITableViewDelegate
extension PersonnelAddOverviewViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewJob {
            return (jobTitleList.count) + 1
        }  else  if tableView == tableViewClassification {
            return (classificationTitleList.count) + 1
        }
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableViewJob == tableView {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                cell.textField.placeholder = "Title"
                cell.delegate = self
                cell.refTableView = tableViewJob
                cell.backgroundColor = UIColor.clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                cell.delegate = self
                cell.refTableView = tableViewJob
                cell.lblTitle.text = jobTitleList[indexPath.row-1]
                cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
                cell.backgroundColor = UIColor.clear
                return cell
            }
            
        } else if tableViewClassification == tableView {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                cell.textField.placeholder = "Ex. Coach, trainer, etc."
                cell.delegate = self
                cell.refTableView = tableViewClassification
                cell.backgroundColor = UIColor.clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                cell.delegate = self
                cell.refTableView = tableViewClassification
                cell.lblTitle.text = classificationTitleList[indexPath.row-1]
                cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
                cell.backgroundColor = UIColor.clear
                return cell
            }
        }
        
        return UITableViewCell(frame: CGRect(x:0, y: 0, width: 0, height: 0))
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension PersonnelAddOverviewViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        if refTableView == tableViewJob {
            if jobTitleList.count < HelperConstant.minimumBlocks {
                isUpdate = true
                jobTitleList.append(text)
                tableViewJob.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else if refTableView == tableViewClassification {
            if classificationTitleList.count < HelperConstant.minimumBlocks {
                isUpdate = true
                classificationTitleList.append(text)
                tableViewClassification.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

extension PersonnelAddOverviewViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?) {
        isUpdate = true
        if refTableView == tableViewJob {
            jobTitleList.remove(at: cellIndex?.row ?? 0)
            tableViewJob.reloadData()
        } else  if refTableView == tableViewClassification {
            classificationTitleList.remove(at: cellIndex?.row ?? 0)
            tableViewClassification.reloadData()
        }
    }
}

/*******************************************/
//MARK: - UICollectionViewDelegate
/*******************************************/
extension PersonnelAddOverviewViewController : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == documentImgCollectionView {
            return arrDocumentImages.count
        } else if collectionView == collectionViewPeronnelType {
            return personalType?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //MARK:- Store Images views
        if collectionView == documentImgCollectionView {
            
            //MARK:- Store Images views
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath as IndexPath) as! SerivceImageCell
            
            let imageData = arrDocumentImages[indexPath.row]
            
            cell.delegate = self
            cell.cellIndex = indexPath.row
            cell.btnDelete.isHidden = false
            cell.btnDelete.tag = indexPath.row
            
            if let imageUrl = imageData.imageUrl, !imageUrl.trim().isEmpty {
                if imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame {
                    cell.imgPlay.isHidden = false
                    cell.imgService.sd_setImage(with: URL(string: imageUrl))
                } else {
                    cell.imgPlay.isHidden = true
                    cell.imgService.sd_setImage(with: URL(string: imageUrl))
                }
            } else {
                if imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame {
                    cell.imgPlay.isHidden = false
                    cell.imgService.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                } else {
                    cell.imgPlay.isHidden = true
                    cell.imgService.image = UIImage(data: imageData.imageData ?? Data())
                }
            }
            return cell
        } else if collectionView == collectionViewPeronnelType {
            let cell = collectionViewPeronnelType.dequeueReusableCell(withReuseIdentifier: CollectionCheckboxListCVCell.nameOfClass, for: indexPath as IndexPath) as! CollectionCheckboxListCVCell
            
            constraintCollectionPersonnelHeight.constant = collectionViewPeronnelType.intrinsicContentSize.height

            var listingData: ListingDataDetail?
            if personalType != nil && (personalType?.count)! > indexPath.section {
                listingData = personalType?[indexPath.row]
            }

            if listingData != nil {
                cell.delegate = self
                cell.cellIndex = indexPath
                cell.btnCheckBox.setTitle(listingData?.listing_title ?? "", for: UIControl.State.normal)
                cell.btnCheckBox.isSelected = listingData?.selectionStaus ?? false

                if !isUpdate && personnelAddRequest?.personal_type != nil && (personnelAddRequest?.personal_type?.count)! > 0 {
                    if personnelAddRequest?.personal_type?.contains(listingData?.listing_id ?? "") ?? false {
                        cell.btnCheckBox.isSelected = true
                        listingData?.selectionStaus = true
                    }
                }

                cell.backgroundColor = .clear
            }
            return cell
        }
        return UICollectionViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == documentImgCollectionView {
            return CGSize(width:100, height: 80)
        } else if collectionView == collectionViewPeronnelType {
            return CGSize(width: (collectionView.frame.size.width ) / 2, height: 45)
        }
        
        return CGSize(width: 0, height: 0)
    }
}

extension PersonnelAddOverviewViewController:CollectionCheckboxListCVCellDelegate{
    func selectListItem(cellIndex: IndexPath?, status:Bool, refCollection: UICollectionView?) {
        if personalType != nil && (personalType?.count)! > cellIndex?.section ?? 0 {
            personalType![cellIndex?.row ?? 0].selectionStaus = status
            isUpdate = true
        }
    }
}


extension PersonnelAddOverviewViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        //TODO: Update image as per selection
        if !arrDocumentImages.isEmpty {
            let imageData = arrDocumentImages[cellIndex]
            if let imageUrl = imageData.imageUrl, !imageUrl.trim().isEmpty {
                if imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame {
                    imgPlayBtn.isHidden = false
                    imgProfile.sd_setImage(with: URL(string: imageUrl))
                } else {
                    imgPlayBtn.isHidden = true
                    imgProfile.sd_setImage(with: URL(string: imageUrl))
                }
            } else {
                if imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame {
                    imgPlayBtn.isHidden = false
                    imgProfile.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                } else {
                    imgPlayBtn.isHidden = true
                    imgProfile.image = UIImage(data: imageData.imageData ?? Data())
                }
            }
        }
    }
}


//MARK:- Button Actions
//MARK:-
extension PersonnelAddOverviewViewController{
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnAddImg(_ sender: UIButton) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if arrDocumentImages.count < HelperConstant.maximumProfileMedia {
                if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                    //already authorized
                    debugPrint("already authorized")
                    
                    OperationQueue.main.addOperation() { [weak self] in
                        guard let self = self else { return }
                        if !self.isVideoInList() {
                            self.displayPhotoSelectionOption(withCircularAllow:false, isSelectVideo: true)
                        } else {
                            self.displayPhotoSelectionOption(withCircularAllow: false)
                        }
                    }
                } else {
                    AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                        if granted {
                            //access allowed
                            debugPrint("access allowed")
                            OperationQueue.main.addOperation() { [weak self] in
                                guard let self = self else { return }
                                if !self.isVideoInList() {
                                    self.displayPhotoSelectionOption(withCircularAllow:false, isSelectVideo: true)
                                } else {
                                    self.displayPhotoSelectionOption(withCircularAllow: false)
                                }
                            }
                        } else {
                            //access denied
                            debugPrint("Access denied")
                            OperationQueue.main.addOperation() {                                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_this_app_needs_access_to_your_camera_and_gallery.getLocalized(), buttonTitle: nil, controller: nil)
                            }
                        }
                    })
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_video_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnDeleteStoreImg(_ sender: UIButton) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            let imageData = arrDocumentImages[sender.tag]
            if let imageUrl = imageData.imageUrl, !imageUrl.trim().isEmpty {
                selectedDocumentImages = imageData
                // TODO: approval alert
                logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
                logoutViewController?.delegate = self
                logoutViewController?.scrTitle = ""
                logoutViewController?.scrDesc = LocalizationKeys.error_remove_media.getLocalized()
                selectedIndex = sender.tag
                logoutViewController?.modalPresentationStyle = .overFullScreen
                present(logoutViewController!, animated: true, completion: nil)
            } else {
                if !arrDocumentImages.isEmpty {
                    arrDocumentImages.remove(at: sender.tag)
                }
                
                if arrDocumentImages.isEmpty {
                    constraintDocumentCollectionHeight.constant = 0.00
                    imgPlayBtn.isHidden = true
                    imgProfile.image = nil
                } else {
                    // Update profile image
                    let imageData = arrDocumentImages[0]
                    if let imageUrl = imageData.imageUrl, !imageUrl.trim().isEmpty {
                        if imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame {
                            imgPlayBtn.isHidden = false
                            imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                        } else {
                            imgPlayBtn.isHidden = true
                            imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                        }
                    } else {
                        if imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame {
                            imgPlayBtn.isHidden = false
                            imgProfile.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                        } else {
                            imgPlayBtn.isHidden = true
                            imgProfile.image = UIImage(data: imageData.imageData ?? Data())
                        }
                    }
                    documentImgCollectionView.reloadData()
                }
            }
        }
    }
}


extension PersonnelAddOverviewViewController: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr() {
        if logoutViewController != nil {
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr() {
        if logoutViewController != nil {
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
        
        if selectedDocumentImages != nil {
            let deleteMediaRequest = DeleteMediaRequest(media_id: selectedDocumentImages?.id ?? "")
            presenter.deleteMediaData(deleteMediaRequest: deleteMediaRequest, callback: { [weak self]
                (status, response, message) in
                guard let self = self else { return }
                if status {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    if !self.arrDocumentImages.isEmpty {
                        self.arrDocumentImages.remove(at: self.selectedIndex)
                    }
                    if self.arrDocumentImages.isEmpty {
                        self.constraintDocumentCollectionHeight.constant = 0
                        self.imgProfile.image = nil
                    }
                    self.documentImgCollectionView.reloadData()
                }
            })
        }
    }
}

extension PersonnelAddOverviewViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        if tableView == tableViewJob {
            constraintTableViewJobHeight.constant = size.height
        } else if tableView == tableViewClassification {
            constraintTableViewClassificationHeight.constant = size.height
        }
    }
}

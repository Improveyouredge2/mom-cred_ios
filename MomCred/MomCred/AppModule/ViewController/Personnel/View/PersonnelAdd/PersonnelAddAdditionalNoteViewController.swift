//
//  PersonnelAddAdditionalNoteViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

// Page 13


/**
 * PersonnelAddAdditionalNoteViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PersonnelAddAdditionalNoteViewController : LMBaseViewController{
    
    fileprivate let formNumber = 13
    
    // Event
    @IBOutlet weak var tableViewSpecificTarget: UITableView!
    
    @IBOutlet weak var constraintstableViewSpecificTargetHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingPersonnel: Bool = false

    fileprivate var specificTargetList:[String] = []
    fileprivate var isUpdate = false
    fileprivate var personnelAddExternalLinkViewController:PersonnelAddExternalLinkViewController?
    fileprivate var presenter = PersonnelAddAdditionalNotePresenter()
    
    var personnelAddRequest:PersonnelAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        self.tableViewSpecificTarget.backgroundColor = UIColor.clear
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()

        if !isEditingPersonnel {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.personnelAddExternalLinkViewController = nil
        
        //        self.presenter.getHelpAndSupport()
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        //        @IBOutlet weak var tableViewSpecificTarget: UITableView!
        //        @IBOutlet weak var constraintstableViewSpecificTargetHeight: NSLayoutConstraint!
        //        fileprivate var specificTargetList:[String] = []
        
        
        if(self.tableViewSpecificTarget.contentSize.height > 0){
            tableViewSpecificTarget.scrollToBottom()
            if((self.specificTargetList.count) == 0){
                self.constraintstableViewSpecificTargetHeight?.constant = AddMoreCell.inputViewCellSize
            } else {
                self.constraintstableViewSpecificTargetHeight?.constant = self.tableViewSpecificTarget.contentSize.height
            }
            
            self.updateTableViewHeight(tableView: tableViewSpecificTarget)
        }
        
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewSpecificTarget == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewSpecificTarget.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewSpecificTarget.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewSpecificTargetHeight.constant = heightOfTableView
            }
        }
    }
}

extension PersonnelAddAdditionalNoteViewController{
    fileprivate func setScreenData(){
        
        if(self.personnelAddRequest?.personal_additional_information != nil){
            
            self.specificTargetList = self.personnelAddRequest?.personal_additional_information?.additionalNotes ?? []
            
            self.tableViewSpecificTarget.reloadData()
        }
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        // It is optional as per client feedback
        //        if (self.specificTargetList.count == 0){
        //            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_additional_information_listing.getLocalized(), buttonTitle: nil, controller: nil)
        //            return false
        //        }
        
        if(UserDefault.getPID() != nil && (UserDefault.getPID()?.length)! > 0  && self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = UserDefault.getPID() ?? ""
        } else if(self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = ""
        }
        
        self.personnelAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        let serviceListing = ServiceAddNotes()
        if(self.specificTargetList != nil && self.specificTargetList.count > 0){
            serviceListing.additionalNotes = self.specificTargetList
        }
        
        self.personnelAddRequest?.personal_additional_information = serviceListing
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.personnelAddExternalLinkViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddExternalLinkViewController.nameOfClass) as PersonnelAddExternalLinkViewController
        
        self.personnelAddExternalLinkViewController?.personnelAddRequest = self.personnelAddRequest
        personnelAddExternalLinkViewController?.isEditingPersonnel = isEditingPersonnel
        self.navigationController?.pushViewController(self.personnelAddExternalLinkViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension PersonnelAddAdditionalNoteViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let mpDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyPersonnelDetailViewController }
            if let mpDetailVC = mpDetailVC as? MyPersonnelDetailViewController {
                mpDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(mpDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension PersonnelAddAdditionalNoteViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tableViewSpecificTarget){
//            if((self.specificTargetList.count) != HelperConstant.minimumBlocks){
                return (specificTargetList.count) + 1
//            } else {
//                return specificTargetList.count
//            }
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        if(self.tableViewSpecificTarget == tableView){
            
            if(indexPath.row == 0){
                
                let cell = tableView.dequeueReusableCell(withIdentifier: AddAdditionalMoreCell.nameOfClass) as! AddAdditionalMoreCell
                
                cell.delegate = self
                cell.refTableView = self.tableViewSpecificTarget
                
                cell.backgroundColor = UIColor.clear
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: AdditionalTitleDescView.nameOfClass) as! AdditionalTitleDescView
                
                cell.delegate = self
                //                cell.cellIndex = indexPath
                cell.refTableView = self.tableViewSpecificTarget
                
//                if(self.specificTargetList.count) != HelperConstant.minimumBlocks{
                    cell.lblTitleDesc.text = specificTargetList[indexPath.row-1]
                    
                    
                    cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
//                } else {
//                    cell.cellIndex = indexPath
//                    cell.lblTitleDesc.text = specificTargetList[indexPath.row]
//                }
                
//                cell.backgroundColor = UIColor.clear
                return cell
            }
            
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension PersonnelAddAdditionalNoteViewController: AddAdditionalMoreCellDelegate{
    func newText (textDesc:String, refTableView: UITableView?) {
        
        if(refTableView == self.tableViewSpecificTarget){
            if((self.specificTargetList.count) < HelperConstant.minimumBlocks){
                self.isUpdate = true
                self.specificTargetList.append(textDesc)
                self.tableViewSpecificTarget.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

extension PersonnelAddAdditionalNoteViewController : AdditionalTitleDescViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        self.isUpdate = true
        if(refTableView == self.tableViewSpecificTarget && self.specificTargetList.count > 0){
            self.specificTargetList.remove(at: cellIndex?.row ?? 0)
            
            self.tableViewSpecificTarget.reloadData()
        }
    }
}


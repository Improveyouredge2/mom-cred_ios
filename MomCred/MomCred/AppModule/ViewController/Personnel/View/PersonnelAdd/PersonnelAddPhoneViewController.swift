//
//  PersonnelAddPhoneViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

// Page 7


/**
 * PersonnelAddPhoneViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PersonnelAddPhoneViewController : LMBaseViewController{
    
    //    var expandTableNumber = [Int] ()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    
    @IBOutlet weak var btnOptionLandline: UIButton!
    @IBOutlet weak var btnOptionCell: UIButton!
    @IBOutlet weak var btnOptionBusiness: UIButton!

    @IBOutlet weak var businessInformationQualification:BusinessInformationQualification!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingPersonnel: Bool = false

    fileprivate var businessQualificationInfoList:[BusinessQualificationInfo] = []
    fileprivate let formNumber = 7
    fileprivate var personnelAddEmailViewController:PersonnelAddEmailViewController?
    fileprivate var activeType:DetailData?
    
    var personnelAddRequest:PersonnelAddRequest?
    
    fileprivate var presenter = PersonnelAddPhonePresenter()
    
    var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activeType = DetailData()
        
        // TODO: For testing
        //        biAddRequest = BIAddRequest()
        
        businessInformationQualification.fieldType = .Phone
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        businessInformationQualification.namePlaceHolder = LocalizationKeys.txt_phone.getLocalized()
        businessInformationQualification.textViewDesc.placeholder = LocalizationKeys.txt_description.getLocalized()
        businessInformationQualification.setScreenData()

        presenter.connectView(view: self)
        
        self.setScreenData()
        
        self.methodOptionTypeAction(self.btnOptionLandline)
        
        if !isEditingPersonnel {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.personnelAddEmailViewController = nil
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(businessQualificationInfoList.count == 0){
            self.viewTableView.isHidden = true
        } else {
            self.viewTableView.isHidden = false
        }
        
        if(self.tableView.contentSize.height > 0){
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension PersonnelAddPhoneViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        if(self.personnelAddRequest != nil){
            
            if(self.personnelAddRequest?.personal_contact != nil && (self.personnelAddRequest?.personal_contact?.count)! > 0){
                self.businessQualificationInfoList = self.personnelAddRequest?.personal_contact ?? []
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.personnelAddEmailViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: PersonnelAddEmailViewController.nameOfClass) as PersonnelAddEmailViewController
        
        self.personnelAddEmailViewController?.personnelAddRequest = self.personnelAddRequest
        personnelAddEmailViewController?.isEditingPersonnel = isEditingPersonnel
        self.navigationController?.pushViewController(self.personnelAddEmailViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool {
        if businessQualificationInfoList.isEmpty {
            let message = LocalizationKeys.error_add_contacts.getLocalized()
            showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if(UserDefault.getPID() != nil && (UserDefault.getPID()?.length)! > 0  && self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = UserDefault.getPID() ?? ""
        } else if(self.personnelAddRequest?.personal_id == nil){
            self.personnelAddRequest?.personal_id = ""
        }
        
        self.personnelAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        self.personnelAddRequest?.personal_contact = self.businessQualificationInfoList
        return true
    }
}

extension PersonnelAddPhoneViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let mpDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyPersonnelDetailViewController }
            if let mpDetailVC = mpDetailVC as? MyPersonnelDetailViewController {
                mpDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(mpDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }

    @IBAction func methodOptionTypeAction(_ sender: UIButton){
        self.btnOptionLandline.isSelected = false
        self.btnOptionCell.isSelected = false
        self.btnOptionBusiness.isSelected = false
        self.activeType?.title = sender.titleLabel?.text
        
        if(sender == self.btnOptionLandline){
            self.btnOptionLandline.isSelected = true
            self.activeType?.id = "1"
        } else if(sender == self.btnOptionCell){
            self.btnOptionCell.isSelected = true
            self.activeType?.id = "2"
        } else if(sender == self.btnOptionBusiness){
            self.btnOptionBusiness.isSelected = true
            self.activeType?.id = "3"
        }
    }
}


extension PersonnelAddPhoneViewController{
    
    @IBAction func methodAddAwardAction(_ sender: UIButton){
        let businessQualificationInfo = BusinessQualificationInfo()
        if((self.businessInformationQualification.inputName.text()?.length)! > 0){
            businessQualificationInfo.name = businessInformationQualification.inputName.text() ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_phone_number.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((self.businessInformationQualification.textViewDesc.text?.length)! > 0){
            businessQualificationInfo.desc = businessInformationQualification.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_phone_desc.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if self.businessQualificationInfoList.count < HelperConstant.minimumBlocks {
            businessQualificationInfo.type = self.activeType
            self.isUpdate = true
            self.businessInformationQualification.inputName.input.text = ""
            self.businessInformationQualification.textViewDesc.text = ""
            self.businessQualificationInfoList.append(businessQualificationInfo)
            self.tableView.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension PersonnelAddPhoneViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return businessQualificationInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
        
        let objInfo = businessQualificationInfoList[indexPath.row]
        cell.lblIndex.text = "Number \(indexPath.row + 1)"
        cell.cellIndex = indexPath
        cell.delegate = self

        cell.lblLink.text = objInfo.type?.title
        cell.lblName.text = objInfo.name
        cell.lblDesc.text = objInfo.desc
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension PersonnelAddPhoneViewController:BusinessInformationQualificationAwardCellDelegate{
    func removeWebsiteInfo(cellIndex:IndexPath?){
        if self.businessQualificationInfoList.count > 0 {
            self.businessQualificationInfoList.remove(at: cellIndex?.row ?? 0)
            self.tableView.reloadData()
            
            if(businessQualificationInfoList.count == 0){
                self.viewTableView.isHidden = true
            }
        }
    }
}

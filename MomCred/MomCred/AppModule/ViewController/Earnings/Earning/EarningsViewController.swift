//
//  EarningsViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * EarningsViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class EarningsViewController : LMBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblWithdrawals: UILabel!

    var totalDAmount = 0.0
    fileprivate var menuArray: [HSMenu] = []
    
    var currentPageIndex = 0
    
    //MARK:- Var(s)
    fileprivate var presenter = EarningsPresenter()
    fileprivate var isDisplayLoader = true
    
    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var earningDetailView:EarningDetailView?
    fileprivate var logoutViewController:PMLogoutView?
    fileprivate var withdrawAmountStatusResponse:WithdrawAmountStatusResponse?
    
    var purchaseList:[PurchaseListResponse.PurchaseDataResponse]?
    
    var purchaseListRequest:PurchaseListRequest?
    private var purchaseFilter: String? {
        didSet {
            serverRequest()
        }
    }
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnReset.isHidden = true
        // Add refresh loader for pulling and refresh
        refreshView.delegate = self
        tableView.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        
        presenter.connectView(view: self)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(EarningsViewController.withdrawAmount))
        lblTotalAmount.isUserInteractionEnabled = true
        lblTotalAmount.addGestureRecognizer(tap)
        
        let tapW = UITapGestureRecognizer(target: self, action: #selector(EarningsViewController.withdrawlist))
        lblWithdrawals.isUserInteractionEnabled = true
        lblWithdrawals.addGestureRecognizer(tapW)

        
    }
    
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.earningDetailView = nil
        self.logoutViewController = nil
        
        if(purchaseList == nil || (purchaseList != nil && (purchaseList?.count)! == 0)){
            Spinner.show()
            self.serverRequest()
        }
    }
    
  
    @objc
       func withdrawAmount(sender:UITapGestureRecognizer) {
           print("tap working")
         if totalDAmount > 0 {
            serverWithdrawAmount()
         }
       }
    @objc
       func withdrawlist(sender:UITapGestureRecognizer) {
         
       let withdrawalsListView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.EarningsStoryboard, viewControllerName: WithdrawalsListView.nameOfClass) as WithdrawalsListView
        
        self.navigationController?.pushViewController(withdrawalsListView, animated: true)

       }
}

extension EarningsViewController{
    
    fileprivate func getPurchaseListRequest() -> PurchaseListRequest{
        
        if(self.btnReset.isHidden){
            let purchaseListRequest = PurchaseListRequest()
            purchaseListRequest.page_id = "\(currentPageIndex)"
            
            let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
            
            switch  role_id {
            case AppUser.ServiceProvider.rawValue:
                purchaseListRequest.type = "2"
                break
            case AppUser.LocalBusiness.rawValue:
                purchaseListRequest.type = "1"
                break
            case AppUser.Enthusiast.rawValue:
                purchaseListRequest.type = "1"
            default:
                break
            }
            purchaseListRequest.filter = purchaseFilter
            
            self.purchaseListRequest = purchaseListRequest
        } else if(self.purchaseListRequest != nil){
            self.purchaseListRequest?.page_id = "\(currentPageIndex)"
        }
        
        return self.purchaseListRequest ?? PurchaseListRequest()
    }
    
    fileprivate func serverRequest(){
        let purchaseListRequest = self.getPurchaseListRequest()
        self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest,callback: {
            (status, response, message) in
        })
    }
    
    func serverWithdrawStatus(){
        Spinner.show()
        self.presenter.serverWithdrawStatus() { [weak self] (status, response, message) in
            if(status && response != nil){
                
                self?.withdrawAmountStatusResponse = response
                if let totalValue = response?.data?.totalamount?.doubleValue {
                    self?.totalDAmount = totalValue
                    self?.lblTotalAmount.text = "Withdraw \(totalValue.formattedPrice)"
                }
            }
        }
    }
    
    fileprivate func serverWithdrawAmount(){
        
        if(self.withdrawAmountStatusResponse != nil){
            let withdrawAmountRequest = WithdrawAmountRequest(getids: withdrawAmountStatusResponse?.data?.ids ?? "")
            
            Spinner.show()
            self.presenter.serverWithdraw(withdrawAmountRequest: withdrawAmountRequest,callback: {
                (status, response, message) in
                
                self.serverWithdrawStatus()
            })
        }
    }
    
    fileprivate func callbackListing(_ dateList:[String]?, _ tagIndexList:[String]?) -> Void?{
        
        var startDate = ""
        var endDate = ""
        if(dateList != nil && (dateList?.count)! > 0 ){
            
            startDate = dateList?.first ?? ""
            if((dateList?.count)! > 1){
                endDate = dateList?.last ?? ""
            }
        }
        
        var tagListText = ""
        if(tagIndexList != nil && (tagIndexList?.count)! > 0){
            tagListText = tagIndexList?.joined(separator: ",") ?? ""
        }
        
        self.currentPageIndex = 0
        let purchaseListRequest = self.getPurchaseListRequest()
        purchaseListRequest.from = startDate
        purchaseListRequest.to = endDate
        purchaseListRequest.filter = tagListText
        self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
            (status, response, message) in
            
            if(response != nil){
                self.btnFilter.isHidden = true
                self.btnReset.isHidden = false
            }
            
        })
        
        return nil
    }
}


extension EarningsViewController{
    @IBAction private func btnSearchAction(sender: UIButton) {
        let searchVC: SearchEarningsViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.EarningsStoryboard, viewControllerName: SearchEarningsViewController.nameOfClass)
        navigationController?.pushViewController(searchVC, animated: true)
    }

    @IBAction func resetFilter(_ sender: UIButton){
        self.btnFilter.isHidden = false
        self.btnReset.isHidden = true
        self.currentPageIndex = 0
        self.serverRequest()
    }
    
    @IBAction func actionFilter(_ sender: UIButton) {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "Month", style: .default, handler: { [weak self] _ in
            self?.purchaseFilter = "Month"
        }))
        controller.addAction(UIAlertAction(title: "Overall", style: .default, handler: { [weak self] _ in
            self?.purchaseFilter = nil
        }))
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        navigationController?.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.isAllowMultiLine = true
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}

extension EarningsViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        /*
        if(index == 0){
            self.logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
            self.logoutViewController?.delegate = self
            self.logoutViewController?.scrTitle = ""
            self.logoutViewController?.scrDesc = LocalizationKeys.withdraw_alert_desc.getLocalized()
             self.logoutViewController?.modalPresentationStyle = .overFullScreen
            self.present(logoutViewController!, animated: true, completion: nil)
        }
         */
    }
}

extension EarningsViewController: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
        
        // call Withdraw api
        self.serverWithdrawAmount()
    }
}

extension EarningsViewController: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .pulling(offset, threshould):
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                let purchaseListRequest = self.getPurchaseListRequest()
                self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            let purchaseListRequest = self.getPurchaseListRequest()
            self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension EarningsViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return purchaseList != nil ? (purchaseList?.count)! : 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EarningCell") as! EarningCell
        
        let purchase = purchaseList?[indexPath.row]
        cell.lblTitle.text = purchase?.customername ?? ""
        cell.lblSubTitle.text = ""
        cell.cellIndex = indexPath.row
        
        cell.lblTitle.text = "\(purchase?.customername ?? "")"
        cell.lblSubTitle.text = "\(purchase?.service_name ?? "")"
        
        cell.lblTranscationNum.text = purchase?.transactionid ?? ""
        cell.lblTranscationDate.text = purchase?.date ?? ""
        
        cell.lblVerification.text = purchase?.verification_number
        if let donation = purchase?.purchase_donation_amount, let donationVal = Double(donation), donationVal > 0 {
            cell.lblDonationAmount.text = donationVal.formattedPrice
            cell.lblDonationAmountContainer.isHidden = false
        } else {
            cell.lblDonationAmount.text = Double("\(purchase?.purchase_donation_amount ?? "0")")?.formattedPrice
            cell.lblDonationAmountContainer.isHidden = false
        }
        if let credit = purchase?.purchaseData?.creditloyalty?.doubleValue, credit > 0 {
            cell.lblCreditAmount.text = credit.formattedPrice
            cell.lblCreditAmountContainer.isHidden = false
        } else {
            cell.lblCreditAmountContainer.isHidden = true
        }
        if let serviceAmount = purchase?.purchaseData?.serviceamount {
            cell.lblServiceAmount.text = serviceAmount.doubleValue.formattedPrice
        } else {
            cell.lblServiceAmount.text = 0.0.formattedPrice
        }

        let totalAmount =  (purchase?.purchaseData?.serviceamount?.floatValue ?? 0.00) - (purchase?.purchaseData?.creditloyalty?.floatValue ?? 0.00)
        cell.stackViewRedeem.isHidden = true

//        if purchase?.voucher_status?.caseInsensitiveCompare("0") == ComparisonResult.orderedSame {
//            cell.stackViewRedeem.isHidden = false
//        } else {
//            cell.stackViewRedeem.isHidden = true
//        }
        
        cell.lblAmount.text = totalAmount.formattedPrice
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.earningDetailView == nil){
            
            self.earningDetailView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.EarningsStoryboard, viewControllerName: EarningDetailView.nameOfClass) as EarningDetailView
            
            let purchase = purchaseList?[indexPath.row]
            var serviceName = ""
            var serviceType = ""
            
            
            serviceName = "\(purchase?.customername ?? "")"
            serviceType = "\(purchase?.service_name ?? "")"
            
            self.earningDetailView?.serviceName = serviceName
            self.earningDetailView?.serviceType = serviceType
            self.earningDetailView?.purchaseId = purchase?.purchase_id ?? ""
            
            self.navigationController?.pushViewController(self.earningDetailView!, animated: true)
        }
    }
}

class EarningCell: UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var stackViewRedeem : UIStackView!
    @IBOutlet weak var btnRedeem : UIButton!
    @IBOutlet weak var lblTranscationNum : UILabel!
    @IBOutlet weak var lblTranscationDate : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblVerification : UILabel!

    @IBOutlet weak var lblDonationAmountContainer: UIView! { didSet { lblDonationAmountContainer.isHidden = true } }
    @IBOutlet weak var lblDonationAmount : UILabel!
    @IBOutlet weak var lblCreditAmountContainer: UIView! { didSet { lblCreditAmountContainer.isHidden = true } }
    @IBOutlet weak var lblCreditAmount : UILabel!
    @IBOutlet weak var lblServiceAmount : UILabel!

    var cellIndex:Int!
    var delegate:ProductCellDelegate?
    
    /**
     *  Show Redeem event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodShowRedeemOption(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodShowProductDetails(cellIndex: self.cellIndex)
        }
    }
}

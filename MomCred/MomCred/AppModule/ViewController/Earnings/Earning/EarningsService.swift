//
//  EarningsService.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  EarningsService is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class EarningsService{

    /**
     *  Method connect to get Purchase list data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverPurchaseDetail(purchaseDetailRequest:PurchaseDetailRequest?, callback:@escaping (_ status:Bool, _ response: PurchaseDetailResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = purchaseDetailRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_PAYMENT_HISTORY_DETAIL, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:PurchaseDetailResponse? = PurchaseDetailResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get Purchase list data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverPurchaseListing(purchaseListRequest:PurchaseListRequest?, callback:@escaping (_ status:Bool, _ response: PurchaseListResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = purchaseListRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_PAYMENT_HISTORY_PROVIDER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:PurchaseListResponse? = PurchaseListResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method Withdraw amount status for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverWithdrawStatus( callback:@escaping (_ status:Bool, _ response: WithdrawAmountStatusResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // GET
        // Convert Model request object into JSONString
        
        reqPost = APIManager().sendGetRequest(urlString: APIKeys.API_WITHDRAW_STATUS, header: APIHeaders().getDefaultHeaders())
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:WithdrawAmountStatusResponse? = WithdrawAmountStatusResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method Withdraw amount for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverWithdrawAmount(withdrawAmountRequest:WithdrawAmountRequest?, callback:@escaping (_ status:Bool, _ response: WithdrawAmountResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = withdrawAmountRequest?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_WITHDRAW_AMOUNT, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:WithdrawAmountResponse? = WithdrawAmountResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method Withdrawl list for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func serverWithdrawlList( callback:@escaping (_ status:Bool, _ response: WithdrawalResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // GET
        // Convert Model request object into JSONString
        
        reqPost = APIManager().sendGetRequest(urlString: APIKeys.API_WITHDRAWL_LIST, header: APIHeaders().getDefaultHeaders())
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:WithdrawalResponse? = WithdrawalResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    
}

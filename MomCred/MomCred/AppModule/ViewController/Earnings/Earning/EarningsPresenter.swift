//
//  EarningsPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  EarningsPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class EarningsPresenter {
    
    var view: EarningsViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: EarningsViewController) {
        self.view = view
    }
}

extension EarningsPresenter{
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverPurchaseListing(purchaseListRequest:PurchaseListRequest?, callback:@escaping (_ status:Bool, _ response: PurchaseListResponse?, _ message: String?) -> Void){
        
        EarningsService.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    if(response != nil && response?.data != nil && (response?.data?.count)! > 0){
                        if(self.view.currentPageIndex == 0){
                            self.view.purchaseList = []
                            self.view.purchaseList = response?.data
                        } else {
                            self.view.purchaseList?.append(contentsOf: response?.data ?? [])
                        }
                    }
                    
                    self.view.tableView.reloadData()
                    
                    self.view.serverWithdrawStatus()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    if(self.view.currentPageIndex == 0){
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    }
                    
                    self.view.serverWithdrawStatus()
                    callback(status, nil, message)
                }
            }
        })
    }
    
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverPurchaseListingFilter(purchaseListFilterRequest:PurchaseListFilterRequest?, callback:@escaping (_ status:Bool, _ response: PurchaseListFilterResponse?, _ message: String?) -> Void){
        
        PurchaseListService.serverPurchaseListingFilter(purchaseListFilterRequest: purchaseListFilterRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, nil, message)
                }
            }
        })
    }
    
    /**
     *  Get Earning Amount Status from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverWithdrawStatus(callback:@escaping (_ status:Bool, _ response: WithdrawAmountStatusResponse?, _ message: String?) -> Void){
        
        EarningsService.serverWithdrawStatus(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, nil, message)
                }
            }
        })
    }
    
    /**
     *  Get Earning Amount from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverWithdraw(withdrawAmountRequest:WithdrawAmountRequest?, callback:@escaping (_ status:Bool, _ response: WithdrawAmountResponse?, _ message: String?) -> Void){
        
        EarningsService.serverWithdrawAmount(withdrawAmountRequest:withdrawAmountRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, nil, message)
                }
            }
        })
    }
}


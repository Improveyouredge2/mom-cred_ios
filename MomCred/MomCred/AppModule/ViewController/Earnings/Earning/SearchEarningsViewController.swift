//
//  SearchEarningsViewController.swift
//  MomCred
//
//  Created by MD on 28/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation

class SearchEarningsViewController : LMBaseViewController {
    
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.barTintColor = UIColor.clear
            searchBar.backgroundColor = UIColor.clear
            searchBar.isTranslucent = true
            searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
            searchBar.delegate = self
            
            if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField {
                searchTextField.textColor = UIColor.white
                searchTextField.attributedPlaceholder =  NSAttributedString(string: "Verification Number", attributes: [.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
            }
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var menuArray: [HSMenu] = []
    
    var currentPageIndex = 0
    
    //MARK:- Var(s)
    fileprivate var presenter = SearchEarningsPresenter()
    fileprivate var isDisplayLoader = true
    
    //MARK:- fileprivate
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var earningDetailView:EarningDetailView?
    fileprivate var logoutViewController:PMLogoutView?
    fileprivate var withdrawAmountStatusResponse:WithdrawAmountStatusResponse?
    
    var purchaseList:[PurchaseListResponse.PurchaseDataResponse]?
    
    var purchaseListRequest:SearchPurchaseListRequest?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        presenter.connectView(view: self)
    }
    
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.earningDetailView = nil
        self.logoutViewController = nil
    }
}

extension SearchEarningsViewController{
    
    fileprivate func getPurchaseListRequest() -> SearchPurchaseListRequest{
        
        if purchaseListRequest == nil {
            let purchaseListRequest = SearchPurchaseListRequest()
            purchaseListRequest.page_id = "\(currentPageIndex)"
            
            let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
            
            switch  role_id {
            case AppUser.ServiceProvider.rawValue:
                purchaseListRequest.type = "2"
                break
            case AppUser.LocalBusiness.rawValue:
                purchaseListRequest.type = "1"
                break
            case AppUser.Enthusiast.rawValue:
                purchaseListRequest.type = "1"
            default:
                break
            }
            purchaseListRequest.search = searchBar.text
            
            self.purchaseListRequest = purchaseListRequest
        } else if(self.purchaseListRequest != nil){
            purchaseListRequest?.page_id = "\(currentPageIndex)"
        }
        return self.purchaseListRequest ?? SearchPurchaseListRequest()
    }
    
    fileprivate func serverRequest(){
        let purchaseListRequest = self.getPurchaseListRequest()
        self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest,callback: {
            (status, response, message) in
        })
    }
        
    fileprivate func callbackListing(_ dateList:[String]?, _ tagIndexList:[String]?) -> Void?{
        
        var startDate = ""
        var endDate = ""
        if(dateList != nil && (dateList?.count)! > 0 ){
            
            startDate = dateList?.first ?? ""
            if((dateList?.count)! > 1){
                endDate = dateList?.last ?? ""
            }
        }
        
        var tagListText = ""
        if(tagIndexList != nil && (tagIndexList?.count)! > 0){
            tagListText = tagIndexList?.joined(separator: ",") ?? ""
        }
        
        self.currentPageIndex = 0
        let purchaseListRequest = self.getPurchaseListRequest()
        purchaseListRequest.from = startDate
        purchaseListRequest.to = endDate
        purchaseListRequest.filter = tagListText
        self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
            (status, response, message) in
        })
        return nil
    }
}


extension SearchEarningsViewController{
    @IBAction func resetFilter(_ sender: UIButton){
        self.currentPageIndex = 0
        self.serverRequest()
    }
    
    @IBAction func actionFilter(_ sender: UIButton) {
    }
    
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.isAllowMultiLine = true
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}

extension SearchEarningsViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if(index == 0){
            
            //call Withdraw amount Confirmation
            self.logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
            self.logoutViewController?.delegate = self
            self.logoutViewController?.scrTitle = ""
            self.logoutViewController?.scrDesc = LocalizationKeys.withdraw_alert_desc.getLocalized()
             self.logoutViewController?.modalPresentationStyle = .overFullScreen
            self.present(logoutViewController!, animated: true, completion: nil)
        }
    }
}

extension SearchEarningsViewController: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
    }
}

extension SearchEarningsViewController: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .pulling(offset, threshould):
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                let purchaseListRequest = self.getPurchaseListRequest()
                self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            let purchaseListRequest = self.getPurchaseListRequest()
            self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension SearchEarningsViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return purchaseList != nil ? (purchaseList?.count)! : 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EarningCell") as! EarningCell
        
        let purchase = purchaseList?[indexPath.row]
        cell.lblTitle.text = purchase?.customername ?? ""
        cell.lblSubTitle.text = ""
        cell.cellIndex = indexPath.row
        
        cell.lblTitle.text = "\(purchase?.customername ?? "")"
        cell.lblSubTitle.text = "\(purchase?.service_name ?? "")"
        
        cell.lblTranscationNum.text = purchase?.transactionid ?? ""
        cell.lblTranscationDate.text = purchase?.date ?? ""
        
        cell.lblVerification.text = purchase?.verification_number
        if let donation = purchase?.purchase_donation_amount, let donationVal = Double(donation), donationVal > 0 {
            cell.lblDonationAmount.text = donationVal.formattedPrice
            cell.lblDonationAmountContainer.isHidden = false
        } else {
            cell.lblDonationAmountContainer.isHidden = true
        }
        if let credit = purchase?.purchaseData?.creditloyalty?.doubleValue, credit > 0 {
            cell.lblCreditAmount.text = credit.formattedPrice
            cell.lblCreditAmountContainer.isHidden = false
        } else {
            cell.lblCreditAmountContainer.isHidden = true
        }
        if let serviceAmount = purchase?.purchaseData?.serviceamount {
            cell.lblServiceAmount.text = serviceAmount.doubleValue.formattedPrice
        } else {
            cell.lblServiceAmount.text = 0.0.formattedPrice
        }

        let totalAmount =  (purchase?.purchaseData?.serviceamount?.floatValue ?? 0.00) - (purchase?.purchaseData?.creditloyalty?.floatValue ?? 0.00)
        
        if(purchase?.voucher_status != nil && (purchase?.voucher_status?.length)! > 0 && purchase?.voucher_status?.caseInsensitiveCompare("0") == ComparisonResult.orderedSame){
            cell.stackViewRedeem.isHidden = false
        } else {
            cell.stackViewRedeem.isHidden = true
        }
        
        cell.lblAmount.text = totalAmount.formattedPrice
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.earningDetailView == nil){
            
            self.earningDetailView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.EarningsStoryboard, viewControllerName: EarningDetailView.nameOfClass) as EarningDetailView
            
            let purchase = purchaseList?[indexPath.row]
            var serviceName = ""
            var serviceType = ""
            
            
            serviceName = "\(purchase?.customername ?? "")"
            serviceType = "\(purchase?.service_name ?? "")"
            
            self.earningDetailView?.serviceName = serviceName
            self.earningDetailView?.serviceType = serviceType
            self.earningDetailView?.purchaseId = purchase?.purchase_id ?? ""
            
            self.navigationController?.pushViewController(self.earningDetailView!, animated: true)
        }
    }
}

extension SearchEarningsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload(_:)), object: searchBar)
        perform(#selector(self.reload(_:)), with: searchBar, afterDelay: 0.75)
    }

    @objc func reload(_ searchBar: UISearchBar) {
        purchaseListRequest = nil
        purchaseList = nil
        currentPageIndex = 0

        guard let query = searchBar.text, query.trimmingCharacters(in: .whitespaces) != "" else {
            print("nothing to search")
            tableView.reloadData()
            return
        }

        print(query)
        let request = getPurchaseListRequest()
        presenter.serverPurchaseListing(purchaseListRequest: request) { (status, response, message) in
        }
    }
}

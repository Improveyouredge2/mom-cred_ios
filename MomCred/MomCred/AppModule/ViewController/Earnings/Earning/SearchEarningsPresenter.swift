//
//  SearchEarningsPresenter.swift
//  MomCred
//
//  Created by MD on 28/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation

class SearchEarningsPresenter {
    
    weak var view: SearchEarningsViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: SearchEarningsViewController) {
        self.view = view
    }
}

extension SearchEarningsPresenter{
    func serverPurchaseListing(purchaseListRequest: SearchPurchaseListRequest?, callback:@escaping (_ status:Bool, _ response: PurchaseListResponse?, _ message: String?) -> Void){
        
        EarningsService.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    if(response != nil && response?.data != nil && (response?.data?.count)! > 0){
                        if(self.view.currentPageIndex == 0){
                            self.view.purchaseList = []
                            self.view.purchaseList = response?.data
                        } else {
                            self.view.purchaseList?.append(contentsOf: response?.data ?? [])
                        }
                    }
                    
                    self.view.tableView.reloadData()
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    if(self.view.currentPageIndex == 0){
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    }
                    callback(status, nil, message)
                }
            }
        })
    }
}

//
//  WutgdrawalResponse.swift
//  MomCred
//
//  Created by Shiv on 14/09/21.
//  Copyright © 2021 Consagous. All rights reserved.
//


import Foundation
import ObjectMapper

// MARK: - WithdrawalResponse
/*struct WithdrawalResponse: Codable {
    let status, errorCode, errorLine: Int
    let data: [WithdrawalData]
    let message: String

    enum CodingKeys: String, CodingKey {
        case status
        case errorCode = "error_code"
        case errorLine = "error_line"
        case data, message
    }
    
}*/
    

// MARK: - Datum
/*struct WithdrawalData: Codable {
    let payID, payTransID, payUser, payRequest: String?
    let payResponse, payAmount, payTraxCharge, payDiscount: String?
    let payData, payStatus, payDate, payResponsePayout,withdraw_status: String?
    let payoutID, isWithdraw: String?

    enum CodingKeys: String, CodingKey {
        case payID = "pay_id"
        case payTransID = "pay_trans_id"
        case payUser = "pay_user"
        case payRequest = "pay_request"
        case payResponse = "pay_response"
        case payAmount = "pay_amount"
        case payTraxCharge = "pay_trax_charge"
        case payDiscount = "pay_discount"
        case payData = "pay_data"
        case payStatus = "pay_status"
        case payDate = "pay_date"
        case payResponsePayout = "pay_response_payout"
        case payoutID = "payout_id"
        case isWithdraw = "is_withdraw"
        case withdraw_status = "withdraw_status"

    }
}*/

class WithdrawalData: Mappable {
    var payID, payTransID, payUser, payRequest: String?
    var payResponse, payAmount, payTraxCharge, payDiscount: String?
    var payData, payStatus, payDate, payResponsePayout,withdraw_status: String?
    var payoutID, isWithdraw: String?
    
   
    required init() {
        //        super.init()
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        //        super.init(map: map)
    }
    
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->WithdrawalData?{
        var myFacilityAddRequest:WithdrawalData?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<WithdrawalData>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    myFacilityAddRequest = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<WithdrawalData>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        myFacilityAddRequest = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return myFacilityAddRequest ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        payID                       <- map["pay_id"]
        payTransID                  <- map["pay_trans_id"]
        payUser                     <- map["pay_user"]
        payRequest                  <- map["pay_request"]
        payResponse                 <- map["pay_response"]
        payAmount                   <- map["pay_amount"]
        payTraxCharge               <- map["pay_trax_charge"]
        payDiscount                 <- map["pay_discount"]
        payData                     <- map["pay_data"]
        payStatus                   <- map["pay_status"]
        payDate                     <- map["pay_date"]
        payResponsePayout           <- map["pay_response_payout"]
        payoutID                    <- map["payout_id"]
        isWithdraw                  <- map["is_withdraw"]
        withdraw_status             <- map["withdraw_status"]
   
       
    }
}

class WithdrawalResponse : APIResponse {
    
    var data:[WithdrawalData]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->MyFacilityResponse?{
        var myFacilityResponse:MyFacilityResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<MyFacilityResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    myFacilityResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<MyFacilityResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        myFacilityResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return myFacilityResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
}

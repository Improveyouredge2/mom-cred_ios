//
//  WithdrawalListCell.swift
//  MomCred
//
//  Created by Shiv on 14/09/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class WithdrawalListCell: UITableViewCell {
    
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lbltransaction : UILabel!
    @IBOutlet weak var lblDate : UILabel!

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

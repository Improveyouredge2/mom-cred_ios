//
//  WithdrawalsPresenter.swift
//  MomCred
//
//  Created by Shiv on 14/09/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation
class WithdrawalsPresenter {
    
    var view:WithdrawalsListView! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: WithdrawalsListView) {
        self.view = view
    }
}
extension WithdrawalsPresenter{
    //Users/withdrawList
    func withdrawList(){
        
        EarningsService.serverWithdrawlList(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    if(response != nil && response?.data != nil){
                        self.view.updateData(response: response?.data)

                    }
                    
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

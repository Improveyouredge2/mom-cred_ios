//
//  WithdrawalsListView.swift
//  MomCred
//
//  Created by Shiv on 14/09/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class WithdrawalsListView: LMBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    //var withdrawalResponse:WithdrawalResponse?
    var data:[WithdrawalData]?
    let presenter  = WithdrawalsPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)
        presenter.withdrawList()

        // Do any additional setup after loading the view.
    }
    
    func updateData(response:[WithdrawalData]?) -> Void {
        if let re = response{
            data = re
            tableView.reloadData()

        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WithdrawalsListView:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: WithdrawalListCell.nameOfClass) as! WithdrawalListCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        if let obj = data?[indexPath.row]{
            
            cell.lblAmount.text = "$ " + (obj.payAmount ?? "0.0")
            cell.lblStatus.text = obj.withdraw_status
            cell.lbltransaction.text = obj.payoutID
            cell.lblDate.text = " \(DateTimeUtils.sharedInstance.toLocalDate(obj.payDate ?? "", with: "yyyy-MM-dd HH:mm:ss")?.appSpecificStringFromFormat() ?? "")"

        }

        return cell
    }
    
}

//
//  PurchaseDetailView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class EarningDetailView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableViewVoucher: UITableView!
    @IBOutlet weak var tableViewPackage: UITableView!
    
    @IBOutlet weak var lblPackageTitle : UILabel!
    @IBOutlet weak var lblPackageDetail : UILabel!
    @IBOutlet weak var viewPurchaseDetail : UIView!
    
    @IBOutlet weak var constraintPackageListHeightConstant:NSLayoutConstraint!
    @IBOutlet weak var constraintVoucherListHeightConstant:NSLayoutConstraint!
    
    fileprivate var purchaseRefundView:PurchaseRefundView?
    
    //MARK:- Var(s)
    var presenter = EarningDetailPresenter()
    var purchaseDetailResponse: PurchaseDetailResponse.PurchaseDetailDataResponse?
    var purchaseId = ""
    
    var serviceName = ""
    var serviceType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)
        
        viewPurchaseDetail.isHidden = true
        
        let purchaseDetailView = PurchaseDetailRequest()
        purchaseDetailView.purchase_id = purchaseId
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            purchaseDetailView.type = "2"
            break
        case AppUser.LocalBusiness.rawValue:
            purchaseDetailView.type = "1"
            break
        case AppUser.Enthusiast.rawValue:
            purchaseDetailView.type = "1"
        default:
            break
        }
        
        Spinner.show()
        presenter.serverPurchaseDetail(purchaseDetailRequest: purchaseDetailView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        purchaseRefundView = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(tableViewVoucher.contentSize.height > 0){
            tableViewVoucher.scrollToBottom()
            constraintVoucherListHeightConstant?.constant =
                tableViewVoucher.contentSize.height
            
            updateTableViewHeight(tableView:tableViewVoucher)
        }
        
        if(tableViewPackage.contentSize.height > 0){
            constraintPackageListHeightConstant?.constant = tableViewPackage.contentSize.height
            
            updateTableViewHeight(tableView:tableViewPackage)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView:UITableView) {
    
        // Location
        if(tableView == tableViewVoucher){
            UIView.animate(withDuration: 0, animations: { [weak self] in
                self?.tableViewVoucher.layoutIfNeeded()
            }, completion: {  [weak self] (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                if let cells = self?.tableViewVoucher.visibleCells {
                    for cell in cells {
                        heightOfTableView += cell.frame.height
                    }
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self?.constraintVoucherListHeightConstant.constant = heightOfTableView
            })
        }
        
        if(tableView == tableViewPackage){
            UIView.animate(withDuration: 0, animations: { [weak self] in
                self?.tableViewPackage.layoutIfNeeded()
            }, completion: { [weak self] (complete) in
                var heightOfTableView: CGFloat = 0.0
                if let cells = self?.tableViewPackage.visibleCells {
                    for cell in cells {
                        heightOfTableView += cell.frame.height
                    }
                }
                self?.constraintPackageListHeightConstant.constant = heightOfTableView
            })
        }
    }
}

extension EarningDetailView {
    func updateScrInfo() {
        lblPackageTitle.text = serviceName
        lblPackageDetail.text = serviceType
        tableViewVoucher.reloadData()
        tableViewPackage.reloadData()
    }
}

extension EarningDetailView {
    /**
     *  Buy Now action button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func actionInitiateRefund(_ sender: UIButton) {
        if(purchaseRefundView == nil){
            purchaseRefundView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PurchaseStoryboard, viewControllerName: PurchaseRefundView.nameOfClass) as PurchaseRefundView
            navigationController?.pushViewController(purchaseRefundView!, animated: true)
        }
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension EarningDetailView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == tableViewPackage){
            return purchaseDetailResponse?.service?.count ?? 0
        } else if(tableView == tableViewVoucher){
            return purchaseDetailResponse?.voucher?.count ?? 0
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if(tableView == tableViewPackage){
            updateViewConstraints()
            let cell = tableView.dequeueReusableCell(withIdentifier: PurchaseDetailListCell.nameOfClass, for: indexPath) as! PurchaseDetailListCell
            
            let obj = purchaseDetailResponse?.service?[indexPath.row]

            if let image = obj?.service_img, let url = URL(string: image) {
                cell.imgProfilePic.sd_setImage(with: url)
            } else {
                cell.imgProfilePic.image = nil
            }
            
            cell.lblTitle.text = obj?.service_name ?? ""
            cell.lblBusiName.text = "By: \(obj?.serviceusername ?? "")"
            
            cell.lblDesciption.text = ""
            if let price = obj?.price, let priceVal = Double(price) {
                cell.lblPrice.text = priceVal.formattedPrice
            } else {
                cell.lblPrice.text = 0.0.formattedPrice
            }
            
            if let price = obj?.creditamount, let priceVal = Double(price) {
                cell.lblCreditTitle.isHidden = false
                cell.lblCredit.isHidden = false
                cell.lblCredit.text = priceVal.formattedPrice
            } else {
                cell.lblCreditTitle.isHidden = true
                cell.lblCredit.isHidden = true
            }

            cell.lblCharitable.text = "Donation Amount"

            if let donationAmount = obj?.purchase_donation_amount, let donation = Double(donationAmount) {
                cell.lblCharitableAmt.text = donation.formattedPrice
                cell.stackViewCharitable.isHidden = false
                cell.lblCharitable.isHidden = false
            } else {
                
                cell.lblCharitableAmt.text = Double("\(obj?.purchase_donation_amount ?? "0")")?.formattedPrice

                cell.stackViewCharitable.isHidden = false
                cell.lblCharitable.isHidden = false
            }
            
            return cell
        } else if(tableView == tableViewVoucher){
            updateViewConstraints()
            let cell = tableView.dequeueReusableCell(withIdentifier: PurchaseVoucherListCell.nameOfClass, for: indexPath) as! PurchaseVoucherListCell
            
            let obj = purchaseDetailResponse?.voucher?[indexPath.row]
            
            cell.lblVerificationNum.text = obj?.voucher_id
            if let price = obj?.voucher_amount, let priceVal = Double(price) {
                cell.lblPrice.text = priceVal.formattedPrice
            } else {
                cell.lblPrice.text = 0.0.formattedPrice
            }

            let service = purchaseDetailResponse?.service?.first { $0.service_id == obj?.voucher_service }
            if let serviceName = service?.service_name {
                cell.lblServiceName.text = serviceName
            } else {
                cell.lblServiceName.text = ""
            }
            
            return cell
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

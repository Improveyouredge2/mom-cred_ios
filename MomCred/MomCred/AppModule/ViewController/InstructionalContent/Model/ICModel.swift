//
//  MyFacilityModel.swift
//  MomCred
//
//  Created by consagous on 10/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

///********************************/
////MARK:- Serialize Request
///*******************************/

/**
 *  PersonnelAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class ICAddRequest: Mappable {
    
    var content_id:String?
    var create_dt: String?
    
    // Page 1
    var content_name:String?
    var content_type:[String]?
    var content_type_title:[String]?
    var content_target_goal:[String]?
    var content_description:String?
    var content_location_info:PersonnelBusiLocationResponseData?
    var content_location:String?
    
    // Page 2
    var content_field:[BusinessFieldServiceCategoryInfo]?
    
    // Page 3
    var content_service_listing:[BusinessQualificationInfo]?
    
    // Page 4
    var content_facility:[BusinessQualificationInfo]?
    
    // Page 5
    var content_personal_listing:[BusinessQualificationInfo]?
    
    // Page 6
    var content_instruction_content:[BusinessQualificationInfo]?
    
    // Page 7
    var content_specific:ICSpecificInfo?
    
    // Page 8
    var content_other:[BusinessQualificationInfo]?
    
    // Page 9
    var content_notes:ServiceAddNotes?
    
    // Page 10
    var content_ext_link:ServiceExternalLink?
    
    
    // For business detail display
    var doc:[BusinessAdditionalDocumentInfo]?
    var mediabusiness:[BusinessAdditionalDocumentInfo]?
    var status:String? = "0"
    var number_of_purchase:String?
    var personal_user:String?
    var pageid:NSNumber?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init() {
        //        super.init()
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        //        super.init(map: map)
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ICAddRequest?{
        var addRequest:ICAddRequest?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ICAddRequest>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    addRequest = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ICAddRequest>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        addRequest = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return addRequest ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        content_id                      <- map["content_id"]
        create_dt                       <- map["create_dt"]
        
        //Page 1
        content_name                    <- map["content_name"]
        content_type                    <- map["content_type"]
        content_type_title              <- map["content_type_title"]
        content_target_goal             <- map["content_target_goal"]
        content_description             <- map["content_description"]
        content_location_info           <- map["content_location_info"]
        content_location                <- map["content_location"]
        
        //Page 2
        content_field                  <- map["content_field"]
        
        // Page 3
        content_service_listing        <- map["content_service_listing"]
        
        // Page 4
        content_facility               <- map["content_facility"]
        
        // Page 5
        content_personal_listing       <- map["content_personal_listing"]
        
        // Page 6
        content_instruction_content    <- map["content_instruction_content"]
        
        // Page 7
        content_specific               <- map["content_specific"]
        
        // Page 8
        content_other                  <- map["content_other"]
        
        // Page 9
        content_notes                  <- map["content_notes"]
        
        // Page 10
        content_ext_link               <- map["content_ext_link"]
        
        
        // Page  only in response, For business detail
        doc                         <- map["externalmedia"]
        
        // For business detail
        mediabusiness               <- map["content_media"]
        
        pageid                      <- map["pageid"]
        
        number_of_purchase          <- map["number_of_purchase"]
        
        // In-reponse
        status                      <- map["status"]
        
        personal_user               <- map["content_user"]
        
    }
}

class ICClassificationInfo: Mappable{
    var name:String?
    var descList:[String]?
    var desc:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ICClassificationInfo?{
        var classificationInfo:ICClassificationInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ICClassificationInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    classificationInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ICClassificationInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        classificationInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return classificationInfo ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        name        <- map["name"]
        descList    <- map["descList"]
        desc        <- map["desc"]
    }
}



class ICSpecificInfo: Mappable{
    
    var stdServices:String? = ""
    var exceptionalServices:[DetailData]? = []
    var exceptionalServicesInfo : [ExceptionalServicesInfo]?
    var service_target_goal:[String]?
    var service_target_goal_title:[String]?
    var special_listing_option:[String]?
    var special_listing_option_title:[String]?
    var minAge:String?
    var maxAge:String?
    var skillLevel:[DetailData]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ICSpecificInfo?{
        var ICSpecificInfo:ICSpecificInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ICSpecificInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    ICSpecificInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ICSpecificInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        ICSpecificInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return ICSpecificInfo ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        exceptionalServicesInfo     <- map["exceptionalServicesinfo"]
        stdServices                     <- map["stdServices"]
        exceptionalServices             <- map["exceptionalServices"]
        service_target_goal             <- map["content_service_target_goal"]
        service_target_goal_title       <- map["content_service_target_goal_title"]
        special_listing_option          <- map["special_listing_option"]
        special_listing_option_title    <- map["special_listing_option_title"]
        minAge                          <- map["minAge"]
        maxAge                          <- map["maxAge"]
        skillLevel                      <- map["skillLevel"]
    }
    
    class ExceptionalServicesInfo: Mappable{
        
        var id:String?
        var value: String?
        var data : [DetailData]?
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key user id, first name, last name, email, address and token.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init() {
            
        }
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map) {
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ExceptionalServicesInfo?{
            var ICSpecificInfo:ExceptionalServicesInfo?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<ExceptionalServicesInfo>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        ICSpecificInfo = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ExceptionalServicesInfo>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            ICSpecificInfo = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return ICSpecificInfo ?? nil
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map) {
            
            id                              <-                  map["id"]
            value                               <-                  map["value"]
            data                                <-                  map["data"]

        }
        
//        class ExceptionalServicesInfoData: Mappable{
//
//            var id:String?
//            var value: String?
//            var data : [DetailData]?
//            /**
//             *  Class constructor with custom parameter as per requirement.
//             *
//             *  @param key user id, first name, last name, email, address and token.
//             *
//             *  @return empty.
//             *
//             *  @Developed By: Team Consagous [CNSGSIN100]
//             */
//            init() {
//
//            }
//            /**
//             *  Method is required constructor of Mappable class
//             *
//             *  @param key mappable object.
//             *
//             *  @return empty.
//             *
//             *  @Developed By: Team Consagous [CNSGSIN100]
//             */
//            required init?(map: Map) {
//            }
//
//            /**
//             *  Convert Json response to class object.
//             *
//             *  @param key server api response is either key-value pair or string.
//             *
//             *  @return empty.
//             *
//             *  @Developed By: Team Consagous [CNSGSIN100]
//             */
//
//            func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ExceptionalServicesInfo?{
//                var ICSpecificInfo:ExceptionalServicesInfo?
//                if jsonResponse is NSDictionary{
//                    let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
//                    if tempDic != nil{
//                        let mapper = Mapper<ExceptionalServicesInfo>()
//                        if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
//                            ICSpecificInfo = testObject
//                        }
//                    }
//                }else if jsonResponse is String{
//                    let tempJsonString = jsonResponse as? String
//                    let data = tempJsonString?.data(using: String.Encoding.utf8)!
//
//                    do {
//                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
//                            let mapper = Mapper<ExceptionalServicesInfo>()
//                            if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
//                                ICSpecificInfo = testObject
//                            }
//                        }
//                    } catch let error as NSError {
//                        print(error.localizedDescription)
//                    }
//                }
//                return ICSpecificInfo ?? nil
//            }
//
//            /**
//             *  Sync server api response value to class objects.
//             *
//             *  @param key mappable object.
//             *
//             *  @return empty.
//             *
//             *  @Developed By: Team Consagous [CNSGSIN100]
//             */
//            func mapping(map: Map) {
//
//                id                              <-                  map["id"]
//                value                               <-                  map["value"]
//                data                                <-                  map["data"]
//
//            }
//        }
    }
}



class InstructionalContentDetailResponse: APIResponse {
    var data: ICAddRequest?
    
    required init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

///********************************/
////MARK:- Serialize Response
///*******************************/

/**
 *  ServiceResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class ICResponse : APIResponse {
    
    var data:[ICAddRequest]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ICResponse?{
        var ICResponse:ICResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ICResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    ICResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ICResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        ICResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return ICResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
}


class ICAddResponse : APIResponse {
    
    var data:ICAddResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ICAddResponse?{
        var myFacilityAddResponse:ICAddResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ICAddResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    myFacilityAddResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ICAddResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        myFacilityAddResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return myFacilityAddResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class ICAddResponseData: Mappable{
        var content_id : NSNumber?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ICAddResponseData?{
            var myFacilityAddResponseData:ICAddResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<ICAddResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        myFacilityAddResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ICAddResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            myFacilityAddResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return myFacilityAddResponseData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            content_id     <- map["content_id"]
            
        }
    }
}

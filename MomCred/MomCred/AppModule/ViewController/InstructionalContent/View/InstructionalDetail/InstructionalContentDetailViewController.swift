//
//  InstructionalContentDetailViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * InstructionalContentDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class InstructionalContentDetailViewController : LMBaseViewController {
    
//    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var lblTitleValue : UILabel!
    @IBOutlet weak var lblTypeValue : UILabel!
    @IBOutlet weak var lblGenDescValue : UILabel!
    @IBOutlet weak var lblTargetGoalValue : UILabel!
    @IBOutlet weak var lblLocationValue : UILabel!
    
    @IBOutlet weak var collectionView : UICollectionView!
    
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        self.presenter.getHelpAndSupport()
    }
    
}

extension InstructionalContentDetailViewController : UICollectionViewDataSource {
    
    /**
     *  Specify number of rows on section in collectionView with UICollectionViewDataSource method override.
     *
     *  @param key UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    /**
     *  Implement collectionview cell in collectionView with UICollectionViewDataSource method override.
     *
     *  @param UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let productInfo = productList?[indexPath.row]
        
            // Default product cell
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath) as! SerivceImageCell
        
//            if(productInfo?.image_url != nil){
//                //cell.btnForProductImage.sd_setImage(with: URL(string: "\(BASEURLs.baseURL)\(productInfo?.image_url ?? "")"), for: .normal, completed: nil)
//                cell.btnForProductImage.sd_setImage(with: URL(string: productInfo?.image_url ?? ""), for: .normal, completed: nil)
//            }
        
        let profileImageUrl = PMUserDefault.getProfilePic()
        if(profileImageUrl != nil){
            cell.imgService.sd_setImage(with: URL(string: (profileImageUrl ?? "")))
        }
        
            return cell
    }
}

extension InstructionalContentDetailViewController : UICollectionViewDelegate {
    
    /**
     *  Implement tap event on collectionview cell in collectionView with UICollectionViewDelegate method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        
    }
}

extension InstructionalContentDetailViewController : UICollectionViewDelegateFlowLayout {
    
    /**
     *  Implement collectionView with UICollectionViewDelegateFlowLayout method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:100, height: 80)
    }
}



//
//  ICAddSpecificViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import RangeSeekSlider

// Page 7

/**
 * ICAddSpecificViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ICAddSpecificViewController : LMBaseViewController{
    
    //    var expandTableNumber = [Int] ()
    @IBOutlet weak var tableViewExceptional: UITableView!
    @IBOutlet weak var constraintstableViewExceptionalHeight: NSLayoutConstraint!
    
//    @IBOutlet weak var ageRangeBar  : RangeSeekSlider!
//    @IBOutlet weak var lblAgeRange : UILabel!
    
    @IBOutlet weak var textFieldMiniAge: RYFloatingInput!
    @IBOutlet weak var textFieldMaxAge: RYFloatingInput!
    
    
    @IBOutlet weak var targetGoalCollectionView: DynamicHeightCollectionView!
    @IBOutlet weak var constraintsCollectionTargetHeight: NSLayoutConstraint!
    
    @IBOutlet weak var specialListingCollectionView: DynamicHeightCollectionView!
    @IBOutlet weak var constraintsCollectionSpecialListingHeight: NSLayoutConstraint!
//
    @IBOutlet weak var businessInformationQualification:BusinessInformationQualification!
    
    
    @IBOutlet weak var btnStandard: UIButton!
    @IBOutlet weak var btnExceptional: UIButton!
    @IBOutlet weak var btnStandardExceptional: UIButton!
    
    @IBOutlet weak var btnChkBeginner: UIButton!
    @IBOutlet weak var btnChkIntermediate: UIButton!
    @IBOutlet weak var btnChkExpert: UIButton!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingInstructionalContent: Bool = false

    var min_age            :   String?
    var max_age            :   String?

    
    fileprivate var businessQualificationInfoList:[BusinessQualificationInfo] = []
    
    fileprivate let formNumber = 7
    fileprivate var icAddOtherInstructionalViewController:ICAddOtherInstructionalViewController?
    
    var icAddRequest:ICAddRequest?
    
    fileprivate var presenter = ICAddSpecificPresenter()
    
    var isUpdate = false
    
    var exceptional_classification : [ListingDataDetail]?
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    let kHeaderSectionTag: Int = 6900;
    var expandTableNumber:[Int] = []
    
    fileprivate var targetGoalId:[String] = []
    fileprivate var targetGoal : [ListingDataDetail]?
    
    fileprivate var specialListingId:[String] = []
    fileprivate var specialListing : [ListingDataDetail]?
    
    fileprivate var skillLevel:[DetailData]?
    
    fileprivate var icSpecificInfo:ICSpecificInfo?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewExceptional.estimatedRowHeight = 200
        self.tableViewExceptional.rowHeight = UITableView.automaticDimension
        self.tableViewExceptional.backgroundColor = UIColor.clear
        
//        self.ageRangeBar.delegate  =   self
//        self.min_age    =   "18"
//        self.max_age    =   "30"
        
        presenter.connectView(view: self)
        
        self.exceptional_classification = ICAddOverviewPresenter.parentListingList?.parentListingData?.exceptional_classification
        
        self.reloadChildListing()
        
        self.updateScreenListing()
        self.updateSpecialListing()
        
//        self.specialListingCollectionView.backgroundColor = UIColor.red
        self.setScreenData()

        if !isEditingInstructionalContent {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.icAddOtherInstructionalViewController = nil
        
//        if let minAge = self.min_age, let maxAge = self.max_age {
//            self.ageRangeBar.selectedMaxValue   =   CGFloat(Int(maxAge) ?? 0)
//            self.ageRangeBar.selectedMinValue   =   CGFloat(Int(minAge) ?? 0)
//            self.lblAgeRange.text                           =       "\(Int(minAge) ?? 0)-\(Int(maxAge) ?? 0)"
//        } else {
//            self.ageRangeBar.selectedMaxValue   =   30.0
//            self.ageRangeBar.selectedMinValue   =   18.0
//            self.lblAgeRange.text                           =       "18-30"
//
//        }
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView: UITableView) {
        
        if(tableView == self.tableViewExceptional){
            self.tableViewExceptional.scrollToBottom()
            self.constraintstableViewExceptionalHeight?.constant = self.tableViewExceptional.contentSize.height
            
            self.updateTableViewHeight(tableView: tableView)
        }
    }
    
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableView == self.tableViewExceptional){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewExceptional.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewExceptional.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                
                heightOfTableView += 40 // 2 header of 20
                
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewExceptionalHeight.constant = heightOfTableView
            }
        }
    }
}


extension ICAddSpecificViewController{
    func updateScreenListing(){
        self.targetGoal = ICAddOverviewPresenter.parentListingList?.parentListingData?.targetgoal ?? []
        
        self.targetGoalCollectionView.reloadData()
    }
    
    func updateSpecialListing(){
        self.specialListing = ICAddOverviewPresenter.parentListingList?.parentListingData?.speciallisting ?? []
        
        self.specialListingCollectionView.reloadData()
    }
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        
        self.textFieldMiniAge.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(#colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1))//(.white)
                .warningColor(.white)
                .placeholer("Min Age")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        self.textFieldMiniAge.input.keyboardType = UIKeyboardType.numberPad
        
        self.textFieldMaxAge.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(#colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1))//(.white)
                .warningColor(.white)
                .placeholer("Max Age")
                .maxLength(HelperConstant.LIMIT_AGE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        self.textFieldMaxAge.input.keyboardType = UIKeyboardType.numberPad
        
        
        if(self.icAddRequest != nil){
            //TODO: Display server info on screen
            
            if(icAddRequest != nil && icAddRequest?.content_specific != nil){
                
                self.icSpecificInfo = icAddRequest?.content_specific
                
                // Exceptional
                self.btnStandard.isSelected = false
                self.btnExceptional.isSelected = false
                self.btnStandardExceptional.isSelected = false
                
                let serviceProvider:Int = Int(self.icSpecificInfo?.stdServices ?? "1") ?? 1
                
                switch serviceProvider{
                case 1:
                    self.btnStandard.isSelected = true
                    break
                case 2:
                    self.btnExceptional.isSelected = true
                    break
                case 3:
                    self.btnStandardExceptional.isSelected = true
                    break
                default:
                    break
                }
                
                // skill level
                if(self.icSpecificInfo?.skillLevel != nil){
                    
                    if(self.icSpecificInfo?.skillLevel?.map{$0.id}.contains("1") ?? false){
                        self.btnChkBeginner.isSelected = true
                    }
                    
                    if(self.icSpecificInfo?.skillLevel?.map{$0.id}.contains("2") ?? false){
                        self.btnChkIntermediate.isSelected = true
                    }
                    
                    if(self.icSpecificInfo?.skillLevel?.map{$0.id}.contains("3") ?? false){
                        self.btnChkExpert.isSelected = true
                    }
                }
                
//                self.min_age = self.icSpecificInfo?.minAge
//                self.max_age = self.icSpecificInfo?.maxAge
                
                self.textFieldMiniAge.input.text = self.icSpecificInfo?.minAge ?? ""
                self.textFieldMaxAge.input.text = self.icSpecificInfo?.maxAge ?? ""
                
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.icAddOtherInstructionalViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: ICAddOtherInstructionalViewController.nameOfClass) as ICAddOtherInstructionalViewController
        
        self.icAddOtherInstructionalViewController?.icAddRequest = self.icAddRequest
        icAddOtherInstructionalViewController?.isEditingInstructionalContent = isEditingInstructionalContent
        self.navigationController?.pushViewController(self.icAddOtherInstructionalViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        var selectedId:[DetailData] = []
        
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > 0){
            for expectional in self.exceptional_classification!{
                if(expectional.subcat != nil && (expectional.subcat?.count)! > 0){
                    for childExceptional in expectional.subcat!{
                        if(childExceptional.selectionStaus){
                            let detailData = DetailData()
                            detailData.id = childExceptional.listing_id ?? ""
                            detailData.title = childExceptional.listing_title ?? ""
                            
                            selectedId.append(detailData)
                        }
                    }
                }
            }
        }
        
        if(self.targetGoal != nil){
            self.targetGoalId = []
            for target in self.targetGoal!{
                if(target.selectionStaus){
                    self.targetGoalId.append(target.listing_id ?? "")
                }
            }
        }
        
        if(self.specialListing != nil){
            self.specialListingId = []
            for sepcialList in self.specialListing!{
                if(sepcialList.selectionStaus){
                    self.specialListingId.append(sepcialList.listing_id ?? "")
                }
            }
        }
        
        if (!self.btnChkBeginner.isSelected && !self.btnChkIntermediate.isSelected && !self.btnChkExpert.isSelected) {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_skill_level.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        
        if(self.btnExceptional.isSelected || self.btnStandardExceptional.isSelected){
            if(selectedId.count == 0){
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_exceptional_service_offered.getLocalized(), buttonTitle: nil, controller: nil)
                
                return false
            }
        }
        
//        if (self.textFieldMiniAge.text()?.trim().isEmpty)!{
//            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_skill_min_age.getLocalized(), buttonTitle: nil, controller: nil)
//            return false
//        }
//        
//        if (self.textFieldMaxAge.text()?.trim().isEmpty)!{
//            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_skill_max_age.getLocalized(), buttonTitle: nil, controller: nil)
//            return false
//        }
        /*
        let minAge:String = self.textFieldMiniAge.text() ?? ""
        let maxAge:String = self.textFieldMaxAge.text() ?? ""
        
        if (Int(minAge) ?? 0) >= (Int(maxAge) ?? 0){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_skill_age_diff.getLocalized(), buttonTitle: nil, controller: nil)
            return false
        }
        */
        var stdServices = ""
        if(self.btnStandard.isSelected){
            stdServices = "1"
        } else if(self.btnExceptional.isSelected){
            stdServices = "2"
        } else {
            stdServices = "3"
        }
        
//        serviceInformation.exceptionalServices = selectedId
        
        self.skillLevel = []
        if(self.skillLevel != nil){
            var detailData = DetailData()
            
            if(self.btnChkBeginner.isSelected){
                detailData.id = "1"
                detailData.title = self.btnChkBeginner.titleLabel?.text ?? ""
                 self.skillLevel?.append(detailData)
            }
            
            if(self.btnChkIntermediate.isSelected){
                detailData = DetailData()
                detailData.id = "2"
                detailData.title = self.btnChkIntermediate.titleLabel?.text ?? ""
                self.skillLevel?.append(detailData)
            }
            
            if(self.btnChkExpert.isSelected){
                detailData = DetailData()
                detailData.id = "3"
                detailData.title = self.btnChkExpert.titleLabel?.text ?? ""
                self.skillLevel?.append(detailData)
            }
        }
        
        if(UserDefault.getIC() != nil && (UserDefault.getIC()?.length)! > 0  && self.icAddRequest?.content_id == nil){
            self.icAddRequest?.content_id = UserDefault.getIC() ?? ""
        } else if(self.icAddRequest?.content_id == nil){
            self.icAddRequest?.content_id = ""
        }
        
        self.icAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        
        if(self.icSpecificInfo == nil){
            self.icSpecificInfo = ICSpecificInfo()
        }
        
        self.icSpecificInfo?.stdServices = stdServices
        self.icSpecificInfo?.exceptionalServices = selectedId
        self.icSpecificInfo?.service_target_goal = self.targetGoalId
          
        self.icSpecificInfo?.special_listing_option = self.specialListingId
        self.icSpecificInfo?.minAge = self.textFieldMiniAge.input.text ?? ""
        self.icSpecificInfo?.maxAge = self.textFieldMaxAge.input.text ?? ""
        self.icSpecificInfo?.skillLevel = self.skillLevel
        
        self.icAddRequest?.content_specific = self.icSpecificInfo
        
        return true
    }
}

extension ICAddSpecificViewController{
    
    
    @IBAction func methodStandardAction(_ sender: UIButton) {
        
        self.btnStandard.isSelected = false
        self.btnExceptional.isSelected = false
        self.btnStandardExceptional.isSelected = false
        self.isUpdate = true
        
        if(self.btnStandard == sender){
            self.btnStandard.isSelected = true
        } else if(self.btnExceptional == sender){
            self.btnExceptional.isSelected = true
        } else {
            self.btnStandardExceptional.isSelected = true
        }
    }
    
    @IBAction func methodChkSkillSelectionAction(_ sender: UIButton) {
        self.isUpdate = true
        //        self.btnChkBeginner.isSelected = false
        //        self.btnChkIntermediate.isSelected = false
        //        self.btnChkExpert.isSelected = false
        if(self.btnChkBeginner == sender){
            self.btnChkBeginner.isSelected = !self.btnChkBeginner.isSelected
        } else if(self.btnChkIntermediate == sender){
            self.btnChkIntermediate.isSelected = !self.btnChkIntermediate.isSelected
        } else if(self.btnChkExpert == sender){
            self.btnChkExpert.isSelected = !self.btnChkExpert.isSelected
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let icDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ICDetailViewController }
            if let icDetailVC = icDetailVC as? ICDetailViewController {
                icDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(icDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension ICAddSpecificViewController{
    fileprivate func reloadChildListing(){

        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > 0){
            for expectional in self.exceptional_classification!{
                self.presenter.serverChildListingRequest(parentId: expectional.listing_id ?? "")
            }
        }
    }
    
    func updateExceptionalTableList(){
        self.tableViewExceptional.reloadData()
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ICAddSpecificViewController : UITableViewDataSource , UITableViewDelegate {
    // MARK: - Tableview Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        if(tableView == self.tableViewExceptional){
            return self.exceptional_classification?.count ?? 0
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rowCount = 0
        
        if(tableView == self.tableViewExceptional && self.exceptional_classification != nil && (self.exceptional_classification?.count)! > section){
            rowCount = self.exceptional_classification![section].subcat?.count ?? 0
        }
        
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > section){
            return self.exceptional_classification![section].listing_title ?? ""
        }
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(tableView == self.tableViewExceptional){
            return 20.0;
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(tableView == self.tableViewExceptional){
            let headerView = UIView()
            headerView.backgroundColor = UIColor(hexString: ColorCode.spinnerInnerColor)
            
            let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width:
                tableView.bounds.size.width, height: tableView.bounds.size.height))
            headerLabel.backgroundColor = UIColor.clear
            headerLabel.font = FontsConfig.FontHelper.defaultRegularFontWithSize(20)
            headerLabel.textColor = #colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1) //UIColor.white
            headerLabel.text = self.tableView(self.tableViewExceptional, titleForHeaderInSection: section)
            headerLabel.sizeToFit()
            
            if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > section){
                headerLabel.text = self.exceptional_classification![section].listing_title ?? ""
            }
            
            headerView.backgroundColor = UIColor(hex: ColorCode.spinnerInnerColor)
            
            headerView.addSubview(headerLabel)
            return headerView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints(tableView: tableView)
        
        if(tableView == self.tableViewExceptional){
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessContactAddPersonnelCell.nameOfClass) as! BusinessContactAddPersonnelCell
            
            var listingData:ListingDataDetail?
            if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > indexPath.section){
                listingData = self.exceptional_classification![indexPath.section].subcat?[indexPath.row]
            }
            
            if(listingData != nil){
                cell.delegate = self
                cell.cellIndex = indexPath
                cell.lblTitle.text = listingData?.listing_title
                cell.btnCheck.isSelected = listingData?.selectionStaus ?? false
                
                if(!self.isUpdate && self.icAddRequest?.content_specific?.exceptionalServices != nil && (self.icAddRequest?.content_specific?.exceptionalServices?.count)! > 0){
                    if(self.icAddRequest?.content_specific?.exceptionalServices?.map{$0.id}.contains(listingData?.listing_id ?? "") ?? false){
                        cell.btnCheck.isSelected = true
                        listingData?.selectionStaus = true
                    }
                }
            }
            
            return cell
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Expand / Collapse Methods
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
//            self.tableView!.beginUpdates()
//            self.tableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
//            self.tableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
//            self.tableView!.beginUpdates()
//            self.tableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
//            self.tableView!.endUpdates()
        }
    }
}

/*******************************************/
//MARK: - UICollectionViewDelegate
/*******************************************/
extension ICAddSpecificViewController : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == self.targetGoalCollectionView){
            
            if(self.targetGoal != nil && (self.targetGoal?.count)! > 0){
                return self.targetGoal?.count ?? 0
            }
        } else if(collectionView == self.specialListingCollectionView){
            if(self.specialListing != nil && (self.specialListing?.count)! > 0){
                return self.specialListing?.count ?? 0
            }
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == self.targetGoalCollectionView){
            let cell = self.targetGoalCollectionView.dequeueReusableCell(withReuseIdentifier: CollectionCheckboxListCVCell.nameOfClass, for: indexPath as IndexPath) as! CollectionCheckboxListCVCell
            
            var listingData:ListingDataDetail?
            if(self.targetGoal != nil && (self.targetGoal?.count)! > indexPath.section){
                listingData = self.targetGoal![indexPath.row]
            }
            
            cell.refCollection = collectionView
            
            self.constraintsCollectionTargetHeight.constant = self.targetGoalCollectionView.intrinsicContentSize.height
            
            if(listingData != nil){
                cell.delegate = self
                cell.cellIndex = indexPath
                cell.btnCheckBox.setTitle(listingData?.listing_title ?? "", for: UIControl.State.normal)
                cell.btnCheckBox.isSelected = listingData?.selectionStaus ?? false
                
                                if(!self.isUpdate && self.icAddRequest?.content_specific?.service_target_goal != nil && (self.icAddRequest?.content_specific?.service_target_goal?.count)! > 0){
                                    if(self.icAddRequest?.content_specific?.service_target_goal?.contains(listingData?.listing_id ?? "") ?? false){
                                        cell.btnCheckBox.isSelected = true
                                        listingData?.selectionStaus = true
                                    }
                                }
                
                cell.backgroundColor = .clear
            }
            
            return cell
        } else if(collectionView == self.specialListingCollectionView){
            let cell = self.specialListingCollectionView.dequeueReusableCell(withReuseIdentifier: CollectionCheckboxListCVCell.nameOfClass, for: indexPath as IndexPath) as! CollectionCheckboxListCVCell

                    var listingData:ListingDataDetail?
                    if(self.specialListing != nil && (self.specialListing?.count)! > indexPath.section){
                        listingData = self.specialListing![indexPath.row]
                    }

                self.constraintsCollectionSpecialListingHeight.constant = self.specialListingCollectionView.intrinsicContentSize.height
            cell.refCollection = collectionView

                    if(listingData != nil){
                        cell.delegate = self
                        cell.cellIndex = indexPath
                        cell.btnCheckBox.setTitle(listingData?.listing_title ?? "", for: UIControl.State.normal)
                        cell.btnCheckBox.isSelected = listingData?.selectionStaus ?? false

                        if(!self.isUpdate && self.icAddRequest?.content_specific?.special_listing_option != nil && (self.icAddRequest?.content_specific?.special_listing_option?.count)! > 0){
                            if(self.icAddRequest?.content_specific?.special_listing_option?.contains(listingData?.listing_id ?? "") ?? false){
                                cell.btnCheckBox.isSelected = true
                                listingData?.selectionStaus = true
                            }
                        }

                        cell.backgroundColor = .clear
                    }

                    return cell
                }
        
        return UICollectionViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(collectionView == self.targetGoalCollectionView){
             return CGSize(width: (collectionView.frame.size.width ) / 2, height: 45 )
        } else if(collectionView == self.specialListingCollectionView){
             return CGSize(width: (collectionView.frame.size.width ) / 2, height: 70 )
        }
        
        return CGSize(width: 0, height: 0)
    }
}


// TODO: Multiple selection cell delegate
extension ICAddSpecificViewController: BusinessContactAddPersonnelCellDelegate{
    
    func selectListItem(cellIndex: IndexPath?, status:Bool, tableView: UITableView?){

        self.isUpdate = true
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > cellIndex?.section ?? 0){
            self.exceptional_classification![cellIndex?.section ?? 0].subcat?[cellIndex?.row ?? 0].selectionStaus = status
        }
    }
}


extension ICAddSpecificViewController:CollectionCheckboxListCVCellDelegate{
    func selectListItem(cellIndex: IndexPath?, status:Bool, refCollection: UICollectionView?){
        
        self.isUpdate = true
        if(refCollection == self.targetGoalCollectionView){
            if(self.targetGoal != nil && (self.targetGoal?.count)! > cellIndex?.section ?? 0){
                self.targetGoal![cellIndex?.row ?? 0].selectionStaus = status
                self.isUpdate = true
            }
        } else if(refCollection == self.specialListingCollectionView){
            self.specialListing![cellIndex?.row ?? 0].selectionStaus = status
            self.isUpdate = true
        }
    }
}

//
////MARK:- RangeSeekSliderDelegate
////MARK:-
//extension ICAddSpecificViewController : RangeSeekSliderDelegate {
//    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
//        print("minValue - \(minValue), maxValue - \(maxValue) ")
//        
//        self.isUpdate = true
//        self.lblAgeRange.text   =   "\(Int(minValue))-\(Int(maxValue))"
//        self.min_age    =   "\(Int(minValue))"
//        self.max_age    =   "\(Int(maxValue))"
//        
//    }
//}

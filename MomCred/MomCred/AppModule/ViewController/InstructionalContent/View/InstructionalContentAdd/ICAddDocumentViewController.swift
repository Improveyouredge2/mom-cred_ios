//
//  ICAddDocumentViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit
import AVKit

// Page 11


/**
 * ICAddDocumentViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ICAddDocumentViewController : LMBaseViewController{
    
    //    fileprivate let formNumber = 11
    
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    
    @IBOutlet weak var documentImgCollectionView: UICollectionView!
    
    var arrDocumentImages = [UploadImageData]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    
    fileprivate var businessAdditionalDocumentInfoList:[BusinessAdditionalDocumentInfo] = []
    fileprivate var presenter = ICAddDocumentPresenter()
    fileprivate var isUpdate = false
    
    var optionType:String? = "2"
    
    var icAddRequest:ICAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        documentImgCollectionView.backgroundColor = UIColor.clear
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()
        
        presenter.connectView(view: self)
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(businessAdditionalDocumentInfoList.count == 0){
            self.viewTableView.isHidden = true
        }
        
        if(self.tableView.contentSize.height > 0){
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
    
    override func pickedImage(fromImagePicker: UIImage, imageData: Data, mediaType:String, imageName:String, fileUrl: String?) {
        let imageDataTemp = UploadImageData()
        
        if(UserDefault.getIC() != nil && (UserDefault.getIC()?.length)! > 0  && self.icAddRequest?.content_id == nil){
            imageDataTemp.id = UserDefault.getIC() ?? ""
        } else if(self.icAddRequest?.content_id != nil){
            imageDataTemp.id =  self.icAddRequest?.content_id
        }
        
        imageDataTemp.imageData = imageData
        imageDataTemp.imageName = "Test_\(imageName)"
        imageDataTemp.imageKeyName = "doc_img[]"
        
        let theExt = (imageDataTemp.imageName as! NSString).pathExtension
        
        if(theExt.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpeg") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame){
            imageDataTemp.imageType = "1"
        } else {
            imageDataTemp.imageType = "2"
        }
        
        if(optionType != nil && (optionType?.length)! > 0){
            imageDataTemp.mediaCategory = optionType ?? ""
        }
        
        self.arrDocumentImages.append(imageDataTemp)
        
        self.documentImgCollectionView.reloadData()
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension ICAddDocumentViewController : UIDocumentPickerDelegate {
    
    func openDocumentController(){
        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String, "com.microsoft.word.doc", "org.openxmlformats.wordprocessingml.document"], in: .import)
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("didPickDocumentsAt: \(urls)")
        if let url = urls.first {
            let imageDataTemp = UploadImageData()
            
            if(UserDefault.getIC() != nil && (UserDefault.getIC()?.length)! > 0  && self.icAddRequest?.content_id == nil){
                imageDataTemp.id = UserDefault.getIC() ?? ""
            } else if(self.icAddRequest?.content_id != nil){
                imageDataTemp.id =  self.icAddRequest?.content_id
            }
            
            imageDataTemp.filePath = url.path
            imageDataTemp.imageData = try? Data(contentsOf: url)
            if let filename = url.pathComponents.last {
                imageDataTemp.imageName = "Test_\(filename)"
            }
            imageDataTemp.imageKeyName = "doc_img[]"
            imageDataTemp.imageType = "2"
            
            if url.pathExtension.lowercased() == "pdf" {
                imageDataTemp.imageThumbnailData = UIImage(named: "ic_pdf")?.jpegData(compressionQuality: 1)
                imageDataTemp.mimetype = "application/pdf"
            } else {
                imageDataTemp.imageThumbnailData = UIImage(named: "ic_doc")?.jpegData(compressionQuality: 1)
                if url.pathExtension.lowercased() == "docx" {
                    imageDataTemp.mimetype = "application/vnd.openxmlformats-officedocument.wordprocessingml"
                } else {
                    imageDataTemp.mimetype = "application/msword"
                }
            }

            if let optionType = optionType, !optionType.isEmpty {
                imageDataTemp.mediaCategory = optionType
            }
            
            self.arrDocumentImages.append(imageDataTemp)
            
            self.documentImgCollectionView.reloadData()
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("documentPickerWasCancelled")
    }
}

//MARK:- Button Actions
//MARK:-
extension ICAddDocumentViewController{
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnAddImg(_ sender: UIButton) {
        self.openDocumentController()
        /*
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            debugPrint("already authorized")
            self.displayPhotoSelectionOption(withCircularAllow: false)
            
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    debugPrint("access allowed")
                    self.displayPhotoSelectionOption(withCircularAllow: false)
                    
                } else {
                    //access denied
                    debugPrint("Access denied")
                    //                    LMBaseApp.sharedInstance.presentCustomAlertsViewController(self, title: LocalizationKeys.app_name.getLocalized(), message: LocalizationKeys.msg_validation_on_camera.getLocalized(), buttonTitle: LocalizationKeys.btn_ok.getLocalized().uppercased(), actionOk: {
                    
                    //                    })
                }
            })
        }
        */
    }
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnDeleteStoreImg(_ sender: UIButton) {
        
        if(self.arrDocumentImages.count > 0){
            self.arrDocumentImages.remove(at: sender.tag)
            //let imageArray = self.update_img_id.map{(String($0))}.joined(separator: ",")
            
            if self.arrDocumentImages.count == 0{
                //            self.imgDataVenuePics = nil
            }
            self.documentImgCollectionView.reloadData()
        }
    }
}

extension ICAddDocumentViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(#colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1)) //(.white)
                .warningColor(.white)
                .placeholer("Document Name")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        if(self.icAddRequest != nil){
            
            if(self.icAddRequest?.doc != nil && (self.icAddRequest?.doc?.count)! > 0){
                
                //                self.biAddRequest?.doc = self.businessAdditionalDocumentInfoList
                self.businessAdditionalDocumentInfoList = self.icAddRequest?.doc ?? []
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        isUpdate = self.isUpdate
        
        return isUpdate
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        if(UserDefault.getIC() != nil && (UserDefault.getIC()?.length)! > 0  && self.icAddRequest?.content_id == nil){
            self.icAddRequest?.content_id = UserDefault.getIC() ?? ""
        } else if(self.icAddRequest?.content_id == nil){
            self.icAddRequest?.content_id = ""
        }
        
        //        self.serviceAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        //        self.serviceAddRequest?.doc = self.businessAdditionalDocumentInfoList
        
        return true
    }
    
    fileprivate func openNextScr(){
        
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension ICAddDocumentViewController{
    fileprivate func uploadInfoOnServer(){
        
        var isFound = false
        for businessDocumentInfo in self.businessAdditionalDocumentInfoList{
            if(businessDocumentInfo != nil && !businessDocumentInfo.uploadStatus && businessDocumentInfo.imageurls == nil){
                
                let multipleAdditionalDocumentInfo = MultipleAdditionalDocumentInfo()
                
                multipleAdditionalDocumentInfo.busi_id = businessDocumentInfo.busi_id
                multipleAdditionalDocumentInfo.name = businessDocumentInfo.name
                multipleAdditionalDocumentInfo.desc = businessDocumentInfo.desc
                multipleAdditionalDocumentInfo.imageData = businessDocumentInfo.imageData
                multipleAdditionalDocumentInfo.arrImages = businessDocumentInfo.arrImages
                multipleAdditionalDocumentInfo.arrImageIdList = businessDocumentInfo.arrImageIdList
                multipleAdditionalDocumentInfo.uploadStatus = businessDocumentInfo.uploadStatus
                
                multipleAdditionalDocumentInfo.media_id = businessDocumentInfo.media_id
                multipleAdditionalDocumentInfo.media_service = businessDocumentInfo.media_service
                multipleAdditionalDocumentInfo.imageurl = businessDocumentInfo.imageurl
                multipleAdditionalDocumentInfo.imageurls = businessDocumentInfo.imageurls
                multipleAdditionalDocumentInfo.thumb = businessDocumentInfo.thumb
                multipleAdditionalDocumentInfo.media_extension = businessDocumentInfo.media_extension
                multipleAdditionalDocumentInfo.media_title = businessDocumentInfo.media_title
                multipleAdditionalDocumentInfo.media_desc = businessDocumentInfo.media_desc
                multipleAdditionalDocumentInfo.count = businessDocumentInfo.count
                
                isFound = true
                multipleAdditionalDocumentInfo.count = NSNumber(integerLiteral: businessDocumentInfo.arrImages?.count ?? 0)
                presenter.submitData(businessAdditionalDocumentInfo: multipleAdditionalDocumentInfo, imageList: businessDocumentInfo.arrImages, callback: { (status, response, message) in
                    
                    if(status == true){
                        businessDocumentInfo.uploadStatus = true
                        self.uploadInfoOnServer()
                    }
                })
                
                break
            }
        }
        
        if(!isFound){
            self.openNextScr()
        }
    }
}

extension ICAddDocumentViewController{
    @IBAction func methodNextAction(_ sender: UIButton){
        
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            if(!self.isDataUpdate()){
                if(self.isFormValid()){
                    self.openNextScr()
                }
            } else if(self.isFormValid()){
                self.uploadInfoOnServer()
            }
        }
    }
}


extension ICAddDocumentViewController{
    
    @IBAction func methodAddDocumentAction(_ sender: UIButton){
        
        var isValid = true
        let businessAdditionalDocumentInfo = BusinessAdditionalDocumentInfo()
        
        if((self.inputName.text()?.trim().length)! > 0){
            businessAdditionalDocumentInfo.name = self.inputName.text() ?? ""
            businessAdditionalDocumentInfo.media_title = self.inputName.text() ?? ""
            businessAdditionalDocumentInfo.media_service = "5"
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_document_name.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if((self.textViewDesc.text?.trim().length)! > 0){
            businessAdditionalDocumentInfo.desc = self.textViewDesc.text ?? ""
            businessAdditionalDocumentInfo.media_desc = self.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_document_description.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(arrDocumentImages.count > 0){
            if(UserDefault.getIC() != nil && (UserDefault.getIC()?.length)! > 0  && self.icAddRequest?.content_id == nil){
                businessAdditionalDocumentInfo.busi_id = UserDefault.getIC() ?? ""
            } else if(self.icAddRequest?.content_id != nil){
                businessAdditionalDocumentInfo.busi_id =  self.icAddRequest?.content_id
            }
            
            businessAdditionalDocumentInfo.arrImages = arrDocumentImages
            
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_document_image_not_found.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(isValid && self.businessAdditionalDocumentInfoList.count < HelperConstant.minimumBlocks){
            
            self.isUpdate = true
            self.businessAdditionalDocumentInfoList.append(businessAdditionalDocumentInfo)
            
            self.inputName.input.text = ""
            self.textViewDesc.text = ""
            arrDocumentImages = []
            documentImgCollectionView.reloadData()
            
            
            self.viewTableView.isHidden = false
            self.tableView.reloadData()
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ICAddDocumentViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return businessAdditionalDocumentInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationDocumentCell.nameOfClass) as! BusinessInformationAddAdditionalInformationDocumentCell
        
        let objInfo = businessAdditionalDocumentInfoList[indexPath.row]
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        if(objInfo.media_title != nil){
            cell.lblName.text = objInfo.media_title ?? ""
        } else {
            cell.lblName.text = objInfo.name
        }
        
        if(objInfo.media_desc != nil){
            cell.lblDesc.text = objInfo.media_desc ?? ""
        } else {
            cell.lblDesc.text = objInfo.desc
        }
        
        //        if(data?.imageurls != nil && (data?.imageurls?.count)! > 0){
        //            cell.arrDocumentImagesUrl = data?.imageurls?.map{$0.thumb ?? ""}
        //        }
        /*
        if(objInfo.imageurls != nil && (objInfo.imageurls?.count)! > 0){
            cell.arrDocumentImagesUrl = objInfo.imageurls?.map{$0.thumb ?? ""}
        } else {
            cell.arrDocumentImages = objInfo.arrImages?.map{ UIImage(data: $0.imageData ?? Data())!}
        }
        */
        cell.arrDocumentImages = objInfo.arrImages?.map{ UIImage(data: $0.imageThumbnailData ?? Data())!}
        cell.refreshScreenInfo()
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension ICAddDocumentViewController:BusinessInformationAddAdditionalInformationDocumentCellDelegate{
    
    func zoomableImage(cellIndex: IndexPath?) {
        
    }
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
        let objInfo = businessAdditionalDocumentInfoList[cellIndex?.row ?? 0]
        if(objInfo.imageurls != nil && (objInfo.imageurls?.count)! > 0){
            let deleteMediaRequest = DeleteMediaRequest(media_id: objInfo.media_id ?? "")
            
            self.presenter.deleteMediaData(deleteMediaRequest: deleteMediaRequest, callback: {
                (status, response, message) in
                
                if(status){
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    if(self.businessAdditionalDocumentInfoList.count > 0){
                        self.businessAdditionalDocumentInfoList.remove(at: cellIndex?.row ?? 0)
                        self.tableView.reloadData()
                    }
                } else {
                    
                }
            })
            
        } else {
            if(self.businessAdditionalDocumentInfoList.count > 0){
                self.businessAdditionalDocumentInfoList.remove(at: cellIndex?.row ?? 0)
            }
            self.tableView.reloadData()
        }
        
        if(businessAdditionalDocumentInfoList.count == 0){
            self.viewTableView.isHidden = true
            self.businessAdditionalDocumentInfoList = []
        }
    }
}


/*******************************************/
//MARK: - UICollectionViewDelegate
/*******************************************/
extension ICAddDocumentViewController : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if arrDocumentImages.count < 2{ // For 2 images
            return arrDocumentImages.count + 1
        }else{
            return arrDocumentImages.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //MARK:- Store Images views
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DocumentImageCVCell.nameOfClass, for: indexPath as IndexPath) as! DocumentImageCVCell
        
        
        if    (arrDocumentImages.count == 0 || arrDocumentImages.count + 1 == indexPath.row + 1){
            //cell.btnAddImage.isUserInteractionEnabled = true
            cell.btnAddImage.isHidden = false
            cell.imgViewDocumnt.image = nil
            cell.btnDelete.isHidden = true
            
        }else  {
            // cell.btnAddImage.isUserInteractionEnabled = false
            cell.btnAddImage.isHidden = true
            cell.imgViewDocumnt.clipsToBounds = true
            //            cell.imgViewDocumnt.image = arrDocumentImages[indexPath.row].imageData
//            cell.imgViewDocumnt.image = UIImage(data: arrDocumentImages[indexPath.row].imageData ?? Data())
            cell.imgViewDocumnt.image = UIImage(data: arrDocumentImages[indexPath.row].imageThumbnailData ?? Data())
            cell.btnDelete.isHidden = false
            cell.btnDelete.tag = indexPath.row
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
}

///********************************/
////MARK:- UICollectionViewCell
///*******************************/

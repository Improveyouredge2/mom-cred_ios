//
//  ICAddFacilityListingViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

// Page 4

/**
 * ICAddFacilityListingViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ICAddFacilityListingViewController : LMBaseViewController{
    
    //    var expandTableNumber = [Int] ()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    
    @IBOutlet weak var businessInformationQualification:BusinessInformationQualification!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingInstructionalContent: Bool = false

    fileprivate var businessQualificationInfoList:[BusinessQualificationInfo] = []
    fileprivate let formNumber = 4
    fileprivate var icAddPersonnelListingViewController:ICAddPersonnelListingViewController?
    
    var icAddRequest:ICAddRequest?
    
    fileprivate var presenter = ICAddFacilityListingPresenter()
    var facilityList:[ServiceListingList]?
    
    var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        businessInformationQualification.namePlaceHolder = LocalizationKeys.txt_link.getLocalized()
        businessInformationQualification.linkPlaceHolder = LocalizationKeys.txt_link.getLocalized()
        businessInformationQualification.textViewDesc.placeholder = LocalizationKeys.txt_description.getLocalized()
        businessInformationQualification.setScreenData()
        businessInformationQualification.updateDropDown(listInfo: [])

        presenter.connectView(view: self)
        presenter.getFacilityServiceList()
        
        self.setScreenData()

        if !isEditingInstructionalContent {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.icAddPersonnelListingViewController = nil
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(businessQualificationInfoList.count == 0){
            self.viewTableView.isHidden = true
        } else {
            self.viewTableView.isHidden = false
        }
        
        if(self.tableView.contentSize.height > 0){
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension ICAddFacilityListingViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        if(self.facilityList != nil && (self.facilityList?.count)! > 0){
            self.businessInformationQualification.updateDropDown(listInfo: self.facilityList ?? [])
        }
        
        if(self.icAddRequest != nil){
            
            if(self.icAddRequest?.content_facility != nil && (self.icAddRequest?.content_facility?.count)! > 0){
                self.businessQualificationInfoList = self.icAddRequest?.content_facility ?? []
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.icAddPersonnelListingViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: ICAddPersonnelListingViewController.nameOfClass) as ICAddPersonnelListingViewController
        
        self.icAddPersonnelListingViewController?.icAddRequest = self.icAddRequest
        icAddPersonnelListingViewController?.isEditingInstructionalContent = isEditingInstructionalContent
        self.navigationController?.pushViewController(self.icAddPersonnelListingViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
//        
//        if(self.businessQualificationInfoList != nil && self.businessQualificationInfoList.count == 0){
//            message = LocalizationKeys.error_add_facility_listing.getLocalized()
//            self.showBannerAlertWith(message: message, alert: .error)
//            return false
//        }
//        
        if(UserDefault.getIC() != nil && (UserDefault.getIC()?.length)! > 0  && self.icAddRequest?.content_id == nil){
            self.icAddRequest?.content_id = UserDefault.getIC() ?? ""
        } else if(self.icAddRequest?.content_id == nil){
            self.icAddRequest?.content_id = ""
        }
        
        self.icAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        //self.biAddRequest?.award = self.businessQualificationInfoList.toJSONString()
        self.icAddRequest?.content_facility = self.businessQualificationInfoList ?? []
        
        return true
    }
}

extension ICAddFacilityListingViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let icDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ICDetailViewController }
            if let icDetailVC = icDetailVC as? ICDetailViewController {
                icDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(icDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}


extension ICAddFacilityListingViewController{
    
    @IBAction func methodAddAwardAction(_ sender: UIButton){
        
        var isValid = true
        let businessQualificationInfo = BusinessQualificationInfo()
//        if((self.businessInformationQualification.inputName.text()?.length)! > 0){
//        if((self.businessInformationQualification.inputName.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: businessInformationQualification.inputName.text() ?? "")){
//            businessQualificationInfo.name = businessInformationQualification.inputName.text() ?? ""
//        } else {
//            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_invalid_url.getLocalized(), buttonTitle: nil, controller: nil)
//
//            return
//        }
        
        if(self.businessInformationQualification.selectedServiceListingList == nil){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_facility_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if((self.businessInformationQualification.textViewDesc.text?.length)! == 0){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_website_invalid_url_desc.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(isValid && self.businessQualificationInfoList.count < HelperConstant.minimumBlocks){
            
            
            // Check if object already exists
            if(self.businessQualificationInfoList.map{$0.id}.contains(self.businessInformationQualification.selectedServiceListingList?.id) ?? false){
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                return
            }
            
            businessQualificationInfo.id = self.businessInformationQualification.selectedServiceListingList?.id
            businessQualificationInfo.name = self.businessInformationQualification.selectedServiceListingList?.title
            businessQualificationInfo.desc = businessInformationQualification.textViewDesc.text ?? ""
            
            self.isUpdate = true
            self.businessInformationQualification.dropDownList.text = ""
            self.businessInformationQualification.selectedServiceListingList = nil
            
            self.businessInformationQualification.inputName.input.text = ""
//            self.businessInformationQualification.inputLink.input.text = ""
            
            self.businessInformationQualification.textViewDesc.text = ""
            self.businessQualificationInfoList.append(businessQualificationInfo)
            self.tableView.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ICAddFacilityListingViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return businessQualificationInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
        
        let objInfo = businessQualificationInfoList[indexPath.row]
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
//        cell.lblLink.text = objInfo.link
        cell.lblName.text = objInfo.name
        cell.lblDesc.text = objInfo.desc
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ICAddFacilityListingViewController:BusinessInformationQualificationAwardCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
        if(self.businessQualificationInfoList.count > 0){
            self.businessQualificationInfoList.remove(at: cellIndex?.row ?? 0)
            self.tableView.reloadData()
            
            if(businessQualificationInfoList.count == 0){
                self.viewTableView.isHidden = true
            }
        }
    }
}

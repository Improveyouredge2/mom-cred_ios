//
//  ICAddExternalLinkViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

// Page 10


/**
 * ICAddExternalLinkViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ICAddExternalLinkViewController : LMBaseViewController{
    
    fileprivate let formNumber = 10
    
    // Event
    //    @IBOutlet weak var tableViewSpecificTarget: UITableView!
    @IBOutlet weak var tableViewExternalLink: UITableView!
    
    //    @IBOutlet weak var constraintstableViewSpecificTargetHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintstableViewExternalLinkHeight: NSLayoutConstraint!
    
    @IBOutlet weak var inputLink: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var viewExternalLinkTableView: UIView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingInstructionalContent: Bool = false

    fileprivate var specificTargetList:[String] = []
    fileprivate var externalLinkDetailList:[ServiceExternalLinkData] = []
    fileprivate var isUpdate = false
    fileprivate var icAddDocumentViewController:ICAddDocumentViewController?
    fileprivate var presenter = ICAddExternalLinkPresenter()
    
    var icAddRequest:ICAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        //        self.tableViewSpecificTarget.backgroundColor = UIColor.clear
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()

        if !isEditingInstructionalContent {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.icAddDocumentViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        //        if(self.tableViewSpecificTarget.contentSize.height > 0){
        //            if((self.specificTargetList.count) == 0){
        //                self.constraintstableViewSpecificTargetHeight?.constant = AddMoreCell.inputViewCellSize
        //            } else {
        //                self.constraintstableViewSpecificTargetHeight?.constant = self.tableViewSpecificTarget.contentSize.height
        //            }
        //
        //            self.updateTableViewHeight(tableView: tableViewSpecificTarget)
        //        }
        
        if(self.externalLinkDetailList.count == 0){
            self.viewExternalLinkTableView.isHidden = true
        } else {
            self.viewExternalLinkTableView.isHidden = false
        }
        
        if(self.tableViewExternalLink.contentSize.height > 0){
            self.constraintstableViewExternalLinkHeight?.constant = self.tableViewExternalLink.contentSize.height
            
            self.updateTableViewHeight(tableView: self.tableViewExternalLink)
        }
        
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        //        if(tableViewSpecificTarget == tableView){
        //
        //            UIView.animate(withDuration: 0, animations: {
        //                self.tableViewSpecificTarget.layoutIfNeeded()
        //            }) { (complete) in
        //                var heightOfTableView: CGFloat = 0.0
        //                // Get visible cells and sum up their heights
        //                let cells = self.tableViewSpecificTarget.visibleCells
        //                for cell in cells {
        //                    heightOfTableView += cell.frame.height
        //                }
        //                // Edit heightOfTableViewConstraint's constant to update height of table view
        //                self.constraintstableViewSpecificTargetHeight.constant = heightOfTableView
        //            }
        //        } else
        if(tableViewExternalLink == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewExternalLink.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewExternalLink.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewExternalLinkHeight.constant = heightOfTableView
            }
        }
    }
}


extension ICAddExternalLinkViewController{
    fileprivate func setScreenData(){
        
        self.inputLink.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(#colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1)) //(.white)
                .warningColor(.white)
                .placeholer("Link ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        
        if(self.icAddRequest?.content_ext_link != nil){
            
            self.externalLinkDetailList = self.icAddRequest?.content_ext_link?.serviceExternalLinkList ?? []
            
            self.tableViewExternalLink.reloadData()
        }
        
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        var message = ""
        
//        if(self.externalLinkDetailList != nil && self.externalLinkDetailList.count == 0){
//            message = LocalizationKeys.error_add_external_link.getLocalized()
//            self.showBannerAlertWith(message: message, alert: .error)
//            return false
//        }
        
        if(UserDefault.getIC() != nil && (UserDefault.getIC()?.length)! > 0  && self.icAddRequest?.content_id == nil){
            self.icAddRequest?.content_id = UserDefault.getIC() ?? ""
        } else if(self.icAddRequest?.content_id == nil){
            self.icAddRequest?.content_id = ""
        }
        
        self.icAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if (self.externalLinkDetailList.count > 0){
            let serviceListing = ServiceExternalLink()
            serviceListing.serviceExternalLinkList = self.externalLinkDetailList
            
            self.icAddRequest?.content_ext_link = serviceListing
        } else {
            self.icAddRequest?.content_ext_link = nil
        }
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.icAddDocumentViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: ICAddDocumentViewController.nameOfClass) as ICAddDocumentViewController
        
        self.icAddDocumentViewController?.icAddRequest = self.icAddRequest
        self.navigationController?.pushViewController(self.icAddDocumentViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension ICAddExternalLinkViewController{
    @IBAction func methodAddExternalLinkAction(_ sender: UIButton){
        
        if(!((self.inputLink.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: self.inputLink.text() ?? ""))){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_invalid_url.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        //        && Helper.sharedInstance.verifyUrl(urlString: primaryWebLink.inputName.text() ?? "")
        
        if((self.inputLink.text()?.length)! > 0){
            self.isUpdate = true
            let externalLinkDetailInfo = ServiceExternalLinkData()
            externalLinkDetailInfo.link = self.inputLink.text()
            externalLinkDetailInfo.desc = self.textViewDesc.text
            
            if(self.externalLinkDetailList.count < HelperConstant.minimumBlocks){
                externalLinkDetailList.append(externalLinkDetailInfo)
                
                self.specificTargetList = []
                self.inputLink.input.text = ""
                self.textViewDesc.text = ""
                
                if(self.externalLinkDetailList.count == 1){
                    self.constraintstableViewExternalLinkHeight?.constant = 10
                }
                
                //            self.tableViewSpecificTarget.reloadData()
                self.tableViewExternalLink.reloadData()
                
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let icDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is ICDetailViewController }
            if let icDetailVC = icDetailVC as? ICDetailViewController {
                icDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(icDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension ICAddExternalLinkViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if(tableView == tableViewExternalLink){
            return externalLinkDetailList.count
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
        
        if(externalLinkDetailList.count > indexPath.row){
            let objInfo = externalLinkDetailList[indexPath.row]
            
            cell.cellIndex = indexPath
            cell.delegate = self
            cell.lblLink.text = objInfo.link
            cell.lblDesc.text = objInfo.desc
        }
        return cell
        
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return 200
    }
}

extension ICAddExternalLinkViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        self.isUpdate = true
        //        if(refTableView == self.tableViewSpecificTarget){
        //            self.specificTargetList.append(text)
        //            self.tableViewSpecificTarget.reloadData()
        //        }
    }
}

extension ICAddExternalLinkViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        self.isUpdate = true
        //        if(refTableView == self.tableViewSpecificTarget){
        //            self.specificTargetList.remove(at: cellIndex?.row ?? 0)
        //
        //            self.tableViewSpecificTarget.reloadData()
        //        }
    }
}

extension ICAddExternalLinkViewController:ExternalLinkTableViewCellDelegate{
    
    func removeExternalLinkInfo(cellIndex:IndexPath?){
        
        self.isUpdate = true
        if(self.externalLinkDetailList.count > 0){
            self.externalLinkDetailList.remove(at: cellIndex?.row ?? 0)
            self.tableViewExternalLink.reloadData()
            
            if(externalLinkDetailList.count == 0){
                self.viewExternalLinkTableView.isHidden = true
            }
        }
    }
}

//
//  ICListViewController.swift
//  MomCred
//
//  Created by consagous on 11/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


class ICListViewController: LMBaseViewController{
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tbServiceList : UITableView!
    
    @IBOutlet weak var lblNoRecordFound: UILabel!
    
    fileprivate var icAddOverviewViewController:ICAddOverviewViewController?
    
    fileprivate var insideLookDetailViewController:InsideLookDetailViewController?
    fileprivate var icDetailViewController:ICDetailViewController?
    fileprivate var menuArray: [HSMenu] = []
    
    fileprivate var presenter = ICListPresenter()
    
//    var myServiceList:[ServiceAddRequest]?
    
    var icResponse:[ICAddRequest]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter.connectView(view: self)
        
        tbServiceList.estimatedRowHeight = 100
        tbServiceList.rowHeight = UITableView.automaticDimension
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.add_new.getLocalized())
        
//        if(UserDefault.getPID() != nil && (UserDefault.getPID()?.length)! > 0){
//            let menu2 = HSMenu(icon: nil, title: LocalizationKeys.complete_pending.getLocalized())
//            menuArray = [menu1,menu2]
//        }  else{
            menuArray = [menu1]
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutSubviews()
        self.icAddOverviewViewController = nil
        self.icDetailViewController = nil
        self.insideLookDetailViewController = nil
        
        self.presenter.getInstructionalService()
    }
    
}

extension ICListViewController {
    func refreshScr(){
        if(self.tbServiceList != nil){
            self.tbServiceList.reloadData()
        }
    }
}

extension ICListViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        self.icAddOverviewViewController = nil
        if(index == 0){
            
            UserDefault.removeIC()
            
            self.icAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: ICAddOverviewViewController.nameOfClass)
            self.navigationController?.pushViewController(self.icAddOverviewViewController!, animated: true)
        }
//        else if(index == 1){
//            self.facilitiesDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: ICDetailViewController.nameOfClass) as ICDetailViewController
//            self.navigationController?.pushViewController(self.facilitiesDetailViewController!, animated: true)
//        }
    }
}

extension ICListViewController{
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension ICListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return self.arrNoteList.count
        return self.icResponse != nil ? (self.icResponse?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data       =    self.icResponse?[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: MyServiceCell.nameOfClass, for: indexPath) as! MyServiceCell
        
        cell.lblTitle.text = data?.content_name
        cell.lblDate.text = data?.create_dt?.dateFromFormat("yyyy-MM-dd HH:mm:ss")?.stringFromFormat("dd MMM yyyy") //2020-08-20 10:33:01
        cell.lblDesciption.text = data?.content_description
        
        if(data?.mediabusiness != nil && (data?.mediabusiness?.count)! > 0){
            cell.imgProfilePic.sd_setImage(with: URL(string:data?.mediabusiness![0].thumb ?? ""), placeholderImage: #imageLiteral(resourceName: "profile"))
        } else {
            cell.imgProfilePic.image = #imageLiteral(resourceName: "profile")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        if(role_id == AppUser.ServiceProvider.rawValue){
            
            self.icDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: ICDetailViewController.nameOfClass) as ICDetailViewController
            
            if(self.icDetailViewController != nil){
                self.icDetailViewController?.icAddRequest = self.icResponse?[indexPath.row]
                self.navigationController?.pushViewController(self.icDetailViewController!, animated: true)
            }
            
        } else {
            self.insideLookDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: InsideLookDetailViewController.nameOfClass) as InsideLookDetailViewController
            
            if(self.insideLookDetailViewController != nil){
                //            self.personnelDetailViewController?.serviceAddRequest = self.myServiceList?[indexPath.row]
                self.navigationController?.pushViewController(self.insideLookDetailViewController!, animated: true)
            }
        }
    }
}

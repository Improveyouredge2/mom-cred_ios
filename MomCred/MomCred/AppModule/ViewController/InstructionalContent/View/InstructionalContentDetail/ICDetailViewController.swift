//
//  ICDetailViewController.swift
//  MomCred
//
//  Created by consagous on 12/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import Lightbox
import AVKit

/**
 * ICDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ICDetailViewController : LMBaseViewController {
    
    // Page 1
    // Profile image view
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn : UIImageView!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitleValue : UILabel!
    @IBOutlet weak var lblDescValue : UILabel!
    @IBOutlet weak var lblInsideLookTypeValue : UILabel!
    @IBOutlet weak var lblServiceTypeValue : UILabel!
    @IBOutlet weak var lblLocationAddress : UILabel!
    
    // Page 2
    @IBOutlet weak var tableViewFieldServiceView: UITableView!
    @IBOutlet weak var constraintsTableViewFieldServiceHeight: NSLayoutConstraint!
    
    // Page 3
    @IBOutlet weak var tableViewServiceListingView: UITableView!
    @IBOutlet weak var constraintsTableViewServiceListingHeight: NSLayoutConstraint!
    
    // Page 4
    @IBOutlet weak var tableViewFacilityListingView: UITableView!
    @IBOutlet weak var constraintsTableViewFacilityListingHeight: NSLayoutConstraint!
    
    // Page 5
    @IBOutlet weak var tableViewPersonnelListingView: UITableView!
    @IBOutlet weak var constraintsTableViewPersonnelListingHeight: NSLayoutConstraint!
    
    // Page 6
    @IBOutlet weak var tableViewInstructionalListingView: UITableView!
    @IBOutlet weak var constraintsTableViewInstructionalListingHeight: NSLayoutConstraint!
    
    // Page 7
    @IBOutlet weak var lblStandardExceptionalValue : UILabel!
    @IBOutlet weak var lblExceptionalValue : UILabel!
    @IBOutlet weak var lblTargetGoalValue : UILabel!
    @IBOutlet weak var lblSpecialListingValue : UILabel!
    @IBOutlet weak var lblAgeRangeValueMin : UILabel!
    @IBOutlet weak var lblAgeRangeValueMax : UILabel!
    @IBOutlet weak var lblSkillLevelValue : UILabel!
    
    // Page 8
    @IBOutlet weak var tableViewOtherInsideLookView: UITableView!
    @IBOutlet weak var constraintsTableViewOtherInsideLookHeight: NSLayoutConstraint!
    
    // Form 9 Additional notes
    @IBOutlet weak var tableViewAddNotesListing : UITableView!
    @IBOutlet weak var constraintTableViewAddNotesHeight: NSLayoutConstraint!
    
    // Form 10 External link
    @IBOutlet weak var tableViewExtenalLinkListing : UITableView!
    @IBOutlet weak var constraintTableViewExtenalLinkLisitingHeight: NSLayoutConstraint!
    
    // Additional Document type display
    @IBOutlet weak var tableViewAdditionalDocument : UITableView!
    @IBOutlet weak var constraintTableViewAdditionalDocumentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var btnOptionMenu : UIButton!
    
    var icAddRequest:ICAddRequest?
    var isHideEditOption = false
    var shouldRefreshContent: Bool = false
    
    fileprivate var menuArray: [HSMenu] = []
    fileprivate var myFacilitiesAddOverviewViewController:ICAddOverviewViewController?
    fileprivate var defaultDocumentCollectionHeight:CGFloat = 0.00
    
    private var collectionContentHeightObserver: NSKeyValueObservation?

    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.isHidden = true
        defaultDocumentCollectionHeight = constraintDocumentCollectionHeight.constant
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.edit.getLocalized())
        menuArray = [menu1]
        
        // getPersonalServiceList
        setupScrInfo()
        if isHideEditOption {
            btnOptionMenu.isHidden = true
        }
        
        collectionContentHeightObserver = collectionView.observe(\.contentSize, options: [.new]) { [weak self] _, value in
            if let height = value.newValue?.height, height > 0 {
                self?.constraintDocumentCollectionHeight.constant = height
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myFacilitiesAddOverviewViewController = nil
        
        if shouldRefreshContent, let insId = icAddRequest?.content_id {
            ICService.getInstructionalContentDetailFor(instructionalId: insId) { (request, error) in
                DispatchQueue.main.async { [weak self] in
                    Spinner.hide()
                    if let request = request {
                        self?.icAddRequest = request
                        self?.setupScrInfo()
                    }
                }
            }
        }
    }

    deinit {
        collectionContentHeightObserver?.invalidate()
    }
}

extension ICDetailViewController{
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
    
    @IBAction func actionZoomOption(_ sender: AnyObject) {
        zoomableView()
    }
}


extension ICDetailViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if index == 0 {
            myFacilitiesAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: ICAddOverviewViewController.nameOfClass)
            
            myFacilitiesAddOverviewViewController?.icAddRequest = icAddRequest
            myFacilitiesAddOverviewViewController?.isEditingInstructionalContent = true
            navigationController?.pushViewController(myFacilitiesAddOverviewViewController!, animated: true)
        }
    }
}

extension ICDetailViewController{
    fileprivate func updateViewConstraints(tableView:UITableView) {
        
        // Page 2
        if(tableView == self.tableViewFieldServiceView){
            self.tableViewFieldServiceView.scrollToBottom()
            if(self.tableViewFieldServiceView.contentSize.height > 0){
                self.constraintsTableViewFieldServiceHeight?.constant =
                    self.tableViewFieldServiceView.contentSize.height
                
                self.updateTableViewHeight(tableView:self.tableViewFieldServiceView)
            }
        }
        
        // Page 3
        if(tableView == self.tableViewServiceListingView){
            self.tableViewServiceListingView.scrollToBottom()
            if(self.tableViewServiceListingView.contentSize.height > 0){
                self.constraintsTableViewServiceListingHeight?.constant =
                    self.tableViewServiceListingView.contentSize.height
                
                self.updateTableViewHeight(tableView:self.tableViewServiceListingView)
            }
        }
        
        // Page 4
        if(tableView == self.tableViewFacilityListingView){
            self.tableViewFacilityListingView.scrollToBottom()
            if(self.tableViewFacilityListingView.contentSize.height > 0){
                self.constraintsTableViewFacilityListingHeight?.constant =
                    self.tableViewFacilityListingView.contentSize.height
                
                self.updateTableViewHeight(tableView:self.tableViewFacilityListingView)
            }
        }
        
        // Page 5
        if(tableView == self.tableViewPersonnelListingView){
            self.tableViewPersonnelListingView.scrollToBottom()
            if(self.tableViewPersonnelListingView.contentSize.height > 0){
                self.constraintsTableViewPersonnelListingHeight.constant =
                    self.tableViewPersonnelListingView.contentSize.height
                
                self.updateTableViewHeight(tableView:self.tableViewPersonnelListingView)
            }
        }
        
        // Page 6
        if(tableView == self.tableViewInstructionalListingView){
            self.tableViewInstructionalListingView.scrollToBottom()
            if(self.tableViewInstructionalListingView.contentSize.height > 0){
                self.constraintsTableViewInstructionalListingHeight?.constant =
                    self.tableViewInstructionalListingView.contentSize.height
                
                self.updateTableViewHeight(tableView:self.tableViewInstructionalListingView)
            }
        }
        
        // Page 8
        if(tableView == self.tableViewOtherInsideLookView){
            self.tableViewOtherInsideLookView.scrollToBottom()
            if(self.tableViewOtherInsideLookView.contentSize.height > 0){
                self.constraintsTableViewOtherInsideLookHeight?.constant =
                    self.tableViewOtherInsideLookView.contentSize.height
                
                self.updateTableViewHeight(tableView:self.tableViewOtherInsideLookView)
            }
        }
        
        // Form 9 Additional notes
        if(tableView == self.tableViewAddNotesListing){
            self.tableViewAddNotesListing.scrollToBottom()
            if(self.tableViewAddNotesListing.contentSize.height > 0){
                self.constraintTableViewAddNotesHeight?.constant =
                    self.tableViewAddNotesListing.contentSize.height
                
                self.updateTableViewHeight(tableView:self.tableViewAddNotesListing)
            }
        }
        
        // Form 10 External link
        if(tableView == self.tableViewExtenalLinkListing){
            self.tableViewExtenalLinkListing.scrollToBottom()
            if(self.tableViewExtenalLinkListing.contentSize.height > 0){
                self.constraintTableViewExtenalLinkLisitingHeight?.constant =
                    self.tableViewExtenalLinkListing.contentSize.height
                
                self.updateTableViewHeight(tableView:self.tableViewExtenalLinkListing)
            }
        }
        
        if(tableView == self.tableViewAdditionalDocument){
            self.tableViewAdditionalDocument.scrollToBottom()
            if(self.tableViewAdditionalDocument.contentSize.height > 0){
                self.constraintTableViewAdditionalDocumentHeight?.constant =
                    self.tableViewAdditionalDocument.contentSize.height
                
                self.updateTableViewHeight(tableView:self.tableViewAdditionalDocument)
            }
        }
    }
    
    fileprivate func updateTableViewHeight(tableView:UITableView) {
        // Page 2
        if(tableView == self.tableViewFieldServiceView){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewFieldServiceView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewFieldServiceView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintsTableViewFieldServiceHeight.constant = heightOfTableView
            }
        }
        
        // Page 3
        if(tableView == self.tableViewServiceListingView){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewServiceListingView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewServiceListingView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintsTableViewServiceListingHeight.constant = heightOfTableView
            }
        }
        
        // Page 4
        if(tableView == self.tableViewFacilityListingView){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewFacilityListingView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewFacilityListingView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintsTableViewFacilityListingHeight.constant = heightOfTableView
            }
        }
        
        // Page 5
        if(tableView == self.tableViewPersonnelListingView){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewPersonnelListingView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewPersonnelListingView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintsTableViewPersonnelListingHeight.constant = heightOfTableView
            }
        }
        
        // Page 6
        if(tableView == self.tableViewInstructionalListingView){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewInstructionalListingView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewInstructionalListingView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintsTableViewInstructionalListingHeight.constant = heightOfTableView
            }
        }
        
        // Page 8
        if(tableView == self.tableViewOtherInsideLookView){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewOtherInsideLookView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewOtherInsideLookView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintsTableViewOtherInsideLookHeight.constant = heightOfTableView
            }
        }
        
        // Form 9 Additional notes
        if(tableView == self.tableViewAddNotesListing){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewAddNotesListing.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewAddNotesListing.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewAddNotesHeight.constant = heightOfTableView
            }
        }
        
        // Form 10 External link
        if(tableView == self.tableViewExtenalLinkListing){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewExtenalLinkListing.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewExtenalLinkListing.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewExtenalLinkLisitingHeight.constant = heightOfTableView
            }
        }
        
        // Additional Document type display
        if(tableView == self.tableViewAdditionalDocument){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewAdditionalDocument.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewAdditionalDocument.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewAdditionalDocumentHeight.constant = heightOfTableView
            }
        }
    }
    
    fileprivate func setupScrInfo(){
        
        if(self.icAddRequest != nil){
            self.constraintDocumentCollectionHeight.constant = self.defaultDocumentCollectionHeight
            
            self.collectionView.reloadData()
            if(self.icAddRequest?.mediabusiness != nil && (self.icAddRequest?.mediabusiness?.count)! > 0){
                self.imgProfile.sd_setImage(with: URL(string: (self.icAddRequest?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((self.icAddRequest?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    self.imgPlayBtn.isHidden = false
                } else {
                    self.imgPlayBtn.isHidden = true
                }
                
                
            } else {
                self.imgPlayBtn.isHidden = true
                self.imgProfile.image = nil
                self.constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
            }
            
            self.lblTitleValue.text = self.icAddRequest?.content_name ?? ""
            
            self.lblDescValue.text = self.icAddRequest?.content_description ?? ""
            
            if(self.icAddRequest?.content_type_title != nil && (self.icAddRequest?.content_type_title?.count)! > 0){
                self.lblInsideLookTypeValue.text = "• \(String(describing: self.icAddRequest?.content_type_title?.joined(separator: "\n• ") ?? "") )"
            } else {
                self.lblInsideLookTypeValue.text = ""
            }
            
            if(self.icAddRequest?.content_target_goal != nil && (self.icAddRequest?.content_target_goal?.count)! > 0){
                self.lblServiceTypeValue.text = "• \(String(describing: self.icAddRequest?.content_target_goal?.joined(separator: "\n• ") ?? "") )"
            } else {
                self.lblServiceTypeValue.text = ""
            }
            
            self.lblLocationAddress.text = ""
            
            if(self.icAddRequest?.content_location_info != nil){
                var address = ""
                
                if(self.icAddRequest?.content_location_info?.location_name != nil && (self.icAddRequest?.content_location_info?.location_name?.length)! > 0){
                    address = "\(self.icAddRequest?.content_location_info?.location_name ?? "") \n\(self.icAddRequest?.content_location_info?.location_address ?? "")"
                    
                } else if(self.icAddRequest?.content_location_info?.location_name_sec != nil && (self.icAddRequest?.content_location_info?.location_name_sec?.length)! > 0){
                    address = "\(self.icAddRequest?.content_location_info?.location_name_sec ?? "") \n\(self.icAddRequest?.content_location_info?.location_address_sec ?? "")"
                }
                
                self.lblLocationAddress.text = address
            } else {
                self.lblLocationAddress.text = ""
            }
            
            if(self.icAddRequest == nil || self.icAddRequest?.content_field == nil || (self.icAddRequest?.content_field?.count)! == 0 ){
                self.constraintsTableViewFieldServiceHeight.constant = 0
            }
                
            if(self.icAddRequest == nil || self.icAddRequest?.content_service_listing == nil || (self.icAddRequest?.content_service_listing?.count)! == 0){
                self.constraintsTableViewServiceListingHeight.constant = 0
            }

            if(self.icAddRequest == nil || self.icAddRequest?.content_facility == nil || (self.icAddRequest?.content_facility?.count)! == 0){
                self.constraintsTableViewFacilityListingHeight.constant = 0
            }
            
            if(self.icAddRequest == nil || self.icAddRequest?.content_personal_listing == nil || (self.icAddRequest?.content_personal_listing?.count)! == 0){
                self.constraintsTableViewPersonnelListingHeight.constant = 0
            }
            
            if(self.icAddRequest == nil || self.icAddRequest?.content_instruction_content == nil || (self.icAddRequest?.content_instruction_content?.count)! == 0){
                self.constraintsTableViewInstructionalListingHeight.constant = 0
            }
            
            if(self.icAddRequest == nil || self.icAddRequest?.content_other == nil || (self.icAddRequest?.content_other?.count)! == 0){
                self.constraintsTableViewOtherInsideLookHeight.constant = 0
            }
            
            if(self.icAddRequest == nil || self.icAddRequest?.content_notes == nil || self.icAddRequest?.content_notes?.additionalNotes == nil || (self.icAddRequest?.content_notes?.additionalNotes?.count)! == 0){
                self.constraintTableViewAddNotesHeight.constant = 0
            }
            
            if(self.icAddRequest == nil || self.icAddRequest?.content_ext_link == nil || self.icAddRequest?.content_ext_link?.serviceExternalLinkList == nil || (self.icAddRequest?.content_ext_link?.serviceExternalLinkList?.count)! == 0){
                self.constraintTableViewExtenalLinkLisitingHeight.constant = 0
            }
            
            if(self.icAddRequest == nil || self.icAddRequest?.doc == nil || (self.icAddRequest?.doc?.count)! == 0){
                self.constraintTableViewAdditionalDocumentHeight.constant = 0
            }
            
            if(self.icAddRequest?.content_specific != nil && self.icAddRequest?.content_specific?.stdServices != nil){
                
                self.lblStandardExceptionalValue.text =  self.icAddRequest?.content_specific?.stdServices != nil ? ( Int(self.icAddRequest?.content_specific?.stdServices ?? "0") == 3 ? LocalizationKeys.std_exp.getLocalized() : (Int(self.icAddRequest?.content_specific?.stdServices ?? "0") == 2 ? LocalizationKeys.exceptional.getLocalized() : LocalizationKeys.standard.getLocalized())) : LocalizationKeys.standard.getLocalized()
            } else {
                self.lblStandardExceptionalValue.text = ""
            }
            
            if(self.icAddRequest?.content_specific != nil && self.icAddRequest?.content_specific?.exceptionalServices != nil && (self.icAddRequest?.content_specific?.exceptionalServices?.count)! > 0){
                
                let expTypeTitleList:[String] = self.icAddRequest?.content_specific?.exceptionalServices != nil ? (self.icAddRequest?.content_specific?.exceptionalServices?.map{$0.title})! as! [String] : [""]
                self.lblExceptionalValue.text = "• \(expTypeTitleList.joined(separator: "\n• ") )"
                
            } else {
                self.lblExceptionalValue.text = ""
            }
            
            if let dataToDisplay = self.icAddRequest?.content_specific?.exceptionalServicesInfo {
                let combination = NSMutableAttributedString()

                for (index, data) in dataToDisplay.enumerated() {
                    let title = data.value ?? ""
                    var stringToDisplay = ""
                    if let arrSubTitle = data.data?.map({$0.title ?? "-"}) {
                        stringToDisplay = "• \(arrSubTitle.joined(separator: "\n• "))"
                        if (index + 1) != dataToDisplay.count {
                            stringToDisplay.append("\n\n")
                        }
                    }
                    
                    
                    let yourAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1) , NSAttributedString.Key.font: self.lblExceptionalValue.font] as [NSAttributedString.Key : Any]
                    let yourOtherAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: self.lblExceptionalValue.font] as [NSAttributedString.Key : Any]

                    let partOne = NSMutableAttributedString(string: "\(title)\n", attributes: yourAttributes)
                    let partTwo = NSMutableAttributedString(string: stringToDisplay, attributes: yourOtherAttributes)


                    combination.append(partOne)
                    combination.append(partTwo)

                    
                    
//                    self.lblExceptionalValue.text       =       title + stringToDisplay
                }
                self.lblExceptionalValue.attributedText =   combination

            } else {
                self.lblExceptionalValue.text = ""
            }
            
            if(self.icAddRequest?.content_specific != nil && self.icAddRequest?.content_specific?.service_target_goal_title != nil && (self.icAddRequest?.content_specific?.service_target_goal_title?.count)! > 0){
                
//                self.lblTargetGoalValue.text = "• \(self.icAddRequest?.content_specific?.service_target_goal_title?.joined(separator: "\n• ") )"
                
               self.lblTargetGoalValue.text = "• \(self.icAddRequest?.content_specific?.service_target_goal_title?.joined(separator: "\n• ") ?? "")"
                
            } else {
                self.lblTargetGoalValue.text = ""
            }
            
            if(self.icAddRequest?.content_specific != nil && self.icAddRequest?.content_specific?.special_listing_option_title != nil && (self.icAddRequest?.content_specific?.special_listing_option_title?.count)! > 0){
                
//                self.lblSpecialListingValue.text = "• \(self.icAddRequest?.content_specific?.special_listing_option_title?.joined(separator: "\n• ") )"
                
                self.lblSpecialListingValue.text = "• \(self.icAddRequest?.content_specific?.special_listing_option_title?.joined(separator: "\n• ") ?? "")"
                
            } else {
                self.lblSpecialListingValue.text = ""
            }
            
            if(self.icAddRequest?.content_specific != nil && self.icAddRequest?.content_specific?.minAge != nil && self.icAddRequest?.content_specific?.maxAge != nil){
                self.lblAgeRangeValueMax.text      =   "\(self.icAddRequest?.content_specific?.maxAge ?? "")"
                self.lblAgeRangeValueMin.text      =   "\(self.icAddRequest?.content_specific?.minAge ?? "")"
            } else {
                self.lblAgeRangeValueMax.text       =               ""
                self.lblAgeRangeValueMin.text       =               ""
            }
            
            if(self.icAddRequest?.content_specific != nil && self.icAddRequest?.content_specific?.skillLevel != nil && (self.icAddRequest?.content_specific?.skillLevel?.count)! > 0){
                
                let expTypeTitleList:[String] = self.icAddRequest?.content_specific?.skillLevel != nil ? (self.icAddRequest?.content_specific?.skillLevel?.map{$0.title})! as! [String] : [""]
                self.lblSkillLevelValue.text = "• \(expTypeTitleList.joined(separator: "\n• ") )"
                
            } else {
                self.lblSkillLevelValue.text = ""
            }
                
                

            
            self.scrollView.isHidden = false
//            self.viewCreateNew.isHidden = true
//            self.btnOptionMenu.isHidden = false
        } else {
//            self.viewCreateNew.isHidden = false
            self.scrollView.isHidden = true
//            self.btnOptionMenu.isHidden = true
        }
    }
    
    fileprivate func zoomableView(){
        // Create an array of images.
        var images:[LightboxImage] = []
        
        if(self.icAddRequest?.mediabusiness != nil && (self.icAddRequest?.mediabusiness?.count)! > 0){
            
            for media in (self.icAddRequest?.mediabusiness)! {
                //            self.imgProfile.sd_setImage(with: URL(string: (self.biAddRequest?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((media.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    
                    images.append(LightboxImage(imageURL: URL(string: media.thumb ?? "")!, text: "", videoURL: URL(string: media.imageurl ?? "")!))
                    
                } else {
                    images.append(LightboxImage(
                        imageURL: URL(string: media.imageurl ?? "")!
                    ))
                }
            }
        }
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)
        // Present your controller.
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
        
        LightboxConfig.handleVideo = { from, videoURL in
            // Custom video handling
            let videoController = AVPlayerViewController()
            videoController.player = AVPlayer(url: videoURL)
            
            from.present(videoController, animated: true) {
                videoController.player?.play()
            }
        }
    }
}


extension ICDetailViewController: LightboxControllerPageDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension ICDetailViewController: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}

extension ICDetailViewController: UITableViewDelegate{
    
    /**
     *  Specify height of row in tableview with UITableViewDelegate method override.
     *
     *  @param key tableView object and heightForRowAt index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    /**
     *  Specify number of section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.tableViewFieldServiceView){
            return (self.icAddRequest != nil && self.icAddRequest?.content_field != nil) ? (self.icAddRequest?.content_field?.count)! : 0
            
        } else if(tableView == self.tableViewServiceListingView){
            return (self.icAddRequest != nil && self.icAddRequest?.content_service_listing != nil) ? (self.icAddRequest?.content_service_listing?.count)! : 0

        } else if(tableView == self.tableViewFacilityListingView){
            return (self.icAddRequest != nil && self.icAddRequest?.content_facility != nil) ? (self.icAddRequest?.content_facility?.count)! : 0
            
        }  else if(tableView == self.tableViewPersonnelListingView){
            return (self.icAddRequest != nil && self.icAddRequest?.content_personal_listing != nil) ? (self.icAddRequest?.content_personal_listing?.count)! : 0
            
        }  else if(tableView == self.tableViewInstructionalListingView){
            return (self.icAddRequest != nil && self.icAddRequest?.content_instruction_content != nil) ? (self.icAddRequest?.content_instruction_content?.count)! : 0
            
        } else if(tableView == self.tableViewOtherInsideLookView){
            return (self.icAddRequest != nil && self.icAddRequest?.content_other != nil) ? (self.icAddRequest?.content_other?.count)! : 0

        } else if(tableView == self.tableViewAddNotesListing){
            return (self.icAddRequest != nil && self.icAddRequest?.content_notes != nil && self.icAddRequest?.content_notes?.additionalNotes != nil) ? (self.icAddRequest?.content_notes?.additionalNotes?.count)! : 0

        } else if(tableView == self.tableViewExtenalLinkListing){
            return (self.icAddRequest != nil && self.icAddRequest?.content_ext_link != nil && self.icAddRequest?.content_ext_link?.serviceExternalLinkList != nil) ? (self.icAddRequest?.content_ext_link?.serviceExternalLinkList?.count)! : 0
        
        } else if(tableView == self.tableViewAdditionalDocument){
            return (self.icAddRequest != nil && self.icAddRequest?.doc != nil) ? (self.icAddRequest?.doc?.count)! : 0
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        updateViewConstraints(tableView: tableView)
        
        if(tableView == self.tableViewFieldServiceView){
            let cell = tableView.dequeueReusableCell(withIdentifier: BIFieldServiceCell.nameOfClass) as! BIFieldServiceCell
            
            cell.lblFieldIndex.text     =       "Field \(indexPath.row + 1)"
            
            let objInfo = self.icAddRequest?.content_field![indexPath.row]
            
            cell.lblCatName.text = objInfo?.catFieldName ?? ""
            
            if(objInfo?.add_new_specific_field != nil && (objInfo?.add_new_specific_field?.length)! > 0){
                cell.lblSubCatName.text = objInfo?.add_new_specific_field ?? ""
            } else {
                cell.lblSubCatName.text = objInfo?.specificFieldName ?? ""
            }
            
            
            if(objInfo?.classification != nil && (objInfo?.classification?.count)! > 0){
                cell.lblClassification.text = "• \(objInfo?.classification?.joined(separator: "\n• ") ?? "")"
            } else {
                cell.lblClassification.text = ""
            }
            
            if(objInfo?.technique != nil && (objInfo?.technique?.count)! > 0){
                cell.lblTechnique.text = "• \(objInfo?.technique?.joined(separator: "\n• ") ?? "")"
            } else {
                cell.lblTechnique.text = ""
            }
            
            
            cell.backgroundColor = UIColor.clear
            return cell
            
        } else if(tableView == self.tableViewServiceListingView){
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            cell.lblIndex.text     =       "Service Listing \(indexPath.row + 1)"

            let objInfo = self.icAddRequest?.content_service_listing![indexPath.row]
            
            cell.lblName.text = objInfo?.name ?? ""
            cell.lblDesc.text = objInfo?.desc ?? ""
            
            return cell
            
        } else if(tableView == self.tableViewFacilityListingView){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            cell.lblIndex.text     =       "Facility Listing \(indexPath.row + 1)"

            let objInfo = self.icAddRequest?.content_facility![indexPath.row]
            
            cell.lblName.text = objInfo?.name ?? ""
            cell.lblDesc.text = objInfo?.desc ?? ""
            
            return cell
            
        } else if(tableView == self.tableViewPersonnelListingView){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            cell.lblIndex.text     =       "Personnel Listing \(indexPath.row + 1)"

            let objInfo = self.icAddRequest?.content_personal_listing![indexPath.row]
            
            cell.lblName.text = objInfo?.name ?? ""
            cell.lblDesc.text = objInfo?.desc ?? ""
            
            return cell
            
        } else if(tableView == self.tableViewInstructionalListingView){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            cell.lblIndex.text     =       "Inside-Look Content \(indexPath.row + 1)"
            
            let objInfo = self.icAddRequest?.content_instruction_content![indexPath.row]
            
            cell.lblName.text = objInfo?.name ?? ""
            cell.lblDesc.text = objInfo?.desc ?? ""
            
            return cell
            
        } else if(tableView == self.tableViewOtherInsideLookView){
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            cell.lblIndex.text     =       "Instructional Content \(indexPath.row + 1)"

            let objInfo = self.icAddRequest?.content_other![indexPath.row]
            
            cell.lblName.text = objInfo?.name ?? ""
            cell.lblDesc.text = objInfo?.desc ?? ""
            
            return cell
            
        } else if(tableView == self.tableViewAddNotesListing){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: AdditionalTitleDescView.nameOfClass) as! AdditionalTitleDescView
            cell.lblIndex.text = "Additional notes \(indexPath.row + 1)"
            
            let objInfo = self.icAddRequest?.content_notes?.additionalNotes![indexPath.row]
            cell.lblTitleDesc.text = "• \(objInfo ?? "")"
            
            return cell
            
        } else if(tableView == self.tableViewExtenalLinkListing){
            let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
            cell.lblIndex.text = "External Link \(indexPath.row + 1)"
            let objInfo = self.icAddRequest?.content_ext_link?.serviceExternalLinkList?[indexPath.row]
            cell.lblLink.text = objInfo?.link
            cell.lblDesc.text = objInfo?.desc
            cell.backgroundColor = UIColor.clear
            
            return cell
            
        } else if(tableView == self.tableViewAdditionalDocument){
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationDocumentCell.nameOfClass) as! BusinessInformationAddAdditionalInformationDocumentCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            cell.lblIndex.text  =       "Information Document \(indexPath.row + 1)"
            
            let data = self.icAddRequest?.doc![indexPath.row]
            
            cell.lblName.text = data?.media_title ?? ""
            cell.lblDesc.text = data?.media_desc ?? ""
            if(data?.imageurls != nil && (data?.imageurls?.count)! > 0){
                cell.arrDocumentImagesUrl = data?.imageurls?.map{$0.thumb ?? ""}
            }
            cell.imgCollectionView.tag = indexPath.row
            cell.imgCollectionView.delegate = self
            cell.refreshScreenInfo()
            
            return cell
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableViewServiceListingView {
            let objInfo = self.icAddRequest?.content_service_listing![indexPath.row]
            if let id = objInfo?.id {
                showServiceDetail(for: id)
            }
        } else if tableView == self.tableViewFacilityListingView {
            let objInfo = self.icAddRequest?.content_facility![indexPath.row]
            if let id = objInfo?.id {
                showFacilityDetail(for: id)
            }
        } else if tableView == self.tableViewPersonnelListingView {
            let objInfo = self.icAddRequest?.content_personal_listing![indexPath.row]
            if let id = objInfo?.id {
                showPersonalDetail(for: id)
            }
        } else if tableView == self.tableViewInstructionalListingView {
            let objInfo = self.icAddRequest?.content_instruction_content![indexPath.row]
            if let id = objInfo?.id {
                showInsideLookDetail(for: id)
            }
        } else if tableView == self.tableViewOtherInsideLookView {
            let objInfo = self.icAddRequest?.content_other![indexPath.row]
            if let id = objInfo?.id {
                showInstructionalDetail(for: id)
            }
        }  else if(tableView == self.tableViewExtenalLinkListing){
            let objInfo = self.icAddRequest?.content_ext_link?.serviceExternalLinkList?[indexPath.row]
            if let link = objInfo?.link?.trim(), !link.isEmpty {
                openLink(for: link)
            }
        }
    }
}

extension ICDetailViewController: UITableViewDataSource{
}


extension ICDetailViewController : UICollectionViewDataSource {
    
    /**
     *  Specify number of rows on section in collectionView with UICollectionViewDataSource method override.
     *
     *  @param key UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.icAddRequest != nil && self.icAddRequest?.mediabusiness != nil && (self.icAddRequest?.mediabusiness?.count)! > 0) ? (self.icAddRequest?.mediabusiness?.count)! : 0
    }
    
    /**
     *  Implement collectionview cell in collectionView with UICollectionViewDataSource method override.
     *
     *  @param UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        let productInfo = productList?[indexPath.row]
        
        // Default product cell
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath) as! SerivceImageCell
        cell.delegate =  self
        cell.cellIndex = indexPath.row
        
        let data = self.icAddRequest?.mediabusiness![indexPath.row]
        
        if(data?.thumb != nil){
            cell.imgService.sd_setImage(with: URL(string: (data?.thumb ?? "")))
        }
        
        if((data?.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
            cell.imgPlay.isHidden = false
        } else {
            cell.imgPlay.isHidden = true
        }
        
        return cell
    }
}

extension ICDetailViewController : UICollectionViewDelegate {
    
    /**
     *  Implement tap event on collectionview cell in collectionView with UICollectionViewDelegate method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let data = self.icAddRequest?.doc![collectionView.tag]
        if let link = data?.imageurls?[indexPath.row].imageurls?.trim(), !link.isEmpty {
            openLink(for: link)
        }
    }
}

extension ICDetailViewController : UICollectionViewDelegateFlowLayout {
    
    /**
     *  Implement collectionView with UICollectionViewDelegateFlowLayout method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:100, height: 80)
    }
}

extension ICDetailViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        if(self.icAddRequest?.mediabusiness != nil && (self.icAddRequest?.mediabusiness?.count)! > 0){
            if((self.icAddRequest?.mediabusiness![cellIndex].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.imgProfile.sd_setImage(with: URL(string: (self.icAddRequest?.mediabusiness![cellIndex].thumb ?? "")))
        }
    }
}

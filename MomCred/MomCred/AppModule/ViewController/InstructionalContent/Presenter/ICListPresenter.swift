//
//  MyFacilitiesListPresenter.swift
//  MomCred
//
//  Created by consagous on 12/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
//personnelResponse


/**
 *  ICListPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ICListPresenter {
    
    var view:ICListViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ICListViewController) {
        self.view = view
    }
}

extension ICListPresenter{
    
    /**
     *  Get Personal service list from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getInstructionalService(){
        
        // TODO: Update after udpate
//        let response = [PersonnelAddRequest().getModelObjectFromServerResponse(jsonResponse: self.dummyData() as AnyObject)]
        
//        self.view.personnelResponse = response as! [PersonnelAddRequest]
//
//        self.view.refreshScr()
        

        ICService.getInstructionalService(callback: { (status, response, message) in

            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()

                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        self.view.icResponse = response?.data ?? []
                    }
                    
                    self.view.lblNoRecordFound.isHidden = true
                    self.view.tbServiceList.isHidden = false

                    self.view.refreshScr()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.lblNoRecordFound.text = message ?? ""
                    self.view.lblNoRecordFound.isHidden = false
                    self.view.tbServiceList.isHidden = true

                    self.view.refreshScr()
                }
            }
        })
    }
}

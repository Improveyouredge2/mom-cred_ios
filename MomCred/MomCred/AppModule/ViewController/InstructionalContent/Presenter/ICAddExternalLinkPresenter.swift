//
//  MyFacilitiesAddExternalLinkPresenter.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  ICAddExternalLinkPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ICAddExternalLinkPresenter {
    
    var view:ICAddExternalLinkViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ICAddExternalLinkViewController) {
        self.view = view
    }
}

extension ICAddExternalLinkPresenter{
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: ICAddResponse?, _ message: String?) -> Void) {
        
        ICService.updateData(serviceAddRequest: view.icAddRequest, imageList: nil, callback: { (status, response, message) in
            DispatchQueue.main.async {
                Spinner.hide()
                
                if status {
                    callback(status, response, message)
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, response, message)
                }
            }
        })
    }
}

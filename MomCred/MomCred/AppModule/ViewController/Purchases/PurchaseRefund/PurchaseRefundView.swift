//
//  PurchaseRefundView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PurchaseRefundView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
//    @IBOutlet weak var tblTestimonialList : UITableView!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblReviewCount : UILabel!
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    
    @IBOutlet weak var btnOption1 : UIButton!
    @IBOutlet weak var btnOption2 : UIButton!
    @IBOutlet weak var btnOption3 : UIButton!
    @IBOutlet weak var btnOption4 : UIButton!
    @IBOutlet weak var btnOption5 : UIButton!
    @IBOutlet weak var textViewComment : UITextView!
    
    fileprivate var selectedIndex = -1
    fileprivate var selectedText = ""
    
    
    //MARK:- Var(s)
    var presenter       =       PurchaseRefundPresenter()
    var servicePurchaseId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)
        self.textViewComment.isUserInteractionEnabled = false
    }
}

//MARK:- Action method implementation
extension PurchaseRefundView{
    
    @IBAction func actionSelectedOptionMethod(_ sender:UIButton){
        
        btnOption1.isSelected = false
        btnOption2.isSelected = false
        btnOption3.isSelected = false
        btnOption4.isSelected = false
        btnOption5.isSelected = false
        
        sender.isSelected = true
        selectedIndex = sender.tag
        selectedText = sender.titleLabel?.text ?? ""
        
        if(btnOption1.isSelected || btnOption2.isSelected || btnOption3.isSelected || btnOption4.isSelected){
            self.textViewComment.text = ""
            self.textViewComment.isUserInteractionEnabled = false
        } else {
            self.textViewComment.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func actionSubmitMethod(_ sender:UIButton){
        let refundRequest = RefundRequest()
        refundRequest.purchase_id = self.servicePurchaseId
        refundRequest.reason = "\(selectedText)"
        
        if(selectedIndex == self.btnOption5.tag){
            refundRequest.comment = self.textViewComment.text ?? ""
        } else {
            refundRequest.comment = ""
        }
        presenter.serverRefund(refundRequest: refundRequest)
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PurchaseRefundView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.arrNoteList.count
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let data       =    self.arrNoteList[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: PMTestimonialTableViewCell.nameOfClass, for: indexPath) as! PMTestimonialTableViewCell

        //TODO: For testing
        let profileImageUrl = PMUserDefault.getProfilePic()
        cell.imgProfilePic.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
        
        cell.lblDesciption.text = "sfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd fl"
//
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
}

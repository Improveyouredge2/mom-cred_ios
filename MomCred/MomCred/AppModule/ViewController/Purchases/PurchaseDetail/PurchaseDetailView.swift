//
//  PurchaseDetailView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Lightbox

class PurchaseDetailView: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableViewVoucher: UITableView! {
        didSet {
            tableViewVoucher.estimatedRowHeight = 100
            tableViewVoucher.rowHeight = UITableView.automaticDimension
        }
    }
    @IBOutlet weak var tableViewPackage: UITableView! {
        didSet {
            tableViewPackage.estimatedRowHeight = 100
            tableViewPackage.rowHeight = UITableView.automaticDimension
        }
    }
    
    @IBOutlet weak var lblMsg : UILabel!

    @IBOutlet weak var lblPackageTitle : UILabel!
    @IBOutlet weak var lblPackageDetail : UILabel!
    @IBOutlet weak var lblPackageAmount : UILabel!
    @IBOutlet weak var lblPackageCreditAmount : UILabel!
    @IBOutlet weak var lblPackageTransactionAmount : UILabel!
    @IBOutlet weak var lblPackageTotalAmount : UILabel!
    
    @IBOutlet weak var viewPurchaseDetail : UIView!
    
    @IBOutlet weak var constraintPackageListHeightConstant:NSLayoutConstraint!
    @IBOutlet weak var constraintVoucherListHeightConstant:NSLayoutConstraint!
    
    fileprivate var purchaseRefundView:PurchaseRefundView?
    fileprivate var logoutViewController:PMLogoutView?
    fileprivate var serviceDetailView:ServiceDetailView?
    
    
    //MARK:- Var(s)
    var presenter = PurchaseDetailPresenter()
    var purchaseDetailResponse: PurchaseDetailResponse.PurchaseDetailDataResponse?
    var purchaseId = ""
    var servicePurchaseId = ""
    
    var serviceName = ""
    var serviceType = ""
    
    var isFromPaymentPage: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)
        self.viewPurchaseDetail.isHidden = true
        
        self.serverPurchaseDetail()
    }

    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.tableViewVoucher.contentSize.height > 0){
            tableViewVoucher.scrollToBottom()
            self.constraintVoucherListHeightConstant?.constant =
                self.tableViewVoucher.contentSize.height
            
            self.updateTableViewHeight(tableView:self.tableViewVoucher)
        }
        
        if(self.tableViewPackage.contentSize.height > 0){
            self.constraintPackageListHeightConstant?.constant = self.tableViewPackage.contentSize.height
            
            self.updateTableViewHeight(tableView:self.tableViewPackage)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView:UITableView) {
    
        // Location
        if(tableView == self.tableViewVoucher){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewVoucher.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewVoucher.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintVoucherListHeightConstant.constant = heightOfTableView
            }
        }
        
        if(tableView == self.tableViewPackage){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewPackage.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewPackage.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintPackageListHeightConstant.constant = heightOfTableView
            }
        }
    }
    
    override func actionNavigationBack(_ sender: Any) {
        if isFromPaymentPage {
            navigationController?.popToRootViewController(animated: true)
        } else {
            super.actionNavigationBack(sender)
        }
    }
}

extension PurchaseDetailView{
    
    func serverPurchaseDetail(){
        
        let purchaseDetailView = PurchaseDetailRequest()
        purchaseDetailView.purchase_id = purchaseId
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            purchaseDetailView.type = "2"
            break
        case AppUser.LocalBusiness.rawValue:
            purchaseDetailView.type = "1"
            break
        case AppUser.Enthusiast.rawValue:
            purchaseDetailView.type = "1"
        default:
            break
        }
        
        Spinner.show()
        self.presenter.serverPurchaseDetail(purchaseDetailRequest: purchaseDetailView)
    }
    
    func updateScrInfo(){
        
        if(self.purchaseDetailResponse != nil){
            
            self.lblPackageTitle.text = self.serviceName
            self.lblPackageDetail.text = self.serviceType
            
            // TODO: Change to total amount of services if require
            self.lblPackageAmount.text = "$ \(self.purchaseDetailResponse?.packageTotal ?? "")"
            lblPackageCreditAmount.text = Double(purchaseDetailResponse?.creditamount ?? "0")?.formattedPrice

            self.lblPackageTransactionAmount.text = "$ \(self.purchaseDetailResponse?.processFees ?? "")"
            self.lblPackageTotalAmount.text = "$ \(self.purchaseDetailResponse?.totalamount ?? "")"
            
            self.tableViewVoucher.reloadData { [weak self] in
                self?.tableViewVoucher.beginUpdates()
                self?.tableViewVoucher.endUpdates()
            }
            self.tableViewPackage.reloadData { [weak self] in
                self?.tableViewPackage.beginUpdates()
                self?.tableViewPackage.endUpdates()
            }
        }
    }
}

extension PurchaseDetailView{
    /**
     *  Buy Now action button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  Open image picker for fetch the image from gallary or camera
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    @IBAction func actionInitiateRefund(_ sender: UIButton) {
        if(self.purchaseRefundView == nil){
            self.purchaseRefundView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PurchaseStoryboard, viewControllerName: PurchaseRefundView.nameOfClass) as PurchaseRefundView
            self.navigationController?.pushViewController(self.purchaseRefundView!, animated: true)
        }
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PurchaseDetailView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.tableViewPackage){
//            return 3
            return self.purchaseDetailResponse?.service != nil ? (self.purchaseDetailResponse?.service?.count)! : 0
        } else if(tableView == self.tableViewVoucher){
//            return 4
            return self.purchaseDetailResponse?.voucher != nil ? (self.purchaseDetailResponse?.voucher?.count)! : 0
        }
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewPackage {
            self.updateViewConstraints()
            let cell        =       tableView.dequeueReusableCell(withIdentifier: PurchaseDetailListCell.nameOfClass, for: indexPath) as! PurchaseDetailListCell
            
            cell.delegate = self
            cell.cellIndex = indexPath.row
            
            if let obj = self.purchaseDetailResponse?.service?[indexPath.row] {
                if let img = obj.service_img {
                    cell.imgProfilePic.sd_setImage(with: URL(string: img))
                }
                cell.lblTitle.text = obj.service_name
                cell.lblBusiName.text = "By: \(obj.serviceusername ?? "")"
                cell.lblCalendarDate.text = "\(obj.calendar_date ?? "")"
                
                if obj.calendar_date == "" {
                    cell.lblCalendarListing.isHidden = true
                    cell.lblCalendarDate.isHidden = true
                } else {
                    cell.lblCalendarListing.isHidden = false
                    cell.lblCalendarDate.isHidden = false
                }
                
                if let priceStr = obj.price, let price = Double(priceStr) {
                    cell.lblPrice.text = price.formattedPrice
                }
                if let creditStr = obj.creditamount, let credit = Double(creditStr) {
                    cell.lblCreditTitle.isHidden = false
                    cell.lblCredit.isHidden = false
                    cell.lblCredit.text = credit.formattedPrice
                } else {
                    cell.lblCreditTitle.isHidden = true
                    cell.lblCredit.isHidden = true
                }
                
                if let donationAmtStr = obj.purchase_donation_amount, !donationAmtStr.trim().isEmpty, let donationAmt = Double(donationAmtStr), donationAmt > 0 {
                    cell.lblCharitable.text = "Donation Amount"
                    cell.lblCharitableAmt.text = donationAmt.formattedPrice
                    cell.stackViewCharitable.isHidden = false
                }
                
                if obj.purchase_status == "0" {
                    let data = self.purchaseDetailResponse?.service?.filter({ resp in
                        resp.purchase_status == "1" || resp.purchase_status == "2"
                    })
                    if data?.count ?? 0>0{
                        cell.btnRefund.isHidden = true
                    }else{
                        cell.btnRefund.isHidden = false
                    }
                } else if obj.purchase_status == "1" && obj.money_back_status == "1" {
                    cell.btnMoneyBack.isHidden = false
                } else if obj.purchase_status == "2" {
                    cell.btnRefunded.isHidden = false
                } else if obj.purchase_status == "3" {
                    cell.btnRefunded.isHidden = false
                    cell.btnRefunded.setTitle("Money back guarantee pending", for: .normal)
                } else if obj.purchase_status == "4" {
                    cell.btnRefunded.isHidden = false
                    cell.btnRefunded.setTitle("Money back guarantee granted", for: .normal)
                }  else if obj.purchase_status == "6" {
                    cell.btnRefunded.isHidden = false
                    cell.btnRefunded.setTitle("Money back guarantee denied", for: .normal)
                }
            }

            return cell
        } else if tableView == tableViewVoucher {
            self.updateViewConstraints()
            
            let cell = tableView.dequeueReusableCell(withIdentifier: PurchaseVoucherListCell.nameOfClass, for: indexPath) as! PurchaseVoucherListCell
            
            let obj = self.purchaseDetailResponse?.voucher![indexPath.row]
            cell.voucherItem = obj
            cell.lblVerificationNum.text = obj?.voucher_id ?? ""
            cell.lblRedeemingNum.text = obj?.voucher_redeem_no ?? ""
            
            cell.lblServiceName.text = ""
            if let overview = obj?.overview {
                cell.lblServiceName.text = overview
            } else {
                let service = purchaseDetailResponse?.service?.first { $0.service_id == obj?.voucher_service }
                if let serviceName = service?.service_name {
                    cell.lblServiceName.text = serviceName
                }
            }
            
            cell.lblPrice.text = "$ \(obj?.voucher_amount ?? "")"
            cell.lblNote.text = "Date Purchase \(DateTimeUtils.sharedInstance.toLocalDate(obj?.voucher_date ?? "", with: "yyyy-MM-dd HH:mm:ss")?.appSpecificStringFromFormatWithTime() ?? "")"

            if let qrImage = obj?.voucher_qr, let url = URL(string: qrImage) {
                cell.imgVoucher.setImage(url: url)
            } else {
                cell.imgVoucher.image = nil
            }
            
            return cell
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
//        let purchaseRefundView:PMPurchaseRefundView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMPurchaseRefundView.nameOfClass) as PMPurchaseRefundView
//        self.navigationController?.pushViewController(purchaseRefundView, animated: true)
        
        if(tableView == self.tableViewPackage){
            if(self.serviceDetailView == nil){
                self.serviceDetailView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceDetailView.nameOfClass) as ServiceDetailView
                
                self.serviceDetailView?.isHideEditOption = true
                
                let service = self.purchaseDetailResponse?.service![indexPath.row]
                
                self.serviceDetailView?.serviceId = service?.service_id ?? ""
                self.serviceDetailView?.isHideEditOption = true
                self.navigationController?.pushViewController(self.serviceDetailView!, animated: true)
            }
        }
    }
}

extension PurchaseDetailView: PurchaseDetailListCellDelegate{
    func methodRefund(cellIndex: Int) {
        //API Initate refund view
        
        let obj = self.purchaseDetailResponse?.service?[cellIndex]
        
        self.servicePurchaseId = obj?.purchase_id ?? ""
        self.logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
        self.logoutViewController?.delegate = self
        self.logoutViewController?.scrTitle = ""
        self.logoutViewController?.scrDesc = LocalizationKeys.refund_alert_desc.getLocalized()
        self.logoutViewController?.modalPresentationStyle = .overFullScreen
        self.present(logoutViewController!, animated: true, completion: nil)

    }
    
    func methodMoneyBack(cellIndex:Int){
        //TODO: Initate Money back
        if(self.purchaseRefundView == nil){
            self.purchaseRefundView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PurchaseStoryboard, viewControllerName: PurchaseRefundView.nameOfClass) as PurchaseRefundView
            
            let obj = self.purchaseDetailResponse?.service![cellIndex]
            
            self.purchaseRefundView?.servicePurchaseId = obj?.purchase_id ?? ""
            self.navigationController?.pushViewController(self.purchaseRefundView!, animated: true)
        }
    }
}

extension PurchaseDetailView: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
        
        // call Withdraw api
        let refundRequest = RefundRequest()
        refundRequest.purchase_id = servicePurchaseId
        refundRequest.reason = "1"
        refundRequest.comment = ""
        
        self.presenter.serverRefund(refundRequest: refundRequest)
    }
}

/**
 *  ProductCellDelegate protocol class.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol PurchaseDetailListCellDelegate {
    func methodRefund(cellIndex:Int)
    func methodMoneyBack(cellIndex:Int)
}

class PurchaseDetailListCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel! { didSet { lblTitle.text = nil } }
    @IBOutlet weak var lblBusiName: UILabel! { didSet { lblBusiName.text = nil } }
    @IBOutlet weak var lblDesciption: UILabel! { didSet { lblDesciption.text = nil } }
    @IBOutlet weak var lblPrice: UILabel! { didSet { lblPrice.text = nil } }
    @IBOutlet weak var lblCreditTitle: UILabel! { didSet { lblCreditTitle.isHidden = true } }
    @IBOutlet weak var lblCredit: UILabel! { didSet { lblCredit.isHidden = true } }

    @IBOutlet weak var lblCalendarListing: UILabel! { didSet {   } }
    @IBOutlet weak var lblCalendarDate: UILabel! { didSet { lblCalendarDate.text = nil } }

    @IBOutlet weak var lblCharitable: UILabel! { didSet { lblCharitable.text = nil } }
    @IBOutlet weak var lblCharitableAmt: UILabel! { didSet { lblCharitableAmt.text = nil } }
    @IBOutlet weak var imgProfilePic: UIImageView! { didSet { imgProfilePic.image = nil } }
    
    @IBOutlet weak var stackViewCharitable: UIStackView! { didSet { stackViewCharitable.isHidden = true } }
    
    @IBOutlet weak var btnRefund: UIButton! { didSet { btnRefund.isHidden = true } }
    @IBOutlet weak var btnRefunded: UIButton! { didSet { btnRefunded.isHidden = true } }
    @IBOutlet weak var btnMoneyBack: UIButton! { didSet { btnMoneyBack.isHidden = true } }

    @IBOutlet weak var stackAcceptReject: UIStackView! { didSet { stackAcceptReject.isHidden = true } }
    
    var cellIndex:Int!
    var delegate:PurchaseDetailListCellDelegate?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgProfilePic.image = nil
        lblTitle.text = nil
        lblBusiName.text = nil
        lblDesciption.text = nil
        lblPrice.text = 0.0.formattedPrice
        lblCreditTitle.isHidden = true
        lblCredit.isHidden = true
        stackViewCharitable.isHidden = true
        lblCharitableAmt.text = nil
        btnRefund.isHidden = true
        btnRefunded.isHidden = true
        btnMoneyBack.isHidden = true
    }
    /**
     *  Show Redeem event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodRefundAction(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodRefund(cellIndex: self.cellIndex)
        }
    }
    
    /**
     *  Show Money Back action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodMoneyBackAction(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodMoneyBack(cellIndex: self.cellIndex)
        }
    }
    
    @IBAction private func methodAcceptRejectAction(_ sender: UIButton) {
        if sender.tag == 0 {
            print("Accept action pending")
            // accept
        } else if sender.tag == 1 {
            // reject
            print("Reject action pending")
        }
    }
}

class PurchaseVoucherListCell: UITableViewCell {
    @IBOutlet weak var lblVerificationNum: UILabel!
    @IBOutlet weak var lblRedeemingNum: UILabel!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var imgVoucher: UIImageView! { didSet { setupImageView(imgVoucher) } }
    
    var voucherItem: PurchaseServiceVoucherDataResponse?
    
    private func setupImageView(_ imgView: UIImageView) {
        imgView.image = nil
        imgView.isUserInteractionEnabled = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showImageCarousal(gesture:)))
        imgView.addGestureRecognizer(gesture)
    }
    
    @objc private func showImageCarousal(gesture: UITapGestureRecognizer) {
        // Create an array of images.
        var images: [LightboxImage] = []
        if let image = imgVoucher.image {
            images.append(LightboxImage(image: image))
        } else if let pic = voucherItem?.qr_code, let url = URL(string: pic) {
            images.append(LightboxImage(imageURL: url))
        }  else if let pic = voucherItem?.voucher_qr, let url = URL(string: pic) {
            images.append(LightboxImage(imageURL: url))
        }

        let controller = LightboxController(images: images, startIndex: 0)
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        controller.dynamicBackground = true
        HelperConstant.appDelegate.window?.rootViewController?.present(controller, animated: true, completion: nil)
    }
}

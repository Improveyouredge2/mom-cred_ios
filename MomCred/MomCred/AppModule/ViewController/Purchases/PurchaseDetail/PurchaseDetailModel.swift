//
//  PurchaseDetailModel.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper


/**
 *  PurchaseDetailRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PurchaseDetailRequest : Mappable {
    
    var purchase_id: String?
    
//    type - 1 for user
//    type - 2 for service provider
    var type: String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        
        purchase_id <- map["purchase_id"]
        type        <- map["type"]
    }
}

/**
 *  RefundRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */

class RefundRequest : Mappable {
    
    var purchase_id: String?
    var reason: String?
    var comment: String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        
        purchase_id <- map["purchase_id"]
        reason      <- map["reason"]
        comment     <- map["Comment"]
    }
}

/**
 *  PurchaseListResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PurchaseDetailResponse : APIResponse {
    
    var data:PurchaseDetailDataResponse?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> PurchaseDetailResponse?{
        var purchaseDetailResponse:PurchaseDetailResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PurchaseDetailResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    purchaseDetailResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PurchaseDetailResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        purchaseDetailResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return purchaseDetailResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        data         <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class PurchaseDetailDataResponse: Mappable{
        
        var purchase_id:String?
        var customername:String?
        var transactionid:String?
        var date:String?
        var purchase_package:String?
        var totalamount:String?
        var packageTotal:String?
        var processFees:String?
        var creditamount:String?

        var service:[PurchaseServiceDetailDataResponse]?
        var voucher:[PurchaseServiceVoucherDataResponse]?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PurchaseDetailDataResponse?{
            var purchaseDetailDataResponse:PurchaseDetailDataResponse?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<PurchaseDetailDataResponse>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        purchaseDetailDataResponse = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<PurchaseDetailDataResponse>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            purchaseDetailDataResponse = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return purchaseDetailDataResponse ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            purchase_id         <- map["purchase_id"]
            customername        <- map["customername"]
            transactionid       <- map["transactionid"]
            date                <- map["date"]
            purchase_package    <- map["purchase_package"]
            totalamount         <- map["totalamount"]
            service             <- map["service"]
            voucher             <- map["voucher"]
            
            packageTotal        <- map["serviceamounttotal"]
            processFees         <- map["stripecharge"]
            creditamount        <- map["creditamount"]

        }
    }
}

/**
 *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class PurchaseServiceDetailDataResponse: Mappable{
    
    var service_id:String?
    var service_name:String?
    var purchase_donation:String?
    var purchase_donation_amount:String?
    var serviceusername:String?
    var price:String?
    var creditamount:String?
    var service_img:String?
    var calendar_date:String?
    
    //Money back Gurantee
    var money_back_status:String?


    //0 for Refund, 1 for Request for Money back Gurantee, 2 for refunded, 3 for money back, 4 for money back accept
    var purchase_status:String?
    var purchase_id:String?
    
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init(){
        
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PurchaseServiceDetailDataResponse?{
        var purchaseServiceDetailDataResponse:PurchaseServiceDetailDataResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PurchaseServiceDetailDataResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    purchaseServiceDetailDataResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PurchaseServiceDetailDataResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        purchaseServiceDetailDataResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return purchaseServiceDetailDataResponse ?? nil
    }
    
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map){
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map){
        purchase_id                 <- map["purchase_id"]
        service_id                  <- map["service_id"]
        service_name                <- map["service_name"]
        purchase_donation           <- map["purchase_donation"]
        purchase_donation_amount    <- map["purchase_donation_amount"]
        serviceusername             <- map["serviceusername"]
        price                       <- map["price"]
        if let creditAmt: String = map["creditamount"].value() {
            creditamount = creditAmt
        } else if let creditAmt: Double = map["creditamount"].value() {
            creditamount = String(creditAmt)
        }
        service_img                 <- map["service_img"]
        purchase_status             <- map["purchase_status"]
        money_back_status             <- map["money_back_status"]
        
        calendar_date             <- map["calendar_date"]
    }
}

/**
 *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class PurchaseServiceVoucherDataResponse: Mappable{
    var voucher_id: String?
    var voucher_purchase_id: String?
    var voucher_name: String?
    var voucher_no: String?
    var voucher_redeem_no: String?
    var voucher_amount: String?
    var voucher_min_amount: String?
    var voucher_donation: String?
    var voucher_date: String?
    var voucher_service: String?
    var voucher_pay_id: String?
    var voucher_user: String?
    var voucher_status: String?
    var voucher_qr: String?
    var qr_code: String?
    
    var service_pic: String?
    var service_type: String?
    var overview: String?
    var service_field: [BusinessFieldServiceCategoryInfo]?
    var calendar_date: String?
    var service_user_name: String?

    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init(){
        
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PurchaseServiceVoucherDataResponse?{
        var purchaseServiceVoucherDataResponse:PurchaseServiceVoucherDataResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PurchaseServiceVoucherDataResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    purchaseServiceVoucherDataResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PurchaseServiceVoucherDataResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        purchaseServiceVoucherDataResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return purchaseServiceVoucherDataResponse ?? nil
    }
    
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map){
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map){
        voucher_id          <- map["voucher_id"]
        voucher_purchase_id <- map["voucher_purchase_id"]
        voucher_name        <- map["voucher_name"]
        voucher_no          <- map["voucher_no"]
        voucher_redeem_no   <- map["voucher_redeem_no"]
        voucher_amount      <- map["voucher_amount"]
        voucher_min_amount  <- map["voucher_min_amount"]
        voucher_donation    <- map["voucher_donation"]
        voucher_date        <- map["voucher_date"]
        voucher_service     <- map["voucher_service"]
        voucher_pay_id      <- map["voucher_pay_id"]
        voucher_user        <- map["voucher_user"]
        voucher_status      <- map["voucher_status"]
        voucher_qr          <- map["voucher_qr"]
        qr_code             <- map["qr_code"]
        
        service_pic         <- map["service_pic"]
        overview            <- map["overview"]
        service_field       <- map["service_field"]
        service_type        <- map["service_type"]
        calendar_date       <- map["calendar_date"]
        service_user_name       <- map["service_user_name"]

        
    }
}

//
//  PurchaseListPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  PurchaseListPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PurchaseListPresenter {
    
    var view:PurchaseListViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: PurchaseListViewController) {
        self.view = view
    }
}

extension PurchaseListPresenter{
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverPurchaseListing(purchaseListRequest:PurchaseListRequest?, callback:@escaping (_ status:Bool, _ response: PurchaseListResponse?, _ message: String?) -> Void){
        
        PurchaseListService.serverPurchaseListing(purchaseListRequest: purchaseListRequest) { (status, response, message) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if status {
                    self?.view.purchaseListResponse = response
                    callback(status, response, message)
                } else {
                    if self?.view.currentPageIndex == 0 {
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    }
                    callback(status, nil, message)
                }
            }
        }
    }
    
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverPurchaseListingFilter(purchaseListFilterRequest:PurchaseListFilterRequest?, callback:@escaping (_ status:Bool, _ response: PurchaseListFilterResponse?, _ message: String?) -> Void){
        
        PurchaseListService.serverPurchaseListingFilter(purchaseListFilterRequest: purchaseListFilterRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, nil, message)
                }
            }
        })
    }
}

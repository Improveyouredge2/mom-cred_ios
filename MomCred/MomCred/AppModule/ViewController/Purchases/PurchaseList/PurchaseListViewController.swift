//
//  PurchaseListViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class PurchaseListViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet private weak var navigationBar: NavigationBarView! {
        didSet {
            navigationBar.navigationController = navigationController
        }
    }
    
    @IBOutlet private weak var lblTotalAmount: UILabel! { didSet { lblTotalAmount.text = "" } }
    @IBOutlet private weak var lblTotalCredit: UILabel! { didSet { lblTotalCredit.text = "" } }
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblReviewCount : UILabel!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    
    var currentPageIndex = 0
    
    //MARK:- Var(s)
    fileprivate var presenter = PurchaseListPresenter()
    fileprivate var isDisplayLoader = true
    
    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var purchaseDetailView:PurchaseDetailView?
    
    var purchaseList: [PurchaseListResponse.PurchaseDataResponse]? {
        didSet {
            if tableView != nil {
                tableView.reloadData()
            }
        }
    }
    var purchaseListResponse: PurchaseListResponse? {
        didSet {
            if currentPageIndex == 0 {
                purchaseList = purchaseListResponse?.data
            } else if let data = purchaseListResponse?.data {
                purchaseList?.append(contentsOf: data)
            }

            lblTotalAmount.attributedText = amountAttributedString(title: "Total Amount\n", amount: purchaseListResponse?.totalServiceAmount)
            lblTotalCredit.attributedText = amountAttributedString(title: "Total Credit\n", amount: purchaseListResponse?.totalCreditAmount)
        }
    }
    
    var purchaseListRequest:PurchaseListRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnReset.isHidden = true
        
        // Add refresh loader for pulling and refresh
        refreshView.delegate = self
        tableView.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        
        self.presenter.connectView(view: self)
        
//        self.serverRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.purchaseDetailView = nil
        
        if(purchaseList == nil || (purchaseList != nil && (purchaseList?.count)! == 0)){
            Spinner.show()
            self.serverRequest()
        }
    }
    
    private func amountAttributedString(title: String, amount: String?) -> NSAttributedString {
        let magentaColor = UIColor(hex: "C8057A")
        let attributedString = NSMutableAttributedString(string: title, attributes: [.foregroundColor: UIColor.white])
        var amountValue = "$0.00"
        if let amount = amount, !amount.trim().isEmpty {
            amountValue = amount
        }
        attributedString.append(NSAttributedString(string: amountValue, attributes: [.foregroundColor: magentaColor]))
        return attributedString
    }
}

extension PurchaseListViewController{
    
    fileprivate func getPurchaseListRequest() -> PurchaseListRequest{
        
        if(self.btnReset.isHidden){
            let purchaseListRequest = PurchaseListRequest()
            purchaseListRequest.page_id = "\(currentPageIndex)"
//            purchaseListRequest.purchase_status = "0"

            self.purchaseListRequest = purchaseListRequest
            
            let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
            
            switch  role_id {
            case AppUser.ServiceProvider.rawValue:
                purchaseListRequest.type = "2"
                break
            case AppUser.LocalBusiness.rawValue:
                purchaseListRequest.type = "1"
                break
            case AppUser.Enthusiast.rawValue:
                purchaseListRequest.type = "1"
            default:
                break
            }
        } else if(self.purchaseListRequest != nil){
            self.purchaseListRequest?.page_id = "\(currentPageIndex)"
        }
        
        return self.purchaseListRequest ?? PurchaseListRequest()
    }
    
    fileprivate func serverRequest(){
        let purchaseListRequest = self.getPurchaseListRequest()
        self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest,callback: {
            (status, response, message) in
        })
    }
    
    fileprivate func callbackListing(_ dateList:[String]?, _ tagIndexList:[String]?) -> Void?{
        
        var startDate = ""
        var endDate = ""
        if(dateList != nil && (dateList?.count)! > 0 ){
            
            startDate = dateList?.first ?? ""
            if((dateList?.count)! > 1){
                endDate = dateList?.last ?? ""
            }
        }
        
        var tagListText = ""
        if(tagIndexList != nil && (tagIndexList?.count)! > 0){
            tagListText = tagIndexList?.joined(separator: ",") ?? ""
        }
        
        self.currentPageIndex = 0
        let purchaseListRequest = self.getPurchaseListRequest()
        purchaseListRequest.from = startDate
        purchaseListRequest.to = endDate
        purchaseListRequest.filter = tagListText
        self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
            (status, response, message) in
            
            if(response != nil){
                self.btnFilter.isHidden = true
                self.btnReset.isHidden = false
            }
            
        })
        
        return nil
    }
}


extension PurchaseListViewController: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case .pulling:
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                let purchaseListRequest = self.getPurchaseListRequest()
                self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            let purchaseListRequest = self.getPurchaseListRequest()
            self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}

extension PurchaseListViewController{
    
    @IBAction func resetFilter(_ sender: UIButton){
        self.currentPageIndex = 0
        self.serverRequest()
        self.btnFilter.isHidden = false
        self.btnReset.isHidden = true
    }
    
    @IBAction func actionFilter(_ sender: UIButton) {
        let searchVC: PurchaseSearchListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PurchaseStoryboard, viewControllerName: PurchaseSearchListViewController.nameOfClass)
        navigationController?.pushViewController(searchVC, animated: true)
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PurchaseListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.arrNoteList.count
        return purchaseList != nil ? (purchaseList?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let purchase = purchaseList?[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: PurchaseListCell.nameOfClass, for: indexPath) as! PurchaseListCell
        
        cell.lblTitle.text = purchase?.customername ?? ""
        cell.lblSubTitle.text = ""
        
        if(purchase?.purchase_package != nil){
            let packageArray = purchase?.purchase_package?.split(separator: ",")
            
            if(packageArray != nil && (packageArray?.count)! > 1){
                
                let serviceCount = packageArray![0]
                let profileCount = packageArray![1]
                
                if Int(profileCount) == 1 {
                    cell.lblSubTitle.text = "\(serviceCount) from \(profileCount) profile"
                } else {
                    
                    cell.lblSubTitle.text = "\(serviceCount) from \(profileCount) profile each"
                }
                
                cell.lblTitle.text = LocalizationKeys.package_bought_text.getLocalized()
            } else {
                cell.lblTitle.text = purchase?.service_name ?? ""
                cell.lblSubTitle.text = ""
            }
        } else {
            cell.lblTitle.text = purchase?.service_name ?? ""
            cell.lblSubTitle.text = ""
        }
        
        if let purchase_package = purchase?.purchase_package, purchase_package == "0,0" {
            cell.lblTitle.text = purchase?.serviceusername ?? ""
            cell.lblSubTitle.text = purchase?.service_name ?? ""
        }
        
        cell.lblTranscationNum.text = purchase?.transactionid ?? ""
        cell.lblTranscationDate.text = purchase?.date ?? ""
        cell.lblCalendarDate.text = purchase?.calendar_date ?? ""
        cell.lblAmount.text = "$ \(purchase?.totalamount ?? "")"
        
        if purchase?.calendar_date == "" {
            cell.lblCalendarListing.isHidden = true
            cell.lblCalendarDate.isHidden = true
        } else {
            cell.lblCalendarListing.isHidden = false
            cell.lblCalendarDate.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if purchaseDetailView == nil {
            purchaseDetailView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PurchaseStoryboard, viewControllerName: PurchaseDetailView.nameOfClass) as PurchaseDetailView
            
            let purchase = purchaseList?[indexPath.row]
            var serviceName = purchase?.service_name ?? ""
            var serviceType = ""
            if let package = purchase?.purchase_package {
                let packageArray = package.split(separator: ",")
                if let serviceCount = packageArray.first, let profileCount = packageArray.last {
                    if serviceCount.trimmingCharacters(in: .whitespaces) != "0" || profileCount.trimmingCharacters(in: .whitespaces) != "0" {
                        if profileCount == "1" {
                            serviceType = "\(serviceCount) from \(profileCount) profile"
                        } else {
                            serviceType = "\(serviceCount) from \(profileCount) profile each"
                        }
                        serviceName = LocalizationKeys.package_bought_text.getLocalized()
                    }
                }
                
                purchaseDetailView?.serviceName = serviceName
                purchaseDetailView?.serviceType = serviceType
                purchaseDetailView?.purchaseId = purchase?.purchase_id ?? ""
                
                navigationController?.pushViewController(purchaseDetailView!, animated: true)
            }
        }
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        //        return UITableView.automaticDimension
//        return 140
//    }
}

class PurchaseListCell: UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblTranscationNum : UILabel!
    @IBOutlet weak var lblTranscationDate : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblCalendarDate : UILabel!
    @IBOutlet weak var lblCalendarListing : UILabel!

}

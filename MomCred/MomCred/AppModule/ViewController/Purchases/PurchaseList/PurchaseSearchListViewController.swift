//
//  PurchaseSearchListViewController.swift
//  MomCred
//
//  Created by MD on 28/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation

class PurchaseSearchListViewController: LMBaseViewController {
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.barTintColor = UIColor.clear
            searchBar.backgroundColor = UIColor.clear
            searchBar.isTranslucent = true
            searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
            searchBar.delegate = self
            
            if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField {
                searchTextField.textColor = UIColor.white
                searchTextField.attributedPlaceholder =  NSAttributedString(string: "Service Name", attributes: [.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
            }
        }
    }
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView: UITableView!
        
    var currentPageIndex = 0
    
    //MARK:- Var(s)
    fileprivate var presenter = PurchaseSearchListPresenter()
    fileprivate var isDisplayLoader = true
    
    //MARK:- fileprivate
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var purchaseDetailView:PurchaseDetailView?
    
    var purchaseList: [PurchaseListResponse.PurchaseDataResponse]? {
        didSet {
            if tableView != nil {
                tableView.reloadData()
            }
        }
    }
    var purchaseListResponse: PurchaseListResponse? {
        didSet {
            if currentPageIndex == 0 {
                purchaseList = purchaseListResponse?.data
            } else if let data = purchaseListResponse?.data {
                purchaseList?.append(contentsOf: data)
            }
        }
    }
    
    var purchaseListRequest: PurchaseSearchListRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        // Add refresh loader for pulling and refresh
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        
        self.presenter.connectView(view: self)
    }
    
    private func amountAttributedString(title: String, amount: String?) -> NSAttributedString {
        let magentaColor = UIColor(hex: "C8057A")
        let attributedString = NSMutableAttributedString(string: title, attributes: [.foregroundColor: UIColor.white])
        var amountValue = "$0.00"
        if let amount = amount, !amount.trim().isEmpty {
            amountValue = amount
        }
        attributedString.append(NSAttributedString(string: amountValue, attributes: [.foregroundColor: magentaColor]))
        return attributedString
    }
}

extension PurchaseSearchListViewController{
    
    fileprivate func getPurchaseListRequest() -> PurchaseSearchListRequest {
        
        if purchaseListRequest ==  nil {
            let request = PurchaseSearchListRequest()
            request.page_id = "\(currentPageIndex)"
            
            let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
            
            switch  role_id {
            case AppUser.ServiceProvider.rawValue:
                request.type = "2"
            case AppUser.LocalBusiness.rawValue:
                request.type = "1"
            case AppUser.Enthusiast.rawValue:
                request.type = "1"
            default:
                break
            }
            request.search = searchBar.text
            purchaseListRequest = request
        } else {
            purchaseListRequest?.page_id = "\(currentPageIndex)"
        }
        
        return purchaseListRequest ?? PurchaseSearchListRequest()
    }
    
    fileprivate func callbackListing(_ dateList:[String]?, _ tagIndexList:[String]?) -> Void?{
        
        var startDate = ""
        var endDate = ""
        if(dateList != nil && (dateList?.count)! > 0 ){
            
            startDate = dateList?.first ?? ""
            if((dateList?.count)! > 1){
                endDate = dateList?.last ?? ""
            }
        }
        
        var tagListText = ""
        if(tagIndexList != nil && (tagIndexList?.count)! > 0){
            tagListText = tagIndexList?.joined(separator: ",") ?? ""
        }
        
        self.currentPageIndex = 0
        let purchaseListRequest = self.getPurchaseListRequest()
        purchaseListRequest.from = startDate
        purchaseListRequest.to = endDate
        purchaseListRequest.filter = tagListText
        self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
            (status, response, message) in
        })
        
        return nil
    }
}


extension PurchaseSearchListViewController: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case .pulling:
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                let purchaseListRequest = self.getPurchaseListRequest()
                self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            let purchaseListRequest = self.getPurchaseListRequest()
            self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}

extension PurchaseSearchListViewController{
    
    @IBAction func resetFilter(_ sender: UIButton){
        self.currentPageIndex = 0
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension PurchaseSearchListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.arrNoteList.count
        return purchaseList != nil ? (purchaseList?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let purchase = purchaseList?[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: PurchaseListCell.nameOfClass, for: indexPath) as! PurchaseListCell
        
        cell.lblTitle.text = purchase?.customername ?? ""
        cell.lblSubTitle.text = ""
        
        if(purchase?.purchase_package != nil){
            let packageArray = purchase?.purchase_package?.split(separator: ",")
            
            if(packageArray != nil && (packageArray?.count)! > 1){
                
                let serviceCount = packageArray![0]
                let profileCount = packageArray![1]
                
                if Int(profileCount) == 1 {
                    cell.lblSubTitle.text = "\(serviceCount) from \(profileCount) profile"
                } else {
                    
                    cell.lblSubTitle.text = "\(serviceCount) from \(profileCount) profile each"
                }
                
                cell.lblTitle.text = LocalizationKeys.package_bought_text.getLocalized()
            } else {
                cell.lblTitle.text = purchase?.service_name ?? ""
                cell.lblSubTitle.text = ""
            }
        } else {
            cell.lblTitle.text = purchase?.service_name ?? ""
            cell.lblSubTitle.text = ""
        }
        
        cell.lblTranscationNum.text = purchase?.transactionid ?? ""
        cell.lblTranscationDate.text = purchase?.date ?? ""
        cell.lblAmount.text = "$ \(purchase?.totalamount ?? "")"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if purchaseDetailView == nil {
            purchaseDetailView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PurchaseStoryboard, viewControllerName: PurchaseDetailView.nameOfClass) as PurchaseDetailView
            
            let purchase = purchaseList?[indexPath.row]
            var serviceName = purchase?.service_name ?? ""
            var serviceType = ""
            if let package = purchase?.purchase_package {
                let packageArray = package.split(separator: ",")
                if let serviceCount = packageArray.first, let profileCount = packageArray.last {
                    if serviceCount.trimmingCharacters(in: .whitespaces) != "0" || profileCount.trimmingCharacters(in: .whitespaces) != "0" {
                        if profileCount == "1" {
                            serviceType = "\(serviceCount) from \(profileCount) profile"
                        } else {
                            serviceType = "\(serviceCount) from \(profileCount) profile each"
                        }
                        serviceName = LocalizationKeys.package_bought_text.getLocalized()
                    }
                }
                
                purchaseDetailView?.serviceName = serviceName
                purchaseDetailView?.serviceType = serviceType
                purchaseDetailView?.purchaseId = purchase?.purchase_id ?? ""
                
                navigationController?.pushViewController(purchaseDetailView!, animated: true)
            }
        }
    }
}

extension PurchaseSearchListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload(_:)), object: searchBar)
        perform(#selector(self.reload(_:)), with: searchBar, afterDelay: 0.75)
    }

    @objc func reload(_ searchBar: UISearchBar) {
        purchaseListRequest = nil
        purchaseList = nil
        currentPageIndex = 0

        guard let query = searchBar.text, query.trimmingCharacters(in: .whitespaces) != "" else {
            print("nothing to search")
            tableView.reloadData()
            return
        }

        print(query)
        let request = getPurchaseListRequest()
        presenter.serverPurchaseListing(purchaseListRequest: request, callback: {
            (status, response, message) in
        })
    }
}

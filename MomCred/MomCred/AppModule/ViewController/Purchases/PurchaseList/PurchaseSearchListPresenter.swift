//
//  PurchaseSearchListPresenter.swift
//  MomCred
//
//  Created by MD on 28/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import Foundation

class PurchaseSearchListPresenter {
    
    weak var view: PurchaseSearchListViewController! // Object of account view screen

    func connectView(view: PurchaseSearchListViewController) {
        self.view = view
    }
}

extension PurchaseSearchListPresenter {
    func serverPurchaseListing(purchaseListRequest: PurchaseSearchListRequest?, callback:@escaping (_ status:Bool, _ response: PurchaseListResponse?, _ message: String?) -> Void){
        
        PurchaseListService.serverPurchaseListing(purchaseListRequest: purchaseListRequest) { (status, response, message) in
            DispatchQueue.main.async { [weak self] in
                Spinner.hide()
                if status {
                    self?.view.purchaseListResponse = response
                    callback(status, response, message)
                } else {
                    if self?.view.currentPageIndex == 0 {
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    }
                    callback(status, nil, message)
                }
            }
        }
    }
}

//
//  PurchaseListModel.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  PurchaseListRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */

class SearchPurchaseListRequest: PurchaseListRequest {
    var search: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        search  <- map["search"]
    }
}

class PurchaseSearchListRequest : PurchaseListRequest {
    var search: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        search <- map["search"]
    }
}

class PurchaseListRequest : Mappable {
    
    var page_id: String?
    
//    type - 1 for user
//    type - 2 for service provider
    var type: String?
    
    var from: String?
    var to: String?
    var filter: String?
    
    var purchase_status:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        
        page_id     <- map["page_id"]
        type        <- map["type"]
        
        from    <- map["from"]
        to      <- map["to"]
        filter  <- map["filter"]
        purchase_status <- map["purchase_status"]
    }
}

/**
 *  PurchaseListRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PurchaseListFilterRequest : Mappable {
    
//    type - 1 for user
//    type - 2 for service provider
    var type: String?
    
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        
        type        <- map["type"]
    }
}

/**
 *  PurchaseListResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PurchaseListResponse : APIResponse {
    var totalCreditAmount: String?
    var totalServiceAmount: String?
    var data:[PurchaseDataResponse]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> PurchaseListResponse?{
        var purchaseListResponse:PurchaseListResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PurchaseListResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    purchaseListResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PurchaseListResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        purchaseListResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return purchaseListResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        if let creditAmt: String = map["totalCreditAmount"].value() {
            totalCreditAmount = creditAmt
        } else if let creditAmt: Double = map["total_credit"].value() {
            totalCreditAmount = String(creditAmt)
        }
        totalServiceAmount <- map["totalServiceAmount"]
        data <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class PurchaseDataResponse: Mappable{
        
        var customername:String?
        var transactionid:String?
        var date:String?
        var calendar_date:String?
        var purchase_id:String?
        var purchase_package:String?
        var purchase_donation:String?
        var purchase_donation_amount:String?
        var totalamount:String?
        var service_name:String?
        var serviceusername:String?
        var purchaseData:PurchaseData?
        var voucher_status:String?
        var verification_number: String?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PurchaseDataResponse?{
            var purchaseDataResponse:PurchaseDataResponse?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<PurchaseDataResponse>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        purchaseDataResponse = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<PurchaseDataResponse>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            purchaseDataResponse = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return purchaseDataResponse ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            purchase_id                 <- map["purchase_id"]
            customername                <- map["customername"]
            transactionid               <- map["transactionid"]
            date                        <- map["date"]
            purchase_package            <- map["purchase_package"]
            purchase_donation           <- map["purchase_donation"]
            purchase_donation_amount    <- map["purchase_donation_amount"]
            totalamount                 <- map["totalamount"]
            service_name                <- map["service_name"]
            serviceusername             <- map["serviceusername"]
            purchaseData                <- map["purchase_data"]
            
            voucher_status              <- map["voucher_status"]
            verification_number         <- map["verification_number"]
            calendar_date         <- map["calendar_date"]

        }
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class PurchaseData: Mappable{
        
        var creditloyalty:NSNumber?
        var service_id:String?
        var service_user:String?
        var serviceamount:NSNumber?
        var stripefees:NSNumber?
        var withstripefees:NSNumber?
        
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PurchaseData?{
            var purchaseData:PurchaseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<PurchaseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        purchaseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<PurchaseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            purchaseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return purchaseData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            creditloyalty   <- map["creditloyalty"]
            service_id      <- map["service_id"]
            service_user    <- map["service_user"]
            serviceamount   <- map["serviceamount"]
            stripefees      <- map["stripefees"]
            withstripefees  <- map["withstripefees"]
        }
    }
}


/**
 *  PurchaseListResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PurchaseListFilterResponse : APIResponse {
    
    var data:[PurchaseFilterDataResponse]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> PurchaseListFilterResponse?{
        var purchaseListFilterResponse:PurchaseListFilterResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<PurchaseListFilterResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    purchaseListFilterResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<PurchaseListFilterResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        purchaseListFilterResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return purchaseListFilterResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        data         <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class PurchaseFilterDataResponse: Mappable{
        
        var servicename:String?
        var service_id:String?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->PurchaseFilterDataResponse?{
            var purchaseFilterDataResponse:PurchaseFilterDataResponse?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<PurchaseFilterDataResponse>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        purchaseFilterDataResponse = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<PurchaseFilterDataResponse>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            purchaseFilterDataResponse = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return purchaseFilterDataResponse ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            servicename <- map["servicename"]
            service_id  <- map["service_id"]
            
        }
    }
}

//
//  RedeemAlertViewController.swift
//  MomCred
//
//  Copyright © 2019 consagous. All rights reserved.
//

import Foundation
import UIKit

protocol RedeemAlertViewControllerDelegate {
    func cancelViewScr()
    func acceptViewScr(code:String?,cellIndex:Int)
}

class RedeemAlertViewController: LMBaseViewController {
    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var titleHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var buttonError: UIButton!
    @IBOutlet weak var inputRedeemCode: RYFloatingInput!
    @IBOutlet weak var btnScanner: UIButton!
    
    var scrTitle:String?
    var scrDesc:String?
    var cellIndex:Int!
    var delegate:RedeemAlertViewControllerDelegate?
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        super.viewDidLoad()
        
        if let scrTitle = scrTitle {
            lblTitle.text = scrTitle
        }

        inputRedeemCode.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .warningColor(.white)
                .placeholer(LocalizationKeys.title_redeem_code.getLocalized())
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .build()
        )
    }
    
    @IBAction private func btnScanAction(sender: UIButton) {
        let scanner = QRScannerViewController()
        scanner.delegate = self
        present(scanner, animated: true, completion: nil)
    }
}

extension RedeemAlertViewController {
    
    /**
     *  Close button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if(self.delegate != nil){
                self.delegate?.cancelViewScr()
            }
        })
    }
    
    /**
     *  Cancel button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if(self.delegate != nil){
                self.delegate?.cancelViewScr()
            }
        })
    }
    
    /**
     *  Ok button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodOkAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            if(self.delegate != nil){
                self.delegate?.acceptViewScr(code: self.inputRedeemCode.text() ?? "", cellIndex:self.cellIndex)
            }
        })
    }
}

extension RedeemAlertViewController: QRScannerViewControllerDelegate {
    func scannerViewController(_ controller: QRScannerViewController, didScanCode code: String) {
        inputRedeemCode.input.text = code
        dismiss(animated: true, completion: nil)
    }
    
    func scannerViewControllerDidFail(_ controller: QRScannerViewController) {
        dismiss(animated: true, completion: nil)
    }
}

//
//  SearchBookingPresenter.swift
//  MomCred
//
//  Created by MD on 29/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class SearchBookingPresenter {
    weak var view: SearchBookingViewController!

    func connectView(view: SearchBookingViewController) {
        self.view = view
    }
}

extension SearchBookingPresenter{
    func serverPurchaseListing(purchaseListRequest: PurchaseSearchListRequest?, callback:@escaping (_ status:Bool, _ response: PurchaseListResponse?, _ message: String?) -> Void){
        
        EarningsService.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    if(response != nil && response?.data != nil && (response?.data?.count)! > 0){
                        if(self.view.currentPageIndex == 0){
                            self.view.purchaseList = []
                            self.view.purchaseList = response?.data
                        } else {
                            self.view.purchaseList?.append(contentsOf: response?.data ?? [])
                        }
                    }
                    
                    self.view.tableView.reloadData()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                   
                    if(self.view.currentPageIndex == 0){
                        Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    }
                    
                    callback(status, nil, message)
                }
            }
        })
    }
    
    func updateRedeemStatus(redeemCodeRequest:RedeemCodeRequest?, callback:@escaping (_ status:Bool, _ response: APIResponse?, _ message: String?) -> Void){
        
        BookingService.serverRedeemCode(redeemCodeRequest: redeemCodeRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, nil, message)
                }
            }
        })
    }
}

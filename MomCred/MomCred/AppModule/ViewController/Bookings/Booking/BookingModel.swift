//
//  BookingViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  RedeemCodeRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class RedeemCodeRequest : Mappable {
    
    var code: String?
    var purchase_id: String?
    var auth_token: String?
    var user_id: String?

    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    init(code:String?, auth_token: String?, user_id: String?) {
        self.code = code
        self.auth_token = auth_token
        self.user_id = user_id
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        
        code        <- map["code"]
        purchase_id <- map["purchase_id"]
        auth_token  <- map["auth_token"]
        user_id     <- map["user_id"]
    }
}

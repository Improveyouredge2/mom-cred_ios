//
//  BookingViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

enum BookingTabbar: Int, CustomStringConvertible {
    case AllBooking
    case Redeem
    case Refund
    case MoneyBackRequested
    case MoneyBackAccept
    case MoneyBackReject
    
    var description: String {
        switch self {
        case .AllBooking:
            return "ALL BOOKINGS"
        case .Redeem:
            return "REDEEMED"
        case .Refund:
            return "REFUNDED"
        case .MoneyBackRequested:
            return "MONEY BACK REQUESTED"
        case .MoneyBackAccept:
            return "MONEY BACK ACCEPTED"
        case .MoneyBackReject:
            return "MONEY BACK REJECTED"
        }
    }
}

/**
 * BookingViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BookingViewController: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var lblMyTransactions: UILabel!
    @IBOutlet weak var lblTotalBookingAmount: UILabel!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnFilter: UIButton! { didSet { btnFilter.isHidden = true } }

    override func viewDidLoad() {
        self.btnReset.isHidden = true
        self.setupScreenData()
    }
    
    func setupScreenData(){
        
        settings.style.buttonBarBackgroundColor = #colorLiteral(red: 0.2823529412, green: 0.3137254902, blue: 0.3490196078, alpha: 1)
        settings.style.buttonBarItemBackgroundColor = #colorLiteral(red: 0.2823529412, green: 0.3137254902, blue: 0.3490196078, alpha: 1)
        settings.style.selectedBarBackgroundColor = #colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1)
        settings.style.selectedBarHeight = 03
        settings.style.buttonBarMinimumLineSpacing = 05
        settings.style.buttonBarItemTitleColor = UIColor.white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        settings.style.buttonBarItemLeftRightMargin = 05
        settings.style.buttonBarItemFont = FontsConfig.FontHelper.defaultRegularFontWithSize(16)
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
//            oldCell?.label.font = FontsConfig.FontHelper.defaultRegularFontWithSize(16)
            newCell?.label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            //newCell?.label.font = FontsConfig.FontHelper.defaultBoldFontWithSize(18)
        }
        
        buttonBarItemSpec = ButtonBarItemSpec.cellClass(width: { _ -> CGFloat in
            return 90  // <-- Your desired width
        })
        super.viewDidLoad()
        //self.setUpLanguageText()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.notificationListViewController = nil
        self.delegate = self
        self.view.layoutSubviews()
        //self.updateBadgeCount()
    }
    
    private func bookingListVC(for type: BookingTabbar) -> BookingListViewController {
        let storyboard = UIStoryboard(name: HelperConstant.BookingStoryboard, bundle: nil)
        let bookingList = storyboard.instantiateViewController(withIdentifier: BookingListViewController.nameOfClass) as! BookingListViewController
        bookingList.type = type
        bookingList.itemInfo.title = type.description
        bookingList.bookingViewController = self
        bookingList.callbackUpdateFilterOption = updateFilterOption
        return bookingList
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let vc1 = bookingListVC(for: .AllBooking)
        let vc2 = bookingListVC(for: .Redeem)
        let vc3 = bookingListVC(for: .Refund)
        let vc4 = bookingListVC(for: .MoneyBackRequested)
        let vc5 = bookingListVC(for: .MoneyBackAccept)
        let vc6 = bookingListVC(for: .MoneyBackReject)
        
        return [vc1, vc2, vc3, vc4, vc5, vc6]
    }
}

extension BookingViewController{
    func updateFilterOption(_ type:BookingTabbar) -> Void? {
        /*
        switch type {
        case BookingTabbar.AllBooking:
            if(self.childOneVC != nil && (self.childOneVC?.isFilterEnable)!){
                self.btnFilter.isHidden = true
                self.btnReset.isHidden = false
            } else {
                self.btnFilter.isHidden = false
                self.btnReset.isHidden = true
            }
            break
        case BookingTabbar.Redeem:
            if(self.childTwoVC != nil && (self.childTwoVC?.isFilterEnable)!){
                self.btnFilter.isHidden = true
                self.btnReset.isHidden = false
            } else {
                self.btnFilter.isHidden = false
                self.btnReset.isHidden = true
            }
            
            break
        case BookingTabbar.Refund:
            if(self.childThreeVC != nil && (self.childThreeVC?.isFilterEnable)!){
                self.btnFilter.isHidden = true
                self.btnReset.isHidden = false
            } else {
                self.btnFilter.isHidden = false
                self.btnReset.isHidden = true
            }
            
            break
        case BookingTabbar.MoneyBackAccept:
            if(self.childFourVC != nil && (self.childFourVC?.isFilterEnable)!){
                self.btnFilter.isHidden = true
                self.btnReset.isHidden = false
            } else {
                self.btnFilter.isHidden = false
                self.btnReset.isHidden = true
            }
            
            break
        case BookingTabbar.MoneyBackReject:
            if(self.childFifthVC != nil && (self.childFifthVC?.isFilterEnable)!){
                self.btnFilter.isHidden = true
                self.btnReset.isHidden = false
            } else {
                self.btnFilter.isHidden = false
                self.btnReset.isHidden = true
            }
            break
        default:
            break
        }
        */
        return nil
    }
}


extension BookingViewController{
    
    @IBAction func resetFilter(_ sender: UIButton){
        /*
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            self.btnFilter.isHidden = false
            self.btnReset.isHidden = true
            
            switch currentIndex {
            case BookingTabbar.AllBooking.rawValue:
                if(self.childOneVC != nil){
                    self.childOneVC?.resetFilter()
                }
                break
            case BookingTabbar.Redeem.rawValue:
                if(self.childTwoVC != nil){
                    self.childTwoVC?.resetFilter()
                }
                
                break
            case BookingTabbar.Refund.rawValue:
                if(self.childThreeVC != nil){
                    self.childThreeVC?.resetFilter()
                }
                
                break
            case BookingTabbar.MoneyBackAccept.rawValue:
                if(self.childFourVC != nil){
                    self.childFourVC?.resetFilter()
                }
                
                break
            case BookingTabbar.MoneyBackReject.rawValue:
                if(self.childFifthVC != nil){
                    self.childFifthVC?.resetFilter()
                }
                break
            default:
                break
            }
        }
         */
    }
    
    @IBAction func actionFilter(_ sender: UIButton) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if let bookingVC = viewControllers[currentIndex] as? BookingListViewController {
                bookingVC.openFilter(sender: sender)
            }
        }
    }
    
    @IBAction private func btnSearchAction(sender: UIButton) {
        let searchVC: SearchBookingViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BookingStoryboard, viewControllerName: SearchBookingViewController.nameOfClass)
        navigationController?.pushViewController(searchVC, animated: true)
    }
}

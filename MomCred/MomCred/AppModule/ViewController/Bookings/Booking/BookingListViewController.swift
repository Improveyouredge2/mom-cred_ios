//
//  BookingListViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 04/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
//
//enum BookingType{
//    case AllBooking
//    case Redeem
//    case Refund
//    case MoneyBackAccept
//    case MoneyBackReject
//}

class BookingListViewController: LMBaseViewController, IndicatorInfoProvider {
    
    var itemInfo: IndicatorInfo = "Items"
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var currentPageIndex = 0
    var type:BookingTabbar = .AllBooking
    
    //MARK:- Var(s)
    fileprivate var presenter = BookingPresenter()
    fileprivate var isDisplayLoader = true
    
    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var bookingDetailView:BookingDetailView?
    fileprivate var redeemAlertViewController:RedeemAlertViewController?
    
    weak var bookingViewController: BookingViewController?
    var totalCredits: String? {
        didSet {
            if let credits = totalCredits, let creditVal = Double(credits) {
                bookingViewController?.lblTotalBookingAmount.text = "Total Closing Balance: \(creditVal.formattedPrice)"
            }
        }
    }
    var purchaseList:[PurchaseListResponse.PurchaseDataResponse]?
    
    var isFilterEnable = false
    
    var purchaseListRequest:PurchaseListRequest?
    
    var callbackUpdateFilterOption:((_ type:BookingTabbar) -> Void?)?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add refresh loader for pulling and refresh
        refreshView.delegate = self
        tableView.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        
        presenter.connectView(view: self)
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bookingDetailView = nil
        self.redeemAlertViewController = nil
        
        if(self.callbackUpdateFilterOption != nil){
            self.callbackUpdateFilterOption!(self.type)
        }
        
        if(purchaseList == nil || (purchaseList != nil && (purchaseList?.count)! == 0)){
            Spinner.show()
            self.serverRequest()
        }
    }
}

extension BookingListViewController{
    func openFilter(sender:UIButton){
        
        let purchaseListFilterRequest = PurchaseListFilterRequest()
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            purchaseListFilterRequest.type = "2"
            break
        case AppUser.LocalBusiness.rawValue:
            purchaseListFilterRequest.type = "1"
            break
        case AppUser.Enthusiast.rawValue:
            purchaseListFilterRequest.type = "1"
        default:
            break
        }
        self.presenter.serverPurchaseListingFilter(purchaseListFilterRequest: purchaseListFilterRequest, callback: {
            (status, response, message) in
            
            Helper.sharedInstance.openFilterOption(purchaseFilterDataList: response?.data, callbackListing:self.callbackListing)
        })
    }
    
    func resetFilter(){
        self.currentPageIndex = 0
        self.isFilterEnable = false
        self.serverRequest()
    }
}

extension BookingListViewController{
    
    fileprivate func callbackListing(_ dateList:[String]?, _ tagIndexList:[String]?) -> Void?{
        
        var startDate = ""
        var endDate = ""
        if(dateList != nil && (dateList?.count)! > 0 ){
            
            startDate = dateList?.first ?? ""
            if((dateList?.count)! > 1){
                endDate = dateList?.last ?? ""
            }
        }
        
        var tagListText = ""
        if(tagIndexList != nil && (tagIndexList?.count)! > 0){
            tagListText = tagIndexList?.joined(separator: ",") ?? ""
        }
        
        self.currentPageIndex = 0
        let purchaseListRequest = self.getPurchaseListRequest()
        purchaseListRequest.from = startDate
        purchaseListRequest.to = endDate
        purchaseListRequest.filter = tagListText
        self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
            (status, response, message) in
            if(status){
                if(response != nil){
                    //                self.btnFilter.isHidden = true
                    //                self.btnReset.isHidden = false
                    
                    self.isFilterEnable = true
                    
                    if(self.callbackUpdateFilterOption != nil){
                        self.callbackUpdateFilterOption!(self.type)
                    }
                }
            }
        })
        
        return nil
    }
    
    fileprivate func getPurchaseListRequest() -> PurchaseListRequest{
        if(!isFilterEnable){
            let purchaseListRequest = PurchaseListRequest()
            purchaseListRequest.page_id = "\(currentPageIndex)"
            
            self.purchaseListRequest = purchaseListRequest
            
            let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
            
            switch  role_id {
            case AppUser.ServiceProvider.rawValue:
                purchaseListRequest.type = "2"
                break
            case AppUser.LocalBusiness.rawValue:
                purchaseListRequest.type = "1"
                break
            case AppUser.Enthusiast.rawValue:
                purchaseListRequest.type = "1"
            default:
                break
            }
            
            switch self.type {
            case .AllBooking:
                purchaseListRequest.purchase_status = "0"
            case .Redeem:
                purchaseListRequest.purchase_status = "1"
            case .Refund:
                purchaseListRequest.purchase_status = "2"
            case .MoneyBackRequested:
                purchaseListRequest.purchase_status = "3"
            case .MoneyBackAccept:
                purchaseListRequest.purchase_status = "4"
            case .MoneyBackReject:
                purchaseListRequest.purchase_status = "5"
            }
        } else if(self.purchaseListRequest != nil){
            self.purchaseListRequest?.page_id = "\(currentPageIndex)"
        }
        
        return self.purchaseListRequest ?? PurchaseListRequest()
    }
    
    fileprivate func serverRequest(){
        let purchaseListRequest = self.getPurchaseListRequest()
        self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest,callback: {
            (status, response, message) in
        })
    }
}


extension BookingListViewController: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case .pulling:
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                let purchaseListRequest = self.getPurchaseListRequest()
                self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            let purchaseListRequest = self.getPurchaseListRequest()
            self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension BookingListViewController: UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return purchaseList != nil ? (purchaseList?.count)!: 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingListCell") as! BookingListCell
        
        let purchase = purchaseList?[indexPath.row]
        cell.lblTitle.text = purchase?.customername ?? ""
        cell.lblSubTitle.text = ""
        cell.cellIndex = indexPath.row
        cell.delegate = self
        
        cell.lblTitle.text = "\(purchase?.customername ?? "")"
        cell.lblSubTitle.text = "\(purchase?.service_name ?? "")"
        
        cell.lblTranscationNum.text = purchase?.transactionid ?? ""
        cell.lblTranscationDate.text = purchase?.date ?? ""
        
        cell.lblVerification.text = purchase?.verification_number
        if let donation = purchase?.purchase_donation_amount, let donationVal = Double(donation), donationVal > 0 {
            cell.lblDonationAmountContainer.isHidden = false
            cell.lblDonationAmount.text = donationVal.formattedPrice
        } else {
            cell.lblDonationAmountContainer.isHidden = true
        }
        if let credit = purchase?.purchaseData?.creditloyalty?.doubleValue, credit > 0 {
            cell.lblCreditAmountContainer.isHidden = false
            cell.lblCreditAmount.text = credit.formattedPrice
        } else {
            cell.lblCreditAmountContainer.isHidden = true
        }
        if let serviceAmount = purchase?.purchaseData?.serviceamount {
            cell.lblServiceAmount.text = serviceAmount.doubleValue.formattedPrice
        } else {
            cell.lblServiceAmount.text = 0.0.formattedPrice
        }

        let totalAmount =  (purchase?.purchaseData?.serviceamount?.floatValue ?? 0.00) - (purchase?.purchaseData?.creditloyalty?.floatValue ?? 0.00)
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        if role_id == AppUser.ServiceProvider.rawValue, purchase?.voucher_status == "0" {
            cell.stackViewRedeem.isHidden = false
        } else {
            cell.stackViewRedeem.isHidden = true
        }
        
        cell.lblAmount.text = totalAmount.formattedPrice
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.bookingDetailView == nil){
            
            self.bookingDetailView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BookingStoryboard, viewControllerName: BookingDetailView.nameOfClass) as BookingDetailView
            
            let purchase = purchaseList?[indexPath.row]
            var serviceName = ""
            var serviceType = ""

            serviceName = "\(purchase?.customername ?? "")"
            serviceType = "\(purchase?.service_name ?? "")"
            
            self.bookingDetailView?.serviceName = serviceName
            self.bookingDetailView?.serviceType = serviceType
            self.bookingDetailView?.purchaseId = purchase?.purchase_id ?? ""
            
            self.navigationController?.pushViewController(self.bookingDetailView!, animated: true)
        }
        
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        //        return UITableView.automaticDimension
//        return 140
//    }
}

extension BookingListViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        print("Show Redeem option")
        if(self.redeemAlertViewController == nil){
            self.redeemAlertViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BookingStoryboard, viewControllerName: RedeemAlertViewController.nameOfClass) as RedeemAlertViewController
            
            self.redeemAlertViewController?.delegate = self
            self.redeemAlertViewController?.cellIndex = cellIndex
            
            self.present(self.redeemAlertViewController!, animated: true, completion: nil)
        }
    }
}

extension BookingListViewController:RedeemAlertViewControllerDelegate {
    
    func cancelViewScr(){
        self.redeemAlertViewController = nil
    }
    
    func acceptViewScr(code:String?, cellIndex: Int){
        if(code != nil && (code?.length)! > 0){
            self.redeemAlertViewController = nil
            Spinner.show()
            
            let purchase = purchaseList?[cellIndex]
            
            let redeemCodeRequest = RedeemCodeRequest()
            redeemCodeRequest.code = code
            redeemCodeRequest.purchase_id = purchase?.purchase_id ?? ""
            
            self.presenter.updateRedeemStatus(redeemCodeRequest: redeemCodeRequest,callback: {
                (status, response, message) in
                
                if(status){
                    Spinner.show()
                    self.serverRequest()
                }
            })
        } else {
            Helper.sharedInstance.showAlertViewControllerWith(title: "Error", message: LocalizationKeys.error_redeem_code.getLocalized(), buttonTitle: "OK", controller: self)
        }
    }
}

class BookingListCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var stackViewRedeem: UIStackView!
    @IBOutlet weak var btnRedeem: UIButton!
    @IBOutlet weak var lblTranscationNum: UILabel!
    @IBOutlet weak var lblTranscationDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblVerification: UILabel!

    @IBOutlet weak var lblDonationAmountContainer: UIView! { didSet { lblDonationAmountContainer.isHidden = true } }
    @IBOutlet weak var lblDonationAmountTitle: UILabel!
    @IBOutlet weak var lblDonationAmount: UILabel!
    @IBOutlet weak var lblCreditAmountContainer: UIView! { didSet { lblCreditAmountContainer.isHidden = true } }
    @IBOutlet weak var lblCreditAmountTitle: UILabel!
    @IBOutlet weak var lblCreditAmount: UILabel!
    @IBOutlet weak var lblServiceAmountTitle: UILabel!
    @IBOutlet weak var lblServiceAmount: UILabel!

    var cellIndex: Int!
    var delegate: ProductCellDelegate?
    
    /**
     *  Show Redeem event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodShowRedeemOption(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodShowProductDetails(cellIndex: self.cellIndex)
        }
    }
}

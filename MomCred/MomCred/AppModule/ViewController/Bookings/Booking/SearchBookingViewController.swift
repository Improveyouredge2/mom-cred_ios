//
//  SearchBookingViewController.swift
//  MomCred
//
//  Created by MD on 29/01/21.
//  Copyright © 2021 Consagous. All rights reserved.
//

import UIKit

class SearchBookingViewController : LMBaseViewController {
    
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.barTintColor = UIColor.clear
            searchBar.backgroundColor = UIColor.clear
            searchBar.isTranslucent = true
            searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
            searchBar.delegate = self
            
            if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField {
                searchTextField.textColor = UIColor.white
                searchTextField.attributedPlaceholder =  NSAttributedString(string: "Verification Number", attributes: [.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
            }
        }
    }

    @IBOutlet weak var tableView: UITableView!
    
    var currentPageIndex = 0
    
    //MARK:- Var(s)
    fileprivate var presenter = SearchBookingPresenter()
    fileprivate var isDisplayLoader = true
    
    //MARK:- fileprivate
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var bookingDetailView: BookingDetailView?
    fileprivate var redeemAlertViewController: RedeemAlertViewController?
    
    var purchaseList:[PurchaseListResponse.PurchaseDataResponse]?
    
    var purchaseListRequest: PurchaseSearchListRequest?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        
        presenter.connectView(view: self)
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bookingDetailView = nil
        self.redeemAlertViewController = nil
    }
}

extension SearchBookingViewController{
    func openFilter(sender:UIButton) { }
    
    func resetFilter(){
        self.currentPageIndex = 0
        self.serverRequest()
    }
}

extension SearchBookingViewController{
    fileprivate func getPurchaseListRequest() -> PurchaseSearchListRequest {
        if purchaseListRequest == nil {
            let request = PurchaseSearchListRequest()
            request.page_id = "\(currentPageIndex)"

            let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
            
            switch  role_id {
            case AppUser.ServiceProvider.rawValue:
                request.type = "2"
                break
            case AppUser.LocalBusiness.rawValue:
                request.type = "1"
                break
            case AppUser.Enthusiast.rawValue:
                request.type = "1"
            default:
                break
            }
            request.search = searchBar.text
            purchaseListRequest = request
        } else {
            purchaseListRequest?.page_id = "\(currentPageIndex)"
        }
        
        return purchaseListRequest ?? PurchaseSearchListRequest()
    }
    
    fileprivate func serverRequest(){
        let purchaseListRequest = self.getPurchaseListRequest()
        self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest,callback: {
            (status, response, message) in
        })
    }
}


extension SearchBookingViewController: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .pulling(offset, threshould):
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                let purchaseListRequest = self.getPurchaseListRequest()
                self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            let purchaseListRequest = self.getPurchaseListRequest()
            self.presenter.serverPurchaseListing(purchaseListRequest: purchaseListRequest, callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension SearchBookingViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return purchaseList != nil ? (purchaseList?.count)! : 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingListCell") as! BookingListCell
        
        let purchase = purchaseList?[indexPath.row]
        cell.lblTitle.text = purchase?.customername ?? ""
        cell.lblSubTitle.text = ""
        cell.cellIndex = indexPath.row
        cell.delegate = self
        
        cell.lblTitle.text = "\(purchase?.customername ?? "")"
        cell.lblSubTitle.text = "\(purchase?.service_name ?? "")"
        
        cell.lblTranscationNum.text = purchase?.transactionid ?? ""
        cell.lblTranscationDate.text = purchase?.date ?? ""
        
        cell.lblVerification.text = purchase?.verification_number
        if let donation = purchase?.purchase_donation_amount, let donationVal = Double(donation), donationVal > 0 {
            cell.lblDonationAmountContainer.isHidden = false
            cell.lblDonationAmount.text = donationVal.formattedPrice
        } else {
            cell.lblDonationAmountContainer.isHidden = true
        }
        if let credit = purchase?.purchaseData?.creditloyalty?.doubleValue, credit > 0 {
            cell.lblCreditAmountContainer.isHidden = false
            cell.lblCreditAmount.text = credit.formattedPrice
        } else {
            cell.lblCreditAmountContainer.isHidden = true
        }
        if let serviceAmount = purchase?.purchaseData?.serviceamount {
            cell.lblServiceAmount.text = serviceAmount.doubleValue.formattedPrice
        } else {
            cell.lblServiceAmount.text = 0.0.formattedPrice
        }

        let totalAmount =  (purchase?.purchaseData?.serviceamount?.floatValue ?? 0.00) - (purchase?.purchaseData?.creditloyalty?.floatValue ?? 0.00)
        
        if(purchase?.voucher_status != nil && (purchase?.voucher_status?.length)! > 0 && purchase?.voucher_status?.caseInsensitiveCompare("0") == ComparisonResult.orderedSame){
            cell.stackViewRedeem.isHidden = false
        } else {
            cell.stackViewRedeem.isHidden = true
        }
        
        cell.lblAmount.text = totalAmount.formattedPrice
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.bookingDetailView == nil){
            
            self.bookingDetailView = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BookingStoryboard, viewControllerName: BookingDetailView.nameOfClass) as BookingDetailView
            
            let purchase = purchaseList?[indexPath.row]
            var serviceName = ""
            var serviceType = ""

            serviceName = "\(purchase?.customername ?? "")"
            serviceType = "\(purchase?.service_name ?? "")"
            
            self.bookingDetailView?.serviceName = serviceName
            self.bookingDetailView?.serviceType = serviceType
            self.bookingDetailView?.purchaseId = purchase?.purchase_id ?? ""
            
            self.navigationController?.pushViewController(self.bookingDetailView!, animated: true)
        }
        
    }
}

extension SearchBookingViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        print("Show Redeem option")
        if(self.redeemAlertViewController == nil){
            self.redeemAlertViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BookingStoryboard, viewControllerName: RedeemAlertViewController.nameOfClass) as RedeemAlertViewController
            
            self.redeemAlertViewController?.delegate = self
            self.redeemAlertViewController?.cellIndex = cellIndex
            
            self.present(self.redeemAlertViewController!, animated: true, completion: nil)
        }
    }
}

extension SearchBookingViewController:RedeemAlertViewControllerDelegate {
    
    func cancelViewScr(){
        self.redeemAlertViewController = nil
    }
    
    func acceptViewScr(code:String?, cellIndex: Int){
        if(code != nil && (code?.length)! > 0){
            self.redeemAlertViewController = nil
            Spinner.show()
            
            let purchase = purchaseList?[cellIndex]
            
            let redeemCodeRequest = RedeemCodeRequest()
            redeemCodeRequest.code = code
            redeemCodeRequest.purchase_id = purchase?.purchase_id ?? ""
            
            self.presenter.updateRedeemStatus(redeemCodeRequest: redeemCodeRequest,callback: {
                (status, response, message) in
                
                if(status){
                    Spinner.show()
                    self.serverRequest()
                }
            })
        } else {
            Helper.sharedInstance.showAlertViewControllerWith(title: "Error", message: LocalizationKeys.error_redeem_code.getLocalized(), buttonTitle: "OK", controller: self)
        }
    }
}

extension SearchBookingViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload(_:)), object: searchBar)
        perform(#selector(self.reload(_:)), with: searchBar, afterDelay: 0.75)
    }

    @objc func reload(_ searchBar: UISearchBar) {
        purchaseListRequest = nil
        purchaseList = nil
        currentPageIndex = 0

        guard let query = searchBar.text, query.trimmingCharacters(in: .whitespaces) != "" else {
            print("nothing to search")
            tableView.reloadData()
            return
        }

        print(query)
        let request = getPurchaseListRequest()
        presenter.serverPurchaseListing(purchaseListRequest: request) { (status, response, message) in
        }
    }
}

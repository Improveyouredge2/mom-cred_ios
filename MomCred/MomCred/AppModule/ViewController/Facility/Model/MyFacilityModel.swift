//
//  MyFacilityModel.swift
//  MomCred
//
//  Created by consagous on 10/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

///********************************/
////MARK:- Serialize Request
///*******************************/

/**
 *  PersonnelAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class MyFacilityExternalLinkItem: Mappable {
    var links: [String]?
    var description: String?
    
    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        links <- map["link"]
        description <- map["description"]
    }
}

class MyFacilityExternalLink: Mappable {
    var serviceExternalLinks: [MyFacilityExternalLinkItem]?
    
    init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        serviceExternalLinks <- map["serviceExternalLinkList"]
    }
}

class MyFacilityAddRequest: Mappable {
    
    var facility_id:String?
    
    // Page 1
    var facility_name:String?
    var facility_type:[String]?
    var facility_type_title:[String]?
    var facility_classification:[String]?
    var facility_description:String?
    var facility_instruction:String?
    var facility_instruction_info:[String]?
    var facility_instruction_info_title:[String]?
    var facility_location_info:PersonnelBusiLocationResponseData?
    var facility_location:String?
    
    // Page 2
    var facility_field:[BusinessFieldServiceCategoryInfo]?
    
    // Page 3
    var facility_equipment:[FacilityClassificationInfo]?
    
    // Page 4
    var facility_amenities:[FacilityClassificationInfo]?
    
    // Page 5
    var facility_service_list:[BusinessQualificationInfo]?
    
    // Page 6
    var facility_personal_list:[BusinessQualificationInfo]?
    
    var facility_inside_look_list: [BusinessQualificationInfo]?

    var facility_instructional_content_list: [BusinessQualificationInfo]?

    // this is an extra/duplicate key just to save data back to service, because andoid can't send the data with long key names
    var facility_instructional_content: [BusinessQualificationInfo]?

    // Page 7
    var facility_other:[BusinessQualificationInfo]?
    
    // Page 8
    var facility_notes:ServiceAddNotes?
    
    // Page 9
    var facility_ext_link: MyFacilityExternalLink?
    
    
    // For business detail display
    var doc:[BusinessAdditionalDocumentInfo]?
    var mediabusiness:[BusinessAdditionalDocumentInfo]?
    var status:String? = "0"
    var number_of_purchase:String?
    var personal_user:String?
    var pageid:NSNumber?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init() {
        //        super.init()
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        //        super.init(map: map)
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->MyFacilityAddRequest?{
        var myFacilityAddRequest:MyFacilityAddRequest?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<MyFacilityAddRequest>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    myFacilityAddRequest = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<MyFacilityAddRequest>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        myFacilityAddRequest = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return myFacilityAddRequest ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        facility_id                  <- map["facility_id"]
        
        //Page 1
        facility_name                   <- map["facility_name"]
        facility_type                   <- map["facility_type"]
        facility_type_title             <- map["facility_type_title"]
        facility_classification         <- map["facility_classification"]
        facility_instruction               <- map["facility_instruction"]
        facility_instruction_info       <- map["facility_instruction_info"]
        facility_description            <- map["facility_description"]
        facility_location_info         <- map["facility_location_info"]
        facility_instruction_info_title <- map["facility_instruction_info_title"]
        facility_location               <- map["facility_location"]
        
        //Page 2
        facility_field              <- map["facility_field"]
        
        // Page 3
        facility_equipment          <- map["facility_equipment"]
        
        // Page 4
        facility_amenities          <- map["facility_amenities"]
        
        // Page 5
        facility_service_list       <- map["facility_service_list"]
        
        // Page 6
        facility_personal_list      <- map["facility_personal_list"]
        
        // Page 7
        facility_other              <- map["facility_other"]
        
        facility_inside_look_list   <- map["facility_insidelook"]
        
        facility_instructional_content_list <- map["facility_instructionalcontent"]
        
        // extra key just for saving the data back to service
        facility_instructional_content <- map["facility_instructional"]
        
        // Page 8
        facility_notes              <- map["facility_notes"]
        
        // Page 9
        facility_ext_link           <- map["facility_ext_link"]
        
        // Page  only in response, For business detail
        doc                             <- map["externalmedia"]
        
        // For business detail
        mediabusiness                   <- map["facility_media"]
        
        pageid                          <- map["pageid"]
        
        number_of_purchase              <- map["number_of_purchase"]
        
        // In-reponse
        status                          <- map["status"]
        
        personal_user                   <- map["facility_user"]
        
    }
}

class FacilityClassificationInfo: Mappable{
    var name:String?
    var descList:[String]?
    var desc:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->FacilityClassificationInfo?{
        var facilityClassificationInfo:FacilityClassificationInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<FacilityClassificationInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    facilityClassificationInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<FacilityClassificationInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        facilityClassificationInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return facilityClassificationInfo ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        name        <- map["name"]
        descList    <- map["descList"]
        desc        <- map["desc"]
    }
}

///********************************/
////MARK:- Serialize Response
///*******************************/

/**
 *  ServiceResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class MyFacilityResponse : APIResponse {
    
    var data:[MyFacilityAddRequest]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->MyFacilityResponse?{
        var myFacilityResponse:MyFacilityResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<MyFacilityResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    myFacilityResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<MyFacilityResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        myFacilityResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return myFacilityResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
}

class MyFacilityDetailResponse: APIResponse {
    var data: MyFacilityAddRequest?
    
    required init() { super.init() }
    
    required init?(map: Map) { super.init(map: map) }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
}

class MyFacilityAddResponse : APIResponse {
    
    var data:MyFacilityAddResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->MyFacilityAddResponse?{
        var myFacilityAddResponse:MyFacilityAddResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<MyFacilityAddResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    myFacilityAddResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<MyFacilityAddResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        myFacilityAddResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return myFacilityAddResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class MyFacilityAddResponseData: Mappable{
        var facility_id : NSNumber?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->MyFacilityAddResponseData?{
            var myFacilityAddResponseData:MyFacilityAddResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<MyFacilityAddResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        myFacilityAddResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<MyFacilityAddResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            myFacilityAddResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return myFacilityAddResponseData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            facility_id     <- map["facility_id"]
            
        }
    }
}

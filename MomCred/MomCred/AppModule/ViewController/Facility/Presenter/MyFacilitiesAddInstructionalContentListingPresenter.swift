//
//  MyFacilitiesAddInstructionalContentListingPresenter.swift
//  MomCred
//
//  Created by MD on 08/08/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import Foundation

class MyFacilitiesAddInstructionalContentListingPresenter {
    
    var view: MyFacilitiesAddInstructionalContentListingViewController! // Object of account view screen

    func connectView(view: MyFacilitiesAddInstructionalContentListingViewController) {
        self.view = view
    }
}

extension MyFacilitiesAddInstructionalContentListingPresenter {
    func submitData(callback:@escaping (_ status:Bool, _ response: MyFacilityAddResponse?, _ message: String?) -> Void){
        
        MyFacilitiesService.updateData(serviceAddRequest: view.myFacilityAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }

    func getInstructionalContent(completionHandler: @escaping (_ response: ICResponse?) -> Void) {
        ICService.getInstructionalService { status, response, message in
            OperationQueue.main.addOperation() {
                if !status {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
                completionHandler(response)
                Spinner.hide()
            }
        }
    }
}

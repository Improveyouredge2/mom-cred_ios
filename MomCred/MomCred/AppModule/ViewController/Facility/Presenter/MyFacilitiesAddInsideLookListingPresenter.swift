//
//  MyFacilitiesAddInsideLookListingPresenter.swift
//  MomCred
//
//  Created by MD on 08/08/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import Foundation

class MyFacilitiesAddInsideLookListingPresenter {
    
    var view: MyFacilitiesAddInsideLookListingViewController! // Object of account view screen

    func connectView(view: MyFacilitiesAddInsideLookListingViewController) {
        self.view = view
    }
}

extension MyFacilitiesAddInsideLookListingPresenter {

    func submitData(callback:@escaping (_ status:Bool, _ response: MyFacilityAddResponse?, _ message: String?) -> Void) {
        
        MyFacilitiesService.updateData(serviceAddRequest: view.myFacilityAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }

    func getInsideLook(completionHandler: @escaping (_ response: ILCResponse?) -> Void) {
        ILCService.getInsideLookService() { (status, response, message) in
            OperationQueue.main.addOperation() {
                if !status {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
                completionHandler(response)
                Spinner.hide()
            }
        }
    }
}

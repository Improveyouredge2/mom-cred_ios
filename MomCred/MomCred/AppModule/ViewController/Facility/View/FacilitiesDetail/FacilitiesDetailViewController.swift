//
//  ServiceDetailViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * ServiceDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class FacilitiesDetailViewController : LMBaseViewController {
    
    @IBOutlet weak var lblTitleValue : UILabel!
    @IBOutlet weak var lblClassificationValue : UILabel!
    @IBOutlet weak var lblTypeValue : UILabel!
//    @IBOutlet weak var lblJobTitleValue : UILabel!
    
    @IBOutlet weak var lblInstrutionalUseValue : UILabel!
    @IBOutlet weak var lblInstrutionalServiceTypeValue : UILabel!
    @IBOutlet weak var lblInstrutionalOfficeServiceTypeValue : UILabel!
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        self.presenter.getHelpAndSupport()
    }
    
}

//
//  MyFacilitiesDetailViewController.swift
//  MomCred
//
//  Created by consagous on 12/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import Lightbox
import AVKit

/**
 * MyFacilitiesDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class MyFacilitiesDetailViewController : LMBaseViewController {
    
    // Page 1
    // Profile image view
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn : UIImageView!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitleValue : UILabel!
    @IBOutlet weak var lblClassificationValue : UILabel!
    @IBOutlet weak var lblTypeValue : UILabel!
    @IBOutlet weak var lblDescValue : UILabel!
    @IBOutlet weak var lblIsFacilityInst : UILabel!
    @IBOutlet weak var lblServiceDeliver : UILabel!
    
    @IBOutlet weak var lblLocationAddress : UILabel!
    
    // Page 2
    @IBOutlet weak var tableViewFieldServiceView: BIDetailTableView! {
        didSet {
            tableViewFieldServiceView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewFieldServiceHeight: NSLayoutConstraint!
    
    // Page 3
    @IBOutlet weak var tableViewEquipmentView: BIDetailTableView! {
        didSet {
            tableViewEquipmentView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewEquipmentHeight: NSLayoutConstraint!
    
    // Page 4
    @IBOutlet weak var tableViewAmenitiesView: BIDetailTableView! {
        didSet {
            tableViewAmenitiesView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewAmenitiesHeight: NSLayoutConstraint!
    
    // Page 10
    @IBOutlet weak var tableViewServiceListingView: BIDetailTableView! {
        didSet {
            tableViewServiceListingView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewServiceListingHeight: NSLayoutConstraint!
    
    // Page 11
    @IBOutlet weak var tableViewFacilityListingView: BIDetailTableView! {
        didSet {
            tableViewFacilityListingView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewFacilityListingHeight: NSLayoutConstraint!
    
    // Page 7
    @IBOutlet weak var tableViewOtherPersonnelView: BIDetailTableView! {
        didSet {
            tableViewOtherPersonnelView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewOtherPersonalHeight: NSLayoutConstraint!
    
    // Page 7
    @IBOutlet weak var tableViewInsideLookView: BIDetailTableView! {
        didSet {
            tableViewInsideLookView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewInsideLookHeight: NSLayoutConstraint!

    // Page 7
    @IBOutlet weak var tableViewInstructionalContentView: BIDetailTableView! {
        didSet {
            tableViewInstructionalContentView.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintsTableViewInstructionalContentHeight: NSLayoutConstraint!

    // Form 15 Additional notes
    @IBOutlet weak var tableViewAddNotesListing : BIDetailTableView! {
        didSet {
            tableViewAddNotesListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewAddNotesHeight: NSLayoutConstraint!
    
    // Form 16 External link
    @IBOutlet weak var tableViewExtenalLinkListing : BIDetailTableView! {
        didSet {
            tableViewExtenalLinkListing.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewExtenalLinkLisitingHeight: NSLayoutConstraint!
    
    // Additional Document type display
    @IBOutlet weak var tableViewAdditionalDocument : BIDetailTableView! {
        didSet {
            tableViewAdditionalDocument.sizeDelegate = self
        }
    }
    @IBOutlet weak var constraintTableViewAdditionalDocumentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView : UIScrollView!
    
    @IBOutlet weak var btnOptionMenu : UIButton!
    
    var myFacilityAddRequest:MyFacilityAddRequest?
    
    fileprivate var menuArray: [HSMenu] = []
    fileprivate var myFacilitiesAddOverviewViewController:MyFacilitiesAddOverviewViewController?
    fileprivate var defaultDocumentCollectionHeight:CGFloat = 0.00
    
    var isHideEditOption = false
    
    var collectionContentHeightObserver: NSKeyValueObservation?
    var shouldRefreshContent: Bool = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView.isHidden = true
        self.defaultDocumentCollectionHeight = self.constraintDocumentCollectionHeight.constant
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.edit.getLocalized())
        menuArray = [menu1]
        
        
        // getPersonalServiceList
        
        self.setupScrInfo()
        
        if(self.isHideEditOption){
            self.btnOptionMenu.isHidden = true
        }
        
        collectionContentHeightObserver = collectionView.observe(\.contentSize, options: [.new]) { [weak self] _, value in
            if let height = value.newValue?.height, height > 0 {
                self?.constraintDocumentCollectionHeight.constant = height
            }
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.myFacilitiesAddOverviewViewController = nil
        
        if shouldRefreshContent, let facilityId = myFacilityAddRequest?.facility_id {
            MyFacilitiesService.getFacilityDetailFor(facilityId: facilityId) { (request, error) in
                DispatchQueue.main.async {  [weak self] in
                    Spinner.hide()
                    if let request = request {
                        self?.myFacilityAddRequest = request
                        self?.setupScrInfo()
                    }
                }
            }
        }
    }
    
    deinit {
        collectionContentHeightObserver?.invalidate()
    }
}

extension MyFacilitiesDetailViewController{
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
    
    @IBAction func actionZoomOption(_ sender: AnyObject) {
        zoomableView()
    }
}


extension MyFacilitiesDetailViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if(index == 0){
            
            self.myFacilitiesAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesAddOverviewViewController.nameOfClass)
            
            self.myFacilitiesAddOverviewViewController?.myFacilityAddRequest = self.myFacilityAddRequest
            myFacilitiesAddOverviewViewController?.isEditingFacility = true
            self.navigationController?.pushViewController(self.myFacilitiesAddOverviewViewController!, animated: true)
        }
    }
}

extension MyFacilitiesDetailViewController {
    private func displayString(for titles: [String]) -> String {
        let text: String = titles.reduce("") { result, input -> String in
            result.isEmpty ? "• \(input)" : "\(result)\n• \(input)"
        }
        return text
    }

    fileprivate func setupScrInfo() {
        
        if(self.myFacilityAddRequest != nil){
            self.constraintDocumentCollectionHeight.constant = self.defaultDocumentCollectionHeight
            
            self.collectionView.reloadData()
            if(self.myFacilityAddRequest?.mediabusiness != nil && (self.myFacilityAddRequest?.mediabusiness?.count)! > 0){
                self.imgProfile.sd_setImage(with: URL(string: (self.myFacilityAddRequest?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((self.myFacilityAddRequest?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    self.imgPlayBtn.isHidden = false
                } else {
                    self.imgPlayBtn.isHidden = true
                }
                
                
            } else {
                self.imgPlayBtn.isHidden = true
                self.imgProfile.image = nil
                self.constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
            }
            
            self.lblTitleValue.text = self.myFacilityAddRequest?.facility_name ?? ""
            
            if let facilityTitles = myFacilityAddRequest?.facility_type_title, !facilityTitles.isEmpty {
                lblTypeValue.text = displayString(for: facilityTitles)
            } else {
                lblTypeValue.text = ""
            }
            
            self.lblDescValue.text = self.myFacilityAddRequest?.facility_description ?? ""
            
            self.lblIsFacilityInst.text = (self.myFacilityAddRequest?.facility_instruction != nil && (self.myFacilityAddRequest?.facility_instruction?.length)! > 0 && (self.myFacilityAddRequest?.facility_instruction ?? "") == "1") ? "Yes" : "No"
            self.lblServiceDeliver.text = ""
            
            if let instructionsTitles = myFacilityAddRequest?.facility_instruction_info_title, !instructionsTitles.isEmpty {
                lblServiceDeliver.text = displayString(for: instructionsTitles)
            } else {
                lblServiceDeliver.text = ""
            }

            if let classifications = myFacilityAddRequest?.facility_classification, !classifications.isEmpty {
                lblClassificationValue.text = displayString(for: classifications)
            } else {
                lblClassificationValue.text = ""
            }

            self.lblLocationAddress.text = ""
            
            if(self.myFacilityAddRequest?.facility_location_info != nil){
                var address = ""
                
                if(self.myFacilityAddRequest?.facility_location_info?.location_name != nil && (self.myFacilityAddRequest?.facility_location_info?.location_name?.length)! > 0){
                    address = "\(self.myFacilityAddRequest?.facility_location_info?.location_name ?? "") \n\(self.myFacilityAddRequest?.facility_location_info?.location_address ?? "")"
                    
                } else if(self.myFacilityAddRequest?.facility_location_info?.location_name_sec != nil && (self.myFacilityAddRequest?.facility_location_info?.location_name_sec?.length)! > 0){
                    address = "\(self.myFacilityAddRequest?.facility_location_info?.location_name_sec ?? "") \n\(self.myFacilityAddRequest?.facility_location_info?.location_address_sec ?? "")"
                }
                
                self.lblLocationAddress.text = address
            } else {
                self.lblLocationAddress.text = ""
            }
            
            self.scrollView.isHidden = false
        } else {
            self.scrollView.isHidden = true
        }
    }
    
    fileprivate func zoomableView(){
        // Create an array of images.
        var images:[LightboxImage] = []
        
        if(self.myFacilityAddRequest?.mediabusiness != nil && (self.myFacilityAddRequest?.mediabusiness?.count)! > 0){
            
            for media in (self.myFacilityAddRequest?.mediabusiness)! {
                //            self.imgProfile.sd_setImage(with: URL(string: (self.biAddRequest?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((media.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    
                    images.append(LightboxImage(imageURL: URL(string: media.thumb ?? "")!, text: "", videoURL: URL(string: media.imageurl ?? "")!))
                    
                } else {
                    images.append(LightboxImage(
                        imageURL: URL(string: media.imageurl ?? "")!
                    ))
                }
            }
        }
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)
        // Present your controller.
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
        
        LightboxConfig.handleVideo = { from, videoURL in
            // Custom video handling
            let videoController = AVPlayerViewController()
            videoController.player = AVPlayer(url: videoURL)
            
            from.present(videoController, animated: true) {
                videoController.player?.play()
            }
        }
    }
}


extension MyFacilitiesDetailViewController: LightboxControllerPageDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension MyFacilitiesDetailViewController: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}

extension MyFacilitiesDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    /**
     *  Specify height of row in tableview with UITableViewDelegate method override.
     *
     *  @param key tableView object and heightForRowAt index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    /**
     *  Specify number of section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewFieldServiceView {
            return myFacilityAddRequest?.facility_field?.count ?? 0
        } else if tableView == tableViewEquipmentView {
            return myFacilityAddRequest?.facility_equipment?.count ?? 0
        } else if tableView == tableViewAmenitiesView {
            return myFacilityAddRequest?.facility_amenities?.count ?? 0
        } else if tableView == tableViewServiceListingView {
            return myFacilityAddRequest?.facility_service_list?.count ?? 0
        } else if tableView == tableViewFacilityListingView {
            return myFacilityAddRequest?.facility_personal_list?.count ?? 0
        } else if tableView == tableViewOtherPersonnelView {
            return myFacilityAddRequest?.facility_other?.count ?? 0
        } else if tableView == tableViewInsideLookView {
            return myFacilityAddRequest?.facility_inside_look_list?.count ?? 0
        } else if tableView == tableViewInstructionalContentView {
            return myFacilityAddRequest?.facility_instructional_content_list?.count ?? 0
        } else if tableView == tableViewAddNotesListing {
            return myFacilityAddRequest?.facility_notes?.additionalNotes?.count ?? 0
        } else if tableView == tableViewExtenalLinkListing {
            return myFacilityAddRequest?.facility_ext_link?.serviceExternalLinks?.count ?? 0
        } else if tableView == tableViewAdditionalDocument {
            return myFacilityAddRequest?.doc?.count ?? 0
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableViewFieldServiceView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BIFieldServiceCell.nameOfClass) as! BIFieldServiceCell
            
            if let objInfo = myFacilityAddRequest?.facility_field?[indexPath.row] {
                cell.lblFieldIndex.text = "Field \(indexPath.row + 1)"
                cell.lblCatName.text = objInfo.catFieldName
                
                if let newSpecificField = objInfo.add_new_specific_field?.trim(), !newSpecificField.isEmpty {
                    cell.lblSubCatName.text = newSpecificField
                } else {
                    cell.lblSubCatName.text = objInfo.specificFieldName
                }
                
                if let classifications = objInfo.classification, !classifications.isEmpty {
                    cell.lblClassification.text = displayString(for: classifications)
                } else {
                    cell.lblClassification.text = ""
                }

                if let techniques = objInfo.technique, !techniques.isEmpty {
                    cell.lblTechnique.text = displayString(for: techniques)
                } else {
                    cell.lblTechnique.text = ""
                }
            }
            
            cell.backgroundColor = UIColor.clear
            return cell
            
        } else if tableView == self.tableViewEquipmentView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            if let objInfo = myFacilityAddRequest?.facility_equipment?[indexPath.row] {
                cell.lblIndex.text = "Equipment \(indexPath.row + 1)"
                cell.lblLink.text = objInfo.name
                cell.lblDesc.text = objInfo.desc
            }
            
            return cell
            
        } else if tableView == tableViewAmenitiesView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            if let objInfo = myFacilityAddRequest?.facility_amenities?[indexPath.row] {
                cell.lblIndex.text = "Amenity \(indexPath.row + 1)"
                cell.lblName.text = objInfo.name
                if let titles = objInfo.descList {
                    cell.lblLink.text = displayString(for: titles)
                } else {
                    cell.lblLink.text = ""
                }
                cell.lblDesc.text = objInfo.desc
            }
                        
            return cell
            
        } else if tableView == tableViewServiceListingView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            if let objInfo = myFacilityAddRequest?.facility_service_list?[indexPath.row] {
                cell.lblIndex.text = "Service Listing \(indexPath.row + 1)"
                cell.lblLink.text = objInfo.name
                cell.lblDesc.text = objInfo.desc
            }
            
            return cell
            
        } else if tableView == tableViewFacilityListingView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            if let objInfo = myFacilityAddRequest?.facility_personal_list?[indexPath.row] {
                cell.lblIndex.text = "Personnel listing \(indexPath.row + 1)"
                cell.lblLink.text = objInfo.name
                cell.lblDesc.text = objInfo.desc
            }
            
            return cell
            
        } else if tableView == tableViewOtherPersonnelView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            if let objInfo = myFacilityAddRequest?.facility_other?[indexPath.row] {
                cell.lblIndex.text = "Facility listing \(indexPath.row + 1)"
                cell.lblLink.text = objInfo.name
                cell.lblDesc.text = objInfo.desc
            }
            
            return cell
            
        }  else if tableView == tableViewInsideLookView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            if let objInfo = myFacilityAddRequest?.facility_inside_look_list?[indexPath.row] {
                cell.lblIndex.text = "Inside-look content listing \(indexPath.row + 1)"
                cell.lblLink.text = objInfo.name
                cell.lblDesc.text = objInfo.desc
            }
            
            return cell
            
        }  else if tableView == tableViewInstructionalContentView {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            if let objInfo = myFacilityAddRequest?.facility_instructional_content_list?[indexPath.row] {
                cell.lblIndex.text = "Instructional content listing \(indexPath.row + 1)"
                cell.lblLink.text = objInfo.name
                cell.lblDesc.text = objInfo.desc
            }
            
            return cell
            
        } else if tableView == tableViewAddNotesListing {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: AdditionalTitleDescView.nameOfClass) as! AdditionalTitleDescView
            
            let objInfo = myFacilityAddRequest?.facility_notes?.additionalNotes?[indexPath.row]
            cell.lblIndex.text = "Additional notes \(indexPath.row + 1)"
            cell.lblTitleDesc.text = objInfo
            
            return cell
            
        } else if tableView == tableViewExtenalLinkListing {
            let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
            
            if let objInfo = myFacilityAddRequest?.facility_ext_link?.serviceExternalLinks?[indexPath.row] {
                cell.lblIndex.text = "External link \(indexPath.row + 1)"
                cell.lblLink.text = objInfo.links?.first
                cell.lblDesc.text = objInfo.description
            }
            cell.backgroundColor = UIColor.clear
            
            return cell
            
        } else if(tableView == self.tableViewAdditionalDocument){
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationDocumentCell.nameOfClass) as! BusinessInformationAddAdditionalInformationDocumentCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.documentSelectionDelegate = self
            cell.cellIndex = indexPath
            
            if let data = self.myFacilityAddRequest?.doc?[indexPath.row] {
                cell.lblIndex.text = "Informational document \(indexPath.row + 1)"
                cell.lblName.text = data.media_title
                cell.lblDesc.text = data.media_desc
                if let imageURLs = data.imageurls, !imageURLs.isEmpty {
                    cell.arrDocumentImagesUrl = imageURLs.compactMap { $0.thumb }
                }
                cell.refreshScreenInfo()
            }
            
            return cell
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewServiceListingView {
            if let serviceId = myFacilityAddRequest?.facility_service_list?[indexPath.row].id {
                showServiceDetail(for: serviceId)
            }
        } else if tableView == tableViewFacilityListingView {
            if let personnelId = myFacilityAddRequest?.facility_personal_list?[indexPath.row].id {
                showPersonalDetail(for: personnelId)
            }
        } else if tableView == tableViewOtherPersonnelView {
            if let facilityId = myFacilityAddRequest?.facility_other?[indexPath.row].id {
                showFacilityDetail(for: facilityId)
            }
        } else if tableView == tableViewInsideLookView {
            if let insideLookId = myFacilityAddRequest?.facility_inside_look_list?[indexPath.row].id {
                showInsideLookDetail(for: insideLookId)
            }
        } else if tableView == tableViewInstructionalContentView {
            if let instructionalId = myFacilityAddRequest?.facility_instructional_content_list?[indexPath.row].id {
                showInstructionalDetail(for: instructionalId)
            }
        } else if tableView == tableViewExtenalLinkListing {
            if let link = myFacilityAddRequest?.facility_ext_link?.serviceExternalLinks?[indexPath.row].links?.first?.trim(), !link.isEmpty {
                openLink(for: link)
            }
        }
    }
}

extension MyFacilitiesDetailViewController : UICollectionViewDataSource {
    
    /**
     *  Specify number of rows on section in collectionView with UICollectionViewDataSource method override.
     *
     *  @param key UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.myFacilityAddRequest != nil && self.myFacilityAddRequest?.mediabusiness != nil && (self.myFacilityAddRequest?.mediabusiness?.count)! > 0) ? (self.myFacilityAddRequest?.mediabusiness?.count)! : 0
    }
    
    /**
     *  Implement collectionview cell in collectionView with UICollectionViewDataSource method override.
     *
     *  @param UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        let productInfo = productList?[indexPath.row]
        
        // Default product cell
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath) as! SerivceImageCell
        cell.delegate =  self
        cell.cellIndex = indexPath.row
        
        let data = self.myFacilityAddRequest?.mediabusiness![indexPath.row]
        
        if(data?.thumb != nil){
            cell.imgService.sd_setImage(with: URL(string: (data?.thumb ?? "")))
        }
        
        if((data?.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
            cell.imgPlay.isHidden = false
        } else {
            cell.imgPlay.isHidden = true
        }
        
        return cell
    }
}

extension MyFacilitiesDetailViewController : UICollectionViewDelegate {
    
    /**
     *  Implement tap event on collectionview cell in collectionView with UICollectionViewDelegate method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
    }
}

extension MyFacilitiesDetailViewController : UICollectionViewDelegateFlowLayout {
    
    /**
     *  Implement collectionView with UICollectionViewDelegateFlowLayout method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:100, height: 80)
    }
}

extension MyFacilitiesDetailViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        if(self.myFacilityAddRequest?.mediabusiness != nil && (self.myFacilityAddRequest?.mediabusiness?.count)! > 0){
            if((self.myFacilityAddRequest?.mediabusiness![cellIndex].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.imgProfile.sd_setImage(with: URL(string: (self.myFacilityAddRequest?.mediabusiness![cellIndex].thumb ?? "")))
        }
    }
}

extension MyFacilitiesDetailViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        if tableView == tableViewFieldServiceView {
            constraintsTableViewFieldServiceHeight.constant = size.height
        } else if tableView == tableViewEquipmentView {
            constraintsTableViewEquipmentHeight.constant = size.height
        } else if tableView == tableViewAmenitiesView {
            constraintsTableViewAmenitiesHeight.constant = size.height
        } else if tableView == tableViewServiceListingView {
            constraintsTableViewServiceListingHeight.constant = size.height
        } else if tableView == tableViewFacilityListingView {
            constraintsTableViewFacilityListingHeight.constant = size.height
        } else if tableView == tableViewOtherPersonnelView {
            constraintsTableViewOtherPersonalHeight.constant = size.height
        } else if tableView == tableViewInsideLookView {
            constraintsTableViewInsideLookHeight.constant = size.height
        } else if tableView == tableViewInstructionalContentView {
            constraintsTableViewInstructionalContentHeight.constant = size.height
        } else if tableView == tableViewAddNotesListing {
            constraintTableViewAddNotesHeight.constant = size.height
        } else if tableView == tableViewExtenalLinkListing {
            constraintTableViewExtenalLinkLisitingHeight.constant = size.height
        } else if tableView == tableViewAdditionalDocument {
            constraintTableViewAdditionalDocumentHeight.constant = size.height
        }
    }
}

extension MyFacilitiesDetailViewController: DocumentSelectionDelegate {
    func didSelectDocument(at indexPath: IndexPath, inCellAt cellIndexPath: IndexPath) {
        if let data = myFacilityAddRequest?.doc?[cellIndexPath.row], let imageURLs = data.imageurls, imageURLs.count > indexPath.item, let imageURLString = imageURLs[indexPath.item].imageurls, !imageURLString.isEmpty {
            openLink(for: imageURLString)
        }
    }
}

//
//  MyFacilitiesAddAmenityViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

// Page 5

/**
 * MyFacilitiesAddAmenityViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class MyFacilitiesAddAmenityViewController : LMBaseViewController{
    
    //    var expandTableNumber = [Int] ()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    
    @IBOutlet weak var tableViewClassification: UITableView!
    @IBOutlet weak var constraintTableViewClassificationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var businessInformationQualification:BusinessInformationQualification!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingFacility: Bool = false

    fileprivate var businessQualificationInfoList:[FacilityClassificationInfo] = []
    fileprivate let formNumber = 4
    fileprivate var myFacilitiesAddServiceListingViewController:MyFacilitiesAddServiceListingViewController?
    
    var myFacilityAddRequest:MyFacilityAddRequest?
    
    fileprivate var presenter = MyFacilitiesAddAmenityPresenter()
    fileprivate var classificationTitleList:[String] = []
    
    var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: For testing
        //        biAddRequest = BIAddRequest()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        viewTableView.isHidden = true
        
        tableViewClassification.backgroundColor = UIColor.clear
        
        businessInformationQualification.namePlaceHolder = LocalizationKeys.txt_name.getLocalized()
        businessInformationQualification.linkPlaceHolder = LocalizationKeys.txt_link.getLocalized()
        businessInformationQualification.textViewDesc.placeholder = LocalizationKeys.txt_description.getLocalized()
        businessInformationQualification.setScreenData()
        
        presenter.connectView(view: self)
        
        self.setScreenData()

        if !isEditingFacility {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.myFacilitiesAddServiceListingViewController = nil
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView:UITableView) {
//        super.updateViewConstraints()
        
        if(tableView == self.tableView){
            self.tableView.scrollToBottom()
            if(businessQualificationInfoList.count == 0){
                self.viewTableView.isHidden = true
            } else {
                self.viewTableView.isHidden = false
            }
            
            if(self.tableView.contentSize.height > 0){
                self.tableView.scrollToBottom()
                self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
                
                self.updateTableViewHeight(tableView:tableView)
            }
        }else if(tableView == self.tableViewClassification){
            self.tableViewClassification.scrollToBottom()
            if((self.classificationTitleList.count) == 0){
                self.constraintTableViewClassificationHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                
                self.constraintTableViewClassificationHeight?.constant = self.tableViewClassification.contentSize.height
            }
            
            self.updateTableViewHeight(tableView:tableView)
        }
    }
    
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableView == self.tableView){
            UIView.animate(withDuration: 0, animations: {
                self.tableView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewHeight.constant = heightOfTableView
            }
        } else if(tableView == self.tableViewClassification){
            UIView.animate(withDuration: 0, animations: {
                self.tableViewClassification.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewClassification.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewClassificationHeight.constant = heightOfTableView
            }
        }
    }
}

extension MyFacilitiesAddAmenityViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        if(self.myFacilityAddRequest != nil){
            
            if(self.myFacilityAddRequest?.facility_amenities != nil && (self.myFacilityAddRequest?.facility_amenities?.count)! > 0){
                self.businessQualificationInfoList = self.myFacilityAddRequest?.facility_amenities ?? []
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.myFacilitiesAddServiceListingViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesAddServiceListingViewController.nameOfClass) as MyFacilitiesAddServiceListingViewController
        
        self.myFacilitiesAddServiceListingViewController?.myFacilityAddRequest = self.myFacilityAddRequest
        myFacilitiesAddServiceListingViewController?.isEditingFacility = isEditingFacility
        self.navigationController?.pushViewController(self.myFacilitiesAddServiceListingViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool {
        /*
        if(self.businessQualificationInfoList.count == 0){
            let message = LocalizationKeys.error_add_amenity_listing.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        */
        
        if(UserDefault.getFID() != nil && (UserDefault.getFID()?.length)! > 0  && self.myFacilityAddRequest?.facility_id == nil){
            self.myFacilityAddRequest?.facility_id = UserDefault.getFID() ?? ""
        } else if(self.myFacilityAddRequest?.facility_id == nil){
            self.myFacilityAddRequest?.facility_id = ""
        }
        
        self.myFacilityAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        self.myFacilityAddRequest?.facility_amenities = self.businessQualificationInfoList        
        return true
    }
}

extension MyFacilitiesAddAmenityViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let icDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyFacilitiesDetailViewController }
            if let icDetailVC = icDetailVC as? MyFacilitiesDetailViewController {
                icDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(icDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}


extension MyFacilitiesAddAmenityViewController{
    
    @IBAction func methodAddAwardAction(_ sender: UIButton){
        
        let businessQualificationInfo = FacilityClassificationInfo()
        if((self.businessInformationQualification.inputName.text()?.length)! > 0){
            businessQualificationInfo.name = businessInformationQualification.inputName.text() ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_amenity_name.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if((self.businessInformationQualification.textViewDesc.text?.length)! > 0){
            businessQualificationInfo.desc = businessInformationQualification.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_media_content.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(self.businessQualificationInfoList.count < HelperConstant.minimumBlocks){
            
            self.isUpdate = true
            
            if(self.classificationTitleList.count > 0){
                businessQualificationInfo.descList = self.classificationTitleList
                self.classificationTitleList = []
            }
            
            self.businessInformationQualification.inputName.input.text = ""
//            self.businessInformationQualification.inputLink.input.text = ""
            
            self.businessInformationQualification.textViewDesc.text = ""
            self.businessQualificationInfoList.append(businessQualificationInfo)
            self.tableView.reloadData()
            self.tableViewClassification.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension MyFacilitiesAddAmenityViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == self.tableView){
            return businessQualificationInfoList.count
        } else if(tableView == self.tableViewClassification){
//            if((self.classificationTitleList.count) != HelperConstant.minimumBlocks){
                return (classificationTitleList.count) + 1
//            } else {
//                return classificationTitleList.count
//            }
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints(tableView: tableView)
        
        if(tableView == self.tableView){
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationQualificationAwardCell.nameOfClass) as! BusinessInformationQualificationAwardCell
            
            let objInfo = businessQualificationInfoList[indexPath.row]
            
            cell.cellIndex = indexPath
            cell.delegate = self
            
            //        cell.lblLink.text = objInfo.link
            cell.lblName.text = objInfo.name
            cell.lblDesc.text = objInfo.desc
            
            if(objInfo.descList != nil && (objInfo.descList?.count)! > 0){
                cell.lblLink.text = "• \(objInfo.descList?.joined(separator: "\n• ") ?? "")"
            } else {
                cell.lblLink.text = ""
            }
        
        return cell
            
        } else if(tableView == self.tableViewClassification){
            if(indexPath.row == 0){
                
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                
                cell.textField.placeholder = "Title"
                cell.delegate = self
                cell.refTableView = self.tableViewClassification
                
                cell.backgroundColor = UIColor.clear
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                
                cell.delegate = self
                //                cell.cellIndex = indexPath
                cell.refTableView = self.tableViewClassification
                
//                if(self.classificationTitleList.count) != HelperConstant.minimumBlocks{
                    cell.lblTitle.text = classificationTitleList[indexPath.row-1]
                    
                    cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
//                } else {
//                    cell.cellIndex = indexPath
//                    cell.lblTitle.text = classificationTitleList[indexPath.row]
//                }
                
                cell.backgroundColor = UIColor.clear
                return cell
            }
        }
        
        return UITableViewCell(frame: CGRect(x:0, y: 0, width: 0, height: 0))
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension MyFacilitiesAddAmenityViewController:BusinessInformationQualificationAwardCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
        if(self.businessQualificationInfoList.count > 0){
            self.businessQualificationInfoList.remove(at: cellIndex?.row ?? 0)
            self.tableView.reloadData()
            
            if(businessQualificationInfoList.count == 0){
                self.viewTableView.isHidden = true
            }
        }
    }
}


extension MyFacilitiesAddAmenityViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        if((self.classificationTitleList.count) < HelperConstant.minimumBlocks){
            self.isUpdate = true
            
            self.classificationTitleList.append(text)
            self.tableViewClassification.reloadData()
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension MyFacilitiesAddAmenityViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        self.isUpdate = true
        
        self.classificationTitleList.remove(at: cellIndex?.row ?? 0)
        self.tableViewClassification.reloadData()
        
    }
}

//
//  MyFacilitiesAddInstructionalContentListingViewController.swift
//  MomCred
//
//  Created by MD on 08/08/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit
import iOSDropDown

class MyFacilitiesAddInstructionalContentListingViewController : LMBaseViewController{
    
    private let formNumber = 11
    
    @IBOutlet weak var tblInstructionalContent: BIDetailTableView! {
        didSet {
            tblInstructionalContent.sizeDelegate = self
        }
    }
    
    @IBOutlet weak var tblInstructionalContentHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingFacility: Bool = false

    private var myFacilitiesAddOtherPersonnelViewController: MyFacilitiesAddOtherPersonnelViewController?
    private var isUpdate = false
    private var presenter = MyFacilitiesAddInstructionalContentListingPresenter()
    
    private var instructionalContentItems: [BusinessQualificationInfo] = [] {
        didSet {
            tblInstructionalContent.reloadData()
        }
    }
    private var instructionalContentResponse: [ICAddRequest] = [] {
        didSet {
            tblInstructionalContent.reloadData()
        }
    }

    var myFacilityAddRequest:MyFacilityAddRequest?
    var screenName:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if !screenName.isEmpty {
           lbl_NavigationTitle.text = screenName
        }
        
        tblInstructionalContent.backgroundColor = UIColor.clear
        
        if let existingInstructionalContentItems = myFacilityAddRequest?.facility_instructional_content_list {
            instructionalContentItems = existingInstructionalContentItems
        }
        
        presenter.getInstructionalContent { [weak self] response in
            if let icResponse = response?.data {
                self?.instructionalContentResponse = icResponse
            }
        }

        if !isEditingFacility {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
}

extension MyFacilitiesAddInstructionalContentListingViewController {
    private func isFormValid() -> Bool{

        if (UserDefault.getFID() != nil && (UserDefault.getFID()?.length)! > 0  && myFacilityAddRequest?.facility_id == nil){
            myFacilityAddRequest?.facility_id = UserDefault.getFID() ?? ""
        } else if (myFacilityAddRequest?.facility_id == nil) {
            myFacilityAddRequest?.facility_id = ""
        }
                
        myFacilityAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        myFacilityAddRequest?.facility_instructional_content = instructionalContentItems

        return true
    }
    
    private func openNextScr() {
        myFacilitiesAddOtherPersonnelViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesAddOtherPersonnelViewController.nameOfClass) as MyFacilitiesAddOtherPersonnelViewController
        
        myFacilitiesAddOtherPersonnelViewController?.myFacilityAddRequest = myFacilityAddRequest
        myFacilitiesAddOtherPersonnelViewController?.isEditingFacility = isEditingFacility
        navigationController?.pushViewController(self.myFacilitiesAddOtherPersonnelViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension MyFacilitiesAddInstructionalContentListingViewController {
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isUpdate {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let icDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyFacilitiesDetailViewController }
            if let icDetailVC = icDetailVC as? MyFacilitiesDetailViewController {
                icDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(icDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension MyFacilitiesAddInstructionalContentListingViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return instructionalContentItems.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddMoreCell.nameOfClass) as! AddMoreCell
            
            cell.textField.placeholder = "Link"
            cell.delegate = self
            cell.refTableView = tableView
            
            let listing = instructionalContentResponse.map { ServiceListingList(id: $0.content_id, title: $0.content_name) }
            cell.updateDropDown(listInfo: listing)

            cell.backgroundColor = UIColor.clear
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
            
            cell.delegate = self
            cell.refTableView = tableView
            
            cell.lblTitle.text = instructionalContentItems[indexPath.row - 1].name
            cell.lblTitleDesc.text = instructionalContentItems[indexPath.row - 1].desc
            
            cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
            
            cell.backgroundColor = UIColor.clear
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension MyFacilitiesAddInstructionalContentListingViewController: AddMoreCellDelegate {
    func newText(text: String, textDesc: String, textRoleDesc: String, refTableView: UITableView?, selectedServiceListingList: ServiceListingList?) {
        
        let insideLookItem = BusinessQualificationInfo()
        insideLookItem.name = text
        insideLookItem.desc = textDesc
        
        if textDesc.isEmpty {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_primary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
        } else if let serviceList = selectedServiceListingList {
            insideLookItem.id = serviceList.id
            insideLookItem.name = serviceList.title

            if instructionalContentItems.count < HelperConstant.minimumBlocks {
                if instructionalContentItems.map({ $0.id }).contains(serviceList.id) {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                } else {
                    isUpdate = true
                    instructionalContentItems.append(insideLookItem)
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_facility_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension MyFacilitiesAddInstructionalContentListingViewController: TitleDescViewDelegate {
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?) {
        isUpdate = true
        if let index = cellIndex?.row, instructionalContentItems.count > index {
            instructionalContentItems.remove(at: index)
        }
    }
}

extension MyFacilitiesAddInstructionalContentListingViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        tblInstructionalContentHeight.constant = size.height
    }
}

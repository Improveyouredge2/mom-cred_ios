//
//  MyFacilitiesAddOverviewViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import AVKit
import iOSDropDown

// Page 1
class MyFacilitiesAddOverviewViewController : LMBaseViewController{
    
    fileprivate let formNumber = 1
    
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn : UIImageView!
    @IBOutlet weak var documentImgCollectionView: UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnInstructional: UIButton!
    @IBOutlet weak var btnFrontOffice: UIButton!
    
    @IBOutlet weak var inputName: RYFloatingInput!
    
    @IBOutlet weak var collectionViewPeronnelType: DynamicHeightCollectionView!
    @IBOutlet weak var constraintCollectionPersonnelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var collectionViewInstructionalService: DynamicHeightCollectionView!
    @IBOutlet weak var collectionViewInstructionalServiceHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewFrontOfficeService: DynamicHeightCollectionView!
    @IBOutlet weak var collectionViewFrontOfficeServiceHeight: NSLayoutConstraint!

//    @IBOutlet weak var collectionViewServiceTypeDeliver: DynamicHeightCollectionView!
//    @IBOutlet weak var constraintCollectionServiceTypeDeliverHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewJob: UITableView!
    @IBOutlet weak var constraintTableViewJobHeight: NSLayoutConstraint!
    
    @IBOutlet weak var textViewDescription: KMPlaceholderTextView!
    
    @IBOutlet weak var dropDownLocation: DropDown!
    @IBOutlet weak var lblLocationAddress: UILabel!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingFacility: Bool = false

    var arrDocumentImages = [UploadImageData]()
    
    fileprivate var facilityTypeId:[String] = []
    fileprivate var serviceDeliveryTypeId:[String] = []
    
    fileprivate var presenter = MyFacilitiesAddOverviewPresenter()
    
    fileprivate var facilityType : [ListingDataDetail]?
//    fileprivate var serviceDeliveryType : [ListingDataDetail]?
    
    fileprivate var classificationTitleList:[String] = []
    
    fileprivate var isUpdate = false
    
    var myFacilityAddRequest:MyFacilityAddRequest? = MyFacilityAddRequest()
    
    fileprivate var defaultDocumentCollectionHeight:CGFloat = 0.00
    
    fileprivate var logoutViewController:PMLogoutView?
    fileprivate var selectedIndex = -1
    fileprivate var selectedDocumentImages:UploadImageData?
    fileprivate var myFacilitiesAddFieldServiceViewController:MyFacilitiesAddFieldServiceViewController?
    var isScreenDataReload = false
    var screenName:String = ""
    
    var instruction_frontoffice_list : [ListingDataDetail]?
    
    var busiLocation:PersonnelBusiLocationResponseData?
    var selectedLocationIndex:String? = "0"
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isScreenDataReload = true
        presenter.connectView(view: self)
        
        self.defaultDocumentCollectionHeight = self.constraintDocumentCollectionHeight.constant
        self.constraintDocumentCollectionHeight.constant = 0.0
        
        self.checkBusinessLocalImage()
        self.presenter.serverBusiLocationRequest()
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
//        self.reloadChildListing()
        self.setScreenData()
        
        if !isEditingFacility {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.myFacilitiesAddFieldServiceViewController = nil
        
        self.documentImgCollectionView.backgroundColor = UIColor.clear
        
        if(isScreenDataReload){
            self.presenter.serverParentListingRequest()
        }
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView:UITableView) {
        
        if(tableView == self.tableViewJob){
            self.tableViewJob.scrollToBottom()
            if((self.classificationTitleList.count) == 0){
                self.constraintTableViewJobHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                
                self.constraintTableViewJobHeight?.constant = self.tableViewJob.contentSize.height
            }
            
            self.updateTableViewHeight(tableView: tableView)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewJob == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewJob.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewJob.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewJobHeight.constant = heightOfTableView
            }
        }
    }
}

extension MyFacilitiesAddOverviewViewController{
    
    /**
     *  Check local image in business information id folder
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func checkBusinessLocalImage(){
        
        if((self.myFacilityAddRequest != nil && self.myFacilityAddRequest?.facility_id != nil) || (UserDefault.getFID() != nil && (UserDefault.getFID()?.length)! > 0)){
            var busi_id = ""
            
            if(self.myFacilityAddRequest != nil && self.myFacilityAddRequest?.facility_id != nil){
                busi_id = self.myFacilityAddRequest?.facility_id ?? ""
            } else {
                busi_id = UserDefault.getFID() ?? ""
            }
            
            let folderName = "service_\(busi_id)"
            if(PMFileUtils.directoryExistsAtPath(folderName)){
                
                let fileUrls:[URL]? = PMFileUtils.getFolderAllFileUrls(folderName: folderName)
                print("Urls: \(fileUrls ?? [])")
                
                if(fileUrls != nil && (fileUrls?.count)! > 0){
                    do {
                        for fileUrl in (fileUrls)! {
                            let imageData = try Data(contentsOf: fileUrl)
                            let theFileName = (fileUrl.absoluteString as NSString).lastPathComponent
                            let theExt = (fileUrl.absoluteString as NSString).pathExtension

                            let imageDataTemp = UploadImageData()
                            
                            imageDataTemp.imageData = imageData
                            imageDataTemp.imageName = theFileName
                            imageDataTemp.imageKeyName = "busi_img"
                            if(theExt.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpeg") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame){
                                imageDataTemp.imageType = "1"
                            } else {
                                imageDataTemp.imageType = "2"
                            }
                            
                            self.arrDocumentImages.append(imageDataTemp)
                            
                        }
                    } catch {
                        print("Error while enumerating files")
                    }
                    
                    if !arrDocumentImages.isEmpty {
                        documentImgCollectionView.reloadData()
                    }
                }
            }
        }
    }
    
    func updateScreenListing(){
        self.facilityType = MyFacilitiesAddOverviewPresenter.parentListingList?.parentListingData?.facilitytype ?? []
        
        self.collectionViewPeronnelType.reloadData()
        self.collectionViewPeronnelType.layoutIfNeeded()
         if(MyFacilitiesAddOverviewPresenter.parentListingList?.parentListingData?.services_types != nil){
            self.instruction_frontoffice_list = MyFacilitiesAddOverviewPresenter.parentListingList?.parentListingData?.services_types
            
            self.reloadChildListing()
        }
        
    }
    
    func updateLocationInfo(){
        
        // dropDownLocation
        // Location list
        ////////////////////////////////////////////////////////////
        dropDownLocation.optionArray = []
        dropDownLocation.optionIds = []
        
        if(self.busiLocation != nil){
            if(self.busiLocation?.location_name != nil && (self.busiLocation?.location_name?.length)! > 0){
                dropDownLocation.optionArray.append(self.busiLocation?.location_name ?? "")
                dropDownLocation.optionIds?.append(1)
            }
            
            if(self.busiLocation?.location_name_sec != nil && (self.busiLocation?.location_name_sec?.length)! > 0){
                dropDownLocation.optionArray.append(self.busiLocation?.location_name_sec ?? "")
                dropDownLocation.optionIds?.append(2)
            }
            
            // The the Closure returns Selected Index and String
            dropDownLocation.didSelect{(selectedText , index ,id) in
                print("Selected String: \(selectedText) \n index: \(index)")
                self.selectedLocationIndex = "\(id)"
                
                var address = ""
                
                if(self.busiLocation?.location_name != nil && (self.busiLocation?.location_name?.length)! > 0 && index == 0){
                    address = "\(self.busiLocation?.location_name ?? "") \n\(self.busiLocation?.location_address ?? "")"
                    
                } else if(self.self.busiLocation?.location_name_sec != nil && (self.busiLocation?.location_name_sec?.length)! > 0){
                    address = "\(self.busiLocation?.location_name_sec ?? "") \n\(self.busiLocation?.location_address_sec ?? "")"
                }
                self.lblLocationAddress.text = address
                self.isUpdate = true
            }
        }
    }
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Facility Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        
        if(self.myFacilityAddRequest?.mediabusiness != nil && (self.myFacilityAddRequest?.mediabusiness?.count)! > 0){
            self.imgProfile.sd_setImage(with: URL(string: (self.myFacilityAddRequest?.mediabusiness![0].thumb ?? "")))
            
            // Check video on first pos
            if((self.myFacilityAddRequest?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.constraintDocumentCollectionHeight.constant = self.defaultDocumentCollectionHeight
        }
        
        self.inputName.input.text = self.myFacilityAddRequest?.facility_name ?? ""
        
        self.classificationTitleList = self.myFacilityAddRequest?.facility_classification ?? []
        self.textViewDescription.text = self.myFacilityAddRequest?.facility_description ?? ""
        self.facilityTypeId = self.myFacilityAddRequest?.facility_type ?? []
        self.serviceDeliveryTypeId = self.myFacilityAddRequest?.facility_instruction_info ?? []
        
        if(self.myFacilityAddRequest?.facility_location_info != nil){
            var locationName = ""
            
            if(self.myFacilityAddRequest?.facility_location_info?.location_name != nil && (self.myFacilityAddRequest?.facility_location_info?.location_name?.length)! > 0){
                locationName = self.myFacilityAddRequest?.facility_location_info?.location_name ?? ""
            } else if(self.myFacilityAddRequest?.facility_location_info?.location_name_sec != nil && (self.myFacilityAddRequest?.facility_location_info?.location_name_sec?.length)! > 0){
                locationName = self.myFacilityAddRequest?.facility_location_info?.location_name_sec ?? ""
            }
            
            self.dropDownLocation.text = locationName
        }
        self.selectedLocationIndex = self.myFacilityAddRequest?.facility_location
        
        if(self.myFacilityAddRequest?.facility_location_info != nil){
            var address = ""
            
            if(self.myFacilityAddRequest?.facility_location_info?.location_name != nil && (self.myFacilityAddRequest?.facility_location_info?.location_name?.length)! > 0){
                address = "\(self.myFacilityAddRequest?.facility_location_info?.location_name ?? "") \n\(self.myFacilityAddRequest?.facility_location_info?.location_address ?? "")"
                
            } else if(self.myFacilityAddRequest?.facility_location_info?.location_name_sec != nil && (self.myFacilityAddRequest?.facility_location_info?.location_name_sec?.length)! > 0){
                address = "\(self.myFacilityAddRequest?.facility_location_info?.location_name_sec ?? "") \n\(self.myFacilityAddRequest?.facility_location_info?.location_address_sec ?? "")"
            }
            
            self.lblLocationAddress.text = address
        } else {
            self.lblLocationAddress.text = ""
        }
        
        if(self.myFacilityAddRequest?.facility_instruction != nil && self.myFacilityAddRequest?.facility_instruction == "1"){
            self.methodInstructionFrontOfficeAction(self.btnInstructional)
        } else {
            self.methodInstructionFrontOfficeAction(self.btnFrontOffice)
        }
        
        self.isUpdate = false
        
        // update collection info
        if(self.myFacilityAddRequest?.mediabusiness != nil && (self.myFacilityAddRequest?.mediabusiness?.count)! > 0){
            
            //            self.arrDocumentImages
            for mediaBusiness in (self.myFacilityAddRequest?.mediabusiness)! {
                let mediaImage = UploadImageData()
                mediaImage.imageUrl = mediaBusiness.thumb ?? ""
                mediaImage.id = mediaBusiness.media_id ?? ""
                if let mediaExt = Int(mediaBusiness.media_extension ?? "0"){
                    if(mediaExt == 0 || mediaExt == 1){
                        mediaImage.imageType = "1"
                    } else {
                        mediaImage.imageType = "2"
                    }
                } else {
                    mediaImage.imageType = "1"
                }
                
                self.arrDocumentImages.append(mediaImage)
            }
            self.documentImgCollectionView.reloadData()
        }
        
        
        
        //        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.myFacilityAddRequest?.facility_name != (self.inputName.text()?.trim() ?? "")){
            isUpdate = true
        }
        
        
        if(self.textViewDescription.text != (self.myFacilityAddRequest?.facility_description ?? "")){
            isUpdate = true
        }
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.myFacilitiesAddFieldServiceViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesAddFieldServiceViewController.nameOfClass) as MyFacilitiesAddFieldServiceViewController
        
        self.myFacilitiesAddFieldServiceViewController?.myFacilityAddRequest = self.myFacilityAddRequest
        myFacilitiesAddFieldServiceViewController?.isEditingFacility = isEditingFacility
        self.navigationController?.pushViewController(self.myFacilitiesAddFieldServiceViewController!, animated: true)
    }
    
    fileprivate func isVideoInList() -> Bool{
        
        if(self.arrDocumentImages.count > 0){
            var isFound = false
            for imageData in arrDocumentImages {
                
                
                if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                    
                    if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                        isFound = true
                    }
                } else {
                    if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                        isFound = true
                    }
                }
            }
            
            return isFound
        }
        
        return false
    }
    
}

extension MyFacilitiesAddOverviewViewController{
    fileprivate func reloadChildListing(){
        
        if(self.instruction_frontoffice_list != nil && (self.instruction_frontoffice_list?.count)! > 0){
            for service in self.instruction_frontoffice_list!{
                self.presenter.serverChildListingRequest(parentId: service.listing_id ?? "")
            }
        }
    }
    
    func updateServiceDeliveryTypeList() {
        collectionViewInstructionalService.reloadData()
        collectionViewFrontOfficeService.reloadData()
        
        /*
        if(self.instruction_frontoffice_list != nil){
            if(self.btnInstructional.isSelected){
                self.serviceDeliveryType = self.instruction_frontoffice_list![0].subcat
            } else {
                self.serviceDeliveryType = self.instruction_frontoffice_list![1].subcat
            }
            
            self.collectionViewServiceTypeDeliver.reloadData()
            self.collectionViewServiceTypeDeliver.layoutIfNeeded()
        }
         */
    }
}

extension MyFacilitiesAddOverviewViewController: UITextFieldDelegate{
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        let numCheckStatus:Bool = false
        var lenCheckStatus:Bool = false
        let isNumCheckReq = false
        
        // check backspace in input character
        if (isBackSpace == -92) {
            return true
        }
        
        // check new length of string after adding new character
        let newLength = text.count + string.count - range.length
        if newLength > HelperConstant.LIMIT_PURCHASEBILITY{
            return false
        }
        
        lenCheckStatus = true
        
        if(isNumCheckReq == true){
            return numCheckStatus && lenCheckStatus
        }else if(lenCheckStatus == true){
            return lenCheckStatus
        }
    }
}

extension MyFacilitiesAddOverviewViewController{
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func pickedImage(fromImagePicker: UIImage, imageData: Data, mediaType:String, imageName:String, fileUrl: String?) {
        
        let imageDataTemp = UploadImageData()
        
        let theExt = (imageName as NSString).pathExtension
        
        if(theExt.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpeg") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame){
            imageDataTemp.imageType = "1"
        } else {
            imageDataTemp.imageType = "2"
        }
        
        if(self.myFacilityAddRequest?.facility_id != nil){
            imageDataTemp.id = self.myFacilityAddRequest?.facility_id ?? ""
        } else if(UserDefault.getFID() != nil && (UserDefault.getFID()?.length)! > 0){
            let busi_id = UserDefault.getFID() ?? ""
            imageDataTemp.id = busi_id
        }
        
        imageDataTemp.imageData = imageData
        imageDataTemp.imageName = "\((self.arrDocumentImages.count + 1))\(imageName)"
        imageDataTemp.imageKeyName = "busi_img"
        imageDataTemp.filePath = fileUrl
        
        imageDataTemp.imageType = mediaType
        imageDataTemp.imageThumbnailData = fromImagePicker.pngData()
        
        // Check of video size
        if(imageDataTemp.imageType == "2"){
            if(Int(Helper.sharedInstance.checkVideoSize(reqData: imageData)) < HelperConstant.LIMIT_VIDEO_SIZE){
                
                self.imgProfile.image = fromImagePicker
                self.imgPlayBtn.isHidden = false
                
                var arrTempDocumentImages = [UploadImageData]()
                arrTempDocumentImages.append(imageDataTemp)
                for arrDocument in self.arrDocumentImages{
                    arrTempDocumentImages.append(arrDocument)
                }
                
                self.arrDocumentImages = arrTempDocumentImages
            } else {
                self.showBannerAlertWith(message: LocalizationKeys.error_file_size.getLocalized(), alert: .error)
            }
        } else {
            // Display image on main view
            self.imgProfile.image = fromImagePicker
            self.imgPlayBtn.isHidden = true
            
            self.arrDocumentImages.append(imageDataTemp)
        }

        if(self.arrDocumentImages.count > 0 && self.arrDocumentImages.count < HelperConstant.maximumProfileMedia){
            self.isUpdate = true
            self.constraintDocumentCollectionHeight.constant = self.defaultDocumentCollectionHeight
            
            self.documentImgCollectionView.reloadData()
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension MyFacilitiesAddOverviewViewController{
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if(self.facilityType != nil){
            self.facilityTypeId = []
            for target in self.facilityType!{
                if(target.selectionStaus){
                    self.facilityTypeId.append(target.listing_id ?? "")
                }
            }
        }
        
        serviceDeliveryTypeId = []
        if let instructionalServices = instruction_frontoffice_list?.first?.subcat {
            for service in instructionalServices where service.selectionStaus {
                if let listingId = service.listing_id {
                    serviceDeliveryTypeId.append(listingId)
                }
            }
        }
        
        if let frontOfficeServices = instruction_frontoffice_list?.last?.subcat {
            for service in frontOfficeServices where service.selectionStaus {
                if let listingId = service.listing_id {
                    serviceDeliveryTypeId.append(listingId)
                }
            }
        }

        /*
        if btnInstructional.isSelected, let instructionalServices = instruction_frontoffice_list?.first?.subcat {
            for service in instructionalServices where service.selectionStaus {
                if let listingId = service.listing_id {
                    serviceDeliveryTypeId.append(listingId)
                }
            }
        } else if btnFrontOffice.isSelected, let frontOfficeServices = instruction_frontoffice_list?.last?.subcat {
            for service in frontOfficeServices where service.selectionStaus {
                if let listingId = service.listing_id {
                    serviceDeliveryTypeId.append(listingId)
                }
            }
        }
         */
        
        if(self.arrDocumentImages.count == 0){
            message = LocalizationKeys.error_select_facility_service_image.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }else if (self.inputName.text()?.trim().isEmpty)! {
            message = LocalizationKeys.error_select_facility_service_name.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }/* else if(self.classificationTitleList.count == 0){
            message = LocalizationKeys.error_facility_classification.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }*/  else if(self.facilityTypeId.count == 0){
            message = LocalizationKeys.error_select_facility_type.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if(self.textViewDescription.text.trim().isEmpty){
            message = LocalizationKeys.error_select_facility_desc.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if(self.serviceDeliveryTypeId.count == 0){
            message = LocalizationKeys.error_select_facility_facility_title.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if(UserDefault.getFID() != nil && (UserDefault.getFID()?.length)! > 0  && self.myFacilityAddRequest?.facility_id == nil){
            self.myFacilityAddRequest?.facility_id = UserDefault.getFID() ?? ""
        } else if(self.myFacilityAddRequest?.facility_id == nil){
            self.myFacilityAddRequest?.facility_id = ""
        }
        
        self.myFacilityAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        self.myFacilityAddRequest?.facility_name = self.inputName.text()?.trim() ?? ""
        
        self.myFacilityAddRequest?.facility_classification = self.classificationTitleList
        self.myFacilityAddRequest?.facility_description = self.textViewDescription.text
        self.myFacilityAddRequest?.facility_type = self.facilityTypeId
        self.myFacilityAddRequest?.facility_instruction = self.btnInstructional.isSelected ? "1" : "0"
        self.myFacilityAddRequest?.facility_instruction_info = self.serviceDeliveryTypeId
        self.myFacilityAddRequest?.facility_location = self.selectedLocationIndex
        
        return true
    }
}

//MARK:- Button Action method implementation
extension MyFacilitiesAddOverviewViewController{
    
    @IBAction func methodInstructionFrontOfficeAction(_ sender: UIButton) {
        
        self.btnInstructional.isSelected = false
        self.btnFrontOffice.isSelected = false
        self.isUpdate = true
        
        if(self.btnInstructional == sender){
            self.btnInstructional.isSelected = true
//            self.updateServiceDeliveryTypeList()
            
        } else {
            self.btnFrontOffice.isSelected = true
//            self.updateServiceDeliveryTypeList()
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let icDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyFacilitiesDetailViewController }
            if let icDetailVC = icDetailVC as? MyFacilitiesDetailViewController {
                icDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(icDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }

    /**
     *  Image picker button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnImagePickerAction(_ sender : UIButton){
        self.displayPhotoSelectionOption(withCircularAllow:false)
    }
}


// MARK:- UITableViewDataSource & UITableViewDelegate
extension MyFacilitiesAddOverviewViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == tableViewJob){
//            if((self.classificationTitleList.count) != HelperConstant.minimumBlocks){
                return (classificationTitleList.count) + 1
//            } else {
//                return classificationTitleList.count
//            }
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints(tableView: tableView)
        
        if(self.tableViewJob == tableView){
            
            
            if(indexPath.row == 0){
                
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                
                cell.textField.placeholder = "Title"
                cell.delegate = self
                cell.refTableView = self.tableViewJob
                
                cell.backgroundColor = UIColor.clear
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                
                cell.delegate = self
                //                cell.cellIndex = indexPath
                cell.refTableView = self.tableViewJob
                
//                if(self.classificationTitleList.count) != HelperConstant.minimumBlocks{
                    cell.lblTitle.text = classificationTitleList[indexPath.row-1]
                    
                    cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
//                } else {
//                    cell.cellIndex = indexPath
//                    cell.lblTitle.text = classificationTitleList[indexPath.row]
//                }
                
                cell.backgroundColor = UIColor.clear
                return cell
            }
            
        }
        
        return UITableViewCell(frame: CGRect(x:0, y: 0, width: 0, height: 0))
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension MyFacilitiesAddOverviewViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        
        if(refTableView == tableViewJob){
            if((self.classificationTitleList.count) < HelperConstant.minimumBlocks){
                self.isUpdate = true
                self.classificationTitleList.append(text)
                self.tableViewJob.reloadData()
                
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

extension MyFacilitiesAddOverviewViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        self.isUpdate = true
        if(refTableView == tableViewJob){
            
            self.classificationTitleList.remove(at: cellIndex?.row ?? 0)
            self.tableViewJob.reloadData()
        }
    }
}

/*******************************************/
//MARK: - UICollectionViewDelegate
/*******************************************/
extension MyFacilitiesAddOverviewViewController : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == documentImgCollectionView {
            return arrDocumentImages.count
        } else if collectionView == collectionViewPeronnelType {
            return facilityType?.count ?? 0
        } else if collectionView == collectionViewInstructionalService {
            return instruction_frontoffice_list?.first?.subcat?.count ?? 0
        } else if collectionView == collectionViewFrontOfficeService {
            return instruction_frontoffice_list?.last?.subcat?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //MARK:- Store Images views
        if(collectionView == self.documentImgCollectionView){
            
            //MARK:- Store Images views
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath as IndexPath) as! SerivceImageCell
            
            let imageData = arrDocumentImages[indexPath.row]
            
            cell.delegate = self
            cell.cellIndex = indexPath.row
            cell.btnDelete.isHidden = false
            cell.btnDelete.tag = indexPath.row
            
            if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                
                if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                    cell.imgPlay.isHidden = false
                    cell.imgService.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                } else {
                    cell.imgPlay.isHidden = true
                    cell.imgService.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                }
            } else {
                if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                    cell.imgPlay.isHidden = false
                    cell.imgService.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                } else {
                    cell.imgPlay.isHidden = true
                    cell.imgService.image = UIImage(data: imageData.imageData ?? Data())
                }
            }
            return cell
        } else if(collectionView == self.collectionViewPeronnelType){
            let cell = self.collectionViewPeronnelType.dequeueReusableCell(withReuseIdentifier: CollectionCheckboxListCVCell.nameOfClass, for: indexPath as IndexPath) as! CollectionCheckboxListCVCell
            
            self.constraintCollectionPersonnelHeight.constant = self.collectionViewPeronnelType.intrinsicContentSize.height
            cell.refCollection = collectionView

            var listingData:ListingDataDetail?
            if(self.facilityType != nil && (self.facilityType?.count)! > indexPath.section){
                listingData = self.facilityType![indexPath.row]
            }

            if(listingData != nil){
                cell.delegate = self
                cell.cellIndex = indexPath
                cell.btnCheckBox.setTitle(listingData?.listing_title ?? "", for: UIControl.State.normal)
                cell.btnCheckBox.isSelected = listingData?.selectionStaus ?? false

                if(!self.isUpdate && self.myFacilityAddRequest?.facility_type != nil && (self.myFacilityAddRequest?.facility_type?.count)! > 0){
                    if(self.myFacilityAddRequest?.facility_type?.contains(listingData?.listing_id ?? "") ?? false){
                        cell.btnCheckBox.isSelected = true
                        listingData?.selectionStaus = true
                    }
                }
                cell.backgroundColor = .clear
            }
            return cell
        } else if collectionView == collectionViewInstructionalService {
            let cell = collectionViewPeronnelType.dequeueReusableCell(withReuseIdentifier: CollectionCheckboxListCVCell.nameOfClass, for: indexPath as IndexPath) as! CollectionCheckboxListCVCell
            cell.backgroundColor = .clear

            collectionViewInstructionalServiceHeight.constant = collectionViewInstructionalService.intrinsicContentSize.height
            cell.refCollection = collectionView
            
            if let listingData: ListingDataDetail = instruction_frontoffice_list?.first?.subcat?[indexPath.item] {
                cell.delegate = self
                cell.cellIndex = indexPath
                cell.btnCheckBox.setTitle(listingData.listing_title ?? "", for: UIControl.State.normal)
                cell.btnCheckBox.isSelected = listingData.selectionStaus
                
                let isSelected = myFacilityAddRequest?.facility_instruction_info?.contains(listingData.listing_id ?? "") ?? false
                if !isUpdate && isSelected {
                    cell.btnCheckBox.isSelected = true
                    listingData.selectionStaus = true
                }
            }
            return cell
        }  else if collectionView == collectionViewFrontOfficeService {
            let cell = collectionViewPeronnelType.dequeueReusableCell(withReuseIdentifier: CollectionCheckboxListCVCell.nameOfClass, for: indexPath as IndexPath) as! CollectionCheckboxListCVCell
            cell.backgroundColor = .clear
            
            collectionViewFrontOfficeServiceHeight.constant = collectionViewFrontOfficeService.intrinsicContentSize.height
            cell.refCollection = collectionView
            
            if let listingData: ListingDataDetail = instruction_frontoffice_list?.last?.subcat?[indexPath.item] {
                cell.delegate = self
                cell.cellIndex = indexPath
                cell.btnCheckBox.setTitle(listingData.listing_title ?? "", for: UIControl.State.normal)
                cell.btnCheckBox.isSelected = listingData.selectionStaus
                
                let isSelected = myFacilityAddRequest?.facility_instruction_info?.contains(listingData.listing_id ?? "") ?? false
                if !isUpdate && isSelected {
                    cell.btnCheckBox.isSelected = true
                    listingData.selectionStaus = true
                }
            }
            return cell
        }
        return UICollectionViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == documentImgCollectionView {
            return CGSize(width:100, height: 80)
        } else if collectionView ==  collectionViewInstructionalService || collectionView == collectionViewFrontOfficeService || collectionView == collectionViewPeronnelType {
            return CGSize(width: (collectionView.frame.size.width / 2), height: 45 )
        }
        
        return CGSize(width: 0, height: 0)
    }
}

extension MyFacilitiesAddOverviewViewController:CollectionCheckboxListCVCellDelegate{
    func selectListItem(cellIndex: IndexPath?, status:Bool, refCollection: UICollectionView?){
        
        if refCollection == collectionViewPeronnelType {
            if let indexPath = cellIndex, let facilityData = facilityType?[indexPath.item] {
                facilityData.selectionStaus = status
                isUpdate = true
            }
        } else if refCollection == collectionViewInstructionalService {
            if let indexPath = cellIndex, let listingData = instruction_frontoffice_list?.first?.subcat?[indexPath.item] {
                listingData.selectionStaus = status
                isUpdate = true
            }
        } else if refCollection == collectionViewInstructionalService {
            if let indexPath = cellIndex, let listingData = instruction_frontoffice_list?.last?.subcat?[indexPath.item] {
                listingData.selectionStaus = status
                isUpdate = true
            }
        }
    }
}


extension MyFacilitiesAddOverviewViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        //TODO: Update image as per selection
        
        if !arrDocumentImages.isEmpty {
            let imageData = self.arrDocumentImages[cellIndex]
            if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                    self.imgPlayBtn.isHidden = false
                    self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                } else {
                    self.imgPlayBtn.isHidden = true
                    self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                }
            } else {
                if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                    self.imgPlayBtn.isHidden = false
                    self.imgProfile.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                } else {
                    self.imgPlayBtn.isHidden = true
                    self.imgProfile.image = UIImage(data: imageData.imageData ?? Data())
                }
            }
        }
        
    }
}

//MARK:- Button Actions
extension MyFacilitiesAddOverviewViewController{
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnAddImg(_ sender: UIButton) {
        if (!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)) {
            if arrDocumentImages.count < HelperConstant.maximumProfileMedia {
                if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                    //already authorized
                    debugPrint("already authorized")
                    
                    OperationQueue.main.addOperation() {
                        if(!self.isVideoInList()){
                            self.displayPhotoSelectionOption(withCircularAllow:false, isSelectVideo: true)
                        } else {
                            self.displayPhotoSelectionOption(withCircularAllow: false)
                        }
                    }
                } else {
                    AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                        if granted {
                            //access allowed
                            debugPrint("access allowed")
                            OperationQueue.main.addOperation() {
                                //                            if(self.arrDocumentImages.count == 0){
                                if(!self.isVideoInList()){
                                    self.displayPhotoSelectionOption(withCircularAllow:false, isSelectVideo: true)
                                } else {
                                    self.displayPhotoSelectionOption(withCircularAllow: false)
                                }
                            }
                        } else {
                            //access denied
                            debugPrint("Access denied")
                            OperationQueue.main.addOperation() {
                                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_this_app_needs_access_to_your_camera_and_gallery.getLocalized(), buttonTitle: nil, controller: nil)
                            }
                        }
                    })
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_video_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnDeleteStoreImg(_ sender: UIButton) {
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            let imageData = arrDocumentImages[sender.tag]
            if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                self.selectedDocumentImages = imageData
                // TODO: approval alert
                self.logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
                self.logoutViewController?.delegate = self
                self.logoutViewController?.scrTitle = ""
                self.logoutViewController?.scrDesc = LocalizationKeys.error_remove_media.getLocalized()
                self.selectedIndex = sender.tag
                self.logoutViewController?.modalPresentationStyle = .overFullScreen
                self.present(logoutViewController!, animated: true, completion: nil)
            } else {
                if(self.arrDocumentImages.count > 0){
                    self.arrDocumentImages.remove(at: sender.tag)
                }
                
                if self.arrDocumentImages.count == 0{
                    self.constraintDocumentCollectionHeight.constant = 0.00
                    self.imgPlayBtn.isHidden = true
                    self.imgProfile.image = nil
                } else {
                    let imageData = self.arrDocumentImages[0]
                    if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                        if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                            self.imgPlayBtn.isHidden = false
                            self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                        } else {
                            self.imgPlayBtn.isHidden = true
                            self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                        }
                    } else {
                        if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                            self.imgPlayBtn.isHidden = false
                            self.imgProfile.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                        } else {
                            self.imgPlayBtn.isHidden = true
                            self.imgProfile.image = UIImage(data: imageData.imageData ?? Data())
                        }
                    }
                    
                    self.documentImgCollectionView.reloadData()
                }
            }
        }
    }
}

extension MyFacilitiesAddOverviewViewController: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
        
        if(self.selectedDocumentImages != nil){
            let deleteMediaRequest = DeleteMediaRequest(media_id: self.selectedDocumentImages?.id ?? "")
            self.presenter.deleteMediaData(deleteMediaRequest: deleteMediaRequest, callback: {
                (status, response, message) in
                
                if(status){
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    if(self.arrDocumentImages.count > 0){
                        self.arrDocumentImages.remove(at: self.selectedIndex)
                    }
                    
                    if self.arrDocumentImages.count == 0{
                        self.constraintDocumentCollectionHeight.constant = 0.00
                        self.imgProfile.image = nil
                    }
                    
                    self.documentImgCollectionView.reloadData()
                    
                } else {
                    
                }
            })
        }
    }
}

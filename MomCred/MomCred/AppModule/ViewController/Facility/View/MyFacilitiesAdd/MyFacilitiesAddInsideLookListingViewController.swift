//
//  MyFacilitiesAddInsideLookListingViewController.swift
//  MomCred
//
//  Created by MD on 08/08/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit
import iOSDropDown

class MyFacilitiesAddInsideLookListingViewController : LMBaseViewController{
    
    private let formNumber = 10
        
    @IBOutlet weak var tblInsideLook: BIDetailTableView! {
        didSet {
            tblInsideLook.sizeDelegate = self
        }
    }
    
    @IBOutlet weak var tblInsideLookHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingFacility: Bool = false

    private var addInstructionalContentVC: MyFacilitiesAddInstructionalContentListingViewController?
    private var isUpdate = false
    private var presenter = MyFacilitiesAddInsideLookListingPresenter()
    
    private var insideLookItems: [BusinessQualificationInfo] = [] {
        didSet {
            tblInsideLook.reloadData()
        }
    }
    private var insideLookResponse: [ILCAddRequest] = [] {
        didSet {
            tblInsideLook.reloadData()
        }
    }

    var myFacilityAddRequest: MyFacilityAddRequest?
    var screenName:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        if !screenName.isEmpty {
           lbl_NavigationTitle.text = screenName
        }
        
        tblInsideLook.backgroundColor = UIColor.clear
        
        if let existingInsideLookItems = myFacilityAddRequest?.facility_inside_look_list {
            insideLookItems = existingInsideLookItems
        }
        
        presenter.getInsideLook { [weak self] response in
            if let ilcResponse = response?.data {
                self?.insideLookResponse = ilcResponse
            }
        }

        if !isEditingFacility {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
}

extension MyFacilitiesAddInsideLookListingViewController {
    private func isFormValid() -> Bool{

        if (UserDefault.getFID() != nil && (UserDefault.getFID()?.length)! > 0  && myFacilityAddRequest?.facility_id == nil){
            myFacilityAddRequest?.facility_id = UserDefault.getFID() ?? ""
        } else if (myFacilityAddRequest?.facility_id == nil) {
            myFacilityAddRequest?.facility_id = ""
        }
                
        myFacilityAddRequest?.pageid = NSNumber(integerLiteral: formNumber)
        myFacilityAddRequest?.facility_inside_look_list = insideLookItems

        return true
    }
    
    private func openNextScr() {
        addInstructionalContentVC = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesAddInstructionalContentListingViewController.nameOfClass) as MyFacilitiesAddInstructionalContentListingViewController
        addInstructionalContentVC?.myFacilityAddRequest = myFacilityAddRequest
        addInstructionalContentVC?.isEditingFacility = isEditingFacility
        navigationController?.pushViewController(addInstructionalContentVC!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension MyFacilitiesAddInsideLookListingViewController {
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isUpdate {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let icDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyFacilitiesDetailViewController }
            if let icDetailVC = icDetailVC as? MyFacilitiesDetailViewController {
                icDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(icDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension MyFacilitiesAddInsideLookListingViewController: UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return insideLookItems.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AddMoreCell.nameOfClass) as! AddMoreCell
            
            cell.textField.placeholder = "Link"
            cell.delegate = self
            cell.refTableView = tableView
            
            let listing = insideLookResponse.map { ServiceListingList(id: $0.look_id, title: $0.look_name) }
            cell.updateDropDown(listInfo: listing)

            cell.backgroundColor = UIColor.clear
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleDescView.nameOfClass) as! TitleDescView
            
            cell.delegate = self
            cell.refTableView = tableView
            
            cell.lblTitle.text = insideLookItems[indexPath.row - 1].name
            cell.lblTitleDesc.text = insideLookItems[indexPath.row - 1].desc
            cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
            
            cell.backgroundColor = UIColor.clear
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension MyFacilitiesAddInsideLookListingViewController: AddMoreCellDelegate {
    func newText(text: String, textDesc: String, textRoleDesc: String, refTableView: UITableView?, selectedServiceListingList: ServiceListingList?) {
        
        let insideLookItem = BusinessQualificationInfo()
        insideLookItem.name = text
        insideLookItem.desc = textDesc
        
        if textDesc.isEmpty {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_primary_service_type.getLocalized(), buttonTitle: nil, controller: nil)
        } else if let serviceList = selectedServiceListingList {
            insideLookItem.id = serviceList.id
            insideLookItem.name = serviceList.title

            if insideLookItems.count < HelperConstant.minimumBlocks {
                if insideLookItems.map({ $0.id }).contains(serviceList.id) {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_service_added_selection.getLocalized(), buttonTitle: nil, controller: nil)
                } else {
                    isUpdate = true
                    insideLookItems.append(insideLookItem)
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_facility_service_selection.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}

extension MyFacilitiesAddInsideLookListingViewController: TitleDescViewDelegate {
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?) {
        isUpdate = true
        if let index = cellIndex?.row, insideLookItems.count > index {
            insideLookItems.remove(at: index)
        }
    }
}

extension MyFacilitiesAddInsideLookListingViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        tblInsideLookHeight.constant = size.height
    }
}

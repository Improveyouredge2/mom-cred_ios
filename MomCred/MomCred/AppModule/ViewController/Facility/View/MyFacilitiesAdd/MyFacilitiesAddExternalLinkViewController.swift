//
//  MyFacilitiesAddExternalLinkViewController.swift
//  MomCred
//
//  Created by consagous on 09/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

// Page 14


/**
 * MyFacilitiesAddExternalLinkViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class MyFacilitiesAddExternalLinkViewController : LMBaseViewController{
    
    fileprivate let formNumber = 9
    
    // Event
    //    @IBOutlet weak var tableViewSpecificTarget: UITableView!
    @IBOutlet weak var tableViewExternalLink: UITableView!
    
    //    @IBOutlet weak var constraintstableViewSpecificTargetHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintstableViewExternalLinkHeight: NSLayoutConstraint!
    
    @IBOutlet weak var inputLink: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var viewExternalLinkTableView: UIView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingFacility: Bool = false

    fileprivate var specificTargetList:[String] = []
    fileprivate var externalLinkDetailList: [MyFacilityExternalLinkItem] = []
    fileprivate var isUpdate = false
    fileprivate var myFacilitiesAddDocumentViewController:MyFacilitiesAddDocumentViewController?
    fileprivate var presenter = MyFacilitiesAddExternalLinkPresenter()
    
    var myFacilityAddRequest:MyFacilityAddRequest?
    var screenName:String = ""
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        //        self.tableViewSpecificTarget.backgroundColor = UIColor.clear
        
        if(self.screenName.length > 0){
            self.lbl_NavigationTitle.text = self.screenName
        }
        
        self.setScreenData()

        if !isEditingFacility {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.myFacilitiesAddDocumentViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        //        if(self.tableViewSpecificTarget.contentSize.height > 0){
        //            if((self.specificTargetList.count) == 0){
        //                self.constraintstableViewSpecificTargetHeight?.constant = AddMoreCell.inputViewCellSize
        //            } else {
        //                self.constraintstableViewSpecificTargetHeight?.constant = self.tableViewSpecificTarget.contentSize.height
        //            }
        //
        //            self.updateTableViewHeight(tableView: tableViewSpecificTarget)
        //        }
        
        if(self.externalLinkDetailList.count == 0){
            self.viewExternalLinkTableView.isHidden = true
        } else {
            self.viewExternalLinkTableView.isHidden = false
        }
        
        if(self.tableViewExternalLink.contentSize.height > 0){
            self.tableViewExternalLink.scrollToBottom()
            self.constraintstableViewExternalLinkHeight?.constant = self.tableViewExternalLink.contentSize.height
            
            self.updateTableViewHeight(tableView: self.tableViewExternalLink)
        }
        
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        //        if(tableViewSpecificTarget == tableView){
        //
        //            UIView.animate(withDuration: 0, animations: {
        //                self.tableViewSpecificTarget.layoutIfNeeded()
        //            }) { (complete) in
        //                var heightOfTableView: CGFloat = 0.0
        //                // Get visible cells and sum up their heights
        //                let cells = self.tableViewSpecificTarget.visibleCells
        //                for cell in cells {
        //                    heightOfTableView += cell.frame.height
        //                }
        //                // Edit heightOfTableViewConstraint's constant to update height of table view
        //                self.constraintstableViewSpecificTargetHeight.constant = heightOfTableView
        //            }
        //        } else
        if(tableViewExternalLink == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewExternalLink.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewExternalLink.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewExternalLinkHeight.constant = heightOfTableView
            }
        }
    }
}


extension MyFacilitiesAddExternalLinkViewController{
    fileprivate func setScreenData(){
        
        self.inputLink.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Link ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        
        if let existingLinks: [MyFacilityExternalLinkItem] = myFacilityAddRequest?.facility_ext_link?.serviceExternalLinks {
            externalLinkDetailList = existingLinks
            tableViewExternalLink.reloadData()
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        var message = ""
        
//        if(self.externalLinkDetailList != nil && self.externalLinkDetailList.count == 0){
//            message = LocalizationKeys.error_add_external_link.getLocalized()
//            self.showBannerAlertWith(message: message, alert: .error)
//            return false
//        }
        
        if(UserDefault.getFID() != nil && (UserDefault.getFID()?.length)! > 0  && self.myFacilityAddRequest?.facility_id == nil){
            self.myFacilityAddRequest?.facility_id = UserDefault.getFID() ?? ""
        } else if(self.myFacilityAddRequest?.facility_id == nil){
            self.myFacilityAddRequest?.facility_id = ""
        }
        
        self.myFacilityAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if (externalLinkDetailList.count > 0){
            let externalLink = MyFacilityExternalLink()
            externalLink.serviceExternalLinks = externalLinkDetailList
            
            myFacilityAddRequest?.facility_ext_link = externalLink
        } else {
            myFacilityAddRequest?.facility_ext_link = nil
        }
        
        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.myFacilitiesAddDocumentViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesAddDocumentViewController.nameOfClass) as MyFacilitiesAddDocumentViewController
        
        self.myFacilitiesAddDocumentViewController?.myFacilityAddRequest = self.myFacilityAddRequest
        self.navigationController?.pushViewController(self.myFacilitiesAddDocumentViewController!, animated: true)
    }
}

//MARK:- Button Action method implementation
extension MyFacilitiesAddExternalLinkViewController{
    @IBAction func methodAddExternalLinkAction(_ sender: UIButton){
        
        if(!((self.inputLink.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: self.inputLink.text() ?? ""))){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_invalid_url.getLocalized(), buttonTitle: nil, controller: nil)
            return
        }
        
        if let link = inputLink.text()?.trim(), !link.isEmpty {
            self.isUpdate = true
            let externalLinkDetailInfo = MyFacilityExternalLinkItem()
            externalLinkDetailInfo.links = [link]
            externalLinkDetailInfo.description = self.textViewDesc.text
            
            if(self.externalLinkDetailList.count < HelperConstant.minimumBlocks){
                externalLinkDetailList.append(externalLinkDetailInfo)
                
                self.specificTargetList = []
                self.inputLink.input.text = ""
                self.textViewDesc.text = ""
                
                if(self.externalLinkDetailList.count == 1){
                    self.constraintstableViewExternalLinkHeight?.constant = 10
                }
                
                self.tableViewExternalLink.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let icDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is MyFacilitiesDetailViewController }
            if let icDetailVC = icDetailVC as? MyFacilitiesDetailViewController {
                icDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(icDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension MyFacilitiesAddExternalLinkViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        if(tableView == tableViewExternalLink){
            return externalLinkDetailList.count
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkTableViewCell.nameOfClass) as! ExternalLinkTableViewCell
        
        if(externalLinkDetailList.count > indexPath.row){
            let objInfo: MyFacilityExternalLinkItem = externalLinkDetailList[indexPath.row]
            
            cell.cellIndex = indexPath
            cell.delegate = self
            cell.lblLink.text = objInfo.links?.first
            cell.lblDesc.text = objInfo.description
        }
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension MyFacilitiesAddExternalLinkViewController: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        self.isUpdate = true
    }
}

extension MyFacilitiesAddExternalLinkViewController : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        self.isUpdate = true
    }
}

extension MyFacilitiesAddExternalLinkViewController:ExternalLinkTableViewCellDelegate{
    
    func removeExternalLinkInfo(cellIndex:IndexPath?){
        
        self.isUpdate = true
        if(self.externalLinkDetailList.count > 0){
            self.externalLinkDetailList.remove(at: cellIndex?.row ?? 0)
            self.tableViewExternalLink.reloadData()
            
            if(externalLinkDetailList.count == 0){
                self.viewExternalLinkTableView.isHidden = true
            }
        }
    }
}

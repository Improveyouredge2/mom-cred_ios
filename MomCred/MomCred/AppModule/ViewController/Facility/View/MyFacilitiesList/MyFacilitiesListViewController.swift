//
//  MyFacilitiesListViewController.swift
//  MomCred
//
//  Created by consagous on 11/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class MyFacilityTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: ImageLayerSetup!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
}

class MyFacilitiesListViewController: LMBaseViewController{
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tbServiceList: UITableView!
    
    @IBOutlet weak var lblNoRecordFound: UILabel!
    
    private var myFacilitiesAddOverviewViewController: MyFacilitiesAddOverviewViewController?
    private var facilitiesDetailViewController: FacilitiesDetailViewController?
    private var myFacilitiesDetailViewController: MyFacilitiesDetailViewController?
    private var menuArray: [HSMenu] = []
    private var presenter = MyFacilitiesListPresenter()
    
    var personnelResponse: [MyFacilityAddRequest]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter.connectView(view: self)
        
        tbServiceList.estimatedRowHeight = 100
        tbServiceList.rowHeight = UITableView.automaticDimension
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.add_new.getLocalized())
        
//        if(UserDefault.getPID() != nil && (UserDefault.getPID()?.length)! > 0){
//            let menu2 = HSMenu(icon: nil, title: LocalizationKeys.complete_pending.getLocalized())
//            menuArray = [menu1,menu2]
//        }  else{
            menuArray = [menu1]
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutSubviews()
        self.myFacilitiesAddOverviewViewController = nil
        self.facilitiesDetailViewController = nil
        self.myFacilitiesDetailViewController = nil
        
        self.presenter.getPersonalServiceList()
    }
    
}

extension MyFacilitiesListViewController {
    func refreshScr(){
        if(self.tbServiceList != nil){
            self.tbServiceList.reloadData()
        }
    }
}

extension MyFacilitiesListViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        self.facilitiesDetailViewController = nil
        if(index == 0){
            
            UserDefault.removeFID()
            
            self.myFacilitiesAddOverviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesAddOverviewViewController.nameOfClass)
            self.navigationController?.pushViewController(self.myFacilitiesAddOverviewViewController!, animated: true)
        } else if(index == 1){
            self.facilitiesDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: FacilitiesDetailViewController.nameOfClass) as FacilitiesDetailViewController
            self.navigationController?.pushViewController(self.facilitiesDetailViewController!, animated: true)
        }
    }
}

extension MyFacilitiesListViewController{
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension MyFacilitiesListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return self.arrNoteList.count
        return self.personnelResponse != nil ? (self.personnelResponse?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = personnelResponse?[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MyFacilityTableViewCell.nameOfClass, for: indexPath) as! MyFacilityTableViewCell
        
        cell.lblTitle.text = data?.facility_name
        cell.lblLocation.text = data?.facility_description
        cell.imgView.image = #imageLiteral(resourceName: "profile")
        if let mediaInfo = data?.mediabusiness?.first, let thumb = mediaInfo.thumb?.trim(), let url = URL(string: thumb) {
            cell.imgView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "profile"))
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        if(role_id == AppUser.ServiceProvider.rawValue){
            
//
            self.myFacilitiesDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesDetailViewController.nameOfClass) as MyFacilitiesDetailViewController
            
            if(self.myFacilitiesDetailViewController != nil){
                self.myFacilitiesDetailViewController?.myFacilityAddRequest = self.personnelResponse?[indexPath.row]
                self.navigationController?.pushViewController(self.myFacilitiesDetailViewController!, animated: true)
            }
            
        } else {
            self.facilitiesDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: FacilitiesDetailViewController.nameOfClass) as FacilitiesDetailViewController
            
            if(self.facilitiesDetailViewController != nil){
                //            self.personnelDetailViewController?.serviceAddRequest = self.myServiceList?[indexPath.row]
                self.navigationController?.pushViewController(self.facilitiesDetailViewController!, animated: true)
            }
        }
    }
}

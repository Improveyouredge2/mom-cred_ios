//
//  BIAddSocialMediaPresenter.swift
//  MomCred
//
//  Created by consagous on 21/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  BIAddSocialMediaPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BIAddSocialMediaPresenter {
    private weak var view: BIAddLocationPresentable!

    func connectView(view: BIAddLocationPresentable) {
        self.view = view
    }
}

extension BIAddSocialMediaPresenter{
    func submitData(callback:@escaping (_ status:Bool, _ response: BIAddResponse?, _ message: String?) -> Void){
        BIServiceStep1.updateData(biRequest: view.biAddRequest, imageList: nil, callback: { (status, response, message) in
            DispatchQueue.main.async {
                Spinner.hide()
                if !status {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
                callback(status, response, message)
            }
        })
    }
}

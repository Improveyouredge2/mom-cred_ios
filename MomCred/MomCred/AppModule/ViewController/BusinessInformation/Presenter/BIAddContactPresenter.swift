//
//  BIAddContactPresenter.swift
//  MomCred
//
//  Created by consagous on 21/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

//struct User:Codable
//{
//    var firstName:String
//    var lastName:String
//    var country:[String]
//    
//    init() {
//        firstName = ""
//        lastName = ""
//        country = []
//    }
//    
//    enum CodingKeys: String, CodingKey {
//        case firstName = "first_name"
//        case lastName = "last_name"
//        case country
//    }
//}

/**
 *  BIAddContactPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BIAddContactPresenter {
    
    var view:BIAddContactViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: BIAddContactViewController) {
        self.view = view
        
//        var user = User()
//        user.firstName = "Bob"
//        user.lastName = "and Alice"
//        user.country = ["Cryptoland", "Cryptoland1"]
//
//        let jsonData = try! JSONEncoder().encode(user)
//        let jsonString = String(data: jsonData, encoding: .utf8)!
//        print(jsonString)
    }
}

extension BIAddContactPresenter{
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: BIAddResponse?, _ message: String?) -> Void){
        
        BIServiceStep1.updateData(biRequest: view.biAddRequest, imageList: nil, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                    
                }
            }
        })
    }
    
    func getPersonalServiceList(){
        PersonnelService.getPersonnelService(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
//                    Helper.sharedInstance.hideProgressHudView()
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil && (response?.data?.count)! > 0){
                        let personnelList = response?.data ?? []
                        
                        if(personnelList.count > 0){
                            self.view.personnelList = personnelList.compactMap { personalItem in
                                if let id = personalItem.personal_id, let title = personalItem.personal_name, !title.trim().isEmpty {
                                    return ServiceListingList(id: id, title: title)
                                }
                                return nil
                            }
                        }
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
//                    Helper.sharedInstance.hideProgressHudView()
                    Spinner.hide()
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}

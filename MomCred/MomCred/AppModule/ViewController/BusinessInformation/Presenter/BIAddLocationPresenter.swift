//
//  BIAddLocationPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 16/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  BIAddLocationPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */

protocol BIAddLocationPresentable: class {
    var biAddRequest: BIAddRequest? { get set }
}

class BIAddLocationPresenter {
    
    var view: BIAddLocationPresentable! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: BIAddLocationPresentable) {
        self.view = view
    }
}

extension BIAddLocationPresenter {
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func submitData(callback:@escaping (_ status:Bool, _ response: BIAddResponse?, _ message: String?) -> Void){
        
        BIServiceStep1.updateData(biRequest: view.biAddRequest, imageList: nil, callback: { (status, response, message) in

            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()

                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)

                    callback(status, response, message)

                }
            }
        })
    }
}

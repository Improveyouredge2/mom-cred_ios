//
//  BIServiceStep1.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import Alamofire

/**
 *  BIServiceStep1 is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BIServiceStep1{
    
    /**
     *  Method to get Forms Option Parent listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func getBIDetail(callback:@escaping (_ status:Bool, _ response: BIResponse?, _ message: String?) -> Void) {
        
        // GET
        // Convert Model request object into JSONString
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_BUSINESS_DETAIL, header: APIHeaders().getDefaultHeaders(), strJSON: nil)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:BIResponse? = BIResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res?.toJSON() ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method to get Forms Option Parent listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func getParentListing(callback:@escaping (_ status:Bool, _ response: ParentListingResponse?, _ message: String?) -> Void) {
        
        // GET
        // Convert Model request object into JSONString
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_PARENT_LIST, header: APIHeaders().getDefaultHeaders(), strJSON: nil)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:ParentListingResponse? = ParentListingResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method to get Forms Option Child listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func getChildListing(requestData: ChildListingRequest?, callback:@escaping (_ status:Bool, _ response: ChildListingResponse?, _ message: String?) -> Void) {
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_CHILD_LIST, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:ChildListingResponse? = ChildListingResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method to add Child listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func addChildListing(requestData: AddListingModelRequest?, callback:@escaping (_ status:Bool, _ response: AddListingModelResponse?, _ message: String?) -> Void) {
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_ADD_CHILD_LIST, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:AddListingModelResponse? = AddListingModelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method to add Child listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func getCategoryListing(requestData: GetListingCategoryModelRequest?, callback:@escaping (_ status:Bool, _ response: GetListingCategoryModelResponse?, _ message: String?) -> Void) {
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_CATEGORY_LIST, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:GetListingCategoryModelResponse? = GetListingCategoryModelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method upload screen information on server with profile data and user profiel image.
     *
     *  @param key profile information and selected image for user profile. Callback method to update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func updateData(biRequest: BIAddRequest?, imageList:[UploadImageData]?, callback:@escaping (_ status:Bool, _ response: BIAddResponse?, _ message: String?) -> Void) {
        
        // MultiForm
        let jsonArray = biRequest?.toJSON()
        
        //TODO: update qpi request
        let reqMultiForm = APIManager().sendPostMultiFormRequest(urlString: APIKeys.API_ADD_BUSINESS_ALL, header: APIHeaders().getDefaultHeaders(), formDataParameter: jsonArray, uploadImageList: imageList)

        // API Calling with URLRequest
        APIManager.request(urlRequest: reqMultiForm, isDisplayLoader: false) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:BIAddResponse? = BIAddResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method upload screen information on server with profile data and user profiel image.
     *
     *  @param key profile information and selected image for user profile. Callback method to update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func updateAdditionalInfoData(businessAdditionalDocumentInfo: BusinessAdditionalDocumentInfo?, imageList:[UploadImageData]?, callback:@escaping (_ status:Bool, _ response: ImageModelResponse?, _ message: String?) -> Void) {
        
        
        // MultiForm
        var jsonArray:[String:Any] = businessAdditionalDocumentInfo?.toJSON() ?? [:]
        
        jsonArray.updateValue("\(imageList![0].imageType!)", forKey: "media_extension")
        jsonArray.updateValue("\(imageList![0].mediaCategory!)", forKey: "media_category")
        
        
        let reqMultiForm = APIManager().sendPostMultiFormRequest(urlString: APIKeys.API_UPLOAD_IMAGE, header: APIHeaders().getDefaultHeaders(), formDataParameter: jsonArray, uploadImageList: imageList)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqMultiForm, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:ImageModelResponse? = ImageModelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    
    /**
     *  Method upload screen information on server with profile data and user profiel image.
     *
     *  @param key profile information and selected image for user profile. Callback method to update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func updateData(imageList:[UploadImageData]?, callback:@escaping (_ status:Bool, _ response: ImageModelResponse?, _ message: String?) -> Void) {
        
        // Update image with Alomefire
        let jsonArray = ["busi_id":"\(imageList![0].id!)","name":"\(imageList![0].imageName!)","media_extension":"\(imageList![0].imageType!)","media_category":"\(imageList![0].mediaCategory!)", "media_service":"\(imageList![0].media_service!)"]
        
        // MultiForm
        let reqMultiForm = APIManager().sendPostMultiFormRequest(urlString: APIKeys.API_UPLOAD_IMAGE, header: APIHeaders().getDefaultImageHeaders(), formDataParameter: jsonArray, uploadImageList: imageList)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqMultiForm, isDisplayLoader: false) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:ImageModelResponse? = ImageModelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    static func call_Api_Add_PostWithVideo(_ videoURL : URL?, request : [String:Any],fileName:String, _ fileData:Data, fileNameSnap:String,snapShotData : Data,callback:@escaping (_ status:Bool, _ response: ImageModelResponse?, _ message: String?) -> Void){
        
        Spinner.show()
        
        //let strUrl = "https://talentbegins.consagous.co.in/webservices/posts/posts_add"
        let strUrl = "\(BASEURLs.baseURL + APIKeys.API_UPLOAD_IMAGE)"
        
        let strAPIUrl = URL(string: strUrl)!
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if videoURL == nil {
//                multipartFormData.append(fileData as Data, withName: "busi_img", fileName: fileName, mimeType: MIME.VIDEO_MP4)
                multipartFormData.append(fileData as Data, withName: "busi_img", fileName: fileName, mimeType: MIME.VIDEO_MOV)
            } else {
//                multipartFormData.append(videoURL!, withName: "busi_img", fileName: fileName, mimeType: MIME.VIDEO_MP4)
                multipartFormData.append(videoURL!, withName: "busi_img", fileName: fileName, mimeType: MIME.VIDEO_MOV)
            }
            
            // TODO: Testing
//            multipartFormData.append(fileData as Data, withName: "busi_img", fileName: fileNameSnap, mimeType: MIME.IMAGE_PNG)
            
//            multipartFormData.append(snapShotData as Data, withName: "post_asset_thumb", fileName: "\(fileName)snapshot.PNG", mimeType: MIME.IMAGE_PNG)
            multipartFormData.append(snapShotData as Data, withName: "snapshot", fileName: fileNameSnap, mimeType: MIME.IMAGE_PNG)
            
            //for (key, value) in request.toJSON() {
            for (key, value) in request {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, to: strAPIUrl,headers: APIHeaders().getDefaultHeaders())
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    DispatchQueue.main.async {
                        debugPrint(Progress)
                    }
                    
                })
                
                upload.responseJSON { response in
                    if(response.result.value != nil){
                        let responseDict :NSDictionary = response.result.value! as! NSDictionary
                        let  res:ImageModelResponse? = ImageModelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict as AnyObject)
                        Spinner.hide()
                        callback(((res?.status) != nil), res, res?.message)
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                callback(false, nil, encodingError.localizedDescription)
                DispatchQueue.main.async {
                    Spinner.hide()
                }
            }
            
        }
    }
    
    // Upload multi-form json object array with alamofire
    //func uploadDataToServer(endPoint: String, dataToUpload: [[String: Any]], completionHandler:@escaping (Bool) -> ()){
    class func uploadDataToServer(endPoint: String, dataToUpload: [String: Any], completionHandler:@escaping (Bool) -> ()){
        
        let url = "\(BASEURLs.baseURL + APIKeys.API_ADD_BUSINESS_ALL)"
        
        var request = URLRequest(url: URL.init(string: url)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = APIHeaders().getDefaultHeaders()
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let dataToSync = dataToUpload
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataToSync)
        
        Alamofire.request(request).responseJSON{ (response) in
            
            print("Success: \(response)")
            switch response.result{
            case .success:
                let statusCode: Int = (response.response?.statusCode)!
                switch statusCode{
                case 200:
                    completionHandler(true)
                    break
                default:
                    completionHandler(false)
                    break
                }
                break
            case .failure:
                completionHandler(false)
                break
            }
        }
    }
    
    // Delete media
//
    /**
     *  Method to add Child listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func deleteMediaInfo(requestData: DeleteMediaRequest?, callback:@escaping (_ status:Bool, _ response: DeleteMediaModelResponse?, _ message: String?) -> Void) {
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_DELETE_MEDIA, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:DeleteMediaModelResponse? = DeleteMediaModelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    
    /**
     *  Method to add Child listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func deleteIndividualMediaInfo(requestData: DeleteMediaRequest?, callback:@escaping (_ status:Bool, _ response: DeleteMediaModelResponse?, _ message: String?) -> Void) {
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_DELETE_MEDIA_INDIVIDUAL, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:DeleteMediaModelResponse? = DeleteMediaModelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

//
//  BusinessInformationModel.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper
import GoogleMaps

///********************************/
////MARK:- Serialize Request
///*******************************/

/**
 *  BIAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class BIAddRequest: APIResponse {
    
    var busi_id:String?
    
    // Page 1
    var busi_title:String?
    var busi_desc:String?
    var busi_statement:String?
    var busi_service_provider:String?
    var busi_service_provider_title:String? // In Response
    var busi_service_provider_type:String?
    var busi_service_provider_type_title:String? // In Response
    var busi_entity:String?
    var busi_entity_title: String? // In Response
    var manualEntry:String?
    var business_donation:String?
    var business_credit_syestem:String?
    var business_calenderlisting:String?
    
    
    
    // Page 2
    var location_exclusive_travel:String?
    var location_name:String?
    var location_address:String?
    var location_city:String?
    var location_state:String?
    var location_lat:String?
    var location_long:String?
    var location_type:String?
    var location_ownership:String?
    var location_classification:[String]?
    var location_speciality:[String]?
    var location_busi_hours:[BusniessHour]?
    
    var location_exclusive_travel_sec:String?
    var location_name_sec:String?
    var location_address_sec:String?
    var location_city_sec:String?
    var location_state_sec:String?
    var location_lat_sec:String?
    var location_long_sec:String?
    var location_type_sec:String?
    var location_ownership_sec:String?
    var location_classification_sec:[String]?
    var location_speciality_sec:[String]?
    var location_busi_hours_sec:[BusniessHour]?

    
    // Page 3
    var busi_email:ContactInfo?
    var busi_email_sec:ContactInfo?
    var busi_phone:ContactInfo?
    var busi_phone_sec:ContactInfo?
    
    // Page 4
    var busi_weblink_list:[BusinessWebLinkInfo]?
    
    // Page 5
    var busi_sociallink_list:[BusinessSocialMediaInfo]?
    
    // Page 6
    var exceptional_services:String?
    var service_provider_classification:String?
    var primarily_exceptional:String?
    var exceptional_classification_offered:[String]?
    var exceptional_classification_offered_title:[String]? // In Response
    
    // Page 7
    var fieldServiceType:[BusinessFieldServiceCategoryInfo]?
    
    // Page 8
    var service_type:[String]?
    var purchasability:String?
    
    // Page 9
    var accreditations:[BusinessQualificationInfo]?
    
    // Page 10
    var affiliation:[BusinessQualificationInfo]?
    
    // Page 11
    var award:[BusinessQualificationInfo]?
    var deliveryExp:String?
    
    // Page 12
    var additional_link:[BusinessAdditionalInfo]?
    
    // For business detail display
    var doc:[BusinessAdditionalDocumentInfo]?
    var mediabusiness:[BusinessAdditionalDocumentInfo]?
    
    var pageid:NSNumber?
    
    // Extra parameter for Enuthaist
    var busitype:String?
    var provider_image:String?
    var rating:NSNumber?
    var totalservice:Int?
    var busi_user:String?
    var marker  : GMSMarker?

    var business_type: [ListingDataDetail]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init() {
        super.init()
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->BIAddRequest?{
        var biaddRequest:BIAddRequest?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<BIAddRequest>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    biaddRequest = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<BIAddRequest>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        biaddRequest = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return biaddRequest ?? nil
    }

    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map) {
        
        super.mapping(map: map)
        
        busi_id                     <- map["busi_id"]
        
        //Page 1
        busi_title                  <- map["busi_title"]
        busi_desc                   <- map["busi_desc"]
        busi_statement              <- map["busi_statement"]
        busi_service_provider       <- map["busi_service_provider"]
        busi_service_provider_title <- map["busi_service_provider_title"]
        busi_service_provider_type  <- map["busi_service_provider_type"]
        busi_service_provider_type_title    <- map["busi_service_provider_type_title"]
        busi_entity                 <- map["busi_entity"]
        busi_entity_title           <- map["busi_entity_title"]
        manualEntry                 <- map["add_new_busi_entity"]
        business_donation             <- map["business_donation"]
        business_credit_syestem       <- map["business_credit_syestem"]
        business_calenderlisting      <- map["business_calenderlisting"]

        //Page 2
        location_name                   <- map["location_name"]
        location_address                <- map["location_address"]
        location_city                   <- map["location_city"]
        location_state                  <- map["location_state"]
        location_lat                    <- map["location_lat"]
        location_long                   <- map["location_long"]
        location_type                   <- map["location_type"]
        location_ownership              <- map["location_ownership"]
        location_classification         <- map["location_classification"]
        location_speciality             <- map["location_speciality"]
        location_busi_hours             <- map["busi_hours"]
        location_name_sec               <- map["location_name_sec"]
        location_address_sec            <- map["location_address_sec"]
        location_city_sec               <- map["location_city_sec"]
        location_state_sec              <- map["location_state_sec"]
        location_lat_sec                <- map["location_lat_sec"]
        location_long_sec               <- map["location_long_sec"]
        location_type_sec               <- map["location_type_sec"]
        location_ownership_sec          <- map["location_ownership_sec"]
        location_classification_sec     <- map["location_classification_sec"]
        location_speciality_sec         <- map["location_speciality_sec"]
        location_busi_hours_sec         <- map["busi_hours_sec"]
        
        // Page 3
        busi_email                      <- map["busi_email"]
        busi_email_sec                  <- map["busi_email_sec"]
        busi_phone                      <- map["busi_phone"]
        busi_phone_sec                  <- map["busi_phone_sec"]
        
        // Page 4
        busi_weblink_list               <- map["website"]
        
        // Page 5
        busi_sociallink_list            <- map["social_media"]
        
        // Page 6
        service_provider_classification <- map["service_provider_classification"]
        exceptional_services       <- map["exceptional_services"]
        primarily_exceptional           <- map["primarily_exceptional"]
        exceptional_classification_offered            <- map["exceptional_classification_offered"]
        exceptional_classification_offered_title    <- map["exceptional_classification_offered_title"]
        
        // Page 7
        fieldServiceType                <- map["field_service_type"]
        
        // Page 8
        service_type                    <- map["service_type"]
        purchasability                  <- map["purchasability"]
        
        // Page 9
        accreditations                  <- map["accreditations"]
        
        // Page 10
        affiliation                     <- map["affiliation"]
        
        // Page 11
        award                           <- map["award"]
        deliveryExp                     <- map["year_exp"]
        
        // Page 12
        additional_link                 <- map["additional_link"]
        
        // Page 13 only in response, For business detail
        doc                             <- map["document"]
        
        // For business detail
        mediabusiness                   <- map["mediabusiness"]
        
        pageid                          <- map["pageid"]
        
        
        // Extra parameter for Enuthaist
        busitype        <- map["busitype"]
        provider_image  <- map["provider_image"]
        rating          <- map["business_rating"]
        totalservice    <- map["totalservice"]
        busi_user       <- map["busi_user"]
        
        business_type <- map["business_type"]
    }
}

class BusniessHour: Mappable {
    var index = -1
    var dayTitle:String? = ""
    var status:NSNumber? = NSNumber(booleanLiteral: false)
    var openTime:String? = ""
    var closeTime:String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        index       <- map["index"]
        status      <- map["status"]
        dayTitle    <- map["dayTitle"]
        openTime    <- map["openTime"]
        closeTime   <- map["closeTime"]
    }
}

class LocalBusinessLocationInfo: Mappable {
    var locationAddress = ""
    var locationState = ""
    var locationCity = ""
    var busniessHour: [BusniessHour] = [BusniessHour(), BusniessHour(), BusniessHour(), BusniessHour(), BusniessHour(), BusniessHour(), BusniessHour()]

    init() { }
    
    required init?(map: Map) { }

    func mapping(map: Map) {
        locationAddress <- map["locationAddress"]
        locationState <- map["locationState"]
        locationCity <- map["locationCity"]
        busniessHour <- map["busniessHour"]
    }
}

class BusinessLocationInfo: Mappable {
    var exclusiveTravel = false
    var locationType = false
    var locationBelong = false
    
    var locationName = ""
    var locationAddress = ""
    var locationState = ""
    var locationCity = ""
    var locationLat = ""
    var locationLng = ""
    
    var locationClassification:[String] = []
    var locationSpeciality:[String] = []
    
    var busniessHour:[BusniessHour] = [BusniessHour(),BusniessHour(),BusniessHour(),BusniessHour(),BusniessHour(),BusniessHour(),BusniessHour()]
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        
        exclusiveTravel         <- map["exclusiveTravel"]
        locationType            <- map["locationType"]
        locationBelong          <- map["locationBelong"]
        
        locationName            <- map["locationName"]
        locationAddress         <- map["locationAddress"]
        locationState           <- map["locationState"]
        locationCity            <- map["locationCity"]
        locationLat             <- map["locationLat"]
        locationLng             <- map["locationLng"]
        
        locationClassification  <- map["locationClassification"]
        locationSpeciality      <- map["locationSpeciality"]
        
        busniessHour            <- map["busniessHour"]
    }
}

class ContactInfo: Mappable {
    var name:String? = ""
    var desc:String? = ""
    var affilatedList:[String]? = []
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
    
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ContactInfo?{
        var contactInfo:ContactInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ContactInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    contactInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ContactInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        contactInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return contactInfo ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        name            <- map["name"]
        desc            <- map["desc"]
        affilatedList   <- map["affilatedList"]
    }
}

class BusinessWebLinkInfo: Mappable {
    var mainWebsite:NSNumber? = NSNumber(booleanLiteral: false)
    var webLinkTitle:String? = ""
    var webLinkDesc:String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        mainWebsite     <- map["mainWebsite"]
        webLinkTitle    <- map["webLinkTitle"]
        webLinkDesc     <- map["webLinkDesc"]
        
    }
}

class BusinessSocialMediaInfo: Mappable{
    var socialMediaName:String? = ""
    var webLinkTitle:String? = ""
    var webLinkDesc:String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        socialMediaName     <- map["socialMediaName"]
        webLinkTitle        <- map["webLinkTitle"]
        webLinkDesc         <- map["webLinkDesc"]
    }
}

class BusinessFieldServiceCategoryInfo: Mappable{
    var catField:String? = ""
    var catFieldName:String? = ""
    var specificField:String? = ""
    var add_new_specific_field:String? = ""
    var specificFieldName:String? = ""
    var classification:[String]? = []
    var technique:[String]? = []
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->BusinessFieldServiceCategoryInfo?{
        var businessFieldServiceCategoryInfo:BusinessFieldServiceCategoryInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<BusinessFieldServiceCategoryInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    businessFieldServiceCategoryInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<BusinessFieldServiceCategoryInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        businessFieldServiceCategoryInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return businessFieldServiceCategoryInfo ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        catField       <- map["catField"]
        specificField   <- map["specificField"]
        add_new_specific_field  <- map["add_new_specific_field"]
        classification  <- map["classification"]
        technique       <- map["technique"]
        catFieldName    <- map["catField_title"]
        specificFieldName   <- map["specificField_title"]
    }
}

class BusinessQualificationInfo: Mappable{
    var id:String?
    var name:String?
    var link:String?
    var desc:String?
    var descList : [String]?
    var type:DetailData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->BusinessQualificationInfo?{
        var businessQualificationInfo:BusinessQualificationInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<BusinessQualificationInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    businessQualificationInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<BusinessQualificationInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        businessQualificationInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return businessQualificationInfo ?? nil
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        id      <- map["id"]
        name    <- map["name"]
        link    <- map["link"]
        desc    <- map["desc"]
        descList    <- map["descList"]
        type    <- map["type"]
    }
}

class BusinessAdditionalInfo: Mappable{
    var name:String? = ""
    var desc:String? = ""
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        name    <- map["name"]
        desc    <- map["desc"]
    }
}

class BusinessAdditionalDocumentInfo: Mappable{
    var busi_id:String? = ""
    var name:String? = ""
    var desc:String? = ""
    var imageData:Data?
    var arrImages:[UploadImageData]?
    var arrImageIdList:[String]?
    var uploadStatus = false
    
    var media_id:String?
    var media_service:String?
    var imageurl:String?
    var imageurls:[DocUrl]?
    var thumb:String?
    var media_extension:String?
    var media_title:String?
    var media_desc:String?
    var count:NSNumber?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->BusinessAdditionalDocumentInfo?{
        var businessAdditionalDocumentInfo:BusinessAdditionalDocumentInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<BusinessAdditionalDocumentInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    businessAdditionalDocumentInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<BusinessAdditionalDocumentInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        businessAdditionalDocumentInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return businessAdditionalDocumentInfo ?? nil
    }
    
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        name            <- map["name"]
        desc            <- map["desc"]
        arrImageIdList  <- map["arrImageIdList"]
        busi_id         <- map["busi_id"]
        
        media_id        <- map["media_id"]
        media_service   <- map["media_service"]
        imageurl        <- map["imageurl"]
        
        if(imageurl == nil){
            imageurl        <- map["imageurls"]
        }
        
        imageurls       <- map["imageurls"]
        thumb           <- map["thumb"]
        media_extension <- map["media_extension"]
        media_title     <- map["media_title"]
        media_desc      <- map["media_desc"]
    }
}

class MultipleAdditionalDocumentInfo: Mappable{
    
    var busi_id:String? = ""
    var name:String? = ""
    var desc:String? = ""
    var imageData:Data?
    
    var arrImages:[UploadImageData]?
    var arrImageIdList:[String]?
    var uploadStatus = false
    
    var media_id:String?
    var media_service:String?
    var imageurl:String?
    var imageurls:[DocUrl]?
    var thumb:String?
    var media_extension:String?
    var media_title:String?
    var media_desc:String?
    var count:NSNumber?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->MultipleAdditionalDocumentInfo?{
        var multipleAdditionalDocumentInfo:MultipleAdditionalDocumentInfo?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<MultipleAdditionalDocumentInfo>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    multipleAdditionalDocumentInfo = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<MultipleAdditionalDocumentInfo>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        multipleAdditionalDocumentInfo = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return multipleAdditionalDocumentInfo ?? nil
    }
    
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        name            <- map["name"]
        desc            <- map["desc"]
        arrImageIdList  <- map["arrImageIdList"]
        busi_id         <- map["busi_id"]
        
        media_id        <- map["media_id"]
        media_service   <- map["media_service"]
        imageurl        <- map["imageurl"]
        thumb           <- map["thumb"]
        media_extension <- map["media_extension"]
        media_title     <- map["media_title[]"]
        media_desc      <- map["media_desc[]"]
        count           <- map["count[]"]
    }
}


class DocUrl: Mappable{
    
    var imageurls:String?
    var thumb:String?
    var media_extension:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init() {
        
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->DocUrl?{
        var docUrl:DocUrl?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<DocUrl>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    docUrl = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<DocUrl>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        docUrl = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return docUrl ?? nil
    }
    
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map) {
        imageurls        <- map["imageurls"]
        thumb           <- map["thumb"]
        media_extension <- map["media_extension"]
    }
}


class UploadImageData : Mappable{
    var id:String?
    var imageData:Data?
    var imageName:String?
    var imageKeyName:String?
    var imageType:String? // 1 for image  2 for video
    var imageThumbnailData:Data?
    var filePath:String?
    var folderName:String?
    var mediaCategory:String? // 1 for businessimage 2 for documents
    var imageUrl:String?
    var mimetype = "image/png"
    var media_service:String? // 1 for business image 2 for service image
    var media_extension:String?
    var status = false
    
//    override init(){
//
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//
//        self.id = aDecoder.decodeObject(forKey: "id") as? String
//        self.imageName = aDecoder.decodeObject(forKey: "imageName") as? String
//        self.imageKeyName = aDecoder.decodeObject(forKey: "imageKeyName") as? String
//        self.imageType = aDecoder.decodeObject(forKey: "imageType") as? String
//        self.filePath = aDecoder.decodeObject(forKey: "filePath") as? String
//        self.mediaCategory = aDecoder.decodeObject(forKey: "mediaCategory") as? String
//
//        self.documentTitle = aDecoder.decodeObject(forKey: "documentTitle") as? String
//        self.documentDesc = aDecoder.decodeObject(forKey: "documentDesc") as? String
//    }
//
//    func encode(with aCoder: NSCoder)
//    {
//        aCoder.encode(self.id, forKey: "id")
//        aCoder.encode(self.imageName, forKey: "imageName")
//        aCoder.encode(self.imageKeyName, forKey: "imageKeyName")
//        aCoder.encode(self.imageType, forKey: "imageType")
//        aCoder.encode(self.filePath, forKey: "filePath")
//        aCoder.encode(self.mediaCategory, forKey: "mediaCategory")
//
//        aCoder.encode(self.documentTitle, forKey: "documentTitle")
//        aCoder.encode(self.documentDesc, forKey: "documentDesc")
//    }
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init(){
        
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->UploadImageData?{
        var uploadImageData:UploadImageData?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<UploadImageData>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    uploadImageData = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<UploadImageData>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        uploadImageData = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return uploadImageData ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        
        id              <- map["media_id"]
        imageName       <- map["imageName"]
        imageKeyName    <- map["imageKeyName"]
        imageType       <- map["imageType"]
        filePath        <- map["filePath"]
        mediaCategory   <- map["mediaCategory"]
        media_service   <- map["media_service"]
        media_extension <- map["media_extension"]
    }
}

///********************************/
////MARK:- Serialize Response
///*******************************/

/**
 *  SignUpRespone is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class BIResponse : APIResponse {
    
    var data:[BIAddRequest]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->BIResponse?{
        var biResponse:BIResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<BIResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    biResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<BIResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        biResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return biResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
}

class BIAddResponse : APIResponse {
    
    var data:BIAddResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->BIAddResponse?{
        var biaddResponse:BIAddResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<BIAddResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    biaddResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<BIAddResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        biaddResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return biaddResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class BIAddResponseData: Mappable{
        var business_id : NSNumber?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->BIAddResponseData?{
            var biAddResponseData:BIAddResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<BIAddResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        biAddResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<BIAddResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            biAddResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return biAddResponseData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            business_id     <- map["business_id"]
            
        }
    }
}

/**
 *  AddListingModelResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class ImageModelResponse : APIResponse {
    
    var data:ImageModelResponseData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ImageModelResponse?{
        var imageModelResponse:ImageModelResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ImageModelResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    imageModelResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ImageModelResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        imageModelResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return imageModelResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
    
    /**
     *  AddListingData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class ImageModelResponseData: Mappable{
        var media_name:String?
        var media_id:NSNumber?
        var media_extension:String?
        var name:String?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ImageModelResponseData?{
            var imageModelResponseData:ImageModelResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<ImageModelResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        imageModelResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ImageModelResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            imageModelResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return imageModelResponseData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            media_name      <- map["media_name"]
            media_id        <- map["media_id"]
            name            <- map["name"]
            media_extension <- map["media_extension"]
        }
    }
}


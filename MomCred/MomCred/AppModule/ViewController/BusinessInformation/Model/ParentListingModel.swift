//
//  ParentListingModel.swift
//  MomCred
//
//  Created by Apple_iOS on 04/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

///********************************/
////MARK:- Serialize Request
///*******************************/


///********************************/
////MARK:- Serialize Response
///*******************************/

/**
 *  SignUpRespone is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class ParentListingResponse : APIResponse {
    
    var parentListingData:ParentListingData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ParentListingResponse?{
        var parentListingResponse:ParentListingResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ParentListingResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    parentListingResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ParentListingResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        parentListingResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return parentListingResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        parentListingData        <- map["data"]
    }
    
    /**
     *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class ParentListingData: Mappable{
        var service_provider : [ListingDataDetail]?
        var exceptional_classification : [ListingDataDetail]?
        var business_entity : [ListingDataDetail]?
        var services_types : [ListingDataDetail]?
        var fields:[ListingDataDetail]?
        var targetgoal:[ListingDataDetail]?
        var therapy:[ListingDataDetail]?
        var personaltype:[ListingDataDetail]?
        var facilitytype:[ListingDataDetail]?
        var insidelooktype:[ListingDataDetail]?
        var speciallisting:[ListingDataDetail]?
        var instructionalcontent:[ListingDataDetail]?
        var noninstructionbusitype: [ListingDataDetail]?
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ParentListingData?{
            var parentListingData:ParentListingData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<ParentListingData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        parentListingData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ParentListingData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            parentListingData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return parentListingData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            service_provider            <- map["service_provider"]
            exceptional_classification  <- map["exceptional_classification"]
            business_entity             <- map["business_entity"]
            services_types              <- map["services_types"]
            fields                      <- map["fields"]
            targetgoal                  <- map["targetgoal"]
            therapy                     <- map["therapy"]
            personaltype                <- map["personaltype"]
            facilitytype                <- map["facilitytype"]
            
            insidelooktype              <- map["insidelooktype"]
            speciallisting              <- map["speciallist"]
            instructionalcontent        <- map["instructionalcontent"]
            noninstructionbusitype      <- map["noninstructionbusitype"]
        }
    }
}

/**
 *  Sub-class SignUpResponseData is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
class ListingDataDetail: Mappable{
    var listing_id:String?
    var listing_title:String?
    var listing_user:String?
    var listing_parent:String?
    var listing_status:String?
    var listing_delete:String?
    var listing_type:String?
    var listing_category:String?
    var create_dt:String?
    var child:NSNumber?
//    var childListing:[ListingDataDetail]?
    
    var subcat:[ListingDataDetail]?
    
    var selectionStaus:Bool = false
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    init(){
        
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ListingDataDetail?{
        var listingDataDetail:ListingDataDetail?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ListingDataDetail>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    listingDataDetail = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ListingDataDetail>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        listingDataDetail = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return listingDataDetail ?? nil
    }
    
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map){
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func mapping(map: Map){
        
        listing_id          <- map["listing_id"]
        listing_title       <- map["listing_title"]
        listing_user        <- map["listing_user"]
        listing_parent      <- map["listing_parent"]
        listing_status      <- map["listing_status"]
        listing_delete      <- map["listing_delete"]
        listing_type        <- map["listing_type"]
        listing_category    <- map["listing_category"]
        create_dt           <- map["create_dt"]
        child               <- map["child"]
        subcat              <- map["subcat"]
    }
}

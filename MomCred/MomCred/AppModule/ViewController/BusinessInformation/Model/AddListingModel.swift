//
//  AddListingModel.swift
//  MomCred
//
//  Created by Apple_iOS on 05/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

///********************************/
////MARK:- Serialize Request
///*******************************/
/**
 *  AddListingModelRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class AddListingModelRequest: Mappable {
    
    var listing_title       :String?
    var listing_category    :String?
    var listing_parent      :String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    init(listing_title:String?,listing_category:String?,listing_parent:String?) {
        self.listing_title = listing_title
        self.listing_category = listing_category
        self.listing_parent = listing_parent
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        
    }
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func mapping(map: Map) {
        listing_title       <- map["listing_title"]
        listing_category    <- map["listing_category"]
        listing_parent      <- map["listing_parent"]
    }
}

///********************************/
////MARK:- Serialize Response
///*******************************/

/**
 *  AddListingModelResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class AddListingModelResponse : APIResponse {
    
    var data:AddListingData?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->AddListingModelResponse?{
        var addListingModelResponse:AddListingModelResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<AddListingModelResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    addListingModelResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<AddListingModelResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        addListingModelResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return addListingModelResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        data        <- map["data"]
    }
    
    /**
     *  AddListingData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    class AddListingData: Mappable{
        var listing_id:NSNumber?
        var listing_title:String?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->AddListingData?{
            var addListingData:AddListingData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<AddListingData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        addListingData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<AddListingData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            addListingData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return addListingData ?? nil
        }
        
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN100]
         */
        func mapping(map: Map){
            
            listing_id      <- map["listing_id"]
            listing_title   <- map["listing_title"]
            
        }
    }
}

//
//  ChildListingModel.swift
//  MomCred
//
//  Created by Apple_iOS on 04/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

///********************************/
////MARK:- Serialize Request
///*******************************/
/**
 *  ChildListingRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class ChildListingRequest: Mappable {
    
    var listing_id           :String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, first name, last name, email, address and token.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    init(listing_id:String?) {
        self.listing_id = listing_id
    }
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        
    }
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func mapping(map: Map) {
        listing_id  <- map["listing_id"]
    }
}

///********************************/
////MARK:- Serialize Response
///*******************************/

/**
 *  ChildListingResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */

class ChildListingResponse : APIResponse {
    
    var dataList:[ListingDataDetail]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ChildListingResponse?{
        var childListingResponse:ChildListingResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ChildListingResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    childListingResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ChildListingResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        childListingResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return childListingResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    override func mapping(map: Map){
        // TODO: Update key-value pair when socket response fetch
        super.mapping(map: map)
        dataList        <- map["data"]
    }
}

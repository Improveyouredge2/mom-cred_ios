//
//  BIAddAdditionalInformationLinkViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 26/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 * BIAddAdditionalInformationLinkViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BIAddAdditionalInformationLinkViewController : LMBaseViewController{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    
    @IBOutlet weak var businessInformationAdditionalLink:BusinessInformationAdditionalLink!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    fileprivate var businessAdditionalInfoList:[BusinessAdditionalInfo] = []
    
    var biAddRequest:BIAddRequest?
    fileprivate let formNumber = 12
    
    fileprivate var presenter = BIAddAdditionalInformationLinkPresenter()
    fileprivate var biaddAdditionalInformationDocumentViewController:BIAddAdditionalInformationDocumentViewController?
    
    var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.backgroundColor = UIColor.clear
        
        self.businessInformationAdditionalLink.namePlaceHolder = LocalizationKeys.txt_link.getLocalized()
        
        self.businessInformationAdditionalLink.textViewDesc.placeholder = LocalizationKeys.txt_description.getLocalized()
        
        businessInformationAdditionalLink.setScreenData()
        
        //presenter.connectView(view: self)
        
        // TODO: For testing
//        biAddRequest = BIAddRequest()
        
        presenter.connectView(view: self)
        
        self.setScreenData()

        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.biaddAdditionalInformationDocumentViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(businessAdditionalInfoList.count == 0){
            self.viewTableView.isHidden = true
        } else {
            self.viewTableView.isHidden = false
        }
        
        
        if(self.tableView.contentSize.height > 0){
            self.tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension BIAddAdditionalInformationLinkViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        if(self.biAddRequest != nil){
            
            if(self.biAddRequest?.additional_link != nil && (self.biAddRequest?.additional_link?.count)! > 0){
                self.businessAdditionalInfoList = self.biAddRequest?.additional_link ?? []
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        isUpdate = self.isUpdate
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.biaddAdditionalInformationDocumentViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddAdditionalInformationDocumentViewController.nameOfClass) as BIAddAdditionalInformationDocumentViewController
        
        self.biaddAdditionalInformationDocumentViewController?.biAddRequest = self.biAddRequest
        
        self.navigationController?.pushViewController(self.biaddAdditionalInformationDocumentViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0 && self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = ""
        }
        
        self.biAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
       
//        self.biAddRequest?.additional_link = self.businessAdditionalInfoList.toJSONString()
        self.biAddRequest?.additional_link = self.businessAdditionalInfoList ?? []
        
        return true
    }
}

extension BIAddAdditionalInformationLinkViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is BIDetailViewController }
            if let biDetailVC = biDetailVC as? BIDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension BIAddAdditionalInformationLinkViewController{
    
    @IBAction func methodAddAdditionalInfoAction(_ sender: UIButton){
        
        var isValid = true
        let businessAdditionalInfo = BusinessAdditionalInfo()
        
        if((self.businessInformationAdditionalLink.inputName.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: businessInformationAdditionalLink.inputName.text() ?? "")){
            businessAdditionalInfo.name = businessInformationAdditionalLink.inputName.text() ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_invalid_url.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        if((self.businessInformationAdditionalLink.textViewDesc.text?.length)! > 0){
            businessAdditionalInfo.desc = businessInformationAdditionalLink.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_website_invalid_url_desc.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(isValid && self.businessAdditionalInfoList.count < HelperConstant.minimumBlocks){
            
            self.isUpdate = true
            
            self.businessInformationAdditionalLink.inputName.input.text = ""
            
            self.businessInformationAdditionalLink.textViewDesc.text = ""
            self.businessAdditionalInfoList.append(businessAdditionalInfo)
            self.tableView.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension BIAddAdditionalInformationLinkViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return businessAdditionalInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationLinkCell.nameOfClass) as! BusinessInformationAddAdditionalInformationLinkCell
        
        let objInfo = businessAdditionalInfoList[indexPath.row]
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        cell.lblLinkName.text = objInfo.name
        cell.lblLinkDesc.text = objInfo.desc
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension BIAddAdditionalInformationLinkViewController:BusinessInformationAddAdditionalInformationLinkCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        self.isUpdate = true
        if(self.businessAdditionalInfoList.count > 0){
            self.businessAdditionalInfoList.remove(at: cellIndex?.row ?? 0)
            self.tableView.reloadData()
            
            if(businessAdditionalInfoList.count == 0){
                self.viewTableView.isHidden = true
            }
        }
    }
}

/**
 * BusinessLocationAddMoreCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol BusinessInformationAddAdditionalInformationLinkCellDelegate {
    func removeWebsiteInfo(cellIndex:IndexPath?)
}

class BusinessInformationAddAdditionalInformationLinkCell: UITableViewCell {
    @IBOutlet weak var lblLinkName : UILabel!
    @IBOutlet weak var lblLinkDesc : UILabel!
    @IBOutlet weak var btnRemove : UIButton!
    
    var cellIndex:IndexPath?
    var delegate:BusinessInformationAddAdditionalInformationLinkCellDelegate?
    
    @IBAction func methodCloseAction(_ sender: UIButton) {
        if(delegate != nil){
            delegate?.removeWebsiteInfo(cellIndex: cellIndex)
        }
    }
}

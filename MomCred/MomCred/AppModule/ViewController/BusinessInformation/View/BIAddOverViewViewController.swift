//
//  BIAddOverViewViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 04/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown
import AVKit

class BIAddOverViewViewController : LMBaseViewController{
    
    //MARK:- ADDED BY SHELEN
     @IBOutlet weak var btnCreditYes: UIButton!
     @IBOutlet weak var btnCreditNo: UIButton!
     @IBOutlet weak var btnDonationYes: UIButton!
     @IBOutlet weak var btnDonationNo: UIButton!
     @IBOutlet weak var btnCalenderYes: UIButton!
     @IBOutlet weak var btnCalenderNo: UIButton!
    
    
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn : UIImageView!
    @IBOutlet weak var documentImgCollectionView: UICollectionView!
    
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    
//    @IBOutlet weak var imgProfilePic : UIImageView!
//    @IBOutlet weak var imgPlayButton : UIImageView!
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var inputNameCustomEntry: RYFloatingInput!
    
    
    @IBOutlet weak var textViewDesc : KMPlaceholderTextView!
    @IBOutlet weak var textViewMissionStmt : KMPlaceholderTextView!
    @IBOutlet weak var btnTypeYes: UIButton!
    @IBOutlet weak var btnTypeNo: UIButton!
    @IBOutlet weak var lblUploadImageVideo: UILabel!
    
    @IBOutlet weak var dropDownOrganisationType: DropDown!
    @IBOutlet weak var dropDownOrganisationSubType: DropDown!
    @IBOutlet private weak var dropDownTitleHeight: NSLayoutConstraint!
    @IBOutlet private weak var dropDownTypeHeight: NSLayoutConstraint!
    @IBOutlet weak var dropDownType: DropDown!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    fileprivate var businessNewTypeView:BusinessAddNewTypeViewController?
    fileprivate var childListingResponse:ChildListingResponse?
    fileprivate var selectedServiceId:String = ""
    fileprivate var selectedChildId:String = ""
    fileprivate var imageData: Data?
    fileprivate var presenter = BIPresenter()
    fileprivate let formNumber = 1
    fileprivate var logoutViewController:PMLogoutView?
    fileprivate var selectedDocumentImages:UploadImageData?
    fileprivate var selectedIndex = -1
    fileprivate var defaultDocumentCollectionHeight:CGFloat = 0.00
    fileprivate var bilocationViewController:BILocationViewController?
    
    var isScreenDataReload = false
    
    var selectedBusinessTypeId:String = ""
    var arrDocumentImages = [UploadImageData]()
    var biAddRequest:BIAddRequest? = BIAddRequest()
    
    
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isScreenDataReload = true
        
        self.presenter.connectView(view: self)
        
        self.documentImgCollectionView.backgroundColor = UIColor.clear
        
        self.checkBusinessLocalImage()
        
        self.defaultDocumentCollectionHeight = self.constraintDocumentCollectionHeight.constant
        self.constraintDocumentCollectionHeight.constant = 0.0
        
        self.setScreenData()
        self.setYesNOStatusOnExtraAddedBtns()
        
        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.logoutViewController = nil
        self.bilocationViewController = nil
        if(isScreenDataReload){
            self.presenter.serverParentListingRequest()
        }
//        self.presenter.serverGetCategoryListingRequest(catId: "2")
        
    }
    
}

extension BIAddOverViewViewController{
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func pickedImage(fromImagePicker: UIImage, imageData: Data, mediaType:String, imageName:String, fileUrl: String?) {
        
        let imageDataTemp = UploadImageData()
        
        let theExt = (imageName as NSString).pathExtension
        
        if(theExt.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpeg") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame){
            imageDataTemp.imageType = "1"
        } else {
            imageDataTemp.imageType = "2"
        }
        
        if(biAddRequest?.busi_id != nil){
            imageDataTemp.id = biAddRequest?.busi_id ?? ""
        } else if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0){
            let busi_id = UserDefault.getBIID() ?? ""
            imageDataTemp.id = busi_id
        }
        
        imageDataTemp.imageData = imageData
        imageDataTemp.imageName = "\((self.arrDocumentImages.count + 1))\(imageName)"
        imageDataTemp.imageKeyName = "busi_img"
        imageDataTemp.filePath = fileUrl
        
        imageDataTemp.imageType = mediaType
        imageDataTemp.imageThumbnailData = fromImagePicker.pngData()
        
        // Check of video size
        if(imageDataTemp.imageType == "2"){
            if(Int(Helper.sharedInstance.checkVideoSize(reqData: imageData)) < HelperConstant.LIMIT_VIDEO_SIZE){
                
                self.imgProfile.image = fromImagePicker
                self.imgPlayBtn.isHidden = false
                
//                self.arrDocumentImages.append(imageDataTemp)
                
                var arrTempDocumentImages = [UploadImageData]()
                arrTempDocumentImages.append(imageDataTemp)
                for arrDocument in self.arrDocumentImages{
                    arrTempDocumentImages.append(arrDocument)
                }
                
                self.arrDocumentImages = arrTempDocumentImages
                
//                self.documentImgCollectionView.reloadData()
            } else {
                self.showBannerAlertWith(message: LocalizationKeys.error_file_size.getLocalized(), alert: .error)
            }
        } else {
            
            // Display image on main view
            self.imgProfile.image = fromImagePicker
            self.imgPlayBtn.isHidden = true
            
            self.arrDocumentImages.append(imageDataTemp)
//            self.documentImgCollectionView.reloadData()
        }
        
        if(self.arrDocumentImages.count > 0 && self.arrDocumentImages.count < HelperConstant.maximumProfileMedia){
            
            self.constraintDocumentCollectionHeight.constant = self.defaultDocumentCollectionHeight
            
            self.documentImgCollectionView.reloadData()
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
}


extension BIAddOverViewViewController{
    
    /**
     *  Check local image in business information id folder
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func checkBusinessLocalImage(){
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0){
            let busi_id = UserDefault.getBIID() ?? ""
            let folderName = "busi_\(busi_id)"
            if(PMFileUtils.directoryExistsAtPath(folderName)){
                
                let fileUrls:[URL]? = PMFileUtils.getFolderAllFileUrls(folderName: folderName)
                print("Urls: \(fileUrls ?? [])")
                
                if(fileUrls != nil && (fileUrls?.count)! > 0){
                    do {
                        for fileUrl in (fileUrls)! {
                            
                            let imageData = try Data(contentsOf: fileUrl)
                            
                            let theFileName = (fileUrl.absoluteString as NSString).lastPathComponent
                            let theExt = (fileUrl.absoluteString as NSString).pathExtension

                            let imageDataTemp = UploadImageData()
                            
                            imageDataTemp.imageData = imageData
                            imageDataTemp.imageName = theFileName
                            imageDataTemp.imageKeyName = "busi_img"
                            if(theExt.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpeg") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame){
                                imageDataTemp.imageType = "1"
                            } else {
                                imageDataTemp.imageType = "2"
                            }
                            
                            self.arrDocumentImages.append(imageDataTemp)
                            
                        }
                    } catch {
                        //                    print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
                        print("Error while enumerating files")
                    }
                    
                    if !arrDocumentImages.isEmpty {
                        self.documentImgCollectionView.reloadData()
                    }
                }
            }
        }
    }
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Business Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputNameCustomEntry.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Manual Entry ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        dropDownTitleHeight.priority = .defaultHigh
        dropDownTypeHeight.constant = 0
        self.inputNameCustomEntry.isHidden = true
        
        if(self.biAddRequest?.mediabusiness != nil && (self.biAddRequest?.mediabusiness?.count)! > 0){
            self.imgProfile.sd_setImage(with: URL(string: (self.biAddRequest?.mediabusiness![0].thumb ?? "")))
            
            // Check video on first pos
            if((self.biAddRequest?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.constraintDocumentCollectionHeight.constant = self.defaultDocumentCollectionHeight
            
            self.documentImgCollectionView.reloadData()
        }
        
        if(self.biAddRequest?.busi_title != nil){
            self.inputName.input.text = self.biAddRequest?.busi_title ?? ""
        }
        
        if(self.biAddRequest?.busi_desc != nil){
            self.textViewDesc.text = self.biAddRequest?.busi_desc ?? ""
        }
        
        if(self.biAddRequest?.busi_statement != nil){
            self.textViewMissionStmt.text = self.biAddRequest?.busi_statement
        }
        
        if(self.biAddRequest?.busi_service_provider_title != nil && self.biAddRequest?.busi_service_provider != nil){
            // TODO: Call child listing
            self.dropDownOrganisationType.text = self.biAddRequest?.busi_service_provider_title ?? ""
            
            self.selectedServiceId = self.biAddRequest?.busi_service_provider ?? ""
            self.presenter.serverChildListingRequest(parentId: self.selectedServiceId)
        }
        
        if(self.biAddRequest?.busi_service_provider_type_title != nil){
            self.dropDownOrganisationSubType.text = self.biAddRequest?.busi_service_provider_type_title ?? ""
            
            self.selectedChildId = self.biAddRequest?.busi_service_provider_type ?? ""
        }
        
        if(self.biAddRequest?.manualEntry != nil && (self.biAddRequest?.manualEntry?.length)! > 0){
            
            self.methodBusinessTypeAction(self.btnTypeYes)
            
            self.inputNameCustomEntry.input.text = self.biAddRequest?.manualEntry ?? ""
        } else if(self.biAddRequest?.busi_entity_title != nil && (self.biAddRequest?.busi_entity_title?.length)! > 0 && self.biAddRequest?.busi_entity != nil){
            self.dropDownType.text = self.biAddRequest?.busi_entity_title ?? ""
            self.selectedBusinessTypeId = self.biAddRequest?.busi_entity ?? ""
            
            self.methodBusinessTypeAction(self.btnTypeYes)
        }
        
        if(self.biAddRequest?.mediabusiness != nil && (self.biAddRequest?.mediabusiness?.count)! > 0){
            
//            self.arrDocumentImages
            for mediaBusiness in (self.biAddRequest?.mediabusiness)! {
                let mediaImage = UploadImageData()
                mediaImage.imageUrl = mediaBusiness.thumb ?? ""
                mediaImage.id = mediaBusiness.media_id ?? ""
                if let mediaExt = Int(mediaBusiness.media_extension ?? "0"){
                    if(mediaExt == 0 || mediaExt == 1){
                        mediaImage.imageType = "1"
                    } else {
                        mediaImage.imageType = "2"
                    }
                } else {
                    mediaImage.imageType = "1"
                }
                
                self.arrDocumentImages.append(mediaImage)
            }
            
            self.documentImgCollectionView.reloadData()
        }
        
        // Business Type
        ///////////////////////////////////////////////////////////
//        self.businessTypeList()
    }
    
    func orginationTypList(){
        // Organization list
        ////////////////////////////////////////////////////////////
        dropDownOrganisationType.optionArray = []
        dropDownOrganisationType.optionIds = []
        for listDetail in (BIPresenter.parentListingList?.parentListingData?.service_provider)! {
            dropDownOrganisationType.optionArray.append(listDetail.listing_title ?? "")
            dropDownOrganisationType.optionIds?.append(Int(listDetail.listing_id ?? "") ?? 0)
        }

        // The the Closure returns Selected Index and String
        dropDownOrganisationType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.selectedServiceId = "\(id)"
            
            self.presenter.serverChildListingRequest(parentId: self.selectedServiceId)
            
        }
        ///////////////////////////////////////////////////////////
        
//        // Business Type
//        ///////////////////////////////////////////////////////////
        self.businessTypeList()
    }
    
    func loadSubCatList(childListingResponse:ChildListingResponse?){
        
        if(childListingResponse != nil && childListingResponse?.dataList != nil && (childListingResponse?.dataList?.count)! > 0){
            self.childListingResponse = childListingResponse
            
            self.dropDownOrganisationSubType.optionArray = []
            self.dropDownOrganisationSubType.optionIds = []
            
            for childDetail in (childListingResponse?.dataList)!{
                
                self.dropDownOrganisationSubType.optionArray.append(childDetail.listing_title ?? "")
                self.dropDownOrganisationSubType.optionIds?.append(Int(childDetail.listing_id ?? "") ?? 0)
            }
            
            // The the Closure returns Selected Index and String
            self.dropDownOrganisationSubType.didSelect{(selectedText , index ,id) in
                print("Selected String: \(selectedText) \n index: \(index)")
                
                self.selectedChildId = "\(id)"
            }
        }
    }
    
    fileprivate func businessTypeList(){
        // Individual
        ////////////////////////////////////////////////////////////
        
        self.inputNameCustomEntry.isHidden = true
        
        dropDownType.optionIds = []
        if (BIPresenter.parentListingList?.parentListingData?.business_entity != nil){
            for listDetail in (BIPresenter.parentListingList?.parentListingData?.business_entity)! {
                dropDownType.optionArray.append(listDetail.listing_title ?? "")
                dropDownType.optionIds?.append(Int(listDetail.listing_id ?? "") ?? 0)
                
                if let listingId = listDetail.listing_id, listingId == selectedBusinessTypeId, let title = listDetail.listing_title {
                    dropDownType.text = title
                }
            }
        }
        
        
        dropDownType.optionArray.append("Custom Entry")
        dropDownType.optionIds?.append(1005)
        
        self.inputNameCustomEntry.isHidden = true
        // The the Closure returns Selected Index and String
        dropDownType.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            
            if(id == 1005){
                self.inputNameCustomEntry.isHidden = false
                self.selectedBusinessTypeId = ""
            } else {
                self.selectedBusinessTypeId = "\(id)"
                self.inputNameCustomEntry.isHidden = true
                self.inputNameCustomEntry.input.text = ""
            }
            
        }
        ////////////////////////////////////////////////////////////
    }
    
    fileprivate func openNewBusinessTypeAlert(){
        self.businessNewTypeView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BusinessAddNewTypeViewController.nameOfClass) as BusinessAddNewTypeViewController
        self.businessNewTypeView?.delegate = self
        self.businessNewTypeView?.modalPresentationStyle = .overFullScreen
        self.present(self.businessNewTypeView!, animated: true, completion: nil)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if(self.arrDocumentImages.count == 0){
            message = LocalizationKeys.error_select_business_image.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.inputName.text()?.trim().isEmpty)! {
            message = LocalizationKeys.error_select_business_name.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.textViewDesc.text?.trim().isEmpty)! {
            message = LocalizationKeys.error_enter_business_desc.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.textViewMissionStmt.text?.trim().isEmpty)! {
            message = LocalizationKeys.error_enter_mission_statement.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if(self.selectedServiceId.length == 0){
            message = LocalizationKeys.error_select_service_provider_type.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if(self.selectedChildId.length == 0){
            message = LocalizationKeys.error_select_service_provider_sub_cat_type.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if(self.btnTypeYes.isSelected && (self.selectedBusinessTypeId.length == 0 || self.inputNameCustomEntry.isHidden)){
            if(!self.inputNameCustomEntry.isHidden && (self.inputNameCustomEntry.text()?.trim().isEmpty)! ){
                message = LocalizationKeys.error_enter_busi_custom_entry.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            } else if(self.selectedBusinessTypeId.length == 0 && self.inputNameCustomEntry.isHidden){
                message = LocalizationKeys.error_select_business_type.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
        }
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0  && self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = ""
        }
        
        self.biAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        self.biAddRequest?.busi_title = self.inputName.text()?.trim() ?? ""
        self.biAddRequest?.busi_desc = self.textViewDesc.text?.trim() ?? ""
        self.biAddRequest?.busi_statement = self.textViewMissionStmt.text?.trim() ?? ""
        self.biAddRequest?.busi_service_provider = self.selectedServiceId
        self.biAddRequest?.busi_service_provider_type = self.selectedChildId
        
        // Check business type
        if(self.btnTypeYes.isSelected){
            if(!self.inputNameCustomEntry.isHidden){
                self.biAddRequest?.manualEntry = self.inputNameCustomEntry.text() ?? ""
            } else {
                self.biAddRequest?.busi_entity = self.selectedBusinessTypeId
            }
        } else {
//           self.biAddRequest.busi_entity = NSNumber(integerLiteral: 0)
        }

        return true
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        
        var isUpdate = false
        
        if(self.biAddRequest?.busi_title != (self.inputName.text()?.trim() ?? "")){
            isUpdate = true
        } else if(self.biAddRequest?.busi_desc != (self.textViewDesc.text?.trim() ?? "")){
            isUpdate = true
        } else if(self.biAddRequest?.busi_statement != (self.textViewMissionStmt.text?.trim() ?? "")){
          isUpdate = true
        } else if(self.biAddRequest?.busi_service_provider != self.selectedServiceId){
            isUpdate = true
        }else if(self.biAddRequest?.busi_service_provider_type != self.selectedChildId) {
            isUpdate = true
        } else  if((self.biAddRequest?.busi_entity != self.selectedBusinessTypeId) || self.biAddRequest?.manualEntry != self.inputNameCustomEntry.text()) {
            // Check business type
            if(self.btnTypeYes.isSelected){
                if(!self.inputNameCustomEntry.isHidden){
                    if(self.biAddRequest?.manualEntry != (self.inputNameCustomEntry.text() ?? "")){
                        isUpdate = true
                    }
                } else {
                    if(self.biAddRequest?.busi_entity != self.selectedBusinessTypeId){
                        isUpdate = true
                    }
                }
            }
        }
        
        for imageObj in self.arrDocumentImages{
            if(imageObj.imageData != nil){
                isUpdate = true
            }
        }

        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.bilocationViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BILocationViewController.nameOfClass) as BILocationViewController
        
        bilocationViewController?.biAddRequest = self.biAddRequest
        bilocationViewController?.isEditingBusiness = isEditingBusiness
        self.navigationController?.pushViewController(self.bilocationViewController!, animated: true)
    }
    
    fileprivate func isVideoInList() -> Bool{
        
        if(self.arrDocumentImages.count > 0){
            var isFound = false
            for imageData in arrDocumentImages {
                
                
                if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                    
                    if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                        isFound = true
                    }
                } else {
                    if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                      isFound = true
                    }
                }
            }
            
            return isFound
        }
        
        return false
    }
    
}

//MARK:- Button Action method implementation
extension BIAddOverViewViewController : BusinessNewTypeViewDelegate {
    
    func cancelViewScr(){
        if(businessNewTypeView != nil){
            businessNewTypeView = nil
        }
    }
    
    func addNewType(newType: String){
        print("New Type: \(newType)")
        
        self.presenter.serverAddListingRequest(title: newType, catId: BIPresenter.parentListingList?.parentListingData?.business_entity?[0].listing_category ?? "", listingParentId: "0")
    }
}


//MARK:- Button Action method implementation
extension BIAddOverViewViewController{
    
    @IBAction func methodBusinessTypeAction(_ sender: UIButton) {
        
        self.btnTypeYes.isSelected = false
        self.btnTypeNo.isSelected = false
        
        if(self.btnTypeYes == sender){
            self.btnTypeYes.isSelected = true
            dropDownTitleHeight.priority = .defaultLow
            dropDownTypeHeight.constant = 50
        } else {
            self.btnTypeNo.isSelected = true
            dropDownTypeHeight.constant = 0
            dropDownTitleHeight.priority = .defaultHigh
            self.inputNameCustomEntry.isHidden = true
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is BIDetailViewController }
            if let biDetailVC = biDetailVC as? BIDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }

    /**
     *  Image picker button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnImagePickerAction(_ sender : UIButton){
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            self.displayPhotoSelectionOption(withCircularAllow:false, isSelectVideo: true)
        }
    }
    
}

/*******************************************/
//MARK: - UICollectionViewDelegate
/*******************************************/
extension BIAddOverViewViewController : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        if arrDocumentImages.count < 5{ // For 2 images
//            return arrDocumentImages.count + 1
//        }else{
            return arrDocumentImages.count
//        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //MARK:- Store Images views
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath as IndexPath) as! SerivceImageCell
        
        let imageData = arrDocumentImages[indexPath.row]
        
        cell.delegate = self
        cell.cellIndex = indexPath.row
        cell.btnDelete.isHidden = false
        cell.btnDelete.tag = indexPath.row
        
        if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
            
            if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                cell.imgPlay.isHidden = false
                cell.imgService.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
            } else {
                cell.imgPlay.isHidden = true
                cell.imgService.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
            }
        } else {
            if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                cell.imgPlay.isHidden = false
                cell.imgService.image = UIImage(data: imageData.imageThumbnailData ?? Data())
            } else {
                cell.imgPlay.isHidden = true
                cell.imgService.image = UIImage(data: imageData.imageData ?? Data())
            }
        }
        
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        //return CGSize(width: 100, height: 100)
//        if(arrDocumentImages.count == 0){
//        return CGSize(width: self.documentImgCollectionView.frame.size.width, height: self.documentImgCollectionView.frame.size.height)
//        } else {
//            return CGSize(width: self.documentImgCollectionView.frame.size.width - 50, height: self.documentImgCollectionView.frame.size.height)
//        }
//    }
    
    /**
     *  Implement collectionView with UICollectionViewDelegateFlowLayout method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:100, height: 80)
    }
    
}

extension BIAddOverViewViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        //TODO: Update image as per selection
        
        if arrDocumentImages.count > cellIndex {
            let imageData = self.arrDocumentImages[cellIndex]
            if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                    self.imgPlayBtn.isHidden = false
                    self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                } else {
                    self.imgPlayBtn.isHidden = true
                    self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                }
            } else {
                if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                    self.imgPlayBtn.isHidden = false
                    self.imgProfile.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                } else {
                    self.imgPlayBtn.isHidden = true
                    self.imgProfile.image = UIImage(data: imageData.imageData ?? Data())
                }
            }
        }
    }
}


//MARK:- Button Actions
//MARK:-
extension BIAddOverViewViewController{
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnAddImg(_ sender: UIButton) {
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            if(self.arrDocumentImages.count < HelperConstant.maximumProfileMedia){
                if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                    //already authorized
                    debugPrint("already authorized")
                    
                    OperationQueue.main.addOperation() {
                        if(!self.isVideoInList()){
                            self.displayPhotoSelectionOption(withCircularAllow:false, isSelectVideo: true)
                        } else {
                            self.displayPhotoSelectionOption(withCircularAllow: false)
                        }
                    }
                    
                } else {
                    AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                        if granted {
                            //access allowed
                            debugPrint("access allowed")
                            
                            OperationQueue.main.addOperation() {
                                //                            if(self.arrDocumentImages.count == 0){
                                if(!self.isVideoInList()){
                                    self.displayPhotoSelectionOption(withCircularAllow:false, isSelectVideo: true)
                                } else {
                                    self.displayPhotoSelectionOption(withCircularAllow: false)
                                }
                            }
                            
                        } else {
                            //access denied
                            debugPrint("Access denied")
                            OperationQueue.main.addOperation() {
                                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_this_app_needs_access_to_your_camera_and_gallery.getLocalized(), buttonTitle: nil, controller: nil)
                            }
                        }
                    })
                }
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_video_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnDeleteStoreImg(_ sender: UIButton) {
        
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            
//            if(self.arrDocumentImages.count > 1){
                let imageData = arrDocumentImages[sender.tag]
                
                if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                    
                    //            self.lblUploadImageVideo.isHidden =
                    
                    self.selectedDocumentImages = imageData
                    // TODO: approval alert
                    self.logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
                    self.logoutViewController?.delegate = self
                    self.logoutViewController?.scrTitle = ""
                    self.logoutViewController?.scrDesc = LocalizationKeys.error_remove_media.getLocalized()
                    self.selectedIndex = sender.tag
                    self.logoutViewController?.modalPresentationStyle = .overFullScreen
                    self.present(logoutViewController!, animated: true, completion: nil)
                } else {
                    
                    if(self.arrDocumentImages.count > 0){
                        self.arrDocumentImages.remove(at: sender.tag)
                    }
                    
                    if self.arrDocumentImages.count == 0{
                        self.constraintDocumentCollectionHeight.constant = 0.00
                        self.imgProfile.image = nil
                    } else {
                        
                        // Update profile image
                        //////////////////////////////////////////////////////////////////////
                        let imageData = self.arrDocumentImages[0]
                        
                        if(imageData.imageUrl != nil && (imageData.imageUrl?.length)! > 0){
                            
                            if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                                self.imgPlayBtn.isHidden = false
                                self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                            } else {
                                self.imgPlayBtn.isHidden = true
                                self.imgProfile.sd_setImage(with: URL(string: (imageData.imageUrl ?? "")))
                            }
                            //                cell.btnDelete.isHidden = true
                        } else {
                            if(imageData.imageType?.caseInsensitiveCompare("2") == ComparisonResult.orderedSame){
                                self.imgPlayBtn.isHidden = false
                                self.imgProfile.image = UIImage(data: imageData.imageThumbnailData ?? Data())
                            } else {
                                self.imgPlayBtn.isHidden = true
                                self.imgProfile.image = UIImage(data: imageData.imageData ?? Data())
                            }
                        }
                        //////////////////////////////////////////////////////////////////////
                        
                        
                        self.documentImgCollectionView.reloadData()
                    }
                }
//            } else {
////                error_busi_minimum_media
//                self.showBannerAlertWith(message: LocalizationKeys.error_busi_minimum_media.getLocalized(), alert: .error)
//            }
        }
    }
}

extension BIAddOverViewViewController: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
        
        if(self.selectedDocumentImages != nil){
            let deleteMediaRequest = DeleteMediaRequest(media_id: self.selectedDocumentImages?.id ?? "")
            self.presenter.deleteMediaData(deleteMediaRequest: deleteMediaRequest, callback: {
                (status, response, message) in
                
                if(status){
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    if(self.arrDocumentImages.count > 0){
                        self.arrDocumentImages.remove(at: self.selectedIndex)
                    }
                    
                    if self.arrDocumentImages.count == 0{
                        self.constraintDocumentCollectionHeight.constant = 0.00
                        self.imgPlayBtn.isHidden = true
                        self.imgProfile.image = nil
                    }
                    
                    self.documentImgCollectionView.reloadData()
                    
                } else {
                    
                }
            })
        }
    }
}



//MARK:- Button Action method implementation
extension BIAddOverViewViewController{
    func setYesNOStatusOnExtraAddedBtns(){
        if self.biAddRequest?.business_credit_syestem == "1"{
            self.creditSystemAction(self.btnCreditYes)
        }else{
            self.creditSystemAction(self.btnCreditNo)
        }
        if self.biAddRequest?.business_donation == "1"{
            self.donationSystemAction(self.btnDonationYes)
        }else{
             self.donationSystemAction(self.btnDonationNo)
        }
        if self.biAddRequest?.business_calenderlisting == "1"{
            self.calenderListingAction(self.btnCalenderYes)
        }else{
             self.calenderListingAction(self.btnCalenderNo)
        }
        
    }
    
    
   @IBAction func creditSystemAction(_ sender: UIButton) {
    self.btnCreditYes.isSelected = false
    self.btnCreditNo.isSelected = false
    
    if(self.btnCreditYes == sender){
        self.btnCreditYes.isSelected = true
        self.biAddRequest?.business_credit_syestem = "1"
    } else {
        self.btnCreditNo.isSelected = true
         self.biAddRequest?.business_credit_syestem = "0"
    }
}
    @IBAction func donationSystemAction(_ sender: UIButton) {
    
    self.btnDonationYes.isSelected = false
    self.btnDonationNo.isSelected = false
    
    if(self.btnDonationYes == sender){
        self.btnDonationYes.isSelected = true
        self.biAddRequest?.business_donation = "1"
    } else {
        self.btnDonationNo.isSelected = true
        self.biAddRequest?.business_donation = "0"
    }
}
    
    @IBAction func calenderListingAction(_ sender: UIButton) {
    
    self.btnCalenderYes.isSelected = false
    self.btnCalenderNo.isSelected = false
    
    if(self.btnCalenderYes == sender){
        self.btnCalenderYes.isSelected = true
        self.biAddRequest?.business_calenderlisting = "1"
    } else {
        self.btnCalenderNo.isSelected = true
        self.biAddRequest?.business_calenderlisting = "0"
    }
    }
}


//
//  BusinessInformationAddContactViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 24/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 * BIAddContactViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BIAddContactViewController : LMBaseViewController{
    
    var expandTableNumber = [Int] ()
    

    
    @IBOutlet weak var primaryEmailContact:BusinessInformationEmailContactView!
    @IBOutlet weak var secondaryEmailContact:BusinessInformationEmailContactView!
    @IBOutlet weak var primaryPhoneContact:BusinessInformationEmailContactView!
    @IBOutlet weak var secondaryPhoneContact:BusinessInformationEmailContactView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    var biAddRequest:BIAddRequest?
    var personnelList: [ServiceListingList]? {
        didSet {
            if let list = personnelList {
                primaryEmailContact.affilatedInfo = list
                secondaryEmailContact.affilatedInfo = list
                primaryPhoneContact.affilatedInfo = list
                secondaryPhoneContact.affilatedInfo = list
            }
        }
    }
    
    fileprivate var presenter = BIAddContactPresenter()
    fileprivate let formNumber = 3
    fileprivate var biaddWebLinkViewController:BIAddWebLinkViewController?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: For testing
//        biAddRequest = BIAddRequest()
        
        presenter.connectView(view: self)
        
        presenter.getPersonalServiceList()
        setScreenData()
        
        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.biaddWebLinkViewController = nil
    }
}

extension BIAddContactViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        primaryEmailContact.placeHolderText = "Email "
        secondaryEmailContact.placeHolderText = "Email "
        primaryPhoneContact.placeHolderText = "Phone Number "
        secondaryPhoneContact.placeHolderText = "Phone Number "
        
        primaryEmailContact.inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Email ")
                .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        secondaryEmailContact.inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Email ")
                .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        self.primaryPhoneContact.inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Mobile Number ")
                .maxLength(HelperConstant.LIMIT_UPPER_PHONE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        self.primaryPhoneContact.inputName.input.keyboardType = UIKeyboardType.phonePad
        
        self.secondaryPhoneContact.inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Mobile Number ")
                .maxLength(HelperConstant.LIMIT_UPPER_PHONE, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .inputType(.number, onViolated: (message: LocalizationKeys.invalidEmail.getLocalized(), callback: nil))
                .build()
        )
        self.secondaryPhoneContact.inputName.input.keyboardType = UIKeyboardType.phonePad
        
        if(self.biAddRequest != nil){
            
            // Primary Email
            if(self.biAddRequest?.busi_email != nil){
                let contactInfo = self.biAddRequest?.busi_email!
                
                self.primaryEmailContact.inputName.input.text = contactInfo?.name ?? ""
                self.primaryEmailContact.textViewDesc.text = contactInfo?.desc ?? ""
                self.primaryEmailContact.affilatedSelectedId = contactInfo?.affilatedList ?? []
             //   self.primaryEmailContact.setInitialSetupForTable()
            }
            
            
            // Secondary Email
            if(self.biAddRequest?.busi_email_sec != nil){
                let contactInfo = self.biAddRequest?.busi_email_sec!
                
                self.secondaryEmailContact.inputName.input.text = contactInfo?.name ?? ""
                self.secondaryEmailContact.textViewDesc.text = contactInfo?.desc ?? ""
                self.secondaryEmailContact.affilatedSelectedId = contactInfo?.affilatedList ?? []
             //   self.secondaryEmailContact.setInitialSetupForTable()
            }
            
            
            // Primary Phone number
            if(self.biAddRequest?.busi_phone != nil){
                let contactInfo = self.biAddRequest?.busi_phone!
                
                self.primaryPhoneContact.inputName.input.text = contactInfo?.name ?? ""
                self.primaryPhoneContact.textViewDesc.text = contactInfo?.desc ?? ""
                self.primaryPhoneContact.affilatedSelectedId = contactInfo?.affilatedList ?? []
              //  self.primaryPhoneContact.setInitialSetupForTable()
                
            }
            
            // Secondary Phone number
            if(self.biAddRequest?.busi_phone_sec != nil){
                let contactInfo = self.biAddRequest?.busi_phone_sec!
                
                self.secondaryPhoneContact.inputName.input.text = contactInfo?.name ?? ""
                self.secondaryPhoneContact.textViewDesc.text = contactInfo?.desc ?? ""
                self.secondaryPhoneContact.affilatedSelectedId = contactInfo?.affilatedList ?? []
             //   self.secondaryPhoneContact.setInitialSetupForTable()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.biAddRequest != nil){
            
            // Primary Email
            if(self.biAddRequest?.busi_email != nil || self.primaryEmailContact.inputName.input.text != nil){
                let contactInfo:ContactInfo? = self.biAddRequest?.busi_email ?? ContactInfo()
                
                if(self.primaryEmailContact.inputName.input.text != (contactInfo?.name ?? "")){
                    isUpdate = true
                }
                
                if(self.primaryEmailContact.textViewDesc.text != (contactInfo?.desc ?? "")){
                    isUpdate = true
                }
                
                if(self.primaryEmailContact.affilatedSelectedId != (contactInfo?.affilatedList ?? [])){
                    isUpdate = true
                }
            }
            
            // Secondary Email
            if(self.biAddRequest?.busi_email_sec != nil || self.secondaryEmailContact.inputName.input.text != nil){
                
                let contactInfo:ContactInfo? = self.biAddRequest?.busi_email_sec ?? ContactInfo()
                
                if(self.secondaryEmailContact.inputName.input.text != (contactInfo?.name ?? "")){
                    isUpdate = true
                }
                
                if(self.secondaryEmailContact.textViewDesc.text != (contactInfo?.desc ?? "")){
                    isUpdate = true
                }
                
                if(self.secondaryEmailContact.affilatedSelectedId != (contactInfo?.affilatedList ?? [])){
                    isUpdate = true
                }
            }
            
            // Primary Phone number
            if(self.biAddRequest?.busi_phone != nil || self.primaryPhoneContact.inputName.input.text != nil){
                
                let contactInfo:ContactInfo? = self.biAddRequest?.busi_phone ?? ContactInfo()
                
                if(self.primaryPhoneContact.inputName.input.text != (contactInfo?.name ?? "")){
                    isUpdate = true
                }
                
                if(self.primaryPhoneContact.textViewDesc.text != (contactInfo?.desc ?? "")){
                    isUpdate = true
                }
                
                if(self.primaryPhoneContact.affilatedSelectedId != (contactInfo?.affilatedList ?? [])){
                    isUpdate = true
                }
                
            }
            
            // Secondary Phone number
            if(self.biAddRequest?.busi_phone_sec != nil || self.secondaryPhoneContact.inputName.input.text != nil){
                let contactInfo:ContactInfo? = self.biAddRequest?.busi_phone_sec ?? ContactInfo()
                
                if(self.secondaryPhoneContact.inputName.input.text != (contactInfo?.name ?? "")){
                    isUpdate = true
                }
                
                if(self.secondaryPhoneContact.textViewDesc.text != (contactInfo?.desc ?? "")){
                    isUpdate = true
                }
                
                if(self.secondaryPhoneContact.affilatedSelectedId != (contactInfo?.affilatedList ?? [])){
                    isUpdate = true
                }
            }
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.biaddWebLinkViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddWebLinkViewController.nameOfClass) as BIAddWebLinkViewController
        
        self.biaddWebLinkViewController?.biAddRequest = self.biAddRequest
        biaddWebLinkViewController?.isEditingBusiness = isEditingBusiness
        self.navigationController?.pushViewController(self.biaddWebLinkViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if (self.primaryEmailContact.inputName.text()?.trim().isEmpty)! {
            message = LocalizationKeys.error_enter_primary_email.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if !(self.primaryEmailContact.inputName.text()?.isEmail())!{
            message = LocalizationKeys.invalidEmail.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.primaryEmailContact.textViewDesc.text?.trim().isEmpty)! {
            message = LocalizationKeys.error_enter_primaryEmailDesc.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        // Secondary Email
        if(!(self.secondaryEmailContact.inputName.text()?.trim().isEmpty)! || !(self.secondaryEmailContact.textViewDesc.text?.trim().isEmpty)!){
            if (self.secondaryEmailContact.inputName.text()?.trim().isEmpty)! {
                message = LocalizationKeys.error_enter_secondary_email.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            } else if !(self.secondaryEmailContact.inputName.text()?.isEmail())!{
                message = LocalizationKeys.invalidEmail.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            } else if (self.secondaryEmailContact.textViewDesc.text?.trim().isEmpty)! {
                message = LocalizationKeys.error_enter_secondaryEmailDesc.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
        }
        
        // Primary Phone contact
        if (self.primaryPhoneContact.inputName.text()?.trim().isEmpty)! {
            message = LocalizationKeys.error_primary_number.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if ((self.primaryPhoneContact.inputName.text()?.trim().length)! < HelperConstant.LIMIT_LOWER_PHONE || (self.primaryPhoneContact.inputName.text()?.trim().length)! > HelperConstant.LIMIT_UPPER_PHONE ) {
            message = LocalizationKeys.invalidMobile.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if (self.primaryPhoneContact.textViewDesc.text?.trim().isEmpty)! {
            message = LocalizationKeys.error_primary_phone_desc.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        // Secondary Contact
        if(!(self.secondaryPhoneContact.inputName.text()?.trim().isEmpty)! || !(self.secondaryPhoneContact.textViewDesc.text?.trim().isEmpty)!){
            if (self.secondaryPhoneContact.inputName.text()?.trim().isEmpty)! {
                message = LocalizationKeys.error_secondary_number.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            } else if ((self.secondaryPhoneContact.inputName.text()?.trim().length)! < HelperConstant.LIMIT_LOWER_PHONE || (self.secondaryPhoneContact.inputName.text()?.trim().length)! > HelperConstant.LIMIT_UPPER_PHONE ) {
                message = LocalizationKeys.invalidMobile.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            } else if (self.secondaryPhoneContact.textViewDesc.text?.trim().isEmpty)! {
                message = LocalizationKeys.error_secondary_phone_desc.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            }
        }
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0 && self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = ""
        }
        
        self.biAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        // Primary Email
        var contactInfo = ContactInfo()
        contactInfo.name = self.primaryEmailContact.inputName.text() ?? ""
        contactInfo.desc = self.primaryEmailContact.textViewDesc.text ?? ""
        contactInfo.affilatedList = self.primaryEmailContact.affilatedSelectedId
        
        //self.biAddRequest?.busi_email = contactInfo.toJSONString()
        self.biAddRequest?.busi_email = contactInfo
        
        // Secondary Email
        if(!(self.secondaryEmailContact.inputName.text()?.trim().isEmpty)! || !(self.secondaryEmailContact.textViewDesc.text?.trim().isEmpty)!){
            contactInfo = ContactInfo()
            contactInfo.name = self.secondaryEmailContact.inputName.text() ?? ""
            contactInfo.desc = self.secondaryEmailContact.textViewDesc.text ?? ""
            contactInfo.affilatedList = self.secondaryEmailContact.affilatedSelectedId
            //self.biAddRequest?.busi_email_sec = contactInfo.toJSONString()
            self.biAddRequest?.busi_email_sec = contactInfo
        }
        
        
        // Primary Phone number
        contactInfo = ContactInfo()
        contactInfo.name = self.primaryPhoneContact.inputName.text() ?? ""
        contactInfo.desc = self.primaryPhoneContact.textViewDesc.text ?? ""
        contactInfo.affilatedList = self.primaryPhoneContact.affilatedSelectedId
//        self.biAddRequest?.busi_phone = contactInfo.toJSONString()
        self.biAddRequest?.busi_phone = contactInfo
        
        // Secondary Phone number
        if(!(self.secondaryPhoneContact.inputName.text()?.trim().isEmpty)! || !(self.secondaryPhoneContact.textViewDesc.text?.trim().isEmpty)!){
            contactInfo = ContactInfo()
            contactInfo.name = self.secondaryPhoneContact.inputName.text() ?? ""
            contactInfo.desc = self.secondaryPhoneContact.textViewDesc.text ?? ""
            contactInfo.affilatedList = self.secondaryPhoneContact.affilatedSelectedId
            //self.biAddRequest?.busi_phone_sec = contactInfo.toJSONString()
            self.biAddRequest?.busi_phone_sec = contactInfo
        }
        
        return true
    }
}

extension BIAddContactViewController {
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is BIDetailViewController }
            if let biDetailVC = biDetailVC as? BIDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

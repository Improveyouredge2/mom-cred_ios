//
//  BIAddSocialMediaViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 25/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 * BIAddSocialMediaViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BIAddSocialMediaViewController : LMBaseViewController, BIAddLocationPresentable {
    
//    var expandTableNumber = [Int] ()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    @IBOutlet weak var primarySocialMedia:BusinessInformationSocialMedia!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    fileprivate var socialMediaInfoList:[BusinessSocialMediaInfo] = []
    
    var biAddRequest:BIAddRequest?
    
    fileprivate var presenter = BIAddSocialMediaPresenter()
    fileprivate var biaddExceptionalViewController:BIAddExceptionalViewController?
    fileprivate let formNumber = 5
    fileprivate var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: For testing
//        biAddRequest = BIAddRequest()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        presenter.connectView(view: self)
        
        self.setScreenData()

        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.biaddExceptionalViewController = nil
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(socialMediaInfoList.count == 0){
            self.viewTableView.isHidden = true
        } else {
            self.viewTableView.isHidden = false
        }
        
        if(self.tableView.contentSize.height > 0){
            self.tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension BIAddSocialMediaViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        if(self.biAddRequest != nil){
            if(self.biAddRequest?.busi_sociallink_list != nil && (self.biAddRequest?.busi_sociallink_list?.count)! > 0){
                self.socialMediaInfoList = self.biAddRequest?.busi_sociallink_list ?? []
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.biAddRequest?.busi_sociallink_list != nil && (self.biAddRequest?.busi_sociallink_list?.count)! != self.socialMediaInfoList.count){
            isUpdate = true
        }
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.biaddExceptionalViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddExceptionalViewController.nameOfClass) as BIAddExceptionalViewController
        
        self.biaddExceptionalViewController?.biAddRequest = self.biAddRequest
        biaddExceptionalViewController?.isEditingBusiness = isEditingBusiness
        self.navigationController?.pushViewController(self.biaddExceptionalViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if (self.socialMediaInfoList.count == 0) {
            message = LocalizationKeys.error_add_social_webiste.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0  && self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = ""
        }
        
        self.biAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        //self.biAddRequest?.busi_sociallink_list = socialMediaInfoList.toJSONString()
        self.biAddRequest?.busi_sociallink_list = socialMediaInfoList ?? []
        
        return true
    }
    
}

extension BIAddSocialMediaViewController{
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is BIDetailViewController }
            if let biDetailVC = biDetailVC as? BIDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}


extension BIAddSocialMediaViewController{
    
    @IBAction func methodAddSocialMediaAction(_ sender: UIButton){
        
        var isValid = true
        let businessSocialMediaInfo = BusinessSocialMediaInfo()
        
        if((primarySocialMedia.inputName.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: primarySocialMedia.inputName.text() ?? "")){
            businessSocialMediaInfo.webLinkTitle = primarySocialMedia.inputName.text() ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_media_link.getLocalized(), buttonTitle: nil, controller: nil)
            
            isValid = false
            return
        }
        
        if((primarySocialMedia.textViewDesc.text?.length)! > 0){
            businessSocialMediaInfo.webLinkDesc = primarySocialMedia.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_media_content.getLocalized(), buttonTitle: nil, controller: nil)
            
            isValid = false
            return
        }
        
        
        if(primarySocialMedia.selectedSocialMedia.length > 0){
            businessSocialMediaInfo.socialMediaName = primarySocialMedia.selectedSocialMedia
        } else {
            if(!primarySocialMedia.inputNameCustomEntry.isHidden && (primarySocialMedia.inputNameCustomEntry.text()?.trim().isEmpty)!){
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_enter_social_platform_name.getLocalized(), buttonTitle: nil, controller: nil)
                isValid = false
            } else if(primarySocialMedia.selectedSocialMedia.length == 0 && primarySocialMedia.inputNameCustomEntry.isHidden){
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_social_media_platform.getLocalized(), buttonTitle: nil, controller: nil)
                isValid = false
            }
            
            
            if(isValid == false){
                return
            }
        }
        
        if(!primarySocialMedia.inputNameCustomEntry.isHidden && !(primarySocialMedia.inputNameCustomEntry.text()?.trim().isEmpty)!){
            businessSocialMediaInfo.socialMediaName = primarySocialMedia.inputNameCustomEntry.text() ?? ""
        }
        
        
        if(isValid && self.socialMediaInfoList.count < HelperConstant.minimumBlocks){
            
            self.isUpdate = true
            self.primarySocialMedia.inputName.input.text = ""
            self.primarySocialMedia.inputNameCustomEntry.input.text = ""
            self.primarySocialMedia.inputNameCustomEntry.isHidden = true
            self.primarySocialMedia.textViewDesc.text = ""
            self.primarySocialMedia.dropDownSocialMediaPlatform.text = ""
            self.socialMediaInfoList.append(businessSocialMediaInfo)
            self.tableView.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension BIAddSocialMediaViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return socialMediaInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationSocialMediaCell.nameOfClass) as! BusinessInformationSocialMediaCell
        
        let objInfo = socialMediaInfoList[indexPath.row]
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        cell.lblMainWebsite.text = objInfo.socialMediaName
        cell.lblWebsiteLink.text = objInfo.webLinkTitle
        cell.lblWebsiteDesc.text = objInfo.webLinkDesc
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension BIAddSocialMediaViewController:BusinessInformationSocialMediaCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        self.isUpdate = true
        if(self.socialMediaInfoList.count > 0){
            self.socialMediaInfoList.remove(at: cellIndex?.row ?? 0)
            self.tableView.reloadData()
            
            if(socialMediaInfoList.count == 0){
                self.viewTableView.isHidden = true
            }
        }
    }
}

/**
 * BusinessLocationAddMoreCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol BusinessInformationSocialMediaCellDelegate {
    func removeWebsiteInfo(cellIndex:IndexPath?)
}

class BusinessInformationSocialMediaCell: UITableViewCell {
    @IBOutlet weak var lblMainWebsite : UILabel!
    @IBOutlet weak var lblWebsiteLink : UILabel!
    @IBOutlet weak var lblWebsiteDesc : UILabel!
    @IBOutlet weak var btnRemove : UIButton!
    
    var cellIndex:IndexPath?
    var delegate:BusinessInformationSocialMediaCellDelegate?
    
    @IBAction func methodCloseAction(_ sender: UIButton) {
        if(delegate != nil){
            delegate?.removeWebsiteInfo(cellIndex: cellIndex)
        }
    }
}

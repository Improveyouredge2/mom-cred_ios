//
//  BIAddWebLinkViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 25/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 * BIAddWebLinkViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BIAddWebLinkViewController : LMBaseViewController, BIAddLocationPresentable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var primaryWebLink:BusinessInformationWeblink!
    @IBOutlet weak var viewTableView: UIView!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    fileprivate var weblinkList:[BusinessWebLinkInfo] = []
    fileprivate let formNumber = 4
    
    var biAddRequest:BIAddRequest?
    fileprivate var isUpdate = false
    fileprivate var biaddSocialMediaViewController:BIAddSocialMediaViewController?
    
    fileprivate var presenter = BIAddWebLinkPresenter()
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: For testing
//        biAddRequest = BIAddRequest()
        
        presenter.connectView(view: self)
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        viewTableView.isHidden = true
        
        self.setScreenData()

        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.biaddSocialMediaViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(weblinkList.count == 0){
            self.viewTableView.isHidden = true
        } else {
            self.viewTableView.isHidden = false
        }
        
        if(self.tableView.contentSize.height > 0){
            self.tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

extension BIAddWebLinkViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        if(self.biAddRequest != nil){
            if(self.biAddRequest?.busi_weblink_list != nil && (self.biAddRequest?.busi_weblink_list?.count)! > 0){
                self.weblinkList = self.biAddRequest?.busi_weblink_list ?? []
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.biAddRequest?.busi_weblink_list != nil && (self.biAddRequest?.busi_weblink_list?.count)! != self.weblinkList.count){
            isUpdate = true
        }
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.biaddSocialMediaViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddSocialMediaViewController.nameOfClass) as BIAddSocialMediaViewController
        
        self.biaddSocialMediaViewController?.biAddRequest = self.biAddRequest
        biaddSocialMediaViewController?.isEditingBusiness = isEditingBusiness
        self.navigationController?.pushViewController(self.biaddSocialMediaViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if (self.weblinkList.count == 0) {
            message = LocalizationKeys.error_add_website.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        }
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0  && self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = ""
        }
        
        self.biAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        self.biAddRequest?.busi_weblink_list = weblinkList ?? []
        
        return true
    }
}

extension BIAddWebLinkViewController{
    
    @IBAction func methodAddWebLinkAction(_ sender: UIButton){
        
        
        var isValid = true
        var businessWebLinkInfo = BusinessWebLinkInfo()
        
        if((primaryWebLink.inputName.text()?.length)! > 0 && Helper.sharedInstance.verifyUrl(urlString: primaryWebLink.inputName.text() ?? "")){
            businessWebLinkInfo.webLinkTitle = primaryWebLink.inputName.text() ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_invalid_url.getLocalized(), buttonTitle: nil, controller: nil)
            
            isValid = false
            return
        }
        
        if((primaryWebLink.textViewDesc.text?.length)! > 0){
            businessWebLinkInfo.webLinkDesc = primaryWebLink.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_website_invalid_url_desc.getLocalized(), buttonTitle: nil, controller: nil)
            
            isValid = false
            return
        }
        
        if(primaryWebLink.btnMainWebsiteYes.isSelected){
            businessWebLinkInfo.mainWebsite = NSNumber(booleanLiteral: true)
        }
        
        if(isValid && self.weblinkList.count < HelperConstant.minimumBlocks){
            
            self.isUpdate = true
            self.primaryWebLink.inputName.input.text = ""
            self.primaryWebLink.textViewDesc.text = ""
            
            self.weblinkList.append(businessWebLinkInfo)
            self.tableView.reloadData()
            self.viewTableView.isHidden = false
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is BIDetailViewController }
            if let biDetailVC = biDetailVC as? BIDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension BIAddWebLinkViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return weblinkList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationWebsiteCell.nameOfClass) as! BusinessInformationWebsiteCell
        
        let objInfo = weblinkList[indexPath.row]
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        cell.lblMainWebsite.text = (objInfo.mainWebsite?.boolValue ?? false ) ? "Yes" : "No"
        cell.lblWebsiteLink.text = objInfo.webLinkTitle
        cell.lblWebsiteDesc.text = objInfo.webLinkDesc
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
//        return 200
    }
}

extension BIAddWebLinkViewController:BusinessInformationWebsiteCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
        self.isUpdate = true
        if(self.weblinkList.count > 0){
            self.weblinkList.remove(at: cellIndex?.row ?? 0)
            
            self.tableView.reloadData()
            
            if(weblinkList.count == 0){
                self.viewTableView.isHidden = true
            }
        }
    }
}

/**
 * BusinessLocationAddMoreCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol BusinessInformationWebsiteCellDelegate {
    func removeWebsiteInfo(cellIndex:IndexPath?)
}

class BusinessInformationWebsiteCell: UITableViewCell {
    @IBOutlet weak var lblMainWebsite : UILabel!
    @IBOutlet weak var lblWebsiteLink : UILabel!
    @IBOutlet weak var lblWebsiteDesc : UILabel!
    @IBOutlet weak var btnRemove : UIButton!
    
    var cellIndex:IndexPath?
    var delegate:BusinessInformationWebsiteCellDelegate?
    
    @IBAction func methodCloseAction(_ sender: UIButton) {
        if(delegate != nil){
            delegate?.removeWebsiteInfo(cellIndex: cellIndex)
        }
    }
}

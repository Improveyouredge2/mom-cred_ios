//
//  BusinessLocationAddMoreCell.swift
//  MomCred
//
//  Created by Robert Chen on 20/06/19.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit

/**
 * BusinessLocationAddMoreCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol BusinessLocationAddMoreCellDelegate {
    func newText(text: String, zipcode: String, refTableView: UITableView?)
//    func openPlacePicker()
}

/**
 * BusinessLocationAddMoreCell display Category list of product with Product option in collection on UITableViewCell.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BusinessLocationAddMoreCell : UITableViewCell {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var btnAddMore: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    
    static let inputViewCellSize:CGFloat = 170
    
    var delegate: BusinessLocationAddMoreCellDelegate?
    var refTableView: UITableView?
    var cityName = ""
    var zipcode = ""
    
    fileprivate var autocompleteController:GMSAutocompleteViewController?
    
    @IBAction func methodAddMoreAction(_ sender: UIButton) {
        if(self.cityName.length > 0){
            delegate?.newText(text: cityName, zipcode: zipcode, refTableView: refTableView)
        } else if(delegate != nil && (self.textField.text?.length)! > 0){
            delegate?.newText(text: textField.text ?? "", zipcode: zipcode, refTableView: refTableView)
        }
        self.textField.text = ""
    }
 }

extension BusinessLocationAddMoreCell{
    @IBAction func actionLocationSearch(_ sender: UIButton) {
        self.openPlacePicker()
    }
}

extension BusinessLocationAddMoreCell{
    func openPlacePicker() {
        //        BaseApp.isDashboardSearch = true
        autocompleteController = GMSAutocompleteViewController()
        let filter  = GMSAutocompleteFilter()
        //        filter.country = "IN"
        //filter.type = GMSPlacesAutocompleteTypeFilter.address
        filter.type = GMSPlacesAutocompleteTypeFilter.city
        
        autocompleteController?.autocompleteFilter   =   filter
        autocompleteController?.delegate = self
        autocompleteController?.autocompleteBoundsMode = .restrict
        (HelperConstant.appDelegate.window?.rootViewController as! UINavigationController).visibleViewController?.present(autocompleteController!, animated: true, completion: nil)
        
//        HelperConstant.appDelegate.navigationController?.present(autocompleteController!, animated: true, completion: nil)
    }
}

//MARK:- GMSAutocompleteViewController Delegate
//MARK:-
extension BusinessLocationAddMoreCell : GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        print("Place name: \(place.name ?? "") with Coordinate - Lat - \(place.coordinate.latitude) Long - \(place.coordinate.longitude)")
        print("Place address: \(place.formattedAddress!)")
        print("Place attributions: \(String(describing: place.attributions))")
        
        var address = place.formattedAddress ?? ""
        let city = place.addressComponents?.first(where: { $0.type == "locality" })?.name
        let state = place.addressComponents?.first(where: { $0.type == "administrative_area_level_1" })?.name
        let country = place.addressComponents?.first(where: { $0.type == "country" })?.name
        
//        address = "\(address), \(city ?? ""), \(state ?? "")"
        address = "\(city ?? ""), \(state ?? ""), \(country ?? "")"
        
        self.cityName = city ?? ""
        self.textField.text = address
        zipcode = place.addressComponents?.first(where: { $0.type == "postal_code" })?.name ?? ""
        
//        self.quickSearchRequest.location_lat = "\(place.coordinate.latitude)"
//        self.quickSearchRequest.location_long = "\(place.coordinate.longitude)"
        
        if(self.autocompleteController != nil){
            self.autocompleteController?.removeFromParent()
            self.autocompleteController = nil
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        HelperConstant.appDelegate.navigationController?.dismiss(animated: true, completion: nil)
        self.autocompleteController?.dismiss(animated: true, completion: nil)
    }
}

//
//  BusinessContactAddPersonnelCell.swift
//  MomCred
//
//  Created by Robert Chen on 20/06/19.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit

/**
 * BusinessLocationAddMoreCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol SingleSelectionCellDelegate {
    func selectListItem(cellIndex: IndexPath?, status:Bool)
}

/**
 * BusinessLocationAddMoreCell display Category list of product with Product option in collection on UITableViewCell.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class SingleSelectionCell : UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    var cellIndex: IndexPath?
    
    var delegate: SingleSelectionCellDelegate?
    
    func updateStatus(status:Bool){
        btnCheck.isSelected = status
    }
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//        btnCheck.isSelected = selected
//    }
    
    @IBAction func methodCheckAction(_ sender: UIButton) {
        
        btnCheck.isSelected = !btnCheck.isSelected
        
        if(delegate != nil){
            delegate?.selectListItem(cellIndex: cellIndex, status: btnCheck.isSelected)
        }
    }
 }

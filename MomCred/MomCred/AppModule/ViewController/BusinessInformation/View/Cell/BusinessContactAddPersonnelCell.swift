//
//  BusinessContactAddPersonnelCell.swift
//  MomCred
//
//  Created by Robert Chen on 20/06/19.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit

/**
 * BusinessLocationAddMoreCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol BusinessContactAddPersonnelCellDelegate {
    func selectListItem(cellIndex: IndexPath?, status:Bool, tableView:UITableView?)
}

/**
 * BusinessLocationAddMoreCell display Category list of product with Product option in collection on UITableViewCell.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BusinessContactAddPersonnelCell : UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    var cellIndex: IndexPath?
    
    var delegate: BusinessContactAddPersonnelCellDelegate?
    var refTableView:UITableView?
    
    func updateStatus(status:Bool) {
        btnCheck.isSelected = status
    }
    
    @IBAction func methodCheckAction(_ sender: UIButton) {
        
        btnCheck.isSelected = !btnCheck.isSelected
        
        if(delegate != nil){
            delegate?.selectListItem(cellIndex: cellIndex, status: btnCheck.isSelected, tableView: refTableView)
        }
    }
 }

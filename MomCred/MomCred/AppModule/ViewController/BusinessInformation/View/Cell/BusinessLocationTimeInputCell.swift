//
//  BusinessLocationTimeInputCell.swift
//  MomCred
//
//  Created by Robert Chen on 20/06/19.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit

/**
 * BusinessLocationTimeInputCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol BusinessLocationTimeInputCellDelegate {
    func sectionSwitchStatus(cellIndex:IndexPath?, status:Bool)
    
    func updateTimeInfo(cellIndex:IndexPath?, openTime:String, closeTime:String)
}

/**
 * BusinessLocationTimeInputCell display Category list of product with Product option in collection on UITableViewCell.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BusinessLocationTimeInputCell : UITableViewCell {
    @IBOutlet weak var inputOpenTime: RYFloatingInput!
    @IBOutlet weak var inputCloseTime: RYFloatingInput!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var switchSection: UISwitch!
    @IBOutlet weak var constraintsTimeInput: NSLayoutConstraint!
    @IBOutlet weak var stackViewInputView: UIStackView!
    
    static let inputFullTimeViewCellSize:CGFloat = 125
    static let inputHalfTimeViewCellSize:CGFloat = 55
    
    var delegate:BusinessLocationTimeInputCellDelegate?
    var cellIndex:IndexPath?
    var status:Bool = false
    var openTime:String = ""
    var closeTime:String = ""
    
    fileprivate var pickerCloseTime: UIDatePicker?
    
    override func awakeFromNib() {
//        self.setScreenData()
        
        self.openDatePicker()
        
    }
    
    
    @IBAction func methodSwitchChangeAction(_ sender: UISwitch) {
        //self.status = !self.switchSection.isOn
//        self.switchSection.isOn = self.status
        
        self.status = self.switchSection.isOn
        
        if(self.switchSection.isOn){
            //TODO: Section on
            self.stackViewInputView.isHidden = false
        } else {
            //TODO: Section off
            self.stackViewInputView.isHidden = true
        }
        
        if(delegate != nil){
            delegate?.sectionSwitchStatus(cellIndex:cellIndex, status: self.status)
        }
    }
 }

extension BusinessLocationTimeInputCell{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        inputOpenTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(openTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Open ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputCloseTime.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(closeTime)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Close ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        self.switchSection.isOn = status
        
        if(self.switchSection.isOn){
            self.stackViewInputView.isHidden = false
        } else {
            self.stackViewInputView.isHidden = true
        }
    }
}

// MARK:- Date Picker
extension BusinessLocationTimeInputCell{
    fileprivate func openDatePicker(){
        
        // Time picker for Open time
        ////////////////////////////////////////////////////////
        var picker: UIDatePicker = UIDatePicker.init()
        
        // Set some of UIDatePicker properties
        picker.datePickerMode = .time
        picker.timeZone = NSTimeZone.local
        picker.backgroundColor = UIColor.white
        
        // Add an event to call onDidChangeDate function when value is changed.
        picker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        
        self.inputOpenTime.input.inputView = picker
        ////////////////////////////////////////////////////////
        
        // Set picker for close time
        ////////////////////////////////////////////////////////
        picker = UIDatePicker.init()
        
        // Set some of UIDatePicker properties
        picker.datePickerMode = .time
//        picker.maximumDate = Date()
        picker.timeZone = NSTimeZone.local
        picker.backgroundColor = UIColor.white
        
        // Add an event to call onDidChangeDate function when value is changed.
        picker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        
        self.inputCloseTime.input.inputView = picker
        self.pickerCloseTime = picker
        ////////////////////////////////////////////////////////
    }
    
    @objc fileprivate func dateValueChanged(_ sender: UIDatePicker){
        
        if(self.inputOpenTime.input.inputView == sender){
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            // Set date format
            dateFormatter.dateFormat = "hh:mm a"
            
            // Apply date format
            let selectedDate: String = dateFormatter.string(from: sender.date)
            
            self.pickerCloseTime?.minimumDate = sender.date
            
            self.inputOpenTime.input.text = selectedDate
            self.openTime = selectedDate
            
            if(self.delegate != nil){
                self.delegate?.updateTimeInfo(cellIndex: cellIndex, openTime: self.openTime, closeTime: self.closeTime)
            }
            
        } else if(self.inputCloseTime.input.inputView == sender){
            // Create date formatter
            let dateFormatter: DateFormatter = DateFormatter()
            
            // Set date format
            dateFormatter.dateFormat = "hh:mm a"
            
            // Apply date format
            let selectedDate: String = dateFormatter.string(from: sender.date)
            
            self.inputCloseTime.input.text = selectedDate
            self.closeTime = selectedDate
            
            if(self.delegate != nil){
                self.delegate?.updateTimeInfo(cellIndex: cellIndex, openTime: self.openTime, closeTime: self.closeTime)
            }
        }
        
    }
}

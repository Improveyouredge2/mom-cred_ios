//
//  BusinessLocationInputView.swift
//  MomCred
//
//  Created by Robert Chen on 20/06/19.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit

/**
 * BusinessLocationInputViewDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol BusinessLocationTitleViewDelegate {
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?)
}

/**
 * BusinessLocationInputView display Category list of product with Product option in collection on UITableViewCell.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BusinessLocationTitleView : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnClose: UIButton?
    
    static let inputValueViewCellSize:CGFloat = 60
    
    var delegate:BusinessLocationTitleViewDelegate?
    var cellIndex: IndexPath?
    var refTableView: UITableView?
    
    @IBAction func methodCloseAction(_ sender: UIButton) {
        
        if(delegate != nil){
            delegate?.removeCellInfo(cellIndex: cellIndex, refTableView: refTableView)
        }
        
    }
 }



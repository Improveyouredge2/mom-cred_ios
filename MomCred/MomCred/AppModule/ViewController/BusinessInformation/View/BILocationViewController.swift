//
//  BILocationViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * BILocationViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BILocationViewController : LMBaseViewController, BIAddLocationPresentable {
    
    var expandTableNumber = [Int] ()
    
    @IBOutlet weak var btnYesPrimLoc: UIButton!
    @IBOutlet weak var btnNoPrimLoc: UIButton!
    @IBOutlet weak var btnYesSecLoc: UIButton!
    @IBOutlet weak var btnNoSecLoc: UIButton!
    @IBOutlet weak var primaryLocation:BusinessLocationView!
    @IBOutlet weak var secondaryLocation:BusinessLocationView!
    
    @IBOutlet weak var switchSecondaryLocationSelection: UISwitch!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    var biAddRequest:BIAddRequest?
    
    fileprivate var presenter = BIAddLocationPresenter()
    fileprivate let formNumber = 2
    fileprivate var biaddContactViewController:BIAddContactViewController?
    var isScreenDataReload = false
    var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        //        self.perform(#selector(self.updateScreen), with: self, afterDelay: 0.02)
        
        // Prepare business location info with empty data
        var businessLocationInfo = BusinessLocationInfo()
        self.primaryLocation.businessLocationInfo = businessLocationInfo
        
        businessLocationInfo = BusinessLocationInfo()
        self.secondaryLocation.businessLocationInfo = businessLocationInfo
        
        DispatchQueue.main.async {
            // Prepare business location info with empty data
            self.primaryLocation.setScreenData()
            self.secondaryLocation.setScreenData()
            
            self.secondaryLocation.isHidden = true
            self.isScreenDataReload = true
            //        // TODO: For testing
            //        biAddRequest = BIAddRequest()
            self.setScreenData()
        }

        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.biaddContactViewController = nil
        
       // self.setYesNOStatusOnExtraAddedBtns()
    }
    
    @objc fileprivate func updateScreen(){
        // Prepare business location info with empty data
        var businessLocationInfo = BusinessLocationInfo()
        self.primaryLocation.businessLocationInfo = businessLocationInfo
        self.primaryLocation.setScreenData()
        
        businessLocationInfo = BusinessLocationInfo()
        self.secondaryLocation.businessLocationInfo = businessLocationInfo
        self.secondaryLocation.setScreenData()
        self.secondaryLocation.isHidden = true
        
        self.isScreenDataReload = true
        //        // TODO: For testing
        //        biAddRequest = BIAddRequest()
        
        self.setScreenData()
        
    }
}

extension BILocationViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        
        if(self.biAddRequest != nil){
            
            // Primary location
            if(self.biAddRequest?.location_lat == nil && self.biAddRequest?.location_city != nil){
                self.primaryLocation.businessLocationInfo?.exclusiveTravel = true
            }
            
            if(self.biAddRequest?.location_name != nil){
                self.primaryLocation.businessLocationInfo?.locationName = self.biAddRequest?.location_name ?? ""
            }
            
            if(self.biAddRequest?.location_address != nil){
            self.primaryLocation.businessLocationInfo?.locationAddress = self.biAddRequest?.location_address ?? ""
            }
            
            if(self.biAddRequest?.location_city != nil){
            self.primaryLocation.businessLocationInfo?.locationCity = self.biAddRequest?.location_city ?? ""
            }
            
            if(self.biAddRequest?.location_state != nil){
            self.primaryLocation.businessLocationInfo?.locationState = self.biAddRequest?.location_state ?? ""
            }
            
            if(self.biAddRequest?.location_lat != nil){
                self.primaryLocation.businessLocationInfo?.locationLat = self.biAddRequest?.location_lat ?? ""
            }
            
            if(self.biAddRequest?.location_long != nil){
                self.primaryLocation.businessLocationInfo?.locationLng = self.biAddRequest?.location_long ?? ""
            }
            
            if(self.biAddRequest?.location_type != nil){
                self.primaryLocation.businessLocationInfo?.locationType = (self.biAddRequest?.location_type != nil) ? (Int(self.biAddRequest?.location_type ?? "0") == 0 ? false : true) : false
            }
            
            if(self.biAddRequest?.location_ownership != nil){
                self.primaryLocation.businessLocationInfo?.locationBelong = (self.biAddRequest?.location_ownership != nil) ? (Int(self.biAddRequest?.location_ownership ?? "0") == 0 ? false : true) : false
            }
            
            if(self.biAddRequest?.location_classification != nil && (self.biAddRequest?.location_classification?.count)! > 0){
                self.primaryLocation.businessLocationInfo?.locationClassification = self.biAddRequest?.location_classification ?? []
            }
            
            if(self.biAddRequest?.location_speciality != nil && (self.biAddRequest?.location_classification?.count)! > 0){
                self.primaryLocation.businessLocationInfo?.locationSpeciality = self.biAddRequest?.location_speciality ?? []
            }
            
            if(self.biAddRequest?.location_busi_hours != nil && (self.biAddRequest?.location_busi_hours?.count)! > 0){
                self.primaryLocation.businessLocationInfo?.busniessHour = self.biAddRequest?.location_busi_hours ?? []
            }
            
            // update view
            self.primaryLocation.setScreenData()
            
            // Secondary location
            if(self.biAddRequest?.location_lat_sec == nil && self.biAddRequest?.location_city_sec != nil && (self.biAddRequest?.location_city_sec?.length)! > 0){
                self.secondaryLocation.businessLocationInfo?.exclusiveTravel = true
            }
            
            if(self.biAddRequest?.location_name_sec != nil && (self.biAddRequest?.location_name_sec?.length)! > 0){
                self.switchSecondaryLocationSelection.isOn = true
                self.methodSwitchChangeAction(self.switchSecondaryLocationSelection)
                
                self.secondaryLocation.businessLocationInfo?.locationName = self.biAddRequest?.location_name_sec ?? ""
            }
            
            if(self.biAddRequest?.location_address_sec != nil){
                self.secondaryLocation.businessLocationInfo?.locationAddress = self.biAddRequest?.location_address_sec ?? ""
            }
            
            if(self.biAddRequest?.location_city_sec != nil){
                self.secondaryLocation.businessLocationInfo?.locationCity = self.biAddRequest?.location_city_sec ?? ""
            }
            
            if(self.biAddRequest?.location_state_sec != nil){
                self.secondaryLocation.businessLocationInfo?.locationState = self.biAddRequest?.location_state_sec ?? ""
            }
            
            if(self.biAddRequest?.location_lat_sec != nil){
                self.secondaryLocation.businessLocationInfo?.locationLat = self.biAddRequest?.location_lat_sec ?? ""
            }
            
            if(self.biAddRequest?.location_long_sec != nil){
                self.secondaryLocation.businessLocationInfo?.locationLng = self.biAddRequest?.location_long_sec ?? ""
            }
            
            if(self.biAddRequest?.location_type_sec != nil){
                self.secondaryLocation.businessLocationInfo?.locationType = (self.biAddRequest?.location_type_sec != nil) ? (Int(self.biAddRequest?.location_type_sec ?? "0") == 0 ? false : true) : false
            }
            
            if(self.biAddRequest?.location_ownership_sec != nil){
                self.secondaryLocation.businessLocationInfo?.locationBelong = (self.biAddRequest?.location_ownership_sec != nil) ? (Int(self.biAddRequest?.location_ownership_sec ?? "0") == 0 ? false : true) : false
            }
            
            if(self.biAddRequest?.location_classification_sec != nil && (self.biAddRequest?.location_classification_sec?.count)! > 0){
                self.secondaryLocation.businessLocationInfo?.locationClassification = self.biAddRequest?.location_classification_sec ?? []
            }
            
            if(self.biAddRequest?.location_speciality_sec != nil && (self.biAddRequest?.location_classification_sec?.count)! > 0){
                self.secondaryLocation.businessLocationInfo?.locationSpeciality = self.biAddRequest?.location_speciality_sec ?? []
            }
            
            if(self.biAddRequest?.location_busi_hours_sec != nil && (self.biAddRequest?.location_busi_hours_sec?.count)! > 0){
                self.secondaryLocation.businessLocationInfo?.busniessHour = self.biAddRequest?.location_busi_hours_sec ?? []
            }
            
            // update view
            self.secondaryLocation.setScreenData()
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        if(self.biAddRequest != nil){
            
            // Primary location
            var status:Int = (self.primaryLocation.businessLocationInfo?.exclusiveTravel ?? false) ? 1 : 0
            if(self.biAddRequest?.location_lat == nil && self.biAddRequest?.location_city != nil && status != 1){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_name != self.primaryLocation.businessLocationInfo?.locationName){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_address != self.primaryLocation.businessLocationInfo?.locationAddress){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_city != self.primaryLocation.businessLocationInfo?.locationCity){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_state != self.primaryLocation.businessLocationInfo?.locationState){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_lat != self.primaryLocation.businessLocationInfo?.locationLat){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_long != self.primaryLocation.businessLocationInfo?.locationLng){
                isUpdate = true
            }
            
            status = (self.primaryLocation.businessLocationInfo?.locationType ?? false) ? 1 : 0
            if(self.biAddRequest?.location_type != nil && status !=  Int(self.biAddRequest?.location_type ?? "0")){
                
                isUpdate = true
            }
            
            status = (self.primaryLocation.businessLocationInfo?.locationBelong ?? false) ? 1 : 0
            if(self.biAddRequest?.location_ownership != nil && status !=  Int(self.biAddRequest?.location_ownership ?? "0")){
                isUpdate = true
            }
            
            if(self.primaryLocation.isUpdateNewVal){
                isUpdate = self.primaryLocation.isUpdateNewVal
            }
            
            // Secondary location
            if(self.biAddRequest?.location_lat_sec == nil && self.biAddRequest?.location_city_sec != nil && (self.primaryLocation.businessLocationInfo?.exclusiveTravel != nil && !(self.primaryLocation.businessLocationInfo?.exclusiveTravel)!)){
            isUpdate = true
            }
            
            
            if(self.biAddRequest?.location_name_sec != self.secondaryLocation.businessLocationInfo?.locationName){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_address_sec != self.secondaryLocation.businessLocationInfo?.locationAddress){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_city_sec != self.secondaryLocation.businessLocationInfo?.locationCity){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_state_sec != self.secondaryLocation.businessLocationInfo?.locationState){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_lat_sec != self.secondaryLocation.businessLocationInfo?.locationLat){
                isUpdate = true
            }
            
            if(self.biAddRequest?.location_long_sec != self.secondaryLocation.businessLocationInfo?.locationLng){
                isUpdate = true
            }
            
            
            status = (self.secondaryLocation.businessLocationInfo?.locationType ?? false) ? 1 : 0
            if(self.biAddRequest?.location_type_sec != nil && status !=  Int(self.biAddRequest?.location_type_sec ?? "0")){
                isUpdate = true
            }
            
            status = (self.secondaryLocation.businessLocationInfo?.locationBelong ?? false) ? 1 : 0
            if(self.biAddRequest?.location_ownership_sec != nil && status !=  Int(self.biAddRequest?.location_ownership_sec ?? "0")){
                isUpdate = true
            }
            
            if(self.secondaryLocation.isUpdateNewVal){
                isUpdate = self.secondaryLocation.isUpdateNewVal
            }
        }
        
        if(self.isUpdate){
            isUpdate = self.isUpdate
        }
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.biaddContactViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddContactViewController.nameOfClass) as BIAddContactViewController
        
        if self.primaryLocation.constraintstableViewBusinessHourHeight.constant == 0{
            self.biAddRequest?.location_busi_hours = []
        }
        if self.secondaryLocation.constraintstableViewBusinessHourHeight.constant == 0{
            self.biAddRequest?.location_busi_hours_sec = []
        }
        biaddContactViewController?.isEditingBusiness = isEditingBusiness
        self.biaddContactViewController?.biAddRequest = self.biAddRequest
        
        self.navigationController?.pushViewController(self.biaddContactViewController!, animated: true)
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var message = ""
        
        if (self.primaryLocation.inputName.text()?.trim().isEmpty)! {
            message = LocalizationKeys.error_select_business_name.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
        } else if(((self.primaryLocation?.btnExclusiveTravelYes) != nil) &&  (self.primaryLocation?.inputCity.text()?.trim().isEmpty)!){
            message = LocalizationKeys.error_city.getLocalized()
            self.showBannerAlertWith(message: message, alert: .error)
            return false
//        } else if (self.primaryLocation.inputAddress.text()?.trim().isEmpty)! {
//            message = LocalizationKeys.error_select_business_name.getLocalized()
//            self.showBannerAlertWith(message: message, alert: .error)
//            return false
//        } else if (self.primaryLocation.inputState.text()?.trim().isEmpty)! {
//            message = LocalizationKeys.error_select_business_name.getLocalized()
//            self.showBannerAlertWith(message: message, alert: .error)
//            return false
//        } else if (self.primaryLocation.inputCity.text()?.trim().isEmpty)! {
//            message = LocalizationKeys.error_select_business_name.getLocalized()
//            self.showBannerAlertWith(message: message, alert: .error)
//            return false
        }
        
        //TODO Business Hours Calculation require only if private is selected
        if(self.primaryLocation.btnLocationBelongYes.isSelected){
        }
        
        if(self.switchSecondaryLocationSelection.isOn){
            if (self.secondaryLocation.inputName.text()?.trim().isEmpty)! {
                message = LocalizationKeys.error_select_business_name.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
            } else if(((self.secondaryLocation?.btnExclusiveTravelYes) != nil) &&  (self.secondaryLocation?.inputCity.text()?.trim().isEmpty)!){
                message = LocalizationKeys.error_city.getLocalized()
                self.showBannerAlertWith(message: message, alert: .error)
                return false
//            }else if (self.secondaryLocation.inputAddress.text()?.trim().isEmpty)! {
//                message = LocalizationKeys.error_select_business_name.getLocalized()
//                self.showBannerAlertWith(message: message, alert: .error)
//                return false
//            } else if (self.secondaryLocation.inputState.text()?.trim().isEmpty)! {
//                message = LocalizationKeys.error_select_business_name.getLocalized()
//                self.showBannerAlertWith(message: message, alert: .error)
//                return false
//            } else if (self.secondaryLocation.inputCity.text()?.trim().isEmpty)! {
//                message = LocalizationKeys.error_select_business_name.getLocalized()
//                self.showBannerAlertWith(message: message, alert: .error)
//                return false
            }
            
            //TODO Business Hours Calculation require only if private is selected
            if(self.secondaryLocation.btnLocationBelongYes.isSelected){
            }
        }

        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0  && self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = ""
        }
        
        self.biAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        self.biAddRequest?.location_exclusive_travel = (self.primaryLocation.businessLocationInfo?.exclusiveTravel ?? false) ? "1" : "0"
        self.biAddRequest?.location_name = self.primaryLocation.businessLocationInfo?.locationName ?? ""
        self.biAddRequest?.location_address = self.primaryLocation.businessLocationInfo?.locationAddress ?? ""
        self.biAddRequest?.location_city = self.primaryLocation.businessLocationInfo?.locationCity ?? ""
        self.biAddRequest?.location_state = self.primaryLocation.businessLocationInfo?.locationState ?? ""
        self.biAddRequest?.location_lat = self.primaryLocation.businessLocationInfo?.locationLat ?? ""
        self.biAddRequest?.location_long = self.primaryLocation.businessLocationInfo?.locationLng ?? ""
        self.biAddRequest?.location_type = (self.primaryLocation.businessLocationInfo?.locationType ?? false) ? "1" : "0"
        self.biAddRequest?.location_ownership = (self.primaryLocation.businessLocationInfo?.locationBelong ?? false) ? "1" : "0"
        
        if(self.primaryLocation.businessLocationInfo?.locationClassification != nil && (self.primaryLocation.businessLocationInfo?.locationClassification.count)! > 0){
            self.biAddRequest?.location_classification = self.primaryLocation.businessLocationInfo?.locationClassification ?? []
        } else {
            self.biAddRequest?.location_classification = []
        }
        
        if(self.primaryLocation.businessLocationInfo?.locationSpeciality != nil && (self.primaryLocation.businessLocationInfo?.locationSpeciality.count)! > 0){
            self.biAddRequest?.location_speciality = self.primaryLocation.businessLocationInfo?.locationSpeciality ?? []
        } else {
            self.biAddRequest?.location_speciality = []
        }
        
        self.biAddRequest?.location_busi_hours = self.primaryLocation.businessLocationInfo?.busniessHour ?? []
        
        
        if(self.switchSecondaryLocationSelection.isOn){
            self.biAddRequest?.location_exclusive_travel_sec = (self.secondaryLocation.businessLocationInfo?.exclusiveTravel ?? false) ? "1" : "0"
            self.biAddRequest?.location_name_sec = self.secondaryLocation.businessLocationInfo?.locationName ?? ""
            self.biAddRequest?.location_address_sec = self.secondaryLocation.businessLocationInfo?.locationAddress ?? ""
            self.biAddRequest?.location_city_sec = self.secondaryLocation.businessLocationInfo?.locationCity ?? ""
            self.biAddRequest?.location_state_sec = self.secondaryLocation.businessLocationInfo?.locationState ?? ""
            self.biAddRequest?.location_lat_sec = self.secondaryLocation.businessLocationInfo?.locationLat ?? ""
            self.biAddRequest?.location_long_sec = self.secondaryLocation.businessLocationInfo?.locationLng ?? ""
            self.biAddRequest?.location_type_sec = (self.secondaryLocation.businessLocationInfo?.locationType ?? false) ? "1" : "0"
            self.biAddRequest?.location_ownership_sec = (self.secondaryLocation.businessLocationInfo?.locationBelong ?? false) ? "1" : "0"
            
            if(self.secondaryLocation.businessLocationInfo?.locationClassification != nil && (self.secondaryLocation.businessLocationInfo?.locationClassification.count)! > 0){
                self.biAddRequest?.location_classification_sec = self.secondaryLocation.businessLocationInfo?.locationClassification ?? []
            } else {
                self.biAddRequest?.location_classification_sec = []
            }
            
            if(self.secondaryLocation.businessLocationInfo?.locationSpeciality != nil && (self.secondaryLocation.businessLocationInfo?.locationSpeciality.count)! > 0){
                self.biAddRequest?.location_speciality_sec = self.secondaryLocation.businessLocationInfo?.locationSpeciality ?? []
            } else {
                self.biAddRequest?.location_speciality_sec = []
            }
            
            self.biAddRequest?.location_busi_hours_sec = self.secondaryLocation.businessLocationInfo?.busniessHour ?? []
        } else {
            self.biAddRequest?.location_exclusive_travel_sec = "0"
            self.biAddRequest?.location_name_sec = ""
            self.biAddRequest?.location_address_sec = ""
            self.biAddRequest?.location_city_sec = ""
            self.biAddRequest?.location_state_sec = ""
            self.biAddRequest?.location_lat_sec = self.secondaryLocation.businessLocationInfo?.locationLat ?? ""
            self.biAddRequest?.location_long_sec = self.secondaryLocation.businessLocationInfo?.locationLng ?? ""
            self.biAddRequest?.location_type_sec = "0"
            self.biAddRequest?.location_ownership_sec = "0"
            
            self.biAddRequest?.location_classification_sec = []
            
            self.biAddRequest?.location_speciality_sec = []
            
            self.biAddRequest?.location_busi_hours_sec = []
        }
        
        return true
    }
}

extension BILocationViewController{
    @IBAction func methodSwitchChangeAction(_ sender: UISwitch) {
        self.isUpdate = true
        if(switchSecondaryLocationSelection.isOn){
            self.secondaryLocation.isHidden = false
        } else {
            self.secondaryLocation.isHidden = true
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is BIDetailViewController }
            if let biDetailVC = biDetailVC as? BIDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}



//MARK:- Button Action method implementation
extension BILocationViewController{
    func setYesNOStatusOnExtraAddedBtns(){
        if (self.biAddRequest?.location_busi_hours!.count)! > 0 {
            self.actionBtnPrimeLocYesNo(self.btnYesPrimLoc)
            
        }else{
            self.actionBtnPrimeLocYesNo(self.btnNoPrimLoc)
        self.primaryLocation.constraintstableViewBusinessHourHeight.constant = 0
            self.primaryLocation.tableViewBusinessHour.isHidden = true
        }
        
        if (self.biAddRequest?.location_busi_hours_sec!.count)! > 0{
            self.actionBtnSecLocYesNo(self.btnYesSecLoc)
        }else{
             self.actionBtnSecLocYesNo(self.btnNoSecLoc)
            self.secondaryLocation.constraintstableViewBusinessHourHeight.constant = 0
            self.secondaryLocation.tableViewBusinessHour.isHidden = true
        }
        
    }
    
    
   @IBAction func actionBtnPrimeLocYesNo(_ sender: UIButton) {
    self.btnYesPrimLoc.isSelected = false
    self.btnNoPrimLoc.isSelected = false
    
    if(self.btnYesPrimLoc == sender){
        self.btnYesPrimLoc.isSelected = true
      //  self.primaryLocation.constraintstableViewBusinessHourHeight.constant = CGFloat((125 * (self.biAddRequest?.location_busi_hours!.count)!))
    } else {
        self.btnNoPrimLoc.isSelected = true
  //  self.primaryLocation.constraintstableViewBusinessHourHeight.constant = 0
    }
}
    @IBAction func actionBtnSecLocYesNo(_ sender: UIButton) {
    
    self.btnYesSecLoc.isSelected = false
    self.btnNoSecLoc.isSelected = false
    
    if(self.btnYesSecLoc == sender){
        self.btnYesSecLoc.isSelected = true
   // self.secondaryLocation.constraintstableViewBusinessHourHeight.constant = CGFloat((125 * (self.biAddRequest?.location_busi_hours_sec!.count)!))
    } else {
        self.btnNoSecLoc.isSelected = true
   //  self.secondaryLocation.constraintstableViewBusinessHourHeight.constant = 0
        
    }
}
    
   
}

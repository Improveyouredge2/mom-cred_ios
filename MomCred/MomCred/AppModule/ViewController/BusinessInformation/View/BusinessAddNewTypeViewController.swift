//
//  BusinessAddNewTypeViewController.swift
//  MomCred
//
//  Copyright © 2019 consagous. All rights reserved.
//

import Foundation
import UIKit

protocol BusinessNewTypeViewDelegate {

    func cancelViewScr()
    func addNewType(newType:String)
}

class BusinessAddNewTypeViewController: LMBaseViewController {
    
    @IBOutlet weak var titleHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var buttonError: UIButton!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var textField:UITextField!
    
    var delegate:BusinessNewTypeViewDelegate?
    
    var titleText:String = ""
    var placeHolderText:String = ""
    
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        super.viewDidLoad()
        
        if(titleText.length > 0){
            lblTitle.text = titleText
        }
        
        if(placeHolderText.length > 0){
            textField.placeholder = placeHolderText
        }
        
    }
}

extension BusinessAddNewTypeViewController {
    
    /**
     *  Close button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if(self.delegate != nil){
                self.delegate?.cancelViewScr()
            }
        })
    }
    
    /**
     *  Cancel button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if(self.delegate != nil){
                self.delegate?.cancelViewScr()
            }
        })
    }
    
    /**
     *  Ok button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodOkAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if(self.delegate != nil){
                self.delegate?.addNewType(newType: self.textField.text ?? "")
            }
        })
    }
}

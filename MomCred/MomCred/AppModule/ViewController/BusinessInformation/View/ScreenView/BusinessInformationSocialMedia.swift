//
//  BusinessInformationSocialMedia.swift
//  MomCred
//
//  Created by Apple_iOS on 25/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

class BusinessInformationSocialMedia : UIView {
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var dropDownSocialMediaPlatform: DropDown!
    @IBOutlet weak var inputNameCustomEntry: RYFloatingInput!
    
    fileprivate var businessNewTypeView:BusinessAddNewTypeViewController?
    
    var selectedSocialMedia:String = ""
    
    override func awakeFromNib() {
        setScreenData()
    }
    
}

extension BusinessInformationSocialMedia{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Social Media Link ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputNameCustomEntry.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Social Media Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        // Individual
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownSocialMediaPlatform.optionArray = ["Facebook", "Twitter", "Instagram", "LinkedIn", "YouTube", "Snapchat", "Enter New Social Media..."]
        //Its Id Values and its optional
        dropDownSocialMediaPlatform.optionIds = [1,2,3,4,5,6,1001]
        
//        dropDownSocialMediaPlatform.selectedIndex = 0
        //        selectedMonth = dropDown.optionArray[0]
        //        dropDownStdExpectional.text = selectedMonth
        // The the Closure returns Selected Index and String
        dropDownSocialMediaPlatform.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            
            
            
//            if(index == (self.dropDownSocialMediaPlatform.optionIds?.count)! - 1){
            if(id == 1001){
//                self.openNewSocialMediaTypeAlert()
                self.inputNameCustomEntry.isHidden = false
                self.selectedSocialMedia = ""
                self.inputNameCustomEntry.input.text = ""
            } else {
                self.inputNameCustomEntry.isHidden = true
                self.selectedSocialMedia = selectedText
                self.inputNameCustomEntry.input.text = ""
            }
        }
        ////////////////////////////////////////////////////////////
        
        self.inputNameCustomEntry.isHidden = true
    }
}

//MARK:- Button Action method implementation
extension BusinessInformationSocialMedia{
    fileprivate func openNewSocialMediaTypeAlert(){
        self.businessNewTypeView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BusinessAddNewTypeViewController.nameOfClass) as BusinessAddNewTypeViewController
        
        self.businessNewTypeView?.titleText = "New Social Media Type"
        self.businessNewTypeView?.placeHolderText = "New Social Media"
        
        self.businessNewTypeView?.delegate = self
        self.businessNewTypeView?.modalPresentationStyle = .overFullScreen
        
        HelperConstant.appDelegate.navigationController?.present(self.businessNewTypeView!, animated: true, completion: nil)
    }
}

//MARK:- Button Action method implementation
extension BusinessInformationSocialMedia : BusinessNewTypeViewDelegate {
    
    func cancelViewScr(){
        if(businessNewTypeView != nil){
            businessNewTypeView = nil
        }
    }
    
    func addNewType(newType: String){
        print("New Type: \(newType)")
    }
}

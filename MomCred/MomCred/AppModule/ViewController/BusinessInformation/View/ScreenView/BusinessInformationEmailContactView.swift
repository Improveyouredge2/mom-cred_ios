//
//  BusinessInformationEmailContactView.swift
//  MomCred
//
//  Created by Apple_iOS on 24/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class BusinessInformationEmailContactView : UIView {
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var tableViewPersonnelAffilated: BIDetailTableView! {
        didSet {
            tableViewPersonnelAffilated.sizeDelegate = self
        }
    }
    
    @IBOutlet weak var constraintstableViewPersonnelAffilatedHeight: NSLayoutConstraint!
    
    fileprivate var expandTableNumber = [Int] ()
    fileprivate var minimumRowHeight = 60
    
    var placeHolderText: String = ""
    var affilatedSelectedId:[String] = [] {
        didSet {
            tableViewPersonnelAffilated.reloadData()
        }
    }
    var affilatedInfo: [ServiceListingList]? {
        didSet {
            tableViewPersonnelAffilated.reloadData()
        }
    }
}

extension BusinessInformationEmailContactView{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer(placeHolderText)
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
    }
}


// MARK:- UITableViewDataSource & UITableViewDelegate
extension BusinessInformationEmailContactView : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return affilatedInfo?.count ?? 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessContactAddPersonnelCell.nameOfClass) as! BusinessContactAddPersonnelCell
        
        cell.delegate = self
        cell.cellIndex = indexPath
        cell.lblTitle.text = affilatedInfo?[indexPath.row].title
        
        if let affilatedId = affilatedInfo?[indexPath.row].id, affilatedSelectedId.contains(affilatedId) {
            cell.updateStatus(status: true)
        }
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension BusinessInformationEmailContactView: BusinessContactAddPersonnelCellDelegate{
    func selectListItem(cellIndex: IndexPath?, status:Bool, tableView:UITableView?){
        guard let cellIndex = cellIndex, let affilatedId = affilatedInfo?[cellIndex.row].id else { return }
        if status {
            self.expandTableNumber.append(cellIndex.row)
            self.affilatedSelectedId.append(affilatedId)
        } else {
            let index = affilatedSelectedId.firstIndex { $0 == affilatedId }
            if let index = index {
                affilatedSelectedId.remove(at: index)
            }

            if self.expandTableNumber.count > 0, let index = self.expandTableNumber.index(of: cellIndex.row) {
                self.expandTableNumber.remove(at: index)
            }
        }
    }
}

extension BusinessInformationEmailContactView: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        constraintstableViewPersonnelAffilatedHeight.constant = size.height
    }
}

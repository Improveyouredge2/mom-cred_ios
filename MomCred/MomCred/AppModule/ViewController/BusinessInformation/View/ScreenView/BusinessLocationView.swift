//
//  BusinessLocationCell.swift
//  MomCred
//
//  Created by Robert Chen on 20/06/19.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit

/**
 * BusinessLocationCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol BusinessLocationViewDelegate {
    func reloadCellInfo(cellIndex:IndexPath?, businessLocationInfo:BusinessLocationInfo?)
    
    func openAddressPicker(cellIndex:IndexPath?)
}

/**
 * BusinessLocationCell display Category list of product with Product option in collection on UITableViewCell.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BusinessLocationView : UIView {
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var inputAddress: RYFloatingInput!
    @IBOutlet weak var inputCity: RYFloatingInput!
    @IBOutlet weak var inputState: RYFloatingInput!
    
    @IBOutlet weak var btnExclusiveTravelYes: UIButton!
    @IBOutlet weak var btnExclusiveTravelNo: UIButton!
    @IBOutlet weak var btnLocationPrivate: UIButton!
    @IBOutlet weak var btnLocationPublic: UIButton!
    @IBOutlet weak var btnLocationBelongYes: UIButton!
    @IBOutlet weak var btnLocationBelongNo: UIButton!
    
    @IBOutlet weak var tableViewLocationClassification: BIDetailTableView! {
        didSet {
            tableViewLocationClassification.sizeDelegate = self
        }
    }
    @IBOutlet weak var tableViewLocationSpecility: BIDetailTableView! {
        didSet {
            tableViewLocationSpecility.sizeDelegate = self
        }
    }
    @IBOutlet weak var tableViewBusinessHour: BIDetailTableView! {
        didSet {
            tableViewBusinessHour.sizeDelegate = self
        }
    }
    
    @IBOutlet weak var constraintstableViewLocationClassificationHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintstableViewLocationSpecilityHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintstableViewBusinessHourHeight: NSLayoutConstraint!
    
    var delegate:BusinessLocationViewDelegate?
    
    var cellIndex:IndexPath?
    var businessLocationInfo:BusinessLocationInfo?
    var isUpdateNewVal = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tableViewLocationClassification.register(UINib(nibName: BusinessLocationTitleView.nameOfClass, bundle: nil), forCellReuseIdentifier: BusinessLocationTitleView.nameOfClass)
        self.tableViewLocationClassification.register(UINib(nibName: BusinessLocationAddMoreCell.nameOfClass, bundle: nil), forCellReuseIdentifier: BusinessLocationAddMoreCell.nameOfClass)
        
        self.tableViewLocationSpecility.register(UINib(nibName: BusinessLocationTitleView.nameOfClass, bundle: nil), forCellReuseIdentifier: BusinessLocationTitleView.nameOfClass)
        self.tableViewLocationSpecility.register(UINib(nibName: BusinessLocationAddMoreCell.nameOfClass, bundle: nil), forCellReuseIdentifier: BusinessLocationAddMoreCell.nameOfClass)
        
        self.tableViewBusinessHour.register(UINib(nibName: BusinessLocationTimeInputCell.nameOfClass, bundle: nil), forCellReuseIdentifier: BusinessLocationTimeInputCell.nameOfClass)
        
        Helper.setViewCornerRadius(views: [inputName,inputCity,inputState,inputAddress], borderColor: .white, borderWidth: 1.0, cornerRadius: 8)
    }
}

extension BusinessLocationView {
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData() {
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(businessLocationInfo?.locationName ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Name ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        inputName.setBorderColorClear()
        inputName.input.delegate = self
        
        inputAddress.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(businessLocationInfo?.locationAddress ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Address ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputAddress.setBorderColorClear()
        inputAddress.input.delegate = self
        
        inputCity.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(businessLocationInfo?.locationCity ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("City ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputCity.setBorderColorClear()
        inputCity.input.delegate = self
        
        inputState.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .text(businessLocationInfo?.locationState ?? "")
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("State ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        inputState.setBorderColorClear()
        inputState.input.delegate = self
        
        // update
        if(self.businessLocationInfo?.exclusiveTravel ?? false){
            self.methodExclusiveYesNoAction(self.btnExclusiveTravelYes)
        } else {
            self.methodExclusiveYesNoAction(self.btnExclusiveTravelNo)
        }
        
        if(self.businessLocationInfo?.locationName != nil){
            self.inputName.input.text = self.businessLocationInfo?.locationName ?? ""
        }
        
        if(self.businessLocationInfo?.locationAddress != nil){
            self.inputAddress.input.text = self.businessLocationInfo?.locationAddress ?? ""
        }
        
        if(self.businessLocationInfo?.locationCity != nil){
            self.inputCity.input.text = self.businessLocationInfo?.locationCity ?? ""
        }
        
        if(self.businessLocationInfo?.locationState != nil){
            self.inputState.input.text = self.businessLocationInfo?.locationState ?? ""
        }
        
        if(self.businessLocationInfo?.locationType ?? false){
            self.methodLocatoinPrivatePublicAction(self.btnLocationPrivate)
        } else {
            self.methodLocatoinPrivatePublicAction(self.btnLocationPublic)
        }
        
        if(self.businessLocationInfo?.locationBelong ?? false){
            self.methodLocationBelongYesNoAction(self.btnLocationBelongYes)
        } else {
            self.methodLocationBelongYesNoAction(self.btnLocationBelongNo)
        }
        
        if(self.businessLocationInfo?.locationClassification != nil && (self.businessLocationInfo?.locationClassification.count)! > 0){
            self.tableViewLocationClassification.reloadData()
        }
        
        if(self.businessLocationInfo?.locationSpeciality != nil && (self.businessLocationInfo?.locationSpeciality.count)! > 0){
            self.tableViewLocationSpecility.reloadData()
        }
        
        if(self.businessLocationInfo?.busniessHour != nil && (self.businessLocationInfo?.busniessHour.count)! > 0){
            self.tableViewBusinessHour.reloadData()
        }
    }
}

//MARK:- Button Action method implementation
extension BusinessLocationView{
    
    @IBAction func methodExclusiveYesNoAction(_ sender: UIButton) {
        self.btnExclusiveTravelYes.isSelected = false
        self.btnExclusiveTravelNo.isSelected = false
        
        if(self.btnExclusiveTravelYes == sender){
            self.btnExclusiveTravelYes.isSelected = true
            self.businessLocationInfo?.exclusiveTravel = true
        } else {
            self.btnExclusiveTravelNo.isSelected = true
            self.businessLocationInfo?.exclusiveTravel = false
        }
    }
    
    @IBAction func methodLocatoinPrivatePublicAction(_ sender: UIButton) {
        self.btnLocationPrivate.isSelected = false
        self.btnLocationPublic.isSelected = false
        
        if(self.btnLocationPrivate == sender){
            self.btnLocationPrivate.isSelected = true
            self.businessLocationInfo?.locationType = true
        } else {
            self.btnLocationPublic.isSelected = true
            self.businessLocationInfo?.locationType = false
        }
    }
    
    @IBAction func methodLocationBelongYesNoAction(_ sender: UIButton) {
        self.btnLocationBelongYes.isSelected = false
        self.btnLocationBelongNo.isSelected = false
        
        if(self.btnLocationBelongYes == sender){
            self.btnLocationBelongYes.isSelected = true
            self.businessLocationInfo?.locationBelong = true
        } else {
            self.btnLocationBelongNo.isSelected = true
            self.businessLocationInfo?.locationBelong = false
        }
    }
    
    @IBAction func methodAddressAction(_ sender: UIButton) {
        self.openPlacePicker()
    }
    
    @IBAction func methodStateAction(_ sender: UIButton) {
        self.openPlacePicker()
    }
    
    @IBAction func methodCityAction(_ sender: UIButton) {
        self.openPlacePicker()
    }
 }

// MARK:- UITableViewDataSource & UITableViewDelegate
extension BusinessLocationView : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == self.tableViewLocationClassification){
            self.tableViewLocationClassification.scrollToBottom()
            if let localClassificationCount = self.businessLocationInfo?.locationClassification.count{
                return (localClassificationCount + 1)
            } else {
                return (businessLocationInfo?.locationClassification.count) ?? 0
            }
        } else if(tableView == self.tableViewLocationSpecility) {
            self.tableViewLocationSpecility.scrollToBottom()
                if let locationSpecialityCount = self.businessLocationInfo?.locationSpeciality.count{
                return (locationSpecialityCount + 1)
            } else {
                return (businessLocationInfo?.locationSpeciality.count) ?? 0
            }
        } else if(tableView == self.tableViewBusinessHour) {
            return HelperConstant.weekDayName.count // show as many rows as supported in week day names
        }
        
        return 1
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.tableViewLocationClassification){
            
            if(indexPath.row == 0) {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                
                cell.textField.placeholder = "Classification Title"
                cell.delegate = self
                cell.refTableView = self.tableViewLocationClassification
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                
                cell.delegate = self
                cell.refTableView = self.tableViewLocationClassification
                cell.lblTitle.text = businessLocationInfo?.locationClassification[indexPath.row-1]
                cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
                return cell
            }
        } else if(tableView == self.tableViewLocationSpecility) {
            
            if(indexPath.row == 0) {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                
                cell.textField.placeholder = "Speciality Title"
                cell.delegate = self
                cell.refTableView = self.tableViewLocationSpecility
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                
                cell.delegate = self
                cell.refTableView = self.tableViewLocationSpecility
                cell.lblTitle.text = businessLocationInfo?.locationSpeciality[indexPath.row-1]
                cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
                return cell
            }
        } else if (tableView == self.tableViewBusinessHour) {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTimeInputCell.nameOfClass) as! BusinessLocationTimeInputCell
            
            cell.delegate  = self
            cell.cellIndex = indexPath
            
            let businessHour = businessLocationInfo?.busniessHour.first { $0.index == indexPath.row }
            if let businessHour = businessHour {
                cell.status = businessHour.status?.boolValue ?? false
                cell.openTime = businessHour.openTime ?? ""
                cell.closeTime = businessHour.closeTime ?? ""
            } else {
                let dayName = HelperConstant.weekDayName[indexPath.row]
                cell.lblTitle.text = dayName
                cell.status = false
                cell.openTime = ""
                cell.closeTime = ""
            }

            cell.setScreenData()
            return cell
        }
        
        return UITableViewCell(frame: tableView.frame)
    }
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension BusinessLocationView: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
       
        self.isUpdateNewVal = true
        if(refTableView == self.tableViewLocationClassification){
            if(self.businessLocationInfo?.locationClassification.count)! < HelperConstant.minimumBlocks{
                self.businessLocationInfo?.locationClassification.append(text)
                self.tableViewLocationClassification.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else if(refTableView == self.tableViewLocationSpecility){
            if(self.businessLocationInfo?.locationSpeciality.count)! < HelperConstant.minimumBlocks{
                self.businessLocationInfo?.locationSpeciality.append(text)
                self.tableViewLocationSpecility.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
        
        if(self.delegate != nil){
            self.delegate?.reloadCellInfo(cellIndex: self.cellIndex, businessLocationInfo: self.businessLocationInfo)
        }
    }
}

extension BusinessLocationView : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        self.isUpdateNewVal = true
        if(refTableView == self.tableViewLocationClassification && self.businessLocationInfo != nil && (self.businessLocationInfo?.locationClassification.count)! > 0){
            self.businessLocationInfo?.locationClassification.remove(at: cellIndex?.row ?? 0)
            
            self.tableViewLocationClassification.reloadData()
            
        } else if(refTableView == self.tableViewLocationSpecility && self.businessLocationInfo != nil && (self.businessLocationInfo?.locationSpeciality.count)! > 0){
            self.businessLocationInfo?.locationSpeciality.remove(at: cellIndex?.row ?? 0)
            self.tableViewLocationSpecility.reloadData()
        }
        
        if(self.delegate != nil){
            self.delegate?.reloadCellInfo(cellIndex: self.cellIndex, businessLocationInfo: self.businessLocationInfo)
        }
    }
}

extension BusinessLocationView : UITextFieldDelegate {
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if(inputName.input == textField){
            businessLocationInfo?.locationName = inputName.text() ?? ""
        } else if (inputAddress.input == textField){
            businessLocationInfo?.locationAddress = inputAddress.text() ?? ""
        } else if(inputCity.input == textField){
            businessLocationInfo?.locationCity = inputCity.text() ?? ""
        } else if(inputState.input == textField){
            businessLocationInfo?.locationState = inputState.text() ?? ""
        }
        return true
    }
}

extension BusinessLocationView : BusinessLocationTimeInputCellDelegate{
    
    func sectionSwitchStatus(cellIndex:IndexPath?, status:Bool){
        guard let cellIndex = cellIndex else { return }
        
        self.isUpdateNewVal = true
        let existingBusinessHour = businessLocationInfo?.busniessHour.first { $0.index == cellIndex.row }
        if let existingBusinessHour = existingBusinessHour {
            existingBusinessHour.status = NSNumber(booleanLiteral: status)
        } else {
            let businessHour = BusniessHour()
            businessHour.index = cellIndex.row
            businessHour.dayTitle = HelperConstant.weekDayName[cellIndex.row]
            businessHour.status = NSNumber(booleanLiteral: status)
            businessHour.openTime = ""
            businessHour.closeTime = ""
            businessLocationInfo?.busniessHour.append(businessHour)
        }
        
        if(self.delegate != nil){
            self.delegate?.reloadCellInfo(cellIndex: self.cellIndex, businessLocationInfo: self.businessLocationInfo)
        }
        
        self.tableViewBusinessHour.reloadData()
    }
    
    func updateTimeInfo(cellIndex: IndexPath?, openTime: String, closeTime: String) {
        guard let cellIndex = cellIndex else { return }
        let businessHour = businessLocationInfo?.busniessHour.first { $0.index == cellIndex.row }
        if let businessHour = businessHour {
            businessHour.openTime = openTime
            businessHour.closeTime = closeTime
        }
    }
}

extension BusinessLocationView{
    // Present the Autocomplete view controller when the button is pressed.
    func openPlacePicker() {
        //        BaseApp.isDashboardSearch = true
        let autocompleteController = GMSAutocompleteViewController()
        let filter  = GMSAutocompleteFilter()
        filter.country = "IN"
        filter.type = GMSPlacesAutocompleteTypeFilter.address
        
        autocompleteController.autocompleteFilter   =   filter
        autocompleteController.delegate = self
        autocompleteController.autocompleteBoundsMode = .restrict
        
        HelperConstant.appDelegate.navigationController?.present(autocompleteController, animated: true, completion: nil)
    }
}

//MARK:- GMSAutocompleteViewController Delegate
//MARK:-
extension BusinessLocationView : GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name)) with Coordinate - Lat - \(place.coordinate.latitude) Long - \(place.coordinate.longitude)")
        print("Place address: \(place.formattedAddress!)")
        print("Place attributions: \(String(describing: place.attributions))")
        
        let address = place.formattedAddress ?? ""
        let city = place.addressComponents?.first(where: { $0.type == "locality" })?.name
        let state = place.addressComponents?.first(where: { $0.type == "administrative_area_level_1" })?.name
        
        self.isUpdateNewVal = true
        self.inputAddress.input.text = address
        self.inputCity.input.text  = city
        self.inputState.input.text = state
        
        self.businessLocationInfo?.locationAddress = address
        self.businessLocationInfo?.locationCity = city ?? ""
        self.businessLocationInfo?.locationState = state ?? ""
        self.businessLocationInfo?.locationLat = "\(place.coordinate.latitude)"
        self.businessLocationInfo?.locationLng = "\(place.coordinate.longitude)"
        HelperConstant.appDelegate.navigationController?.dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        HelperConstant.appDelegate.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension BusinessLocationView: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        if tableView == tableViewLocationClassification {
            constraintstableViewLocationClassificationHeight.constant = size.height
        } else if tableView == tableViewLocationSpecility {
            constraintstableViewLocationSpecilityHeight.constant = size.height
        } else if tableView == tableViewBusinessHour {
            constraintstableViewBusinessHourHeight.constant = size.height
        }
    }
}

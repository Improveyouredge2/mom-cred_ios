//
//  BusinessInformationAdditionalLink.swift
//  MomCred
//
//  Created by Apple_iOS on 26/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


class BusinessInformationAdditionalLink : UIView {
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    
    var namePlaceHolder:String = ""
    
    override func awakeFromNib() {
        //        setScreenData()
    }
    
}

extension BusinessInformationAdditionalLink{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer(namePlaceHolder)
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
    }
}

//
//  BusinessInformationWeblink.swift
//  MomCred
//
//  Created by Apple_iOS on 25/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


class BusinessInformationWeblink : UIView {
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    
    @IBOutlet weak var btnMainWebsiteYes: UIButton!
    @IBOutlet weak var btnMainWebsiteNo: UIButton!
    
    override func awakeFromNib() {
        setScreenData()
    }
    
}

extension BusinessInformationWeblink{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Website Link ")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
    }
}

//MARK:- Button Action method implementation
extension BusinessInformationWeblink{
    
    @IBAction func methodWebsiteLinkYesNoAction(_ sender: UIButton) {
        
        self.btnMainWebsiteYes.isSelected = false
        self.btnMainWebsiteNo.isSelected = false
        
        if(self.btnMainWebsiteYes == sender){
            self.btnMainWebsiteYes.isSelected = true
        } else {
            self.btnMainWebsiteNo.isSelected = true
        }
        
    }
}

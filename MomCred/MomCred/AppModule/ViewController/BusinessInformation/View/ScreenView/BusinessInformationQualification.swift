//
//  BusinessInformationQualification.swift
//  MomCred
//
//  Created by Apple_iOS on 26/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

enum FieldType:String{
    case Name
    case Phone
    case Email
}

class BusinessInformationQualification : UIView {
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var inputLink: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    @IBOutlet weak var dropDownList: DropDown!
    
    var namePlaceHolder:String = ""
    var linkPlaceHolder:String = ""
    var listSelectedName:String = ""
    
    var fieldType:FieldType = .Name
    
    var serviceList:[ServiceListingList]?
    var selectedServiceListingList:ServiceListingList?
}

extension BusinessInformationQualification{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData() {
        if let inputName = inputName {
            if fieldType == .Name {
                inputName.setup(setting:
                    RYFloatingInputSetting.Builder.instance()
                        .backgroundColor(.clear)
                        .accentColor(.white)
                        .warningColor(.white)
                        .placeholer(namePlaceHolder)
                        .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                        .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                        .build()
                )
            } else if fieldType == .Phone {
                inputName.setup(setting:
                    RYFloatingInputSetting.Builder.instance()
                        .backgroundColor(.clear)
                        .accentColor(.white)
                        .warningColor(.white)
                        .placeholer(namePlaceHolder)
                        .maxLength(HelperConstant.LIMIT_UPPER_PHONE, onViolated: (message: "", callback: nil))
                        .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                        .build()
                )
                inputName.input.keyboardType = UIKeyboardType.phonePad
            } else if fieldType == .Email {
                inputName.setup(setting:
                    RYFloatingInputSetting.Builder.instance()
                        .backgroundColor(.clear)
                        .accentColor(.white)
                        .warningColor(.white)
                        .placeholer(namePlaceHolder)
                        .maxLength(HelperConstant.LIMIT_EMAIL, onViolated: (message: "", callback: nil))
                        .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                        .build()
                )
                inputName.input.keyboardType = UIKeyboardType.emailAddress
            }
        }
        
        if let inputLink = inputLink {
            inputLink.setup(setting:
                RYFloatingInputSetting.Builder.instance()
                    .backgroundColor(.clear)
                    .accentColor(.white)
                    .warningColor(.white)
                    .placeholer(linkPlaceHolder)
                    .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                    .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                    .build()
            )
        }
    }
    
    func updateDropDown(listInfo: [ServiceListingList]) {
        inputName.isHidden = true
        if !listInfo.isEmpty {
            // Array value listing
            dropDownList.optionArray = listInfo.compactMap { $0.title }
            
            //Its Id Values and its optional
            dropDownList.optionIds = listInfo.compactMap { Int($0.id ?? "0") }
            
            // The the Closure returns Selected Index and String
            dropDownList.didSelect{ [weak self] (selectedText , index ,id) in
                print("Selected String: \(selectedText) \n index: \(index), Id: \(id)")
                guard let self = self else { return }
                if self.selectedServiceListingList == nil {
                    self.selectedServiceListingList = ServiceListingList(id: "\(id)", title: selectedText)
                } else {
                    self.selectedServiceListingList?.id = "\(id)"
                    self.selectedServiceListingList?.title = selectedText
                }
            }
        }
    }
}


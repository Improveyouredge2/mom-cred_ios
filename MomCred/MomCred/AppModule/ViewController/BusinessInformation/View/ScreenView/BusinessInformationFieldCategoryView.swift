//
//  BusinessInformationFieldCategoryView.swift
//  MomCred
//
//  Created by Apple_iOS on 25/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import iOSDropDown

protocol BIFieldCategoryViewDelegate{
    func updateFieldCategorySelected(optionId:String, tagIndex:Int)
}

class BusinessInformationFieldCategoryView : UIView {
    @IBOutlet weak var dropDownCategory: DropDown!
    @IBOutlet weak var dropDownService: DropDown!
    
    @IBOutlet weak var tableViewServiceClassification: UITableView!
    @IBOutlet weak var tableViewServiceTechnique: UITableView!
    
    @IBOutlet weak var constraintstableViewServiceClassificationHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintstableViewServiceTechniqueHeight: NSLayoutConstraint!
    
    @IBOutlet weak var inputNameCustomEntry: RYFloatingInput!
    
    var businessLocationInfo:BusinessFieldServiceCategoryInfo?
    
    fileprivate var businessNewTypeView:BusinessAddNewTypeViewController?
    
    var techniqueTablePlaceHolder = "Speciality Title"
    
    var fieldCategoryList:[ListingDataDetail] = []
    var specificCategoryList:[ListingDataDetail] = []
    
    var delegate:BIFieldCategoryViewDelegate?
    
    var isAllowManualCat = true
    
    override func awakeFromNib() {
        
        if(inputNameCustomEntry != nil){
            self.inputNameCustomEntry.isHidden = true
        }
        
        self.tableViewServiceClassification.register(UINib(nibName: BusinessLocationTitleView.nameOfClass, bundle: nil), forCellReuseIdentifier: BusinessLocationTitleView.nameOfClass)
        self.tableViewServiceClassification.register(UINib(nibName: BusinessLocationAddMoreCell.nameOfClass, bundle: nil), forCellReuseIdentifier: BusinessLocationAddMoreCell.nameOfClass)
        
        self.tableViewServiceTechnique.register(UINib(nibName: BusinessLocationTitleView.nameOfClass, bundle: nil), forCellReuseIdentifier: BusinessLocationTitleView.nameOfClass)
        self.tableViewServiceTechnique.register(UINib(nibName: BusinessLocationAddMoreCell.nameOfClass, bundle: nil), forCellReuseIdentifier: BusinessLocationAddMoreCell.nameOfClass)
        
        self.setScreenData()
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    func updateViewConstraints(tableView: UITableView) {
        if(self.tableViewServiceClassification.contentSize.height > 0){
             self.tableViewServiceClassification.scrollToBottom()
            if((self.businessLocationInfo?.classification?.count)! == 0){
                self.constraintstableViewServiceClassificationHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                self.constraintstableViewServiceClassificationHeight?.constant = self.tableViewServiceClassification.contentSize.height
            }
            self.updateTableViewHeight(tableView: tableView)
        }
        
        if(self.tableViewServiceTechnique.contentSize.height > 0){
            self.tableViewServiceTechnique.scrollToBottom()
            if((self.businessLocationInfo?.technique?.count)! == 0){
                self.constraintstableViewServiceTechniqueHeight?.constant = BusinessLocationAddMoreCell.inputViewCellSize
            } else {
                self.constraintstableViewServiceTechniqueHeight?.constant = self.tableViewServiceTechnique.contentSize.height
            }
            
            self.updateTableViewHeight(tableView: tableView)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView: UITableView) {
        
        if(tableViewServiceClassification == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewServiceClassification.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewServiceClassification.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewServiceClassificationHeight.constant = heightOfTableView
            }
        } else if(tableViewServiceTechnique == tableView){
            
            UIView.animate(withDuration: 0, animations: {
                self.tableViewServiceTechnique.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableViewServiceTechnique.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintstableViewServiceTechniqueHeight.constant = heightOfTableView
            }
        }
    }
    
}

extension BusinessInformationFieldCategoryView{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        // Individual
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        
        if(self.inputNameCustomEntry != nil){
            inputNameCustomEntry.setup(setting:
                RYFloatingInputSetting.Builder.instance()
                    .backgroundColor(.clear)
                    .accentColor(.white)
                    .warningColor(.white)
                    .placeholer("Field Name ")
                    .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                    .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                    .build()
            )
        }
        
        // Array value listing
        dropDownCategory.optionArray = fieldCategoryList.map{$0.listing_title ?? ""}
        
        //Its Id Values and its optional
        dropDownCategory.optionIds = fieldCategoryList.map{Int($0.listing_id ?? "0") ?? 0}
        
        // The the Closure returns Selected Index and String
        dropDownCategory.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index), Id: \(id)")
            
                self.businessLocationInfo?.catFieldName = selectedText
                self.businessLocationInfo?.catField = "\(id)"
                
                self.businessLocationInfo?.specificField = ""
            self.dropDownService.text = ""
                if(self.delegate != nil){
                    self.delegate?.updateFieldCategorySelected(optionId: "\(id)", tagIndex: self.tag)
            }
        }
        ////////////////////////////////////////////////////////////
    }
    
    //fileprivate func updateServiceList(selectedIndex:Int = 0){
    func updateServiceList(){
        // Individual
        ////////////////////////////////////////////////////////////
        // The list of array to display. Can be changed dynamically
        dropDownService.optionArray = specificCategoryList.map{$0.listing_title ?? ""}
        //Its Id Values and its optional
        dropDownService.optionIds = specificCategoryList.map{Int($0.listing_id ?? "0") ?? 0}
        
        if(self.isAllowManualCat){
            dropDownService.optionArray.append("Enter New Field...")
            dropDownService.optionIds?.append(1001)
        }
        
        // The the Closure returns Selected Index and String
        dropDownService.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            
            if(id == 1001){
                if(self.inputNameCustomEntry != nil){
                    self.inputNameCustomEntry.isHidden = false
                    self.inputNameCustomEntry.input.text = ""
                }
                
                self.businessLocationInfo?.specificFieldName = selectedText
                self.businessLocationInfo?.specificField = "\(id)"
                
            } else {
                
                if(self.inputNameCustomEntry != nil){
                    self.inputNameCustomEntry.input.text = ""
                }
                
                self.inputNameCustomEntry.isHidden = true
                self.businessLocationInfo?.specificFieldName = selectedText
                self.businessLocationInfo?.specificField = "\(id)"
            }
        }
        ////////////////////////////////////////////////////////////
    }
    
    fileprivate func openNewSocialMediaTypeAlert(){
        self.businessNewTypeView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BusinessAddNewTypeViewController.nameOfClass) as BusinessAddNewTypeViewController
        
        self.businessNewTypeView?.titleText = "New Field Service Type"
        self.businessNewTypeView?.placeHolderText = "New Field Service"
        
        self.businessNewTypeView?.delegate = self
        self.businessNewTypeView?.modalPresentationStyle = .overFullScreen
        
        
        //        self.present(businessNewTypeView!, animated: true, completion: nil)
        
        HelperConstant.appDelegate.navigationController?.present(self.businessNewTypeView!, animated: true, completion: nil)
    }
}


// MARK:- UITableViewDataSource & UITableViewDelegate
extension BusinessInformationFieldCategoryView : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows: Int = 0
        if tableView == tableViewServiceClassification {
            rows = businessLocationInfo?.classification?.count ?? 0
        } else if tableView == tableViewServiceTechnique {
            rows = businessLocationInfo?.technique?.count ?? 0
        }
        return rows + 1
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        updateViewConstraints(tableView: tableView)

        if tableView == tableViewServiceClassification {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                
                cell.textField.placeholder = "Classification Title"
                cell.delegate = self
                cell.refTableView = self.tableViewServiceClassification
                cell.contentView.backgroundColor = UIColor.clear
                cell.backgroundColor = UIColor.clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                
                cell.delegate = self
                cell.refTableView = self.tableViewServiceClassification
                cell.lblTitle.text = businessLocationInfo?.classification?[indexPath.row-1]
                cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
                cell.contentView.backgroundColor = UIColor.clear
                cell.backgroundColor = UIColor.clear
                return cell
            }
        } else if tableView == tableViewServiceTechnique {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationAddMoreCell.nameOfClass) as! BusinessLocationAddMoreCell
                
                cell.textField.placeholder = techniqueTablePlaceHolder
                cell.delegate = self
                cell.refTableView = self.tableViewServiceTechnique
                cell.backgroundColor = UIColor.clear
                cell.contentView.backgroundColor = UIColor.clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: BusinessLocationTitleView.nameOfClass) as! BusinessLocationTitleView
                
                cell.delegate = self
                cell.refTableView = self.tableViewServiceTechnique
                cell.lblTitle.text = businessLocationInfo?.technique?[indexPath.row-1]
                cell.cellIndex = IndexPath(row: indexPath.row-1, section: indexPath.section)
                cell.backgroundColor = UIColor.clear
                cell.contentView.backgroundColor = UIColor.clear
                return cell
            }
        }
        
        return UITableViewCell(frame: tableView.frame)
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension BusinessInformationFieldCategoryView: BusinessLocationAddMoreCellDelegate{
    func newText(text: String, zipcode: String, refTableView: UITableView?) {
        
        if(refTableView == self.tableViewServiceClassification){
            if(self.businessLocationInfo?.classification?.count)! < HelperConstant.minimumBlocks{
                self.businessLocationInfo?.classification?.append(text)
                self.tableViewServiceClassification.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        } else if(refTableView == self.tableViewServiceTechnique){
            if(self.businessLocationInfo?.technique?.count)! < HelperConstant.minimumBlocks{
                self.businessLocationInfo?.technique?.append(text)
                self.tableViewServiceTechnique.reloadData()
            } else {
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
            }
        }
    }
}

extension BusinessInformationFieldCategoryView : BusinessLocationTitleViewDelegate{
    func removeCellInfo(cellIndex: IndexPath?, refTableView: UITableView?){
        
        if(refTableView == self.tableViewServiceClassification && self.businessLocationInfo != nil && (self.businessLocationInfo?.classification?.count)! > 0){
            self.businessLocationInfo?.classification?.remove(at: cellIndex?.row ?? 0)
            
            self.tableViewServiceClassification.reloadData()
            
        } else if(refTableView == self.tableViewServiceTechnique && self.businessLocationInfo != nil && (self.businessLocationInfo?.technique?.count)! > 0){
            self.businessLocationInfo?.technique?.remove(at: cellIndex?.row ?? 0)
            
            self.tableViewServiceTechnique.reloadData()
        }
    }
}

//MARK:- Button Action method implementation
extension BusinessInformationFieldCategoryView : BusinessNewTypeViewDelegate {
    
    func cancelViewScr(){
        if(businessNewTypeView != nil){
            businessNewTypeView = nil
        }
    }
    
    func addNewType(newType: String){
        print("New Type: \(newType)")
    }
}

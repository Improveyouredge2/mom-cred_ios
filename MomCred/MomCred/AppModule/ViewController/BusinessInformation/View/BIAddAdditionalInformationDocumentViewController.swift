//
//  BIAddAdditionalInformationDocumentViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 26/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit
import AVKit

/**
 * BIAddAdditionalInformationDocumentViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BIAddAdditionalInformationDocumentViewController : LMBaseViewController{
    
    @IBOutlet weak var inputName: RYFloatingInput!
    @IBOutlet weak var textViewDesc: KMPlaceholderTextView!
    
    @IBOutlet weak var documentImgCollectionView: UICollectionView!
    
    var arrDocumentImages = [UploadImageData]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTableView: UIView!
    
    fileprivate var businessAdditionalDocumentInfoList:[BusinessAdditionalDocumentInfo] = []
    fileprivate let formNumber = 13
    
    var biAddRequest:BIAddRequest?
    var optionType:String? = "2"
    
    fileprivate var arrServerDocumentImages:[UploadImageData]?
    
    fileprivate var presenter = BIAddAdditionalInformationDocumentPresenter()
    
    var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.clear
        
        documentImgCollectionView.backgroundColor = UIColor.clear
        
        // TODO: For testing
//        biAddRequest = BIAddRequest()
        
        self.setScreenData()
        
        presenter.connectView(view: self)
        
        self.setScreenData()
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(businessAdditionalDocumentInfoList.count == 0){
            self.viewTableView.isHidden = true
        } else {
            self.viewTableView.isHidden = false
        }
        
        if(self.tableView.contentSize.height > 0){
            self.tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
            
            self.updateTableViewHeight()
        }
    }

    override func pickedImage(fromImagePicker: UIImage, imageData: Data, mediaType:String, imageName:String, fileUrl: String?) {
        
        let imageDataTemp = UploadImageData()
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0 && self.biAddRequest?.busi_id == nil){
            let busi_id = UserDefault.getBIID() ?? ""
            imageDataTemp.id = busi_id
        } else if(self.biAddRequest?.busi_id != nil){
            let busi_id = self.biAddRequest?.busi_id
            imageDataTemp.id = busi_id
        }
        
        imageDataTemp.imageData = imageData
        imageDataTemp.imageName = "Test\(imageName)"
        imageDataTemp.imageKeyName = "busi_img"
        
        let theExt = (imageDataTemp.imageName! as NSString).pathExtension
        
        if(theExt.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpeg") == ComparisonResult.orderedSame || theExt.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame){
            imageDataTemp.imageType = "1"
        } else {
            imageDataTemp.imageType = "2"
        }
        
        if(optionType != nil && (optionType?.length)! > 0){
            imageDataTemp.mediaCategory = optionType ?? ""
        }
        
        self.arrDocumentImages.append(imageDataTemp)
        self.documentImgCollectionView.reloadData()
    }
    
    fileprivate func updateTableViewHeight() {
        UIView.animate(withDuration: 0, animations: {
            self.tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.constraintstableViewHeight.constant = heightOfTableView
        }
    }
}

//MARK:- Button Actions
//MARK:-
extension BIAddAdditionalInformationDocumentViewController {
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnAddImg(_ sender: UIButton) {
        self.inputName.input.resignFirstResponder()
        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String, "com.microsoft.word.doc", "org.openxmlformats.wordprocessingml.document"], in: .import)
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    /**
     *  AddImg button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func actionBtnDeleteStoreImg(_ sender: UIButton) {
        
        if(self.arrDocumentImages.count > 0){
            self.arrDocumentImages.remove(at: sender.tag)
            
            if self.arrDocumentImages.count == 0{
                
            }
            self.documentImgCollectionView.reloadData()
        }
    }
}

extension BIAddAdditionalInformationDocumentViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("didPickDocumentsAt: \(urls)")
        if let url = urls.first {
            let imageDataTemp = UploadImageData()
            
            if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0 && self.biAddRequest?.busi_id == nil){
                let busi_id = UserDefault.getBIID() ?? ""
                imageDataTemp.id = busi_id
            } else if(self.biAddRequest?.busi_id != nil){
                let busi_id = self.biAddRequest?.busi_id
                imageDataTemp.id = busi_id
            }
            
            imageDataTemp.filePath = url.path
            imageDataTemp.imageData = try? Data(contentsOf: url)
            if let filename = url.pathComponents.last {
                imageDataTemp.imageName = "Test_\(filename)"
            }
            imageDataTemp.imageKeyName = "busi_img"
            imageDataTemp.imageType = "2"
            
            if url.pathExtension.lowercased() == "pdf" {
                imageDataTemp.imageThumbnailData = UIImage(named: "ic_pdf")?.jpegData(compressionQuality: 1)
                imageDataTemp.mimetype = "application/pdf"
            } else {
                imageDataTemp.imageThumbnailData = UIImage(named: "ic_doc")?.jpegData(compressionQuality: 1)
                if url.pathExtension.lowercased() == "docx" {
                    imageDataTemp.mimetype = "application/vnd.openxmlformats-officedocument.wordprocessingml"
                } else {
                    imageDataTemp.mimetype = "application/msword"
                }
            }

            if let optionType = optionType, !optionType.isEmpty {
                imageDataTemp.mediaCategory = optionType
            }
            
            self.arrDocumentImages.append(imageDataTemp)
            self.documentImgCollectionView.reloadData()
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("documentPickerWasCancelled")
    }
}

extension BIAddAdditionalInformationDocumentViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func setScreenData(){
        
        inputName.setup(setting:
            RYFloatingInputSetting.Builder.instance()
                .backgroundColor(.clear)
                .accentColor(.white)
                .warningColor(.white)
                .placeholer("Document Name")
                .maxLength(HelperConstant.LIMIT_FULLNAME, onViolated: (message: "", callback: nil))
                .cursorColor(UIColor(hexString: ColorCode.cursorColor) ?? UIColor.black)
                .build()
        )
        
        if(self.biAddRequest != nil){
            
            if(self.biAddRequest?.doc != nil && (self.biAddRequest?.doc?.count)! > 0){
                
//                self.biAddRequest?.doc = self.businessAdditionalDocumentInfoList
                self.businessAdditionalDocumentInfoList = self.biAddRequest?.doc ?? []
                self.tableView.reloadData()
            }
        }
    }
}

extension BIAddAdditionalInformationDocumentViewController{
    @IBAction func methodNextAction(_ sender: UIButton){
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            if(!self.isDataUpdate()){
                if(self.isFormValid()){
//                    self.navigationController?.popToRootViewController(animated: true)
                    self.openNextScr()
                }
            } else if(self.isFormValid()){
                //            Helper.sharedInstance.showProgressHudViewWithTitle(title: "Updating...")
                
                self.uploadInfoOnServer()
                //            //TODO: Save information on server
                //            self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension BIAddAdditionalInformationDocumentViewController{
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        isUpdate = self.isUpdate
        
        return isUpdate
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0  && self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = ""
        }
        
        self.biAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        self.biAddRequest?.doc = self.businessAdditionalDocumentInfoList
        
        return true
    }
}

extension BIAddAdditionalInformationDocumentViewController{
    fileprivate func uploadInfoOnServer(){
        
        var isFound = false
        for businessDocumentInfo in self.businessAdditionalDocumentInfoList{
            if(businessDocumentInfo != nil && !businessDocumentInfo.uploadStatus && businessDocumentInfo.imageurl == nil){
                
                isFound = true
                presenter.submitData(businessAdditionalDocumentInfo: businessDocumentInfo, imageList: businessDocumentInfo.arrImages, callback: { (status, response, message) in
                    
                    if(status == true){
                        businessDocumentInfo.uploadStatus = true
//                        self.navigationController?.popToRootViewController(animated: true)
                        self.uploadInfoOnServer()
                    }
                })
                
                break
            }
        }
        
        if(!isFound){
            self.openNextScr()
        }
    }
    
    fileprivate func openNextScr(){
        let controllers = self.navigationController?.viewControllers
        for vc in controllers! {
            if vc is BIDetailViewController {
                _ = self.navigationController?.popToViewController(vc as! BIDetailViewController, animated: true)
            }
        }
    }
}

extension BIAddAdditionalInformationDocumentViewController{
    
    @IBAction func methodAddDocumentAction(_ sender: UIButton){
        
        var isValid = true
        let businessAdditionalDocumentInfo = BusinessAdditionalDocumentInfo()
        
        if((self.inputName.text()?.trim().length)! > 0){
            businessAdditionalDocumentInfo.media_title = self.inputName.text() ?? ""
            businessAdditionalDocumentInfo.name = self.inputName.text() ?? ""
            //businessAdditionalDocumentInfo.media_service = "1"
            businessAdditionalDocumentInfo.media_service = "0"
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_document_name.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if((self.textViewDesc.text?.trim().length)! > 0){
            businessAdditionalDocumentInfo.media_desc = self.textViewDesc.text ?? ""
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_document_description.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(arrDocumentImages.count > 0){
            
            if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0 && self.biAddRequest?.busi_id == nil){
                let busi_id = UserDefault.getBIID() ?? ""
                businessAdditionalDocumentInfo.busi_id = busi_id
            } else if(self.biAddRequest?.busi_id != nil){
                let busi_id = self.biAddRequest?.busi_id
                businessAdditionalDocumentInfo.busi_id = busi_id
            }
            
            businessAdditionalDocumentInfo.arrImages = arrDocumentImages
            
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_document_image_not_found.getLocalized(), buttonTitle: nil, controller: nil)
            
            return
        }
        
        if(isValid && self.businessAdditionalDocumentInfoList.count < HelperConstant.minimumBlocks){
            
            self.isUpdate = true
            
            self.inputName.input.text = ""
            self.textViewDesc.text = ""
            arrDocumentImages = []
            documentImgCollectionView.reloadData()
            self.businessAdditionalDocumentInfoList.append(businessAdditionalDocumentInfo)
            self.viewTableView.isHidden = false
            self.tableView.reloadData()
        } else {
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_you_have_reached_max_limit.getLocalized(), buttonTitle: nil, controller: nil)
        }
    }
    
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension BIAddAdditionalInformationDocumentViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return businessAdditionalDocumentInfoList.count
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationDocumentCell.nameOfClass) as! BusinessInformationAddAdditionalInformationDocumentCell
        
        let objInfo = businessAdditionalDocumentInfoList[indexPath.row]
        
        cell.cellIndex = indexPath
        cell.delegate = self
        
        if(objInfo.media_title != nil){
            cell.lblName.text = objInfo.media_title ?? ""
        } else {
            cell.lblName.text = objInfo.name
        }
        
        if(objInfo.media_desc != nil){
            cell.lblDesc.text = objInfo.media_desc ?? ""
        } else {
            cell.lblDesc.text = objInfo.desc
        }
        
        if let imageURLs = objInfo.imageurls, !imageURLs.isEmpty {
            cell.arrDocumentImagesUrl = imageURLs.compactMap { $0.thumb }
        } else {
            cell.arrDocumentImages = objInfo.arrImages?.compactMap { uploadData in
                var image: UIImage?
                if let thumbnailData = uploadData.imageThumbnailData {
                    image = UIImage(data: thumbnailData)
                } else if let imageData = uploadData.imageData {
                    image = UIImage(data: imageData)
                }
                return image
            }
        }
        
        cell.refreshScreenInfo()
        
        return cell
    }

    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension BIAddAdditionalInformationDocumentViewController:BusinessInformationAddAdditionalInformationDocumentCellDelegate{
    func zoomableImage(cellIndex: IndexPath?) {
        
    }
    
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
        let objInfo = businessAdditionalDocumentInfoList[cellIndex?.row ?? 0]
        if(objInfo.imageurl != nil && (objInfo.imageurl?.count)! > 0){
            let deleteMediaRequest = DeleteMediaRequest(media_id: objInfo.media_id ?? "")
            
            self.presenter.deleteMediaData(deleteMediaRequest: deleteMediaRequest, callback: {
                (status, response, message) in
                
                if(status){
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.success.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    self.isUpdate = true
                    if(self.businessAdditionalDocumentInfoList.count > 0){
                        self.businessAdditionalDocumentInfoList.remove(at: cellIndex?.row ?? 0)
                        self.tableView.reloadData()
                    }
                } else {
                    
                }
            })
            
        } else {
            if(self.businessAdditionalDocumentInfoList.count > 0){
                self.businessAdditionalDocumentInfoList.remove(at: cellIndex?.row ?? 0)
            }
            self.tableView.reloadData()
        }
        
        if(businessAdditionalDocumentInfoList.count == 0){
            self.viewTableView.isHidden = true
        }
    }
}


/*******************************************/
//MARK: - UICollectionViewDelegate
/*******************************************/
extension BIAddAdditionalInformationDocumentViewController : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if arrDocumentImages.count < 1{ // For 2 images
            return arrDocumentImages.count + 1
        }else{
            return arrDocumentImages.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //MARK:- Store Images views
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DocumentImageCVCell.nameOfClass, for: indexPath as IndexPath) as! DocumentImageCVCell
        
        if (arrDocumentImages.count == 0 || arrDocumentImages.count + 1 == indexPath.row + 1){
            //cell.btnAddImage.isUserInteractionEnabled = true
            cell.btnAddImage.isHidden = false
            cell.imgViewDocumnt.image = nil
            cell.btnDelete.isHidden = true
            
        } else {
            // cell.btnAddImage.isUserInteractionEnabled = false
            cell.btnAddImage.isHidden = true
            cell.imgViewDocumnt.clipsToBounds = true

            if let thumbnailData = arrDocumentImages[indexPath.row].imageThumbnailData {
                cell.imgViewDocumnt.image = UIImage(data: thumbnailData)
            } else if let imageData = arrDocumentImages[indexPath.row].imageData {
                cell.imgViewDocumnt.image = UIImage(data: imageData)
            }
            cell.btnDelete.isHidden = false
            cell.btnDelete.tag = indexPath.row
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
}

/********************************/
//MARK:- UICollectionViewCell
/*******************************/
class DocumentImageCVCell : UICollectionViewCell {
    
    @IBOutlet weak var imgViewDocumnt: UIImageView!
    @IBOutlet weak var btnAddImage: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var imgPlayButton : UIImageView!
}


/**
 * BusinessLocationAddMoreCellDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol BusinessInformationAddAdditionalInformationDocumentCellDelegate {
    func removeWebsiteInfo(cellIndex:IndexPath?)
    func zoomableImage(cellIndex:IndexPath?)
}

protocol DocumentSelectionDelegate: class {
    func didSelectDocument(at indexPath: IndexPath, inCellAt cellIndexPath: IndexPath)
}

class BusinessInformationAddAdditionalInformationDocumentCell: UITableViewCell {
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgCollectionView: UICollectionView!
    @IBOutlet weak var btnRemove: UIButton!
    
    var arrDocumentImages:[UIImage]?
    
    var arrDocumentImagesUrl:[String]?
    
    var cellIndex:IndexPath?
    var delegate:BusinessInformationAddAdditionalInformationDocumentCellDelegate?
    
    weak var documentSelectionDelegate: DocumentSelectionDelegate?
    
    override func awakeFromNib() {
        self.imgCollectionView.backgroundColor = UIColor.clear
    }
    
    func refreshScreenInfo(){
        self.imgCollectionView.reloadData()
    }
    
    @IBAction func methodCloseAction(_ sender: UIButton) {
        if(delegate != nil){
            delegate?.removeWebsiteInfo(cellIndex: cellIndex)
        }
    }
}

/*******************************************/
//MARK: - UICollectionViewDelegate
/*******************************************/
extension BusinessInformationAddAdditionalInformationDocumentCell : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDocumentImages?.count ?? (arrDocumentImagesUrl?.count ?? 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //MARK:- Store Images views
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DocumentImageCVCell.nameOfClass, for: indexPath as IndexPath) as! DocumentImageCVCell
        
        cell.btnAddImage.isHidden = true
        cell.imgViewDocumnt.clipsToBounds = true
        
        if let arrDocumentImages = arrDocumentImages {
            cell.imgViewDocumnt.image = arrDocumentImages[indexPath.row]
        } else if let imageURLs = arrDocumentImagesUrl {
            if let imageURL = URL(string: imageURLs[indexPath.item]) {
                let pathExtension = imageURL.pathExtension.lowercased()
                if ["jpg", "jpeg", "png", "gif"].contains(pathExtension) {
                    cell.imgViewDocumnt.sd_setImage(with: imageURL)
                } else if pathExtension == "pdf" {
                    cell.imgViewDocumnt.image = UIImage(named: "ic_pdf")
                } else {
                    cell.imgViewDocumnt.image = UIImage(named: "ic_doc")
                }
            } else {
                cell.imgViewDocumnt.image = UIImage(named: "ic_doc")
            }
        }
        
        cell.btnDelete.isHidden = true
        cell.btnDelete.tag = indexPath.row
        let g = UIGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        cell.imgViewDocumnt.isUserInteractionEnabled = true
        cell.imgViewDocumnt.addGestureRecognizer(g)
        
        return cell
    }
    
    @objc func tapped(sender : UIImageView){
        print("fdsfdf")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(delegate != nil){
            delegate?.zoomableImage(cellIndex: cellIndex)
        }
        
        if let cellIndex = cellIndex {
            documentSelectionDelegate?.didSelectDocument(at: indexPath, inCellAt: cellIndex)
        }
    }
}

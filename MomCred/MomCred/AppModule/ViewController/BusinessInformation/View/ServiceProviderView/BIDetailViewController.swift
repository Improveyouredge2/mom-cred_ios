//
//  BIDetailViewController.swift
//  MomCred
//
//  Created by consagous on 26/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import Lightbox
import AVKit
import ImageSlideshow

/**
 * BIDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagbtnous [CNSGSIN054]
 */
class BIDetailViewController : LMBaseViewController {
    
    @IBOutlet weak var btnOptionMenu : UIButton!
    
    // Profile image view
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn : UIImageView!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitleValue : UILabel!
    
    @IBOutlet weak var lblBIName : UILabel!
    @IBOutlet weak var lblBIServiceProviderType : UILabel!
    @IBOutlet weak var lblBIServiceProviderSubType : UILabel!
    @IBOutlet weak var lblBIBusinessEntityStatus : UILabel!
    @IBOutlet weak var lblBIBusinessType : UILabel!
    
    @IBOutlet weak var lblBIDesc : UILabel!
    @IBOutlet weak var lblBIMissionStmt : UILabel!
    
    // Location display
    @IBOutlet weak var tableViewLocation : BIDetailTableView!
    @IBOutlet weak var constraintTableViewLocationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblBIPrimaryEmail : UILabel! {
        didSet {
            lblBIPrimaryEmail.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleEmailTap(_:)))
            lblBIPrimaryEmail.addGestureRecognizer(tapGesture)
        }
    }
    @IBOutlet weak var lblBIPrimaryEmailDesc : UILabel!
    @IBOutlet weak var lblBIPrimaryEmailAffiliation : UILabel!
    
    @IBOutlet weak var lblBISecondaryEmail : UILabel! {
        didSet {
            lblBISecondaryEmail.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleEmailTap(_:)))
            lblBISecondaryEmail.addGestureRecognizer(tapGesture)
        }
    }
    @IBOutlet weak var lblBISecondaryEmailDesc : UILabel!
    @IBOutlet weak var lblBISecondaryEmailAffiliation : UILabel!
    
    @IBOutlet weak var lblBIPrimaryContact : UILabel! {
        didSet {
            lblBIPrimaryContact.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handlePhoneTap(_:)))
            lblBIPrimaryContact.addGestureRecognizer(tapGesture)
        }
    }
    @IBOutlet weak var lblBIPrimaryContactDesc : UILabel!
    @IBOutlet weak var lblBIPrimaryContactAffiliation : UILabel!
    
    @IBOutlet weak var lblBISecondaryContact : UILabel! {
        didSet {
            lblBISecondaryContact.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handlePhoneTap(_:)))
            lblBISecondaryContact.addGestureRecognizer(tapGesture)
        }
    }
    @IBOutlet weak var lblBISecondaryContactDesc : UILabel!
    @IBOutlet weak var lblBISecondaryContactAffiliation : UILabel!
    
    // Website display
    @IBOutlet weak var tableViewWebsite : BIDetailTableView!
    @IBOutlet weak var constraintTableViewWebsiteHeight: NSLayoutConstraint!
    
    // Social Media display
    @IBOutlet weak var tableViewSocialMedia : BIDetailTableView!
    @IBOutlet weak var constraintTableViewSocialMediaHeight: NSLayoutConstraint!
    
    //Exceptional
    @IBOutlet weak var lblBIExceptionalType : UILabel!
    @IBOutlet weak var lblBIExceptionalOfferServiceStatus : UILabel!
    @IBOutlet weak var lblBIPrimaryExceptionalStatus : UILabel!
    @IBOutlet weak var lblBIExceptionalClassificationOffered : UILabel!
    
    // Field Service type display
    @IBOutlet weak var tableViewFieldServiceType : BIDetailTableView!
    @IBOutlet weak var constraintTableViewFieldServiceTypeHeight: NSLayoutConstraint!
    
    // Serivce type
    @IBOutlet weak var lblBIServiceType : UILabel!
    @IBOutlet weak var lblBIPunchabilityStatus : UILabel!
    
    // Qualification Accredation type display
    @IBOutlet weak var tableViewAccredation : BIDetailTableView!
    @IBOutlet weak var constraintTableViewAccredationHeight: NSLayoutConstraint!
    
    // Partnership Accredation type display
    @IBOutlet weak var tableViewPartnership : BIDetailTableView!
    @IBOutlet weak var constraintTableViewPartnershipHeight: NSLayoutConstraint!
    
    // Partnership Award type display
    @IBOutlet weak var tableViewAward : BIDetailTableView!
    @IBOutlet weak var constraintTableViewAwardHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblYearDelExp : UILabel!
    
    // Additional Link type display
    @IBOutlet weak var tableViewAdditionalLink : BIDetailTableView!
    @IBOutlet weak var constraintTableViewAdditionalLinkHeight: NSLayoutConstraint!
    
    // Additional Document type display
    @IBOutlet weak var tableViewAdditionalDocument : BIDetailTableView!
    @IBOutlet weak var constraintTableViewAdditionalDocumentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var viewCreateNew : UIView!
    
    @IBOutlet weak var viewServiceProviderDetail : UIView!
    @IBOutlet weak var viewServiceProviderEnthuaistDetail : UIView!
    
    // Service Provider detail for Enthauist
    @IBOutlet weak var slideshow: ImageSlideshow!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblReviewCount : UILabel!
    @IBOutlet weak var imgServiceProviderProfile : ImageLayerSetup!
    
    @IBOutlet weak var starRatingView : HCSStarRatingView!
    @IBOutlet weak var btnRating: UIButton!

    fileprivate var menuArray: [HSMenu] = []
    fileprivate var presenter = BIDetailViewPresenter()
    fileprivate var biaddOverViewViewController:BIAddOverViewViewController?
    fileprivate var defaultDocumentCollectionHeight:CGFloat = 0.00
    
    var biAddRequest:BIAddRequest?
    var isLocalBusinessProfile: Bool = false
    var shouldRefreshContent: Bool = false
    
    fileprivate var instructionalServiceProviderListingViewController:InstructionalServiceProviderListingViewController?
    
    fileprivate var ratingReviewViewController:RatingReviewViewController?
    
    var collectionContentHeightObserver: NSKeyValueObservation?

    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.connectView(view: self)
        
        self.scrollView.isHidden = true
        self.btnOptionMenu.isHidden = true
        self.defaultDocumentCollectionHeight = self.constraintDocumentCollectionHeight.constant
        if PMUserDefault.getAppToken() == nil || PMUserDefault.getAppToken()?.trim().isEmpty == true {
            btnRating.isHidden = true
        }

        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.edit.getLocalized())
        menuArray = [menu1]
        
        let profileImageUrl = PMUserDefault.getProfilePic()
        if(profileImageUrl != nil){
            self.imgProfile.sd_setImage(with: URL(string: (profileImageUrl!)))
        }
        
        tableViewLocation.sizeDelegate = self
        tableViewWebsite.sizeDelegate = self
        tableViewSocialMedia.sizeDelegate = self
        tableViewFieldServiceType.sizeDelegate = self
        tableViewAccredation.sizeDelegate = self
        tableViewPartnership.sizeDelegate = self
        tableViewAward.sizeDelegate = self
        tableViewAdditionalLink.sizeDelegate = self
        tableViewAdditionalDocument.sizeDelegate = self

        self.constraintTableViewLocationHeight.constant = 0
        self.constraintTableViewWebsiteHeight.constant = 0
        self.constraintTableViewSocialMediaHeight.constant = 0
        self.constraintTableViewFieldServiceTypeHeight.constant = 0
        self.constraintTableViewAccredationHeight.constant = 0
        self.constraintTableViewPartnershipHeight.constant = 0
        self.constraintTableViewAwardHeight.constant = 0
        self.constraintTableViewAdditionalLinkHeight.constant = 0
        self.constraintTableViewAdditionalDocumentHeight.constant = 0
        
        // TODO: Remove after api success
        self.updateScreenInfo()
        
        collectionContentHeightObserver = collectionView.observe(\.contentSize, options: [.new]) { [weak self] _, value in
            if let height = value.newValue?.height, height > 0 {
                self?.constraintDocumentCollectionHeight.constant = height
            }
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.biaddOverViewViewController = nil

        // update screen info with API calling
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            if biAddRequest == nil {
                presenter.getBIDetail()
            }
//        case AppUser.LocalBusiness.rawValue:
//        case AppUser.Enthusiast.rawValue:
//            break
        default: break
        }
        
    }

    deinit {
        collectionContentHeightObserver?.invalidate()
    }
}

extension BIDetailViewController{
    @IBAction func actionCreateBI(_ sender: UIButton) {
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            self.biaddOverViewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddOverViewViewController.nameOfClass) as BIAddOverViewViewController
            
                    UserDefault.removeBIID()
            
            self.navigationController?.pushViewController(self.biaddOverViewViewController!, animated: true)
        }
    }
    
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
    
    @IBAction func actionZoomOption(_ sender: AnyObject) {
        zoomableView()
    }
    
    //MARK:- Enthusiast user action
    @IBAction func actionListingOffer(_ sender: UIButton) {
        
        if(self.instructionalServiceProviderListingViewController == nil){
            self.instructionalServiceProviderListingViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalServiceProviderStoryboard, viewControllerName: InstructionalServiceProviderListingViewController.nameOfClass) as InstructionalServiceProviderListingViewController
            
            self.instructionalServiceProviderListingViewController?.busiInfo = self.biAddRequest
            self.navigationController?.pushViewController(self.instructionalServiceProviderListingViewController!, animated: true)
        }
    }
    
    @IBAction func actionRating(_ sender: UIButton) {
        
        if(self.ratingReviewViewController == nil){
            self.ratingReviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalServiceProviderStoryboard, viewControllerName: RatingReviewViewController.nameOfClass) as RatingReviewViewController
            
            self.ratingReviewViewController?.busiInfo = self.biAddRequest
            self.navigationController?.pushViewController(self.ratingReviewViewController!, animated: true)
        }
        
    }
    
    @objc private func handleEmailTap(_ gestureRecognizer: UITapGestureRecognizer) {
        if let label = gestureRecognizer.view as? UILabel, let email = label.text?.trim(), !email.isEmpty {
            openMail(for: email)
        }
    }
    
    @objc private func handlePhoneTap(_ gestureRecognizer: UITapGestureRecognizer) {
        if let label = gestureRecognizer.view as? UILabel, let phone = label.text?.trim(), !phone.isEmpty, let url = URL(string: "tel:\(phone)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

extension BIDetailViewController: HSPopupMenuDelegate{
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if(index == 0 && self.biaddOverViewViewController == nil){
            
            self.biaddOverViewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddOverViewViewController.nameOfClass) as BIAddOverViewViewController
            
            self.biaddOverViewViewController?.biAddRequest = self.biAddRequest
            biaddOverViewViewController?.isEditingBusiness = true
            self.navigationController?.pushViewController(self.biaddOverViewViewController!, animated: true)
            
        }
    }
    
    func updateScreenInfo(){
        if(biAddRequest != nil){
            // TODO: Update all controls
            // update Business Information media file
            
            let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
            
            switch  role_id {
            case AppUser.ServiceProvider.rawValue:
                
                self.viewServiceProviderDetail.isHidden = false
                self.viewServiceProviderEnthuaistDetail.isHidden = true
                
                self.constraintDocumentCollectionHeight.constant = self.defaultDocumentCollectionHeight
                
                self.collectionView.reloadData()
                if(self.biAddRequest?.mediabusiness != nil && (self.biAddRequest?.mediabusiness?.count)! > 0){
                    self.imgProfile.sd_setImage(with: URL(string: (self.biAddRequest?.mediabusiness![0].thumb ?? "")))
                    
                    // Check video on first pos
                    if((self.biAddRequest?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                        self.imgPlayBtn.isHidden = false
                    } else {
                        self.imgPlayBtn.isHidden = true
                    }
                    
                    
                } else {
                    self.imgPlayBtn.isHidden = true
                    self.imgProfile.image = nil
                    self.constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
                }
                break
            case AppUser.LocalBusiness.rawValue:
                
                break
            case AppUser.Enthusiast.rawValue:
                self.viewServiceProviderDetail.isHidden = true
                self.viewServiceProviderEnthuaistDetail.isHidden = false
                /////////////////////////////////////////////////////////////
                //        slideshow.backgroundColor = UIColor.white
                slideshow.slideshowInterval = 3.0
                slideshow.pageControlPosition = PageControlPosition.insideScrollView
                slideshow.pageControl.currentPageIndicatorTintColor = UIColor.white
                slideshow.pageControl.pageIndicatorTintColor = UIColor.lightGray
                slideshow.contentScaleMode = UIView.ContentMode.scaleAspectFill
                
                //TODO: Remove when image downalod from server api
                ///////////////////////////////////////////////////////////////
                if(self.biAddRequest?.mediabusiness != nil && (self.biAddRequest?.mediabusiness?.count)! > 0){
                    var sdWebImageSource:[SDWebImageSource] = []
                    for tempImageUrl in (self.biAddRequest?.mediabusiness)! {
                        sdWebImageSource.append(SDWebImageSource(urlString: tempImageUrl.thumb ?? "")!)
                    }
                    self.slideshow.setImageInputs(sdWebImageSource)
                    
                    // Business Media first image use as thumbnail
                    self.imgServiceProviderProfile.sd_setImage(with: URL(string: (self.biAddRequest?.mediabusiness![0].thumb ?? "")), completed: nil)
                }
                ///////////////////////////////////////////////////////////////
                
                // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
                slideshow.activityIndicator = DefaultActivityIndicator()
                slideshow.currentPageChanged = { page in
                    //            print("current page:", page)
                }
                /////////////////////////////////////////////////////////////
                
                self.lblTitle.text = self.biAddRequest?.busi_title
                self.lblSubTitle.text = self.biAddRequest?.busi_statement
                self.starRatingView.value = CGFloat(truncating: self.biAddRequest?.rating ?? 0)
                break
            default:
                break
            }
            
            self.lblBIName.text = biAddRequest?.busi_title ?? ""
            self.lblBIDesc.text = biAddRequest?.busi_desc ?? ""
            self.lblBIMissionStmt.text = biAddRequest?.busi_statement ?? ""
            
            self.lblBIServiceProviderType.text = biAddRequest?.busi_service_provider_title ?? ""
            self.lblBIServiceProviderSubType.text = biAddRequest?.busi_service_provider_type_title ?? ""
            self.lblBIBusinessEntityStatus.text = (biAddRequest?.busi_entity_title != nil && (biAddRequest?.busi_entity_title?.length)! > 0) ? LocalizationKeys.btn_yes.getLocalized() : LocalizationKeys.btn_no.getLocalized()
            self.lblBIBusinessType.text = biAddRequest?.busi_entity_title ?? ""
            
            // Primary Email
            if let contactInfo: ContactInfo = biAddRequest?.busi_email {
                lblBIPrimaryEmail.text = contactInfo.name
                lblBIPrimaryEmailDesc.text = contactInfo.desc
                if let affiliations = contactInfo.affilatedList, !affiliations.isEmpty {
                    let titles = affiliations.joined(separator: "\n• ")
                    lblBIPrimaryEmailAffiliation.text = "• \(titles)"
                } else {
                    lblBIPrimaryEmailAffiliation.text = ""
                }
            } else {
                lblBIPrimaryEmail.text = ""
                lblBIPrimaryEmailDesc.text = ""
                lblBIPrimaryEmailAffiliation.text = ""
            }
            
            // Secondary Email
            if let contactInfo: ContactInfo = biAddRequest?.busi_email_sec {
                lblBISecondaryEmail.text = contactInfo.name
                lblBISecondaryEmailDesc.text = contactInfo.desc
                if let affiliations = contactInfo.affilatedList, !affiliations.isEmpty {
                    let titles = affiliations.joined(separator: "\n• ")
                    lblBISecondaryEmailAffiliation.text = "• \(titles)"
                } else {
                    lblBISecondaryEmailAffiliation.text = ""
                }
            } else {
                self.lblBISecondaryEmail.text =  ""
                self.lblBISecondaryEmailDesc.text = ""
                self.lblBISecondaryEmailAffiliation.text = ""
            }
            
            // Primary Contact
            if let contactInfo: ContactInfo = biAddRequest?.busi_phone {
                lblBIPrimaryContact.text = contactInfo.name
                lblBIPrimaryContactDesc.text = contactInfo.desc
                if let affiliations = contactInfo.affilatedList, !affiliations.isEmpty {
                    let titles = affiliations.joined(separator: "\n• ")
                    lblBIPrimaryContactAffiliation.text = "• \(titles)"
                } else {
                    lblBIPrimaryContactAffiliation.text = ""
                }
            } else {
                lblBIPrimaryContact.text = ""
                lblBIPrimaryContactDesc.text = ""
                lblBIPrimaryContactAffiliation.text = ""
            }
            
            // Secondary Contact
            if let contactInfo: ContactInfo = biAddRequest?.busi_phone_sec {
                lblBISecondaryContact.text = contactInfo.name
                lblBISecondaryContactDesc.text = contactInfo.desc
                if let affiliations = contactInfo.affilatedList, !affiliations.isEmpty {
                    let titles = affiliations.joined(separator: "\n• ")
                    lblBISecondaryContactAffiliation.text = "• \(titles)"
                } else {
                    lblBISecondaryContactAffiliation.text = ""
                }
            } else {
                lblBISecondaryContact.text = ""
                lblBISecondaryContactDesc.text = ""
                lblBISecondaryContactAffiliation.text = ""
            }

//            case standard
//            case exceptional
//            case std_exp
            // Exceptional Service
            lblBIExceptionalType.text = LocalizationKeys.standard.getLocalized()
            lblBIExceptionalOfferServiceStatus.text = LocalizationKeys.btn_no.getLocalized()
            
            if let exceptionalService = biAddRequest?.service_provider_classification {
                if exceptionalService == "1" {
                    lblBIExceptionalType.text = LocalizationKeys.standard.getLocalized()
                    lblBIExceptionalOfferServiceStatus.text = LocalizationKeys.btn_no.getLocalized()
                } else if exceptionalService == "2" {
                    lblBIExceptionalType.text = LocalizationKeys.exceptional.getLocalized()
                    lblBIExceptionalOfferServiceStatus.text = LocalizationKeys.btn_yes.getLocalized()
                } else if exceptionalService == "3" {
                    lblBIExceptionalType.text = LocalizationKeys.std_exp.getLocalized()
                    lblBIExceptionalOfferServiceStatus.text = LocalizationKeys.btn_yes.getLocalized()
                }
            }

            if let primarilyExceptional = biAddRequest?.primarily_exceptional, primarilyExceptional == "1" {
                lblBIPrimaryExceptionalStatus.text = LocalizationKeys.btn_yes.getLocalized()
            } else {
                lblBIPrimaryExceptionalStatus.text = LocalizationKeys.btn_no.getLocalized()
            }

            if let offeredTitles = biAddRequest?.exceptional_classification_offered_title, !offeredTitles.isEmpty {
                let titles = offeredTitles.joined(separator: "\n• ")
                lblBIExceptionalClassificationOffered.text = "• \(titles)"
            }
            
            // Exp
            self.lblYearDelExp.text = biAddRequest?.deliveryExp
            /*
            //  update constant info
            if(self.biAddRequest != nil && ((self.biAddRequest?.location_address_sec != nil && (self.biAddRequest?.location_address_sec?.length)! > 0))){
                self.constraintTableViewLocationHeight.constant = 5
            } else {
                self.constraintTableViewLocationHeight.constant = 0
            }
            
            if((self.biAddRequest != nil && self.biAddRequest?.busi_weblink_list != nil) && (self.biAddRequest?.busi_weblink_list?.count)! > 0){
                self.constraintTableViewWebsiteHeight.constant = 5
            } else {
                self.constraintTableViewWebsiteHeight.constant = 0
            }
            
            if((self.biAddRequest != nil && self.biAddRequest?.busi_sociallink_list != nil) && (self.biAddRequest?.busi_sociallink_list?.count)! > 0){
                self.constraintTableViewSocialMediaHeight.constant = 5
            } else {
                self.constraintTableViewSocialMediaHeight.constant = 0
            }
            
            if((self.biAddRequest != nil && self.biAddRequest?.fieldServiceType != nil) && (self.biAddRequest?.fieldServiceType?.count)! > 0){
                self.constraintTableViewFieldServiceTypeHeight.constant = 5
            } else {
                self.constraintTableViewFieldServiceTypeHeight.constant = 0
            }
            
            if((self.biAddRequest != nil && self.biAddRequest?.accreditations != nil) && (self.biAddRequest?.accreditations?.count)! > 0){
                self.constraintTableViewAccredationHeight.constant = 5
            } else {
                self.constraintTableViewAccredationHeight.constant = 0
            }
            
            if((self.biAddRequest != nil && self.biAddRequest?.affiliation != nil) && (self.biAddRequest?.affiliation?.count)! > 0){
                self.constraintTableViewPartnershipHeight.constant = 5
            } else {
                self.constraintTableViewPartnershipHeight.constant = 0
            }
            
            if((self.biAddRequest != nil && self.biAddRequest?.award != nil) && (self.biAddRequest?.award?.count)! > 0){
                self.constraintTableViewAwardHeight.constant = 5
            } else {
                self.constraintTableViewAwardHeight.constant = 0
            }
            
            if((self.biAddRequest != nil && self.biAddRequest?.additional_link != nil) && (self.biAddRequest?.additional_link?.count)! > 0){
                self.constraintTableViewAdditionalLinkHeight.constant = 5
            } else {
                self.constraintTableViewAdditionalLinkHeight.constant = 0
            }
            
            if((self.biAddRequest != nil && self.biAddRequest?.doc != nil) && (self.biAddRequest?.doc?.count)! > 0){
                self.constraintTableViewAdditionalDocumentHeight.constant = 5
            } else {
                self.constraintTableViewAdditionalDocumentHeight.constant = 0
            }
            */
            self.tableViewLocation.reloadData()
            self.tableViewWebsite.reloadData()
            self.tableViewSocialMedia.reloadData()
            self.tableViewFieldServiceType.reloadData()
            self.tableViewAccredation.reloadData()
            self.tableViewPartnership.reloadData()
            self.tableViewAward.reloadData()
            self.tableViewAdditionalLink.reloadData()
            self.tableViewAdditionalDocument.reloadData()

            self.scrollView.isHidden = false
            self.viewCreateNew.isHidden = true
            self.btnOptionMenu.isHidden = false
        } else {
            self.viewCreateNew.isHidden = false
            self.scrollView.isHidden = true
            self.btnOptionMenu.isHidden = true
        }
        
//         self.scrollView.isHidden = false
//         self.viewCreateNew.isHidden = true
        
        if PMUserDefault.getAppToken() == nil || PMUserDefault.getAppToken()?.trim().isEmpty == true {
            btnOptionMenu.isHidden = true
        }
        
        
    }
    
    fileprivate func zoomableView(){
        // Create an array of images.
        var images:[LightboxImage] = []
        
        if(self.biAddRequest?.mediabusiness != nil && (self.biAddRequest?.mediabusiness?.count)! > 0){
            
            for media in (self.biAddRequest?.mediabusiness)! {
                //            self.imgProfile.sd_setImage(with: URL(string: (self.biAddRequest?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((media.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    
                    images.append(LightboxImage(imageURL: URL(string: media.thumb ?? "")!, text: "", videoURL: URL(string: media.imageurl ?? "")!))
                    
                } else {
                    images.append(LightboxImage(
                        imageURL: URL(string: media.imageurl ?? "")!
                    ))
                }
            }
        }
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)
        // Present your controller.
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
        
        LightboxConfig.handleVideo = { from, videoURL in
            // Custom video handling
            let videoController = AVPlayerViewController()
            videoController.player = AVPlayer(url: videoURL)
            
            from.present(videoController, animated: true) {
                videoController.player?.play()
            }
        }
    }
}

extension BIDetailViewController: LightboxControllerPageDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension BIDetailViewController: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}

extension BIDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    /**
     *  Specify height of row in tableview with UITableViewDelegate method override.
     *
     *  @param key tableView object and heightForRowAt index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    /**
     *  Specify number of section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 2
        if(tableView == self.tableViewLocation){
            return self.biAddRequest != nil ? ((self.biAddRequest?.location_address_sec != nil && (self.biAddRequest?.location_address_sec?.length)! > 0) ? 2 : (self.biAddRequest?.location_address != nil && (self.biAddRequest?.location_address?.length)! > 0) ? 1 : 0) : 0
        } else if(tableView == self.tableViewWebsite){
            return (self.biAddRequest != nil && self.biAddRequest?.busi_weblink_list != nil) ? (self.biAddRequest?.busi_weblink_list?.count)! : 0
        } else if(tableView == self.tableViewSocialMedia){
            return (self.biAddRequest != nil && self.biAddRequest?.busi_sociallink_list != nil) ? (self.biAddRequest?.busi_sociallink_list?.count)! : 0
        } else if(tableView == self.tableViewFieldServiceType){
            return (self.biAddRequest != nil && self.biAddRequest?.fieldServiceType != nil) ? (self.biAddRequest?.fieldServiceType?.count)! : 0
        } else if(tableView == self.tableViewAccredation){
            return (self.biAddRequest != nil && self.biAddRequest?.accreditations != nil) ? (self.biAddRequest?.accreditations?.count)! : 0
        } else if(tableView == self.tableViewPartnership){
            return (self.biAddRequest != nil && self.biAddRequest?.affiliation != nil) ? (self.biAddRequest?.affiliation?.count)! : 0
        } else if(tableView == self.tableViewAward){
            return (self.biAddRequest != nil && self.biAddRequest?.award != nil) ? (self.biAddRequest?.award?.count)! : 0
        } else if(tableView == self.tableViewAdditionalLink){
            return (self.biAddRequest != nil && self.biAddRequest?.additional_link != nil) ? (self.biAddRequest?.additional_link?.count)! : 0
        } else if(tableView == self.tableViewAdditionalDocument){
            return (self.biAddRequest != nil && self.biAddRequest?.doc != nil) ? (self.biAddRequest?.doc?.count)! : 0
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        updateViewConstraints(tableView: tableView)
        
        if(tableView == self.tableViewLocation){
            let cell = tableView.dequeueReusableCell(withIdentifier: LocationListCell.nameOfClass) as! LocationListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.addStackView(locationHour: self.biAddRequest!.location_busi_hours!)
            if(indexPath.row == 0){
                cell.lblLocationTitle.text = "Primary Location"
                cell.lblExclusivelyTravel.text = (self.biAddRequest?.location_city != nil && (self.biAddRequest?.location_city?.length)! > 0) ? LocalizationKeys.btn_yes.getLocalized() : LocalizationKeys.btn_no.getLocalized()
                cell.lblAddress.text = self.biAddRequest?.location_address ?? ""
                cell.lblCity.text = biAddRequest?.location_city
                cell.lblState.text = biAddRequest?.location_state
                cell.lblLocationType.text = (self.biAddRequest?.location_type != nil && self.biAddRequest?.location_type == "1") ? LocalizationKeys.private_str.getLocalized() : LocalizationKeys.public_str.getLocalized()
                cell.lblBelong.text = (self.biAddRequest?.location_ownership != nil && self.biAddRequest?.location_ownership == "1") ? LocalizationKeys.btn_yes.getLocalized() : LocalizationKeys.btn_no.getLocalized()
                
                if let classifications = biAddRequest?.location_classification, !classifications.isEmpty {
                    let titles = classifications.joined(separator: "\n• ")
                    cell.lblLocationClassification.text = "• \(titles)"
                } else {
                    cell.lblLocationClassification.text = ""
                }

                if let speciality = biAddRequest?.location_speciality, !speciality.isEmpty {
                    let titles = speciality.joined(separator: "\n• ")
                    cell.lblLocationSpeciality.text = "• \(titles)"
                } else {
                    cell.lblLocationSpeciality.text = ""
                }
            } else {
                cell.lblLocationTitle.text = "Secondary Location"
                cell.addStackView(locationHour: self.biAddRequest!.location_busi_hours_sec!)
                cell.lblExclusivelyTravel.text = (self.biAddRequest?.location_city_sec != nil && (self.biAddRequest?.location_city_sec?.length)! > 0) ? LocalizationKeys.btn_yes.getLocalized() : LocalizationKeys.btn_no.getLocalized()
                cell.lblAddress.text = self.biAddRequest?.location_address_sec ?? ""
                cell.lblCity.text = biAddRequest?.location_city_sec
                cell.lblState.text = biAddRequest?.location_state_sec
                cell.lblLocationType.text = (self.biAddRequest?.location_type_sec != nil && self.biAddRequest?.location_type_sec == "1") ? LocalizationKeys.private_str.getLocalized() : LocalizationKeys.public_str.getLocalized()
                cell.lblBelong.text = (self.biAddRequest?.location_ownership_sec != nil && self.biAddRequest?.location_ownership_sec == "1") ? LocalizationKeys.btn_yes.getLocalized() : LocalizationKeys.btn_no.getLocalized()

                if let classifications = biAddRequest?.location_classification_sec, !classifications.isEmpty {
                    let titles = classifications.joined(separator: "\n• ")
                    cell.lblLocationClassification.text = "• \(titles)"
                } else {
                    cell.lblLocationClassification.text = ""
                }
                if let speciality = biAddRequest?.location_speciality, !speciality.isEmpty {
                    let titles = speciality.joined(separator: "\n• ")
                    cell.lblLocationSpeciality.text = "• \(titles)"
                } else {
                    cell.lblLocationSpeciality.text = ""
                }
            }

            return cell
        } else if(tableView == self.tableViewWebsite){
            let cell = tableView.dequeueReusableCell(withIdentifier: WebsiteListCell.nameOfClass) as! WebsiteListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let data = biAddRequest?.busi_weblink_list?[indexPath.row]
            cell.lblWebsiteIndex.text = "Website \(indexPath.row + 1)"
            cell.lblMainWebsiteStatus.text = (
                data?.mainWebsite?.boolValue ?? false) ? LocalizationKeys.btn_yes.getLocalized() : LocalizationKeys.btn_no.getLocalized()
            cell.lblLink.text = data?.webLinkTitle ?? ""
            cell.lblDesc.text = data?.webLinkDesc ?? ""
            
            return cell
        } else if(tableView == self.tableViewSocialMedia) {
            let cell = tableView.dequeueReusableCell(withIdentifier: WebsiteListCell.nameOfClass) as! WebsiteListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let data = biAddRequest?.busi_sociallink_list?[indexPath.row]
            cell.lblWebsiteIndex.text = "Social Media \(indexPath.row + 1)"
            cell.lblMainWebsiteStatus.text = data?.socialMediaName ?? ""
            cell.lblLink.text = data?.webLinkTitle ?? ""
            cell.lblDesc.text = data?.webLinkDesc ?? ""
            
            return cell
        } else if(tableView == self.tableViewFieldServiceType){
            let cell = tableView.dequeueReusableCell(withIdentifier: FieldServiceListCell.nameOfClass) as! FieldServiceListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
//            let data = biAddRequest?.fieldServiceType?[indexPath.row]
//
//            let fieldServicetype:BusinessFieldServiceCategoryInfo? = BusinessFieldServiceCategoryInfo().getModelObjectFromServerResponse(jsonResponse: data! as AnyObject)
            
            let fieldServicetype:BusinessFieldServiceCategoryInfo? = biAddRequest?.fieldServiceType?[indexPath.row]
            
            if(fieldServicetype != nil){
                cell.lblFieldIndex.text = "Field \(indexPath.row + 1)"
                cell.lblCategoryStatus.text = fieldServicetype?.catFieldName ?? ""
                cell.lblField.text = fieldServicetype?.specificFieldName ?? ""
                
                if let classification = fieldServicetype?.classification, !classification.isEmpty {
                    let titles = classification.joined(separator: "\n• ")
                    cell.lblClassification.text = "• \(titles)"
                } else {
                    cell.lblClassification.text = ""
                }
                if let technique = fieldServicetype?.technique, !technique.isEmpty {
                    let titles = technique.joined(separator: "\n• ")
                    cell.lblTechnique.text = "• \(titles)"
                } else {
                    cell.lblTechnique.text = ""
                }
            }
            
            return cell
        } else if(tableView == self.tableViewAccredation){
            let cell = tableView.dequeueReusableCell(withIdentifier: AccreditationListCell.nameOfClass) as! AccreditationListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let data = biAddRequest?.accreditations?[indexPath.row]
            cell.lblIndex.text = "Accreditation \(indexPath.row + 1)"
            cell.lblName.text = data?.name ?? ""
            cell.lblDesc.text = data?.desc ?? ""
            cell.lblLink.text = data?.link ?? ""
            
            return cell
        } else if(tableView == self.tableViewPartnership){
            let cell = tableView.dequeueReusableCell(withIdentifier: AccreditationListCell.nameOfClass) as! AccreditationListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let data = biAddRequest?.affiliation?[indexPath.row]
            cell.lblIndex.text = "Affiliation / Partnership \(indexPath.row + 1)"
            cell.lblName.text = data?.name ?? ""
            cell.lblDesc.text = data?.desc ?? ""
            cell.lblLink.text = data?.link ?? ""
            
            return cell
        } else if(tableView == self.tableViewAward){
            let cell = tableView.dequeueReusableCell(withIdentifier: AccreditationListCell.nameOfClass) as! AccreditationListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let data = biAddRequest?.award?[indexPath.row]
            cell.lblIndex.text = "Award \(indexPath.row + 1)"
            cell.lblName.text = data?.name ?? ""
            cell.lblDesc.text = data?.desc ?? ""
            cell.lblLink.text = data?.link ?? ""
            
            return cell
        } else if(tableView == self.tableViewAdditionalLink){
            let cell = tableView.dequeueReusableCell(withIdentifier: ExternalLinkListCell.nameOfClass) as! ExternalLinkListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let data = biAddRequest?.additional_link?[indexPath.row]
            cell.lblIndex.text = "Link \(indexPath.row + 1)"
            cell.lblDesc.text = data?.desc ?? ""
            cell.lblLink.text = data?.name ?? ""
            return cell
        } else if tableView == tableViewAdditionalDocument {
            let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationDocumentCell.nameOfClass) as! BusinessInformationAddAdditionalInformationDocumentCell
            
            let data = biAddRequest?.doc?[indexPath.row]
            
            cell.lblName.text = data?.media_title
            cell.lblDesc.text = data?.media_desc
            cell.cellIndex = indexPath
            cell.documentSelectionDelegate = self
            
            if let imageURLs = data?.imageurls, !imageURLs.isEmpty {
                cell.arrDocumentImagesUrl = imageURLs.compactMap { $0.thumb }
            } else if let arrImages = data?.arrImages {
                cell.arrDocumentImages = arrImages.compactMap { uploadData in
                    if let imageData = uploadData.imageThumbnailData {
                        return UIImage(data: imageData)
                    }
                    return nil
                }
            } else if let imageURL = data?.imageurl, !imageURL.isEmpty {
                cell.arrDocumentImagesUrl = [imageURL]
            }
            
            cell.refreshScreenInfo()
            
            return cell
            /*
            let cell = tableView.dequeueReusableCell(withIdentifier: AdditionalDocumentListCell.nameOfClass) as! AdditionalDocumentListCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let data = biAddRequest?.doc?[indexPath.row]
            cell.lblIndex.text = "Information Document \(indexPath.row + 1)"
            cell.lblName.text = data?.media_title ?? ""
            cell.lblDesc.text = data?.media_desc ?? ""

            if let thumb = data?.thumb, let url = URL(string: thumb) {
                if url.pathExtension.lowercased() == "pdf" {
                    cell.imgThumb.image = UIImage(named: "ic_pdf")
                } else if ["png", "jpg", "jpeg"].contains(url.pathExtension.lowercased()) {
                    cell.imgThumb.sd_setImage(with: url, completed: nil)
                } else {
                    cell.imgThumb.image = UIImage(named: "ic_doc")
                }
            }
            
            return cell
             */
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewWebsite {
            if let link: String = biAddRequest?.busi_weblink_list?[indexPath.row].webLinkTitle, !link.isEmpty {
                openLink(for: link)
            }
        } else if tableView == tableViewSocialMedia {
            if let link: String = biAddRequest?.busi_sociallink_list?[indexPath.row].webLinkTitle, !link.isEmpty {
                openLink(for: link)
            }
        } else if tableView == tableViewAccredation {
            if let link: String = biAddRequest?.accreditations?[indexPath.row].link, !link.isEmpty {
                openLink(for: link)
            }
        } else if tableView == tableViewPartnership {
            if let link: String = biAddRequest?.affiliation?[indexPath.row].link, !link.isEmpty {
                openLink(for: link)
            }
        } else if tableView == tableViewAward {
            if let link: String = biAddRequest?.award?[indexPath.row].link, !link.isEmpty {
                openLink(for: link)
            }
        } else if tableView == tableViewAdditionalLink {
            if let link: String = biAddRequest?.additional_link?[indexPath.row].name, !link.isEmpty {
                openLink(for: link)
            }
        }
    }
}

extension BIDetailViewController : UICollectionViewDataSource {
    
    /**
     *  Specify number of rows on section in collectionView with UICollectionViewDataSource method override.
     *
     *  @param key UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.biAddRequest != nil && self.biAddRequest?.mediabusiness != nil && (self.biAddRequest?.mediabusiness?.count)! > 0) ? (self.biAddRequest?.mediabusiness?.count)! : 0
    }
    
    /**
     *  Implement collectionview cell in collectionView with UICollectionViewDataSource method override.
     *
     *  @param UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        let productInfo = productList?[indexPath.row]
        
        // Default product cell
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath) as! SerivceImageCell
        cell.delegate =  self
        cell.cellIndex = indexPath.row
        
        let data = self.biAddRequest?.mediabusiness![indexPath.row]
        
        if(data?.thumb != nil){
            cell.imgService.sd_setImage(with: URL(string: (data?.thumb ?? "")))
        }
        
        if((data?.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
            cell.imgPlay.isHidden = false
        } else {
            cell.imgPlay.isHidden = true
        }
        
        return cell
    }
}

extension BIDetailViewController : UICollectionViewDelegate {
    
    /**
     *  Implement tap event on collectionview cell in collectionView with UICollectionViewDelegate method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
    }
}

extension BIDetailViewController : UICollectionViewDelegateFlowLayout {
    
    /**
     *  Implement collectionView with UICollectionViewDelegateFlowLayout method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:100, height: 80)
    }
}

extension BIDetailViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        //TODO: Update image as per selection
        
//        let profileImageUrl = PMUserDefault.getProfilePic()
//        if(profileImageUrl != nil){
//            self.imgProfile.sd_setImage(with: URL(string: (profileImageUrl!)))
//        }
        
        if(self.biAddRequest?.mediabusiness != nil && (self.biAddRequest?.mediabusiness?.count)! > 0){
            if((self.biAddRequest?.mediabusiness![cellIndex].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.imgProfile.sd_setImage(with: URL(string: (self.biAddRequest?.mediabusiness![cellIndex].thumb ?? "")))
        }
        
    }
}

extension BIDetailViewController: BIDetailTableViewSizeDelegate {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize) {
        switch tableView {
        case tableViewLocation: constraintTableViewLocationHeight.constant = size.height
        case tableViewWebsite: constraintTableViewWebsiteHeight.constant = size.height
        case tableViewSocialMedia: constraintTableViewSocialMediaHeight.constant = size.height
        case tableViewFieldServiceType: constraintTableViewFieldServiceTypeHeight.constant = size.height
        case tableViewAccredation: constraintTableViewAccredationHeight.constant = size.height
        case tableViewPartnership: constraintTableViewPartnershipHeight.constant = size.height
        case tableViewAward: constraintTableViewAwardHeight.constant = size.height
        case tableViewAdditionalLink:
            print("contentSize: \(size)")
            constraintTableViewAdditionalLinkHeight.constant = size.height
        case tableViewAdditionalDocument: constraintTableViewAdditionalDocumentHeight.constant = size.height
        default:
            break
        }
    }
}

/**
 *  LocationListCell class is subclass of UITableViewCell
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class LocationListCell: UITableViewCell {

    @IBOutlet weak var lblExclusivelyTravel: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblLocationType: UILabel!
    @IBOutlet weak var lblBelong: UILabel!
    @IBOutlet weak var lblLocationClassification: UILabel!
    @IBOutlet weak var lblLocationSpeciality: UILabel!
    //@IBOutlet weak var lblBusinessHrs: UILabel!
    @IBOutlet weak var arrStackView: UIStackView!
    @IBOutlet weak var lblLocationTitle: UILabel!
    
    var users:[String] = ["10:50 AM","11:20 PM","1:20 AM"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func addStackView(locationHour: [BusniessHour]) {
        removeFromStack()
        for obj in locationHour {
            if let customView = Bundle.main.loadNibNamed("StackXIB", owner: self, options: nil)?.first as? LoopStackView {
                if obj.openTime != "" && obj.closeTime != "" {
                       customView.lblDay.text = HelperConstant.weekDayName[obj.index]
                       customView.lblStartTime.text = obj.closeTime
                       customView.lblEndTime.text = obj.openTime
                       self.arrStackView.addArrangedSubview(customView)
                }
            }
        }
    }
    
    func removeFromStack() {
        self.arrStackView.removeAllArrangedSubviews()
    }
}

/**
 *  WebsiteListCell class is subclass of UITableViewCell, also use in Social Media
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class WebsiteListCell: UITableViewCell {
    @IBOutlet weak var lblWebsiteIndex: UILabel!
    @IBOutlet weak var lblMainWebsiteStatus: UILabel!
    @IBOutlet weak var lblLink: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

/**
 *  FieldServiceListCell class is subclass of UITableViewCell
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class FieldServiceListCell: UITableViewCell {
    
    @IBOutlet weak var lblFieldIndex: UILabel!
    @IBOutlet weak var lblCategoryStatus: UILabel!
    @IBOutlet weak var lblField: UILabel!
    @IBOutlet weak var lblClassification: UILabel!
    @IBOutlet weak var lblTechnique: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

/**
 *  AccreditationListCell class is subclass of UITableViewCell, use in Partnership, Awards and Additional link
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class AccreditationListCell: UITableViewCell {
    
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblLink: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

/**
 *  ExternalLinkListCell class is subclass of UITableViewCell, use in Partnership, Awards and Additional link
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ExternalLinkListCell: UITableViewCell {
    
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblLink: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

/**
 *  ExternalLinkListCell class is subclass of UITableViewCell, use in Partnership, Awards and Additional link
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class AdditionalDocumentListCell: UITableViewCell {
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
}

extension BIDetailViewController: DocumentSelectionDelegate {
    func didSelectDocument(at indexPath: IndexPath, inCellAt cellIndexPath: IndexPath) {
        if let data = biAddRequest?.doc?[indexPath.row], let imageURLs = data.imageurls, imageURLs.count > indexPath.item, let imageURLString = imageURLs[indexPath.item].imageurls, !imageURLString.isEmpty {
            openLink(for: imageURLString)
        } else if let data = biAddRequest?.doc?[indexPath.row], let imageURLString = data.imageurl, !imageURLString.isEmpty {
            openLink(for: imageURLString)
        }
    }
}

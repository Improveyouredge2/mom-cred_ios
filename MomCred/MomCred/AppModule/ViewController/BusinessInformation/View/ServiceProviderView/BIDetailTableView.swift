//
//  BIDetailTableView.swift
//  MomCred
//
//  Created by MD on 11/07/20.
//  Copyright © 2020 Consagous. All rights reserved.
//

import UIKit

protocol BIDetailTableViewSizeDelegate: class {
    func detailTableView(_ tableView: BIDetailTableView, didUpdateContentSize size: CGSize)
}

class BIDetailTableView: UITableView {
    weak var sizeDelegate: BIDetailTableViewSizeDelegate?
    
    override var contentSize: CGSize {
        didSet {
            sizeDelegate?.detailTableView(self, didUpdateContentSize: contentSize)
        }
    }
}

//
//  BIListViewController.swift
//  MomCred
//
//  Created by consagous on 30/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class BIListViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tbServiceList : UITableView!
    @IBOutlet weak var viewCreateNew : UIView!
    @IBOutlet weak var btnOptionMenu : UIButton!
    
    fileprivate var presenter = BIListViewPresenter()
    
    var biAddRequestList:[BIAddRequest]?
    
    fileprivate var menuArray: [HSMenu] = []
    fileprivate var bidetailViewController:BIDetailViewController?
    fileprivate var biaddOverViewViewController:BIAddOverViewViewController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)

        tbServiceList.estimatedRowHeight = 100
        tbServiceList.rowHeight = UITableView.automaticDimension
        
        self.btnOptionMenu.isHidden = true
        self.tbServiceList.isHidden = true
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bidetailViewController = nil
        self.biaddOverViewViewController = nil
        
//        // TODO: API calling
//        if(self.biAddRequestList == nil){
            presenter.getBIDetail()
//        }
    }
}

extension BIListViewController{
    @IBAction func actionCreateBI(_ sender: UIButton) {
        if(self.biaddOverViewViewController == nil){
            self.biaddOverViewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddOverViewViewController.nameOfClass) as BIAddOverViewViewController
            
            self.navigationController?.pushViewController(self.biaddOverViewViewController!, animated: true)
        }
    }
    
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}

extension BIListViewController: HSPopupMenuDelegate{
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if(index == 0 && self.biaddOverViewViewController == nil){
            
            self.biaddOverViewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddOverViewViewController.nameOfClass) as BIAddOverViewViewController
            
            self.navigationController?.pushViewController(self.biaddOverViewViewController!, animated: true)
            
        }
    }
    
    func updateScreenInfo(){
        if(biAddRequestList != nil){
            // TODO: Update all controls
//            self.lblBIName.text = biAddRequest?.busi_title ?? ""
//            self.lblBIDesc.text = biAddRequest?.busi_desc ?? ""
//            self.lblBIMissionStmt.text = biAddRequest?.busi_statement ?? ""
            
            tbServiceList.reloadData()
            
            self.tbServiceList.isHidden = false
            self.viewCreateNew.isHidden = true
            self.btnOptionMenu.isHidden = false
        } else {
            self.tbServiceList.isHidden = true
            self.viewCreateNew.isHidden = false
            self.btnOptionMenu.isHidden = true
        }
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension BIListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return self.arrNoteList.count
        return self.biAddRequestList != nil ? (self.biAddRequestList?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data       =    self.biAddRequestList?[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: BITableViewCell.nameOfClass, for: indexPath) as! BITableViewCell
        
        //TODO: For testing
        let profileImageUrl = PMUserDefault.getProfilePic()
        cell.imgProfilePic.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
        
        cell.lblTitle.text                       =   data?.busi_title ?? ""
        cell.lblDesciption.text               =    data?.busi_statement ?? ""
        cell.lblType.text = "\(data?.busi_service_provider_title ?? "") - \(data?.busi_service_provider_type_title ?? "")"
        cell.lblServiceConnected.text = "0"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.bidetailViewController == nil){
            self.bidetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIDetailViewController.nameOfClass) as BIDetailViewController
            
            let data       =    self.biAddRequestList?[indexPath.row]
            bidetailViewController?.biAddRequest = data
            self.navigationController?.pushViewController(self.bidetailViewController!, animated: true)
        }
    }
}

class BITableViewCell: UITableViewCell {
    
    @IBOutlet weak var cardView : ViewLayerSetup!
    @IBOutlet weak var imgProfilePic : ImageLayerSetup!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDesciption : UILabel!
    @IBOutlet weak var lblType : UILabel!
    @IBOutlet weak var lblServiceConnected : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //        self.cardView.backgroundColor   =   UIColor(hexString: ColorCode.cardColor)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

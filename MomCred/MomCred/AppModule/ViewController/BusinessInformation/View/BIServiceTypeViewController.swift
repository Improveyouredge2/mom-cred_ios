//
//  BIServiceTypeViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 11/07/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 * BIServiceTypeViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BIServiceTypeViewController : LMBaseViewController{
    
    @IBOutlet weak var btnListingAvailableYes: UIButton!
    @IBOutlet weak var btnListingAvailableNo: UIButton!
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    var biAddRequest:BIAddRequest?
    fileprivate var presenter = BIServiceTypePresenter()
    var serviceType : [ListingDataDetail]?
    fileprivate let formNumber = 8
    fileprivate var biqualificationAccreditationViewController:BIQualificationAccreditationViewController?
    
    var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.connectView(view: self)
        
        self.serviceType = BIPresenter.parentListingList?.parentListingData?.services_types
        
        self.reloadChildListing()
        
        // TODO: For testing
//        biAddRequest = BIAddRequest()
        
        self.setScreenData()
        
        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.biqualificationAccreditationViewController = nil
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.tableView.contentSize.height > 0){
            self.tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
        }
    }
}

extension BIServiceTypeViewController{
    
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        if(self.biAddRequest != nil){
            if(self.biAddRequest?.service_type != nil && (self.biAddRequest?.service_type?.count)! > 0){
                //            self.biAddRequest?.service_type = selectedId ?? []
                self.tableView.reloadData()
            }

            if(self.biAddRequest?.purchasability != nil){
                
                self.btnListingAvailableYes.isSelected = self.biAddRequest?.purchasability?.bool ?? false
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.biAddRequest != nil){
            
            if(self.biAddRequest?.purchasability != nil && self.biAddRequest?.purchasability?.bool != self.btnListingAvailableYes.isSelected){
                isUpdate = true
            }
            
            if(self.isUpdate){
                isUpdate = self.isUpdate
            }
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        
        self.biqualificationAccreditationViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIQualificationAccreditationViewController.nameOfClass) as BIQualificationAccreditationViewController
        
        self.biqualificationAccreditationViewController?.biAddRequest = self.biAddRequest
        biqualificationAccreditationViewController?.isEditingBusiness = isEditingBusiness
        self.navigationController?.pushViewController(self.biqualificationAccreditationViewController!, animated: true)
    }
    
    fileprivate func reloadChildListing(){
        
        if(self.serviceType != nil && (self.serviceType?.count)! > 0){
            for service in self.serviceType!{
                self.presenter.serverBatchChildListingRequest(parentId: service.listing_id ?? "")
            }
        }
    }
    
    func updateServiceTypeTableList(){
        self.tableView.reloadData()
    }
}

extension BIServiceTypeViewController{
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var selectedId:[String] = []
        
        if(self.serviceType != nil && (self.serviceType?.count)! > 0){
            for service in self.serviceType!{
                for childService in service.subcat!{
                    if(childService.selectionStaus){
                        selectedId.append(childService.listing_id ?? "")
                    }
                }
            }
        }
        
        if(selectedId.count == 0){
            Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_service_type.getLocalized(), buttonTitle: nil, controller: nil)
            
            return false
        }
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0  && self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = ""
        }
        
        self.biAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        if(selectedId.count > 0){
            self.biAddRequest?.service_type = selectedId ?? []
        } else{
            self.biAddRequest?.service_type = nil
        }
        
        self.biAddRequest?.purchasability = self.btnListingAvailableYes.isSelected ? "1" : "0"
        
        return true
    }
}

extension BIServiceTypeViewController{
    
    @IBAction func methodListingAvailableAction(_ sender: UIButton) {
        
        self.btnListingAvailableYes.isSelected = false
        self.btnListingAvailableNo.isSelected = false
        
        if(self.btnListingAvailableYes == sender){
            self.btnListingAvailableYes.isSelected = true
        } else {
            self.btnListingAvailableNo.isSelected = true
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is BIDetailViewController }
            if let biDetailVC = biDetailVC as? BIDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension BIServiceTypeViewController: UITableViewDelegate, UITableViewDataSource{
    // MARK: - Tableview Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.serviceType?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rowCount = 0
        
        if(self.serviceType != nil && (self.serviceType?.count)! > section){
            rowCount = self.serviceType![section].subcat?.count ?? 0
        }
        
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(self.serviceType != nil && (self.serviceType?.count)! > section){
            return self.serviceType![section].listing_title ?? ""
        }
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(hexString: ColorCode.tabInActiveColor)
        
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.backgroundColor = UIColor.clear
        headerLabel.font = FontsConfig.FontHelper.defaultRegularFontWithSize(20)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        
        if(self.serviceType != nil && (self.serviceType?.count)! > section){
            headerLabel.text = self.serviceType![section].listing_title ?? ""
        }
        
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: SingleSelectionCell.nameOfClass) as! SingleSelectionCell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessContactAddPersonnelCell.nameOfClass) as! BusinessContactAddPersonnelCell
        
        var listingData:ListingDataDetail?
        if(self.serviceType != nil && (self.serviceType?.count)! > indexPath.section){
            listingData = self.serviceType![indexPath.section].subcat?[indexPath.row]
        }
        
        if(listingData != nil){
            cell.delegate = self
            cell.cellIndex = indexPath
            cell.lblTitle.text = listingData?.listing_title
            cell.btnCheck.isSelected = listingData?.selectionStaus ?? false
            
            if(!self.isUpdate && self.biAddRequest?.service_type != nil && (self.biAddRequest?.service_type?.count)! > 0){
                if(self.biAddRequest?.service_type?.contains(listingData?.listing_id ?? "") ?? false){
                    cell.btnCheck.isSelected = true
                    listingData?.selectionStaus = true
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//extension BIServiceTypeViewController:SingleSelectionCellDelegate {
//    func selectListItem(cellIndex: IndexPath?, status:Bool){
//
//        // remove all selection
////        if(self.serviceType != nil && (self.serviceType?.count)! > 0){
////            let serviceList = self.serviceType![cellIndex?.section ?? 0].childListing!
////
////            for service in serviceList{
////                service.selectionStaus = false
////            }
////
////        }
//
//        if(self.serviceType != nil && (self.serviceType?.count)! > cellIndex?.section ?? 0){
//            self.serviceType![cellIndex?.section ?? 0].childListing?[cellIndex?.row ?? 0].selectionStaus = status
//        }
//
//        self.tableView.reloadData()
//    }
//}


extension BIServiceTypeViewController: BusinessContactAddPersonnelCellDelegate{
    func selectListItem(cellIndex: IndexPath?, status:Bool, tableView:UITableView?){
        
        if(self.serviceType != nil && (self.serviceType?.count)! > cellIndex?.section ?? 0){
            self.serviceType![cellIndex?.section ?? 0].subcat?[cellIndex?.row ?? 0].selectionStaus = status
            
            self.isUpdate = true
        }
    }
}

//
//  BIAddExceptionalViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 25/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 * BIAddExceptionalViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BIAddExceptionalViewController : LMBaseViewController{
    
    
    @IBOutlet weak var btnStandard: UIButton!
    @IBOutlet weak var btnExceptional: UIButton!
    @IBOutlet weak var btnStandardExceptional: UIButton!
    
    @IBOutlet weak var btnExceptionalServicesYes: UIButton!
    @IBOutlet weak var btnExceptionalServicesNo: UIButton!
    
    @IBOutlet weak var btnPrimarilyExceptionalServicesYes: UIButton!
    @IBOutlet weak var btnPrimarilyExceptionalServicesNo: UIButton!
    
    @IBOutlet weak var stackViewPrimarilyExceptional: UIStackView!
    @IBOutlet weak var stackViewClassificationExceptional: UIStackView!
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var constraintstableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSaveAndFinishHeightConstraint: NSLayoutConstraint!
    var isEditingBusiness: Bool = false

    var biAddRequest:BIAddRequest?
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    let kHeaderSectionTag: Int = 6900;
    var expandTableNumber:[Int] = []
    
    var exceptional_classification : [ListingDataDetail]?
    
    fileprivate var presenter = BIAddExceptionalPresenter()
    fileprivate let formNumber = 6
    fileprivate var biaddFieldServiceViewController:BIAddFieldServiceViewController?
    
    var isUpdate = false
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: For testing
//        biAddRequest = BIAddRequest()
        
        presenter.connectView(view: self)
        
        self.exceptional_classification = BIPresenter.parentListingList?.parentListingData?.exceptional_classification
        
        self.resetExceptionalServiceSection()
        self.reloadChildListing()
        self.tableView.backgroundColor = UIColor.clear
        
        self.setScreenData()

        if !isEditingBusiness {
            btnSaveAndFinishHeightConstraint.constant = 0
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.biaddFieldServiceViewController = nil
        
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.tableView.contentSize.height > 0){
            self.tableView.scrollToBottom()
            self.constraintstableViewHeight?.constant = self.tableView.contentSize.height
        }
    }
}

extension BIAddExceptionalViewController{
    /**
     *  Update information on screen controls with user response.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setScreenData(){
        if(self.biAddRequest != nil){
            
            if(self.biAddRequest?.service_provider_classification != nil && (self.biAddRequest?.service_provider_classification?.length)! > 0){
                
                let serviceProvider:Int = Int(self.biAddRequest?.service_provider_classification ?? "1") ?? 1
                
                self.btnStandard.isSelected = false
                self.btnExceptional.isSelected = false
                self.btnStandardExceptional.isSelected = false
                
                switch serviceProvider{
                case 1:
                    self.btnStandard.isSelected = true
                    break
                case 2:
                    self.btnExceptional.isSelected = true
                    break
                case 3:
                    self.btnStandardExceptional.isSelected = true
                    break
                default:
                    break
                }
            }
            
            if(self.biAddRequest?.exceptional_services != nil && (self.biAddRequest?.exceptional_services?.length)! > 0){
                Int(self.biAddRequest?.exceptional_services ?? "0") == 1 ? self.methodExceptionalServicesYesNoAction(self.btnExceptionalServicesYes) : self.methodExceptionalServicesYesNoAction(self.btnExceptionalServicesNo)
            }
            
            if(self.biAddRequest?.primarily_exceptional != nil && (self.biAddRequest?.primarily_exceptional?.length)! > 0){
                Int(self.biAddRequest?.primarily_exceptional ?? "0") == 1 ? self.methodPrimarilyExceptionalServicesYesNoAction(self.btnPrimarilyExceptionalServicesYes) : self.methodPrimarilyExceptionalServicesYesNoAction(self.btnPrimarilyExceptionalServicesNo)
            }
            
            self.isUpdate = false
            if(self.biAddRequest?.exceptional_classification_offered != nil && (self.biAddRequest?.exceptional_classification_offered?.count)! > 0){
                self.tableView.reloadData()
            }
        }
    }
    
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isDataUpdate() -> Bool{
        var isUpdate = false
        
        if(self.biAddRequest != nil){
            
            if(self.biAddRequest?.service_provider_classification != nil && (self.biAddRequest?.service_provider_classification?.length)! > 0){
                
                let serviceProvider:Int = Int(self.biAddRequest?.service_provider_classification ?? "1") ?? 1
                
                
                switch serviceProvider{
                case 1:
                    if(!self.btnStandard.isSelected){
                        isUpdate = true
                    }
                    break
                case 2:
                    if(!self.btnExceptional.isSelected){
                        isUpdate = true
                    }
                    break
                case 3:
                    if(!self.btnStandardExceptional.isSelected){
                        isUpdate = true
                    }
                    break
                default:
                    break
                }
            }
            
            if(self.biAddRequest?.exceptional_services != nil && (self.biAddRequest?.exceptional_services?.length)! > 0 && self.biAddRequest?.exceptional_services?.bool != self.btnExceptionalServicesYes.isSelected){
                isUpdate = true
            }
            
            if(self.biAddRequest?.primarily_exceptional != nil && (self.biAddRequest?.primarily_exceptional?.length)! > 0 && self.biAddRequest?.primarily_exceptional?.bool != self.btnPrimarilyExceptionalServicesYes.isSelected){
                isUpdate = true
            }
            
            if(self.isUpdate){
                isUpdate = true
            }
        }
        
        return isUpdate
    }
    
    fileprivate func openNextScr(){
        self.biaddFieldServiceViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIAddFieldServiceViewController.nameOfClass) as BIAddFieldServiceViewController
        
        self.biaddFieldServiceViewController?.biAddRequest = self.biAddRequest
        biaddFieldServiceViewController?.isEditingBusiness = isEditingBusiness
        self.navigationController?.pushViewController(self.biaddFieldServiceViewController!, animated: true)
    }
    
    fileprivate func reloadChildListing(){
        
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > 0){
            for expectional in self.exceptional_classification!{
                self.presenter.serverChildListingRequest(parentId: expectional.listing_id ?? "")
            }
        }
    }
    
    func updateExceptionalTableList(){
        self.tableView.reloadData()
    }
}

extension BIAddExceptionalViewController{
    fileprivate func resetExceptionalServiceSection(){
        self.btnExceptionalServicesYes.isSelected = false
        self.btnExceptionalServicesNo.isSelected = true
        self.stackViewPrimarilyExceptional.isHidden = true
        self.stackViewClassificationExceptional.isHidden = true
        
        self.btnPrimarilyExceptionalServicesYes.isSelected = false
        self.btnPrimarilyExceptionalServicesNo.isSelected = true
        self.unCheckAllSelection()
        
    }
    
    fileprivate func unCheckAllSelection(){
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > 0){
            for expectional in self.exceptional_classification!{
                if(expectional.subcat != nil &&  (expectional.subcat?.count)! > 0){
                    for childExceptional in expectional.subcat!{
                        childExceptional.selectionStaus = false
                    }
                    
                    tableView.reloadData()
                }
            }
        }
    }
}

extension BIAddExceptionalViewController{
    /**
     *  Returns a status of validation of required input fields.
     *
     *  @param key empty.
     *
     *  @return status true or false on checking all required validation on feild.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    fileprivate func isFormValid() -> Bool{
        var selectedId:[String] = []
        
        if(self.btnExceptionalServicesYes.isSelected){
            
            if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > 0){
                for expectional in self.exceptional_classification!{
                    if(expectional.subcat != nil && (expectional.subcat?.count)! > 0){
                        for childExceptional in expectional.subcat!{
                            if(childExceptional.selectionStaus){
                                selectedId.append(childExceptional.listing_id ?? "")
                            }
                        }
                    }
                }
            }
            
            if(selectedId.count == 0){
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_select_exceptional_service_offered.getLocalized(), buttonTitle: nil, controller: nil)
                return false
            }
        }
        
        
        if(self.btnStandard.isSelected){
            self.biAddRequest?.service_provider_classification = "1"
        } else if(self.btnExceptional.isSelected){
            self.biAddRequest?.service_provider_classification = "2"
        } else {
            self.biAddRequest?.service_provider_classification = "3"
        }
        
        if(UserDefault.getBIID() != nil && (UserDefault.getBIID()?.length)! > 0  && self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = UserDefault.getBIID() ?? ""
        } else if(self.biAddRequest?.busi_id == nil){
            self.biAddRequest?.busi_id = ""
        }
        
        self.biAddRequest?.pageid = NSNumber(integerLiteral: self.formNumber)
        
        self.biAddRequest?.exceptional_services = (self.btnExceptionalServicesYes?.isSelected ?? false) ? "1" : "0"
        
        self.biAddRequest?.primarily_exceptional = (self.btnPrimarilyExceptionalServicesYes?.isSelected ?? false) ? "1" : "0"
        
        if(self.btnExceptionalServicesYes?.isSelected ?? false){
            self.biAddRequest?.exceptional_classification_offered = selectedId
        } else {
            self.biAddRequest?.exceptional_classification_offered = nil
        }
        
        return true
    }
}

extension BIAddExceptionalViewController{
    //MARK:- Button Action method implementation
    
    @IBAction func methodStandardAction(_ sender: UIButton) {
        
        self.btnStandard.isSelected = false
        self.btnExceptional.isSelected = false
        self.btnStandardExceptional.isSelected = false
        self.isUpdate = true
        
        if(self.btnStandard == sender){
            self.btnStandard.isSelected = true
        } else if(self.btnExceptional == sender){
            self.btnExceptional.isSelected = true
        } else {
            self.btnStandardExceptional.isSelected = true
        }
    }
    
    @IBAction func methodExceptionalServicesYesNoAction(_ sender: UIButton) {
        
        self.btnExceptionalServicesYes.isSelected = false
        self.btnExceptionalServicesNo.isSelected = false
        self.isUpdate = true
        if(self.btnExceptionalServicesYes == sender){
            self.btnExceptionalServicesYes.isSelected = true
            self.stackViewPrimarilyExceptional.isHidden = false
            self.stackViewClassificationExceptional.isHidden = false
        } else {
            self.btnExceptionalServicesNo.isSelected = true
            self.resetExceptionalServiceSection()
        }
    }
    
    @IBAction func methodPrimarilyExceptionalServicesYesNoAction(_ sender: UIButton) {
        
        self.btnPrimarilyExceptionalServicesYes.isSelected = false
        self.btnPrimarilyExceptionalServicesNo.isSelected = false
        self.isUpdate = true
        if(self.btnPrimarilyExceptionalServicesYes == sender){
            self.btnPrimarilyExceptionalServicesYes.isSelected = true
        } else {
            self.btnPrimarilyExceptionalServicesNo.isSelected = true
            self.unCheckAllSelection()
        }
    }
    
    private func saveData(sender: UIButton, completion: @escaping () -> Void) {
        if !Helper.sharedInstance.isTooEarlyMultipleClicks(sender) {
            if !isDataUpdate() {
                completion()
            } else if isFormValid() {
                Spinner.show()
                presenter.submitData { status, _, _ in
                    if status {
                        completion()
                    }
                }
            }
        }
    }

    @IBAction func methodNextAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            self?.openNextScr()
        }
    }
    
    @IBAction private func btnSaveAndFinishAction(_ sender: UIButton) {
        saveData(sender: sender) { [weak self] in
            let biDetailVC = self?.navigationController?.viewControllers.reversed().first { $0 is BIDetailViewController }
            if let biDetailVC = biDetailVC as? BIDetailViewController {
                biDetailVC.shouldRefreshContent = true
                self?.navigationController?.popToViewController(biDetailVC, animated: true)
            } else {
                self?.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}

extension BIAddExceptionalViewController: UITableViewDelegate, UITableViewDataSource{
    // MARK: - Tableview Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.exceptional_classification?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rowCount = 0
        
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > section){
            rowCount = self.exceptional_classification![section].subcat?.count ?? 0
        }
        
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > section){
            return self.exceptional_classification![section].listing_title ?? ""
        }
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(hexString: ColorCode.tabInActiveColor)
        
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.backgroundColor = UIColor.clear
        headerLabel.font = FontsConfig.FontHelper.defaultRegularFontWithSize(20)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > section){
            headerLabel.text = self.exceptional_classification![section].listing_title ?? ""
        }
        
        headerView.addSubview(headerLabel)
        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessContactAddPersonnelCell.nameOfClass) as! BusinessContactAddPersonnelCell
        
        var listingData:ListingDataDetail?
        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > indexPath.section){
            listingData = self.exceptional_classification![indexPath.section].subcat?[indexPath.row]
        }
        
        if(listingData != nil){
            cell.delegate = self
            cell.cellIndex = indexPath
            cell.lblTitle.text = listingData?.listing_title
            cell.btnCheck.isSelected = listingData?.selectionStaus ?? false
            
            if(!self.isUpdate && self.biAddRequest?.exceptional_classification_offered != nil && (self.biAddRequest?.exceptional_classification_offered?.count)! > 0){
                if(self.biAddRequest?.exceptional_classification_offered?.contains(listingData?.listing_id ?? "") ?? false){
                    cell.btnCheck.isSelected = true
                    listingData?.selectionStaus = true
                }
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Expand / Collapse Methods
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tableView!.beginUpdates()
            self.tableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }

}

extension BIAddExceptionalViewController: BusinessContactAddPersonnelCellDelegate{
    func selectListItem(cellIndex: IndexPath?, status:Bool, tableView:UITableView?){

        if(self.exceptional_classification != nil && (self.exceptional_classification?.count)! > cellIndex?.section ?? 0){
            self.exceptional_classification![cellIndex?.section ?? 0].subcat?[cellIndex?.row ?? 0].selectionStaus = status
            
            self.isUpdate = true
        }
    }
}

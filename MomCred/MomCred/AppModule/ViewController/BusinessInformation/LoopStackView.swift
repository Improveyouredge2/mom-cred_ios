//
//  LoopStackView.swift
//  MomCred
//
//  Created by Apple on 17/09/19.
//  Copyright © 2019 Consagous. All rights reserved.
//
import UIKit

class LoopStackView: UIView {
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
}

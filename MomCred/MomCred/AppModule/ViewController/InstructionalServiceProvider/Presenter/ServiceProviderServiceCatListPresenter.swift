//
//  ServiceProviderServiceCatListPresenter.swift
//  MomCred
//
//  Created by Rajesh Yadav on 16/10/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  ServiceProviderServiceCatListPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ServiceProviderServiceCatListPresenter {
    
    var view:ServiceProviderServiceCatListViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: ServiceProviderServiceCatListViewController) {
        self.view = view
    }
}

extension ServiceProviderServiceCatListPresenter{
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getAllServiceListDetail(){
        
        let requestData = BusiServicesRequest()
        requestData.busi_user = self.view.busiInfo?.busi_user ?? ""
        
        InstructionalServiceProviderListingService.getAllServiceListDetail(requestData:requestData, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil){
                        self.view.myServiceList = []
                        self.view.myServiceList = response?.data
                    }
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
}

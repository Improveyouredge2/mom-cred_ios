//
//  InstructionalServiceProviderListPresenter.swift
//  MomCred
//
//  Created by Rajesh Yadav on 22/11/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  InstructionalServiceProviderListPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class InstructionalServiceProviderListPresenter {
    
    var view:InstructionalServiceProviderListViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: InstructionalServiceProviderListViewController) {
        self.view = view
    }
}

extension InstructionalServiceProviderListPresenter{
    
    /**
     *  Get Home screen detail informatoin from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func searchKeywordServiceProvider(keywordSearchRequest:KeywordSearchRequest?, callback:@escaping (_ status:Bool, _ response: [BIAddRequest]?) -> Void){
        
        DashboardService.getKeywordSearchData(requestData:keywordSearchRequest,callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
//                    self.view.showSearchResult(result: response, keywordSearchRequest: keywordSearchRequest)
                    callback(status, response?.busi)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
//                    self.view.showSearchResult(result: nil, keywordSearchRequest: nil)
                    callback(status, nil)
                }
            }
        })
    }
    
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverQuickSearchProvider(searchRequest: ServiceProviderSearchRequest?, callback:@escaping (_ status:Bool, _ response: [BIAddRequest]?) -> Void){
        
        QuickSearchService.serverQuickSearchProvider(quickSearchRequest: searchRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    if(response != nil && response?.busi != nil){
//                        self.view.showServiceProvider(busiList: response?.busi)
                        callback(status, response?.busi)
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, nil)
                }
            }
        })
    }
    
    /**
     *  Get Search Service Provider listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverSearchProvider(serviceProviderSearchRequest:ServiceProviderSearchRequest?, callback:@escaping (_ status:Bool, _ response: [BIAddRequest]?) -> Void){
        
        AdvanceSearchService.serverServiceProviderSearchListing(serviceProviderSearchRequest: serviceProviderSearchRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    if(response != nil && response?.busi != nil){
                        //                        self.view.showServiceProvider(busiList: response?.busi)
                        callback(status, response?.busi)
                        
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    //                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, nil)
                }
            }
        })
    }
}

//
//  InstructionalServiceProviderListingListPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  InstructionalServiceProviderListingListPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class InstructionalServiceProviderListingListPresenter {
    
    var view:InstructionalServiceProviderListingListView! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: InstructionalServiceProviderListingListView) {
        self.view = view
    }
}

//MARK:
//MARK:- Implement Instructional Provider Service list
extension InstructionalServiceProviderListingListPresenter{
    
    /**
     *  Get sub cat service provider detail list.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getSubCatServiceProviderList(subCatId:String?){
        
        let getListingCategoryModelRequest = GetListingCategoryModelRequest(listing_id: subCatId)
        
        InstructionalServiceProviderListingService.getServiceByCategory(requestData: getListingCategoryModelRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.service != nil && (response?.service?.count)! > 0){
                        self.view.serviceList = response?.service ?? []
                    }
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getAllPersonnelListDetail(){
        
        let requestData = BusiServicesRequest()
        requestData.busi_user = self.view.busiInfo?.busi_user ?? ""
         InstructionalServiceProviderListingService.getAllPersonnelListDetail(requestData:requestData, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil){
                            self.view.serviceList = []
                            self.view.personnelList = response?.data
                    }
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getAllFacilityListDetail(){
        
        let requestData = BusiServicesRequest()
        requestData.busi_user = self.view.busiInfo?.busi_user ?? ""
         InstructionalServiceProviderListingService.getAllFacilityListDetail(requestData:requestData, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil){
                            self.view.serviceList = []
                            self.view.facilityList = response?.data
                    }
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getAllInsideLookListDetail(){
        
        let requestData = BusiServicesRequest()
        requestData.busi_user = self.view.busiInfo?.busi_user ?? ""
         InstructionalServiceProviderListingService.getAllInsideLookListDetail(requestData:requestData, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil){
                            self.view.serviceList = []
                            self.view.insideLookList = response?.data
                    }
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getAllInstructionalListDetail(){
        
        let requestData = BusiServicesRequest()
        requestData.busi_user = self.view.busiInfo?.busi_user ?? ""
         InstructionalServiceProviderListingService.getAllInstructionalListDetail(requestData:requestData, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.data != nil){
                            self.view.serviceList = []
                            self.view.instructionalList = response?.data
                    }
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
}

//MARK:
//MARK:- Implement Search list
extension InstructionalServiceProviderListingListPresenter{
    
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverQuickSearchListing(quickSearchRequest:QuickSearchRequest?, callback:@escaping (_ status:Bool, _ response: [ServiceAddRequest]?) -> Void){
        
        //let childListingRequest = ChildListingRequest(listing_id: parentId)
        quickSearchRequest?.page_index = "\(view.currentPageIndex)"
        QuickSearchService.serverQuickSearchListing(quickSearchRequest: quickSearchRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                   if(response != nil && response?.service != nil){
                        callback(status, response?.service)
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, nil)
                }
            }
        })
    }
    
    /**
     *  Get Service Provide Service listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverSearchListing(serviceSearchRequest:ServiceSearchRequest?, callback:@escaping (_ status:Bool, _ response: [ServiceAddRequest]?) -> Void){
        
        AdvanceSearchService.serverServiceSearchListing(serviceSearchRequest: serviceSearchRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                   if(response != nil && response?.service != nil){
//                        self.view.showServiceListing(service: response?.service)
                    callback(status, response?.service)
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, nil)
                }
            }
        })
    }
    
    /**
     *  Get Search Facility listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverFacilitySearchListing(facilitySearchRequest:FacilitySearchRequest?, callback:@escaping (_ status:Bool, _ response: [MyFacilityAddRequest]?) -> Void){
        
        //let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        AdvanceSearchService.serverFacilitySearchListing(facilitySearchRequest: facilitySearchRequest, callback:{ (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                   if(response != nil && response?.data != nil){
//                    self.view.showServiceListing(facilityListing: response?.data)
                    callback(status, response?.data)
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, nil)
                }
            }
        })
    }
    
    /**
     *  Get Search Personnel listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverPersonnelSearchListing(personnelSearchRequest:PersonnelSearchRequest?, callback:@escaping (_ status:Bool, _ response: [PersonnelAddRequest]?) -> Void){
        
        //let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        AdvanceSearchService.serverPersonnelSearchListing(personnelSearchRequest: personnelSearchRequest, callback:{ (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    if(response != nil && response?.data != nil){
//                        self.view.showServiceListing(personnelList: response?.data)
                        callback(status, response?.data)
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, nil)
                }
            }
        })
    }
    
    /**
     *  Get Search Inside Look listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverInsideLookSearchListing(insideLookSearchRequest:InsideLookSearchRequest?, callback:@escaping (_ status:Bool, _ response: [ILCAddRequest]?) -> Void){
        
        //let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        AdvanceSearchService.serverInsideLookSearchListing(insideLookSearchRequest: insideLookSearchRequest, callback:{ (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    if(response != nil && response?.data != nil){
//                        self.view.showInsideLookListing(insideLookList: response?.data)
                        callback(status, response?.data)
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, nil)
                }
            }
        })
    }
    
    /**
     *  Get Search Instructional listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverInstructionalSearchListing(instructionalSearchRequest:InstructionalSearchRequest?, callback:@escaping (_ status:Bool, _ response: [ICAddRequest]?) -> Void){
        
        //let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        AdvanceSearchService.serverIstructionalSearchListing(instructionalSearchRequest: instructionalSearchRequest, callback:{ (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    if(response != nil && response?.data != nil){
//                        self.view.showInstructionalListing(instructionalList: response?.data)
                        callback(status, response?.data)
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    callback(status, nil)
                }
            }
        })
    }
}


//
//  RatingReviewViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class RatingReviewViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
//    @IBOutlet weak var tblTestimonialList : UITableView!
    
    @IBOutlet weak var ratingView : HCSStarRatingView!
    @IBOutlet weak var textViewDesc : KMPlaceholderTextView!
    
    //MARK:- Var(s)
    var presenter       =       RatingReviewPresenter()
    var busiInfo:BIAddRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.connectView(view: self)

    }
}

//MARK:- Action method implementation
extension RatingReviewViewController{
    
    @IBAction func actionSubmitMethod(_ sender:UIButton){
        if(!Helper.sharedInstance.isTooEarlyMultipleClicks(sender)){
            if(ratingView.value == 0){
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_rating_value.getLocalized(), buttonTitle: nil, controller: nil)
                return
            }
            
            if(textViewDesc.text != nil && textViewDesc.text.trim().length == 0){
                Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_rating_comment.getLocalized(), buttonTitle: nil, controller: nil)
                return
            }
            
            let addRatingRequest = AddRatingRequest(rate_star: "\(ratingView.value)", rate_service_provider: busiInfo?.busi_user ?? "", rate_comment: textViewDesc.text.trim())
            
            self.presenter.addRatingReview(addRatingRequest: addRatingRequest, callback: {
                (status, response, message) in
                if(status){
                    self.navigationController?.popViewController(animated: true)
                    
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            })
        }
    }
}


//
//  RatingReviewPresenter.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  RatingReviewPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class RatingReviewPresenter {
    
    var view:RatingReviewViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: RatingReviewViewController) {
        self.view = view
    }
}

extension RatingReviewPresenter{
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func addRatingReview(addRatingRequest:AddRatingRequest?, callback:@escaping (_ status:Bool, _ response: AddRatingResponse?, _ message: String?) -> Void){
        
        RatingReviewService.addRatingReview(addRatingRequest:addRatingRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
//                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
//                    self.view.updateScreenInfo()
                    callback(status, response, message)
                }
            }
        })
    }
}

//
//  NearbyUserMarkerView.swift
//  iTalent
//
//  Created by consagous on 10/06/19.
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit

class NearbyUserMarkerView: UIView {

    @IBOutlet weak var imgProfilePic  : ImageLayerSetup!
    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var lblCategoryName : UILabel!
    
    func setUserDetailsWith(_ user : BIAddRequest){
//        if(user.provider_image != nil && (user.provider_image?.length)! > 0){
//            self.imgProfilePic.sd_setImage(with: (user.provider_image?.count)! > 2 ? (user.provider_image ?? "").toURL() : nil, placeholderImage: #imageLiteral(resourceName: "location_pinkicon"))
//        }
        
//        self.imgProfilePic.sd_setImage(with: #imageLiteral(resourceName: "location_pinkicon"), placeholderImage: #imageLiteral(resourceName: "location_pinkicon"))
        
        self.imgProfilePic.image = #imageLiteral(resourceName: "location_pinkicon")
        
        self.lblUserName.text   =   user.busi_title ?? "Name not available"
        self.lblCategoryName.text   =   user.busitype ?? ""
    }
    
}

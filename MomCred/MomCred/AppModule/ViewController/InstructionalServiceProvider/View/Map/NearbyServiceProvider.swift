//
//  NearbyServiceProvider.swift
//  iTalent
//
//  Created by consagous on 23/04/19.
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit
import GoogleMaps

class NearbyServiceProviderViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var officeLocation: UIView!
    
    @IBOutlet weak var lblHeadQuarter : UILabel!
    @IBOutlet weak var lblOfficeAddress : UILabel!
    
    //MARK:- Var(s)
    var isForHeadQuarter    =   false
    var busiList:[BIAddRequest]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        mapView.isMyLocationEnabled = true
//        mapView.settings.myLocationButton = true
//        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 5)
        
        self.setupMarker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mapView.delegate = self
    }
}

//MARK:- Setup Markers with User Data
//MARK:-
extension NearbyServiceProviderViewController {
    func setupMarker(){
        
        self.mapView.clearsContextBeforeDrawing = true
        self.mapView.clear()
        
        for (index, user) in (self.busiList?.enumerated())! {
            if((user.location_lat != nil && (user.location_lat?.length)! > 0 ) || (user.location_lat_sec != nil && (user.location_lat_sec?.length)! > 0)){
                user.marker = GMSMarker()
                user.marker?.map    =   self.mapView
                
                if(user.location_lat != nil && (user.location_lat?.length)! > 0 && Double(user.location_lat ?? "") != nil){
                    user.marker?.position   =   CLLocationCoordinate2D(latitude: Double(user.location_lat ?? "")!, longitude: Double(user.location_long ?? "")!)
                } else if(user.location_lat_sec != nil && (user.location_lat_sec?.length)! > 0 &&  Double(user.location_lat_sec ?? "") != nil){
                    user.marker?.position   =   CLLocationCoordinate2D(latitude: Double(user.location_lat_sec ?? "")!, longitude: Double(user.location_long_sec ?? "")!)
                }
                
                let markerView : NearbyUserMarkerView     =       Bundle.main.loadNibNamed(NearbyUserMarkerView.nameOfClass, owner: self, options: nil)?.first as! NearbyUserMarkerView
                markerView.setUserDetailsWith(user)
                user.marker?.iconView   =   markerView
                user.marker?.accessibilityHint = "\(index)"
            }
        }
        
        if(self.busiList?.first?.location_lat != nil && (self.busiList?.first?.location_lat?.length)! > 0 && Double(self.busiList?.first?.location_lat ?? "") != nil ){
            let fancy = GMSCameraPosition.camera(withLatitude: (Double(self.busiList?.first?.location_lat ?? "0.0") ?? 0.0),longitude: (Double(self.busiList?.first?.location_long ?? "0.0") ?? 0.0),zoom: 18,bearing: 0,viewingAngle: 0)
            self.mapView.camera =   fancy
        } else if(self.busiList?.first?.location_lat_sec != nil && (self.busiList?.first?.location_lat_sec?.length)! > 0 && Double(self.busiList?.first?.location_lat_sec ?? "") != nil){
            let fancy = GMSCameraPosition.camera(withLatitude: (Double(self.busiList?.first?.location_lat_sec ?? "0.0") ?? 0.0),longitude: (Double(self.busiList?.first?.location_long_sec ?? "0.0") ?? 0.0),zoom: 18,bearing: 0,viewingAngle: 0)
            self.mapView.camera =   fancy
        }
        
        self.mapView.layoutSubviews()
        
    }

}

//MARK:- Map Delegate
//MARK:-
extension NearbyServiceProviderViewController : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("Tapped on \(self.busiList![Int(marker.accessibilityHint ?? "0") ?? 0].busi_title ?? "Not found")")
        let data = self.busiList![Int(marker.accessibilityHint ?? "0") ?? 0]
        
//        if data.id == ApplicationPreference.getUserId() {
//            let vc = ProfileMoreVCType.my_account.viewController()
//            self.navigationController?.pushViewController(vc, animated: true)
//        } else {
//            let vc : TalentProfileVC = BaseApp.sharedInstance.getViewController(storyboardName: AppConstant.Profile, viewControllerName: TalentProfileVC.nameOfClass)
//            vc.hidesBottomBarWhenPushed     =   true
//            vc.userId         =   data.id
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
        
        return true
    }
}

//
//  InstructionalServiceProviderListViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class InstructionalServiceProviderListViewController: LMBaseViewController {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView: UITableView!
    
    var keywordSearchRequest:KeywordSearchRequest?
    var quickSearchRequest: QuickSearchRequest?
    var serviceProviderSearchRequest: ServiceProviderSearchRequest?
    var nearbyServiceProviderViewController:NearbyServiceProviderViewController?
    
    var busiList:[BIAddRequest]?
    
    var currentPageIndex = 0
    var isAdvanceSearchResultShow = false
    var isBuildAPackage: Bool = false
    var totalServiceSelection: Int = 0
    var totalProfileSelection: Int = 0
    var selectedServices: [ServiceAddRequest]?
    var selectedPackage: PackageListResponse.PackageDataResponse?

    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var isDisplayLoader = true
    
//    fileprivate var instructionalServiceProviderDetailViewController:InstructionalServiceProviderDetailViewController?
    
    fileprivate var bidetailViewController:BIDetailViewController?
    
    fileprivate var presenter = InstructionalServiceProviderListPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.connectView(view: self)
        
        refreshView.delegate = self
        tableView.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.instructionalServiceProviderDetailViewController = nil
        self.nearbyServiceProviderViewController = nil
        self.bidetailViewController = nil
    }
}

extension InstructionalServiceProviderListViewController{
    @IBAction func actionMap(_ sender: UIButton) {
        
        if(self.nearbyServiceProviderViewController == nil){
            self.nearbyServiceProviderViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalServiceProviderStoryboard, viewControllerName: NearbyServiceProviderViewController.nameOfClass) as NearbyServiceProviderViewController
            
            self.nearbyServiceProviderViewController?.busiList = self.busiList
             self.navigationController?.pushViewController(self.nearbyServiceProviderViewController!, animated: true)
        }
    }
}


extension InstructionalServiceProviderListViewController: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .pulling(offset, threshould):
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
//                if(self.isAdvanceSearchResultShow){
                    if(self.keywordSearchRequest != nil){
                        self.keywordSearchRequest?.page_id = "\(self.currentPageIndex)"
                        presenter.searchKeywordServiceProvider(keywordSearchRequest: self.keywordSearchRequest, callback: {
                            (status, response) in
                            
                            if(response != nil && (response?.count)! > 0){
                                if(self.currentPageIndex == 0){
                                    self.busiList = []
                                    self.busiList = response
                                } else {
                                    self.busiList?.append(contentsOf: response ?? [])
                                }
                                self.tableView.reloadData()
                            }
                            completionHandler()
                        })
                        
                    } else if(serviceProviderSearchRequest != nil){
                        presenter.serverQuickSearchProvider(searchRequest: serviceProviderSearchRequest, callback: {
                            (status, response) in
                            
                            if(response != nil && (response?.count)! > 0){
                                if(self.currentPageIndex == 0){
                                    self.busiList = []
                                    self.busiList = response
                                } else {
                                    self.busiList?.append(contentsOf: response ?? [])
                                }
                                self.tableView.reloadData()
                            }
                            completionHandler()
                        })
                        
                    } else if(self.serviceProviderSearchRequest != nil){
//                        self.serviceProviderSearchRequest?.page_id = "\(self.currentPageIndex)"
                        presenter.serverSearchProvider(serviceProviderSearchRequest: self.serviceProviderSearchRequest, callback: {
                            (status, response) in
                            
                            if(response != nil && (response?.count)! > 0){
                                if(self.currentPageIndex == 0){
                                    self.busiList = []
                                    self.busiList = response
                                } else {
                                    self.busiList?.append(contentsOf: response ?? [])
                                }
                                self.tableView.reloadData()
                            }
                            completionHandler()
                        })
                    }
//                }
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
//            presenter.getAllServiceHomeListDetail(callback: {
//                (status, response, message) in
//
//                completionHandler()
//            })
//            if(self.isAdvanceSearchResultShow){
                if(self.keywordSearchRequest != nil){
                    self.keywordSearchRequest?.page_id = "\(self.currentPageIndex)"
                    presenter.searchKeywordServiceProvider(keywordSearchRequest: self.keywordSearchRequest, callback: {
                        (status, response) in
                        
                        if(response != nil && (response?.count)! > 0){
                            if(self.currentPageIndex == 0){
                                self.busiList = []
                                self.busiList = response
                            } else {
                                self.busiList?.append(contentsOf: response ?? [])
                            }
                            self.tableView.reloadData()
                        }
                        completionHandler()
                    })
                    
                } else if(serviceProviderSearchRequest != nil){
                    presenter.serverQuickSearchProvider(searchRequest: serviceProviderSearchRequest, callback: {
                        (status, response) in
                        
                        if(response != nil && (response?.count)! > 0){
                            if(self.currentPageIndex == 0){
                                self.busiList = []
                                self.busiList = response
                            } else {
                                self.busiList?.append(contentsOf: response ?? [])
                            }
                            self.tableView.reloadData()
                        }
                        completionHandler()
                    })
                    
                } else if(self.serviceProviderSearchRequest != nil){
//                    self.serviceProviderSearchRequest?.page_id = "\(self.currentPageIndex)"
                    presenter.serverSearchProvider(serviceProviderSearchRequest: self.serviceProviderSearchRequest, callback: {
                        (status, response) in
                        
                        if(response != nil && (response?.count)! > 0){
                            if(self.currentPageIndex == 0){
                                self.busiList = []
                                self.busiList = response
                            } else {
                                self.busiList?.append(contentsOf: response ?? [])
                            }
                            self.tableView.reloadData()
                        }
                        completionHandler()
                    })
                }
//                break
//            }
            
            
            break
        }
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension InstructionalServiceProviderListViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 15
        
        return self.busiList != nil ? (self.busiList?.count)! : 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: InstructionalServiceProviderListCell.nameOfClass, for: indexPath) as! InstructionalServiceProviderListCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
//        cell.cellIndex = indexPath
//        cell.delegate = self
//
        
        let busiInfo = self.busiList![indexPath.row]
        
        //TODO: For testing
        if(busiInfo.mediabusiness != nil && (busiInfo.mediabusiness?.count)! > 0){
            let busiProfileImage = busiInfo.mediabusiness![0].thumb ?? ""
            cell.imgProfilePic.sd_setImage(with: URL(string: busiProfileImage), completed: nil)
        } else {
            cell.imgProfilePic.image = nil
        }
        
        cell.lblTitle.text = busiInfo.busi_title ?? ""
        cell.lblDesciption.text = busiInfo.busi_statement ?? ""
        cell.lblListCount.text = "\(busiInfo.totalservice ?? 0)"
        cell.starRatingView.value = CGFloat(truncating: busiInfo.rating ?? 0.0)
        
        if let location = busiInfo.location_address, !location.trim().isEmpty {
            cell.lblLocation.text = location
        } else if let location = busiInfo.location_address_sec, !location.trim().isEmpty {
            cell.lblLocation.text = location
        } else {
            cell.lblLocation.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//
//        if(self.instructionalServiceProviderDetailViewController == nil){
//            self.instructionalServiceProviderDetailViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalServiceProviderStoryboard, viewControllerName: InstructionalServiceProviderDetailViewController.nameOfClass) as InstructionalServiceProviderDetailViewController
//
//            self.instructionalServiceProviderDetailViewController?.busiInfo = self.busiList![indexPath.row]
//            self.instructionalServiceProviderDetailViewController?.screenTitle = LocalizationKeys.business_information.getLocalized()
//
//            self.navigationController?.pushViewController(self.instructionalServiceProviderDetailViewController!, animated: true)
//        }
        if isBuildAPackage {
            let storyboard = UIStoryboard(name: HelperConstant.PackageStoryboard, bundle: nil)
            let spllVC = storyboard.instantiateViewController(withIdentifier: PackageServiceListViewController.nameOfClass) as! PackageServiceListViewController
            spllVC.busiInfo = busiList?[indexPath.row]
            spllVC.totalServiceSelection = totalServiceSelection
            spllVC.totalProfileSelection = totalProfileSelection
            spllVC.selectedServiceList = selectedServices
            spllVC.selectedPackage = selectedPackage
            navigationController?.pushViewController(spllVC, animated: true)
        } else if(self.bidetailViewController == nil){
            self.bidetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.BusinessInformationStoryboard, viewControllerName: BIDetailViewController.nameOfClass) as BIDetailViewController
            
//            let data       =    self.biAddRequestList?[indexPath.row]
            bidetailViewController?.biAddRequest = self.busiList![indexPath.row]
            self.navigationController?.pushViewController(self.bidetailViewController!, animated: true)
        }

    }
    
}

class InstructionalServiceProviderListCell: UITableViewCell {
    @IBOutlet weak var imgProfilePic : ImageLayerSetup!
    @IBOutlet weak var lblTitle         : UILabel!
    @IBOutlet weak var lblDesciption    : UILabel!
    @IBOutlet weak var lblListCount       : UILabel!
    @IBOutlet weak var starRatingView : HCSStarRatingView!
    @IBOutlet weak var btnSelection      : UIButton!
    @IBOutlet weak var lblLocation       : UILabel!
}


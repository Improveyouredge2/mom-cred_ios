//
//  InstructionalServiceProviderMapViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * InstructionalServiceProviderMapViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class InstructionalServiceProviderMapViewController : LMBaseViewController {
    
//    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblReviewCount : UILabel!
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //presenter.connectView(view: self)
//        let userName = PMUserDefault.getUserFirstName()
//        if(userName != nil){
//            let profileImageUrl = PMUserDefault.getProfilePic()
//            self.lblTitle.text = userName
//            self.lblSubTitle.text = PMUserDefault.getUserName()
//            
//            if(profileImageUrl != nil){
//                //                self.imgProfile?.sd_setImage(with: URL(string: (profileImageUrl!)), for: .normal, completed: nil)
//                self.imgProfile.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
//            }
//        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        self.presenter.getHelpAndSupport()
    }
    
}

extension InstructionalServiceProviderMapViewController{
    @IBAction func actionDone(_ sender: UIButton) {
        
    }
}

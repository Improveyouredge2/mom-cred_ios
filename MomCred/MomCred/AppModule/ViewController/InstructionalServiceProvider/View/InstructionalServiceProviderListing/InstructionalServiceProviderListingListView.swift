//
//  InstructionalServiceProviderListingListView.swift
//  MomCred
//
//  Created by Apple_iOS on 05/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class InstructionalServiceProviderListingListView: LMBaseViewController, IndicatorInfoProvider {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet private weak var navigationBar: NavigationBarView! {
        didSet {
            navigationBar.navigationController = navigationController
        }
    }
    
    var presenter = InstructionalServiceProviderListingListPresenter()
    var itemInfo: IndicatorInfo = "Items"
    
    var busiInfo:BIAddRequest?
    var tabScreenType:TabScreenType = .Service
    
    var serviceCatId = ""
    var serviceList:[ServiceAddRequest]?
    var personnelList:[PersonnelAddRequest]?
    var facilityList:[MyFacilityAddRequest]?
    var insideLookList:[ILCAddRequest]?
    var instructionalList:[ICAddRequest]?
    
    
    var quickSearchRequest:QuickSearchRequest?
    var serviceSearchRequest:ServiceSearchRequest?
    var facilitySearchRequest: FacilitySearchRequest?
    var personnelSearchRequest :PersonnelSearchRequest?
    var insideLookSearchRequest: InsideLookSearchRequest?
    var instructionalSearchRequest: InstructionalSearchRequest?
    
    var isNavigationShow = false
    var isAdvanceSearchResultShow = false
    
    var currentPageIndex = 1
    
    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    fileprivate var isDisplayLoader = true
    
    fileprivate var serviceDetailView:ServiceDetailView?
    fileprivate var myPersonnelDetailViewController:MyPersonnelDetailViewController?
    fileprivate var myFacilitiesDetailViewController:MyFacilitiesDetailViewController?
    fileprivate var ilcDetailViewController:ILCDetailViewController?
    fileprivate var icDetailViewController:ICDetailViewController?
    
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add refresh loader for pulling and refresh
        if(self.isAdvanceSearchResultShow){
            refreshView.delegate = self
            tableView.addPullLoadableView(refreshView, type: .refresh)
            loadMoreView.delegate = self
            tableView.addPullLoadableView(loadMoreView, type: .loadMore)
        }
        
        if(!self.isNavigationShow){
            self.constraintNavigationHeight?.constant = 0
        }
        
        self.presenter.connectView(view: self)
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewwill appear on tab change")
        self.serviceDetailView = nil
        self.myPersonnelDetailViewController = nil
        self.myFacilitiesDetailViewController = nil
        self.ilcDetailViewController = nil
        self.icDetailViewController = nil
        
        if(!self.isAdvanceSearchResultShow){
            switch tabScreenType {
            case .Service:
                if(self.serviceCatId != nil && self.serviceCatId.length > 0){
                    presenter.getSubCatServiceProviderList(subCatId: self.serviceCatId)
                }
                break
            case .Personnel:
                presenter.getAllPersonnelListDetail()
                break
            case .Facility:
                presenter.getAllFacilityListDetail()
                break
            case .InsideLook:
                presenter.getAllInsideLookListDetail()
                break
            case .InstructionalContent:
                presenter.getAllInstructionalListDetail()
                break
            default:
                break
            }
        }
    }
}

extension InstructionalServiceProviderListingListView{
    func updateScreenInfo(){
        self.tableView.reloadData()
    }
}

extension InstructionalServiceProviderListingListView: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .pulling(offset, threshould):
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                if(self.isAdvanceSearchResultShow){
                    switch tabScreenType {
                    case .Service:
                        
                        if(self.quickSearchRequest != nil){
                            presenter.serverQuickSearchListing(quickSearchRequest: self.quickSearchRequest, callback: {
                                (status, response) in
                                
                                if(response != nil && (response?.count)! > 0){
                                    if(self.currentPageIndex == 1){
                                        self.serviceList = []
                                        self.serviceList = response
                                    } else {
                                        self.serviceList?.append(contentsOf: response ?? [])
                                    }
                                    self.tableView.reloadData()
                                }
                                completionHandler()
                            })
                            
                        } else if(self.serviceSearchRequest != nil){
                            self.serviceSearchRequest?.page_id = "\(self.currentPageIndex)"
                            presenter.serverSearchListing(serviceSearchRequest: self.serviceSearchRequest, callback: {
                                (status, response) in
                                
                                if(status){
                                    
                                    if(response != nil && (response?.count)! > 0){
                                        if(self.currentPageIndex == 1){
                                            self.serviceList = []
                                            self.serviceList = response
                                        } else {
                                            self.serviceList?.append(contentsOf: response ?? [])
                                        }
                                    }
                                    self.tableView.reloadData()
                                }
                                completionHandler()
                            })
                        }
                        break
                    case .Personnel:
                        self.personnelSearchRequest?.page_id = "\(self.currentPageIndex)"
                        presenter.serverPersonnelSearchListing(personnelSearchRequest: self.personnelSearchRequest, callback: {
                            (status, response) in
                            if(status){
                                if(response != nil && (response?.count)! > 0){
                                    if(self.currentPageIndex == 1){
                                        self.personnelList = []
                                        self.personnelList = response
                                    } else {
                                        self.personnelList?.append(contentsOf: response ?? [])
                                    }
                                }
                                self.tableView.reloadData()
                            }
                            
                            completionHandler()
                        })
                        break
                    case .Facility:
                        self.facilitySearchRequest?.page_id = "\(self.currentPageIndex)"
                        presenter.serverFacilitySearchListing(facilitySearchRequest: self.facilitySearchRequest, callback: {
                            (status, response) in
                            if(status){
                                if(response != nil && (response?.count)! > 0){
                                    if(self.currentPageIndex == 1){
                                        self.facilityList = []
                                        self.facilityList = response
                                    } else {
                                        self.facilityList?.append(contentsOf: response ?? [])
                                    }
                                }
                                self.tableView.reloadData()
                            }
                            
                            completionHandler()
                        })
                        break
                    case .InsideLook:
                        self.insideLookSearchRequest?.page_id = "\(self.currentPageIndex)"
                        presenter.serverInsideLookSearchListing(insideLookSearchRequest: self.insideLookSearchRequest, callback: {
                            (status, response) in
                            if(status){
                                if(response != nil && (response?.count)! > 0){
                                    if(self.currentPageIndex == 1){
                                        self.insideLookList = []
                                        self.insideLookList = response
                                    } else {
                                        self.insideLookList?.append(contentsOf: response ?? [])
                                    }
                                }
                                self.tableView.reloadData()
                            }
                            
                            completionHandler()
                        })
                        break
                    case .InstructionalContent:
                        self.instructionalSearchRequest?.page_id = "\(self.currentPageIndex)"
                        presenter.serverInstructionalSearchListing(instructionalSearchRequest: self.instructionalSearchRequest, callback: {
                            (status, response) in
                            if(status){
                                if(response != nil && (response?.count)! > 0){
                                    if(self.currentPageIndex == 1){
                                        self.instructionalList = []
                                        self.instructionalList = response
                                    } else {
                                        self.instructionalList?.append(contentsOf: response ?? [])
                                    }
                                }
                                self.tableView.reloadData()
                            }
                            
                            completionHandler()
                        })
                        break
                    default:
                        break
                    }
                }
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 1
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
//            presenter.getAllServiceHomeListDetail(callback: {
//                (status, response, message) in
//                
//                completionHandler()
//            })
            currentPageIndex = 1
            if(self.isAdvanceSearchResultShow){
                switch tabScreenType {
                case .Service:
                    if(self.quickSearchRequest != nil){
                        presenter.serverQuickSearchListing(quickSearchRequest: self.quickSearchRequest, callback: {
                            (status, response) in
                            
                            if(response != nil && (response?.count)! > 0){
                                if(self.currentPageIndex == 1){
                                    self.serviceList = []
                                    self.serviceList = response
                                } else {
                                    self.serviceList?.append(contentsOf: response ?? [])
                                }
                                self.tableView.reloadData()
                            }
                            completionHandler()
                        })
                        
                    } else if(self.serviceSearchRequest != nil){
                        self.serviceSearchRequest?.page_id = "\(self.currentPageIndex)"
                        presenter.serverSearchListing(serviceSearchRequest: self.serviceSearchRequest, callback: {
                            (status, response) in
                            if(status){
                                if(response != nil && (response?.count)! > 0){
                                    if(self.currentPageIndex == 1){
                                        self.serviceList = []
                                        self.serviceList = response
                                    } else {
                                        self.serviceList?.append(contentsOf: response ?? [])
                                    }
                                }
                                self.tableView.reloadData()
                            }
                            
                            completionHandler()
                        })
                    }
                    break
                case .Personnel:
                    self.personnelSearchRequest?.page_id = "\(self.currentPageIndex)"
                    presenter.serverPersonnelSearchListing(personnelSearchRequest: self.personnelSearchRequest, callback: {
                        (status, response) in
                        if(status){
                            if(response != nil && (response?.count)! > 0){
                                if(self.currentPageIndex == 1){
                                    self.personnelList = []
                                    self.personnelList = response
                                } else {
                                    self.personnelList?.append(contentsOf: response ?? [])
                                }
                            }
                            self.tableView.reloadData()
                        }
                        
                        completionHandler()
                    })
                    break
                case .Facility:
                    self.facilitySearchRequest?.page_id = "\(self.currentPageIndex)"
                    presenter.serverFacilitySearchListing(facilitySearchRequest: self.facilitySearchRequest, callback: {
                        (status, response) in
                        if(status){
                            if(response != nil && (response?.count)! > 0){
                                if(self.currentPageIndex == 1){
                                    self.facilityList = []
                                    self.facilityList = response
                                } else {
                                    self.facilityList?.append(contentsOf: response ?? [])
                                }
                            }
                            self.tableView.reloadData()
                        }
                        
                        completionHandler()
                    })
                    break
                case .InsideLook:
                    self.insideLookSearchRequest?.page_id = "\(self.currentPageIndex)"
                    presenter.serverInsideLookSearchListing(insideLookSearchRequest: self.insideLookSearchRequest, callback: {
                        (status, response) in
                        if(status){
                            if(response != nil && (response?.count)! > 0){
                                if(self.currentPageIndex == 1){
                                    self.insideLookList = []
                                    self.insideLookList = response
                                } else {
                                    self.insideLookList?.append(contentsOf: response ?? [])
                                }
                            }
                            self.tableView.reloadData()
                        }
                        
                        completionHandler()
                    })
                    break
                case .InstructionalContent:
                    self.instructionalSearchRequest?.page_id = "\(self.currentPageIndex)"
                    presenter.serverInstructionalSearchListing(instructionalSearchRequest: self.instructionalSearchRequest, callback: {
                        (status, response) in
                        if(status){
                            if(response != nil && (response?.count)! > 0){
                                if(self.currentPageIndex == 1){
                                    self.instructionalList = []
                                    self.instructionalList = response
                                } else {
                                    self.instructionalList?.append(contentsOf: response ?? [])
                                }
                            }
                            self.tableView.reloadData()
                        }
                        
                        completionHandler()
                    })
                    break
                default:
                    break
                }
            }
            break
        }
    }
}


//MARK:- UITableView Datasource and Delegates
//MARK:-
extension InstructionalServiceProviderListingListView : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 10
        switch tabScreenType {
        case .Service:
            return self.serviceList != nil ? (self.serviceList?.count)! : 0
            break
        case .Personnel:
            return self.personnelList != nil ? (self.personnelList?.count)! : 0
        case .Facility:
            return self.facilityList != nil ? (self.facilityList?.count)! : 0
        case .InsideLook:
            return self.insideLookList != nil ? (self.insideLookList?.count)! : 0
        case .InstructionalContent:
            return self.instructionalList != nil ? (self.instructionalList?.count)! : 0
        default:
            break
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        let data       =    self.arrNoteList[indexPath.row]
        switch tabScreenType {
        case .Service:
            let cell: ServiceListTableViewCell = tableView.dequeueReusableCell(withIdentifier: ServiceListTableViewCell.nameOfClass) as! ServiceListTableViewCell
            cell.service = serviceList?[indexPath.row]
            return cell
            /*
            let data       =    self.serviceList?[indexPath.row]
            let cell        =       tableView.dequeueReusableCell(withIdentifier: InstructionalServiceProviderListingListViewCell.nameOfClass, for: indexPath) as! InstructionalServiceProviderListingListViewCell
            
            if(data?.mediabusiness != nil && (data?.mediabusiness?.count)! > 0 && data?.mediabusiness![0].thumb != nil){
                cell.imgProfilePic.sd_setImage(with: URL(string: data?.mediabusiness![0].thumb ?? ""), completed: nil)
            }
            
            cell.lblTitle.text = data?.service_name ?? ""
            //        cell.lblDesciption.text = data?.service_location_info != nil ? data?.service_location_info?.location_address : ""
            
            var address = ""
            if(data?.service_location_info?.location_name != nil && (data?.service_location_info?.location_name?.length)! > 0){
                address = data?.service_location_info?.location_address ?? ""
                
            } else if(data?.service_location_info?.location_name_sec != nil && (data?.service_location_info?.location_name_sec?.length)! > 0){
                address = data?.service_location_info?.location_address_sec ?? ""
            }
            cell.lblDesciption.text = address
            
            cell.lblPurchaseCount.text = "0 / \(data?.number_of_purchase ?? " ") purchased"
            
            if(data?.service_price != nil){
                cell.lblPrice.text = data?.service_price?.service_price ?? ""
            } else {
                cell.lblPrice.text = ""
            }
            
            if(data?.status != "1"){
                cell.lblInActive.isHidden = false
            } else {
                cell.lblInActive.isHidden = true
            }
            
            var serviceTypes = [UIImage]()
            if let creditSystem = data?.credit_system, creditSystem == "1", let imgCredit = UIImage(named: "ic_credit") {
                serviceTypes.append(imgCredit)
            }
            if let isDateEmpty = data?.service_calendar?.date?.isEmpty, let isTimeEmpty = data?.service_calendar?.time?.isEmpty, !isDateEmpty, !isTimeEmpty, let imgCalendar = UIImage(named: "ic_calendar") {
                serviceTypes.append(imgCalendar)
            }
            if let charitableEvent = data?.service_event?.causeCharitableEvent, let charitablePercentage = data?.service_event?.causeCharitablePercentage, !charitableEvent.isEmpty, !charitablePercentage.isEmpty, let imgDonation = UIImage(named: "ic_donation") {
                serviceTypes.append(imgDonation)
            }
            if let purchasability = data?.purchasablity, purchasability == "1", let imgPurchasability = UIImage(named: "ic_purchase") {
                serviceTypes.append(imgPurchasability)
            }
            if let exceptional = data?.service_information?.exceptionalServices, !exceptional.isEmpty, let imgExceptional = UIImage(named: "ic_exceptional") {
                serviceTypes.append(imgExceptional)
            }
            
            // remove all arranged subviews in case of reusing the cell
            cell.serviceTypesView.removeAllArrangedSubviews()
            for serviceImage in serviceTypes {
                let imageView = UIImageView(image: serviceImage)
                imageView.contentMode = .scaleAspectFit
                cell.serviceTypesView.addArrangedSubview(imageView)
            }
            
            return cell
         */
        case .Personnel:
            let data       =    self.personnelList?[indexPath.row]
            let cell        =       tableView.dequeueReusableCell(withIdentifier: InstructionalServiceProviderListingListViewCell.nameOfClass, for: indexPath) as! InstructionalServiceProviderListingListViewCell
            
            if(data?.mediabusiness != nil && (data?.mediabusiness?.count)! > 0 && data?.mediabusiness![0].thumb != nil){
                cell.imgProfilePic.sd_setImage(with: URL(string: data?.mediabusiness![0].thumb ?? ""), completed: nil)
            }
            
            cell.lblTitle.text = data?.personal_name ?? ""
            var address = ""
            if(data?.personal_location_info?.location_name != nil && (data?.personal_location_info?.location_name?.length)! > 0){
                address = data?.personal_location_info?.location_address ?? ""
                
            } else if(data?.personal_location_info?.location_name_sec != nil && (data?.personal_location_info?.location_name_sec?.length)! > 0){
                address = data?.personal_location_info?.location_address_sec ?? ""
            }
            cell.lblDesciption.text = address
            
            cell.lblPurchaseCount.text = ""
            cell.lblPrice.text = ""
            
            cell.lblInActive.isHidden = true
            cell.viewSpotPrice.isHidden = true
            
            return cell
            
        case .Facility:
            let data       =    self.facilityList?[indexPath.row]
            let cell        =       tableView.dequeueReusableCell(withIdentifier: InstructionalServiceProviderListingListViewCell.nameOfClass, for: indexPath) as! InstructionalServiceProviderListingListViewCell
            
            if(data?.mediabusiness != nil && (data?.mediabusiness?.count)! > 0 && data?.mediabusiness![0].thumb != nil){
                cell.imgProfilePic.sd_setImage(with: URL(string: data?.mediabusiness![0].thumb ?? ""), completed: nil)
            }
            
            cell.lblTitle.text = data?.facility_name ?? ""
            
            var address = ""
            if(data?.facility_location_info?.location_name != nil && (data?.facility_location_info?.location_name?.length)! > 0){
                address = data?.facility_location_info?.location_address ?? ""
                
            } else if(data?.facility_location_info?.location_name_sec != nil && (data?.facility_location_info?.location_name_sec?.length)! > 0){
                address = data?.facility_location_info?.location_address_sec ?? ""
            }
            cell.lblDesciption.text = address
            
            cell.lblPurchaseCount.text = ""
            cell.lblPrice.text = ""
            cell.lblInActive.isHidden = true
            cell.viewSpotPrice.isHidden = true
            
            return cell
        case .InsideLook:
            let data       =    self.insideLookList?[indexPath.row]
            let cell        =       tableView.dequeueReusableCell(withIdentifier: InstructionalServiceProviderListingListViewCell.nameOfClass, for: indexPath) as! InstructionalServiceProviderListingListViewCell
            
            if(data?.mediabusiness != nil && (data?.mediabusiness?.count)! > 0 && data?.mediabusiness![0].thumb != nil){
                cell.imgProfilePic.sd_setImage(with: URL(string: data?.mediabusiness![0].thumb ?? ""), completed: nil)
            }
            
            cell.lblTitle.text = data?.look_name ?? ""
            var address = ""
            if(data?.look_location_info?.location_name != nil && (data?.look_location_info?.location_name?.length)! > 0){
                address = data?.look_location_info?.location_address ?? ""
                
            } else if(data?.look_location_info?.location_name_sec != nil && (data?.look_location_info?.location_name_sec?.length)! > 0){
                address = data?.look_location_info?.location_address_sec ?? ""
            }
            cell.lblDesciption.text = address
            
            cell.lblInActive.isHidden = true
            cell.viewSpotPrice.isHidden = true
            
            return cell
        case .InstructionalContent:
            let data       =    self.instructionalList?[indexPath.row]
            let cell        =       tableView.dequeueReusableCell(withIdentifier: InstructionalServiceProviderListingListViewCell.nameOfClass, for: indexPath) as! InstructionalServiceProviderListingListViewCell
            
            if(data?.mediabusiness != nil && (data?.mediabusiness?.count)! > 0 && data?.mediabusiness![0].thumb != nil){
                cell.imgProfilePic.sd_setImage(with: URL(string: data?.mediabusiness![0].thumb ?? ""), completed: nil)
            }
            
            cell.lblTitle.text = data?.content_name ?? ""
            
            var address = ""
            if(data?.content_location_info?.location_name != nil && (data?.content_location_info?.location_name?.length)! > 0){
                address = data?.content_location_info?.location_address ?? ""
                
            } else if(data?.content_location_info?.location_name_sec != nil && (data?.content_location_info?.location_name_sec?.length)! > 0){
                address = data?.content_location_info?.location_address_sec ?? ""
            }
            cell.lblDesciption.text = address
            
            cell.lblInActive.isHidden = true
            cell.viewSpotPrice.isHidden = true
            
            return cell
        default:
            break
        }
        
        return UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch tabScreenType {
        case .Service:
            if(self.serviceDetailView == nil){
                self.serviceDetailView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceDetailView.nameOfClass) as ServiceDetailView
                
                self.serviceDetailView?.serviceAddRequest = self.serviceList?[indexPath.row]
                self.serviceDetailView?.isHideEditOption = true
                self.serviceDetailView?.busiInfo = self.busiInfo
                
                self.navigationController?.pushViewController(self.serviceDetailView!, animated: true)
            }
            break
        case .Personnel:
            if(self.myPersonnelDetailViewController == nil){
                self.myPersonnelDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PersonnelStoryboard, viewControllerName: MyPersonnelDetailViewController.nameOfClass) as MyPersonnelDetailViewController
                
                
                self.myPersonnelDetailViewController?.personnelAddRequest = self.personnelList?[indexPath.row]
                self.myPersonnelDetailViewController?.isHideEditOption = true
                self.navigationController?.pushViewController(self.myPersonnelDetailViewController!, animated: true)
            }
            break
        case .Facility:

            if(self.myFacilitiesDetailViewController == nil){
                self.myFacilitiesDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.FacilityStoryboard, viewControllerName: MyFacilitiesDetailViewController.nameOfClass) as MyFacilitiesDetailViewController
                
                
                self.myFacilitiesDetailViewController?.myFacilityAddRequest = self.facilityList?[indexPath.row]
                
                self.myFacilitiesDetailViewController?.isHideEditOption = true
                self.navigationController?.pushViewController(self.myFacilitiesDetailViewController!, animated: true)
            }
            break
        case .InsideLook:
            
            if(self.ilcDetailViewController == nil){
                self.ilcDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InsideLookContentStoryboard, viewControllerName: ILCDetailViewController.nameOfClass) as ILCDetailViewController
                
                
                self.ilcDetailViewController?.ilcAddRequest = self.insideLookList?[indexPath.row]
                self.ilcDetailViewController?.isHideEditOption = true
                self.navigationController?.pushViewController(self.ilcDetailViewController!, animated: true)
            }
            break
        case .InstructionalContent:

            if(self.icDetailViewController == nil){
                self.icDetailViewController =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalContentStoryboard, viewControllerName: ICDetailViewController.nameOfClass) as ICDetailViewController
                
                
                self.icDetailViewController?.icAddRequest = self.instructionalList?[indexPath.row]
                self.icDetailViewController?.isHideEditOption = true
                self.navigationController?.pushViewController(self.icDetailViewController!, animated: true)
            }
            break
        default:
            break
        }
    }
}

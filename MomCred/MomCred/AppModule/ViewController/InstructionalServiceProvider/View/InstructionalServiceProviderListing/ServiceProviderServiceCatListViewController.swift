//
//  ServiceProviderServiceCatListViewController.swift
//  MomCred
//
//  Created by Rajesh Yadav on 16/10/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


class ServiceProviderServiceCatListViewController: LMBaseViewController, IndicatorInfoProvider {
    
    //MARK:- IBOutlet(s)
    @IBOutlet weak var tbCatServiceList : UITableView!
    
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    var itemInfo: IndicatorInfo = "Items"
    var busiInfo:BIAddRequest?
    var myServiceList:[ListingDataDetail]?
    
    fileprivate var presenter = ServiceProviderServiceCatListPresenter()
    fileprivate var instructionalServiceProviderListingListView:InstructionalServiceProviderListingListView?
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.constraintNavigationHeight?.constant = 0
        
        self.presenter.connectView(view: self)
        
        tbCatServiceList.estimatedRowHeight = 100
        tbCatServiceList.rowHeight = UITableView.automaticDimension
        tbCatServiceList.estimatedSectionHeaderHeight = 80
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutSubviews()

        self.instructionalServiceProviderListingListView = nil
        
        self.presenter.getAllServiceListDetail()
    }
}

extension ServiceProviderServiceCatListViewController {
    func updateScreenInfo(){
        if(self.tbCatServiceList != nil){
            self.tbCatServiceList.reloadData()
        }
    }
}

//MARK:- UITableView Datasource and Delegates
//MARK:-
extension ServiceProviderServiceCatListViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.myServiceList != nil && (self.myServiceList?.count)! > 0) ? (self.myServiceList?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 3
        
        return (self.myServiceList != nil && (self.myServiceList?.count)! > section && self.myServiceList![section].subcat != nil && (self.myServiceList![section].subcat?.count)! > 0) ? (self.myServiceList![section].subcat?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = .clear
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = self.myServiceList![section].listing_title ?? ""
        
        label.font = FontsConfig.FontHelper.defaultBoldFontWithSize(16)
        label.textColor = UIColor(hexString: ColorCode.appYellowColor)
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let data       =    self.myServiceList?[indexPath.row]
        
        let cell        =       tableView.dequeueReusableCell(withIdentifier: MyServiceCatCell.nameOfClass, for: indexPath) as! MyServiceCatCell

        if(self.myServiceList![indexPath.section] != nil && self.myServiceList![indexPath.section].subcat![indexPath.row] != nil){
            cell.lblTitle.text  = self.myServiceList![indexPath.section].subcat![indexPath.row].listing_title  ?? ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.instructionalServiceProviderListingListView == nil){
            let storyboard = UIStoryboard(name: HelperConstant.InstructionalServiceProviderStoryboard, bundle: nil)
            self.instructionalServiceProviderListingListView = storyboard.instantiateViewController(withIdentifier: InstructionalServiceProviderListingListView.nameOfClass) as! InstructionalServiceProviderListingListView
            self.instructionalServiceProviderListingListView?.isNavigationShow = true
            self.instructionalServiceProviderListingListView?.tabScreenType = .Service
            self.instructionalServiceProviderListingListView?.busiInfo = self.busiInfo
            self.instructionalServiceProviderListingListView?.screenTitle = self.myServiceList![indexPath.section].subcat![indexPath.row].listing_title  ?? ""
            self.instructionalServiceProviderListingListView?.serviceCatId = self.myServiceList![indexPath.section].subcat![indexPath.row].listing_id  ?? ""
            
            self.navigationController?.pushViewController(self.instructionalServiceProviderListingListView!, animated: true)
        }
    }
}


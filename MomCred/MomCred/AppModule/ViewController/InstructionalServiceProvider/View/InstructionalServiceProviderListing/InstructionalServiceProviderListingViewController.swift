//
//  InstructionalServiceProviderListingViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

enum TabScreenType:String{
    case Service
    case Facility
    case Personnel
    case InsideLook
    case InstructionalContent
}

/**
 * InstructionalServiceProviderListingViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */

class InstructionalServiceProviderListingViewController: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var lblMyTransactions: UILabel!
    
    fileprivate var menuArray: [HSMenu] = []
    
    var busiInfo:BIAddRequest?
    
    override func viewDidLoad() {
        self.setupScreenData()
        
        let menu1 = HSMenu(icon: nil, title: LocalizationKeys.add_new.getLocalized())
        menuArray = [menu1]
    }
    
    func setupScreenData(){
        
        settings.style.buttonBarBackgroundColor = #colorLiteral(red: 0.2823529412, green: 0.3137254902, blue: 0.3490196078, alpha: 1)
        settings.style.buttonBarItemBackgroundColor = #colorLiteral(red: 0.2823529412, green: 0.3137254902, blue: 0.3490196078, alpha: 1)
        settings.style.selectedBarBackgroundColor = #colorLiteral(red: 0.7843137255, green: 0.01960784314, blue: 0.4784313725, alpha: 1)
        settings.style.selectedBarHeight = 03
        settings.style.buttonBarMinimumLineSpacing = 05
        settings.style.buttonBarItemTitleColor = UIColor.white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        settings.style.buttonBarItemLeftRightMargin = 05
        settings.style.buttonBarItemFont = FontsConfig.FontHelper.defaultRegularFontWithSize(16)
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            //            oldCell?.label.font = FontsConfig.FontHelper.defaultRegularFontWithSize(16)
            newCell?.label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            //newCell?.label.font = FontsConfig.FontHelper.defaultBoldFontWithSize(18)
        }
        
        buttonBarItemSpec = ButtonBarItemSpec.cellClass(width: { _ -> CGFloat in
            return 90  // <-- Your desired width
        })
        super.viewDidLoad()
        //self.setUpLanguageText()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.notificationListViewController = nil
        self.view.layoutSubviews()
        //self.updateBadgeCount()
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let storyboard = UIStoryboard(name: HelperConstant.InstructionalServiceProviderStoryboard, bundle: nil)
        let childOneVC = storyboard.instantiateViewController(withIdentifier: ServiceProviderServiceCatListViewController.nameOfClass) as! ServiceProviderServiceCatListViewController
        childOneVC.busiInfo = self.busiInfo
        childOneVC.itemInfo.title = LocalizationKeys.service.getLocalized()
        
        let childFourVC = storyboard.instantiateViewController(withIdentifier: InstructionalServiceProviderListingListView.nameOfClass) as! InstructionalServiceProviderListingListView
        childFourVC.tabScreenType = .Personnel
        childFourVC.busiInfo = self.busiInfo
        childFourVC.itemInfo.title = LocalizationKeys.personnel.getLocalized()
        
        let childFiveVC = storyboard.instantiateViewController(withIdentifier: InstructionalServiceProviderListingListView.nameOfClass) as! InstructionalServiceProviderListingListView
        childFiveVC.tabScreenType = .Facility
        childFiveVC.busiInfo = self.busiInfo
        childFiveVC.itemInfo.title = LocalizationKeys.facilities.getLocalized()
        
        let childSixVC = storyboard.instantiateViewController(withIdentifier: InstructionalServiceProviderListingListView.nameOfClass) as! InstructionalServiceProviderListingListView
        childSixVC.tabScreenType = .InsideLook
        childSixVC.busiInfo = self.busiInfo
        childSixVC.itemInfo.title = LocalizationKeys.inside_look_content.getLocalized()
        
        let childSevenVC = storyboard.instantiateViewController(withIdentifier: InstructionalServiceProviderListingListView.nameOfClass) as! InstructionalServiceProviderListingListView
        childSevenVC.tabScreenType = .InstructionalContent
        childSevenVC.itemInfo.title = LocalizationKeys.instructional_content.getLocalized()
        childSevenVC.busiInfo = self.busiInfo
        
        return [childOneVC, childFourVC, childFiveVC, childSixVC, childSevenVC]
    }
}

extension InstructionalServiceProviderListingViewController{
    /**
     *  Common Back button click event method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous
     */
    @IBAction func actionNavigationBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOptionMenu(_ sender: AnyObject) {
        let popupMenu = HSPopupMenu(menuArray: menuArray, arrowPoint: CGPoint(x: UIScreen.main.bounds.width-35, y: 64))
        popupMenu.popUp()
        popupMenu.delegate = self
    }
}


extension InstructionalServiceProviderListingViewController: HSPopupMenuDelegate {
    func popupMenu(_ popupMenu: HSPopupMenu, didSelectAt index: Int) {
        print("selected index is: " + "\(index)")
        
        if(index == 0){
         
        }
    }
}


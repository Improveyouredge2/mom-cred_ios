//
//  InstructionalServiceProviderListingListViewCell.swift
//  MomCred
//
//  Created by consagous on 05/06/19.
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit

class InstructionalServiceProviderListingListViewCell: UITableViewCell {

    @IBOutlet weak var cardView : ViewLayerSetup!
    @IBOutlet weak var imgProfilePic : ImageLayerSetup!
//    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var lblDesciption : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblPurchaseCount : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblInActive: UILabel!
    
    @IBOutlet weak var viewSpotPrice: UIStackView!
    @IBOutlet weak var serviceTypesView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
//        self.cardView.backgroundColor   =   UIColor(hexString: ColorCode.cardColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

class ServiceListTableViewCell: UITableViewCell {
    @IBOutlet weak var imgProfilePic: ImageLayerSetup!
    @IBOutlet weak var serviceInfoView: UIStackView!
    @IBOutlet weak var serviceDetailView: UIStackView!
    @IBOutlet weak var serviceTypesView: UIStackView!
    @IBOutlet weak var btnAddToCart: UIButton!

    let appLogo: UIImage? = UIImage(named: "appLogo")
    let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue

    var service: ServiceAddRequest? {
        didSet {
            if role_id == AppUser.ServiceProvider.rawValue {
                btnAddToCart.isHidden = true
            }
            if PMUserDefault.getAppToken() == nil || PMUserDefault.getAppToken()?.trim().isEmpty == true {
                btnAddToCart.isHidden = true
            }
            
            if let image = service?.mediabusiness?.first?.thumb, let url = URL(string: image) {
                imgProfilePic.sd_setImage(with: url, placeholderImage: appLogo)
            } else if let image = service?.mediabusiness?.first?.imageurl, let url = URL(string: image) {
                imgProfilePic.sd_setImage(with: url, placeholderImage: appLogo)
            }  else {
                imgProfilePic.image = appLogo
            }
            
            var infoViews: [InfoStackView] = []
            if let value = service?.provider_name {
                let infoV = infoView(title: "Service provider", value: value)
                infoViews.append(infoV)
            }
            if let value = service?.service_name {
                let infoV = infoView(title: "Service Name", value: value)
                infoViews.append(infoV)
            }

            serviceInfoView.removeAllArrangedSubviews()
            for view in infoViews {
                serviceInfoView.addArrangedSubview(view)
            }

            var detailViews: [InfoStackView] = []
            if let value = service?.service_instructor?.inFo_office {
                let infoV = infoView(title: "Service type", value: value)
                detailViews.append(infoV)
            }
            if let serviceFields = service?.service_field {
                for field in serviceFields {
                    if let fieldTitle = field.catFieldName, let specificField = field.specificFieldName {
                        let infoV = infoView(title: "Field category", value: fieldTitle)
                        detailViews.append(infoV)

                        let infoV1 = infoView(title: "Specific field", value: specificField)
                        detailViews.append(infoV1)
                    }
                }
            }

            if let purchasability = service?.purchasablity, purchasability == "1", let numberOfPurchase = service?.number_of_purchase {
                let totalPurchases: String = service?.total_purchase ?? "0"
                if numberOfPurchase.trim() == "0" {
                    let infoV = infoView(title: "Spots", value: "\(totalPurchases) / Unlimited")
                    detailViews.append(infoV)
                } else {
                    let infoV = infoView(title: "Spots", value: "\(totalPurchases) / \(numberOfPurchase)")
                    detailViews.append(infoV)
                }
            }
            if let value = service?.donationamount?.trim(), let amount = Double(value) {
                let infoV = infoView(title: "Donation Amount", value: amount.formattedPrice)
                detailViews.append(infoV)
            } else {
                let infoV = infoView(title: "Donation Amount", value: 0.0.formattedPrice)
                detailViews.append(infoV)
            }
            if let value = service?.service_price?.service_price?.trim(), let price = Double(value) {
                let infoV = infoView(title: "Price", value: price.formattedPrice)
                detailViews.append(infoV)
            } else {
                let infoV = infoView(title: "Price", value: 0.0.formattedPrice)
                detailViews.append(infoV)
            }

            serviceDetailView.removeAllArrangedSubviews()
            var currentIndex: Int = 0
            while currentIndex < detailViews.count {
                var itemsUsed: Int = 0
                if detailViews.count > currentIndex + 1 {
                    var arrangedViews: [InfoStackView] = []
                    if detailViews.count > currentIndex {
                        arrangedViews.append(detailViews[currentIndex])
                        itemsUsed += 1
                    }
                    if detailViews.count > currentIndex + 1 {
                        arrangedViews.append(detailViews[currentIndex + 1])
                        itemsUsed += 1
                    }
                    let stackView = UIStackView(arrangedSubviews: arrangedViews)
                    stackView.axis = .horizontal
                    stackView.alignment = .fill
                    stackView.distribution = .fillEqually
                    serviceDetailView.addArrangedSubview(stackView)
                } else {
                    serviceDetailView.addArrangedSubview(detailViews[currentIndex])
                    itemsUsed += 1
                }
                currentIndex += itemsUsed
            }

            if let value = service?.service_location_info?.location_address {
                let infoV = infoView(title: "Location by town", value: value)
                serviceDetailView.addArrangedSubview(infoV)
            }
            
            if let value = service?.service_calendar {
                var dateString: String = ""
                if let date = value.date {
                    dateString = date
                }
                if let time = value.time {
                    if dateString.trim().isEmpty {
                        dateString = time
                    } else {
                        dateString = "\(dateString)/\(time)"
                    }
                }
                if !dateString.trim().isEmpty {
                    let infoV = infoView(title: "Date/Time", value: dateString)
                    infoViews.append(infoV)
                    serviceDetailView.addArrangedSubview(infoV)
                }
            }

            var serviceTypes = [UIImage]()
            if let creditSystem = service?.credit_system, creditSystem == "1", let imgCredit = UIImage(named: "ic_credit") {
                serviceTypes.append(imgCredit)
            }
            if let isDateEmpty = service?.service_calendar?.date?.isEmpty, let isTimeEmpty = service?.service_calendar?.time?.isEmpty, !isDateEmpty, !isTimeEmpty, let imgCalendar = UIImage(named: "ic_calendar") {
                serviceTypes.append(imgCalendar)
            }
            if let charitableEvent = service?.service_event?.causeCharitableEvent, let charitablePercentage = service?.service_event?.causeCharitablePercentage, !charitableEvent.isEmpty, !charitablePercentage.isEmpty, let imgDonation = UIImage(named: "ic_donation") {
                serviceTypes.append(imgDonation)
            } else if let donationAmt = service?.donationamount, let donationAmtValue = Int(donationAmt), donationAmtValue > 0, let imgDonation = UIImage(named: "ic_donation") {
                serviceTypes.append(imgDonation)
            }
            if let purchasability = service?.purchasablity, purchasability == "1", let imgPurchasability = UIImage(named: "ic_purchase") {
                serviceTypes.append(imgPurchasability)
            }
            if let exceptional = service?.service_information?.exceptionalServices, !exceptional.isEmpty, let imgExceptional = UIImage(named: "ic_exceptional") {
                serviceTypes.append(imgExceptional)
            }
            
            // remove all arranged subviews in case of reusing the cell
            serviceTypesView.removeAllArrangedSubviews()
            for serviceImage in serviceTypes {
                let imageView = UIImageView(image: serviceImage)
                imageView.contentMode = .scaleAspectFit
                serviceTypesView.addArrangedSubview(imageView)
            }

            updatePurchaseButton()
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NotificationCenter.default.addObserver(self, selector: #selector(updatePurchaseButton), name: .cartUpdated, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: .cartUpdated, object: nil)
    }
    
    @objc private func updatePurchaseButton() {
        if let request = service, ServiceCart.shared.serviceExistsInCart(request) {
            btnAddToCart.isEnabled = false
            btnAddToCart.setTitle("Added to Cart", for: .normal)
        } else {
            btnAddToCart.isEnabled = true
            btnAddToCart.setTitle("Add to Cart", for: .normal)
        }
    }

    private func infoView(title: String, value: String) -> InfoStackView {
        let info = Bundle.main.loadNibNamed("InfoStackView", owner: self, options: nil)?.first as! InfoStackView
        info.lblTitle.text = title
        info.lblTitle.textColor = UIColor(hex: "C8047A")
        info.lblTitle.font = UIFont.systemFont(ofSize: 16)
        info.lblValue.text = value
        info.lblValue.textColor = .white
        info.lblValue.font = UIFont.systemFont(ofSize: 16)
        return info
    }
    
    @IBAction private func btnAddToCartAction(sender: UIButton) {
        if let subscriptionInfo = PMUserDefault.getSubsriptionInfo() {
            let subscriptionDetail: SubscriptionDetail? = SubscriptionDetail().getModelObjectFromServerResponse(jsonResponse: subscriptionInfo as AnyObject)
            if subscriptionDetail?.sub_end == nil {
                let purchaseMembershipVC: PMLogoutView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
                purchaseMembershipVC.delegate = self
                purchaseMembershipVC.scrTitle = ""
                purchaseMembershipVC.scrDesc = LocalizationKeys.error_no_subscription.getLocalized()
                purchaseMembershipVC.modalPresentationStyle = .overFullScreen
                if let viewController = HelperConstant.appDelegate.activeTabbar {
                    viewController.present(purchaseMembershipVC, animated: true, completion: nil)
                }
            } else if let service = service {
                var shouldAddToCart: Bool = true
                if let numberOfPurchase = service.number_of_purchase, let numberOfPurchaseVal = Int(numberOfPurchase), numberOfPurchaseVal > 0, let totalPurchase = service.total_purchase, let totalPurchaseValue = Int(totalPurchase) {
                    let existingCartServiceCount: Int = ServiceCart.shared.cartServices?.filter({ $0.service_id == service.service_id }).count ?? 0
                    if totalPurchaseValue + existingCartServiceCount > numberOfPurchaseVal {
                        shouldAddToCart = false
                    }
                }

                if shouldAddToCart {
                    ServiceCart.shared.addServiceToCart(service)
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_purchasability_limit.getLocalized(), buttonTitle: nil, controller: nil)
                }
            }
        }
    }
}

extension ServiceListTableViewCell: PMLogoutViewDelegate {
    func cancelLogoutViewScr() { }
    
    func acceptLogoutViewScr() {
        HelperConstant.appDelegate.openPremiumMembershipView()
    }
}

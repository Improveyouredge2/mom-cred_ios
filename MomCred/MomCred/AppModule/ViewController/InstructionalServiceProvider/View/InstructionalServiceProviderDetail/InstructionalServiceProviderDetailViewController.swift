//
//  InstructionalServiceProviderDetailViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ImageSlideshow

/**
 * InstructionalServiceProviderDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class InstructionalServiceProviderDetailViewController : LMBaseViewController {
    
    @IBOutlet weak var slideshow: ImageSlideshow!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblReviewCount : UILabel!
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblPhone : UILabel!
    @IBOutlet weak var starRatingView : HCSStarRatingView!
    
    var busiInfo:BIAddRequest?
    
    fileprivate var instructionalServiceProviderListingViewController:InstructionalServiceProviderListingViewController?
    
    fileprivate var ratingReviewViewController:RatingReviewViewController?
    
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /////////////////////////////////////////////////////////////
//        slideshow.backgroundColor = UIColor.white
        slideshow.slideshowInterval = 3.0
        slideshow.pageControlPosition = PageControlPosition.insideScrollView
        slideshow.pageControl.currentPageIndicatorTintColor = UIColor.white
        slideshow.pageControl.pageIndicatorTintColor = UIColor.lightGray
        slideshow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        //TODO: Remove when image downalod from server api
        ///////////////////////////////////////////////////////////////
        if(self.busiInfo?.mediabusiness != nil && (self.busiInfo?.mediabusiness?.count)! > 0){
            var sdWebImageSource:[SDWebImageSource] = []
            for tempImageUrl in (self.busiInfo?.mediabusiness)! {
                sdWebImageSource.append(SDWebImageSource(urlString: tempImageUrl.thumb ?? "")!)
            }
            self.slideshow.setImageInputs(sdWebImageSource)
            
            // Business Media first image use as thumbnail
            self.imgProfile.sd_setImage(with: URL(string: (self.busiInfo?.mediabusiness![0].thumb ?? "")), completed: nil)
        }
        ///////////////////////////////////////////////////////////////
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
//            print("current page:", page)
        }
        /////////////////////////////////////////////////////////////
        
        self.lblTitle.text = busiInfo?.busi_title
        self.lblSubTitle.text = busiInfo?.busi_statement
        self.starRatingView.value = CGFloat(truncating: busiInfo?.rating ?? 0)
        self.lblAddress.text = busiInfo?.location_address ?? ""
        self.lblPhone.text = busiInfo?.busi_phone?.name ?? ""
        
        //TODO: Add Review count
//        self.lblReviewCount.text = busii
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.instructionalServiceProviderListingViewController = nil
        self.ratingReviewViewController = nil
    }
    
}

extension InstructionalServiceProviderDetailViewController{
    @IBAction func actionListingOffer(_ sender: UIButton) {
        
        if(self.instructionalServiceProviderListingViewController == nil){
            self.instructionalServiceProviderListingViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalServiceProviderStoryboard, viewControllerName: InstructionalServiceProviderListingViewController.nameOfClass) as InstructionalServiceProviderListingViewController
            
            self.instructionalServiceProviderListingViewController?.busiInfo = self.busiInfo
            self.navigationController?.pushViewController(self.instructionalServiceProviderListingViewController!, animated: true)
        }
    }
    
    @IBAction func actionRating(_ sender: UIButton) {
        
        if(self.ratingReviewViewController == nil){
            self.ratingReviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalServiceProviderStoryboard, viewControllerName: RatingReviewViewController.nameOfClass) as RatingReviewViewController
            
            self.ratingReviewViewController?.busiInfo = self.busiInfo
            self.navigationController?.pushViewController(self.ratingReviewViewController!, animated: true)
        }
        
    }
}


//
//  InstructionalServiceProviderListingListService.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  InstructionalServiceProviderListingListService is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class InstructionalServiceProviderListingService{
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getAllServiceListDetail(requestData:BusiServicesRequest?, callback:@escaping (_ status:Bool, _ response: GetListingCategoryModelResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_SERVICE_CATEGORY_BY_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
//                    let  res:DashboardAllServiceResponseData? = DashboardAllServiceResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    let res:GetListingCategoryModelResponse? = GetListingCategoryModelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method to get Forms Option Parent listing.
     *
     *  @param key callback method update information on screen.
     *
     *  @return server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    
    static func getServiceByCategory(requestData: GetListingCategoryModelRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceResponseData?, _ message: String?) -> Void) {
        
        // GET
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        var reqPost:URLRequest?
        if(requestData?.listing_category != nil || requestData?.listing_id != nil){
            reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_SERVICE_BY_CATEGORY, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        } else {
            reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_GET_SERVICE_BY_CATEGORY, header: APIHeaders().getDefaultHeaders(), strJSON: nil)
        }
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:DashboardAllServiceResponseData? = DashboardAllServiceResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getAllPersonnelListDetail(requestData:BusiServicesRequest?, callback:@escaping (_ status:Bool, _ response: PersonnelResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_PERSONNEL_CATEGORY_BY_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:PersonnelResponse? = PersonnelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getAllFacilityListDetail(requestData:BusiServicesRequest?, callback:@escaping (_ status:Bool, _ response: MyFacilityResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_FACILITY_CATEGORY_BY_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:MyFacilityResponse? = MyFacilityResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getAllInsideLookListDetail(requestData:BusiServicesRequest?, callback:@escaping (_ status:Bool, _ response: ILCResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_INSIDE_LOOK_CATEGORY_BY_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:ILCResponse? = ILCResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getAllInstructionalListDetail(requestData:BusiServicesRequest?, callback:@escaping (_ status:Bool, _ response: ICResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_INSTRUCTIONAL_CONTENT_CATEGORY_BY_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:ICResponse? = ICResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
}

//
//  UserServiceDetailViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import Lightbox
import AVKit

/**
 * UserServiceDetailViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class UserServiceDetailViewController : LMBaseViewController {
    
//    @IBOutlet var tableView: UITableView!
    
    // Profile image view
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    @IBOutlet weak var imgPlayBtn : UIImageView!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var constraintDocumentCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitleValue : UILabel!
    @IBOutlet weak var lblSpotValue : UILabel!
    @IBOutlet weak var lblPriceValue : UILabel!
    @IBOutlet weak var lblBusinessValue : UILabel!
    @IBOutlet weak var lblFeildOfferValue : UILabel!
    @IBOutlet weak var lblAddressValue : UILabel!
    @IBOutlet weak var lblPhoneValue : UILabel!
    @IBOutlet weak var lblLinkValue : UILabel!
    
    @IBOutlet weak var btnPurchaseNow : UIButton!
    @IBOutlet weak var btnBuildPackage : UIButton!
    @IBOutlet weak var viewBuildPackage : UIView!
    
    @IBOutlet weak var viewDesc : UIStackView!
    @IBOutlet weak var viewDocument : UIStackView!
    @IBOutlet weak var viewFieldOffered : UIStackView!
    @IBOutlet weak var viewPhone : UIStackView!
    @IBOutlet weak var viewLink : UIStackView!
    @IBOutlet weak var viewAddress : UIStackView!
    
    @IBOutlet weak var tableView : UITableView!
    
    @IBOutlet weak var constraintBuildViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintTableViewHeight: NSLayoutConstraint!
    
    var tabScreenType:TabScreenType = .Service
    var service:ServiceAddRequest?
    var personnel:PersonnelAddRequest?
    var facility:MyFacilityAddRequest?
    var insideLook:ILCAddRequest?
    var instructional:ICAddRequest?
    
    var busiInfo:BIAddRequest?
    
    fileprivate var logoutViewController:PMLogoutView?
    fileprivate var packagePaymentViewController:PackagePaymentViewController?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.updateScreenInfo()
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.logoutViewController = nil
        self.packagePaymentViewController = nil
        //        self.presenter.getHelpAndSupport()
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.tableView.contentSize.height > 0){
             tableView.scrollToBottom()
            self.constraintTableViewHeight?.constant =
                self.tableView.contentSize.height
            
            self.updateTableViewHeight(tableView: self.tableView)
        }
    }
    
    fileprivate func updateTableViewHeight(tableView:UITableView) {
        // Page 2
        if(tableView == self.tableView){
            UIView.animate(withDuration: 0, animations: {
                self.tableView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.tableView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.constraintTableViewHeight.constant = heightOfTableView
            }
        }
    }
    
}

extension UserServiceDetailViewController{
    fileprivate func updateScreenInfo(){
        
        self.viewPhone.isHidden = true
        self.viewLink.isHidden = true
        
        switch tabScreenType {
        case .Service:
            
            btnPurchaseNow.setTitle(LocalizationKeys.purchase_now.getLocalized(), for: UIControl.State.normal)
            btnBuildPackage.setTitle(LocalizationKeys.build_package.getLocalized(), for: UIControl.State.normal)
            
            let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
            
            switch  role_id {
            case AppUser.ServiceProvider.rawValue:
                btnPurchaseNow.setTitle(LocalizationKeys.edit_service.getLocalized(), for: UIControl.State.normal)
                btnBuildPackage.setTitle(LocalizationKeys.view_purchase.getLocalized(), for: UIControl.State.normal)
                
                break
            case AppUser.LocalBusiness.rawValue:
                break
            case AppUser.Enthusiast.rawValue:
                break
            default:
                break
            }
            
            let profileImageUrl = PMUserDefault.getProfilePic()
            if(profileImageUrl != nil){
                self.imgProfile.sd_setImage(with: URL(string: (profileImageUrl!)))
            }
            
            if(self.service?.mediabusiness != nil && (self.service?.mediabusiness?.count)! > 0){
                self.imgProfile.sd_setImage(with: URL(string: (self.service?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((self.service?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    self.imgPlayBtn.isHidden = false
                } else {
                    self.imgPlayBtn.isHidden = true
                }
            } else {
                self.imgPlayBtn.isHidden = true
                self.imgProfile.image = nil
                self.constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
            }
            
            self.collectionView.reloadData()
            
            self.lblTitleValue.text = self.service?.service_name ?? ""
            self.lblSpotValue.text = self.service?.number_of_purchase ?? ""
            self.lblPriceValue.text = HelperConstant.CURRENCY_SYMBOL + " " + (self.service?.service_price?.service_price ?? "")
            
            //TODO: Hide description
            //        self.lblBusinessValue.text = self.service ?? ""
            
            let service_Field:[String] = self.service?.service_field != nil ? (self.service?.service_field?.map{$0.specificFieldName})! as! [String] : [""]
            
            if(service_Field.count > 0){
                self.lblFeildOfferValue.text = "• \(service_Field.joined(separator: "\n• ") )"
            } else {
                self.lblFeildOfferValue.text = ""
            }
            
//            self.lblAddressValue.text = self.service?.service_location_info != nil ? self.service?.service_location_info?.location_address : ""
            
            var address = ""
            if(self.service?.service_location_info?.location_name != nil && (self.service?.service_location_info?.location_name?.length)! > 0){
                
                address = self.service?.service_location_info?.location_address ?? ""
                
            } else if(self.service?.service_location_info?.location_name_sec != nil && (service?.service_location_info?.location_name_sec?.length)! > 0){
                
                address = self.service?.service_location_info?.location_address_sec ?? ""
            }
            
            self.lblAddressValue.text = address
            
            self.lblPhoneValue.text = ""
            self.lblPhoneValue.isHidden = true
            
            self.lblLinkValue.text = ""
            self.lblLinkValue.isHidden = true
            
            if(!((self.service != nil && self.service?.doc != nil) && (self.service?.doc?.count)! > 0)){
                self.constraintTableViewHeight.constant = 0
            }
            
            self.viewDesc.isHidden = true
            //        self.viewDocument.isHidden = true
            //        self.viewFieldOffered.isHidden = true
            //        self.viewPhone.isHidden = true
            //        self.viewLink.isHidden = true
            //        self.viewAddress.isHidden = true
            
            break
        case .Personnel:
            
            self.btnPurchaseNow.isHidden = true
//            self.btnBuildPackage.isHidden = true
            self.viewBuildPackage.isHidden = true
            
            if(self.personnel?.mediabusiness != nil && (self.personnel?.mediabusiness?.count)! > 0){
                self.imgProfile.sd_setImage(with: URL(string: (self.personnel?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((self.personnel?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    self.imgPlayBtn.isHidden = false
                } else {
                    self.imgPlayBtn.isHidden = true
                }
            } else {
                self.imgPlayBtn.isHidden = true
                self.imgProfile.image = nil
                self.constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
            }
            
            self.collectionView.reloadData()
            
            self.lblTitleValue.text = self.personnel?.personal_name ?? ""
//            self.lblSpotValue.text = self.personnel?.number_of_purchase ?? ""
            
            //TODO: Hide price option
//            self.lblPriceValue.text = ""
            
            
            //TODO: Hide description
            self.lblBusinessValue.text = self.personnel?.personal_description ?? ""
            
            let service_Field:[String] = self.personnel?.personal_field != nil ? (self.personnel?.personal_field?.map{$0.specificFieldName})! as! [String] : [""]

            if(service_Field.count > 0){
                self.lblFeildOfferValue.text = "• \(service_Field.joined(separator: "\n• ") )"
            } else {
                self.lblFeildOfferValue.text = ""
            }
            
            var address = ""
            if(self.personnel?.personal_location_info?.location_name != nil && (self.personnel?.personal_location_info?.location_name?.length)! > 0){
                
                address = self.personnel?.personal_location_info?.location_address ?? ""
                
            } else if(self.personnel?.personal_location_info?.location_name_sec != nil && (self.personnel?.personal_location_info?.location_name_sec?.length)! > 0){
                
                address = self.personnel?.personal_location_info?.location_address_sec ?? ""
            }
            
            self.lblAddressValue.text = address
            
            self.lblPhoneValue.text = ""
            self.lblPhoneValue.isHidden = true
            
            self.lblLinkValue.text = ""
            self.lblLinkValue.isHidden = true
            
            if(!((self.personnel != nil && self.personnel?.doc != nil) && (self.personnel?.doc?.count)! > 0)){
                self.constraintTableViewHeight.constant = 0
            }
            
//            self.viewDesc.isHidden = true
            
            break
        case .Facility:
            
            self.btnPurchaseNow.isHidden = true
//            self.btnBuildPackage.isHidden = true
            self.viewBuildPackage.isHidden = true
            
            if(self.facility?.mediabusiness != nil && (self.facility?.mediabusiness?.count)! > 0){
                self.imgProfile.sd_setImage(with: URL(string: (self.facility?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((self.facility?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    self.imgPlayBtn.isHidden = false
                } else {
                    self.imgPlayBtn.isHidden = true
                }
            } else {
                self.imgPlayBtn.isHidden = true
                self.imgProfile.image = nil
                self.constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
            }
            
            self.collectionView.reloadData()
            
            self.lblTitleValue.text = self.facility?.facility_name ?? ""
            //            self.lblSpotValue.text = self.personnel?.number_of_purchase ?? ""
            
            //TODO: Hide price option
            //            self.lblPriceValue.text = ""
            
            
            //TODO: Hide description
            self.lblBusinessValue.text = self.facility?.facility_description ?? ""
            
            let service_Field:[String] = self.facility?.facility_field != nil ? (self.facility?.facility_field?.map{$0.specificFieldName})! as! [String] : [""]
            
            if(service_Field.count > 0){
                self.lblFeildOfferValue.text = "• \(service_Field.joined(separator: "\n• ") )"
            } else {
                self.lblFeildOfferValue.text = ""
            }
            
            var address = ""
            if(self.facility?.facility_location_info?.location_name != nil && (self.facility?.facility_location_info?.location_name?.length)! > 0){
                
                address = self.facility?.facility_location_info?.location_address ?? ""
                
            } else if(self.facility?.facility_location_info?.location_name_sec != nil && (self.facility?.facility_location_info?.location_name_sec?.length)! > 0){
                
                address = self.facility?.facility_location_info?.location_address_sec ?? ""
            }
            
            self.lblAddressValue.text = address
            
            self.lblPhoneValue.text = ""
            self.lblPhoneValue.isHidden = true
            
            self.lblLinkValue.text = ""
            self.lblLinkValue.isHidden = true
            
            if(!((self.facility != nil && self.facility?.doc != nil) && (self.facility?.doc?.count)! > 0)){
                self.constraintTableViewHeight.constant = 0
            }
            
            //            self.viewDesc.isHidden = true
            
            break
        case .InsideLook:
            self.btnPurchaseNow.isHidden = true
//            self.btnBuildPackage.isHidden = true
            self.viewBuildPackage.isHidden = true
            
            if(self.insideLook?.mediabusiness != nil && (self.insideLook?.mediabusiness?.count)! > 0){
                self.imgProfile.sd_setImage(with: URL(string: (self.insideLook?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((self.insideLook?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    self.imgPlayBtn.isHidden = false
                } else {
                    self.imgPlayBtn.isHidden = true
                }
            } else {
                self.imgPlayBtn.isHidden = true
                self.imgProfile.image = nil
                self.constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
            }
            
            self.collectionView.reloadData()
            
            self.lblTitleValue.text = self.insideLook?.look_name ?? ""
            //            self.lblSpotValue.text = self.personnel?.number_of_purchase ?? ""
            
            //TODO: Hide price option
            //            self.lblPriceValue.text = ""
            
            
            //TODO: Hide description
            self.lblBusinessValue.text = self.insideLook?.look_description ?? ""
            
            let service_Field:[String] = self.insideLook?.look_field != nil ? (self.insideLook?.look_field?.map{$0.specificFieldName})! as! [String] : [""]
            
            if(service_Field.count > 0){
                self.lblFeildOfferValue.text = "• \(service_Field.joined(separator: "\n• ") )"
            } else {
                self.lblFeildOfferValue.text = ""
            }
            
            var address = ""
            if(self.insideLook?.look_location_info?.location_name != nil && (self.insideLook?.look_location_info?.location_name?.length)! > 0){
                
                address = self.insideLook?.look_location_info?.location_address ?? ""
                
            } else if(self.insideLook?.look_location_info?.location_name_sec != nil && (self.insideLook?.look_location_info?.location_name_sec?.length)! > 0){
                
                address = self.insideLook?.look_location_info?.location_address_sec ?? ""
            }
            
            self.lblAddressValue.text = address
            
            self.lblPhoneValue.text = ""
            self.lblPhoneValue.isHidden = true
            
            self.lblLinkValue.text = ""
            self.lblLinkValue.isHidden = true
            
            if(!((self.insideLook != nil && self.insideLook?.doc != nil) && (self.insideLook?.doc?.count)! > 0)){
                self.constraintTableViewHeight.constant = 0
            }
            
            //            self.viewDesc.isHidden = true
            
            break
        case .InstructionalContent:
            self.btnPurchaseNow.isHidden = true
//            self.btnBuildPackage.isHidden = true
            self.viewBuildPackage.isHidden = true
            
            if(self.instructional?.mediabusiness != nil && (self.instructional?.mediabusiness?.count)! > 0){
                self.imgProfile.sd_setImage(with: URL(string: (self.instructional?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((self.instructional?.mediabusiness![0].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    self.imgPlayBtn.isHidden = false
                } else {
                    self.imgPlayBtn.isHidden = true
                }
            } else {
                self.imgPlayBtn.isHidden = true
                self.imgProfile.image = nil
                self.constraintDocumentCollectionHeight.constant = UINavigationController().navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height
            }
            
            self.collectionView.reloadData()
            
            self.lblTitleValue.text = self.instructional?.content_name ?? ""
            //            self.lblSpotValue.text = self.personnel?.number_of_purchase ?? ""
            
            //TODO: Hide price option
            //            self.lblPriceValue.text = ""
            
            
            //TODO: Hide description
            self.lblBusinessValue.text = self.instructional?.content_description ?? ""
            
            let service_Field:[String] = self.instructional?.content_field != nil ? (self.instructional?.content_field?.map{$0.specificFieldName})! as! [String] : [""]
            
            if(service_Field.count > 0){
                self.lblFeildOfferValue.text = "• \(service_Field.joined(separator: "\n• ") )"
            } else {
                self.lblFeildOfferValue.text = ""
            }
            
            var address = ""
            if(self.instructional?.content_location_info?.location_name != nil && (self.instructional?.content_location_info?.location_name?.length)! > 0){
                
                address = self.instructional?.content_location_info?.location_address ?? ""
                
            } else if(self.instructional?.content_location_info?.location_name_sec != nil && (self.instructional?.content_location_info?.location_name_sec?.length)! > 0){
                
                address = self.instructional?.content_location_info?.location_address_sec ?? ""
            }
            
            self.lblAddressValue.text = address
            
            self.lblPhoneValue.text = ""
            self.lblPhoneValue.isHidden = true
            
            self.lblLinkValue.text = ""
            self.lblLinkValue.isHidden = true
            
            if(!((self.instructional != nil && self.instructional?.doc != nil) && (self.instructional?.doc?.count)! > 0)){
                self.constraintTableViewHeight.constant = 0
            }
            
            //            self.viewDesc.isHidden = true
            break
        default:
            break
        }

    }
    
    fileprivate func zoomableView(){
        // Create an array of images.
        var images:[LightboxImage] = []
        
        var mediabusiness:[BusinessAdditionalDocumentInfo]?
        
        switch tabScreenType {
        case .Service:
            if(self.service?.mediabusiness != nil && (self.service?.mediabusiness?.count)! > 0){
                mediabusiness = self.service?.mediabusiness
            }
            break
        case .Personnel:
            if(self.personnel?.mediabusiness != nil && (self.personnel?.mediabusiness?.count)! > 0){
                mediabusiness = self.personnel?.mediabusiness
            }
            break
        case .Facility:
            if(self.facility?.mediabusiness != nil && (self.facility?.mediabusiness?.count)! > 0){
                mediabusiness = self.facility?.mediabusiness
            }
            break
        case .InsideLook:
            if(self.insideLook?.mediabusiness != nil && (self.insideLook?.mediabusiness?.count)! > 0){
                mediabusiness = self.insideLook?.mediabusiness
            }
            break
        case .InstructionalContent:
            if(self.instructional?.mediabusiness != nil && (self.instructional?.mediabusiness?.count)! > 0){
                mediabusiness = self.instructional?.mediabusiness
            }
            break
        default:
            break
        }
        
        if(mediabusiness != nil && (mediabusiness?.count)! > 0){
            for media in (mediabusiness)! {
                //            self.imgProfile.sd_setImage(with: URL(string: (self.biAddRequest?.mediabusiness![0].thumb ?? "")))
                
                // Check video on first pos
                if((media.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    
                    images.append(LightboxImage(imageURL: URL(string: media.thumb ?? "")!, text: "", videoURL: URL(string: media.imageurl ?? "")!))
                    
                } else {
                    images.append(LightboxImage(
                        imageURL: URL(string: media.imageurl ?? "")!
                    ))
                }
            }
        }
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)
        // Present your controller.
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
        
        LightboxConfig.handleVideo = { from, videoURL in
            // Custom video handling
            let videoController = AVPlayerViewController()
            videoController.player = AVPlayer(url: videoURL)
            
            from.present(videoController, animated: true) {
                videoController.player?.play()
            }
        }
    }
    
    fileprivate func checkSubscriptionAndPurhase(){
        
        var isFound = false
        let subscriptionInfoData = PMUserDefault.getSubsriptionInfo()
        
        if(subscriptionInfoData != nil){
            let subscriptionInfo:SubscriptionDetail? = SubscriptionDetail().getModelObjectFromServerResponse(jsonResponse: (subscriptionInfoData ?? "") as AnyObject)
            
            if(subscriptionInfo != nil && subscriptionInfo?.sub_end != nil){
                isFound = true
                if(self.service != nil && self.service?.service_price != nil && self.service?.service_price?.service_price != nil){
                    
                    if(self.packagePaymentViewController == nil){
                        
                        
                        //TODO: Open Credit card payment screen
                        self.packagePaymentViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackagePaymentViewController.nameOfClass) as PackagePaymentViewController
                        
                        if(self.busiInfo != nil){
                            //                        self.service?.busi_id = self.busiInfo?.busi_id ?? ""
                            self.service?.busi_title = self.busiInfo?.busi_title ?? ""
                            self.service?.busitype = self.busiInfo?.busitype ?? ""
                        }
                        self.packagePaymentViewController?.serviceList = [(self.service!)]
                        
                        self.packagePaymentViewController?.packageAmount = Double(self.service?.service_price?.service_price ?? "") ?? 0.00
                        
                        self.packagePaymentViewController?.creditAmount = 0
                        self.packagePaymentViewController?.totalAmount = packagePaymentViewController?.packageAmount ?? 0.00
                        
                        
                        self.packagePaymentViewController?.isSingleServicePurchase = true
                        
                        self.navigationController?.pushViewController(self.packagePaymentViewController!, animated: true)
                    }
                } else {
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: LocalizationKeys.error_price_missing.getLocalized(), buttonTitle: nil, controller: nil)
                }
            }
        }
        
        if(!isFound) {
            self.logoutViewController = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMLogoutView.nameOfClass)
            self.logoutViewController?.delegate = self
            self.logoutViewController?.scrTitle = ""
            self.logoutViewController?.scrDesc = LocalizationKeys.error_no_subscription_service.getLocalized()
            //                self.selectedIndex = sender.tag
            self.logoutViewController?.modalPresentationStyle = .overFullScreen
            self.present(logoutViewController!, animated: true, completion: nil)
        }
    }
    
    fileprivate func openPackageList(){
        Helper.sharedInstance.openTabScreenWithIndex(index: 3)
    }
}

extension UserServiceDetailViewController: LightboxControllerPageDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension UserServiceDetailViewController: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}

extension UserServiceDetailViewController{
    @IBAction func actionPurchase(_ sender: UIButton) {
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            
            break
        case AppUser.LocalBusiness.rawValue:
            
            break
        case AppUser.Enthusiast.rawValue:
            self.checkSubscriptionAndPurhase()
            break
        default:
            break
        }
        
    }
    
    @IBAction func actionBuildPackage(_ sender: UIButton) {
        //TODO: Open Package tab
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            
            break
        case AppUser.LocalBusiness.rawValue:
            
            break
        case AppUser.Enthusiast.rawValue:
            self.openPackageList()
            break
        default:
            break
        }
    }
    
    @IBAction func actionZoomOption(_ sender: AnyObject) {
        zoomableView()
    }
}

extension UserServiceDetailViewController: UITableViewDelegate{
    
    /**
     *  Specify height of row in tableview with UITableViewDelegate method override.
     *
     *  @param key tableView object and heightForRowAt index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    /**
     *  Specify number of section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 5
//        return (self.service != nil && self.service?.doc != nil) ? (self.service?.doc?.count)! : 0
        
        switch tabScreenType {
        case .Service:
            return (self.service != nil && self.service?.doc != nil) ? (self.service?.doc?.count)! : 0
        case .Personnel:
            return (self.personnel != nil && self.personnel?.doc != nil) ? (self.personnel?.doc?.count)! : 0
        case .Facility:
            return (self.facility != nil && self.facility?.doc != nil) ? (self.facility?.doc?.count)! : 0
        case .InsideLook:
            return (self.insideLook != nil && self.insideLook?.doc != nil) ? (self.insideLook?.doc?.count)! : 0
        case .InstructionalContent:
            return (self.instructional != nil && self.instructional?.doc != nil) ? (self.instructional?.doc?.count)! : 0
        default:
            break
        }
        
        return 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessInformationAddAdditionalInformationDocumentCell.nameOfClass) as! BusinessInformationAddAdditionalInformationDocumentCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
                
        switch tabScreenType {
        case .Service:
            let data = service?.doc?[indexPath.row]
            cell.lblName.text = data?.media_title ?? ""
            cell.lblDesc.text = data?.media_desc ?? ""
            if(data?.imageurls != nil && (data?.imageurls?.count)! > 0){
                cell.arrDocumentImagesUrl = data?.imageurls?.map{$0.thumb ?? ""}
            }
            
            cell.delegate = self

            break
        case .Personnel:
            let data = personnel?.doc?[indexPath.row]
            cell.lblName.text = data?.media_title ?? ""
            cell.lblDesc.text = data?.media_desc ?? ""
            if(data?.imageurls != nil && (data?.imageurls?.count)! > 0){
                cell.arrDocumentImagesUrl = data?.imageurls?.map{$0.thumb ?? ""}
            }
            
            cell.delegate = self

            break
        case .Facility:
            let data = facility?.doc?[indexPath.row]
            cell.lblName.text = data?.media_title ?? ""
            cell.lblDesc.text = data?.media_desc ?? ""
            if(data?.imageurls != nil && (data?.imageurls?.count)! > 0){
                cell.arrDocumentImagesUrl = data?.imageurls?.map{$0.thumb ?? ""}
            }
            
            cell.delegate = self

            break
        case .InsideLook:
            let data = insideLook?.doc?[indexPath.row]
            cell.lblName.text = data?.media_title ?? ""
            cell.lblDesc.text = data?.media_desc ?? ""
            if(data?.imageurls != nil && (data?.imageurls?.count)! > 0){
                cell.arrDocumentImagesUrl = data?.imageurls?.map{$0.thumb ?? ""}
            }
            
            cell.delegate = self

            break
        case .InstructionalContent:
            
            let data = instructional?.doc?[indexPath.row]
            cell.lblName.text = data?.media_title ?? ""
            cell.lblDesc.text = data?.media_desc ?? ""
            if(data?.imageurls != nil && (data?.imageurls?.count)! > 0){
                cell.arrDocumentImagesUrl = data?.imageurls?.map{$0.thumb ?? ""}
            }
            
            cell.delegate = self

            break
        default:
            break
        }
        
        cell.refreshScreenInfo()
        
        return cell
    }
}

extension UserServiceDetailViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.zoomableImage(cellIndex: indexPath)
    }
}

extension UserServiceDetailViewController: BusinessInformationAddAdditionalInformationDocumentCellDelegate{
    
    func removeWebsiteInfo(cellIndex:IndexPath?){
        
    }
    
    func zoomableImage(cellIndex:IndexPath?){
        // Create an array of images.
        var images:[LightboxImage] = []
        
        var mediabusiness:BusinessAdditionalDocumentInfo?
        
        switch tabScreenType {
        case .Service:
            if(self.service?.doc != nil && (self.service?.doc?.count)! > 0){
                mediabusiness = self.service?.doc![cellIndex?.row ?? 0]
            }
            break
        case .Personnel:
            if(self.personnel?.doc != nil && (self.personnel?.doc?.count)! > 0){
                mediabusiness = self.personnel?.doc![cellIndex?.row ?? 0]
            }
            break
        case .Facility:
            if(self.facility?.doc != nil && (self.facility?.doc?.count)! > 0){
                mediabusiness = self.facility?.doc![cellIndex?.row ?? 0]
            }
            break
        case .InsideLook:
            if(self.insideLook?.doc != nil && (self.insideLook?.doc?.count)! > 0){
                mediabusiness = self.insideLook?.doc![cellIndex?.row ?? 0]
            }
            break
        case .InstructionalContent:
            if(self.instructional?.doc != nil && (self.instructional?.doc?.count)! > 0){
                mediabusiness = self.instructional?.doc![cellIndex?.row ?? 0]
            }
            break
        default:
            break
        }
        
        if(mediabusiness != nil && mediabusiness?.imageurls != nil && (mediabusiness?.imageurls?.count)! > 0){
            
            for mediaUrl in (mediabusiness?.imageurls)! {
                // Check video on first pos
                if((mediaUrl.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                    
                    images.append(LightboxImage(imageURL: URL(string: mediaUrl.thumb ?? "")!, text: "", videoURL: URL(string: mediaUrl.imageurls ?? "")!))
                    
                } else {
                    
                    //                    data?.imageurls?.map{$0.thumb ?? ""}
                    images.append(LightboxImage(
                        imageURL: URL(string: mediaUrl.imageurls ?? "")!
                    ))
                }
            }
        }
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)
        // Present your controller.
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
        
        LightboxConfig.handleVideo = { from, videoURL in
            // Custom video handling
            let videoController = AVPlayerViewController()
            videoController.player = AVPlayer(url: videoURL)
            
            from.present(videoController, animated: true) {
                videoController.player?.play()
            }
        }
    }
}

//
//extension UserServiceDetailViewController : UICollectionViewDataSource {
//
//    /**
//     *  Specify number of rows on section in collectionView with UICollectionViewDataSource method override.
//     *
//     *  @param key UICollectionView object and section index.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN054]
//     */
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 3
//    }
//
//    /**
//     *  Implement collectionview cell in collectionView with UICollectionViewDataSource method override.
//     *
//     *  @param UICollectionView object and section index.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN054]
//     */
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
////        let productInfo = productList?[indexPath.row]
//
//            // Default product cell
//            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath) as! SerivceImageCell
//        cell.delegate =  self
//        cell.cellIndex = indexPath.row
//
//        let profileImageUrl = PMUserDefault.getProfilePic()
//        if(profileImageUrl != nil){
//            cell.imgService.sd_setImage(with: URL(string: (profileImageUrl ?? "")))
//        }
//
//            return cell
//    }
//}


extension UserServiceDetailViewController : UICollectionViewDataSource {
    
    /**
     *  Specify number of rows on section in collectionView with UICollectionViewDataSource method override.
     *
     *  @param key UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return (self.service != nil && self.service?.mediabusiness != nil && (self.service?.mediabusiness?.count)! > 0) ? (self.service?.mediabusiness?.count)! : 0
        
        switch tabScreenType {
        case .Service:
            return (self.service != nil && self.service?.mediabusiness != nil && (self.service?.mediabusiness?.count)! > 0) ? (self.service?.mediabusiness?.count)! : 0
        case .Personnel:
            return (self.personnel != nil && self.personnel?.mediabusiness != nil && (self.personnel?.mediabusiness?.count)! > 0) ? (self.personnel?.mediabusiness?.count)! : 0
        case .Facility:
            return (self.facility != nil && self.facility?.mediabusiness != nil && (self.facility?.mediabusiness?.count)! > 0) ? (self.facility?.mediabusiness?.count)! : 0
        case .InsideLook:
            return (self.insideLook != nil && self.insideLook?.mediabusiness != nil && (self.insideLook?.mediabusiness?.count)! > 0) ? (self.insideLook?.mediabusiness?.count)! : 0
        case .InstructionalContent:
            return (self.instructional != nil && self.instructional?.mediabusiness != nil && (self.instructional?.mediabusiness?.count)! > 0) ? (self.instructional?.mediabusiness?.count)! : 0
        default:
            break
        }
    }
    
    /**
     *  Implement collectionview cell in collectionView with UICollectionViewDataSource method override.
     *
     *  @param UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        let productInfo = productList?[indexPath.row]
        
        // Default product cell
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: SerivceImageCell.nameOfClass, for: indexPath) as! SerivceImageCell
        cell.delegate =  self
        cell.cellIndex = indexPath.row
        
        switch tabScreenType {
        case .Service:
            let data = self.service?.mediabusiness![indexPath.row]
            
            if(data?.thumb != nil){
                cell.imgService.sd_setImage(with: URL(string: (data?.thumb ?? "")))
            }
            
            if((data?.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                cell.imgPlay.isHidden = false
            } else {
                cell.imgPlay.isHidden = true
            }
            break
        case .Personnel:
            let data = self.personnel?.mediabusiness![indexPath.row]
            
            if(data?.thumb != nil){
                cell.imgService.sd_setImage(with: URL(string: (data?.thumb ?? "")))
            }
            
            if((data?.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                cell.imgPlay.isHidden = false
            } else {
                cell.imgPlay.isHidden = true
            }
            break
        case .Facility:
            let data = self.facility?.mediabusiness![indexPath.row]
            
            if(data?.thumb != nil){
                cell.imgService.sd_setImage(with: URL(string: (data?.thumb ?? "")))
            }
            
            if((data?.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                cell.imgPlay.isHidden = false
            } else {
                cell.imgPlay.isHidden = true
            }
            break
        case .InsideLook:
            let data = self.insideLook?.mediabusiness![indexPath.row]
            
            if(data?.thumb != nil){
                cell.imgService.sd_setImage(with: URL(string: (data?.thumb ?? "")))
            }
            
            if((data?.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                cell.imgPlay.isHidden = false
            } else {
                cell.imgPlay.isHidden = true
            }
            break
        case .InstructionalContent:
            let data = self.instructional?.mediabusiness![indexPath.row]
            
            if(data?.thumb != nil){
                cell.imgService.sd_setImage(with: URL(string: (data?.thumb ?? "")))
            }
            
            if((data?.media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                cell.imgPlay.isHidden = false
            } else {
                cell.imgPlay.isHidden = true
            }
            break
        default:
            break
        }
        
        return cell
    }
}

extension UserServiceDetailViewController : UICollectionViewDelegate {
    
    /**
     *  Implement tap event on collectionview cell in collectionView with UICollectionViewDelegate method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        
    }
}

extension UserServiceDetailViewController : UICollectionViewDelegateFlowLayout {
    
    /**
     *  Implement collectionView with UICollectionViewDelegateFlowLayout method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:100, height: 80)
    }
}

extension UserServiceDetailViewController: ProductCellDelegate{
    func methodShowProductDetails(cellIndex: Int) {
        //TODO: Update image as per selection
        
        if(self.service?.mediabusiness != nil && (self.service?.mediabusiness?.count)! > 0){
            if((self.service?.mediabusiness![cellIndex].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.imgProfile.sd_setImage(with: URL(string: (self.service?.mediabusiness![cellIndex].thumb ?? "")))
        } else if(self.personnel?.mediabusiness != nil && (self.personnel?.mediabusiness?.count)! > 0){
            if((self.personnel?.mediabusiness![cellIndex].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.imgProfile.sd_setImage(with: URL(string: (self.personnel?.mediabusiness![cellIndex].thumb ?? "")))
        } else if(self.facility?.mediabusiness != nil && (self.facility?.mediabusiness?.count)! > 0){
            if((self.facility?.mediabusiness![cellIndex].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.imgProfile.sd_setImage(with: URL(string: (self.facility?.mediabusiness![cellIndex].thumb ?? "")))
        } else if(self.insideLook?.mediabusiness != nil && (self.insideLook?.mediabusiness?.count)! > 0){
            if((self.insideLook?.mediabusiness![cellIndex].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.imgProfile.sd_setImage(with: URL(string: (self.insideLook?.mediabusiness![cellIndex].thumb ?? "")))
        } else if(self.instructional?.mediabusiness != nil && (self.instructional?.mediabusiness?.count)! > 0){
            if((self.instructional?.mediabusiness![cellIndex].media_extension ?? "") == HelperConstant.MediaExtension.Video.MediaExtensionText){
                self.imgPlayBtn.isHidden = false
            } else {
                self.imgPlayBtn.isHidden = true
            }
            
            self.imgProfile.sd_setImage(with: URL(string: (self.instructional?.mediabusiness![cellIndex].thumb ?? "")))
        }
    }
}

extension UserServiceDetailViewController: PMLogoutViewDelegate {
    
    /**
     *  On Cancel, it clear LogoutViewController object from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func cancelLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController = nil
        }
    }
    
    /**
     *  On accept, logout process start and LogoutViewController object is clear from class.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func acceptLogoutViewScr(){
        if(logoutViewController != nil){
            logoutViewController?.view.removeFromSuperview()
            logoutViewController = nil
        }
        
        Helper.sharedInstance.openTabScreenWithIndex(index: 2)
    }
}


class DocumentListCell: UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnView : UIButton!
}


/**
 *  ProductCell class is subclass of UICollectionViewCell
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class SerivceImageCell: UICollectionViewCell {
    
    @IBOutlet weak var  btnForImage: UIButton!
    @IBOutlet weak var  imgService: UIImageView!
    @IBOutlet weak var  imgPlay: UIImageView!
    @IBOutlet weak var  btnDelete: UIButton!
    
    var cellIndex:Int!
    var delegate:ProductCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //btnForServiceImage.imageView?.contentMode =  UIView.ContentMode.scaleAspectFill
        // Initialization code
    }
    
    /**
     *  Show Product detail button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodShowProductDetails(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodShowProductDetails(cellIndex: self.cellIndex)
        }
    }
}


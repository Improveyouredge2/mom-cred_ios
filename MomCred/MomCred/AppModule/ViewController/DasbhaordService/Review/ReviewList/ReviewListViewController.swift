//
//  ReviewListViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * ReviewListViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ReviewListViewController : LMBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblReviewCount : UILabel!
    @IBOutlet weak var imgProfile : ImageLayerSetup!
    
    fileprivate var addReviewViewController:AddReviewViewController?
    fileprivate var packageProfileSelectionViewController:PackageProfileSelectionViewController?
    
    /**
     *  Called after the view has been loaded..
     *
     *  For view controllers created in code, this is after -loadView.
     *
     *  For view controllers unarchived from a nib, this is after the view is set
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //presenter.connectView(view: self)
        let userName = PMUserDefault.getUserFirstName()
        if(userName != nil){
            let profileImageUrl = PMUserDefault.getProfilePic()
            self.lblTitle.text = userName
            self.lblSubTitle.text = PMUserDefault.getUserName()
            
            if(profileImageUrl != nil){
                //                self.imgProfile?.sd_setImage(with: URL(string: (profileImageUrl!)), for: .normal, completed: nil)
                self.imgProfile.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
            }
        }
    }
    
    // Called when the view is about to made visible. Default does nothing
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addReviewViewController = nil
    }
    
}

extension ReviewListViewController{
    @IBAction func actionAddReview(_ sender: UIButton) {
        if(self.addReviewViewController == nil){
            self.addReviewViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ProductDetailStoryboard, viewControllerName: AddReviewViewController.nameOfClass) as AddReviewViewController
            self.navigationController?.pushViewController(self.addReviewViewController!, animated: true)
        }
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate

extension ReviewListViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 15
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ReviewListCell.nameOfClass) as! ReviewListCell
        
        let profileImageUrl = PMUserDefault.getProfilePic()
        cell.imgProfilePic.sd_setImage(with: URL(string: (profileImageUrl ?? "")), completed: nil)
        
        cell.lblDesciption.text = "sfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd fl"
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        return UITableView.automaticDimension
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.packageProfileSelectionViewController == nil){
            self.packageProfileSelectionViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.PackageStoryboard, viewControllerName: PackageProfileSelectionViewController.nameOfClass) as PackageProfileSelectionViewController
            self.navigationController?.pushViewController(self.packageProfileSelectionViewController!, animated: true)
        }
    }
}

//

class ReviewListCell: UITableViewCell {
    @IBOutlet weak var cardView : ViewLayerSetup!
    @IBOutlet weak var imgProfilePic : ImageLayerSetup!
    @IBOutlet weak var lblDesciption : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDate : UILabel!
}

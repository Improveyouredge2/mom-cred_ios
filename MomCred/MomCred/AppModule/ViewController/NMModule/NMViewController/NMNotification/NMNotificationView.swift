//
//  NMNotificationView.swift
//  NotificationLib
//
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftEventBus

class NMNotificationView: LMBaseViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var tblNotification  :   UITableView!
    @IBOutlet weak var lblNoData  :   UILabel!
    
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    
    @IBOutlet weak var btnClearAll: UIButton!
    @IBOutlet weak var btnRefresh: UIButton!
    
    var defaultData = false
    
    //MARK:- var
    var arrNotificationList  :   [NotificationResponse.NotificationResponseData] = []
    var index = 1
    
    // Class object of presenter class
    fileprivate let presenter = NMNotificationPresenter()
    
    
    //MARK:- Life Cycle Methods
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshView.delegate = self
        tblNotification.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        tblNotification.addPullLoadableView(loadMoreView, type: .loadMore)
        
        tblNotification.contentInset.top = 5
        tblNotification.contentInset.bottom = 5
        
        self.btnClearAll.isHidden = true
        self.btnRefresh.isHidden = true
        
        presenter.connectView(view: self)
        
//        Helper.sharedInstance.showProgressHudViewWithTitle(title: nil)
        
//        if(NMHelper.sharedInstance.isNetworkConnected){
//            self.fetchNotificationList()
//        }
    }
    
    // Called after the view was dismissed, covered or otherwise hidden. Default does nothing
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(true)
        SwiftEventBus.unregister(self)
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupLanguage(notification: nil)
        
        SwiftEventBus.onMainThread(self, name: HelperConstant.LanguageChanged, handler: self.setupLanguage)
        SwiftEventBus.onMainThread(self, name: HelperConstant.NetworkConnect, handler: self.onNetworkConnect)
        
        self.fetchNotificationList()
    }
    
    // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SwiftEventBus.unregister(self)
    }
}

//MARK:- Public method implementation
//MARK:-
extension NMNotificationView{
    
    /**
     *  Method udpate server response on screen controls.
     *
     *  @param key server response object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func updateScreenInfo(response:NotificationResponse?){
        
        // check server repsonse
        if(response != nil){
            if(self.defaultData){
                self.defaultData = false
                self.arrNotificationList.removeAll()
            }
            
            // update user notification response on table list
            if let arrReaponseData = response?.notificationResponseData, arrReaponseData.count > 0 {
                if self.index == 1 {
                    self.arrNotificationList.removeAll()
                }
                self.arrNotificationList.append(contentsOf: arrReaponseData)//    =   arrReaponseData
                
                if self.arrNotificationList.count == 0 {
                    self.tblNotification.isHidden   =   true
                }else{
                    self.tblNotification.isHidden   =   false
                }
            }  else{
                Helper.notificationCount    =   ""
            }
            self.lblNoData.isHidden = true
            //                    self.tblNotification.isHidden = false
            self.tblNotification.reloadData()
            print(response ?? "")
            self.isDownloadWorking = false
//            self.tblNotification.windless.end()
            Spinner.hide()
            self.btnClearAll.isHidden = false
            self.btnRefresh.isHidden = true
        } else { // No notification in user account
            self.lblNoData.text = response?.message ?? LocalizationKeys.no_notificaition_to_clear.getLocalized()
            self.lblNoData.isHidden = false
            self.tblNotification.isHidden = true
            Helper.notificationCount    = ""
            self.btnClearAll.isHidden = true
            self.btnRefresh.isHidden = false
            self.defaultData = true
//            self.tblNotification.windless.end()
            Spinner.hide()
        }
    }
}

//MAKR:- Fileprivate method implementation
//MARK:-
extension NMNotificationView{
    
    /**
     *  Method user notification list server request with initial index.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @objc fileprivate func fetchNotificationList(){
        
        //TODO: call when app is connected with api
//        presenter.fetchData(pageIndex: 1)
    }
    
    /**
     *  Observer on application connect on server.
     *
     *  @param key Notification's class object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func onNetworkConnect(notification:Notification?) -> Void{
        self.fetchNotificationList()
    }
    
    /**
     *  Set Up Localized Strings
     *
     *  @param key Notification's class object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setupLanguage(notification:Notification?) -> Void{
        self.lbl_NavigationTitle.text = LocalizationKeys.notifications.getLocalized()
        self.btnClearAll.setTitle(LocalizationKeys.clear_all.getLocalized(), for: UIControl.State.normal)
    }
}

//MARK:- KRPullLoadViewDelegate
//MARK:-
extension NMNotificationView : KRPullLoadViewDelegate {
    
    /**
     *  Method implement pull to refresh and pagination on tableview
     *
     *  @param key pullLoadView, stat and pull type (pull or paging).
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            
            switch state
            {
            case let .loading(completionHandler):
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                    completionHandler()
                    if(!self.isDownloadWorking){
                        self.index += 1
                        self.presenter.fetchData(pageIndex: self.index)
                    }
                }
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                pullLoadView.messageLabel.text = "Pull more."
            } else {
                pullLoadView.messageLabel.text = "Release to refresh."
            }
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                completionHandler()
                if(!self.isDownloadWorking){
                    self.index = 1
                    self.presenter.fetchData(pageIndex: self.index)
                }
            }
        }
    }
}

//MARK:- Actions method implementation
extension NMNotificationView {
    
    /**
     *  Refresh button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnRefreshAction(_ sender : UIButton){
//        self.fetchNotificationList()
    }
    
    /**
     *  Clear button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnClearAllAction(_ sender : UIButton){
        if self.arrNotificationList.count > 0 {
            self.presenter.clearData()
        } else {
            self.showBannerAlertWith(message: LocalizationKeys.no_notificaition_to_clear.getLocalized(), alert: PMBannerAlertType.error)
        }
    }
    
    /**
     *  Navigation Back button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func actionNavigationBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
        self.defaultData = true
        self.arrNotificationList.removeAll()
    }
}

//MARK:- Tableview data source and delegates
//MARK:-
extension NMNotificationView  : UITableViewDelegate , UITableViewDataSource  {
    
    // Number of rows in section. Implementer should specify the number of row display in each section. Default section is 0.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return arrNotificationList.count;
        return 10
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell    =   tableView.dequeueReusableCell(withIdentifier: NMNotificationCell.nameOfClass, for: indexPath) as! NMNotificationCell
        
//        let notification    =   arrNotificationList[indexPath.row]
//
//        if(notification != nil && notification.id != nil){
//
            if(indexPath.row % 2 == 0){
                cell.lblIndicator.isHidden = false
                cell.lblDetail.textColor = UIColor.white
                cell.lblDetail.font = FontsConfig.FontHelper.defaultBoldFontWithSize(16)
            } else {
                cell.lblIndicator.isHidden = true
                cell.lblDetail.textColor = UIColor.lightGray
                cell.lblDetail.font = FontsConfig.FontHelper.defaultRegularFontWithSize(14)
            }
            
            cell.lblDetail.text = "sfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd flsfj lkfjsdlkfs dfslfslf sdlf sldkf sldkf sldkf sldf sldkf lskd fl"
            cell.lblDate.text   = ""
//        }
        return cell
    }
    
    // Selects and deselects rows. These methods will not call the delegate methods (-tableView:willSelectRowAtIndexPath: or tableView:didSelectRowAtIndexPath:), nor will it send out a notification.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let arrNotification = arrNotificationList[indexPath.row]
//        arrNotificationList[indexPath.row] = arrNotification
//        tableView.reloadData()
        
    }
    
    // Use the estimatedHeight methods to quickly calcuate guessed values which will allow for fast load times of the table.
    // If these methods are implemented, the above -tableView:heightForXXX calls will be deferred until views are ready to be displayed, so more expensive logic can be placed there.
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 56
//    }
    
    // Use the estimatedHeight methods to quickly calcuate guessed values which will allow for fast load times of the table.
    // If these methods are implemented, the above -tableView:heightForXXX calls will be deferred until views are ready to be displayed, so more expensive logic can be placed there.
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK:-
//MARK:- @end

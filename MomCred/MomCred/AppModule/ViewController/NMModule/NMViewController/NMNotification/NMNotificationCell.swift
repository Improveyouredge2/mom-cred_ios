//
//  NMNotificationCell.swift
//  NotificationLib
//
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit

class NMNotificationCell: UITableViewCell {
    
    @IBOutlet weak var lblIndicator         :   UILabel!
    @IBOutlet weak var lblDetail        :   UILabel!
    @IBOutlet weak var lblDate          :   UILabel!
    
    @IBOutlet weak var containerView : ViewLayerSetup!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // animate between regular and selected state
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

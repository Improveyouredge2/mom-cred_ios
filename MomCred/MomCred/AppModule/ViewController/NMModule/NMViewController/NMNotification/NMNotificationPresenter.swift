//
//  PMAccountPresenter.swift
//  ProfileLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class NMNotificationPresenter {
    
    var view:NMNotificationView! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: NMNotificationView) {
        self.view = view
    }
    
    /**
     *  Get user profile information from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func fetchData(pageIndex:Int){
        
        NMNotificationService.getData(pageIndex: pageIndex, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    // update information on screen
                    self.view.updateScreenInfo(response: response!)
//                    self.view.hideLoading(
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    self.view.updateScreenInfo(response: nil)
                }
            }
        })
    }
    
    /**
     *  Clear all notificaiton information from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func clearData(){
        
        NMNotificationService.updateData(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    // update information on screen
                    self.view.updateScreenInfo(response: nil)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    self.view.updateScreenInfo(response: nil)
                }
            }
        })
    }
}

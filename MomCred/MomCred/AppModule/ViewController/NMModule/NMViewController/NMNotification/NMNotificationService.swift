//
//  NMNotificationService.swift
//  NotificationLib
//
//  Created by Apple_iOS on 23/01/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class NMNotificationService{
    /**
     *  Method connect with api call to server and provide information to screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getData(pageIndex:Int, callback:@escaping (_ status:Bool, _ response: NotificationResponse?, _ message: String?) -> Void) {
        
        let userID = NMUserDefault.getUserId()
        
        let notificationRequest = NotificationRequest(page: pageIndex)
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = notificationRequest.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_NOTIFICATION_LIST, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:NotificationResponse? = NotificationResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
            
        }
        
    }
    
    /**
     *  Method upload screen information on server with profile data and user profiel image.
     *
     *  @param key profile information and selected image for user profile. Callback method to update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func updateData(callback:@escaping (_ status:Bool, _ response: NotificationResponse?, _ message: String?) -> Void) {
        
        // MultiForm
        let getRequest = APIManager().sendGetRequest(urlString: APIKeys.API_NOTIFICATION_CLEAR, header: APIHeaders().getDefaultHeaders())
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: getRequest, isDisplayLoader: true) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:NotificationResponse? = NotificationResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

//
//  NMHelperConstant.swift
//  NotificationLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit

/**
 *  Module Helper constant class for specifying constant used in module
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class NMHelperConstant: NSObject {
    internal static let userDefaultUserIdKey            = "userid"
    internal static let userDefaultAppTokenKey          = "user_token"
    internal static let userDefaultUserAddressKey       = "user_address"
    internal static let userDefaultAPNSTokenKey         = "APNS_TOKEN"
    internal static let userDefaultFCMTokenKey          = "FCM_TOKEN"
    internal static let kFCMMessageIDs                  = "FCMMessageIDs"
    internal static let LOCAL_NOTIFICATION_IDENTIFIER = "localNotification"
}

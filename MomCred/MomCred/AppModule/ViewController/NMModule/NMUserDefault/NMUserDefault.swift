//
//  NMUserDefault.swift
//  MomCred
//
//  Copyright © 2019 Consagous. All rights reserved.

import Foundation
import UIKit

/**
 *  NMUserDefault class is save, fetch and delete data in device preference (UserDefaults)
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class NMUserDefault{
    
    // common object of UserDefaults, used in class method.
    fileprivate static let defaults = UserDefaults.standard
    
    //////////////// Remove all info //////
    class func clearAllData(){
        //defaults.removeObject(forKey: NMHelperConstant.userDefaultUserIdKey)
        defaults.removeObject(forKey: NMHelperConstant.userDefaultAppTokenKey)
        
        defaults.synchronize()
    }
    
    ///////////////// GET & SAVE UserId /////////////////
    class func saveUserId(userId: String){
        defaults.set(userId, forKey: NMHelperConstant.userDefaultUserIdKey)
        defaults.synchronize()
    }
    
    class func getUserId()->String?{
        let userId:String?
        userId = defaults.object(forKey: NMHelperConstant.userDefaultUserIdKey) as? String
        return userId ?? nil
    }
    
    //////////////// GET & SAVE APP TOKEN /////////////////
    class func saveAppToken(appToken: String){
        defaults.set(appToken, forKey: NMHelperConstant.userDefaultAppTokenKey)
        defaults.synchronize()
    }
    
    class func getAppToken()->String?{
        let appToken:String?
        appToken = defaults.object(forKey: NMHelperConstant.userDefaultAppTokenKey) as? String
        return appToken ?? nil
    }
    
    //////////////// GET & SAVE FCM TOKEN //////
    class func saveFCMToken(appToken: String){
        defaults.set(appToken, forKey: NMHelperConstant.userDefaultFCMTokenKey)
        defaults.synchronize()
    }
    
    class func getDeviceID() -> String {
        return UIDevice.current.identifierForVendor!.uuidString
    }
    
    class func getFCMToken()->String?{
        let appToken:String?
        appToken = defaults.object(forKey: NMHelperConstant.userDefaultFCMTokenKey) as? String
        return appToken ?? nil
    }
    
    class func removeFCMToken(){
        defaults.removeObject(forKey: NMHelperConstant.userDefaultFCMTokenKey)
        defaults.synchronize()
    }
    
}

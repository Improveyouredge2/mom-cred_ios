//
//  PMNotification.swift
//  NotificationLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  NotificationRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class NotificationRequest : APIResponse {
    
    private var userid      : String?
    private var _token       : String?
    private var language    : String?
    private var page        : String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, app token, new password and old password.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(userid : String = NMUserDefault.getUserId()!  , token : String = NMUserDefault.getAppToken()!, language : String = HelperConstant.DefaultLang , page : Int? ) {
        
        super.init()
        
        let pg : Int = page!
        self.page = String(pg)
        self.language = language
        self._token = token
        self.userid = userid
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map) {
        language        <- map["language"]
        userid          <- map["userid"]
        _token           <- map["token"]
        page            <- map["page"]
    }
}

class NotificationResponse : APIResponse {
    
    var notificationResponseData:[NotificationResponseData]?
    var per_page  :   Int?
    var total_records : Int?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->NotificationResponse?{
        var login:NotificationResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<NotificationResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    login = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<NotificationResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        login = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return login ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        notificationResponseData    <- map["data"]
        per_page                    <- map["per_page"]
        total_records               <- map["total_records"]
    }
    
    // Data in login response
    class NotificationResponseData: Mappable{
        
        var id                  :String?
        var title               :String?
        var description             :String?
        
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        required init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->NotificationResponseData?{
            var notificationResponseData:NotificationResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<NotificationResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        notificationResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<NotificationResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            notificationResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return notificationResponseData ?? nil
        }
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        func mapping(map: Map){
            id                  <- map["notfication_id"]
            title               <- map["notification_title"]
            description         <- map["notification_desc"]
           
            
        }
    }
}

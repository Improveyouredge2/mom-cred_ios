//
//  NMAPNS.swift
//  NotificationLib
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit
import UserNotificationsUI
import UserNotifications

class NMAPNSManager: NSObject {
//    static let sharedInstance = NMAPNSManager()
    
//    var kDeviceToken = "00000000000000000000000000000000000000"
    
    override init() {
        super.init()
    }
    
    /**
     *  Overloaded constructor for register APNS notification configuration.
     *
     *  @param key UIApplicaiton object
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(_ application: UIApplication){
        
        super.init()
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        // [END register_for_notifications]
        
        application.applicationIconBadgeNumber = 0
    }
}

//MARK:- Push notification delegate iOS10
//MARK:-
@available(iOS 10, *)
extension NMAPNSManager : UNUserNotificationCenterDelegate {
    
    /**
     * Call on foreground
     * Receive displayed notifications for iOS 10 devices.
     *
     * @param key center object for APNs, notification, completionHandler is a callback method.
     *
     * @return empty.
     *
     * @Developed By: Team Consagous [CNSGSIN054]
     */
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        NMFCMMessageProcessor.sharedInstance.processRemoteMessage(userInfo, openChatScreenStatus: false, isAppInBackground: false)
        completionHandler([])
    }
    
    /**
     * Call on foreground
     * Receive displayed notifications for iOS 10 devices.
     *
     * @param key center object for APNs, notification, completionHandler is a callback method.
     *
     * @return empty.
     *
     * @Developed By: Team Consagous [CNSGSIN054]
     */
    @available(iOS 10, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        print("DidReceive response WithCompletionHandler : \(userInfo)")
        
        print(response.notification.request.content.userInfo)
        if(response.notification.request.content.categoryIdentifier.caseInsensitiveCompare(HelperConstant.LOCAL_NOTIFICATION_IDENTIFIER) == ComparisonResult.orderedSame){
            
            // functionality to show alert
            Helper.sharedInstance.showAlertViewControllerWith(title: "Notification in userNotificationCenter", message: response.notification.request.content.userInfo.description, buttonTitle: nil, controller: nil)
        } else{
            NMFCMMessageProcessor.sharedInstance.processRemoteMessage(response.notification.request.content.userInfo, openChatScreenStatus: true)
        }
    }
}

//MARK:- RemoteNotification
//MARK:-
extension AppDelegate {
    /**
     *  Method to receive remote notifications.
     *
     *  @param key application, userInfo and completionHandler callback method
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let dictNotification = userInfo as! [String: AnyObject]
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Notification Recievied from - didReceiveRemoteNotification ",dictNotification)
        self.getRemoteNotifications(dictNotification)
        
        print("didReceiveRemoteNotification userInfo fetchCompletionHandler : \(userInfo)")
        
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    /**
     *  Method to fail on receive remote notifications.
     *
     *  @param key application, didFailToRegisterForRemoteNotificationsWithError
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    /**
     *  Method to fetch device token
     *
     *  @param key application, didRegisterForRemoteNotificationsWithDeviceToken
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        let token = String(format: "%@", deviceToken as CVarArg)
        print("*** deviceToken: \(token)")
//        NMAPNSManager.sharedInstance.kDeviceToken = token
        
        // manage FCM account staging or production
        HelperConstant.appDelegate.fcmManager.configFCMAccount(deviceToken: deviceToken)
        
        HelperConstant.appDelegate.fcmManager.onTokenRefresh()
    }
    
    /**
     *  Method to fetch  remote notificatio in dictionary
     *
     *  @param key dictApns is [key, value] object
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getRemoteNotifications(_ dictApns: [String: AnyObject]) {
        print(dictApns)
    }
}

//
//  NMFCMManager.swift
//  NotificationLib
//
//  Created by Apple_iOS on 25/01/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import UIKit
import UserNotificationsUI
import UserNotifications
import Firebase
import FirebaseMessaging
import FirebaseInstanceID

class NMFCMManager: NSObject {
    
    /**
     *  Overloaded constructor for register FCM notification configuration.
     *
     *  @param key UIApplicaiton object
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(_ application: UIApplication){
        
        super.init()
        
        FirebaseApp.configure()
        
        // Add observer to listen for the token refresh notification.
        // FIRMessagingRegistrationTokenRefreshedNotification
        NotificationCenter.default.addObserver(self, selector: #selector(self.onTokenRefresh), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            // [START set_messaging_delegate]
            Messaging.messaging().delegate = self
            // [END set_messaging_delegate]
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        // [END register_for_notifications]
        
        application.applicationIconBadgeNumber = 0
    }
}

//MARK:- public method
//MARK:-
extension NMFCMManager {
    
    /**
     *  Method is to attach APNs token to FCM account.
     *
     *  @param key APNs device token
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func configFCMAccount(deviceToken: Data){
        
        // attach device APNs token with FCM
        Messaging.messaging().apnsToken = deviceToken
        Messaging.messaging().isAutoInitEnabled = true
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                //TODO: Check once token
            }
        }
    }
    
    /**
     *  Method is to get FCM token and save in NSUserDefault
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @objc func onTokenRefresh() {
        // TODO: Send token to server
        if let fcmRefreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(fcmRefreshedToken)")
            NMUserDefault.saveFCMToken(appToken: fcmRefreshedToken)
        }
    }
}

//MARK:- FIRMessagingDelegate
//MARK:-
extension NMFCMManager : MessagingDelegate {
    /**
     * Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
     * To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
     *
     * @param key Firbase's MessagingRemoteMessage object.
     *
     * @return empty.
     *
     * @Developed By: Team Consagous [CNSGSIN054]
     */
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}


//MARK:- Push notification delegate iOS10
//MARK:-
@available(iOS 10, *)
extension NMFCMManager : UNUserNotificationCenterDelegate {
    
    /**
     * Call on foreground
     * Receive displayed notifications for iOS 10 devices.
     *
     * @param key center object for APNs, notification, completionHandler is a callback method.
     *
     * @return empty.
     *
     * @Developed By: Team Consagous [CNSGSIN054]
     */
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        NMFCMMessageProcessor.sharedInstance.processRemoteMessage(userInfo, openChatScreenStatus: false, isAppInBackground: false)
        completionHandler([])
    }
    
    /**
     * Call on foreground
     * Receive displayed notifications for iOS 10 devices.
     *
     * @param key center object for APNs, notification, completionHandler is a callback method.
     *
     * @return empty.
     *
     * @Developed By: Team Consagous [CNSGSIN054]
     */
    @available(iOS 10, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        print("DidReceive response WithCompletionHandler : \(userInfo)")
        
        print(response.notification.request.content.userInfo)
        if(response.notification.request.content.categoryIdentifier.caseInsensitiveCompare(HelperConstant.LOCAL_NOTIFICATION_IDENTIFIER) == ComparisonResult.orderedSame){
            
            // functionality to show alert
            Helper.sharedInstance.showAlertViewControllerWith(title: "Notification in userNotificationCenter", message: response.notification.request.content.userInfo.description, buttonTitle: nil, controller: nil)
        } else{
            NMFCMMessageProcessor.sharedInstance.processRemoteMessage(response.notification.request.content.userInfo, openChatScreenStatus: true)
        }
    }
}

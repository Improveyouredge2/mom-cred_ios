//
//  MessageProcessor.swift
//
//

import Foundation

/**
* FCM Message Processor to handle payload by its type.
*/
class NMFCMMessageProcessor {
    static let sharedInstance = NMFCMMessageProcessor()
    
    /**
     *  Process message payload based on it's type and content.
     *
     *  @param key payload Message payload
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func processRemoteMessage(_ payload:[AnyHashable : Any]?, openChatScreenStatus:Bool = true, isAppInBackground:Bool = true) {
        
        print("processRemoteMessage getCollapseKey:")
        
        print(payload ?? "")
        
        if(payload != nil && (payload?.count)! > 0) {
            //payload["a"]
            //let action:String? = payload?["a"] as? String
            if(payload?["aps"] is [AnyHashable : Any] && (payload?["aps"] as! [AnyHashable : Any])["alert"] is [AnyHashable : Any]){
                let payloadMsg:[AnyHashable : Any]? = (payload?["aps"] as! [AnyHashable : Any])["alert"] as? [AnyHashable : Any]
                
                if payloadMsg != nil {
                    
                    //MARK: Open Notification list screen
                    if(isAppInBackground){
                        Spinner.hide()
                        if let data = payload?["data"] {
                            print("Notification Data: \(data)")
                        }
                    } else { // Manage message in foreground
                        
                        
                    }
                }
            } else {
                // Print full message.
                if let aps = payload!["aps"] as? [AnyHashable : Any], let message = aps["alert"] as? String {
                    print("Message : \(message)")
                }
            }
        }
    }
}


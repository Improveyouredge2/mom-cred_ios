//
//  FCMService.swift
//
//

import Foundation
import Firebase
import FirebaseMessaging

/**
* Service handling incoming FCM messages.
*/
class NMFCMService: Messaging {
    
    /**
     *  Messaging override method to get message.
     *
     *  @param key payload Message payload
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func appDidReceiveMessage(_ message: [AnyHashable : Any]) -> MessagingMessageInfo{
        // FCM message details.
        print("onMessageReceived \(message)")
        let firMessagingInfo = MessagingMessageInfo()
        return firMessagingInfo
    }
}

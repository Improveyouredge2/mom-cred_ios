//
//  HomeModel.swift
//  ShoppingCartLib
//
//  Created by Apple_iOS on 05/02/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper
import GoogleMaps


/**
 *  HomeResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class PaginationRequest : APIResponse {
    
    var page_id:NSNumber?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        super.mapping(map: map)
        
        page_id   <- map["page_id"]
    }
}

/**
 *  HomeResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class KeywordSearchRequest : APIResponse {
    
    var keyword:String?
    var page_id:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        super.mapping(map: map)
        
        keyword     <- map["keyword"]
        page_id     <- map["page_id"]
    }
}

/**
 *  HomeResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BusiServicesRequest : APIResponse {
    
    var busi_user:String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        super.mapping(map: map)
        
        busi_user   <- map["busi_user"]
    }
}

/**
 *  HomeResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class HomeResponse : APIResponse {
    
    var homeResponseData:HomeResponseData?
    var isProvider: Int?

    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> HomeResponse?{
        var homeResponse:HomeResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<HomeResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    homeResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<HomeResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        homeResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return homeResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        super.mapping(map: map)
        
        homeResponseData   <- map["data"]
        isProvider <- map["isProvider"]
    }
}

/**
 *  HomeResponseData is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class HomeResponseData : APIResponse {
    
//    var id              :String?
//    var name            :String?
//    var desc            :String?
//    var type            :String?
//    var image_url       :String?
//    var product_list     :[ProductDetailResponse.ProductDetailData]?
    
    var imagebanner:[BannerDetail]?
    var videobanner:[BannerDetail]?
    var service:[ServiceAddRequest]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> HomeResponseData?{
        var homeResponseData:HomeResponseData?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<HomeResponseData>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    homeResponseData = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<HomeResponseData>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        homeResponseData = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return homeResponseData ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        imagebanner     <- map["imagebanner"]
        videobanner     <- map["videobanner"]
        service         <- map["service"]
    }
}

///**
// *  BannerDetail is serialize object user in API request/response. It specify keys used in API reponse.
// *
// *  @Developed By: Team Consagous [CNSGSIN054]
// */
class BannerDetail: Mappable{
    
    var id              :String?
    var title           :String?
    var banner          :String?
    var image_url       :String?
    var description     :String?
    var thumb          :String?

    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){

    }

    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject)->BannerDetail?{
        var bannerDetail:BannerDetail?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<BannerDetail>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    bannerDetail = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!

            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<BannerDetail>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        bannerDetail = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return bannerDetail ?? nil
    }

    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map){

    }

    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map){
        id              <- map["id"]
        title           <- map["title"]
        image_url       <- map["url"]
        banner          <- map["banner"]
        description     <- map["description"]
        thumb           <- map["thumb"]
    }
}

/**
 *  HomeResponseData is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class HomeScrResponseData : APIResponse {
    
    var name            :String?
    var type            :String?
    var desc            :String?
    var imagebanner     :[BannerDetail]?
    var videobanner     :[BannerDetail]?
    var service         :[ServiceAddRequest]?
    
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> HomeScrResponseData?{
        var homeScrResponseData:HomeScrResponseData?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<HomeScrResponseData>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    homeScrResponseData = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<HomeScrResponseData>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        homeScrResponseData = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return homeScrResponseData ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        name            <- map["name"]
        desc            <- map["desc"]
        type            <- map["type"]
        imagebanner     <- map["imagebanner"]
        videobanner     <- map["videobanner"]
        service         <- map["service"]
    }
}

/**
 *  HomeResponseData is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class DashboardAllServiceResponseData : APIResponse {

    var service:[ServiceAddRequest]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> DashboardAllServiceResponseData?{
        var dashboardAllServiceResponseData:DashboardAllServiceResponseData?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<DashboardAllServiceResponseData>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    dashboardAllServiceResponseData = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<DashboardAllServiceResponseData>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        dashboardAllServiceResponseData = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return dashboardAllServiceResponseData ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        service         <- map["data"]
    }
}


/**
 *  ServiceAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
//class HomeServiceResponse: ServiceAddRequest {
//
//    var busitype:String?
//    var provider_image:String?
//    var rating:NSNumber?
//    var busi_id:String?
//    var busi_title:String?
//
//    // For static use
//    var donationPer:Float?
//    var donationAmt:Float?
//
//    /**
//     *  Class constructor with custom parameter as per requirement.
//     *
//     *  @param key user id, first name, last name, email, address and token.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN100]
//     */
//    required init() {
//        super.init()
//    }
//    /**
//     *  Method is required constructor of Mappable class
//     *
//     *  @param key mappable object.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN100]
//     */
//    required init?(map: Map) {
//        super.init(map: map)
//    }
//
//    /**
//     *  Convert Json response to class object.
//     *
//     *  @param key server api response is either key-value pair or string.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN100]
//     */
//
//    override func getModelObjectFromServerResponse(jsonResponse: AnyObject)->HomeServiceResponse?{
//
//        super.getModelObjectFromServerResponse(jsonResponse: jsonResponse)
//
//        var homeServiceResponse:HomeServiceResponse?
//        if jsonResponse is NSDictionary{
//            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
//            if tempDic != nil{
//                let mapper = Mapper<HomeServiceResponse>()
//                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
//                    homeServiceResponse = testObject
//                }
//            }
//        }else if jsonResponse is String{
//            let tempJsonString = jsonResponse as? String
//            let data = tempJsonString?.data(using: String.Encoding.utf8)!
//
//            do {
//                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
//                    let mapper = Mapper<HomeServiceResponse>()
//                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
//                        homeServiceResponse = testObject
//                    }
//                }
//            } catch let error as NSError {
//                print(error.localizedDescription)
//            }
//        }
//        return homeServiceResponse ?? nil
//    }
//
//    /**
//     *  Sync server api response value to class objects.
//     *
//     *  @param key mappable object.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN100]
//     */
//    override func mapping(map: Map) {
//
//        super.mapping(map: map)
//
//        busitype        <- map["busitype"]
//        provider_image  <- map["provider_image"]
//        rating          <- map["rating"]
//        busi_id         <- map["busi_id"]
//        busi_title      <- map["busi_title"]
//
//    }
//}

/**
 *  DashboardAllServiceProviderResponseData is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class DashboardAllServiceProviderResponseData: APIResponse{
    
    var busi:[BIAddRequest]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> DashboardAllServiceProviderResponseData?{
        var dashboardAllServiceProviderResponseData:DashboardAllServiceProviderResponseData?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<DashboardAllServiceProviderResponseData>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    dashboardAllServiceProviderResponseData = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<DashboardAllServiceProviderResponseData>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        dashboardAllServiceProviderResponseData = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return dashboardAllServiceProviderResponseData ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        busi         <- map["data"]
    }
}

/**
 *  ServiceAddRequest Request is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN100]
 */
//class HomeBusiResponse: BIAddRequest {
//    
//    var busitype:String?
//    var provider_image:String?
//    var rating:NSNumber?
//    var totalservice:String?
//    var busi_user:String?
//    
//    var marker  : GMSMarker?
//    
//    /**
//     *  Class constructor with custom parameter as per requirement.
//     *
//     *  @param key user id, first name, last name, email, address and token.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN100]
//     */
//    required init() {
//        super.init()
//    }
//    /**
//     *  Method is required constructor of Mappable class
//     *
//     *  @param key mappable object.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN100]
//     */
//    required init?(map: Map) {
//        super.init(map: map)
//    }
//    
//    /**
//     *  Convert Json response to class object.
//     *
//     *  @param key server api response is either key-value pair or string.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN100]
//     */
//    
//    override func getModelObjectFromServerResponse(jsonResponse: AnyObject)->HomeBusiResponse?{
//        
//        super.getModelObjectFromServerResponse(jsonResponse: jsonResponse)
//        
//        var homeBusiResponse:HomeBusiResponse?
//        if jsonResponse is NSDictionary{
//            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
//            if tempDic != nil{
//                let mapper = Mapper<HomeBusiResponse>()
//                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
//                    homeBusiResponse = testObject
//                }
//            }
//        }else if jsonResponse is String{
//            let tempJsonString = jsonResponse as? String
//            let data = tempJsonString?.data(using: String.Encoding.utf8)!
//            
//            do {
//                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
//                    let mapper = Mapper<HomeBusiResponse>()
//                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
//                        homeBusiResponse = testObject
//                    }
//                }
//            } catch let error as NSError {
//                print(error.localizedDescription)
//            }
//        }
//        return homeBusiResponse ?? nil
//    }
//    
//    /**
//     *  Sync server api response value to class objects.
//     *
//     *  @param key mappable object.
//     *
//     *  @return empty.
//     *
//     *  @Developed By: Team Consagous [CNSGSIN100]
//     */
//    override func mapping(map: Map) {
//        
//        super.mapping(map: map)
//        
//        busitype        <- map["busitype"]
//        provider_image  <- map["provider_image"]
//        rating          <- map["rating"]
//        totalservice    <- map["totalservice"]
//        busi_user       <- map["busi_user"]
//    }
//}

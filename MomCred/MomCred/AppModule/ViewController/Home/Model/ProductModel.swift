//
//  POSProductModel.swift
//  ShoppingCartLib
//
//  Created by Apple_iOS on 04/02/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  NotificationRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ProductListRequest : Mappable {
    
    private var userid      : String?
    private var _token      : String?
    private var language    : String?
    private var cat_id  : String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key userid, cat_id, token and language parameters class variable intialization.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(userid : String = NMUserDefault.getUserId() ?? "", cat_id  : String?  , token : String = NMUserDefault.getAppToken() ?? "", language : String = HelperConstant.DefaultLang) {
        
        self.language = language
        self._token = token
        self.userid = userid
        self.cat_id = cat_id
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        language        <- map["language"]
        userid          <- map["userid"]
        _token          <- map["token"]
        cat_id      <- map["cat_id"]
    }
}

/**
 *  SearchProduct is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class SearchProduct: Mappable {
    
    private var limit:String?
    private var page:String?
    private var cat_id:String?
    private var user_id:String?
    private var token:String?
    private var width:String?
    private var height:String?
    private var type:String?
    private var search:String?
    private var sort:NSNumber?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key limit, page, cat_id,token,user_id,width,height, type, sort and search parameters class variable intialization.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(limit:String?, page:String?, cat_id:String?,token:String?,user_id:String?,width:String?,height:String?, type:String?, sort:NSNumber?, search:String? = "") {
        self.limit = limit
        self.page = page
        self.cat_id = cat_id
        self.user_id = user_id
        self.token = token
        self.width = width
        self.height = height
        self.type = type
        self.search = search
        self.sort = sort
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        limit   <- map["limit"]
        page    <- map["page"]
        cat_id  <- map["cat_id"]
        user_id <- map["user_id"]
        token   <- map["token"]
        width   <- map["width"]
        height  <- map["height"]
        type    <- map["type"]
        search  <- map["search"]
        sort    <- map["sort"]
    }
}

/**
 *  ProductDetailRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ProductDetailRequest : Mappable {
    
    private var userid      : String?
    private var _token      : String?
    private var language    : String?
    private var product_id  : String?
    private var cat_id  : String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        
    }
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, app token, new password and old password.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(userid : String = NMUserDefault.getUserId()!, product_id  : String?  , cat_id: String?, token : String = NMUserDefault.getAppToken()!, language : String = HelperConstant.DefaultLang) {
        
        
        
        self.language = language
        self._token = token
        self.userid = userid
        self.product_id = product_id
        self.cat_id = cat_id
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func mapping(map: Map) {
        language        <- map["language"]
        userid          <- map["user_id"]
        _token          <- map["token"]
        product_id      <- map["prod_id"]
        cat_id          <- map["cat_id"]
    }
}

/**
 *  ProductDetailResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ProductDetailResponse : APIResponse {
    
    var productDetailDataList:[ProductDetailData]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> ProductDetailResponse?{
        var productDetailResponse:ProductDetailResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<ProductDetailResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    productDetailResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<ProductDetailResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        productDetailResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return productDetailResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        productDetailDataList    <- map["data"]
    }
    
    /**
     *  ProductDetailData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class ProductDetailData: Mappable{
        
        var id              :String?
        var name            :String?
        var description     :String?
        var product_price   :String?
        var discount_price  :String?
        var image_url       :String?
        var stock           :String?
        var is_wishlist     :String?
        var is_add_to_cart  :String?
        var rating          :String?
        var category_id     :String?
        var status          :String?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        required init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->ProductDetailData?{
            var productDetailData:ProductDetailData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<ProductDetailData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        productDetailData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<ProductDetailData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            productDetailData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return productDetailData ?? nil
        }
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        func mapping(map: Map){
            id              <- map["id"]
            name            <- map["name"]
            description     <- map["description"]
            image_url       <- map["image_url"]
            product_price   <- map["price"]
            discount_price  <- map["discount_price"]
            stock           <- map["stock"]
            
            is_add_to_cart  <- map["is_add_to_cart"]
            is_wishlist     <- map["is_wishlist"]
            rating          <- map["rating"]
            category_id     <- map["cat_id"]
            status          <- map["status"]
        }
    }
}

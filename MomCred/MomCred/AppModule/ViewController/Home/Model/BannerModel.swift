//
//  BannerModel.swift
//  ShoppingCartLib
//
//  Created by Apple_iOS on 04/02/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import ObjectMapper

/**
 *  NotificationRequest is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BannerRequest : APIResponse {
    
    private var userid      : String?
    private var _token       : String?
    private var language    : String?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key user id, app token, new password and old password.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    init(userid : String = NMUserDefault.getUserId()!  , token : String = NMUserDefault.getAppToken()!, language : String = HelperConstant.DefaultLang) {
        
        super.init()
        
        self.language = language
        self._token = token
        self.userid = userid
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map) {
        language        <- map["language"]
        userid          <- map["userid"]
        _token           <- map["token"]
    }
}

/**
 *  BannerResponse is serialize object user in API request/response. It specify keys used in API reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BannerResponse : APIResponse {
    
    var bannerResponseDataList:[BannerResponseData]?
    
    /**
     *  Class constructor with custom parameter as per requirement.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init(){
        super.init()
    }
    
    /**
     *  Convert Json response to class object.
     *
     *  @param key server api response is either key-value pair or string.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getModelObjectFromServerResponse(jsonResponse: AnyObject) -> BannerResponse?{
        var bannerResponse:BannerResponse?
        if jsonResponse is NSDictionary{
            let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
            if tempDic != nil{
                let mapper = Mapper<BannerResponse>()
                if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                    bannerResponse = testObject
                }
            }
        }else if jsonResponse is String{
            let tempJsonString = jsonResponse as? String
            let data = tempJsonString?.data(using: String.Encoding.utf8)!
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let mapper = Mapper<BannerResponse>()
                    if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                        bannerResponse = testObject
                    }
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return bannerResponse ?? nil
    }
    
    /**
     *  Method is required constructor of Mappable class
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    required init?(map: Map) {
        super.init(map: map)
    }
    
    /**
     *  Sync server api response value to class objects.
     *
     *  @param key mappable object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func mapping(map: Map){
        
        bannerResponseDataList    <- map["data"]
    }
    
    /**
     *  BannerResponseData is serialize object user in API request/response. It specify keys used in API reponse.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    class BannerResponseData: Mappable{
        
        var id                  :String?
        var user_id             :String?
        var trip_id             :String?
        var title               :String?
        var message             :String?
        var read_status         :String?
        var create_dt           :String?
        var notification_type   :String?
        
        /**
         *  Class constructor with custom parameter as per requirement.
         *
         *  @param key empty.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        required init(){
            
        }
        
        /**
         *  Convert Json response to class object.
         *
         *  @param key server api response is either key-value pair or string.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        func getModelObjectFromServerResponse(jsonResponse: AnyObject)->BannerResponseData?{
            var bannerResponseData:BannerResponseData?
            if jsonResponse is NSDictionary{
                let tempDic:NSDictionary? = (jsonResponse as! NSDictionary)
                if tempDic != nil{
                    let mapper = Mapper<BannerResponseData>()
                    if let testObject = mapper.map(JSON: tempDic as! [String : Any]){
                        bannerResponseData = testObject
                    }
                }
            }else if jsonResponse is String{
                let tempJsonString = jsonResponse as? String
                let data = tempJsonString?.data(using: String.Encoding.utf8)!
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        let mapper = Mapper<BannerResponseData>()
                        if let testObject = mapper.map(JSON: jsonResult as! [String : Any]){
                            bannerResponseData = testObject
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            return bannerResponseData ?? nil
        }
        
        /**
         *  Method is required constructor of Mappable class
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        required init?(map: Map){
            
        }
        
        /**
         *  Sync server api response value to class objects.
         *
         *  @param key mappable object.
         *
         *  @return empty.
         *
         *  @Developed By: Team Consagous [CNSGSIN054]
         */
        func mapping(map: Map){
            id                  <- map["id"]
            user_id             <- map["user_id"]
            trip_id             <- map["tripid"]
            title               <- map["title"]
            message             <- map["message"]
            read_status         <- map["read_status"]
            create_dt           <- map["create_dt"]
            notification_type   <- map["notification_type"]
            
        }
    }
}

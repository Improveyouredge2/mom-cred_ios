//
//  FeaturedCell.swift
//
//  Created by Consagous on 13/10/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit

/**
 *  FeaturedCellDelegate protocol class.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol FeaturedCellDelegate {
    func methodSeeCategoriesProduct(cellIndex:Int)
}

/**
 *  FeaturedCell class is subclass of UICollectionViewCell
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class FeaturedCell: UICollectionViewCell {

    var cellIndex:Int!
    var delegate:FeaturedCellDelegate?
    
    @IBOutlet weak var  btnForCategories: UIButton!
    @IBOutlet weak var viewForMain: UIView!
    @IBOutlet weak var  lblForCateName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func methodSeeCategoriesProduct(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodSeeCategoriesProduct(cellIndex: self.cellIndex)
        }
    }
    
}

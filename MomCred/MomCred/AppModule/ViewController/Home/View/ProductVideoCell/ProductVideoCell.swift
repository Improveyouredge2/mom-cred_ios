//
//  ProductVideoCell.swift
//
//  Created by Consagous on 13/10/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit


/**
 *  ProductVideoCellDelegate protocol class.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol ProductVideoCellDelegate {
    func methodShowVideoDetails(cellIndex:Int)
}

/**
 *  ProductCell class is subclass of UICollectionViewCell
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class ProductVideoCell: UICollectionViewCell {
    
    var cellIndex:Int!
    var delegate:ProductVideoCellDelegate?
    
    @IBOutlet weak var  btnForProductImage: UIButton!
    @IBOutlet weak var  viewForLower: ViewLayerSetup!
    @IBOutlet weak var lblForName: UILabel!
    @IBOutlet weak var lblForDesc: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnForProductImage.imageView?.contentMode =  UIView.ContentMode.scaleAspectFill
        // Initialization code
    }
    
    /**
     *  Show Product detail button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodShowVideoDetails(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodShowVideoDetails(cellIndex: self.cellIndex)
        }
    }
}

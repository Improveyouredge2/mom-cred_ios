//
//  CalendarCell.swift
//
//  Created by Consagous on 13/10/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit

/**
 *  ProductCellDelegate protocol class.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol CalendarCellDelegate {
    
    func addToCart(cellIndex:Int)
    func addToWishList(cellIndex:Int)
    func removeFromWishList(cellIndex:Int)
    func methodShowProductDetails(cellIndex:Int)
    
}

/**
 *  ProductCell class is subclass of UICollectionViewCell
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class CalendarCell: UICollectionViewCell {
    
    var cellIndex:Int!
    var delegate:CalendarCellDelegate?
    
//    @IBOutlet weak var  btnForProductImage: UIButton!
    @IBOutlet weak var  btnForAddToCart: UIButton!
//    @IBOutlet weak var  btnForAddToFav: UIButton!
    @IBOutlet weak var  viewForLower: ViewLayerSetup!
    @IBOutlet weak var lblForCompany: UILabel!
    @IBOutlet weak var lblForName: UILabel!
    @IBOutlet weak var lblForAddress: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    
    /**
     *  Add to Cart text button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func addToCart(_ sender: Any) {
        if(delegate != nil){
            delegate?.addToCart(cellIndex: self.cellIndex)
        }
    }
    
    /**
     *  Add to Wishlist text button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func addToWishList(_ sender: Any) {
        let button : UIButton = sender as! UIButton
        if button.isSelected == true{
            if(delegate != nil){
                delegate?.removeFromWishList(cellIndex: self.cellIndex)
            }
        }else{
            
            if(delegate != nil){
                delegate?.addToWishList(cellIndex: self.cellIndex)
            }
            
        }
    }
    
    /**
     *  Show Product detail button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodShowProductDetails(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodShowProductDetails(cellIndex: self.cellIndex)
        }
    }
}




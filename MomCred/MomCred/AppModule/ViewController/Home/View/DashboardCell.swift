//
//  DashboardCell.swift
//
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit

/**
 *  POSDashboardCell class is subclass of UITableViewCell for UITableView display in SideBar
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class DashboardCell: UITableViewCell {
    
    @IBOutlet weak var lblIcon          :   UILabel!
    @IBOutlet weak var lblTitle         :   UILabel!
    @IBOutlet weak var lblDetail        :   UILabel!
    @IBOutlet weak var lblDate          :   UILabel!
    
    @IBOutlet weak var containerView : ViewLayerSetup!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // animate between regular and selected state
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

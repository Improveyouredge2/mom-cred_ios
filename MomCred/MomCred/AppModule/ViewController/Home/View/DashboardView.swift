//
//  NMNotificationView.swift
//  NotificationLib
//
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftEventBus
//import ImageSlideshow
import SideMenu

/**
 *  Enum for section type of server response in list.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
enum SectionType:String{
    case banner
    case category
    case list
    case video
    case calendar
}

/**
 * DashboardView is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class DashboardView: LMBaseViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightHorizontalSpaceConstraint:NSLayoutConstraint?
    
    @IBOutlet weak var textFieldSearch: UITextField!
    
    @IBOutlet weak var backgroundCheckContainer: UIView! { didSet { backgroundCheckContainer.isHidden = true } }
    @IBOutlet weak var btnCompleteBackgroundCheck: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchViewBottomHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var navigationBar: NavigationBarView! {
        didSet {
            navigationBar.navigationController = navigationController
        }
    }
    
    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    var isFirstLoad = true
    
    // Class object of presenter class
    fileprivate let presenter = DashboardPresenter()
    fileprivate var searchText = ""
    
    fileprivate var advanceSearchOptionListViewController:AdvanceSearchOptionListViewController!
    fileprivate var quickSearchOptions: QuickSearchOptionsListViewController!

    //MARK:- var
    var loadedVeiwCategory = FeaturedCell()
    var loadedVeiwProduct = ProductCell()

    var homeResponseData:[HomeScrResponseData]?
    
    var menus : [HamburgerMenuItem] = []
    var defaultData = false
    var arrNotificationList  :   [NotificationResponse.NotificationResponseData] = []
    var index = 1
    
    //MARK:- let
    let hhTabBarView = HHTabBarView.shared
    //Keeping reference of iOS default UITabBarController.
    let referenceUITabBarController = HHTabBarView.shared.referenceUITabBarController
    
    fileprivate var dashboardAllServiceViewController:DashboardAllServiceViewController?
    fileprivate var dashboardAllVideoViewController:DashboardAllVideoViewController?
    fileprivate var instructionalServiceProviderListViewController:InstructionalServiceProviderListViewController?
    fileprivate var userServiceDetailViewController:UserServiceDetailViewController?
    fileprivate var instructionalServiceProviderListingListView:InstructionalServiceProviderListingListView?
    fileprivate var serviceDetailView:ServiceDetailView?
    
    
    //MARK:- Life Cycle Methods
    // Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set.
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
//        
//        if(role_id == AppUser.ServiceProvider.rawValue){
//            self.searchViewHeightConstraint.constant = 0
//            self.searchViewBottomHeightConstraint.constant = 0
//            self.searchView.isHidden = true
//        }
        
        self.textFieldSearch.shouldShowClearButton(show: true)
        self.textFieldSearch.delegate = self
        
//        refreshView.delegate = self
        presenter.connectView(view: self)
        self.prepareLayout()
        
        //TODO: Dummy data for preparing screen with sample
//        let response = presenter.getHomeDummyData()
//        let homeResponse = HomeResponse().getModelObjectFromServerResponse(jsonResponse: response as AnyObject)
//        self.homeResponseData = homeResponse?.homeResponseData ?? nil
        
    }
    
    // Called after the view was dismissed, covered or otherwise hidden. Default does nothing
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(true)
        SwiftEventBus.unregister(self)
    }
    
    // Called when the view is about to made visible. Default does nothing
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.dashboardAllServiceViewController = nil
        self.dashboardAllVideoViewController = nil
        self.instructionalServiceProviderListViewController = nil
        self.instructionalServiceProviderListingListView = nil
        self.userServiceDetailViewController = nil
        self.serviceDetailView = nil
        
        
        self.setupLanguage(notification: nil)
//        Helper.sharedInstance.quickSearchViewController = nil
        
        SwiftEventBus.onMainThread(self, name: HelperConstant.LanguageChanged, handler: self.setupLanguage)
        SwiftEventBus.onMainThread(self, name: HelperConstant.NetworkConnect, handler: self.onNetworkConnect)
        
        if(Helper.sharedInstance.isNetworkConnected){
            self.fetchHomeScreenList()
        }
        
        if !arrNotificationList.isEmpty {
            self.prepareLayout()
        }
        
        manageAccount()
    }
    
    
    // Called when the view is dismissed, covered or otherwise hidden. Default does nothing
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SwiftEventBus.unregister(self)
    }
    
    /* Base implementation sends -updateConstraints to the view.
     * When a view has a view controller, this message is sent to the view controller during
     * the autolayout updateConstraints pass in lieu of sending updateConstraints directly to the view.
     * You may override this method in a UIViewController subclass for updating custom
     * constraints instead of subclassing your view and overriding -[UIView updateConstraints].
     * Overrides must call super or send -updateConstraints to the view.
     */
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if(self.tableView.contentSize.height > 0){
            tableView.scrollToBottom()
            self.tableViewHeightHorizontalSpaceConstraint?.constant = self.tableView.contentSize.height
        }
    }
    
    func manageAccount(){
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        if role_id == AppUser.ServiceProvider.rawValue {
         if  !PMUserDefault.getBankDetailsAdded(){
            let accountView: PMPaymentSettingView = Helper.sharedInstance.getViewController(storyboardName: PMHelperConstant.ProfileStoryboard, viewControllerName: PMPaymentSettingView.nameOfClass)
            navigationController?.pushViewController(accountView, animated: true)
                
        }
            
    }
        
}
}

//MARK: Textfield delegates
extension DashboardView : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        if(textField.text != nil && (textField.text?.length)! > 0){
            Spinner.show()
            self.searchText = textField.text ?? ""
            presenter.getKeywordSearch(searchText: textField.text ?? "", pageIndex: "0")
        }
        
        return true
    }
    
}

//MARK:- Public method implementation
//MARK:-
extension DashboardView{
    
    /**
     *  Method udpate server response on screen controls.
     *
     *  @param key server response object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func updateScreenInfo(){
        
        // check server repsonse
        if(self.homeResponseData != nil){
            if(self.defaultData){
                self.defaultData = false
            }
            
            self.tableView.reloadData()
            
        } else { // No notification in user account

        }
    }
    
    /**
     *  Method udpate server response on screen controls.
     *
     *  @param key server response object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func showSearchResult(result:DashboardAllServiceProviderResponseData?, keywordSearchRequest:KeywordSearchRequest?){
        
        if(result != nil && result?.busi != nil && (result?.busi?.count)! > 0) && self.instructionalServiceProviderListViewController == nil{
            self.instructionalServiceProviderListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalServiceProviderStoryboard, viewControllerName: InstructionalServiceProviderListViewController.nameOfClass) as InstructionalServiceProviderListViewController
            
            self.instructionalServiceProviderListViewController?.busiList = result?.busi
            self.instructionalServiceProviderListViewController?.screenTitle = self.searchText
            self.instructionalServiceProviderListViewController?.keywordSearchRequest = keywordSearchRequest
            
            self.navigationController?.pushViewController(self.instructionalServiceProviderListViewController!, animated: true)
        }
    }
    
    @IBAction private func btnBackgroundCheckAction(sender: UIButton) {
        let controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.MoreOptionStoryboard, viewControllerName: PMWebView.nameOfClass) as PMWebView
        controller.downloadUrl =  APIKeys.API_GET_BACKGROUND_CHECK_URL
        controller.screenTitle = "Background Check"
        navigationController?.pushViewController(controller, animated: true)
    }
}

//MAKR:- Fileprivate method implementation
//MARK:-
extension DashboardView{
    
    /**
     *  Method user notification list server request with initial index.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @objc fileprivate func fetchHomeScreenList(){
//        presenter.fetchData(pageIndex: 1)
        
        if(isFirstLoad){
            Spinner.show()
        }
        
        if let token = PMUserDefault.getAppToken(), !token.trim().isEmpty {
            presenter.getProfile()
        }
        presenter.getHomeScrDetail()
    }
    
    /**
     *  Method check banner list and display banners on screen.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
//    fileprivate func updateBannerList(){
//        if self.bannerList != nil {
//
////            var sdWebImageSource:[SDWebImageSource] = []
////            for tempImageUrl in self.bannerList! {
////                sdWebImageSource.append(SDWebImageSource(urlString: "\(BASEURLs.baseURL)\(tempImageUrl.image_url ?? "")")!)
////            }
////            self.slideshow.setImageInputs(sdWebImageSource)
//
//            self.tableView.reloadData()
//        }
//    }
    
    /**
     *  Observer on application connect on server.
     *
     *  @param key Notification's class object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func onNetworkConnect(notification:Notification?) -> Void{
        self.fetchHomeScreenList()
    }
    
    /**
     *  Prepare Layout and other screen specific method.
     *
     *  @param key emtpy.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func prepareLayout(){
        
        self.defaultData = true
        var tempObj = NotificationResponse.NotificationResponseData()
        arrNotificationList.append(tempObj)
        
        tempObj = NotificationResponse.NotificationResponseData()
        arrNotificationList.append(tempObj)
        
        tempObj = NotificationResponse.NotificationResponseData()
        arrNotificationList.append(tempObj)
        
        tempObj = NotificationResponse.NotificationResponseData()
        arrNotificationList.append(tempObj)
        
        tempObj = NotificationResponse.NotificationResponseData()
        arrNotificationList.append(tempObj)
        
        tempObj = NotificationResponse.NotificationResponseData()
        arrNotificationList.append(tempObj)
    }
    
    /**
     *  Set Up Localized Strings
     *
     *  @param key Notification's class object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    fileprivate func setupLanguage(notification:Notification?) -> Void{
        self.lbl_NavigationTitle.text = LocalizationKeys.app_name.getLocalized()
    }
    
    
}

extension DashboardView{
    func callbackListing(_ serviceList:[ServiceAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceSearchRequest:ServiceSearchRequest?) -> Void?{
        
        if(self.instructionalServiceProviderListingListView == nil){
            let storyboard = UIStoryboard(name: HelperConstant.InstructionalServiceProviderStoryboard, bundle: nil)
            self.instructionalServiceProviderListingListView = storyboard.instantiateViewController(withIdentifier: InstructionalServiceProviderListingListView.nameOfClass) as? InstructionalServiceProviderListingListView
            self.instructionalServiceProviderListingListView?.isNavigationShow = true
            self.instructionalServiceProviderListingListView?.tabScreenType = .Service
            self.instructionalServiceProviderListingListView?.screenTitle = LocalizationKeys.listing.getLocalized()
            self.instructionalServiceProviderListingListView?.isAdvanceSearchResultShow = true
            self.instructionalServiceProviderListingListView?.serviceList = serviceList
            self.instructionalServiceProviderListingListView?.quickSearchRequest = quickSearchRequest
            self.instructionalServiceProviderListingListView?.serviceSearchRequest = serviceSearchRequest
            
            self.navigationController?.pushViewController(self.instructionalServiceProviderListingListView!, animated: true)
        }
     
        return nil
    }

    func callbackServiceProvider(_ busiList:[BIAddRequest]?, _ quickSearchRequest: QuickSearchRequest?, _ serviceProviderSearchRequest:ServiceProviderSearchRequest?) -> Void?{
        if(self.instructionalServiceProviderListViewController == nil){
            self.instructionalServiceProviderListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.InstructionalServiceProviderStoryboard, viewControllerName: InstructionalServiceProviderListViewController.nameOfClass) as InstructionalServiceProviderListViewController
            
            self.instructionalServiceProviderListViewController?.busiList = busiList
            self.instructionalServiceProviderListingListView?.isAdvanceSearchResultShow = true
            self.instructionalServiceProviderListViewController?.screenTitle = LocalizationKeys.service_provider.getLocalized()
            self.instructionalServiceProviderListViewController?.quickSearchRequest = quickSearchRequest
            self.instructionalServiceProviderListViewController?.serviceProviderSearchRequest = serviceProviderSearchRequest
            self.navigationController?.pushViewController(self.instructionalServiceProviderListViewController!, animated: true)
        }
        return nil
    }
    
    func callbackFacilityListing(_ facilityList:[MyFacilityAddRequest]?, _ facilitySearchRequest: FacilitySearchRequest?) -> Void?{
        
        if(self.instructionalServiceProviderListingListView == nil){
            let storyboard = UIStoryboard(name: HelperConstant.InstructionalServiceProviderStoryboard, bundle: nil)
            self.instructionalServiceProviderListingListView = storyboard.instantiateViewController(withIdentifier: InstructionalServiceProviderListingListView.nameOfClass) as? InstructionalServiceProviderListingListView
            self.instructionalServiceProviderListingListView?.isNavigationShow = true
            self.instructionalServiceProviderListingListView?.tabScreenType = .Facility
            self.instructionalServiceProviderListingListView?.screenTitle = LocalizationKeys.listing.getLocalized()
            self.instructionalServiceProviderListingListView?.isAdvanceSearchResultShow = true
            self.instructionalServiceProviderListingListView?.facilityList = facilityList
            self.instructionalServiceProviderListingListView?.facilitySearchRequest = facilitySearchRequest
            
            self.navigationController?.pushViewController(self.instructionalServiceProviderListingListView!, animated: true)
        }
     
        return nil
    }
    
    func callbackPersonalListing(_ personnelList:[PersonnelAddRequest]?, _ personnelSearchRequest :PersonnelSearchRequest?) -> Void?{
        
        if(self.instructionalServiceProviderListingListView == nil){
            let storyboard = UIStoryboard(name: HelperConstant.InstructionalServiceProviderStoryboard, bundle: nil)
            self.instructionalServiceProviderListingListView = storyboard.instantiateViewController(withIdentifier: InstructionalServiceProviderListingListView.nameOfClass) as? InstructionalServiceProviderListingListView
            self.instructionalServiceProviderListingListView?.isNavigationShow = true
            self.instructionalServiceProviderListingListView?.tabScreenType = .Personnel
            self.instructionalServiceProviderListingListView?.screenTitle = LocalizationKeys.listing.getLocalized()
            self.instructionalServiceProviderListingListView?.isAdvanceSearchResultShow = true
            self.instructionalServiceProviderListingListView?.personnelList = personnelList
            self.instructionalServiceProviderListingListView?.personnelSearchRequest = personnelSearchRequest
            
            self.navigationController?.pushViewController(self.instructionalServiceProviderListingListView!, animated: true)
        }
     
        return nil
    }
    
    func callbackInsideLookListing(_ insideLookList:[ILCAddRequest]?, _ insideLookSearchRequest: InsideLookSearchRequest?) -> Void?{
        
        if(self.instructionalServiceProviderListingListView == nil){
            let storyboard = UIStoryboard(name: HelperConstant.InstructionalServiceProviderStoryboard, bundle: nil)
            self.instructionalServiceProviderListingListView = storyboard.instantiateViewController(withIdentifier: InstructionalServiceProviderListingListView.nameOfClass) as? InstructionalServiceProviderListingListView
            self.instructionalServiceProviderListingListView?.isNavigationShow = true
            self.instructionalServiceProviderListingListView?.tabScreenType = .InsideLook
            self.instructionalServiceProviderListingListView?.screenTitle = LocalizationKeys.listing.getLocalized()
            self.instructionalServiceProviderListingListView?.isAdvanceSearchResultShow = true
            self.instructionalServiceProviderListingListView?.insideLookList = insideLookList
            self.instructionalServiceProviderListingListView?.insideLookSearchRequest = insideLookSearchRequest
            
            self.navigationController?.pushViewController(self.instructionalServiceProviderListingListView!, animated: true)
        }
     
        return nil
    }
    
    
    func callbackInstructionalListing(_ instructionalList:[ICAddRequest]?, _ instructionalSearchRequest: InstructionalSearchRequest?) -> Void?{
        
        if(self.instructionalServiceProviderListingListView == nil){
            let storyboard = UIStoryboard(name: HelperConstant.InstructionalServiceProviderStoryboard, bundle: nil)
            self.instructionalServiceProviderListingListView = storyboard.instantiateViewController(withIdentifier: InstructionalServiceProviderListingListView.nameOfClass) as? InstructionalServiceProviderListingListView
            self.instructionalServiceProviderListingListView?.isNavigationShow = true
            self.instructionalServiceProviderListingListView?.tabScreenType = .InstructionalContent
            self.instructionalServiceProviderListingListView?.screenTitle = LocalizationKeys.listing.getLocalized()
            self.instructionalServiceProviderListingListView?.isAdvanceSearchResultShow = true
            self.instructionalServiceProviderListingListView?.instructionalList = instructionalList
            self.instructionalServiceProviderListingListView?.instructionalSearchRequest = instructionalSearchRequest
            
            self.navigationController?.pushViewController(self.instructionalServiceProviderListingListView!, animated: true)
        }
     
        return nil
    }
}

//MARK:- Actions method implementation
extension DashboardView {
    
    /**
     *  Clear button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnClearAllAction(_ sender : UIButton){
        if self.arrNotificationList.count > 0 {
//            self.presenter.clearData()
        } else {
            self.showBannerAlertWith(message: LocalizationKeys.no_notificaition_to_clear.getLocalized(), alert: PMBannerAlertType.error)
        }
    }
    
    /**
     *  Search button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnSearchAction(_ sender:Any){
        self.textFieldSearch.becomeFirstResponder()
    }
    
    @IBAction func methodNotificationAction(_ sender:Any){
        Helper.sharedInstance.openNotificaiton()
    }
    
    /**
     *  Quick button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodQuickSearchAction(_ sender:Any){
        let quickSearchOptions: QuickSearchOptionsListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: QuickSearchOptionsListViewController.nameOfClass)
        quickSearchOptions.callbackListing = callbackListing
        quickSearchOptions.callbackServiceProvider = callbackServiceProvider
        HelperConstant.appDelegate.navigationController?.present(quickSearchOptions, animated: true, completion: nil)
        self.quickSearchOptions = quickSearchOptions
    }
    
    /**
     *  Advance Search button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodAdvanceSearchAction(_ sender:Any){
        
        if(self.advanceSearchOptionListViewController == nil){
            self.advanceSearchOptionListViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.SearchFilterStoryboard, viewControllerName: AdvanceSearchOptionListViewController.nameOfClass)
        }
        
        if(advanceSearchOptionListViewController != nil){
            self.advanceSearchOptionListViewController.callbackListing = self.callbackListing
            self.advanceSearchOptionListViewController.callbackServiceProvider = self.callbackServiceProvider
            self.advanceSearchOptionListViewController.callbackFacilityListing = self.callbackFacilityListing
            self.advanceSearchOptionListViewController.callbackInsideLookListing = self.callbackInsideLookListing
            self.advanceSearchOptionListViewController.callbackInstructionalListing = self.callbackInstructionalListing
            self.advanceSearchOptionListViewController.callbackPersonalListing = self.callbackPersonalListing
            HelperConstant.appDelegate.navigationController?.present(advanceSearchOptionListViewController, animated: true, completion: nil)
        }
    }
    
    /**
     *  Hamburger menu button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func btnHamburgerAction(_ sender: AnyObject) {
//        present(SideMenuManager.defaultManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    /**
     *  Navigation Back button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    override func actionNavigationBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
        self.defaultData = true
        self.arrNotificationList.removeAll()
    }
}

//MARK:- Tableview data source and delegates
//MARK:-
extension DashboardView  : UITableViewDelegate , UITableViewDataSource  {
    
    /**
     *  Specify height of row in tableview with UITableViewDelegate method override.
     *
     *  @param key tableView object and heightForRowAt index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if(indexPath.row == 0 || indexPath.row == 1) { // Banner || Video
//            return 230
//        }else if(indexPath.row == 2) { // Image
//            return 165
//        } else {
//            return 235
//        }
        
        var rowHeight:CGFloat = 265
        if(homeResponseData != nil && (homeResponseData?.count)! > 0){
            if let type = homeResponseData?[indexPath.row].type{
                switch type{
                case SectionType.banner.rawValue:
                    rowHeight = 230
                    break
                case SectionType.category.rawValue:
                    rowHeight = 165
                    break
                case SectionType.video.rawValue:
                    rowHeight = 265
                    break
                case SectionType.list.rawValue:
                    rowHeight = 320
//                    rowHeight = 365
                    break
                case SectionType.calendar.rawValue:
                    rowHeight = 200
                    break
                default:
                    break
                }
            }
        }
        
        return rowHeight
    }
    
    /**
     *  Specify number of section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeResponseData != nil ? (homeResponseData?.count)! : 0
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoryRow.nameOfClass) as! CategoryRow
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.registerCustomCells()
        cell.delegate = self
        cell.rowIndex = indexPath.row
        
        let objectInfo = homeResponseData?[indexPath.row]
        
        if let type = objectInfo?.type{
            switch type{
            case SectionType.banner.rawValue:
                cell.cellType = .Banner
                cell.constraintSectionNameHeight.constant = 0
                cell.constraintSectionDescHeight.constant = 0
                cell.bannerList = objectInfo?.imagebanner ?? []
                break
            case SectionType.category.rawValue:
                cell.cellType = .Category
                break
            case SectionType.video.rawValue:
                cell.cellType = .Video
                cell.bannerList = objectInfo?.videobanner ?? []
                break
            case SectionType.list.rawValue:
                cell.cellType = .Service
                cell.serviceList = objectInfo?.service ?? []
                break
            case SectionType.calendar.rawValue:
                cell.cellType = .CalenderEvent
                break
            default:
                break
            }
        }
        
        cell.labelRowTitle.text = objectInfo?.name ?? ""
        cell.labelRowDesc.text = objectInfo?.desc ?? ""
        cell.backgroundColor = UIColor.clear
        
        cell.updateCellInfo()
        return cell
    }
}

// MARK:- CatgeoryRowDelegate method implementation
extension DashboardView: CategoryRowDelegate {
    /**
     *  Method is for selecting product in catgory list. This is CategoryRowDelegate delegate method implementation. It is open all product list view controller for selected category.
     *
     *  @param key cellIndex and rowIndex.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func methodSeeCategoriesProduct(cellIndex:Int, rowIndex:Int){
        
//        let catInfo = categoryList![cellIndex]
//        let controller = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ProductVCStoryboard, viewControllerName: ProductListVC.nameOfClass) as ProductListVC
//        controller.cat_id = catInfo.id ?? ""
//        controller.screenTitle = catInfo.name ?? ""
//        HelperConstant.appDelegate.navigationController?.pushViewController(controller, animated: true)
    }
    
    fileprivate func openEnthuaistServiceDetail(service:ServiceAddRequest?){
        
//        if(self.userServiceDetailViewController == nil){
//            self.userServiceDetailViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ProductDetailStoryboard, viewControllerName: UserServiceDetailViewController.nameOfClass) as UserServiceDetailViewController
//
//            self.userServiceDetailViewController?.service = service
//
//            self.navigationController?.pushViewController(self.userServiceDetailViewController!, animated: true)
//        }
        
        if(self.serviceDetailView == nil){
            self.serviceDetailView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceDetailView.nameOfClass) as ServiceDetailView
            
            self.serviceDetailView?.isHideEditOption = true
            self.serviceDetailView?.serviceAddRequest = service
            self.navigationController?.pushViewController(self.serviceDetailView!, animated: true)
        }
    }
    
    fileprivate func openServiceProviderServiceDetail(service:ServiceAddRequest?){
        
        if(self.serviceDetailView == nil){
            self.serviceDetailView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceDetailView.nameOfClass) as ServiceDetailView
            
            
            self.serviceDetailView?.serviceAddRequest = service
            self.navigationController?.pushViewController(self.serviceDetailView!, animated: true)
        }
    }
    /**
     *  Method is for selecting product in catgory list. This is CategoryRowDelegate delegate method implementation. It is show product details.
     *
     *  @param key cellIndex and rowIndex.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func methodShowProductDetails(cellIndex:Int, rowIndex:Int){
        
        let objectInfo = homeResponseData?[rowIndex]
        let servcieInfo = objectInfo?.service![cellIndex]
//        self.openServiceScr(service: servcieInfo)
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            self.openServiceProviderServiceDetail(service: servcieInfo)
            break
        case AppUser.LocalBusiness.rawValue:
            self.openEnthuaistServiceDetail(service: servcieInfo)
            break
        case AppUser.Enthusiast.rawValue:
            self.openEnthuaistServiceDetail(service: servcieInfo)
            break
        default:
            break
        }
       
    }
    
    /**
     *  Method is for selecting product in catgory list. This is CategoryRowDelegate delegate method implementation. It is open all product list view controller for selected category.
     *
     *  @param key cellIndex and rowIndex.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func methodSeeAll(rowIndex: Int) {
        
        let objectInfo = homeResponseData?[rowIndex]
        
        if let type = objectInfo?.type{
            switch type{
            case SectionType.banner.rawValue:
                
                break
            case SectionType.category.rawValue:
                
                break
            case SectionType.video.rawValue:
                
                if(self.dashboardAllVideoViewController == nil){
                    self.dashboardAllVideoViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.DashboardStoryboard, viewControllerName: DashboardAllVideoViewController.nameOfClass) as DashboardAllVideoViewController
                    
                    self.dashboardAllVideoViewController?.bannerList = objectInfo?.videobanner
                     self.navigationController?.pushViewController(self.dashboardAllVideoViewController!, animated: true)
                }
                break
            case SectionType.list.rawValue:

                if(self.dashboardAllServiceViewController == nil){
                    self.dashboardAllServiceViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.DashboardStoryboard, viewControllerName: DashboardAllServiceViewController.nameOfClass) as DashboardAllServiceViewController
                     self.navigationController?.pushViewController(self.dashboardAllServiceViewController!, animated: true)
                }

                break
            case SectionType.calendar.rawValue:
                
                break
            default:
                break
            }
        }
    }
}

//MARK:-
//MARK:- @end

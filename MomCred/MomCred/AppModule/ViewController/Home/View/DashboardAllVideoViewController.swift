//
//  DashboardAllVideoViewController.swift
//  MomCred
//
//  Created by Rajesh Yadav on 11/10/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation
import Lightbox
import AVKit


/**
 * DashboardAllVideoViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class DashboardAllVideoViewController: LMBaseViewController {
    
    @IBOutlet weak var collectionViewMain: UICollectionView!
    
    fileprivate var isDisplayLoader = true
    
    //MARK:- fileprivate
//    fileprivate let refreshView = KRPullLoadView()
//    fileprivate let loadMoreView = KRPullLoadView()
//    fileprivate var isDownloadWorking = false
    
    // Class object of presenter class
//    fileprivate let presenter = DashboardAllServicePresenter()
    var currentPageIndex = 0
    
    var bannerList:[BannerDetail]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewMain.register(UINib(nibName: ProductVideoCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: ProductVideoCell.nameOfClass)
        
        // Add refresh loader for pulling and refresh
//        refreshView.delegate = self
//        collectionViewMain.addPullLoadableView(refreshView, type: .refresh)
//        loadMoreView.delegate = self
//        collectionViewMain.addPullLoadableView(loadMoreView, type: .loadMore)
        
//        presenter.connectView(view: self)
        
//        presenter.getAllServiceHomeListDetail(callback: {
//            (status, response, message) in
//            
//        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.perform(#selector(self.prepareScrInfo), with: self, afterDelay: 0.2)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//extension DashboardAllVideoViewController: KRPullLoadViewDelegate{
//
//    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
//        if type == .loadMore {
//            switch state {
//            case let .pulling(offset, threshould):
//                print("Pulling")
//                break
//            case let .loading(completionHandler):
//                //                }
//                self.isDisplayLoader = false
//                self.currentPageIndex += 1
//
//                presenter.getAllServiceHomeListDetail(callback: {
//                    (status, response, message) in
//
//                    completionHandler()
//                })
//                break
//            default: break
//            }
//            return
//        }
//
//        switch state {
//        case .none:
//            pullLoadView.messageLabel.text = ""
//
//        case let .pulling(offset, threshould):
//            if offset.y > threshould {
//                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
//            } else {
//                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
//                 self.currentPageIndex = 0
//            }
//            self.isDisplayLoader = false
//
//            break
//
//        case let .loading(completionHandler):
//            pullLoadView.messageLabel.text = "Updating..."
//
//            presenter.getAllServiceHomeListDetail(callback: {
//                (status, response, message) in
//
//                completionHandler()
//            })
//            break
//        }
//    }
//}

extension DashboardAllVideoViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerList != nil ? (bannerList?.count)! : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let banner = bannerList?[indexPath.row]
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductVideoCell.nameOfClass, for: indexPath) as! ProductVideoCell
        
        cell.cellIndex = indexPath.row
        cell.backgroundColor = UIColor.clear
        
        cell.delegate = self
        
        if(banner?.title != nil){
            cell.lblForName.text =  banner?.title?.capitalized ?? ""
        }
        
        if(banner?.description != nil){
            cell.lblForDesc.text = banner?.description?.capitalized ?? ""
        }
        
        if(banner?.banner != nil){
            //cell.btnForProductImage.sd_setImage(with: URL(string: "\(BASEURLs.baseURL)\(productInfo?.image_url ?? "")"), for: .normal, completed: nil)
            cell.btnForProductImage.sd_setImage(with: URL(string: banner?.thumb ?? ""), for: .normal, completed: nil)
        }

        
        return cell
        
    }
}

extension DashboardAllVideoViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        //        let ssnString = ((arrForData.object(at: indexPath.row) as AnyObject).value(forKey: "ssn") as? String)!
        //        let notificationName = Notification.Name("CELLSELCTED")
        //        // Post notification
        //        NotificationCenter.default.post(name: notificationName, object: ssnString)
        
    }
}

extension DashboardAllVideoViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return  CGSize(width:(collectionView.frame.width/2 ) , height: 265)
    }
}

//MARK:- ProductCellDelegate method implementation
extension DashboardAllVideoViewController: ProductVideoCellDelegate {
    func methodShowVideoDetails(cellIndex:Int){
        let banner = bannerList?[cellIndex]
                    
        if(banner?.banner != nil){
            //                cell.btnForProductImage.sd_setImage(with: URL(string: banner?.banner ?? ""), for: .normal, completed: nil)
            self.zoomableView(videoUrl: banner?.banner)
        }
    }
}

extension DashboardAllVideoViewController{
    fileprivate func zoomableView(videoUrl:String?){
        // Create an array of images.
        var images:[LightboxImage] = []
        
        images.append(LightboxImage(
            imageURL: URL(string: videoUrl ?? "")!
        ))
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)
        // Present your controller.
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
//        present(controller, animated: true, completion: nil)
        HelperConstant.appDelegate.window?.rootViewController?.present(controller, animated: true, completion: nil)
        
        LightboxConfig.handleVideo = { from, videoURL in
            // Custom video handling
            let videoController = AVPlayerViewController()
            videoController.player = AVPlayer(url: videoURL)
            
            from.present(videoController, animated: true) {
                videoController.player?.play()
            }
        }
    }
}


extension DashboardAllVideoViewController: LightboxControllerPageDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension DashboardAllVideoViewController: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}

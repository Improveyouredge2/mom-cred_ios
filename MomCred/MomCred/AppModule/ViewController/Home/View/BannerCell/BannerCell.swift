//
//  BannerCell.swift
//
//  Created by Consagous on 13/10/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit
import ImageSlideshow

/**
 *  BannerCellDelegate protocol class.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol BannerCellDelegate {
    func methodSeeCategoriesProduct(cellIndex:Int)
}

/**
 *  BannerCell class is subclass of UICollectionViewCell
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class BannerCell: UICollectionViewCell {

    var cellIndex:Int!
    var delegate:BannerCellDelegate?
    
    var bannerList:[BannerDetail]?
    
    @IBOutlet weak var slideshow: ImageSlideshow!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        /////////////////////////////////////////////////////////////
        slideshow.backgroundColor = UIColor.white
        slideshow.slideshowInterval = 5.0
        slideshow.pageControlPosition = PageControlPosition.insideScrollView
        slideshow.pageControl.currentPageIndicatorTintColor = UIColor.appTheamRedColor()
        slideshow.pageControl.pageIndicatorTintColor = UIColor.appTheamWhiteColor()
        slideshow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
//            print("current page:", page)
        }
        /////////////////////////////////////////////////////////////
    }
    
    func updateBannerList(){
        if self.bannerList != nil {
            var sdWebImageSource:[SDWebImageSource] = []
            for tempImageUrl in self.bannerList! {
                sdWebImageSource.append(SDWebImageSource(urlString: tempImageUrl.banner ?? "")!)
            }
            self.slideshow.setImageInputs(sdWebImageSource)
        }
    }

    @IBAction func methodSeeCategoriesProduct(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodSeeCategoriesProduct(cellIndex: self.cellIndex)
        }
    }
    
}

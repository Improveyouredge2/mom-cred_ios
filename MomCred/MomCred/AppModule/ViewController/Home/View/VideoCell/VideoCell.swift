//
//  VideoCell.swift
//
//  Created by Consagous on 13/10/17.
//  Copyright © 2017 Consagous. All rights reserved.
//

import UIKit

/**
 *  BannerCellDelegate protocol class.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol VideoCellDelegate {
    func methodVideoCellClick(cellIndex:Int)
}

/**
 *  BannerCell class is subclass of UICollectionViewCell
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class VideoCell: UICollectionViewCell {

//    @IBOutlet weak var playerView: AGVideoPlayerView!
    
    @IBOutlet weak var imagePreview: UIImageView!
    
    var cellIndex:Int!
    var delegate:VideoCellDelegate?
    var videoUrl:String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imagePreview.backgroundColor = UIColor.blue
    }
    
    @IBAction func methodVideoClick(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodVideoCellClick(cellIndex: self.cellIndex)
        }
    }
    
}

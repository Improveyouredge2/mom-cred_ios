//
//  DashboardAllServiceViewController.swift
//  MomCred
//
//  Created by Rajesh Yadav on 10/10/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 * DashboardAllServiceViewController is a landing page of application in this display shopping cart Category, Product list and Banner. Manage sidebar and other screen require for app flow.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class DashboardAllServiceViewController: LMBaseViewController {
    
    @IBOutlet weak var collectionViewMain: UICollectionView!
    
    fileprivate var isDisplayLoader = true
    
    //MARK:- fileprivate
    fileprivate let refreshView = KRPullLoadView()
    fileprivate let loadMoreView = KRPullLoadView()
    fileprivate var isDownloadWorking = false
    
    // Class object of presenter class
    fileprivate let presenter = DashboardAllServicePresenter()
    fileprivate var userServiceDetailViewController:UserServiceDetailViewController?
    fileprivate var serviceDetailView:ServiceDetailView?
    
    var currentPageIndex = 0
    
    var serviceList:[ServiceAddRequest]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewMain.register(UINib(nibName: ProductCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: ProductCell.nameOfClass)
        
        // Add refresh loader for pulling and refresh
        refreshView.delegate = self
        collectionViewMain.addPullLoadableView(refreshView, type: .refresh)
        loadMoreView.delegate = self
        collectionViewMain.addPullLoadableView(loadMoreView, type: .loadMore)
        
        presenter.connectView(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.userServiceDetailViewController = nil
        self.serviceDetailView = nil
        
        // restart page info
        self.currentPageIndex = 0
        presenter.getAllServiceHomeListDetail(callback: {
            (status, response, message) in
            
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension DashboardAllServiceViewController: KRPullLoadViewDelegate{
    
    func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
        if type == .loadMore {
            switch state {
            case let .pulling(offset, threshould):
                print("Pulling")
                break
            case let .loading(completionHandler):
                //                }
                self.isDisplayLoader = false
                self.currentPageIndex += 1
                
                presenter.getAllServiceHomeListDetail(callback: {
                    (status, response, message) in
                    
                    completionHandler()
                })
                break
            default: break
            }
            return
        }
        
        switch state {
        case .none:
            pullLoadView.messageLabel.text = ""
            
        case let .pulling(offset, threshould):
            if offset.y > threshould {
                //pullLoadView.messageLabel.text = "Pull more. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
            } else {
                // pullLoadView.messageLabel.text = "Release to refresh. offset: \(Int(offset.y)), threshould: \(Int(threshould)))"
                 self.currentPageIndex = 0
            }
            self.isDisplayLoader = false
            
            break
            
        case let .loading(completionHandler):
            pullLoadView.messageLabel.text = "Updating..."
            
            presenter.getAllServiceHomeListDetail(callback: {
                (status, response, message) in
                
                completionHandler()
            })
            break
        }
    }
}

extension DashboardAllServiceViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return serviceList != nil ? (serviceList?.count)! : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let service = serviceList?[indexPath.row]
        // Default product cell
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCell.nameOfClass, for: indexPath) as! ProductCell
        cell.delegate =  self
        cell.cellIndex = indexPath.row
        
        if(service?.mediabusiness != nil && (service?.mediabusiness?.count)! > 0 && service?.mediabusiness![0].thumb != nil){
            cell.btnForProductImage.sd_setImage(with: URL(string: service?.mediabusiness![0].thumb ?? ""), for: .normal, completed: nil)
        }
        
        if(service?.provider_image != nil){
            cell.imageViewProfile.sd_setImage(with: URL(string: service?.provider_image ?? ""), completed: nil)
        }
        
        if(service?.service_name != nil){
            cell.lblForName.text =  service?.service_name?.capitalized ?? ""
        } else {
            cell.lblForName.text = ""
        }
        
        if(service?.service_field != nil && (service?.service_field?.count)! > 0){
            cell.lblType.text = service?.service_field![0].specificFieldName?.capitalized ?? ""
        } else {
            cell.lblType.text = ""
        }
        
        if(service?.rating != nil){
            //                if let n = NumberFormatter().numberFromString(service?.rating) {
            //                    let floatValue = CGFloat(n)
            //                    cell.starRatingView.value = floatValue
            //                }
            
            cell.starRatingView.value = CGFloat(service?.rating?.floatValue ?? 0.0)
        } else {
            cell.starRatingView.value = 0.0
        }
        
        if(service?.busitype != nil){
            cell.lblForCompany.text = service?.busitype ?? ""
        } else {
            cell.lblForCompany.text = ""
        }
        
        var address = ""
        if(service?.service_location_info?.location_name != nil && (service?.service_location_info?.location_name?.length)! > 0){
//            cell.lblForCompany.text = service?.service_location_info?.location_name ?? ""
            address = service?.service_location_info?.location_address ?? ""
            
        } else if(service?.service_location_info?.location_name_sec != nil && (service?.service_location_info?.location_name_sec?.length)! > 0){
//            cell.lblForCompany.text = service?.service_location_info?.location_name_sec ?? ""
            address = service?.service_location_info?.location_address_sec ?? ""
        }
        cell.lblForAddress.text = address
        
        return cell
        
    }
}

extension DashboardAllServiceViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        //        let ssnString = ((arrForData.object(at: indexPath.row) as AnyObject).value(forKey: "ssn") as? String)!
        //        let notificationName = Notification.Name("CELLSELCTED")
        //        // Post notification
        //        NotificationCenter.default.post(name: notificationName, object: ssnString)
        
    }
}

extension DashboardAllServiceViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCell.nameOfClass, for: indexPath) as! ProductCell
        
        
        let service = serviceList?[indexPath.row]

        if(service?.mediabusiness != nil && (service?.mediabusiness?.count)! > 0 && service?.mediabusiness![0].thumb != nil){
            cell.btnForProductImage.sd_setImage(with: URL(string: service?.mediabusiness![0].thumb ?? ""), for: .normal, completed: nil)
        }
        
        if(service?.service_name != nil){
            cell.lblForName.text =  service?.service_name?.capitalized ?? ""
        } else {
            cell.lblForName.text = ""
        }
        
        if(service?.service_field != nil && (service?.service_field?.count)! > 0){
            cell.lblType.text = service?.service_field![0].specificFieldName?.capitalized ?? ""
        } else {
            cell.lblType.text = ""
        }
        
        if(service?.busitype != nil){
            cell.lblForCompany.text = service?.busitype ?? ""
        } else {
            cell.lblForCompany.text = ""
        }
        
        var address = ""
        if(service?.service_location_info?.location_name != nil && (service?.service_location_info?.location_name?.length)! > 0){
//            cell.lblForCompany.text = service?.service_location_info?.location_name ?? ""
            address = service?.service_location_info?.location_address ?? ""
            
        } else if(service?.service_location_info?.location_name_sec != nil && (service?.service_location_info?.location_name_sec?.length)! > 0){
//            cell.lblForCompany.text = service?.service_location_info?.location_name_sec ?? ""
            address = service?.service_location_info?.location_address_sec ?? ""
        }
        cell.lblForAddress.text = address

        
        return  CGSize(width:(collectionView.frame.width/2 ) , height: cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height)
    }
}

extension DashboardAllServiceViewController{
    fileprivate func openEnthuaistServiceDetail(cellIndex:Int){
        let service = serviceList?[cellIndex]
        
//        if(self.userServiceDetailViewController == nil){
//            self.userServiceDetailViewController = Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ProductDetailStoryboard, viewControllerName: UserServiceDetailViewController.nameOfClass) as UserServiceDetailViewController
//
//            self.userServiceDetailViewController?.service = service
//
//            self.navigationController?.pushViewController(self.userServiceDetailViewController!, animated: true)
//        }
        
        if(self.serviceDetailView == nil){
            self.serviceDetailView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceDetailView.nameOfClass) as ServiceDetailView
            
            self.serviceDetailView?.serviceAddRequest = service
            self.serviceDetailView?.serviceAddRequest = serviceList?[cellIndex]
            self.serviceDetailView?.isHideEditOption = true
            self.navigationController?.pushViewController(self.serviceDetailView!, animated: true)
        }
    }
    
    fileprivate func openServiceProviderServiceDetail(cellIndex:Int){
        
        if(self.serviceDetailView == nil){
            self.serviceDetailView =  Helper.sharedInstance.getViewController(storyboardName: HelperConstant.ServiceStoryboard, viewControllerName: ServiceDetailView.nameOfClass) as ServiceDetailView
            
            self.serviceDetailView?.serviceAddRequest = serviceList?[cellIndex]
            self.navigationController?.pushViewController(self.serviceDetailView!, animated: true)
        }
    }
}

//MARK:- ProductCellDelegate method implementation
extension DashboardAllServiceViewController: ProductCellDelegate {
    
    /**
     *  ProductCellDelegate method definition, method is for selecting product in product list.
     *
     *  @param key cellIndex.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func methodShowProductDetails(cellIndex:Int){

        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        switch  role_id {
        case AppUser.ServiceProvider.rawValue:
            self.openServiceProviderServiceDetail(cellIndex: cellIndex)
            break
        case AppUser.LocalBusiness.rawValue:
            self.openEnthuaistServiceDetail(cellIndex: cellIndex)
            break
        case AppUser.Enthusiast.rawValue:
            self.openEnthuaistServiceDetail(cellIndex: cellIndex)
            break
        default:
            break
        }
    }
}

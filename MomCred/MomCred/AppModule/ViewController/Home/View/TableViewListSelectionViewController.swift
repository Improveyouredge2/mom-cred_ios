//
//  TableViewListSelectionViewController.swift
//  MomCred
//
//  Created by Apple_iOS on 29/06/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

class TableViewListSelectionData{
    var id:String = ""
    var title:String = ""
    var selectionStatus:Bool = false
}

enum SelectionFilterType {
    case Multi
    case Single
}

/**
 *  FilterOptionListDelegate protocol class.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol TableViewListSelectionViewControllerDelegate{
    func updateOptionList(selectionDataList:[TableViewListSelectionData], isUpdate:Bool)
}

class TableViewListSelectionViewController : LMBaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var selectionType: SelectionFilterType = .Multi
    
    fileprivate var expandTableNumber:[Int] = []
    var selectionDataList:[TableViewListSelectionData] = []
    
    var delegate:TableViewListSelectionViewControllerDelegate?
    var isUpdate = false
    
    override func awakeFromNib() {
        //setScreenData()
    }
}

//MARK:- Action method implementation
extension TableViewListSelectionViewController{
    @IBAction func methodNextAction(_ sender:UIButton){
        
        var tempSelectionList:[TableViewListSelectionData] = []
        for selectedInfo in selectionDataList{
            if(selectedInfo.selectionStatus){
                tempSelectionList.append(selectedInfo)
            }
        }
        
        
        if(delegate != nil){
            delegate?.updateOptionList(selectionDataList: tempSelectionList, isUpdate: self.isUpdate)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK:- UITableViewDataSource & UITableViewDelegate
extension TableViewListSelectionViewController : UITableViewDataSource , UITableViewDelegate {
    
    /**
     *  Specify number of rows on section in tableview with UITableViewDataSource method override.
     *
     *  @param key tableView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectionDataList.count
//        return 5
    }
    
    /**
     *  Implement table cell in tableView with UITableViewDataSource method override.
     *
     *  @param key empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        updateViewConstraints()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessContactAddPersonnelCell.nameOfClass) as! BusinessContactAddPersonnelCell
        
        cell.delegate = self
//        cell.lblTitle.text = "Sample \(indexPath.row)"
        cell.cellIndex = indexPath
        cell.lblTitle.text = self.selectionDataList[indexPath.row].title
        cell.btnCheck.isSelected = self.selectionDataList[indexPath.row].selectionStatus
        
        return cell
    }
    
    
    /**
     *  Asks the delegate for the height to use for a row in a specified location.
     *
     *  @param key empty.
     *
     *  @return row heigth.
     *
     *  @Developed By: Team Consagous [CNSGSIN079]
     */
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
}

extension TableViewListSelectionViewController: BusinessContactAddPersonnelCellDelegate{
    func selectListItem(cellIndex: IndexPath?, status:Bool, tableView:UITableView?){
        
        self.isUpdate = true
        self.selectionDataList[cellIndex?.row ?? 0].selectionStatus = status
        self.tableView.reloadData()
        
        
//        if(status){
//            self.expandTableNumber.append(cellIndex?.row ?? 0)
//        } else {
//            if let index = self.expandTableNumber.index(of: cellIndex?.row ?? 0) {
//                self.expandTableNumber.remove(at: index)
//            }
//        }
    }
}

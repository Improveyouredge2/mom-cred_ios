//
//  CategoryRow.swift
//  TwoDirectionalScroller
//
//  Created by Robert Chen on 7/11/15.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit
import SDWebImage
import Lightbox
import AVKit

enum CellType{
    case Text
    case Service
    case Category
    case Banner
    case CalenderEvent
    case Image
    case Video
}

/**
 * DashboardViewDelegate specify method signature.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
protocol CategoryRowDelegate {
    func methodSeeCategoriesProduct(cellIndex:Int, rowIndex:Int)
    func methodShowProductDetails(cellIndex:Int, rowIndex:Int)
    func methodSeeAll(rowIndex:Int)
}

/**
 * CategoryRow display Category list of product with Product option in collection on UITableViewCell.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class CategoryRow : UITableViewCell {
    @IBOutlet weak var collectionViewMain: UICollectionView!
    @IBOutlet weak var labelRowTitle: UILabel!
    @IBOutlet weak var labelRowDesc: UILabel!
    @IBOutlet weak var btnSellAll: UIButton!
    
    @IBOutlet weak var constraintSectionNameHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintSectionDescHeight: NSLayoutConstraint!
    
    var rowIndex:Int!
    var delegate:CategoryRowDelegate?
    var is_product = true
    
    var cellType:CellType = CellType.Service
    
//    var categoriesList:[CategoryListDetail]?
    var serviceList:[ServiceAddRequest]?
    var bannerList:[BannerDetail]?
    var videoUrl:String?
    
    let rowValue = Int()
    
    /**
     *  Register UITableViewCell for display in UITableView.
     *
     *  @param empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func registerCustomCells(){
        collectionViewMain.register(UINib(nibName: ProductCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: ProductCell.nameOfClass)
        collectionViewMain.register(UINib(nibName: FeaturedCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: FeaturedCell.nameOfClass)
        
        collectionViewMain.register(UINib(nibName: BannerCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: BannerCell.nameOfClass)
        
        collectionViewMain.register(UINib(nibName: VideoCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: VideoCell.nameOfClass)
        
        collectionViewMain.register(UINib(nibName: ProductVideoCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: ProductVideoCell.nameOfClass)
        
        collectionViewMain.register(UINib(nibName: CalendarCell.nameOfClass, bundle: nil), forCellWithReuseIdentifier: CalendarCell.nameOfClass)
    }
    
    /**
     *  Method manage seeAll option and reload product list collection.
     *
     *  @param empty.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func updateCellInfo(){
        
        if(cellType == .Category || cellType == .Banner || cellType == .Image){
            self.btnSellAll.isHidden = true
        } else{
            self.btnSellAll.isHidden = false
        }
        collectionViewMain.reloadData()
    }
    
    func updateBannerList(bannerList:[BannerDetail]?){
//        if self.bannerList != nil {
//
//        }
        
        self.bannerList = bannerList
        
    }
 }

extension CategoryRow{
    
    /**
     *  SeeAll button click event action method.
     *
     *  @param key sender is having button object.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    @IBAction func methodSeeAll(_ sender: Any) {
        if(delegate != nil){
            delegate?.methodSeeAll(rowIndex: self.rowIndex)
        }
    }
}

extension CategoryRow : UICollectionViewDataSource {
   
    /**
     *  Specify number of rows on section in collectionView with UICollectionViewDataSource method override.
     *
     *  @param key UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(cellType == .Category){
//            return categoriesList != nil ? (categoriesList?.count)! : 0
            return 0
        } else if(cellType == .Service || cellType == .CalenderEvent) {
            return serviceList != nil ? (serviceList?.count)! : 0
        } else if(cellType == .Video){
            return bannerList != nil ? (bannerList?.count)! : 0
        } else if(cellType == .Banner || cellType == .Image ){
            return 1
        }
        
        return 0
    }
    
    /**
     *  Implement collectionview cell in collectionView with UICollectionViewDataSource method override.
     *
     *  @param UICollectionView object and section index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(cellType == .Service) {
            let service = serviceList?[indexPath.row]
            // Default product cell
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCell.nameOfClass, for: indexPath) as! ProductCell
            cell.delegate =  self
            cell.cellIndex = indexPath.row
            
            if(service?.mediabusiness != nil && (service?.mediabusiness?.count)! > 0 && service?.mediabusiness![0].thumb != nil){
                cell.btnForProductImage.sd_setImage(with: URL(string: service?.mediabusiness![0].thumb ?? ""), for: .normal, completed: nil)
            }
            
            if(service?.provider_image != nil){
                cell.imageViewProfile.sd_setImage(with: URL(string: service?.provider_image ?? ""), completed: nil)
            }
            
            if(service?.service_name != nil){
                cell.lblForName.text =  service?.service_name?.capitalized ?? ""
            } else {
                cell.lblForName.text = ""
            }
            
            if(service?.service_field != nil && (service?.service_field?.count)! > 0){
                cell.lblType.text = service?.service_field![0].specificFieldName?.capitalized ?? ""
            } else {
                cell.lblType.text = ""
            }
            
            if(service?.rating != nil){
//                if let n = NumberFormatter().numberFromString(service?.rating) {
//                    let floatValue = CGFloat(n)
//                    cell.starRatingView.value = floatValue
//                }
                
                cell.starRatingView.value = CGFloat(service?.rating?.floatValue ?? 0.0)
            } else {
                cell.starRatingView.value = 0.0
            }
            
            
            if(service?.busitype != nil){
                cell.lblForCompany.text = service?.busitype
            } else {
                cell.lblForCompany.text = ""
            }
            var address = ""
            if(service?.service_location_info?.location_name != nil && (service?.service_location_info?.location_name?.length)! > 0){
//                cell.lblForCompany.text = service?.service_location_info?.location_name ?? ""
                address = service?.service_location_info?.location_address ?? ""
                
            } else if(service?.service_location_info?.location_name_sec != nil && (service?.service_location_info?.location_name_sec?.length)! > 0){
//                cell.lblForCompany.text = service?.service_location_info?.location_name_sec ?? ""
                address = service?.service_location_info?.location_address_sec ?? ""
            }
            cell.lblForAddress.text = address
            
            return cell
        } else if(cellType == .Banner) {
//
            // Default product cell
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannerCell.nameOfClass, for: indexPath) as! BannerCell
            cell.bannerList = self.bannerList ?? nil
            
            cell.updateBannerList()
            
            return cell
        } else if(cellType == .CalenderEvent) {
            
            // Default product cell
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: CalendarCell.nameOfClass, for: indexPath) as! CalendarCell
            
            return cell
        } else if(cellType == .Video) {
            let banner = bannerList?[indexPath.row]
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductVideoCell.nameOfClass, for: indexPath) as! ProductVideoCell
            
            cell.cellIndex = indexPath.row
            cell.backgroundColor = UIColor.clear
            
            cell.delegate = self
            
            if(banner?.title != nil){
                cell.lblForName.text =  banner?.title?.capitalized ?? ""
            }
            
            if(banner?.description != nil){
                cell.lblForDesc.text = banner?.description?.capitalized ?? ""
            }
            
            if(banner?.banner != nil){
                //cell.btnForProductImage.sd_setImage(with: URL(string: "\(BASEURLs.baseURL)\(productInfo?.image_url ?? "")"), for: .normal, completed: nil)
                cell.btnForProductImage.sd_setImage(with: URL(string: banner?.thumb ?? ""), for: .normal, completed: nil)
            }

            
            return cell
        }
        
        return UICollectionViewCell(frame: collectionView.frame)
    }
}

extension CategoryRow : UICollectionViewDelegate {
    
    /**
     *  Implement tap event on collectionview cell in collectionView with UICollectionViewDelegate method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        //TODO: Delegate method implement for different cell
    }
}

extension CategoryRow : UICollectionViewDelegateFlowLayout {
    
    /**
     *  Implement collectionView with UICollectionViewDelegateFlowLayout method override.
     *
     *  @param UICollectionView object and cell tap index.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(cellType == .Banner || cellType == .Image){
            return  CGSize(width:collectionView.frame.size.width , height: collectionView.frame.height)
        } else if(cellType == .Category) {
            return CGSize(width:100 , height: 100)
        }
        //return  CGSize(width:140 , height: collectionView.frame.height)
        return  CGSize(width:200 , height: collectionView.frame.height)
    }
}

//MARK:- FeaturedCellDelegate method implementation
extension CategoryRow: FeaturedCellDelegate {
    
    /**
     *  FeaturedCellDelegate method definition, method is for selecting category in category list.
     *
     *  @param key cellIndex.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func methodSeeCategoriesProduct(cellIndex:Int){
        if(delegate != nil){
            delegate?.methodSeeCategoriesProduct(cellIndex: cellIndex, rowIndex: self.rowIndex)
        }
    }
}

//MARK:- ProductCellDelegate method implementation
extension CategoryRow: ProductVideoCellDelegate {
    func methodShowVideoDetails(cellIndex:Int){
        let banner = bannerList?[cellIndex]
                    
        if(banner?.banner != nil){
            //                cell.btnForProductImage.sd_setImage(with: URL(string: banner?.banner ?? ""), for: .normal, completed: nil)
            self.zoomableView(videoUrl: banner?.banner)
        }
    }
}

//MARK:- ProductCellDelegate method implementation
extension CategoryRow: ProductCellDelegate {
    
    /**
     *  ProductCellDelegate method definition, method is for selecting product in product list.
     *
     *  @param key cellIndex.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func methodShowProductDetails(cellIndex:Int){
        if(delegate != nil){
            delegate?.methodShowProductDetails(cellIndex: cellIndex, rowIndex: self.rowIndex)
        }
    }
}

extension CategoryRow{
    fileprivate func zoomableView(videoUrl:String?){
        // Create an array of images.
        var images:[LightboxImage] = []
        
        images.append(LightboxImage(
            imageURL: URL(string: videoUrl ?? "")!
        ))
        
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)
        // Present your controller.
        if #available(iOS 13.0, *) {
            controller.isModalInPresentation  = true
            controller.modalPresentationStyle = .fullScreen
        }
        
        // Set delegates.
        controller.pageDelegate = self
        controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
//        present(controller, animated: true, completion: nil)
        HelperConstant.appDelegate.window?.rootViewController?.present(controller, animated: true, completion: nil)
        
        LightboxConfig.handleVideo = { from, videoURL in
            // Custom video handling
            let videoController = AVPlayerViewController()
            videoController.player = AVPlayer(url: videoURL)
            
            from.present(videoController, animated: true) {
                videoController.player?.play()
            }
        }
    }
}


extension CategoryRow: LightboxControllerPageDelegate {
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        print(page)
    }
}

extension CategoryRow: LightboxControllerDismissalDelegate {
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        // ...
    }
}

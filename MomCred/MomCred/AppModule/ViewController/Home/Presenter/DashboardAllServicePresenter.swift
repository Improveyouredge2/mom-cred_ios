//
//  DashboardAllServicePresenter.swift
//  MomCred
//
//  Created by Rajesh Yadav on 10/10/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation


/**
 *  DashboardAllServicePresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class DashboardAllServicePresenter {
    
    var view:DashboardAllServiceViewController! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: DashboardAllServiceViewController) {
        self.view = view
    }
}

extension DashboardAllServicePresenter{
    
    /**
     *  Create Business information on server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getAllServiceHomeListDetail(callback:@escaping (_ status:Bool, _ response: DashboardAllServiceResponseData?, _ message: String?) -> Void){
        
        let requestData = PaginationRequest()
//        requestData.page_id = "\(self.view.currentPageIndex)"
        requestData.page_id = NSNumber(integerLiteral: self.view.currentPageIndex)
        
        DashboardService.getAllServiceHomeListData(requestData:requestData, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.service != nil){
//                        self.view.myServiceList = response?.data ?? []
//                        self.view.serviceList = []
                        
                        if(self.view.currentPageIndex == 0){
                            self.view.serviceList = []
                            self.view.serviceList = response?.service
                        } else {
//                            for service in response?.service! {
//                                self.serviceList?.append(service)
//                            }
                            
                            self.view.serviceList?.append(contentsOf: response?.service ?? [])
                        }
//
//                        if(response?.homeResponseData?.service != nil && (response?.homeResponseData?.service?.count)! > 0){
//                            let homeScrData = HomeScrResponseData()
//                            homeScrData.type = "list"
//                            homeScrData.name = LocalizationKeys.service_section.getLocalized()
//                            homeScrData.service = response?.homeResponseData?.service
//                            self.view.homeResponseData?.append(homeScrData)
//                        }
                        
                    }
                    
//                    self.view.updateScreenInfo()
                    self.view.collectionViewMain.reloadData()
                    
                    callback(status, response, message)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
//                    self.view.updateScreenInfo()
                    callback(status, response, message)
                }
            }
        })
    }
}

//
//  DashboardPresenter.swift
//  Mom-Cred
//
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  DashboardPresenter is presenter class
 *   The Presenter is responsible to act as the middle man between View and Model. It retrieves data from the Model and returns it formatted to the View. But unlike the typical MVC, it also decides what happens when you interact with the View.
 *
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class DashboardPresenter {
    
    static var parentListingList:ParentListingResponse?
    
    var view:DashboardView! // Object of account view screen
    
    /**
     *  Connect presenter class with view class.
     *
     *  @param key account view screen object.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func connectView(view: DashboardView) {
        self.view = view
    }
}

extension DashboardPresenter{
    
    fileprivate func reloadServiceChildListing(){
        
        if(DashboardPresenter.parentListingList != nil && DashboardPresenter.parentListingList?.parentListingData != nil &&  DashboardPresenter.parentListingList?.parentListingData?.services_types != nil && (DashboardPresenter.parentListingList?.parentListingData?.services_types?.count)! > 0){
            for service in (DashboardPresenter.parentListingList?.parentListingData?.services_types)! {
                self.serverChildListingRequest(parentId: service.listing_id ?? "")
            }
        }
        
        self.reloadExceptionChildListing()
        self.reloadServiceProviderChildListing()
    }
    
    fileprivate func reloadExceptionChildListing(){

        if(DashboardPresenter.parentListingList != nil &&  DashboardPresenter.parentListingList?.parentListingData != nil && DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification != nil && (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification?.count)! > 0){
            for expectional in (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification)!{
                self.serverExceptionalChildListingRequest(parentId: expectional.listing_id ?? "")
            }
        }
    }
    
    fileprivate func reloadServiceProviderChildListing(){

        if(DashboardPresenter.parentListingList != nil &&  DashboardPresenter.parentListingList?.parentListingData != nil && DashboardPresenter.parentListingList?.parentListingData?.service_provider != nil && (DashboardPresenter.parentListingList?.parentListingData?.service_provider?.count)! > 0){
            for serviceProvider in (DashboardPresenter.parentListingList?.parentListingData?.service_provider)!{
                self.serverServiceProviderRequest(parentId: serviceProvider.listing_id ?? "")
            }
        }
    }
    
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverChildListingRequest(parentId:String){
        
        let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        BIServiceStep1.getChildListing(requestData: childListingRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    
                    Spinner.hide()
                    if(response?.dataList != nil && (response?.dataList?.count)! > 0){
                        if(DashboardPresenter.parentListingList != nil && DashboardPresenter.parentListingList?.parentListingData != nil &&  DashboardPresenter.parentListingList?.parentListingData?.services_types != nil && (DashboardPresenter.parentListingList?.parentListingData?.services_types?.count)! > 0){
                            for parentList in (DashboardPresenter.parentListingList?.parentListingData?.services_types)! {
                                if((response?.dataList?.count)! > 0 && response?.dataList?[0].listing_parent?.caseInsensitiveCompare(parentList.listing_id ?? "0") == ComparisonResult.orderedSame){
                                    parentList.subcat = response?.dataList
                                    break
                                }
                            }
                        }
                    }
                    
                    // check all child list complete
                    var isFound = true
                    for parentList in (DashboardPresenter.parentListingList?.parentListingData?.services_types)!{
                        
                        if(parentList.subcat == nil){
                            isFound = false
                            break
                        }
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
    
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverExceptionalChildListingRequest(parentId:String){
        
        let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        BIServiceStep1.getChildListing(requestData: childListingRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    
                    
                    if(response?.dataList != nil && (response?.dataList?.count)! > 0){
                        if(DashboardPresenter.parentListingList != nil &&  DashboardPresenter.parentListingList?.parentListingData != nil && DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification != nil && (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification?.count)! > 0){
                            for parentList in (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification)!{
                                if((response?.dataList?.count)! > 0 && response?.dataList?[0].listing_parent?.caseInsensitiveCompare(parentList.listing_id ?? "0") == ComparisonResult.orderedSame){
                                    parentList.subcat = response?.dataList
                                    break
                                }
                            }
                        }
                        
                        // check all child list complete
                        var isFound = true
                        for parentList in (DashboardPresenter.parentListingList?.parentListingData?.exceptional_classification)!{
                            if(parentList.subcat == nil){
                                isFound = false
                                break
                            }
                        }
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
    
    /**
     *  Get Child listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func serverServiceProviderRequest(parentId:String){
        
        let childListingRequest = ChildListingRequest(listing_id: parentId)
        
        BIServiceStep1.getChildListing(requestData: childListingRequest, callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    
                    if(response?.dataList != nil && (response?.dataList?.count)! > 0){
                        if(DashboardPresenter.parentListingList != nil &&  DashboardPresenter.parentListingList?.parentListingData != nil && DashboardPresenter.parentListingList?.parentListingData?.service_provider != nil && (DashboardPresenter.parentListingList?.parentListingData?.service_provider?.count)! > 0){
                            for parentList in (DashboardPresenter.parentListingList?.parentListingData?.service_provider)!{
                                if((response?.dataList?.count)! > 0 && response?.dataList?[0].listing_parent?.caseInsensitiveCompare(parentList.listing_id ?? "0") == ComparisonResult.orderedSame){
                                    parentList.subcat = response?.dataList
                                    break
                                }
                            }
                        }
                        
                        // check all child list complete
                        var isFound = true
                        for parentList in (DashboardPresenter.parentListingList?.parentListingData?.service_provider)!{
                            if(parentList.subcat == nil){
                                isFound = false
                                break
                            }
                        }
                    }
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
    
    /**
     *  Get Parent listing from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN100]
     */
    func parentListingRequest(){
        
        BIServiceStep1.getParentListing(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    DashboardPresenter.parentListingList = response
                    
                    self.reloadServiceChildListing()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    
                }
            }
        })
    }
    
    /**
     *  Get Home screen detail informatoin from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getHomeScrDetail(){
        
        DashboardService.getHomeListData(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    self.view.isFirstLoad = false
                    
                    // Get parent listing
                    if DashboardPresenter.parentListingList == nil, let token = PMUserDefault.getAppToken(), !token.trim().isEmpty {
                        self.parentListingRequest()
                    }
                    
                    //TODO: Prepare business list screen and send all data to list screen
                    if(response?.homeResponseData != nil){
//                        self.view.myServiceList = response?.data ?? []
                        self.view.homeResponseData = []
                        if(response?.homeResponseData?.imagebanner != nil && (response?.homeResponseData?.imagebanner?.count)! > 0){
                            let homeScrData = HomeScrResponseData()
                            homeScrData.type = "banner"
                            homeScrData.imagebanner = response?.homeResponseData?.imagebanner
                            self.view.homeResponseData?.append(homeScrData)
                        }
                        
                        if(response?.homeResponseData?.videobanner != nil && (response?.homeResponseData?.videobanner?.count)! > 0){
                            let homeScrData = HomeScrResponseData()
                            homeScrData.type = "video"
                            homeScrData.name = LocalizationKeys.video_section.getLocalized()
                            homeScrData.desc = LocalizationKeys.video_section_sub_title.getLocalized()
                            homeScrData.videobanner = response?.homeResponseData?.videobanner
                            self.view.homeResponseData?.append(homeScrData)
                        }
                        
                        if(response?.homeResponseData?.service != nil && (response?.homeResponseData?.service?.count)! > 0){
                            let homeScrData = HomeScrResponseData()
                            homeScrData.type = "list"
                            homeScrData.name = LocalizationKeys.service_section.getLocalized()
                            homeScrData.service = response?.homeResponseData?.service
                            self.view.homeResponseData?.append(homeScrData)
                        }
                        
                    }
                    
                    let hideBackgroundCheck = (response?.isProvider ?? 0) == 0
                    self.view.backgroundCheckContainer.isHidden = hideBackgroundCheck
                    
                    self.view.updateScreenInfo()
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    self.view.isFirstLoad = false
                    
                    self.view.updateScreenInfo()
                }
            }
        })
    }
    
    /**
     *  Get Home screen detail informatoin from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getKeywordSearch(searchText:String, pageIndex:String){
        
        let keywordSearchRequest = KeywordSearchRequest()
        keywordSearchRequest.keyword = searchText
        keywordSearchRequest.page_id = pageIndex
        
        DashboardService.getKeywordSearchData(requestData:keywordSearchRequest,callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    self.view.showSearchResult(result: response, keywordSearchRequest: keywordSearchRequest)
                }
            } else {
                OperationQueue.main.addOperation() {
                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                    self.view.showSearchResult(result: nil, keywordSearchRequest: nil)
                }
            }
        })
    }
    
    /**
     *  Get user profile information from server.
     *
     *  @param key server response.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    func getProfile(){
        PMEditProfileService.getData(callback: { (status, response, message) in
            
            if(status == true){
                OperationQueue.main.addOperation() {
                    PMUserDefault.saveSubsriptionInfo(subsriptionInfo: response?.loginResponseData?.subscription?.toJSONString() ?? "")
                    PMUserDefault.saveSubsriptionPrice(subsriptionPrice: response?.loginResponseData?.membershipprice ?? "")
                    
                    PMUserDefault.saveStripeCharges(subsriptionPrice: response?.loginResponseData?.stripefees ?? "")
                    
                    PMUserDefault.savePlanId(subsriptionPrice: response?.loginResponseData?.plan_id ?? "")
                }
            } else {
                OperationQueue.main.addOperation() {
//                    Spinner.hide()
                    Helper.sharedInstance.showNotificationBanner(title: LocalizationKeys.error.getLocalized(), message: message ?? "", buttonTitle: nil, controller: nil)
                }
            }
        })
    }
}


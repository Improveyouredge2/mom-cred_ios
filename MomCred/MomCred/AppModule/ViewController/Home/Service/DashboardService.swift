//
//  DashboardService.swift
//  NotificationLib
//
//  Created by Apple_iOS on 23/01/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
 *  DashboardService is server API calling with request/reponse.
 *
 *  @Developed By: Team Consagous [CNSGSIN054]
 */
class DashboardService{
    
    /**
     *  Method to get banner list for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getBannerData(callback:@escaping (_ status:Bool, _ response: BannerResponse?, _ message: String?) -> Void) {
        
        let userID = NMUserDefault.getUserId()
        
        let notificationRequest = BannerRequest()
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = notificationRequest.toJSONString()
        let reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_PRODUCT_BANNER_LIST, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:BannerResponse? = BannerResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
                
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getHomeListData(callback:@escaping (_ status:Bool, _ response: HomeResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        if(role_id == AppUser.ServiceProvider.rawValue){
            let req  = APIManager().sendPostRequest(urlString: APIKeys.API_DASHBOARD_SERVICE_PROVIDER, header: APIHeaders().getDefaultHeaders(), strJSON: nil)
            reqPost = req
        } else if(role_id == AppUser.Enthusiast.rawValue || role_id == AppUser.LocalBusiness.rawValue){
            reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_DASHBOARD_USER, header: APIHeaders().getDefaultHeaders(), strJSON: nil)
        }
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:HomeResponse? = HomeResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getAllServiceHomeListData(requestData:PaginationRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceResponseData?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        let role_id = PMUserDefault.getUserRole() ?? AppUser.Enthusiast.rawValue
        
        if(role_id == AppUser.ServiceProvider.rawValue){
            reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_DASHBOARD_SERVICE_PROVIDER_ALL_SERVICE, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        } else if(role_id == AppUser.Enthusiast.rawValue || role_id == AppUser.LocalBusiness.rawValue){
            reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_DASHBOARD_USER_ALL_SERVICE, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        }
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:DashboardAllServiceResponseData? = DashboardAllServiceResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getKeywordSearchData(requestData:KeywordSearchRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceProviderResponseData?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_KEYBOARD_SEARCH, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:DashboardAllServiceProviderResponseData? = DashboardAllServiceProviderResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getServiceCategoryByUserData(requestData:BusiServicesRequest?, callback:@escaping (_ status:Bool, _ response: GetListingCategoryModelResponse?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_SERVICE_CATEGORY_BY_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let res:GetListingCategoryModelResponse? = GetListingCategoryModelResponse().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getPersonnelCategoryByUserData(requestData:BusiServicesRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceResponseData?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_PERSONNEL_CATEGORY_BY_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:DashboardAllServiceResponseData? = DashboardAllServiceResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getFacilityCategoryByUserData(requestData:BusiServicesRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceResponseData?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_FACILITY_CATEGORY_BY_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:DashboardAllServiceResponseData? = DashboardAllServiceResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getInsideLookCategoryByUserData(requestData:BusiServicesRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceResponseData?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_INSIDE_LOOK_CATEGORY_BY_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:DashboardAllServiceResponseData? = DashboardAllServiceResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
    
    /**
     *  Method connect to get HomeList data for screen.
     *
     *  @param key callback method update information on screen.
     *
     *  @return empty.
     *
     *  @Developed By: Team Consagous [CNSGSIN054]
     */
    static func getInstructionalCategoryByUserData(requestData:BusiServicesRequest?, callback:@escaping (_ status:Bool, _ response: DashboardAllServiceResponseData?, _ message: String?) -> Void) {
        
        var reqPost:URLRequest?
        
        // POST
        // Convert Model request object into JSONString
        let strJSON = requestData?.toJSONString()
        
        reqPost = APIManager().sendPostRequest(urlString: APIKeys.API_INSTRUCTIONAL_CONTENT_CATEGORY_BY_USER, header: APIHeaders().getDefaultHeaders(), strJSON: strJSON)
        
        // API Calling with URLRequest
        APIManager.request(urlRequest: reqPost) { (status, response, error) in
            
            // Check API Response Status
            if status == true{ // Success
                
                if let responseDict = response {
                    let  res:DashboardAllServiceResponseData? = DashboardAllServiceResponseData().getModelObjectFromServerResponse(jsonResponse: responseDict)
                    
                    print(res ?? "")
                    callback(status, res, res?.message)
                }
            } else { // Failed
                callback(status, nil, error)
            }
        }
    }
}

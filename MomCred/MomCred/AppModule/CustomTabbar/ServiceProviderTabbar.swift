//
//  ServiceProviderTabbar.swift
//  MomCred
//
//  Created by Apple_iOS on 29/05/19.
//  Copyright © 2019 Consagous. All rights reserved.
//

import Foundation

/**
* ServiceProviderTabbar is a custom class inherited from JBTabBarController for making specific tabbar for service provider tabbar.
*
*  @Developed By: Team Consagous [CNSGSIN054]
*/
class ServiceProviderTabbar: JBTabBarController {
    
}

//
//  AuthorizePaymentViewController.swift
//  Mom-Cred
//
//  Created by Rajesh Yadav on 21/10/19.
//  Copyright © 2019 consagous. All rights reserved.
//

import UIKit
import AuthorizeNetAccept

//struct PAYTM_LIVE {
//
//    let GENERATE_CHECKSUM = "http://13.232.205.186/PaytmAppLive/generateChecksum.php"
//    let VERIFY_CHECKSUM = "http://13.232.205.186/PaytmAppLive/verifyChecksum.php"
//    let MerchantID = "mUIEvy58228107315733"
//    let Website = "DEFAULT"
//    let IndustryID = "Retail"
//    let ChannelID = "WAP"
//    let MerchantKey = "LQRtKWZAWUMPvS5s"
//    let CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="
//}

struct PAYMENT_PRODUCTION {
    static let kClientName = "MAX GREENBERG"
    static let API_LOGIN_ID = "3yK36Vv7"
    static let kClientKey = "" // Transcation key
}

struct PAYMENT_STAGING {
    static let kClientName = "Simon"
    static let API_LOGIN_ID = "7A9Fxt3c4"
    static let kClientKey = "5p9V24rpNC65Se6D6bubg85ZRm52MDH6hA9n56EYs3cW4QEgc94FwNAG7Wej2ydN" // Public Client Key
}


class AuthorizePaymentViewController {
    
    static let sharedInstance = AuthorizePaymentViewController()
    
//    var cardNumberBuffer: String?
//    var cardExpirationMonth: String?
//    var cardExpirationYear: String?
//    var cardVerificationCode: String?
    
    func purchasePayment(cardNumberBuffer: String?, cardExpiration: String?, cardVerificationCode: String?){
        let handler = AcceptSDKHandler(environment: AcceptSDKEnvironment.ENV_TEST)
        
        let expirationDates = cardExpiration?.split(separator: "/")
        var cardExpirationMonth = ""
        var cardExpirationYear = ""
        if(expirationDates != nil && (expirationDates?.count)! > 1){
            cardExpirationMonth = String(expirationDates![0])
            cardExpirationYear = String(expirationDates![1])
        }
        
        let request = AcceptSDKRequest()
        request.merchantAuthentication.name = PAYMENT_STAGING.API_LOGIN_ID
        request.merchantAuthentication.clientKey = PAYMENT_STAGING.kClientKey
        
//        request.merchantAuthentication.name = PAYMENT_PRODUCTION.API_LOGIN_ID
//        request.merchantAuthentication.clientKey = PAYMENT_PRODUCTION.kClientKey
        
        request.securePaymentContainerRequest.webCheckOutDataType.token.cardNumber = cardNumberBuffer?.replacingOccurrences(of: " ", with: "") ?? ""
        request.securePaymentContainerRequest.webCheckOutDataType.token.expirationMonth = cardExpirationMonth ?? ""
        request.securePaymentContainerRequest.webCheckOutDataType.token.expirationYear = cardExpirationYear ?? ""
        
        request.securePaymentContainerRequest.webCheckOutDataType.token.cardCode = cardVerificationCode ?? ""
        
        handler!.getTokenWithRequest(request, successHandler: { (inResponse:AcceptSDKTokenResponse) -> () in
            OperationQueue.main.addOperation() {
                print(inResponse.description)
                //                self.updateTokenButton(true)
                //
                //                self.activityIndicatorAcceptSDKDemo.stopAnimating()
                //                print("Token--->%@", inResponse.getOpaqueData().getDataValue())
                //                var output = String(format: "Response: %@\nData Value: %@ \nDescription: %@", inResponse.getMessages().getResultCode(), inResponse.getOpaqueData().getDataValue(), inResponse.getOpaqueData().getDataDescriptor())
                //                output = output + String(format: "\nMessage Code: %@\nMessage Text: %@", inResponse.getMessages().getMessages()[0].getCode(), inResponse.getMessages().getMessages()[0].getText())
                //                self.textViewShowResults.text = output
                //                self.textViewShowResults.textColor = UIColor.greenColor()
            }
        }) { (inError:AcceptSDKErrorResponse) -> () in
            OperationQueue.main.addOperation() {
                
                print(inError.description)
                
                //            self.activityIndicatorAcceptSDKDemo.stopAnimating()
                //            self.updateTokenButton(true)
                //
                //            let output = String(format: "Response:  %@\nError code: %@\nError text:   %@", inError.getMessages().getResultCode(), inError.getMessages().getMessages()[0].getCode(), inError.getMessages().getMessages()[0].getText())
                //            self.textViewShowResults.text = output
                //            self.textViewShowResults.textColor = UIColor.redColor()
                //            print(output)
            }
        }
    }

}


